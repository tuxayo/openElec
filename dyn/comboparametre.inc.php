<?php
/**
 *  Affectation des paramètres spécifiques applications pour COMBO.PHP 1.1
 *
 * @package openelec
 * @version SVN : $Id$
 */

// Probleme lors de la recherche d'elements avec une apostrophe
// Il faut gerer ca d'une maniere plus globale dans spg/combo.php
// dans openmairie_exemple
$recherche = addslashes($recherche);

/**
 * Dans les cas suivants la recherche porte sur le début de la chaine
 * si on saisit AB, la requête portera sur 'AB%' et non '%AB%' comme 
 * c'est la cas par défaut
 */
if ($champOrigine == "code_departement_naissance") {
    $debut = 1;
}
if ($champOrigine == "code_lieu_de_naissance") {
    $debut = 1;
}
if ($champOrigine == "provenance") {
    $debut = 1;
}


/**
 *
 */
if ($champOrigine == "mandant" or $champOrigine == "mandataire"
    or $champOrigine == "id_electeur" or $champOrigine == "id_electeur_europe") {
    //
    $sql = "select * from ".$table." where ".$zoneOrigine."=".$recherche." ";
    $sql .= " and collectivite='".$_SESSION['collectivite']."'";
}

// Champ département 97 98 99 sur plus de 2 caractères
if ($champCorrel2=="code_departement") {
    if(strlen($zoneCorrel2) > 3)
        $zoneCorrel2=substr($zoneCorrel2,0,2);
}


/**
 * Voie
 */
if ($champOrigine=="libelle_voie" OR $champOrigine=="code_voie") {
    $sql="select * from ".$table." where CAST(".$zoneOrigine.
        " as VARCHAR(50)) like '%".$recherche."%' and code_collectivite='".
        $_SESSION['collectivite']."'";
}

/**
 * Procurations et Centres de vote
 * $debut=0
 * $longueurRecherche = 1;
 */
// z_mandant : form.php?obj=procuration - mandant
if ($champOrigine=="z_mandant") {
    if (substr($recherche,strlen($recherche)-1,1)=='*') {
        $sql = "select * from ".$table." where CAST(".$zoneOrigine.
            " as VARCHAR(50)) like '".substr($recherche,0,strlen($recherche)-1).
            "%' and collectivite='".$_SESSION['collectivite']."' order by withoutaccent(lower(nom)) ";
    } else
        if (substr($recherche,2,1)=='/')
            $sql = "select * from ".$table." where date_naissance like '".
                substr($recherche,6,4)."-".substr($recherche,3,2)."-".substr($recherche,0,2).
                "%' and collectivite='".$_SESSION['collectivite']."' order by withoutaccent(lower(nom)) ";
        else
            $sql = "select * from ".$table." where CAST(".$zoneOrigine.
                " as VARCHAR(50)) like '%".$recherche."%' and collectivite='".
                $_SESSION['collectivite']."'  order by withoutaccent(lower(prenom)) ";
}

// z_mandataire : form.php?obj=procuration - mandataire
if ($champOrigine=="z_mandataire") {
    if (substr($recherche,strlen($recherche)-1,1)=='*') {
        $sql = "select * from ".$table." where CAST(".$zoneOrigine.
            " as VARCHAR(50)) like '".substr($recherche,0,strlen($recherche)-1).
            "%' and collectivite='".$_SESSION['collectivite']."' order by withoutaccent(lower(nom)) ";
    } else
        if (substr($recherche,2,1)=='/')
            $sql = "select * from ".$table." where date_naissance like '".
                substr($recherche,6,4)."-".substr($recherche,3,2)."-".substr($recherche,0,2).
                "%' and collectivite='".$_SESSION['collectivite']."'  order by withoutaccent(lower(nom)) ";
        else
            $sql = "select * from ".$table." where CAST(".$zoneOrigine.
                " as VARCHAR(50)) like '%".$recherche. "%' and collectivite='".
                $_SESSION['collectivite']."' order by withoutaccent(lower(prenom))";
}

// z_electeur : form.php?obj=centrevote - electeur
if ($champOrigine=="z_electeur") {
    if (substr ($recherche, strlen ($recherche) - 1, 1) == '*') {
        $sql = "select * from ".$table." where CAST(".$zoneOrigine.
            " as VARCHAR(50)) like '".substr($recherche,0,strlen($recherche)-1).
            "%' and collectivite='".$_SESSION['collectivite']."' and liste='".
            $_SESSION ['liste']."' order by withoutaccent(lower(nom)) ";
    }
    else 
        if (substr($recherche, 2, 1)=='/')
            $sql = "select * from ".$table." where date_naissance like '".
                substr($recherche,6,4)."-".substr($recherche,3,2)."-".
                substr($recherche,0,2)."%' and collectivite='".
                $_SESSION['collectivite']."' order by withoutaccent(lower(nom)) ";
        else
            $sql = "select * from ".$table.
                " where CAST(".$zoneOrigine." as VARCHAR(50)) like '%".$recherche.
                "%' and collectivite='".$_SESSION['collectivite']."' and liste='".
                $_SESSION['liste']."' order by withoutaccent(lower(prenom)) ";
}

?>

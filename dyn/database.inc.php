<?php
/**
 * Ce fichier permet le parametrage de la connexion a labase de donnees,
 * chaque entree du tableau correspond a une base differente. Attention
 * l'index du tableau conn represente l'identifiant du dossier dans lequel
 * seront stockes les fichiers propres a cette base dans l'application
 * 
 * @package openmairie_exemple
 * @version SVN : $Id: database.inc.php 288 2010-12-02 13:52:25Z fmichon $
 */

//
$conn = array();

//
array_push($conn,
    array(
        "openElec",
        "pgsql",
        "pgsql",
        "postgres", // login
        "postgres", // mot de passe
        "tcp",
        "localhost",
        "5432",
        "",
        "openelec", // nom de la base
        "AAAA-MM-JJ",
        "public",
        "",
        "ldap-default",
        "mail-default",
    ));

?>

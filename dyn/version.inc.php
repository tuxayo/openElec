<?php
/**
 * Ce fichier permet de stocker la version de l'application
 *
 * @package openmairie_exemple
 * @version SVN: $Id: version.inc.php 100 2010-09-09 08:29:29Z fmichon $
 */

/**
 * Le numero de version est affiche en permanence dans le footer
 */
$version = "4.4.4.dev0";

?>

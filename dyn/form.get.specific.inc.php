<?php
/**
 * Ce fichier permet de definir des variables specifiques a passer dans la
 * methode formulaire des objets metier
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

/**
 * Exemple : un ecran specifique me permet de passer la date de naissance de
 * l'utilisateur au formulaire uniquement lorsque l'objet est "agenda".
 *
 * if ($obj == "agenda") {
 *     $datenaissance = "";
 *     if (isset($_GET['datenaissance'])) {
 *         $datenaissance = $_GET['datenaissance'];
 *     }
 *     $extra_parameters["datenaissance"] = $datenaissance;
 * }
 *
 * Ainsi dans la methode formulaire de l'objet en question la valeur de la date
 * de naissance sera accessible
 */

// Identifiant de l'objet metier du formulaire et mode d'ajout
if (isset($_GET['idx'])) {
    $idx = $_GET['idx'];
    if (isset($_GET['ids'])) {
        $maj = 2;
    } else {
        $maj = 1;
    }
} else {
    if (isset($_GET['maj'])) {
        $maj = $_GET['maj'];
        $idx = "]";
    } else {
        $idx = "]";
        $maj = 0;
    }
}

//
if ($obj == "inscription" && $maj == 0) {
    $exact = "";
    if (isset($_GET['exact']) && $_GET['exact']==true) {
        $exact = $_GET['exact'];
    }
    $nom = "";
    if (isset($_GET['nom'])) {
        $nom = $_GET['nom'];
    }
    $datenaissance = "";
    if (isset($_GET['datenaissance'])) {
        $datenaissance = $_GET['datenaissance'];
    }
    $extra_parameters = array(
        "nom" => $nom,
        "datenaissance" => $datenaissance,
        "exact" => $exact,
    );
}

if ($obj == "electeur") {
    $exact = "";
    if (isset($_GET['exact']) && $_GET['exact'] == true) {
        $exact = $_GET['exact'];
    }
    $nom = "";
    if (isset($_GET['nom'])) {
        $nom = $_GET['nom'];
    }
    $datenaissance = "";
    if (isset($_GET['datenaissance'])) {
        $datenaissance = $_GET['datenaissance'];
    }
    $idxelecteur = "]";
    if (isset($_GET['idxelecteur'])) {
        $idxelecteur = $_GET['idxelecteur'];
    }
    $marital = "";
    if (isset($_GET['marital'])) {
        $marital = $_GET['marital'];
    }
    $prenom = "";
    if (isset($_GET['prenom'])) {
        $prenom = $_GET['prenom'];
    }
    $origin = "electeur";
    if (isset($_GET['origin'])) {
        $origin = $_GET['origin'];
    }
    $retour = "";
    if (isset($_GET['retour'])){
        $retour = $_GET ['retour'];
    }
    $extra_parameters = array(
        "retour" => $retour,
        "nom" => $nom,
        "prenom" => $prenom,
        "exact" => $exact,
        "datenaissance" => $datenaissance,
        "marital" => $marital,
        "origin" => $origin,
        "idxelecteur" => $idxelecteur,
    );
}

if ($obj == "inscription_office_valider") {
    $io = "";
    if (isset($_GET['io'])) {
        $io = $_GET['io'];
    }
    $extra_parameters = array(
        "io" => $io,
    );
}

if ($obj == "radiation_office_valider") {
    $idrad = "";
    if (isset($_GET['idrad'])) {
        $idrad = $_GET['idrad'];
    }
    if (isset($_GET['idxelecteur'])) {
        $idxelecteur = $_GET['idxelecteur'];
    }
    $extra_parameters = array(
        "idrad" => $idrad,
        "idxelecteur" => $idxelecteur,
    );
}

if ($obj == "modification" || $obj == "radiation" || $obj == "radiation_insee_valider") {
    //
    if (isset($_GET['idxelecteur'])) {
        if ($obj == "modification") {
            // Recuperation d'un mouvement actif a la date de tableau en cours
            // correspondant a l'id_electeur passe en parametre
            $sql_idx = "select id from mouvement as m inner join param_mouvement as pm";
            $sql_idx .= " on m.types=pm.code where typecat='Modification' and ";
            $sql_idx .= " id_electeur = ".$_GET['idxelecteur']." and etat='actif' ";
            $sql_idx .= " and date_tableau='".$f->collectivite['datetableau']."'";
        } elseif ($obj == "radiation" || $obj == "radiation_insee_valider") {
            // Recuperation d'un mouvement actif a la date de tableau en cours
            // correspondant a l'id_electeur passe en parametre
            $sql_idx = "select id from mouvement as m inner join param_mouvement as pm";
            $sql_idx .= " on m.types=pm.code where typecat='Radiation' and ";
            $sql_idx .= " id_electeur = ".$_GET['idxelecteur']." and etat='actif' ";
            $sql_idx .= " and date_tableau='".$f->collectivite['datetableau']."'";
        }
        $res = $f->db->getOne($sql_idx);
        // Verification de l'erreur
        $f->isDatabaseError($res);
        // Si un mouvement existe alors on l'affecte en idx
        if ($res != NULL) {
            //
            if ($maj != 0) {
                $_GET['idx'] = $res;
            }
            // XXX Faut-il supprimer la variable get ?
            //unset($_GET['idxelecteur']);
        }
    }
    //
    if (isset($_GET['idx'])) {
        $idx = $_GET['idx'];
        if (isset($_GET['ids'])) {
            $maj = 2;
        } else {
            $maj = 1;
        }
    } else {
        if (isset($_GET['maj'])) {
            $maj = $_GET['maj'];
            $idx = "]";
        } else {
            $idx = "]";
            $maj = 0;
        }
    }
    //
    $exact = "";
    if (isset($_GET['exact']) && $_GET['exact'] == true) {
        $exact = $_GET['exact'];
    }
    $nom = "";
    if (isset($_GET['nom'])) {
        $nom = $_GET['nom'];
    }
    $datenaissance = "";
    if (isset($_GET['datenaissance'])) {
        $datenaissance = $_GET['datenaissance'];
    }
    $idxelecteur = "]";
    if (isset($_GET['idxelecteur'])) {
        $idxelecteur = $_GET['idxelecteur'];
    }
    $marital = "";
    if (isset($_GET['marital'])) {
        $marital = $_GET['marital'];
    }
    $prenom = "";
    if (isset($_GET['prenom'])) {
        $prenom = $_GET['prenom'];
    }
    $idrad = "";
    if (isset($_GET['idrad'])) {
        $idrad = $_GET['idrad'];
    }
    if ($obj == "modification") {
        $origin = "electeur_modification";
        if (isset($_GET['origin'])) {
            $origin = $_GET['origin'];
        }
    } elseif ($obj == "radiation") {
        $origin = "electeur_radiation";
        if (isset($_GET['origin'])) {
            $origin = $_GET['origin'];
        }
    } elseif ($obj == "radiation_insee_valider") {
        $origin = "radiation_insee";
        if (isset($_GET['origin'])) {
            $origin = $_GET['origin'];
        }
    }
    $extra_parameters = array(
        "nom" => $nom,
        "prenom" => $prenom,
        "exact" => $exact,
        "datenaissance" => $datenaissance,
        "marital" => $marital,
        "origin" => $origin,
        "idxelecteur" => $idxelecteur,
        "idrad" => $idrad,
    );
}

?>

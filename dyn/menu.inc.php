<?php
/**
 * Ce fichier permet de configurer quelles actions vont etre disponibles
 * dans le menu.
 *
 * @package openmairie_exemple
 * @version SVN : $Id: menu.inc.php 100 2010-09-09 08:29:29Z fmichon $
 */

/**
 * $menu est le tableau associatif qui contient tout le menu de
 * l'application, il contient lui meme un tableau par rubrique, puis chaque
 * rubrique contient un tableau par lien
 *
 * Caracteristiques :
 * --- tableau rubrik
 *     - title [obligatoire]
 *     - description (texte qui s'affiche au survol de la rubrique)
 *     - href (contenu du lien href)
 *     - class (classe css qui s'affiche sur la rubrique)
 *     - right (droit que l'utilisateur doit avoir pour visionner cette rubrique)
 *     - links [obligatoire]
 *
 * --- tableau links
 *     - title [obligatoire]
 *     - href [obligatoire] (contenu du lien href)
 *     - class (classe css qui s'affiche sur l'element)
 *     - right (droit que l'utilisateur doit avoir pour visionner cet element)
 *     - target (pour ouvrir le lien dans une nouvelle fenetre)
 */
$menu = array();

// {{{ Rubrique SAISIE
//
$rubrik = array(
    "title" => _("Saisie Electeur"),
    "class" => "saisie",
    "right" => /*DROIT*/"menu_saisie", 
);
//
$links = array();
array_push($links,
    array(
        "href" => "../app/changecollectivite.php",
        "title" => _("Selectionner la commune"),
        "right" => /*DROIT*/"collectivitedefautmenu",
    ));
array_push($links,
    array(
        "href" => "../app/inscription.search.php",
        "title" => _("Inscription"),
        "right" => /*DROIT*/"inscription",
        "class" => "inscription",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"electeur_modification",
            /*DROIT*/"electeur_radiation",
            /*DROIT*/"electeur_mouvement"),
    ));
array_push($links,
    array(
        "href" => "../app/electeur.search.php?obj=electeur_modification",
        "title" => _("Modification"),
        "right" => /*DROIT*/"electeur_modification",
        "class" => "modification",
    ));
array_push($links,
    array(
        "href" => "../app/electeur.search.php?obj=electeur_radiation",
        "title" => _("Radiation"),
        "right" => /*DROIT*/"electeur_radiation",
        "class" => "radiation",
    ));
array_push($links,
    array(
        "href" => "../app/electeur.search.php?obj=electeur_mouvement",
        "title" => _("Modification / Radiation"),
        "right" => /*DROIT*/"electeur_mouvement",
        "class" => "electeur_mouvement",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"procuration",
            /*DROIT*/"centrevote",
            /*DROIT*/"mairieeurope"),
    ));
array_push($links,
    array(
        "href" => "../scr/form.php?obj=procuration",
        "title" => _("Procuration"),
        "right" => /*DROIT*/"procuration",
        "class" => "procuration",
    ));
array_push($links,
    array(
        "href" => "../scr/form.php?obj=centrevote",
        "title" => _("Centre de vote"),
        "right" => /*DROIT*/"centrevote",
        "class" => "centrevote",
    ));
array_push($links,
    array(
        "href" => "../scr/form.php?obj=mairieeurope",
        "title" => _("Mairie Europe"),
        "right" => /*DROIT*/"mairieeurope",
        "class" => "mairieeurope",
    ));
$rubrik['links'] = $links;
array_push($menu, $rubrik);
// }}}

// {{{ Rubrique CONSULTATION
//
$rubrik = array(
    "title" => _("Consultation"),
    "class" => "consultation",
    "right" => /*DROIT*/"menu_consultation",
);
//
$links = array();
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=consult_electeur",
        "title" => _("Liste electorale"),
        "right" => /*DROIT*/"consult_electeur_tab",
        "class" => "electeurs",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=consult_archive",
        "title" => _("Archive"),
        "right" => /*DROIT*/"consult_archive_tab",
        "class" => "archive",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"inscription_tab"),
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=inscription",
        "title" => _("Inscription"),
        "right" => /*DROIT*/"inscription_tab",
        "class" => "inscriptions",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"modification_tab",
            /*DROIT*/"radiation_tab"),
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=modification",
        "title" => _("Modification"),
        "right" => /*DROIT*/"modification_tab",
        "class" => "modifications",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=radiation",
        "title" => _("Radiation"),
        "right" => /*DROIT*/"radiation_tab",
        "class" => "radiations",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"procuration_tab",
            /*DROIT*/"centrevote_tab",
            /*DROIT*/"mairieeurope_tab"),
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=procuration",
        "title" => _("Procuration"),
        "right" => /*DROIT*/"procuration_tab",
        "class" => "procuration",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=centrevote",
        "title" => _("Centre de vote"),
        "right" => /*DROIT*/"centrevote_tab",
        "class" => "centrevote",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=mairieeurope",
        "title" => _("Mairie Europe"),
        "right" => /*DROIT*/"mairieeurope_tab",
        "class" => "mairieeurope",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"inscription_office_tab"),
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=inscription_office",
        "title" => _("Inscription office INSEE"),
        "right" => /*DROIT*/"inscription_office_tab",
        "class" => "inscription-office-insee",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=radiation_insee",
        "title" => _("Radiation INSEE"),
        "right" => /*DROIT*/"radiation_insee_tab",
        "class" => "radiation-insee",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"tmp"),
    ));
array_push($links,
    array(
        "href" => "../app/tmp.php",
        "title" => _("Traces et Editions"),
        "right" => /*DROIT*/"tmp",
    ));  
$rubrik['links'] = $links;
array_push($menu, $rubrik);
// }}}

// {{{ Rubrique EDITION
//
$rubrik = array(
    "title" => _("Edition"),
    "right" => /*DROIT*/"menu_edition",
    "class" => "edition",
);
//
$links = array();
array_push($links,
    array(
        "href" => "../app/revision_electorale.php",
        "title" => _("Revision electorale"),
        "right" => /*DROIT*/"revision_electorale",
        "class" => "pdf",
    ));
array_push($links,
    array(
        "href" => "../app/editions.bureau.php",
        "title" => _("Par Bureau"),
        "right" => /*DROIT*/"bureau_edition",
        "class" => "pdf",
    ));
array_push($links,
    array(
        "href" => "../app/editions.php",
        "title" => _("Generales"),
        "right" => /*DROIT*/"edition",
        "class" => "pdf",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"statistiques",),
    ));
array_push($links,
    array(
        "href" => "../app/statistiques.php",
        "title" => _("Statistiques"),
        "right" => /*DROIT*/"statistiques",
        "class" => "statistiques",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"reqmo",),
    ));
array_push($links,
    array(
        "href" => "../scr/reqmo.php",
        "class" => "reqmo",
        "title" => _("Requetes memorisees"),
        "right" => /*DROIT*/"reqmo",
    ));
$rubrik['links'] = $links;
array_push($menu, $rubrik);
// }}}

// {{{ Rubrique TRAITEMENT
//
$rubrik = array(
    "title" => _("Traitement"),
    "class" => "traitement",
    "right" => /*DROIT*/"menu_traitement",
);
//
$links = array();
array_push($links,
    array(
        "href" => "../trt/module_commission.php",
        "title" => _("Commissions"),
        "right" => /*DROIT*/"module_commission",
        "class" => "commission",
    ));
array_push($links,
    array(
        "href" => "../trt/module_insee.php",
        "title" => _("Insee"),
        "right" => /*DROIT*/"module_insee",
        "class" => "insee",
    ));
array_push($links,
    array(
        "href" => "../trt/module_prefecture.php",
        "title" => _("Prefecture"),
        "right" => /*DROIT*/"module_prefecture",
        "class" => "prefecture",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"module_election",
            /*DROIT*/"traitement_annuel",
            /*DROIT*/"traitement_j5"),
    ));
array_push($links,
    array(
        "href" => "../trt/module_election.php",
        "title" => _("Election"),
        "right" => /*DROIT*/"module_election",
        "class" => "traitement",
    ));
array_push($links,
    array(
        "href" => "../trt/module_traitement_j5.php",
        "title" => _("Traitement J-5"),
        "right" => /*DROIT*/"module_traitement_j5",
        "class" => "traitement",
    ));
array_push($links,
    array(
        "href" => "../trt/module_traitement_annuel.php",
        "title" => _("Traitement Fin d'annee"),
        "right" => /*DROIT*/"module_traitement_annuel",
        "class" => "traitement",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"module_centrevote",
            /*DROIT*/"module_carteretour",
            /*DROIT*/"module_jury"),
    ));

array_push($links,
    array(
        "href" => "../trt/module_centrevote.php",
        "title" => _("Centre de vote"),
        "right" => /*DROIT*/"module_centrevote",
        "class" => "centrevote",
    ));
array_push($links,
    array(
        "href" => "../trt/module_carteretour.php",
        "title" => _("Cartes en retour"),
        "right" => /*DROIT*/"module_carteretour",
        "class" => "carteretour",
    ));
array_push($links,
    array(
        "href" => "../trt/module_jury.php",
        "title" => _("Jury d'assises"),
        "right" => /*DROIT*/"module_jury",
        "class" => "jury",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"module_refonte",
            /*DROIT*/"module_archivage",
            /*DROIT*/"module_redecoupage"),
    ));
array_push($links,
    array(
        "href" => "../trt/module_refonte.php",
        "title" => _("Refonte"),
        "right" => /*DROIT*/"module_refonte",
        "class" => "refonte",
    ));
array_push($links,
    array(
        "href" => "../trt/module_archivage.php",
        "title" => _("Archivage"),
        "right" => /*DROIT*/"module_archivage",
        "class" => "archive",
    ));
array_push($links,
    array(
        "href" => "../trt/module_redecoupage.php",
        "title" => _("Redecoupage"),
        "right" => /*DROIT*/"module_redecoupage",
        "class" => "redecoupage",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"traitement_multi",
            /*DROIT*/"facturation_multi",
            /*DROIT*/"statistiques_multi"),
    ));
array_push($links,
    array(
        "href" => "../app/multi.traitement.php",
        "title" => _("Multi Collectivites"),
        "right" => /*DROIT*/"traitement_multi",
    ));
array_push($links,
    array(
        "href" => "../app/multi.facturation.php",
        "title" => _("Facturation"),
        "right" => /*DROIT*/"facturation_multi",
    ));
array_push($links,
    array(
        "href" => "../app/multi.statistiques.php",
        "title" => _("Statistiques"),
        "right" => /*DROIT*/"statistiques_multi",
    ));
$rubrik['links'] = $links;
array_push($menu, $rubrik);
// }}}


// {{{ Rubrique PARAMETRAGE
//
$rubrik = array(
    "title" => _("Parametrage"),
    "class" => "parametrage",
    "right" => /*DROIT*/"menu_parametrage",
);
//
$links = array();
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=collectivite",
        "title" => _("Collectivite"),
        "right" => /*DROIT*/"collectivite_tab",
        "class" => "collectivite",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=revision",
        "title" => _("Revisions"),
        "right" => /*DROIT*/"revision_tab",
        "class" => "revision",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=param_mouvement",
        "title" => _("Mouvements"),
        "right" => /*DROIT*/"param_mouvement_tab",
        "class" => "mouvement",
    ));
/*
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=xml_partenaire",
        "title" => _("Partenaires Insee Export Xml"),
        "right" => /*DROIT*/ /*"partenaire_tab",
    ));
*/
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=numerobureau",
        "title" => _("Numerotation Bureau"),
        "right" => /*DROIT*/"numerobureau_tab",
        "class" => "liste",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=numeroliste",
        "title" => _("Numerotation Liste"),
        "right" => /*DROIT*/"numeroliste_tab",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=liste",
        "title" => _("Liste"),
        "right" => /*DROIT*/"liste_tab",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"etat",
            /*DROIT*/"sousetat"),
    ));
array_push($links,
    array(
        "href" => "../app/txtab.php?obj=etat",
        "title" => _("Etat"),
        "right" => /*DROIT*/"etat",
        "class" => "etat",
    ));
array_push($links,
    array(
        "href" => "../app/txtab.php?obj=sousetat",
        "title" => _("Sous Etat"),
        "right" => /*DROIT*/"sousetat",
        "class" => "sousetat",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"utilisateur_tab",
            /*DROIT*/"profil_tab",
            /*DROIT*/"droit_tab"),
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=profil",
        "title" => _("Gestion de Profil"),
        "right" => /*DROIT*/"profil_tab",
        "class" => "profil",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=droit",
        "title" => _("Gestion Droits Acces"),
        "right" => /*DROIT*/"droit_tab",
        "class" => "droit",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=utilisateur",
        "title" => _("Gestion Utilisateur"),
        "right" => /*DROIT*/"utilisateur_tab",
        "class" => "utilisateur",
    ));
array_push($links,
    array(
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"directory"),
    ));
array_push($links,
    array(
        "href" => "../scr/directory.php",
        "title" => _("Annuaire"),
        "right" => /*DROIT*/"directory",
        "class" => "utilisateur",
    ));
$rubrik['links'] = $links;
array_push($menu, $rubrik);
// }}}

// {{{ Rubrique DECOUPAGE
//
$rubrik = array(
    "title" => _("Decoupage"),
    "right" => /*DROIT*/"menu_decoupage",
    "class" => "decoupage",
);
//
$links = array();
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=canton",
        "img" => "",
        "title" => _("Canton"),
        "right" => /*DROIT*/"canton_tab",
        "class" => "canton",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=bureau",
        "img" => "",
        "title" => _("Bureau"),
        "right" => /*DROIT*/"bureau_tab",
        "class" => "bureau",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=voie",
        "img" => "",
        "title" => _("Voie"),
        "right" => /*DROIT*/"voie_tab",
        "class" => "voie",
    ));
array_push($links,
    array(
        "href" => "",
        "img" => "",
        "title" => "<hr />",
        "right" => array(
            /*DROIT*/"commune_tab",
            /*DROIT*/"departement_tab",
            /*DROIT*/"nationalite_tab"),
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=commune",
        "img" => "",
        "title" => _("Commune"),
        "right" => /*DROIT*/"commune_tab",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=departement",
        "img" => "",
        "title" => _("Departement"),
        "right" => /*DROIT*/"departement_tab",
    ));
array_push($links,
    array(
        "href" => "../scr/tab.php?obj=nationalite",
        "img" => "",
        "title" => _("Nationalite"),
        "right" => /*DROIT*/"nationalite_tab",
    ));
$rubrik['links'] = $links;
array_push($menu, $rubrik);
// }}}

?>

<?php
/**
 * Affectation des paramètres spécifiques applications pour COMBO.PHP 1.1
 * Retour lorsqu il y a plusieurs enregistrements correlles
 * choix dans le SELECT
 *  $x=$row[$zoneCorrel];    // retour dans la zone correllée
 *  $y=$row[$zoneOrigine]    // retour dans la zone origine
 * il est possible de renvoyer d autre zone dans le formulaire
 * il est possible d afficher d autre zone de la table  en combo
 *
 * @package openelec
 * @version SVN : $Id$
 */

// =========================================================================
// FORMAT DE DATE
// =========================================================================
if(isset($row["date_naissance"])){
    if ($f -> formatdate=='JJ/MM/AAAA') {
        $affiche_date_naissance=$row['date_naissance'];
    } else {
        $affiche_date_naissance=substr($row["date_naissance"],8,2).'/'.
            substr($row["date_naissance"],5,2).'/'.substr($row["date_naissance"],0,4);
    }
}
if ($champOrigine=="libelle_voie")
   $aff=$aff." - ".$row["cp"]." ".$row["ville"]." ".$row["abrege"];  //*** 2.02

/**
 * Procurations et Centres de vote
 */
// mandant : form.php?obj=procuration - mandant
// IMPORTANT : La concaténation est vérifiée dans la classe
if ($champOrigine=="z_mandant") {
    //echo $retourUnique;
    $y = $y." - ".$row['prenom']." - ".$affiche_date_naissance." - ".
        $row["code_bureau"];
    $aff = $aff." - ".$row['prenom']." - ".$affiche_date_naissance." - "._("BUREAU")." ".
        $row["code_bureau"]." - "._("LISTE")." ".$row["liste"];
}
if ($champOrigine=="mandant") {
    // bug en retour
    $x=$x." - ".$row['prenom']." - ".$affiche_date_naissance." - "._("BUREAU")." ".
        $row["code_bureau"]." - "._("LISTE")." ".$row["liste"];
    $aff=$aff." - ".$row['prenom']." - ".$affiche_date_naissance." - "._("BUREAU")." ".
        $row["code_bureau"]." - "._("LISTE")." ".$row["liste"];
}
// mandataire : form.php?obj=procuration - mandataire
// IMPORTANT : La concaténation est vérifiée dans la classe
if ($champOrigine=="z_mandataire") {
    $y=$y." - ".$row['prenom']." - ". $affiche_date_naissance." - ".
        $row["code_bureau"];
    $aff=$aff." - ".$row['prenom']." - ".$affiche_date_naissance." - "._("BUREAU")." ".
        $row["code_bureau"]." - "._("LISTE")." ".$row["liste"];
}
if ($champOrigine=="mandataire") {
    //bug en retour
    $x=$x." - ".$row['prenom']." - ".$affiche_date_naissance." - "._("BUREAU")." ".
        $row["code_bureau"]." - "._("LISTE")." ".$row["liste"];
    $aff=$aff." - ".$row['prenom']." - ".$affiche_date_naissance." - "._("BUREAU")." ".
        $row["code_bureau"]." - "._("LISTE")." ".$row["liste"];
}
// electeur : form.php?obj=centrevote - electeur
// IMPORTANT : La concaténation est vérifiée dans la classe
if ($champOrigine=="z_electeur") {
    $y=$y." - ".$row['prenom']." - ". $affiche_date_naissance." - ".
        $row["code_bureau"];
    $aff=$aff." - ".$row['prenom']." - ".$affiche_date_naissance." - ".
        $row["code_bureau"];
}
if ($champOrigine=="id_electeur" or $champOrigine=="id_electeur_europe") {
    //bug en retour
    $x=$x." - ".$row['prenom']." - ".$affiche_date_naissance." - ".
        $row["code_bureau"];
    $aff=$aff." - ".$row['prenom']." - ".$affiche_date_naissance." - ".
        $row["code_bureau"];
}

?>

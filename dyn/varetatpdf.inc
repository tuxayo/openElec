<?php
/**
 * $Id$
 * Module de paramétrage des etats
 */
//
if (isset ($_GET['id']) && isset ($_GET['idx'])) {
    if ($_GET ['id'] == "mouvement") {
        $sql = str_replace (
        			iconv("UTF-8", HTTPCHARSET,"£idx"),
        			"id=".$_GET['idx'],
        			$sql);
    } elseif ($_GET ['id'] == "electeur") {
		$sql = str_replace (
					iconv("UTF-8", HTTPCHARSET,"£idx"),
					"id_electeur=".$_GET['idx'],
					$sql);
    } else {
    	$sql = str_replace (
					iconv("UTF-8", HTTPCHARSET,"£idx"),
					$_GET['idx'],
					$sql);
    }
}
$sql = str_replace (
                iconv("UTF-8", HTTPCHARSET,"£collectivite"),
                $_SESSION ['collectivite'],
                $sql);
//
$titre = str_replace (
				iconv("UTF-8", HTTPCHARSET,"£aujourdhui"),
				date('d/m/Y'),
				$titre);
$titre = str_replace (
				iconv("UTF-8", HTTPCHARSET,"£ville"),
				$ville,
				$titre);
$titre = str_replace (
				iconv("UTF-8", HTTPCHARSET,"£nom"),
				$nom,
				$titre);
$titre = str_replace (
				iconv("UTF-8", HTTPCHARSET,"£complement"),
				$complement,
				$titre);
if (isset ($_GET['idx'])) {
    $titre = str_replace (
    				iconv("UTF-8", HTTPCHARSET,"£idx"),
    				$_GET['idx'],
    				$titre);    
}

//
$corps = str_replace (
				iconv("UTF-8", HTTPCHARSET,"£aujourdhui"),
				date('d/m/Y'),
				$corps);
$corps = str_replace (
				iconv("UTF-8", HTTPCHARSET,"£ville"),
				$ville,
				$corps);
$corps = str_replace (
				iconv("UTF-8", HTTPCHARSET,"£nom"),
				$nom,
				$corps);
$corps = str_replace (
				iconv("UTF-8", HTTPCHARSET,"£complement"),
				$complement,
				$corps);
$corps = str_replace (
				iconv("UTF-8", HTTPCHARSET,"£dateTableau"),
				$dateTableau,
				$corps);
$corps = str_replace (
				iconv("UTF-8", HTTPCHARSET,"£anneeTableau"),
				$anneeTableau,
				$corps);
$corps = str_replace (
				iconv("UTF-8", HTTPCHARSET,"£liste"),
				$_SESSION ['liste']." ".$_SESSION ['libelle_liste'],
				$corps);
?>
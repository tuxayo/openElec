<?php
/**
 * affectation des paramètres specifiques applications pour COMBO.PHP 1.1
 * Retour lorsqu il y a qu un enregistrement correlle
 *
 * @package openelec
 * @version SVN : $Id$
 */


$x = $row[$zoneCorrel];
// dans la zone correllée
//$y=$row[$zoneOrigine]
// il est possible de renvoyer d autre zone dans le formulaire
// =========================================================================

// =========================================================================
// FORMAT DE DATE
// =========================================================================

//
$affiche_date_naissance = "";
//
if (isset($row['date_naissance'])) {
    //
    if (OM_DB_FORMATDATE == 'JJ/MM/AAAA') {
        //
        $affiche_date_naissance = $row['date_naissance'];
    } else {
        //
        $affiche_date_naissance = substr($row["date_naissance"],8,2)."/";
        $affiche_date_naissance .= substr($row["date_naissance"],5,2)."/";
        $affiche_date_naissance .= substr($row["date_naissance"],0,4);
    }
}

/**
 * Procurations et Centres de vote
 */
// mandant : form.php?obj=procuration - mandant
// IMPORTANT : La concaténation est vérifiée dans la classe
if ($champOrigine=="z_mandant")
    $y=$y." - ".$row["prenom"]." - ".$affiche_date_naissance." - ".
        $row["code_bureau"];
if ($champOrigine=="mandant")
    $x=$x." - ".$row["prenom"]." - ".$affiche_date_naissance." - ".
        $row["code_bureau"];
// mandataire : form.php?obj=procuration - mandataire
// IMPORTANT : La concaténation est vérifiée dans la classe
if ($champOrigine=="z_mandataire")
    $y=$y." - ".$row["prenom"]." - ".$affiche_date_naissance." - ".
        $row["code_bureau"];
if ($champOrigine=="mandataire")
    $x=$x." - ".$row["prenom"]." - ".$affiche_date_naissance." - ".
        $row["code_bureau"];

// electeur : form.php?obj=centrevote - electeur
// IMPORTANT : La concaténation est vérifiée dans la classe
if ($champOrigine=="z_electeur")
    $y=$y." - ".$row["prenom"]." - ".$affiche_date_naissance." - ".
        $row["code_bureau"];
if ($champOrigine=="id_electeur" or $champOrigine=="id_electeur_europe")
    $x=$x." - ".$row["prenom"]." - ".$affiche_date_naissance." - ".
        $row["code_bureau"];

?>

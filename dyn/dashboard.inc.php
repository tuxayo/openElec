<?php
/**
 * Ce fichier permet de surcharger le tableau de bord standard.
 *
 * @package openmairie_exemple
 * @version SVN: $Id$
 */

 require_once "../obj/utils.class.php";
 $f = new utils("nohtml", NULL, _("Tableau de bord"));

/**
 *
 */
require_once("../obj/workcollectivite.class.php");

// Important le code doit etre place ici pour que la nouvelle collectivite soit
// affichee correctement dans le header
$workcollectivite = new workCollectivite ($f, "../scr/dashboard.php");
$workcollectivite->validateForm();

//
$f->getCollectivite();

/**
 *
 */
require_once("../obj/workliste.class.php");

// Important le code doit etre place ici pour que la nouvelle liste soit
// affichee correctement dans le header
$workliste = new workListe ($f, "../scr/dashboard.php");
$workliste->validateForm();

//
$f->setFlag(NULL);
$f->display();

//
echo "<div id=\"dashboard\">\n";

// Ouverture de balise - Colonne 1
echo "<div class=\"col\">";

    /**
     * CHANGE COLLECTIVITE
     */
    if (count($workcollectivite->getCollectivites()) > 1
        and $f->isAccredited(/*DROIT*/'collectivitedefaut')) {
        //
        echo "<div class=\"boite ui-widget-content ui-corner-all\">";
        echo "<h4>"._("Collectivite de travail")."</h4>";
        echo "<span class=\"description\">";
        echo _("Selectionner ici la collectivite sur laquelle vous souhaitez travailler.");
        echo "</span>";
        $workcollectivite->showForm (false);
        echo "</div>";
    }

    /**
     * CHANGE LISTE
     */
    if (count($workliste->getListes()) > 1
        and $f->isAccredited(/*DROIT*/'listedefaut')) {
        //
        echo "<div class=\"boite ui-widget-content ui-corner-all\">";
        echo "<h4>"._("Liste de travail")."</h4>";
        echo "<span class=\"description\">";
        echo _("Selectionner ici la liste sur laquelle vous souhaitez travailler.");
        echo "</span>";
        $workliste->showForm (false);
        echo "</div>";
    }

    /**
     * SAISIE
     */
    if ($f->isAccredited(array(/*DROIT*/"inscription",
                               /*DROIT*/"electeur_modification",
                               /*DROIT*/"electeur_radiation",
                               /*DROIT*/"electeur_mouvement",
                               /*DROIT*/"procuration",
                               /*DROIT*/"centrevote",
                               /*DROIT*/"mairieeurope"),
                         "OR")) {
        //
        $counter = 0;
        //
        echo "<div class=\"boite ui-widget-content ui-corner-all\">";
        echo "<h4>"._("Saisie Electeur")."</h4>";
        //
        echo "<ul class=\"menu-friendly\">";
        //
        if ($f->isAccredited(/*DROIT*/"inscription")) {
            echo "<li class=\"ui-corner-all col".$counter++."\">";
            echo "<a href=\"../app/inscription.search.php\">";
            echo "<span class=\"link\">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix mvt-inscription-25\"><!-- --></span>";
            echo " "._("Inscription");
            echo "</span>";
            echo "<span class=\"description\">";
            echo _("Effectuer une recherche de doublon avant d'acceder au formulaire d'inscription d'un electeur.");
            echo "</span>";
            echo "</a>";
            echo "</li>";
        }
        //
        if ($f->isAccredited(/*DROIT*/"electeur_modification")) {
            echo "<li class=\"ui-corner-all col".$counter++."\">";
            echo "<a href=\"../app/electeur.search.php?obj=electeur_modification\">";
            echo "<span class=\"link\">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix mvt-modification-25\"><!-- --></span>";
            echo " "._("Modification");
            echo "</span>";
            echo "<span class=\"description\">";
            echo _("Rechercher sur la liste electorale un electeur a modifier (changement d'adresse, correction etat civil, ...).");
            echo "</span>";
            echo "</a>";
            echo "</li>";
        }
        //
        if ($f->isAccredited(/*DROIT*/"electeur_radiation")) {
            echo "<li class=\"ui-corner-all col".$counter++."\">";
            echo "<a href=\"../app/electeur.search.php?obj=electeur_radiation\">";
            echo "<span class=\"link\">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix mvt-radiation-25\"><!-- --></span>";
            echo " "._("Radiation");
            echo "</span>";
            echo "<span class=\"description\">";
            echo _("Rechercher sur la liste electorale un electeur a radier (depart de la commune, deces, ...).");
            echo "</span>";
            echo "</a>";
            echo "</li>";
        }
        //
        if ($f->isAccredited(/*DROIT*/"electeur_mouvement")) {
            echo "<li class=\"ui-corner-all col".$counter++."\">";
            echo "<a href=\"../app/electeur.search.php?obj=electeur_mouvement\">";
            echo "<span class=\"link\">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix mvt-modification-radiation-25\"><!-- --></span>";
            echo " "._("Modification")." / "._("Radiation");
            echo "</span>";
            echo "<span class=\"description\">";
            echo _("Rechercher sur la liste electorale un electeur a modifier ou a radier (changement d'adresse, correction etat civil, depart de la commune, deces, ...).");
            echo "</span>";
            echo "</a>";
            echo "</li>";
        }
        if ($f->isAccredited(/*DROIT*/"procuration")) {
            echo "<li class=\"ui-corner-all col".$counter++."\">";
            echo "<a href=\"../scr/form.php?obj=procuration\">";
            echo "<span class=\"link\">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix procuration-25\"><!-- --></span>";
            echo " "._("Procuration");
            echo "</span>";
            echo "<span class=\"description\">";
            echo _("Un mandant donne procuration a un mandataire sur une periode donnee.");
            echo "</span>";
            echo "</a>";
            echo "</li>";
        }
        //
        if ($f->isAccredited(/*DROIT*/"centrevote")) {
            echo "<li class=\"ui-corner-all col".$counter++."\">";
            echo "<a href=\"../scr/form.php?obj=centrevote\">";
            echo "<span class=\"link\">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix centrevote-25\"><!-- --></span>";
            echo " "._("Centre de vote");
            echo "</span>";
            echo "<span class=\"description\">";
            echo _("Inscrire un electeur de la commune qui reside a l'etranger comme votant pour les scrutins dont la loi electorale prevoit qu'ils se deroulent en partie a l'etranger.");
            echo "</span>";
            echo "</a>";
            echo "</li>";
        }
        //
        if ($f->isAccredited(/*DROIT*/"mairieeurope")) {
            echo "<li class=\"ui-corner-all col".$counter++."\">";
            echo "<a href=\"../scr/form.php?obj=mairieeurope\">";
            echo "<span class=\"link\">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix mairieeurope-25\"><!-- --></span>";
            echo " "._("Mairie Europe");
            echo "</span>";
            echo "<span class=\"description\">";
            echo _("Inscrire un electeur de la commune qui reside a l'etranger comme votant pour les europeennes depuis son pays de residence.");
            echo "</span>";
            echo "</a>";
            echo "</li>";
        }
        //
        echo "</ul>";
        //
        echo "<div class=\"both\"></div>";
        echo "</div>";
    }

// Fermeture de balise - Colonne 1
echo "</div>";

// Ouverture de balise - Colonne 2
echo "<div class=\"col\">";

    /**
     * DATE DE TABLEAU
     */
    if ($f->isAccredited(/*DROIT*/"widget_datetableau")) {
        echo "<div class=\"boite ui-widget-content ui-corner-all\">";
        echo "<h4>"._("Date de tableau")."</h4>";
        if (time() > strtotime($f->collectivite['datetableau'])) {
            $f->displayMessage("error", _("La date de tableau est dans le passe. Attention de ne pas oublier de la changer."));
        }
        //
        $date_tableau_valide = true;
        //
        /**
         *
         */
        require_once "../obj/revisionelectorale.class.php";
        $revision = new revisionelectorale($f);
        $date_tableau_valide = $revision->is_date_tableau_valid();
        //
        if ($date_tableau_valide == false) {
            $f->displayMessage("error", _("La date de tableau n'est pas valide. Merci de la corriger."));
        }
        
        echo "<p class=\"datetableau\">";
        $accredited = $f->isAccredited(/*DROIT*/"collectivite");
        if ($accredited) {
            echo "<a href=\"../app/changedatetableau.php\" title=\""._("Cliquer ici pour mettre a jour la date de tableau")."\">";
        }
        echo "<img src=\"../img/calendar.png\" alt=\"\" title=\"\" />";
        echo " ".$f->formatdate ($f->collectivite['datetableau']);
        if ($accredited) {
            echo "</a>";
        }
        echo "</p>";
        echo "</div>";
    }
    
    /**
     * RECHERCHE GLOBALE
     */
    if ($f->isAccredited(/*DROIT*/"search_tab")) {
        echo "<div class=\"boite ui-widget-content ui-corner-all\">";
        echo "
        <script type='text/javascript'>
            function giveFocus(obj){
                document.getElementById(obj).select();
            }
            setTimeout(\"giveFocus('dashboard_recherche');\",500);
        </script>
        ";
        echo "<h4>"._("Recherche dans la liste electorale")."</h4>";
        echo "<span class=\"description\">";
        echo _("Saisir votre recherche");
        echo "</span>";
        echo "<div id=\"rechercheglobale\">\n";
        echo "<form name=\"f1\" id=\"f1\" method=\"post\" ";
        echo "action=\"../scr/tab.php?validation=0&amp;obj=consult_electeur\">";
		echo "<input type=\"text\" id=\"dashboard_recherche\" class=\"champFormulaire\" value=\"\" name=\"recherche\" />";
		echo "<input type=\"submit\" name=\"s1\" value=\""._("Recherche")."\" />";
        echo "</form>";
        echo "</div>\n";
        echo "<div class=\"both\"></div>";
        echo "</div>\n";
    }

    /**
     * SAISIE
     */
    if ($f->isAccredited(array(/*DROIT*/"module_traitement_j5",
                               /*DROIT*/"module_traitement_annuel",
                               /*DROIT*/"module_commission"),
                         "OR")) {
        //
        $counter = 0;
        //
        echo "<div class=\"boite ui-widget-content ui-corner-all\">";
        echo "<h4>"._("Traitements")."</h4>";
        //
        echo "<ul class=\"menu-friendly\">";
        //
        if ($f->isAccredited(/*DROIT*/"module_commission")) {
            echo "<li class=\"ui-corner-all col".$counter++."\">";
            echo "<a href=\"../trt/module_commission.php\">";
            echo "<span class=\"link\">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix traitement-25\"><!-- --></span>";
            echo " "._("Commission");
            echo "</span>";
            echo "<span class=\"description\">";
            echo _("Tableau de bord destine a la preparation et la validation des commissions.");
            echo "</span>";
            echo "</a>";
            echo "</li>";
        }
        //
        if ($f->isAccredited(/*DROIT*/"module_traitement_j5")) {
            echo "<li class=\"ui-corner-all col".$counter++."\">";
            echo "<a href=\"../trt/module_traitement_j5.php\">";
            echo "<span class=\"link\">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix traitement-25\"><!-- --></span>";
            echo " "._("5 jours");
            echo "</span>";
            echo "<span class=\"description\">";
            echo _("Tableau de bord destine aux traitements des 5 jours.");
            echo "</span>";
            echo "</a>";
            echo "</li>";
        }
        //
        if ($f->isAccredited(/*DROIT*/"module_traitement_annuel")) {
            echo "<li class=\"ui-corner-all col".$counter++."\">";
            echo "<a href=\"../trt/module_traitement_annuel.php\">";
            echo "<span class=\"link\">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix traitement-25\"><!-- --></span>";
            echo " "._("Annuel");
            echo "</span>";
            echo "<span class=\"description\">";
            echo _("Tableau de bord destine a la preparation et la validation des traitements annuels du 10 janvier et du 28 fevrier.");
            echo "</span>";
            echo "</a>";
            echo "</li>";
        }
        //
        echo "</ul>";
        //
        echo "<div class=\"both\"></div>";
        echo "</div>";
    }

// Fermeture de balise - Colonne 2
echo "</div>";

//
echo "<div class=\"both\"></div>";

//
echo "</div>\n";

/**
 * Il suffit de decommenter la ligne suivante pour ne pas utiliser les widgets
 * de base. 
 */
die();

?>

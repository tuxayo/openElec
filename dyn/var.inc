<?php
/**
 * $Id$
 *
 */

/**
 * IMPORTANT : _notice_ 
 **/
if (!isset ($_SESSION ['profil']))
    $_SESSION ['profil'] = "0";
/**
 * IMPORTANT : permettre à INTERNET EXPLORER 6 de lire un pdf en https
 **/
if (isset ($_SERVER ['PATH_TRANSLATED']))
    if (preg_match ("#/pdf/#", $_SERVER ['PATH_TRANSLATED']))
        header("Pragma:");

/**
 * Dossiers temporaires
 **/
$pdfdir = "../pdfic/";
$tmpdir = "../tmp/";
$trsdir = "../trs/";

/**
 * Styles pour les formulaires
 * ToDo : passer ces styles dans le css (modif des deux classes formualire et dbform)
 **/
$style_tablefieldset = ""; //"font-size:11px;background-color:transparent;color:#434343;width:100%";
$style_legendfieldset = ""; //"color:darkblue;font-weight:bold;font-size:12px";
$style_fieldset = "";//"background-color:#F3F7FE;color:darkblue";
$styleBouton = ""; //"font-size:10px;font-weight:bold;height:25px;background-color:#0539AE;color:#ffffff";

/**
 * Code du type de mouvement obligeant la saisie d'une commune de provenance
 **/
$param_mouvement_venantautrecommune = "CC";

/**
 * Code du type de mouvement pour les inscriptions d'office
 **/
$param_mouvement_inscriptionoffice = "IO";

/**
 * Export INSEE - Séparateur de ligne
 **/
$newline = "\n";

/**
 * Affichage des codes  barres
 *  - true : affichage
 *  - false : pas d'affichage
 **/
$code_barre = false;

/**
 * Double colonnes sur l'émargement
 *  - true : oui
 *  - false : colonne unique
 **/
$double_emargement = false;

/* pdf_commission.inc : Etat a la commission de fin d année
si true = que les modifications de changement de bureaux [arles]
sinon : toutes les modifications
*/
$sans_modification=true;

/**
 *
 */
$demo = false;

/**
 * OPTION : Lien electeur voie
 *  - true (defaut)
 *  - false
 **/
$module_voie = true;

/**
 * Edition Facturation 
 * Variables
 **/
$fact_gestionnaire = "A304";
$fact_attributaire = "A304";
$fact_chapitre = "70/7087801";
$fact_electeur_unitaire='.28';
$fact_forfait='30.49';

/**
 * OPTION - La liste d'émargement peut être dressée par ordre des numéros
 * d'inscription ou par ordre alphabétique des électeurs, au choix de la mairie
 * 
 * $option_tri_liste_emargement = "alpha"; => Ordre alphabétique des électeurs
 * $option_tri_liste_emargement = "numero"; => Ordre des numéros d'inscription
 * 
 * default : $option_tri_liste_emargement = "alpha";
 */
$option_tri_liste_emargement = "alpha";

/**
 * OPTION - Possibilite de realiser la generation des editions au format zip
 * ou fichier par fichier. Cette option n'est necessaire que pour le mode
 * MULTI.
 * 
 * $option_generation_as_an_archive = false; => fichiers pdf separes
 * $option_generation_as_an_archive = true; => fichiers pdf dans une archive zip
 *
 * default : $option_genertion_as_an_archive = true;
 */
$option_generation_as_an_archive = true;

/**
 * OPTION - Possibilite de choisir si les codes voie sont automatiques ou non.
 * 
 * $option_code_voie_auto = false; => les codes voies ne sont pas automatiques
 * $option_code_voie_auto = true; => les codes voie sont automatiques
 *
 * default : $option_code_voie_auto = true;
 */
$option_code_voie_auto = true;

/**
 * OPTION - Possibilite de choisir si les courriers de refus de mouvement sont
 * dispo dans l'appli.
 * 
 * $option_refus_mouvement = false; => refus de mouvement sont pas dispo
 * $option_refus_mouvement = true; => refus de mouvement sont dispo
 *
 * default : $option_refus_mouvement = false;
 */
$option_refus_mouvement = true;

/**
 * OPTION - Possibilite de choisir si les procuration peuvent êtres
 * refusé ou non.
 * 
 * default : $option_refus_procuration = true;
 */
//$option_refus_procuration = false;

/**
 * PARAMETRAGE DES ETIQUETTES - Ce parametrage concerne toutes les etiquettes
 * produites par le logiciel (contrainte obligatoire : format A4)
 */
$parametrage_etiquettes = array(
    "etiquette_bordure_etiquette" => 0,
    "etiquette_bordure_texte" => 0,
    "planche_marge_ext_haut" => 1,
    "planche_marge_ext_gauche" => 0,
    "planche_nb_colonnes" => 2,
    "planche_nb_lignes" => 7,
    "etiquette_largeur" => 105,
    "etiquette_hauteur" => 42, 
    "etiquette_marge_int_haut" => 8,
    "etiquette_marge_int_gauche" => 8,
    "etiquette_marge_int_droite" => 8,
    "etiquette_espace_entre_colonnes" => 0,
    "etiquette_espace_entre_lignes" => -1, // -1 correspond à 0
    "etiquette_hauteur_de_ligne_du_texte" => 4,
    "etiquette_taille_du_texte" => 10,
);

?>

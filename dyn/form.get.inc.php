<?php
/**
 *
 */

// Identifiant de l'objet metier du formulaire et mode d'ajout
if (isset($_GET['idx']) and $_GET['idx'] != "") {
    $idx = $_GET['idx'];
    (isset($_GET['ids']) ? $maj = 2 : $maj = 1);
} else {
    $maj = 0;
    $idx = "]";
}
// Flag de validation du formulaire
(isset($_GET['validation']) ? $validation = $_GET['validation'] : $validation = 0);
// Libelle de l'enregistement du formulaire
(isset($_GET['idz']) ? $idz = $_GET['idz'] : $idz = "");
// Premier enregistrement a afficher sur le tableau de la page precedente (tab.php?premier=)
(isset($_GET['premier']) ? $premier = $_GET['premier'] : $premier = 0);
// Colonne choisie pour le tri sur le tableau de la page precedente (tab.php?tricol=)
(isset($_GET['tricol']) ? $tricol = $_GET['tricol'] : $tricol = "");
// Colonne choisie pour la selection sur le tableau de la page precedente (tab.php?selectioncol=)
(isset($_GET['selectioncol']) ? $selectioncol = $_GET['selectioncol'] : $selectioncol = "");
// Chaine recherchee
if (isset($_GET['recherche'])) {
    $recherche = $_GET['recherche'];
    if (get_magic_quotes_gpc()) {
        $recherche1 = StripSlashes($recherche);
    } else {
        $recherche1 = $recherche;
    }
} else {
    $recherche = "";
    $recherche1 = "";
}
//
$extra_parameters = array();

if (file_exists("../dyn/form.get.specific.inc.php")) {
    //
    require "../dyn/form.get.specific.inc.php";
}

?>
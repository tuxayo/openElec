<?php
/**
 * Ce fichier permet de configurer les connexions des annuaires LDAP
 *
 * @package openmairie_exemple
 * @version SVN: $Id$
 */

/**
 * 
 */
$directory = array();

/**
 * Informations sur $directory
 *
 *  La variable $directory est un tableau associatif. Ce tableau peut, de ce
 *  fait, contenir plusieurs configurations d'annuaires LDAP différentes.
 *  Chaque connexion est représentée par une clef de tableau.
 *
 *  Ces clefs se retrouvent dans le fichier database.inc.php et permettent
 *  d'associer une base de données précise à un annuaire LDAP precis.
 *
 *  Les autres clefs de configuration :
 *
 *       ldap_server      -> Adresse du serveur LDAP
 *       ldap_server_port -> Port d'ecoute du serveur LDAP
 *       
 *       ldap_admin_login  -> identifiant de l'administrateur LDAP
 *       ldap_admin_passwd -> mot de passe de cet administrateur
 *
 *       ldap_base       -> Base de l'arbre LDAP
 *       ldap_base_users -> Base utilisateurs de l'arbre LDAP
 *
 *       ldap_user_filter  -> Filtre utiliser par la fonction ldap_search
 *       ldap_login_attrib -> Attribut LDAP qui sera utilisé comme login dans la base
 *       
 */

$directory["ldap-default"] = array(
    'ldap_server' => 'localhost',
    'ldap_server_port' => '389',
    'ldap_admin_login' => 'cn=admin,dc=openmairie,dc=org',
    'ldap_admin_passwd' => 'admin',
    'ldap_base' => 'dc=openmairie,dc=org',
    'ldap_base_users' => 'dc=openmairie,dc=org',
    'ldap_user_filter' => 'objectclass=person',
    'ldap_login_attrib' => 'cn',
);

?>

<?php
/**
 * Ce fichier permet de configurer divers parametres de l'application
 *
 * @package openmairie_exemple
 * @version SVN: $Id: config.inc.php 330 2010-12-07 09:00:21Z fmichon $
 */

/**
 * 
 */
$config = array();

//
$config['application'] = _("openElec");

//
$config['title'] = ":: "._("openMairie")." :: "._("openElec - Gestion des listes electorales");

//
$config['session_name'] = "openelec";

/**
 * Mode demonstration de l'application
 * Permet de pre-remplir le formulaire de login avec l'identifiant 'demo' et le 
 * mot de passe 'demo'
 * Default : $config['demo'] = false;
 */
$config['demo'] = false;

/**
 * Configuration des extensions autorisees dans le module upload.php
 * Pour ajouter votre configuration, decommenter la ligne et modifier les extensions
 * avec des ; comme separateur
 * Default : $config['upload_extension'] = ".gif;.jpg;.jpeg;.png;.txt;.pdf;.csv;"
 */
$config['upload_extension'] = ".png;.pdf;.txt;.xml";

/**
 * Activation de la redefinition du mot de passe
 * Permet de redefinir son mot de passe en cas d'oubli via un lien sur le formulaire
 * de login (Attention un serveur de mail doit etre configure)
 * Default : $config['password_reset'] = false;
 */
//$config['password_reset'] = false;

?>

<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"statistiques", _("Statistiques"));

// Recuperation des donnees
$data = array();

// Identifiant de la statistique
$inc = "stats_archivage_bureau";

//
if (!file_exists("../sql/".$f->phptype."/".$inc.".inc")) {
    $f->notExistsError();
}

//
$totalavant = 0;
$totalactif = 0;
$totaltrs = 0;
$totalcnen = 0;
$totalapres = 0;

//
include "../sql/".$f->phptype."/".$inc.".inc";

//
$res_select_bureau = $f->db->query($query_select_bureau);
$f->isDatabaseError($res_select_bureau);

//
$numArray = 0;
while ($row_select_bureau =& $res_select_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
    //
    include "../sql/".$f->phptype."/".$inc.".inc";
    //
    $res_count_mouvement = $f->db->getOne($query_count_mouvement);
    $f->isDatabaseError($res_count_mouvement);
    //
    $totalavant += $res_count_mouvement;
    //
    $res_count_mouvement_actif = $f->db->getOne($query_count_mouvement_actif);
    $f->isDatabaseError($res_count_mouvement_actif);
    //
    $totalactif += $res_count_mouvement_actif;
    //
    $res_count_mouvement_trs = $f->db->getOne($query_count_mouvement_trs);
    $f->isDatabaseError($res_count_mouvement_trs);
    //
    $totaltrs += $res_count_mouvement_trs;
    //
    $res_count_mouvement_trs_cnen = $f->db->getOne($query_count_mouvement_trs_cnen);
    $f->isDatabaseError($res_count_mouvement_trs_cnen);
    //
    $totalcnen += $res_count_mouvement_trs_cnen;
    //
    $apres = $res_count_mouvement - $res_count_mouvement_trs;
    //
    $totalapres += $apres;
    //
    $datas = array(
                   $row_select_bureau['code'],
                   $row_select_bureau['libelle_bureau'],
                   $res_count_mouvement,
                   $res_count_mouvement_actif,
                   $res_count_mouvement_trs,
                   $res_count_mouvement_trs_cnen,
                   $apres
                   );
    $data[$numArray] = $datas;
    $numArray++;
}
//
$data[$numArray] = array(
                        "-",
                        _("TOTAL"),
                        $totalavant,
                        $totalactif,
                        $totaltrs,
                        $totalcnen,
                        $totalapres
                        );
// Array
$tableau = array(
    "format" => "L",
    "title" => _("Statistiques"),
    "subtitle" => _("Statistiques des mouvements"),
    "offset" => array(10,0,30,30,30,0,30),
    "column" => array("",
                      _("LISTE")." : ".$_SESSION ['liste']." ".$_SESSION ['libelle_liste']."",
                      _("Avant archivage"),
                      _("Mouvements actifs"),
                      _("Mouvements traites"),
                      _("Mouvements traites transmis a l'INSEE"),
                      _("Apres archivage")
                      ),
    "data" => $data,
    "output" => "stats-archivagebureau"
    );

?>

<?php
/**
 * Script permettant de récupérer le fichier le fichier de mec généré
 * @package openelec
 * @version SVN : $Id$
 */
require_once("../obj/utils.class.php");

// new utils
$f = new utils ("nohtml", "traitement_insee_export");

//
$aujourdhui = date("d/m/Y");

// Communes

require_once("../obj/traitement.mec_insee_export.class.php");

$traitementMec = new mecInseeExportTraitement($f);
$nom_fichier = $traitementMec->treatment();

/**
 *
 */
//
$display = "";
if (isset($_GET['display'])) {
    $display = $_GET['display'];
}
//
if ($display == "list_content") {
    displayListContent($f);
    die();
}

/**
 * Declaration d'une fonction permettant de lister les dix derniers exports
 * INSEE realises. La fonction est necessaire puisque ce code et uniquement
 * celui ci va etre appele en ajax pour recharger la liste des exports au
 * moment de la validation des traitements.
 */
function displayListContent($f) {
    //
    $tab = array();
    //
    $dossier = opendir($f->getParameter("chemin_cnen"));
    //
    while ($entree = readdir($dossier)) {
        if (preg_match( "/tedeco/i" , $entree ) OR preg_match( "/mecinsee/i" , $entree )) {
            $exportNumber = "";
            $timing = preg_split("/_/", $entree);
            
            $timingDate = substr($timing[2], 4, 2).substr($timing[2], 2, 2).substr($timing[2], 0, 2);
            if (count($timing) == 5) {
                $timingHour = $timing[3];
                $exportNumber = intval(str_replace(".txt","", $timing[4]));
            } elseif (count($timing) == 4) {
                $timingHour = str_replace(".txt","", $timing[3]);
            }
            $timing = $timingDate.str_pad($timingHour,6,0, STR_PAD_LEFT);
            //
            $date = date("d/m/Y H:i:s", mktime(substr($timing, 6, 2), // [ int $hour = date("H") [,
                                       substr($timing, 8, 2), // int $minute = date("i") [,
                                       substr($timing, 10, 2), // int $second = date("s") [,
                                       substr($timing, 2, 2), // int $month = date("n") [,
                                       substr($timing, 4, 2), // int $day = date("j") [,
                                       substr($timing, 0, 2) // int $year = date("Y") [,
                                       ));
            //
            if (preg_match( "/tedeco/i" , $entree )) {
                $title = _("Export du")." ".$date." [".$exportNumber."]";
            } elseif (preg_match( "/mecinsee/i" , $entree )) {
                $title = _("Export Mec du")." ".$date." [".$exportNumber."]";
            }
            
            array_push($tab,
                array ("date" => $timing,
                       "folder" => 'tmp',
                       "file" => $entree,
                       "ext" => array_pop(explode(".", $entree)),
                       "title" => $title)
                       );
        }
    }
    //
    closedir($dossier);
    arsort($tab);
    //
    $links = array();
    //
    foreach ($tab as $key => $elem) {
        array_push($links, array(
            "href" => "../app/file.php?fic=".$elem['file']."&amp;folder=".$elem['folder'],
            "target" => "_blank",
            "class" => "om-prev-icon file-16",
            "title" => $elem['title'],));
        if (count($links) == 10) {
            break;
        }
    }
    //
    $f->displayLinksAsList($links);
}

?>
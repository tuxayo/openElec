<?php
/**
 * Ce fichier permet de gerer le module Centre de Vote. Il affiche
 * differents onglets contenant chacun un traitement en relation avec
 * les centres de vote.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"module_centrevote",
               _("Traitement")." -> "._("Module Centre de vote"));

/**
 * Affichage de la structure de la page
 */
// Affichage
$f->setFlag(NULL);
$f->display();

/**
 * ONGLETS
 */
// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n";
// Affichage de la liste des onglets
echo "\n<ul>\n";
//
if ($f->isAccredited(/*DROIT*/"traitement_centrevote")) {
    echo "\t<li><a href=\"../trt/centrevote.php\">"._("Mise a jour")."</a></li>\n";
}
//
echo "</ul>\n";
// Fermeture de la balise - Conteneur d'onglets
echo "</div>";

?>

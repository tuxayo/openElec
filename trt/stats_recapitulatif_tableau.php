<?php
/**
 * Statistique recapitulatif tableau
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";


//
$datetableau = "";
if (isset($_GET["tableau"])) {
    $datetableau = $_GET["tableau"];
};

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"recapitulatif_tableau",
                               _("Recapitulatif du tableau rectificatif du")." ".$f->formatDate($datetableau));

// Recuperation des donnees
$data = array();

// STATISTIQUES : 
$inc = "stats_recapitulatif_tableau";

/**
 *
 */
require "../obj/revisionelectorale.class.php";
$revision = new revisionelectorale($f);
$datetableau_precedent = $revision->get_previous_date_tableau($datetableau);

//
function calculNombreDelecteursDateTableau($date_tableau_reference, $bureau, $sexe = "ALL") {
    //
    global $electeurs_actuels_groupby_bureau;
    global $inscriptions_radiations_stats_groupby_bureau;
    global $transferts_plus_stats_groupby_bureau;
    global $transferts_moins_stats_groupby_bureau;
    //
    if ($sexe == "ALL") {
        $cpt_electeur = $electeurs_actuels_groupby_bureau[$bureau]["total"];
    } else {
        $cpt_electeur = $electeurs_actuels_groupby_bureau[$bureau][$sexe];
    }
    //
    if (isset($inscriptions_radiations_stats_groupby_bureau[$bureau])) {
    foreach ($inscriptions_radiations_stats_groupby_bureau[$bureau] as $element) {
        if ($sexe == "ALL" || $element["sexe"] == $sexe) {
            //
            if ($element["date_tableau"] > $date_tableau_reference) {
                if ($element["etat"] == "trs") {
                    if ($element["typecat"] == "Inscription") {
                        $cpt_electeur -= $element["total"];
                    } elseif ($element["typecat"] == "Radiation") {
                        $cpt_electeur += $element["total"];
                    }
                }
            } else {
                if ($element["etat"] == "actif") {
                    if ($element["typecat"] == "Inscription") {
                        $cpt_electeur += $element["total"];
                    } elseif ($element["typecat"] == "Radiation") {
                        $cpt_electeur -= $element["total"];
                    }
                }
            }
        }
    }
    }
    //
    if (isset($transferts_plus_stats_groupby_bureau[$bureau])) {
    foreach ($transferts_plus_stats_groupby_bureau[$bureau] as $element) {
        if ($sexe == "ALL" || $element["sexe"] == $sexe) {
            //
            if ($element["date_tableau"] > $date_tableau_reference) {
                if ($element["etat"] == "trs") {
                    $cpt_electeur -= $element["total"];
                }
            } else {
                if ($element["etat"] == "actif") {
                    $cpt_electeur += $element["total"];
                }
            }
        }
    }
    }
    //
    if (isset($transferts_moins_stats_groupby_bureau[$bureau])) {
    foreach ($transferts_moins_stats_groupby_bureau[$bureau] as $element) {
        if ($sexe == "ALL" || $element["sexe"] == $sexe) {
            //
            if ($element["date_tableau"] > $date_tableau_reference) {
                if ($element["etat"] == "trs") {
                        $cpt_electeur += $element["total"];
                }
            } else {
                if ($element["etat"] == "actif") {
                    $cpt_electeur -= $element["total"];
                }
            }
        }
    }
    }
    return $cpt_electeur;
}

//
include "../sql/".OM_DB_PHPTYPE."/".$inc.".inc";

//
$res = $f->db->query($sql_bureau);
$f->addToLog("trt/stats_recapitulatif_tableau.php: db->query(\"".$sql_bureau."\")", VERBOSE_MODE);
$f->isDatabaseError($res);

// Nombre actuel d'electeurs dans la base de donnees par bureau
// array("<BUREAU>" => array("M" => <>, "F" => <>, "total" => <>),);
$electeurs_actuels_groupby_bureau = array();
$res_electeurs_actuels_groupby_bureau = $f->db->query($query_electeurs_actuels_groupby_bureau);
$f->addToLog("trt/stats_recapitulatif_tableau.php: db->query(\"".$query_electeurs_actuels_groupby_bureau."\")", VERBOSE_MODE);
$f->isDatabaseError($res_electeurs_actuels_groupby_bureau);
while ($row_electeurs_actuels_groupby_bureau =& $res_electeurs_actuels_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
    //
    if (!isset($electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]])) {
        $electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]] = array();
    }
    //
    $electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]][$row_electeurs_actuels_groupby_bureau["sexe"]] = $row_electeurs_actuels_groupby_bureau["total"];
}
foreach ($electeurs_actuels_groupby_bureau as $key => $value) {
    $electeurs_actuels_groupby_bureau[$key]["total"] = (isset($electeurs_actuels_groupby_bureau[$key]["M"]) ? $electeurs_actuels_groupby_bureau[$key]["M"] : 0) + (isset($electeurs_actuels_groupby_bureau[$key]["F"]) ? $electeurs_actuels_groupby_bureau[$key]["F"] : 0);
}

//
$inscriptions_radiations_stats_groupby_bureau = array();
$res_inscriptions_radiations_stats_groupby_bureau = $f->db->query($query_inscriptions_radiations_stats_groupby_bureau);
$f->addToLog("trt/stats_recapitulatif_tableau.php: db->query(\"".$query_inscriptions_radiations_stats_groupby_bureau."\")", VERBOSE_MODE);
$f->isDatabaseError($res_inscriptions_radiations_stats_groupby_bureau);
while ($row_inscriptions_radiations_stats_groupby_bureau =& $res_inscriptions_radiations_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
    //
    if (!isset($inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]])) {
        $inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]] = array();
    }
    //
    array_push($inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]], $row_inscriptions_radiations_stats_groupby_bureau);
}
$f->addToLog("trt/stats_recapitulatif_tableau.php: ".print_r($inscriptions_radiations_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);

//
$transferts_plus_stats_groupby_bureau = array();
$res_transferts_plus_stats_groupby_bureau = $f->db->query($query_transferts_plus_stats_groupby_bureau);
$f->addToLog("trt/stats_recapitulatif_tableau.php: db->query(\"".$query_transferts_plus_stats_groupby_bureau."\")", VERBOSE_MODE);
$f->isDatabaseError($res_transferts_plus_stats_groupby_bureau);
while ($row_transferts_plus_stats_groupby_bureau =& $res_transferts_plus_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
    //
    if (!isset($transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]])) {
        $transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]] = array();
    }
    //
    array_push($transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]], $row_transferts_plus_stats_groupby_bureau);
}
$f->addToLog("trt/stats_recapitulatif_tableau.php: ".print_r($transferts_plus_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);

//
$transferts_moins_stats_groupby_bureau = array();
$res_transferts_moins_stats_groupby_bureau = $f->db->query($query_transferts_moins_stats_groupby_bureau);
$f->addToLog("trt/stats_recapitulatif_tableau.php: db->query(\"".$query_transferts_moins_stats_groupby_bureau."\")", VERBOSE_MODE);
$f->isDatabaseError($res_transferts_moins_stats_groupby_bureau);
while ($row_transferts_moins_stats_groupby_bureau =& $res_transferts_moins_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
    //
    if (!isset($transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]])) {
        $transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]] = array();
    }
    //
    array_push($transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]], $row_transferts_moins_stats_groupby_bureau);
}
$f->addToLog("trt/stats_recapitulatif_tableau.php: ".print_r($transferts_moins_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);

//
$inscriptions_radiations_groupby_bureau = array();
$res_inscriptions_radiations_groupby_bureau = $f->db->query($query_inscriptions_radiations_groupby_bureau);
$f->addToLog("trt/stats_recapitulatif_tableau.php: db->query(\"".$query_inscriptions_radiations_groupby_bureau."\")", VERBOSE_MODE);
$f->isDatabaseError($res_inscriptions_radiations_groupby_bureau);
while ($row_inscriptions_radiations_groupby_bureau =& $res_inscriptions_radiations_groupby_bureau->fetchrow(DB_FETCHMODE_ASSOC)) {
    //
    if (!isset($inscriptions_radiations_groupby_bureau[$row_inscriptions_radiations_groupby_bureau["bureau"]])) {
        $inscriptions_radiations_groupby_bureau[$row_inscriptions_radiations_groupby_bureau["bureau"]] = array();
    }
    //
    $inscriptions_radiations_groupby_bureau[$row_inscriptions_radiations_groupby_bureau["bureau"]][$row_inscriptions_radiations_groupby_bureau["typecat"]][$row_inscriptions_radiations_groupby_bureau["sexe"]] = $row_inscriptions_radiations_groupby_bureau["total"];
}
foreach ($inscriptions_radiations_groupby_bureau as $key1 => $value1) {
    foreach ($value1 as $key2 => $value2) {
        $inscriptions_radiations_groupby_bureau[$key1][$key2]["total"] = (isset($value2["M"]) ? $value2["M"] : 0) + (isset($value2["F"]) ? $value2["F"] : 0);
    }
}

//
$transferts_plus_groupby_bureau = array();
$res_transferts_plus_groupby_bureau = $f->db->query($query_transferts_plus_groupby_bureau);
$f->addToLog("trt/stats_recapitulatif_tableau.php: db->query(\"".$query_transferts_plus_groupby_bureau."\")", VERBOSE_MODE);
$f->isDatabaseError($res_transferts_plus_groupby_bureau);
while ($row_transferts_plus_groupby_bureau =& $res_transferts_plus_groupby_bureau->fetchrow(DB_FETCHMODE_ASSOC)) {
    //
    if (!isset($transferts_plus_groupby_bureau[$row_transferts_plus_groupby_bureau["bureau"]])) {
        $transferts_plus_groupby_bureau[$row_transferts_plus_groupby_bureau["bureau"]] = array();
    }
    //
    $transferts_plus_groupby_bureau[$row_transferts_plus_groupby_bureau["bureau"]][$row_transferts_plus_groupby_bureau["sexe"]] = $row_transferts_plus_groupby_bureau["total"];
}
foreach ($transferts_plus_groupby_bureau as $key => $value) {
    $transferts_plus_groupby_bureau[$key]["total"] = (isset($transferts_plus_groupby_bureau[$key]["M"]) ? $transferts_plus_groupby_bureau[$key]["M"] : 0) + (isset($transferts_plus_groupby_bureau[$key]["F"]) ? $transferts_plus_groupby_bureau[$key]["F"] : 0);
}


//
$transferts_moins_groupby_bureau = array();
$res_transferts_moins_groupby_bureau = $f->db->query($query_transferts_moins_groupby_bureau);
$f->addToLog("trt/stats_recapitulatif_tableau.php: db->query(\"".$query_transferts_moins_groupby_bureau."\")", VERBOSE_MODE);
$f->isDatabaseError($res_transferts_moins_groupby_bureau);
while ($row_transferts_moins_groupby_bureau =& $res_transferts_moins_groupby_bureau->fetchrow(DB_FETCHMODE_ASSOC)) {
    //
    if (!isset($transferts_moins_groupby_bureau[$row_transferts_moins_groupby_bureau["bureau"]])) {
        $transferts_moins_groupby_bureau[$row_transferts_moins_groupby_bureau["bureau"]] = array();
    }
    //
    $transferts_moins_groupby_bureau[$row_transferts_moins_groupby_bureau["bureau"]][$row_transferts_moins_groupby_bureau["sexe"]] = $row_transferts_moins_groupby_bureau["total"];
}
foreach ($transferts_moins_groupby_bureau as $key => $value) {
    $transferts_moins_groupby_bureau[$key]["total"] = (isset($transferts_moins_groupby_bureau[$key]["M"]) ? $transferts_moins_groupby_bureau[$key]["M"] : 0) + (isset($transferts_moins_groupby_bureau[$key]["F"]) ? $transferts_moins_groupby_bureau[$key]["F"] : 0);
}

//
$totalj5=0;
$totalinscription=0;
$totalradiation=0;
$totalapres=0;
$totalarrive=0;
$totaldepart=0;
$totalhommeapres=0;
$totalfemmeapres=0;

//
$numArray = 0;
while ($row=& $res -> fetchRow (DB_FETCHMODE_ASSOC)) {
    $bureau = $row["code"];
    //
    $electeurs_datetableau_precedent = calculNombreDelecteursDateTableau($datetableau_precedent, $bureau);
    //
    $inscriptions = (isset($inscriptions_radiations_groupby_bureau[$row['code']]["Inscription"]["total"]) ? $inscriptions_radiations_groupby_bureau[$row['code']]["Inscription"]["total"] : 0);
    //
    $transferts_plus = (isset($transferts_plus_groupby_bureau[$row['code']]["total"]) ? $transferts_plus_groupby_bureau[$row['code']]["total"] : 0);
    //
    $transferts_moins = (isset($transferts_moins_groupby_bureau[$row['code']]["total"]) ? $transferts_moins_groupby_bureau[$row['code']]["total"] : 0);
    //
    $radiations = (isset($inscriptions_radiations_groupby_bureau[$row['code']]["Radiation"]["total"]) ? $inscriptions_radiations_groupby_bureau[$row['code']]["Radiation"]["total"] : 0);
    //
    $electeurs_datetableau = $electeurs_datetableau_precedent + $inscriptions + $transferts_plus - $transferts_moins - $radiations;
    //
    $electeurs_homme_datetableau = calculNombreDelecteursDateTableau($datetableau, $bureau, "M");
    //
    $electeurs_femme_datetableau = calculNombreDelecteursDateTableau($datetableau, $bureau, "F");
    //
    $erreur = (calculNombreDelecteursDateTableau($datetableau, $bureau) != $electeurs_datetableau ? true : false);
    //
    $datas = array(
                   $row['code']." - ".$row['libelle_bureau'],
                   $electeurs_datetableau_precedent,
                   $inscriptions,
                   $transferts_plus,
                   $transferts_moins,
                   $radiations,
                   $electeurs_datetableau.($erreur == true ? "*" : ""),
                   $electeurs_homme_datetableau,
                   $electeurs_femme_datetableau,
                   );
    $data[$numArray] = $datas;
    $numArray++;
    //
    $totalj5 += $electeurs_datetableau_precedent;
    $totalinscription += $inscriptions;
    $totalarrive += $transferts_plus;
    $totaldepart += $transferts_moins;
    $totalradiation += $radiations;
    $totalapres += $electeurs_datetableau;
    $totalhommeapres += $electeurs_homme_datetableau;
    $totalfemmeapres += $electeurs_femme_datetableau;
}
//
$datas = array(
                   _("TOTAL"),
                   $totalj5,
                   $totalinscription,
                   $totalarrive,
                   $totaldepart,
                   $totalradiation,
                   $totalapres,
                   $totalhommeapres,
                   $totalfemmeapres,
                   );
$data[$numArray] = $datas;

// Array
$tableau = array(
    "collectivite" => $f->collectivite["ville"],
    "format" => "L",
    "title" => _("Recapitulatif du tableau rectificatif du")." ".$f->formatDate($datetableau),
    "subtitle" => $_SESSION["liste"]." - ".$_SESSION["libelle_liste"],
    "offset" => array(80,0,0,0,0,0,0,0,0),
    "align" => array("L","R","R","R","R","R","R","R","R"),
    "column" => array(_("Bureaux de vote"),
                      _("Electeur(s) au")." ".$f->formatDate($datetableau_precedent),
                      _("+ Inscription(s)"),
                      _("+ Transfert(s)"),
                      _("- Transfert(s)"),
                      _("- Radiation(s)"),
                      _("Electeur(s) au")." ".$f->formatDate($datetableau),
                      _("dont Hommes"),
                      _("dont Femmes"),
                      ),
    "data" => $data,
    "output" => "recapitulatif-tableau-rectificatif-".$datetableau,
    );
//print_r($data);
?>

<?php
/**
 * Ce fichier permet de gerer le module Commission. Il affiche
 * differents onglets contenant chacun un traitement en relation avec
 * les commisions.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"module_commission",
               _("Traitement")." -> "._("Module Commission"));

/**
 * Affichage de la structure de la page
 */
// Affichage
$f->setFlag(NULL);
$f->display();

/**
 * ONGLETS
 */
// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n";
// Affichage de la liste des onglets
echo "\n<ul>\n";
//
if ($f->isAccredited(/*DROIT*/"traitement_commission")) {
    echo "\t<li><a href=\"../trt/commission.php\">"._("Commissions")."</a></li>\n";
}
// Affichage de l'onglet d'édition de la convocation à la commission
if ($f->isAccredited(/*DROIT*/"traitement_commission")) {
    echo "\t<li><a href=\"../trt/convocation.php\">"._("Convocation")."</a></li>\n";
}
// Affichage de l'onglet d'édition des membres de commission
if ($f->isAccredited(/*DROIT*/"traitement_commission")) {
    echo "\t<li><a href=\"../scr/soustab.php?obj=membre_commission&amp;idxformulaire=commission&amp;retourformulaire=commission\">"._("Membres")."</a></li>\n";
}
//
echo "</ul>\n";
// Fermeture de la balise - Conteneur d'onglets
echo "</div>";

?>

<?php
/**
 * Ce fichier permet l'affichage des differentes etapes a effectuer lors du traitement j-5.
 * Il contient les liens vers les differentes editions, ainsi que l'execution des differentes requetes.
 * Il recupere les requetes sql dans le fichier du meme nom: sql/.../trt_j5.inc
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_j5",
               _("Traitement")." -> "._("J-5"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
    echo "<div id=\"traitement_j5\">";
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
echo "<div class=\"alert ui-accordion-header ui-helper-reset ui-state-default ui-corner-all\">";
echo "<img title=\""._("Date de tableau")."\" alt=\""._("Date de tableau")."\" src=\"../img/calendar.png\">";
echo _("Tableau du")." ".$f->formatDate($f->collectivite["datetableau"]);
echo "</div>";

/**
 *
 */
//
$description = _("Le \"traitement J-5\" permet d'appliquer les rectifications ".
                 "intervenues depuis la cloture des listes ou depuis le ".
                 "dernier scrutin posterieur a cette cloture (tableau des cinq ".
                 "jours), ainsi que les additions operees au titre du deuxieme ".
                 "alinea de l'article L. 11-2 (tableau des additions). C'est un ".
                 "traitement particulier qui permet de constituer la liste ".
                 "electorale qui entre en vigueur a la date de l'election ".
                 "generale. Pour appliquer le traitement, il suffit de suivre les ".
                 "etapes suivantes.");
$f->displayDescription($description);

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Quand appliquer le traitement J-5 ?");
$f->displaySubTitle($subtitle);
// Affichage du texte du paragraphe
$description = _("Le traitement J-5 doit etre applique 5 jours avant le premier ".
    "tour du scrutin. Il est generalement effectue le mardi avant le scrutin ou ".
    "le lundi si celui-ci se deroule un samedi.");
$f->displayDescription($description);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";


/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Etape 1")." - "._("Selection du ou des tableau(x) a appliquer sur la liste electorale");
$f->displaySubTitle($subtitle);
$description = _("Dans un premier temps, il est necessaire de selectionner ".
                 "le ou les tableau(x) que vous souhaitez appliquer sur la ".
                 "liste electorale. Si vous selectionnez plusieurs tableaux, ".
                 "les nouveaux inscrits se verront attribues un numero d'ordre ".
                 "tous tableaux confondus. Cliquer sur ce premier bouton valider ".
                 "n'applique pas le traitement mais vous permet seulement ".
                 "d'acceder a l'etape suivante.");
$f->displayDescription($description);
//
$step1 = false;
$links = array(
    "0" => array(
        "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_des_cinq_jours",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Tableau des cinq jours"),
    ),
);

//
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."formulairedyn.class.php";
//
echo "<form name=\"f1\" action=\"../trt/traitement_j5.php\" method=\"post\" onsubmit=\"ajaxItForm('traitement_j5', '../trt/traitement_j5.php', this); return false;\">";
//
$champs = array("mouvementatraiter");
//
$form = new formulaire(NULL, 0, 0, $champs);
//
$form->setLib("mouvementatraiter", _("Selectionner ici le ou les tableau(x) ".
                                       "qui entre(nt) en vigueur a la date de l'election"));
$form->setType("mouvementatraiter", "checkbox_multiple");
//
$mouvements_io = "select code, libelle, effet from mouvement inner join param_mouvement ";
$mouvements_io .= " on mouvement.types=param_mouvement.code ";
$mouvements_io .= " where (effet='Election' or effet='Immediat') ";
$mouvements_io .= " and date_tableau='".$f->collectivite["datetableau"]."' ";
$mouvements_io .= " and etat='actif' ";
$mouvements_io .= " group by code, libelle, effet ";
$mouvements_io .= " order by effet DESC, code ";
//
$res = $f->db->query($mouvements_io);
//
$f->addToLog("trt/traitement_j5.php: db->query(\"".$mouvements_io."\")", EXTRA_VERBOSE_MODE);
//
$f->isDatabaseError($res);
//
$contenu = array();
$contenu[0] = array("Immediat",);
$contenu[1] = array(_("Tableau des cinq jours"),);
while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
    $f->addToLog("trt/traitement_j5.php: ".print_r($row, true), EXTRA_VERBOSE_MODE);
    if ($row["effet"] == "Election") {
        array_push($contenu[0], $row["code"]);
        array_push($contenu[1], _("Tableau des additions")." - ".$row["libelle"]);
    }
    array_push($links, array(
            "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_des_additions_des_jeunes&amp;type=".$row["code"],
            "target" => "_blank",
            "class" => "om-prev-icon edition-16",
            "title" => _("Tableau des additions")." - ".$row["libelle"],
        ));
}
$form->setSelect("mouvementatraiter", $contenu);
//
$cinqjours = false;
$additions = array();
if (isset($_POST["mouvementatraiter"])) {
    $val = "";
    foreach($_POST["mouvementatraiter"] as $elem) {
        if ($elem == "Immediat") {
            $cinqjours = true;
        } else {
            array_push($additions, $elem);
        }
        $val .= $elem.";";
    }
    $form->setVal("mouvementatraiter", $val);
    if ($val != "") {
        $step1 = true;
    }
}
//
$form->entete();
$form->afficher($champs, 0, false, false);
$form->enpied();
//
echo "<div class=\"formControls\">\n";
if (count($contenu[0]) == 0) {
    echo "<p class=\"likeabutton\">\n";
    echo _("Aucun mouvement a appliquer");
    echo "</p>\n";
} else {
    echo "<input value=\""._("Valider la selection")."\" type=\"submit\" />";
}
echo "</div>\n";
//
echo "</form>\n";
// Fermeture de la balise - DIV paragraph
echo "</div>\n";


if ($step1 == true) {

    /**
     *
     */
    // Ouverture de la balise - DIV paragraph
    echo "<div class=\"paragraph\">\n";
    // Affichage du titre du paragraphe
    $subtitle = "-> "._("Etape 2 - Verification et application du Traitement J-5 du")." ".date ('d/m/Y')." [Tableau du ".$f->formatDate ($f->collectivite['datetableau'])."]";
    $f->displaySubTitle($subtitle);
    //
    require_once "../obj/traitement.j5.class.php";
    $trt = new j5Traitement($f);
    $trt->displayForm();
    // Fermeture de la balise - DIV paragraph
    echo "</div>\n";

    /**
     *
     */
    // Ouverture de la balise - DIV paragraph
    echo "<div class=\"paragraph\">\n";
    // Affichage du titre du paragraphe
    $subtitle = "-> "._("Etape 3 - Edition des nouvelles cartes d'electeur");
    $f->displaySubTitle($subtitle);
    // Affichage du texte du paragraphe
    $description = _("Une fois le traitement applique, il est possible d'editer les nouvelles cartes electorales a tout moment depuis l'ecran");
    $description .= " <a href=\"../app/revision_electorale.php\">'Edition -> Revision Electorale'</a>.";
    $f->displayDescription($description);
    // Fermeture de la balise - DIV paragraph
    echo "</div>\n";

}

if ($f->isAjaxRequest()) {
    echo "</div>";
}

?>

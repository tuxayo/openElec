<?php
/**
 * Ce fichier permet l'affichage des differentes etapes a effectuer pour gerer les mentions.
 * Il contient les liens vers les differentes editions, ainsi que l'execution des differentes requetes.
 * Il recupere les requetes sql dans le fichier du meme nom: sql/.../trt_mention.inc
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_election_mention",
               _("Traitement")." -> "._("Mentions"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
//
$description = _("Le traitement des mentions permet d'inscrire les mentions ".
                 "(procurations et/ou centre de vote et/ou mairie europe) ".
                 "valides a une date saisie sur les listes d'emargements. Ce ".
                 "traitement ne supprime que les inscriptions sur les listes ".
                 "et non pas les procurations en elles-memes. Attention! Pour ".
                 "les \"mairie europe\" toutes les entrees presentes dans la ".
                 "table sont prises en compte puisqu'aucune date n'y est ".
                 "associee.");
$f->displayDescription($description);

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Preparation des listes d'emargement");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.election_mention.class.php";
$trt = new electionMentionTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

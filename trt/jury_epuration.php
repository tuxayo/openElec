<?php
/**
 * Ce fichier permet de faire le traitement d'epuration des cartes en retour.
 * Il recupere les requetes sql dans le fichier: sql/.../trt_carteretour_epuration.inc
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_jury_epuration",
               _("Traitement")." -> "._("Epuration du jury d'assises"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 * Le module jury n'est pas disponible pour les listes complementaires
 * Si l'utilisateur est connecte sur une liste complementaire, il n'a pas acces
 * a l'interface du traitement
 */
if ($_SESSION['liste'] != "01") {
    // Affichage du message
    $message_class= "error";
    $message = _("Ce module n'est pas disponible pour les listes complementaires.");
    $f->displayMessage($message_class, $message);
    // Fin du script
    die();
}

/**
 *
 */
//
$description = _("L'epuration signifit la suppression des enregistrements ".
                 "dans la base de donnees. Ce traitement doit etre realise ".
                 "avec precaution.");
$f->displayDescription($description);

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Epuration des jures d'assises");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.jury_epuration.class.php";
$trt = new juryEpurationTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>
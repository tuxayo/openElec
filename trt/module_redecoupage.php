<?php
/**
 * Ce fichier permet de gerer le module Redecoupage. Il affiche
 * differents onglets contenant chacun un traitement en relation avec
 * le redecoupage.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"module_redecoupage",
               _("Traitement")." -> "._("Module Redecoupage"));

/**
 * Affichage de la structure de la page
 */
// Affichage
$f->setFlag(NULL);
$f->display();

/**
 * ONGLETS
 */
// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n";
// Affichage de la liste des onglets
echo "\n<ul>\n";
//
if ($f->isAccredited(/*DROIT*/"decoupage_simulation")) {
    echo "\t<li><a href=\"../app/decoupage_simulation.php\" id=\"decoupage_simulation\">"._("Simulation")."</a></li>\n";
}
//
if ($f->isAccredited(/*DROIT*/"traitement_redecoupage")) {
    echo "\t<li><a href=\"../trt/redecoupage.php\" id=\"traitement_redecoupage\">"._("Redecoupage")."</a></li>\n";
}
//
if ($f->isAccredited(/*DROIT*/"decoupage_initialisation")) {
    echo "\t<li><a href=\"../app/decoupage_initialisation.php\" id=\"decoupage_initialisation\">"._("Initialisation")."</a></li>\n";
}
//
echo "</ul>\n";
// Fermeture de la balise - Conteneur d'onglets
echo "</div>";

?>

<?php
/**
 * Ce fichier permet l'affichage des differentes etapes a effectuer lors du
 * traitement de mise a jour des dates de centre de vote
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_centrevote",
               _("Traitement")." -> "._("Centres de vote"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
//
$description = _("Ce traitement permet de changer les dates de debut et de ".
                 "fin de validite pour tous les electeurs en centres de vote. ".
                 "Il vous suffit d'indiquer la nouvelle date de debut de ".
                 "validite, puis de confirmer a l'aide du bouton. Tous les ".
                 "electeurs en centre de vote verront alors la date de debut ".
                 "de validite de leur inscription en centre de vote modifiee ".
                 "pour une une duree d'une annee.");
$f->displayDescription($description);

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> ". _("Mise a jour de la date de debut de validite");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.centrevote.class.php";
$trt = new centrevoteTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Editions des statistiques, listings et etiquettes");
$f->displaySubTitle($subtitle);
//
$links = array(
    "0" => array(
        "href" => "../pdf/pdffromarray.php?obj=centrevote_sexe_bureau",
        "target" => "_blank",
        "class" => "om-prev-icon statistiques-16",
        "title" => _("Electeurs en centre de vote par sexe / details par bureau"),
        "description" => _("Nombre d'electeurs en centre de vote, d'hommes, de femmes par bureau"),
    ),
    "1" => array(
        "href" => "../pdf/listecentrevote.php",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Listing des electeurs inscrits en centre de vote"),
    ),
    "2" => array(
        "href" => "../app/pdfetiquette.php?obj=centrevote",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Etiquettes des electeurs inscrits en centre de vote"),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

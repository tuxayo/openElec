<?php
/**
 * Ce script permet de gérer l'interface utilisateur du traitement d'export 
 * des tableaux pour la préfecture.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils(
    "nohtml", 
    /*DROIT*/"prefecture_dematerialisation_tableau",
    _("Traitement")." -> "._("Tableau pour la Prefecture")
);

/**
 *
 */
//
$description = _(
	"Les prefectures demandent aux communes de fournir des tableaux sous".
	" la forme de fichiers PDF. Tous les tableaux demandes sont directement ".
	" telechargeables depuis l'ecran 'Edition -> Revision Electorale'.".
    " Il faut ensuite se rendre sur la plate-fome du ministere dediee a cet ".
    " effet pour leur transmettre ces fichiers pour chacune des listes."
);
$f->displayDescription($description);

?>

<?php
/**
 * STATISTIQUES : Nombre inscription office, d'hommes, de femmes par sexe
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"statistiques", _("Statistiques"));

// Recuperation des donnees
$data = array();

// STATISTIQUES : Nombre d'inscription office (code insee 8), d'inscription
//  nouvelle (code insee 1), et de décés (code insee D) par bureau pour la
//  liste et la date de tableau en cours
$inc = "stats_io_sexe";

//
include "../sql/".$f -> phptype."/".$inc.".inc";
$res = $f -> db -> query ($sql_bureau);
$f->isDatabaseError($res);

//
$numArray = 0;
while ($row =& $res -> fetchRow (DB_FETCHMODE_ASSOC)) {
    //
    $c = array ('champ' => "codeinscription", 'valeur' => "8");
    include "../sql/".$f -> phptype."/".$inc.".inc";
    // homme
    $res1 = $f -> db -> getOne ($sql1);
    $f->isDatabaseError($res1);
    // femme
    $res2 = $f -> db -> getOne ($sql2);
    $f->isDatabaseError($res2);
    // total
    $temp =  $res1+$res2;
    //
    $datas = array(
                   $row['code'],
                   $row['libelle_bureau'],
                   $res1,
                   $res2,
                   $temp
                   );
    $data[$numArray] = $datas;
    $numArray++;
}
// Array
$tableau = array(
    "format" => "L",
    "title" => _("Statistiques - IO"),
    "subtitle" => _("Details par sexe"),
    "offset" => array(10,0,50,50,40),
    "column" => array("",
                      _("LISTE")." : ".$_SESSION ['liste']." ".$_SESSION ['libelle_liste']."",
                      _("Inscription D'office homme(8)"),
                      _("Inscription Nouvelle femme(8)"),
                      _("Inscription Office")
                      ),
    "data" => $data,
    "output" => "stats-iosexe"
    );

?>

<?php
/**
 * Ce fichier permet l'affichage des differentes etapes a effectuer pour gerer
 * les cartes retour.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_carteretour",
               _("Traitement")." -> "._("Cartes en retour (saisie par lots)"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> ". _("Saisie de la carte en retour");
$f->displaySubTitle($subtitle);
//
?>
<script type="text/javascript">
function trt_form_trigger_succes() {
    giveFocus();
}
</script>
<?php
//
require_once "../obj/traitement.carteretour.class.php";
$trt = new carteretourTraitement($f);
$trt->displayForm();
//
echo "<script type=\"text/javascript\">";
echo "function giveFocus(){
document.getElementsByName('id_elec')[0].value = '';
document.getElementsByName('id_elec')[0].focus();
};";
echo "giveFocus();";
echo "</script>";
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

<?php
/**
 * Ce fichier permet de gerer le module Election. Il affiche
 * differents onglets contenant chacun un traitement en relation avec
 * les elections.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"module_election",
               _("Traitement")." -> "._("Module Election"));

/**
 * Affichage de la structure de la page
 */
// Affichage
$f->setFlag(NULL);
$f->display();

/**
 * ONGLETS
 */
// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n";
// Affichage de la liste des onglets
echo "\n<ul>\n";
//
if ($f->isAccredited(/*DROIT*/"traitement_election_mention")) {
    echo "\t<li><a href=\"../trt/election_mention.php\">"._("Mentions")."</a></li>\n";
}
//
if ($f->isAccredited(/*DROIT*/"traitement_election_epuration")) {
    echo "\t<li><a href=\"../trt/election_epuration.php\">"._("Epuration")."</a></li>\n";
}
//
echo "</ul>\n";
// Fermeture de la balise - Conteneur d'onglets
echo "</div>";

?>

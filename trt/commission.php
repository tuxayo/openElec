<?php
/**
 * Ce fichier permet l'affichage des differentes etapes a effectuer lors des
 * commissions. Il contient les liens vers les differentes
 * editions, ainsi que l'execution des differentes requetes.
 * Il recupere les requetes sql dans le fichier: sql/.../trt_commissions.inc
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_commission",
               _("Traitement")." -> "._("Commissions"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
//
$description = _("Le traitement des commissions permet la preparation des ".
                 "commissions et l'application des decisions. Les dates sont ".
                 "celles de la derniere edition et la date du jour. Pour ".
                 "appliquer les decisions on supprime les mouvements rejetes.");
$f->displayDescription($description);

/**
 * Export des éditions de mouvement sur interval de dates
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Edition des mouvements a presenter (rupture par bureau)");
$f->displaySubTitle($subtitle);
//
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."formulairedyn.class.php";
//
echo "<form name=\"f1\" action=\".\">";
//
$champs = array("datedebut", "datefin");
//
$form = new formulaire(NULL, 0, 0, $champs);
//
$form->setLib("datedebut", _("Date de debut"));
$form->setType("datedebut", "date");
$form->setTaille("datedebut", 10);
$form->setMax("datedebut", 10);
$form->setOnchange("datedebut", "fdate(this)");
//
$form->setLib("datefin", _("Date de fin"));
$form->setType("datefin", "date");
$form->setTaille("datefin", 10);
$form->setMax("datefin", 10);
$form->setOnchange("datefin", "fdate(this)");
//
$form->entete();
$form->afficher($champs, 0, false, false);
$form->enpied();
//
echo "<div class=\"formControls\">\n";
echo "<p class=\"likeabutton\">\n";
echo "<a href=\"javascript:entre2dates()\">";
echo _("Telecharger l'edition des mouvements entre deux dates");
echo "</a>\n";
echo "</p>\n";
echo "</div>\n";
//
echo "</form>\n";
// Fermeture de la balise - DIV paragraph
echo "</div>\n";
/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Suppression des mouvements refuses");
$f->displaySubTitle($subtitle);
//
$links = array(
    "0" => array(
        "href" => "../scr/tab.php?obj=inscription",
        "class" => "om-prev-icon inscriptions-16",
        "title" => _("Suppression des inscriptions rejetees"),
    ),
    "1" => array(
        "href" => "../scr/tab.php?obj=modification",
        "class" => "om-prev-icon modifications-16",
        "title" => _("Suppression des modifications rejetees"),
    ),
    "2" => array(
        "href" => "../scr/tab.php?obj=radiation",
        "class" => "om-prev-icon radiations-16",
        "title" => _("Suppression des radiations rejetees"),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

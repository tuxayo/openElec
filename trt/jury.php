<?php
/**
 * Ce fichier permet l'affichage des differentes etapes a effectuer pour gerer
 * le jury.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_jury",
               _("Traitement")." -> "._("Jury d'assises"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 * Le module jury n'est pas disponible pour les listes complementaires
 * Si l'utilisateur est connecte sur une liste complementaire, il n'a pas acces
 * a l'interface du traitement
 */
if ($_SESSION['liste'] != "01") {
    // Affichage du message
    $message_class= "error";
    $message = _("Ce module n'est pas disponible pour les listes complementaires.");
    $f->displayMessage($message_class, $message);
    // Fin du script
    die();
}

/**
 *
 */
//
$description = _("Ce traitement permet de gerer les jures d'assises. Le tirage ".
                 "aleatoire permet de selectionner des electeurs selon ".
                 "la reglementation en vigueur.");
$f->displayDescription($description);

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> ". _("Tirage aleatoire");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.jury.class.php";
$trt = new juryTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Editions des statistiques, listings et etiquettes");
$f->displaySubTitle($subtitle);
//
$links = array(
    "0" => array(
        "href" => "../app/pdfetiquette.php?obj=jury",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Etiquettes des jures"),
    ),
    "1" => array(
        "href" => "../app/pdf_recapitulatif_jury.php",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Liste preparatoire du jury d'assises"),
    ),
    "3" => array(
        "href" => "../scr/requeteur.php?obj=jury",
        "class" => "om-prev-icon reqmo-16",
        "title" => _("Export CSV des electeurs selectionnes"),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

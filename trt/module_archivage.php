<?php
/**
 * Ce fichier permet de gerer le module Archivage. Il affiche
 * differents onglets contenant chacun un traitement en relation avec
 * l'archivage.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"module_archivage",
               _("Traitement")." -> "._("Module Archivage"));

/**
 * Affichage de la structure de la page
 */
// Affichage
$f->setFlag(NULL);
$f->display();

/**
 * ONGLETS
 */
// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n";
// Affichage de la liste des onglets
echo "\n<ul>\n";
//
if ($f->isAccredited(/*DROIT*/"traitement_archivage")) {
    echo "\t<li><a href=\"../trt/archivage.php\">"._("Archivage")."</a></li>\n";
}
//
echo "</ul>\n";
// Fermeture de la balise - Conteneur d'onglets
echo "</div>";

?>

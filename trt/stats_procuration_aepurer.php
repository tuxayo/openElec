<?php
/**
 * Statistiques des procuration a epurer
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once("../obj/utils.class.php");

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"statistiques", _("Statistiques"));

// Recuperation des donnees
$data = array();

//
$inc = "stats_procuration_aepurer";
$dateElection = "";
if (isset ($_GET ['dateElection']))
    $dateElection = $_GET ['dateElection'];
if ($dateElection == "")
    $dateElection = date ('d/m/Y');

//
if ($dateElection != "") {
    if ($f -> formatdate == "AAAA-MM-JJ") {
        $date = explode("/", $dateElection);
        // controle de date
        if (sizeof($date) == 3 and (checkdate($date[1],$date[0],$date[2]))) {
            $dateElectionR = $date[2]."-".$date[1]."-".$date[0];
        } else {
            die("La date ".$dateElection." n'est pas une date.");
            $correct=false;
        }
    }
    if ($f -> formatdate == "JJ/MM/AAAA") {
        $date = explode("/", $dateElection);
        // controle de date
        if (sizeof($date) == 3 and checkdate($date[1],$date[0],$date[2])) {
            $dateElectionR = $date[0]."/".$date[1]."/".$date[2];
        } else {
            die("La date ".$dateElection." n'est pas une date.");
            $correct=false;
        }
    }
}

//
include "../sql/".$f -> phptype."/".$inc.".inc";

//
$res = $f -> db -> query ($sql);
if (database::isError($res))
    die($res->getMessage()." erreur sur ".$sql);

//
$totalavant = 0;
$totalapres = 0;
$totalapres1 = 0;

//
$numArray = 0;
while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
    //
    include "../sql/".$f -> phptype."/".$inc.".inc";
    // avant
    $avant = $f -> db -> getOne ($sql1);
    if (database::isError($avant))
        die($avant->getMessage()." erreur sur ".$sql1);
    $totalavant=$totalavant+$avant;
    // centre vote
    $apres = $f -> db -> getOne ($sql2);
    if (database::isError($apres))
        die($apres->getMessage()." erreur sur ".$sql2);
    $totalapres=$totalapres+$apres;
    //
    $apres1 = $avant - $apres;
    //
    $totalapres1=$totalapres1+$apres1;
    //
    $datas = array(
                   $row['code'],
                   $row['libelle_bureau'],
                   $avant,
                   $apres,
                   $apres1
                   );
    $data[$numArray] = $datas;
    $numArray++;
}
//
$datas = array(
                   "-",
                   _("TOTAL"),
                   $totalavant,
                   $totalapres,
                   $totalapres1
                   );
$data[$numArray] = $datas;
// Array
$tableau = array(
    "format" => "L",
    "title" => _("Statistiques - Procuration a epurer"),
    "subtitle" => _("Epuration des procurations au ").$dateElection,
    "offset" => array(10,0,50,50,40),
    "column" => array("",
                      _("LISTE")." : ".$_SESSION ['liste']." ".$_SESSION ['libelle_liste']."",
                      _("Avant Epuration"),
                      _("Epuration"),
                      _("Apres Epuration")
                      ),
    "data" => $data,
    "output" => "stats-procurationaepurer"
    );

?>

<?php
/**
 * Ce fichier permet de faire le traitement d'epuration des cartes en retour.
 * Il recupere les requetes sql dans le fichier: sql/.../trt_carteretour_epuration.inc
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_carteretour_epuration",
               _("Traitement")." -> "._("Epuration des cartes en retour"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
//
$description = _("L'epuration signifit la suppression des enregistrements ".
                 "dans la base de donnees. Ce traitement doit etre realise ".
                 "avec precaution.");
$f->displayDescription($description);

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Epuration des cartes en retour");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.carteretour_epuration.class.php";
$trt = new carteretourEpurationTraitement($f);
$trt->displayForm();
//
echo "</form>\n";
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

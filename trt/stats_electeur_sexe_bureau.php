<?php
/**
 * STATISTIQUES : Nombre d'electeurs, d'hommes, de femmes par bureau pour la
 * liste en cours et pour la collectivite en cours
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";

if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"statistiques", _("Statistiques"));

// Recuperation des donnees
$data = array();

// Identifiant de la statistique
$inc = "stats_electeur_sexe_bureau";

//
if (!file_exists("../sql/".$f->phptype."/".$inc.".inc")) {
    $f->notExistsError();
}

//
$total_count_electeur_bureau = 0;
$total_count_electeur_homme_bureau = 0;
$total_count_electeur_femme_bureau = 0;

//
include "../sql/".$f->phptype."/".$inc.".inc";

//
$res_select_bureau = $f->db->query($query_select_bureau);
$f->isDatabaseError($res_select_bureau);

//
$numArray = 0;
while ($row_select_bureau =& $res_select_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
    //
    include "../sql/".$f->phptype."/".$inc.".inc";
    //
    $res_count_electeur_bureau = $f->db->getOne($query_count_electeur_bureau);
    $f->isDatabaseError($res_count_electeur_bureau);
    //
    $total_count_electeur_bureau += $res_count_electeur_bureau;	
    //
    $res_count_electeur_homme_bureau = $f->db->getOne($query_count_electeur_homme_bureau);
    $f->isDatabaseError($res_count_electeur_homme_bureau);
    //
    $total_count_electeur_homme_bureau += $res_count_electeur_homme_bureau;
    //
    $res_count_electeur_femme_bureau = $f->db->getOne($query_count_electeur_femme_bureau);
    $f->isDatabaseError($res_count_electeur_femme_bureau);
    //
    $total_count_electeur_femme_bureau += $res_count_electeur_femme_bureau;
    //
    $datas = array(
                   $row_select_bureau['code'],
                   $row_select_bureau['libelle_bureau'],
                   $res_count_electeur_bureau,
                   $res_count_electeur_homme_bureau,
                   $res_count_electeur_femme_bureau
                   );
    $data[$numArray] = $datas;
    $numArray++;
}

//
$data[$numArray] = array(
                        "-",
                        _("TOTAL"),
                        $total_count_electeur_bureau,
                        $total_count_electeur_homme_bureau,
                        $total_count_electeur_femme_bureau
                        );
// Array
$tableau = array(
    "format" => "L",
    "title" => _("Statistiques - Electeur"),
    "subtitle" => _("Details par sexe et bureau de vote"),
    "offset" => array(10,0,40,40,40),
    "column" => array("",
                      _("LISTE")." : ".$_SESSION ['liste']." ".$_SESSION ['libelle_liste']."",
                      _("Total"),
                      _("Hommes"),
                      _("Femmes")
                      ),
    "data" => $data,
    "output" => "stats-electeursexebureau"
    );

?>

<?php
/**
 * 
 * 
 */

if (file_exists ("../obj/utils.class.php")) {
    include ("../obj/utils.class.php");
}

// variable $obj
$obj = "edition";

// new utils
$f = new utils ("nohtml", $obj);

$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];

///// Session collectivite
$base_collectivite = $_SESSION['collectivite'];

$nom_fichier="export_sql[".$f->collectivite['ville']."].sql";
$fic = fopen ($f->getParameter("tmpdir").$nom_fichier,"w");

		/////// Parametrage des tables et sous-tables
        $tables = array(
                        'archive' => "collectivite='{id}'",
                        'bureau' => "collectivite='{id}'",
						'canton' => "",
						'collectivite' => "id='{id}'",
						'departement' => "",
						'commune' => "",
						'inscription_office' => "collectivite='{id}'",
						'liste' => "",
						'mouvement' => "collectivite='{id}'",
						'nationalite' => "",
						'numerobureau' => "collectivite='{id}'",
						'numeroliste' => "collectivite='{id}'",
						'param_mouvement' => "",
						'version' => "",
						'electeur' => "collectivite='{id}'",
						'voie' => "code_collectivite='{id}'",
                        );

		$sous_tables = array(
						'electeur' => array('centrevote' => "electeur.collectivite='{id}' and electeur.id_electeur=centrevote.id_electeur",
											'procuration' => "electeur.collectivite='{id}' and (electeur.id_electeur=procuration.mandant or electeur.id_electeur=procuration.mandataire)",
											'mairieeurope' => "electeur.collectivite='{id}' and electeur.id_electeur=mairieeurope.id_electeur_europe"
											),
						'voie' => array('decoupage' => "voie.code_collectivite='{id}' and voie.code=decoupage.code_voie")
						);
		//////////////////////////////////////////////////

	$ligneSql = "-- --------------------------------------------------------------------------------\n";
	$ligneSql .= "-- OPENELEC : EXPORTATION SQL DE LA COMMUNE DE ".$f->collectivite['ville']."\n";
	$ligneSql .= "-- --------------------------------------------------------------------------------\n\n";
    fwrite ($fic,  $ligneSql);
	
	/////////////// TABLES ///////////////////////////
     foreach($tables as $t => $tv)
     {
        ///// Recuperation des nom et type des colonnes de la table
		$colonnes = array();
		$colonnesTypes = array();
        $requete="select column_name from INFORMATION_SCHEMA.COLUMNS where table_name='".$t."'; ";
        $resType="select * from ".$t." limit 1; ";		
		$res = pg_query($resType);		
        $collect = array();
        $rescom =& $f -> db -> query ($requete);
        $f->isDatabaseError($rescom);
        while ($rowcom =& $rescom -> fetchRow (DB_FETCHMODE_ASSOC))
		$collect[] = $rowcom;
		$numrow=0;
		foreach($collect as $c){
			foreach($c as $cname){
				$colonnes[] = $cname;
				$colonnesTypes[] = pg_fieldtype($res,$numrow);
			}
			$numrow++;
		}
		
		///// Recuperation des enregistrements de la table
        $requete="select * from ".$t;
		if(!empty($tv)) $requete .= " where ".str_replace("{id}",$base_collectivite,$tv).";";
        $rescom =& $f -> db -> query ($requete);
        $f->isDatabaseError($rescom);
		
		////// Preparation de COPY de la table
		$ligneSql = "--\n";
		$ligneSql .= "-- Data for Name: ".$t."; Type: TABLE DATA; Schema: public; Owner: -\n";
		$ligneSql .= "--\n\n";
		$ligneSql .= "COPY ".$t." (";
		foreach($colonnes as $col) $ligneSql .= $col.", ";
		$ligneSql .= "*) FROM stdin;";
		$ligneSql = str_replace(", *)",")",$ligneSql)."\n";
		fwrite ($fic, $ligneSql);
		$ligneSql = "";		
        while ($rowcom =& $rescom -> fetchRow (DB_FETCHMODE_ASSOC))
		{
			$numRow = 0;
			foreach($rowcom as $d => $vv){
					$valvv = $vv;
					
					/// Si colonne type date et valeur NULL
					if($valvv == NULL && $colonnesTypes[$numRow] == "date"){ $valvv="\\N"; };
					
					$ligneSql .= $valvv."\t";
					$numRow++;
			}
			$ligneSql .= "\n";
		}
		$ligneSql = str_replace("\t\n","\n",$ligneSql);
		fwrite ($fic, $ligneSql);		
		fwrite ($fic, "\\.\n\n\n");
		
		//////////////////// SOUS-TABLES ////////////////////
		foreach($sous_tables as $st => $stval){
			if($st==$t)
			{
				foreach($stval as $t2 => $tv2){
					//////////////////////////////////////////////////////////////////////////////////
					///// Recuperation des nom et type des colonnes de la sous table
					$colonnes2 = array();
					$colonnesTypes2 = array();
					$requete2="select column_name from INFORMATION_SCHEMA.COLUMNS where table_name='".$t2."'; ";
					$resType2="select * from ".$t2." limit 1; ";		
					$res2 = pg_query($resType2);		
					$collect2 = array();
					$rescom2 =& $f -> db -> query ($requete2);
					$f->isDatabaseError($rescom2);
					while ($rowcom2 =& $rescom2 -> fetchRow (DB_FETCHMODE_ASSOC))
					$collect2[] = $rowcom2;
					$numrow2=0;
					foreach($collect2 as $c2){
						foreach($c2 as $cname2){
							$colonnes2[] = $cname2;
							$colonnesTypes2[] = pg_fieldtype($res2,$numrow2);
						}
					$numrow2++;
					}
					
					///// Recuperation des enregistrements de la sous table
					$requete2="select ".$t2.".* from ".$t.", ".$t2." ";
					if(!empty($tv2)) $requete2 .= " where ".str_replace("{id}",$base_collectivite,$tv2).";";

					$rescom2 =& $f -> db -> query ($requete2);
					$f->isDatabaseError($rescom2);
					
					////// Preparation de COPY de la sous table
					$ligneSql2 = "--\n";
					$ligneSql2 .= "-- Data for Name: ".$t2."; Type: TABLE DATA; Schema: public; Owner: -\n";
					$ligneSql2 .= "--\n\n";
					$ligneSql2 .= "COPY ".$t2." (";
					foreach($colonnes2 as $col2) $ligneSql2 .= $col2.", ";
					$ligneSql2 .= "*) FROM stdin;";
					$ligneSql2 = str_replace(", *)",")",$ligneSql2)."\n";
					fwrite ($fic, $ligneSql2);
					$ligneSql2 = "";					
					while ($rowcom2 =& $rescom2 -> fetchRow (DB_FETCHMODE_ASSOC))
					{
						$ligneSql2 = "";							
						$numRow2 = 0;
						foreach($rowcom2 as $d2 => $vv2){
								$valvv2 = $vv2;
								
								/// Si colonne type date et valeur NULL
								if($valvv2 == NULL && $colonnesTypes2[$numRow2] == "date"){ $valvv2="\\N"; };
								
								$ligneSql2 .= $valvv2."\t";
								$numRow2++;
						}
						$ligneSql2 .= "\n";
						
					}
					$ligneSql2 = str_replace("\t\n","\n",$ligneSql2);
					fwrite ($fic, $ligneSql2);		
					fwrite ($fic, "\\.\n\n\n");						
					///////////////////////////////////////////////////////////////////////
				}
			}
		} ////////////////////////// FIN SOUS TABLE /////////////////
     } /////////////////// FIN TABLE ////////////////////
        
fclose ($fic);

$msg = "Le fichier a été exporté, vous pouvez l'ouvrir immédiatement en cliquant sur : ";
$msg .= "<a target=\"_blank\" href=\"../app/file.php?fic=".$nom_fichier."&amp;folder=tmp\" rel=\"facebox\"><img src=\"../img/voir.png\" alt=\"Fichier export\" title=\"Fichier export\" /></a>.<br />";
$msg .= "Il est également accessible dans les traces, son nom est : \"".$nom_fichier."\".<br />";
echo $msg;

?>
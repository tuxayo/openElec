<?php
/**
 * Ce fichier permet de gerer le module Refonte de l'application. Il affiche
 * differents onglets contenant chacun un traitement en relation avec
 * la refonte de la liste electorale.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_refonte",
               _("Traitement")." -> "._("Refonte"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
//
$description = _("La refonte a lieue tous les trois a cinq ans. Elle permet ".
                 "la renumerotation complete de la liste electorale. Chaque ".
                 "electeur se voit attribuer un nouveau numero dans la liste ".
                 "ainsi qu'un nouveau numero dans son bureau. La numerotation ".
                 "est effectuee en fonction de l'ordre alphabetique des ".
                 "electeurs. La refonte est suivie de l'edition de la totalite ".
                 "des cartes d'electeur.");
$f->displayDescription($description);

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Renumerotation de la liste electorale");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.refonte.class.php";
$trt = new refonteTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

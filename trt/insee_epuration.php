<?php
/**
 * Ce fichier permet de faire le traitement d'epuration IO et RAD.
 * Il recupere les requetes sql dans le fichier: sql/.../trt_insee_epuration.inc
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_insee_epuration",
               _("Traitement")." -> "._("Epuration INSEE"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
//
$description = _("L'epuration signifit la suppression des enregistrements ".
                 "dans la base de donnees. Ce traitement doit etre realise ".
                 "avec precaution.");
$f->displayDescription($description);

/**
 *
 */
/**
 * Le traitement insee n'est pas disponible pour les listes complementaires
 * Si l'utilisateur est connecte sur une liste complementaire, il n'a pas acces
 * a l'interface du traitement
 */
if ($_SESSION['liste'] === "01") {

    // Ouverture de la balise - DIV paragraph
    echo "<div class=\"paragraph\">\n";
    // Affichage du titre du paragraphe
    $subtitle = "-> "._("Epuration des inscriptions d'office INSEE");
    $f->displaySubTitle($subtitle);
    //
    require_once "../obj/traitement.insee_epuration_inscription_office.class.php";
    $trt = new inseeEpurationInscriptionOfficeTraitement($f);
    $trt->displayForm();
    // Fermeture de la balise - DIV paragraph
    echo "</div>\n";
}
/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Epuration des radiations INSEE");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.insee_epuration_radiation.class.php";
$trt = new inseeEpurationRadiationTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

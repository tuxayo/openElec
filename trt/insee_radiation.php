<?php
/**
 * Ce fichier permet de faire le traitement d'import des radiations cnen insee.
 * Il recupere les requetes sql dans le fichier: sql/.../trt_insee_radiation.inc
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils(
    "nohtml",
    "traitement_insee_radiation", //DROIT
    _("Traitement")." -> "._("Radiations INSEE")
);

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
//
$description = _("Ce module permet de gerer l'import et l'edition des radiations ".
                 "de l'INSEE importes dans le logiciel numeriquement.".
                 "Il se divise en deux parties : l'import et l'edition. ".
                 "Il suffit de suivre les instructions de chacune de ces parties.");
$f->displayDescription($description);

/**
 * Affichage du formulaire de lancement du traitement
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Import des radiations INSEE");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.insee_radiation.class.php";
$trt = new inseeRadiationTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Validation des radiations INSEE");
$f->displaySubTitle($subtitle);
//
echo "<p>";
echo _("Une fois l'import effectue, il faut valider chacune des radiations ".
       "importee pour verifier les donnees importees et transformer ces ".
       "donnees en mouvements de radiation.");
echo "</p>";
//
$links = array(
    "0" => array(
        "href" => "../scr/tab.php?obj=radiation_insee",
        "class" => "om-prev-icon radiation-insee-16",
        "title" => _("Validation des radiations INSEE"),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

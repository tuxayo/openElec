<?php
/**
 * Ce fichier permet l'affichage des differentes etapes a effectuer lors du
 * traitement de fin d'annee. Il contient les liens vers les differentes
 * editions, ainsi que l'execution des differentes requetes.
 * Il recupere les requetes sql dans le fichier: sql/.../trt_annuel.inc
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_annuel",
               _("Traitement")." -> "._("Annuel"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
echo "<div class=\"alert ui-accordion-header ui-helper-reset ui-state-default ui-corner-all\">";
echo "<img title=\""._("Date de tableau")."\" alt=\""._("Date de tableau")."\" src=\"../img/calendar.png\">";
echo _("Tableau du")." ".$f->formatDate($f->collectivite["datetableau"]);
echo "</div>";

/**
 *
 */
//
$description = _(
    "Le traitement annuel va appliquer les mouvements actifs a effet 'Annuel' ".
    "(1er mars) ainsi que ceux a effet 'Immediat' sur la liste electorale. Les ".
    "mouvements appliques sont ceux concernant la date de tableau en cours et ".
    "la liste par defaut selectionnee. Pour appliquer le traitement, il suffit ".
    "de suivre les etapes suivantes. Attention, les mouvements a effet 'Election' ".
    "ne sont pas appliques, ils doivent l'etre par le traitement J-5."
);
$f->displayDescription($description);


/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Quand appliquer le traitement annuel ?");
$f->displaySubTitle($subtitle);
// Affichage du texte du paragraphe
$description = _(
    "Le traitement annuel doit etre applique le 10 janvier et le dernier ".
    "jour de fevrier une fois que la commission a dresse respectivement le premier et ".
    "le second tableau rectificatif."
);
$f->displayDescription($description);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Etape 1 - Verification des doublons");
$f->displaySubTitle($subtitle);
//
echo "<p>";
echo _("Cette recherche verifie que les inscriptions dans l'etat actif n'ont ".
       "pas de doublons dans la liste electorale. C'est-a-dire si une nouvelle ".
       "inscription d'electeur n'est pas deja inscrit dans la liste (meme nom ".
       "et date de naissance).");
echo "</p>";
//
$links = array(
    "0" => array(
        "href" => "../pdf/pdffromarray.php?obj=doublon2",
        "target" => "_blank",
        "class" => "om-prev-icon doublon-16",
        "title" => _("Verification des doublons"),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Traitement annuel");
$subtitle = "-> "._("Etape 2 - Verification et application du Traitement annuel du")." ".$f->formatDate ($f->collectivite['datetableau'])." [Tableau du ".$f->formatDate ($f->collectivite['datetableau'])."]";
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.annuel.class.php";
$trt = new annuelTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Etape 3 - Edition des nouvelles cartes d'electeur");
$f->displaySubTitle($subtitle);
// Affichage du texte du paragraphe
$description = _("Une fois le traitement applique, il est possible d'editer les nouvelles cartes electorales a tout moment depuis l'ecran");
$description .= " <a href=\"../app/revision_electorale.php\">'Edition -> Revision Electorale'</a>.";
$f->displayDescription($description);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

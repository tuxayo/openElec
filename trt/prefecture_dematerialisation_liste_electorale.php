<?php
/**
 * Ce script permet de gérer l'interface utilisateur du traitement d'export 
 * de la liste électorale pour la préfecture.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils(
    "nohtml", 
    /*DROIT*/"prefecture_dematerialisation_liste_electorale",
    _("Traitement")." -> "._("Liste electorale pour la Prefecture")
);

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 * Declaration d'une fonction permettant de lister les dix derniers exports
 * realises par la commune. La fonction est necessaire puisque ce code et 
 * uniquement celui ci va etre appele en ajax pour recharger la liste des 
 * exports au moment de la validation des traitements.
 */
function displayListContent($f) {
    //
    $tab = array();
    //
    $dossier = opendir($f->getParameter("tmpdir"));
    //
    while ($entree = readdir($dossier)) {
        // Si le fichier correspond à un export préfecture
        if (preg_match( "/_LE_/i" , $entree )) {
            //
            $extension = array_pop(explode(".", $entree));
            //
            $filename = explode("_", $entree);
            //
            $collectivite_code_insee = $filename[0];
            //
            if ($collectivite_code_insee != $f->collectivite["inseeville"]) {
                continue;
            }
            //
            $export_liste = $filename[2];
            $export_date = $filename[3];
            $export_heure = $filename[4];
            //
            $date = date("d/m/Y H:i:s", 
                mktime(
                    substr($export_heure, 0, 2), // [ int $hour = date("H") [,
                    substr($export_heure, 2, 2), // int $minute = date("i") [,
                    substr($export_heure, 4, 2), // int $second = date("s") [,
                    substr($export_date, 4, 2), // int $month = date("n") [,
                    substr($export_date, 6, 2), // int $day = date("j") [,
                    substr($export_date, 0, 4) // int $year = date("Y") [,
                )
            );
            //
            if (preg_match( "/_LE_/i" , $entree )) {
                $title = _("Liste")." ".$export_liste." "._("du")." ".$date." [".$extension."]";
            }
            //
            array_push($tab,
                array ("date" => $export_date.$export_heure,
                       "folder" => 'tmp',
                       "file" => $entree,
                       "ext" => $extension,
                       "title" => $title
                )
            );               
        }
    }
    //
    closedir($dossier);
    arsort($tab);
    //
    $links = array();
    //
    foreach ($tab as $key => $elem) {
        array_push($links, array(
            "href" => "../app/file.php?fic=".$elem['file']."&amp;folder=".$elem['folder'],
            "target" => "_blank",
            "class" => "om-prev-icon file-16",
            "title" => $elem['title'],));
        if (count($links) == 10) {
            break;
        }
    }
    //
    $f->displayLinksAsList($links);
}


/**
 *
 */
//
$display = "";
if (isset($_GET['display'])) {
    $display = $_GET['display'];
}
//
if ($display == "list_content") {
    displayListContent($f);
    die();
}

/**
 *
 */
//
$description = _(
    "Les prefectures demandent aux communes de fournir une liste electorale".
    " sous la forme d'un fichier CSV ou XML. Cet ecran permet de generer le".
    " fichier en question. Il suffit de cliquer sur le bouton 'Generation du".
    " fichier' puis de le telecharger dans la section 'Derniers exports'. La".
    " liste electorale ainsi generee est celle qui concerne la liste en cours.".
    " Il faut ensuite se rendre sur la plate-fome du ministere dediee a cet effet".
    " pour leur transmettre ce fichier pour chacune des listes."
);
$f->displayDescription($description);

/**
 * Affichage du formulaire de lancement du traitement
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> ". _("Generation du fichier");
$f->displaySubTitle($subtitle);
//
?>
<script type="text/javascript">
function trt_form_trigger_succes() {
    $("#prefecture_dematerialisation_liste_electorale_list_content").html(msg_loading);
    $.ajax({
       type: "GET",
       url: "../trt/prefecture_dematerialisation_liste_electorale.php?display=list_content",
       dataType: "html",
       error:function(msg){
         alert( "Error !: " + msg );
       },
       success:function(data){
            $("#prefecture_dematerialisation_liste_electorale_list_content").html(data);
    }});
}
</script>
<?php
//
require_once "../obj/traitement.prefecture_dematerialisation_liste_electorale.class.php";
$trt = new prefectureDematerialisationListeElectoraleTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 * Affichage des dix derniers exports effectues
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> ". _("Derniers exports");
$f->displaySubTitle($subtitle);
//
echo "<div id=\"prefecture_dematerialisation_liste_electorale_list_content\">\n";
//
displayListContent($f);
//
echo "</div>\n";
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

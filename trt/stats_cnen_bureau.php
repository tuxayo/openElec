<?php
/**
 * Statistiques des envois INSEE par bureau
 * Ce fichier est appelé par trt/insee.php => trt/insee_export.php
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"statistiques", _("Statistiques"));

// Recuperation des donnees
$data = array();

//
$inc = "stats_cnen_bureau";
include "../sql/".$f -> phptype."/".$inc.".inc";

// Récupération de tous les bureaux
$res = $f -> db -> query ($sql_bureau);
if (database::isError($res))
    die ($res -> getMessage ()." erreur sur ".$sql_bureau);

//
$totalavant=0;
$totaltrs=0;
$totalcnen=0;

$numArray = 0;
while ($row =& $res -> fetchRow (DB_FETCHMODE_ASSOC)) {
    //
    include "../sql/".$f -> phptype."/".$inc.".inc";
    //
    $avant =$f -> db->getOne($sqlB);
    if (database::isError($avant))
        die($avant->getMessage()." erreur sur ".$sqlB);
    //
    $trs =$f -> db->getOne($sqlT);
    if (database::isError($trs))
        die($trs->getMessage()." erreur sur ".$sqlT);
    //
    $cnen = $f -> db->getOne($sqlC);
    if (database::isError($cnen))
        die($cnen->getMessage()." erreur sur ".$sqlC);
    //
    $totalavant=$totalavant+$avant;
    $totaltrs=$totaltrs+$trs;
    $totalcnen=$totalcnen+$cnen;
    //
    $datas = array(
                   $row['code'],
                   $row['libelle_bureau'],
                   $avant,
                   $trs,
                   $cnen
                   );
    $data[$numArray] = $datas;
    $numArray++;
}

//
$data[$numArray] = array(
                        "-",
                        _("TOTAL"),
                        $totalavant,
                        $totaltrs,
                        $totalcnen
                        );
// Array
$tableau = array(
    "format" => "L",
    "title" => _("Statistiques"),
    "subtitle" => _("Statistiques des envois INSEE par bureau"),
    "offset" => array(10,0,60,30,60),
    "column" => array("",
                      _("LISTE")." : ".$_SESSION ['liste']." ".$_SESSION ['libelle_liste']."",
                      _("Mouvement non archives"),
                      _("Transmis"),
                      _("Nouveau Transfert INSEE")
                      ),
    "data" => $data,
    "output" => "stats-inseebureau"
    );

?>

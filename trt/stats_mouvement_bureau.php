<?php
/**
 * STATISTIQUES : Nombre d'inscription office (code insee 8), d'inscription
 * nouvelle (code insee 1), et de deces (code insee D) par bureau pour la
 * liste , la collectivite et la date de tableau en cours
 * 
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"statistiques", _("Statistiques"));

// Recuperation des donnees
$data = array();

// Identifiant de la statistique
$inc = "stats_mouvement_bureau";

//
if (!file_exists("../sql/".$f->phptype."/".$inc.".inc")) {
    $f->notExistsError();
}

//
$codes = array (
    0 => array ('titre' => _("Inscription D'office (8)"),
                'champ' => "codeinscription",
                'valeur' => "8"),
    1 => array ('titre' => _("Inscription Nouvelle (1)"),
                'champ' => "codeinscription",
                'valeur' => "1"),
    2 => array ('titre' => _("Deces (D)"),
                'champ' => "coderadiation",
                'valeur' => "D")
    );

//
include "../sql/".$f->phptype."/".$inc.".inc";

//
$res_select_bureau = $f->db->query($query_select_bureau);
$f->isDatabaseError($res_select_bureau);

//
$numArray = 0;
while ($row_select_bureau =& $res_select_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
    $datas = array();
    array_push($datas, $row_select_bureau['code']);
    //
    array_push($datas, $row_select_bureau['libelle_bureau']);
    //
    foreach ($codes as $c) {
        //
        include "../sql/".$f->phptype."/".$inc.".inc";
        //
        $res_count_mouvement = $f->db->getOne($query_count_mouvement);
        $f->isDatabaseError($res_count_mouvement);
        //
        array_push($datas, $res_count_mouvement);
    }
    //
    $data[$numArray] = $datas;
    $numArray++;
}
//
$offset = array();
$column = array();
array_push($offset, 10);
array_push($column, "");
array_push($offset, 70);
array_push($column, _("LISTE")." : ".$_SESSION ['liste']." ".$_SESSION ['libelle_liste']);
foreach ($codes as $c) {
    array_push($offset, 0);
    array_push($column, $c['titre']);
}
// Array
$tableau = array(
    "format" => "P",
    "title" => _("Statistiques - Mouvement par bureau"),
    "subtitle" => _("Details Inscription (Office,Nouvelle) et Deces."),
    "offset" => $offset,
    "column" => $column,
    "data" => $data,
    "output" => "stats-mouvementbureau"
    );

?>

<?php
/**
 * Ce fichier permet de faire le traitement d'export des mouvements
 * Il recupere les requetes sql dans le fichier: sql/.../trt_insee_export.inc
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils(
    "nohtml",
    "traitement_insee_export",
    _("Traitement")." -> "._("Export INSEE")
);

// Vérification du type d'appel
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 * Declaration d'une fonction permettant de lister les dix derniers exports
 * INSEE realises. La fonction est necessaire puisque ce code et uniquement
 * celui ci va etre appele en ajax pour recharger la liste des exports au
 * moment de la validation des traitements.
 *
 * @param om_application $f Handler de l'instance om_application.
 */
function displayListContent($f) {
    //
    $tab = array();
    //
    $dossier = opendir($f->getParameter("chemin_cnen"));
    //
    while ($entree = readdir($dossier)) {
        $regex = "/^".$_SESSION["liste"]."_tedeco/i";
        if (preg_match($regex, $entree)) {
            $exportNumber = "";
            $timing = preg_split("/_/", $entree);
            $timingDate = substr($timing[2], 4, 2).substr($timing[2], 2, 2).substr($timing[2], 0, 2);
            if (count($timing) == 5) {
                $timingHour = $timing[3];
                $exportNumber = intval(str_replace(".txt", "", $timing[4]));
            } elseif (count($timing) == 4) {
                $timingHour = str_replace(".txt", "", $timing[3]);
            }
            $timing = $timingDate.str_pad($timingHour, 6, 0, STR_PAD_LEFT);
            //
            $date = date("d/m/Y H:i:s", mktime(substr($timing, 6, 2), // [ int $hour = date("H") [,
                                       substr($timing, 8, 2), // int $minute = date("i") [,
                                       substr($timing, 10, 2), // int $second = date("s") [,
                                       substr($timing, 2, 2), // int $month = date("n") [,
                                       substr($timing, 4, 2), // int $day = date("j") [,
                                       substr($timing, 0, 2) // int $year = date("Y") [,
                                       ));
            //
            if (preg_match("/tedeco/i", $entree)) {
                $title = _("Export du")." ".$date." [".$exportNumber."]";
            }
            $explodeFileName = explode(".", $entree);
            array_push(
                $tab,
                array (
                    "date" => $timing,
                    "folder" => 'tmp',
                    "file" => $entree,
                    "ext" => array_pop($explodeFileName),
                    "title" => $title,
                )
            );
        }
    }
    //
    closedir($dossier);
    arsort($tab);
    //
    $links = array();
    //
    foreach ($tab as $key => $elem) {
        array_push(
            $links,
            array(
                "href" => "../app/file.php?fic=".$elem['file']."&amp;folder=".$elem['folder'],
                "target" => "_blank",
                "class" => "om-prev-icon file-16",
                "title" => $elem['title'],
            )
        );
        if (count($links) == 10) {
            break;
        }
    }
    //
    $f->displayLinksAsList($links);
}


/**
 *
 */
//
$display = "";
if (isset($_GET['display'])) {
    $display = $_GET['display'];
}
//
if ($display == "list_content") {
    displayListContent($f);
    die();
}

/**
 *
 */
//
$description = _("L'export INSEE vous permet de creer un fichier texte ".
                 "pour transferer a l'INSEE tous les mouvements concernant ".
                 "la date de tableau en cours parametres pour un export ".
                 "dans le parametrage des mouvements.");
$f->displayDescription($description);

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Editions des statistiques et des listings de mouvements");
$f->displaySubTitle($subtitle);
//
$links = array(
    "0" => array(
        "href" => "../pdf/pdffromarray.php?obj=cnen_bureau",
        "target" => "_blank",
        "class" => "om-prev-icon statistiques-16",
        "title" => _("Statistiques des envois INSEE par bureau"),
    ),
    "1" => array(
        "href" => "../pdf/pdf.php?obj=cnen_a_envoyer",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Listing des mouvements du prochain envoi a la date de tableau en cours"),
    ),
    "2" => array(
        "href" => "../pdf/pdf.php?obj=cnen_deja_envoyer",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Listing des mouvements deja envoyes a la date de tableau en cours"),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 * Affichage du formulaire de lancement du traitement
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> ". _("Generation du fichier Export INSEE");
$f->displaySubTitle($subtitle);
//
?>
<script type="text/javascript">
function trt_form_trigger_succes() {
    $("#insee_export_list_content").html(msg_loading);
    $.ajax({
       type: "GET",
       url: "../trt/insee_export.php?display=list_content",
       dataType: "html",
       error:function(msg){
         alert( "Error !: " + msg );
       },
       success:function(data){
            $("#insee_export_list_content").html(data);
    }});
}
</script>
<?php
//
require_once "../obj/traitement.insee_export.class.php";
$trt = new inseeExportTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 * Affichage des dix derniers exports INSEE effectues
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> ". _("Derniers exports");
$f->displaySubTitle($subtitle);
//
echo "<div id=\"insee_export_list_content\">\n";
//
displayListContent($f);
//
echo "</div>\n";
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

<?php
/**
 * Ce fichier permet ...
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_insee_export_test",
               _("Traitement")." -> "._("Export Test INSEE"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 * Declaration d'une fonction permettant de lister les dix derniers exports
 * INSEE realises. La fonction est necessaire puisque ce code et uniquement
 * celui ci va etre appele en ajax pour recharger la liste des exports au
 * moment de la validation des traitements.
 */
function displayListContent($f) {
    //
    $tab = array();
    //
    $dossier = opendir($f->getParameter("chemin_cnen"));
    //
    while ($entree = readdir($dossier)) {
        if (preg_match( "/cnen_verification_electeur/i" , $entree)
            OR preg_match( "/mecinsee/i" , $entree )) {

            if (preg_match( "/cnen_verification_electeur/i" , $entree)) {
                // Export de test
                $wk = explode("cnen_verification_electeur_", $entree);
                $wk = explode(".txt", $wk[1]);
                $wk = explode("_", $wk[0]);
                if (isset($wk[2])) {
                    continue;
                }
            } elseif (preg_match( "/mecinsee/i" , $entree )) {
                // Export MEC
                $wk = explode("mecinsee_", $entree);
                $wk = explode(".csv", $wk[1]);
                $wk = explode("_", $wk[0]);
            }
            //
            $timingDate = substr($wk[0], 4, 2).substr($wk[0], 2, 2).substr($wk[0], 0, 2);
            //
            $timing = $timingDate.str_pad($wk[1],6,0, STR_PAD_LEFT);
            //
            $date = date("d/m/Y H:i:s", mktime(substr($timing, 6, 2), // [ int $hour = date("H") [,
                                       substr($timing, 8, 2), // int $minute = date("i") [,
                                       substr($timing, 10, 2), // int $second = date("s") [,
                                       substr($timing, 2, 2), // int $month = date("n") [,
                                       substr($timing, 4, 2), // int $day = date("j") [,
                                       substr($timing, 0, 2) // int $year = date("Y") [,
                                       ));
            
            if (preg_match( "/cnen_verification_electeur/i" , $entree )) {
                $title = _("Export du")." ".$date;
            } elseif (preg_match( "/mecinsee/i" , $entree )) {
                $title = _("Export Mec du")." ".$date;
            }

            array_push($tab,
                array (
                    "date" => $timing,
                    "folder" => 'tmp',
                    "file" => $entree,
                    "ext" => array_pop(explode(".", $entree)),
                    "title" => $title
                )
            );
        }
    }
    //
    closedir($dossier);
    arsort($tab);
    //
    $links = array();
    //
    foreach ($tab as $key => $elem) {
        array_push($links, array(
            "href" => "../app/file.php?fic=".$elem['file']."&amp;folder=".$elem['folder'],
            "target" => "_blank",
            "class" => "om-prev-icon file-16",
            "title" => $elem['title'],));
        if (count($links) == 10) {
            break;
        }
    }
    //
    $f->displayLinksAsList($links);
}


/**
 *
 */
//
$display = "";
if (isset($_GET['display'])) {
    $display = $_GET['display'];
}
//
if ($display == "list_content") {
    displayListContent($f);
    die();
}

/**
 *
 */
//
$description = _("L'export INSEE TEST vous permet de verifier le parametrage ".
                 "de l'application pour l'export INSEE.");
$f->displayDescription($description);

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Parametrage du code expediteur INSEE et du code INSEE");
$f->displaySubTitle($subtitle);
//
$links = array(
    "0" => array(
        "href" => "../scr/form.php?obj=collectivite&amp;idx=".$_SESSION['collectivite'],
        "class" => "om-prev-icon collectivite-16",
        "title" => _("Formulaire de parametrage de la collectivite"),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
//
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\" id=\"edit_partenaire\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Parametrage des partenaires pour l'export INSEE au format XML");
$f->displaySubTitle($subtitle);
//
$links = array(
    "0" => array(
        "href" => "../scr/tab.php?obj=xml_partenaire",
        "class" => "om-prev-icon parametrage-16",
        "title" => _("Formulaire de parametrage des partenaires pour l'export INSEE XML"),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";
//

/**
 * Affichage du formulaire de lancement du traitement
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> ". _("Generation du fichier de verification de la table electeur pour l'INSEE");
$f->displaySubTitle($subtitle);
//
?>
<script type="text/javascript">
function trt_form_trigger_succes() {
    $("#insee_export_list_content").html(msg_loading);
    $.ajax({
       type: "GET",
       url: "../trt/insee_export_test.php?display=list_content",
       dataType: "html",
       error:function(msg){
         alert( "Error !: " + msg );
       },
       success:function(data){
            $("#insee_export_test_list_content").html(data);
    }});
}
</script>
<?php
//
require_once "../obj/traitement.insee_export_test.class.php";
$trt = new inseeExportTestTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";


/**
 * Affichage du formulaire de lancement du traitement d'export du fichier
 * de mise en concordance
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> ". _("Generation du fichier Export mise en concordance pour l'INSEE");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.mec_insee_export.class.php";
$trt = new mecInseeExportTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";


/**
 * Affichage des dix derniers exports INSEE effectues
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> ". _("Derniers exports");
$f->displaySubTitle($subtitle);
//
echo "<div id=\"insee_export_test_list_content\">\n";
//
displayListContent($f);
//
echo "</div>\n";
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

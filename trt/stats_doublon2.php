<?php
/**
 * Statistique inscription doublon 2
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"statistiques", _("Statistiques"));

// Recuperation des donnees
$data = array();

//
$inc = "stats_doublon2";

include "../sql/".$f -> phptype."/".$inc.".inc";

$res = $f -> db -> query ($sql);
if (database::isError($res))
    die ("Erreur requete ".$res -> getMessage()." [".$sql."].");

if ($res -> numRows() == 0) {
    $datas = array(
                    _("Aucun"),
                    "",
                    "",
                    "",
                    );
    $data[0] = $datas;
} else {
    $numArray = 0;
    while ($row =& $res -> fetchRow (DB_FETCHMODE_ASSOC)) {
        $datas = array(
                        $row['nom'],
                        $row['prenom'],
                        $row['naissance'],
                        $row['code_bureau'],
                        );
        $data[$numArray] = $datas;
        $numArray++;
    }
}

// Array
$tableau = array(
    "format" => "P",
    "title" => _("Statistiques - Doublon"),
    "subtitle" => _("Doublon (nom + date de naissance) entre table electeur et mouvement"),
    "column" => array(_("Nom"),
                      _("Prenom(s)"),
                      _("Naissance"),
                      _("Bureau")
                      ),
    "data" => $data,
    "output" => "stats-doublon2"
    );

?>

<?php
/**
 *
 */

/**
 *
 */
require_once("../obj/utils.class.php");

//// Class chiffre en lettre
include ("../app/php/misc/chiffreenlettre.class.php");
$lettre=new ChiffreEnLettre();

// new utils
$f = new utils ("nohtml", "facturation");

//
set_time_limit (480);
$aujourdhui = date("d/m/Y");



$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];


//===========================================================================
include ("../sql/".$f -> phptype."/facturation-publipostage.inc");
//===========================================================================


//// Recupere l'entier d'un nombre decimal sans arrondir
function decompose($somme)
{
    $pos = array("","");
    $virgule = 0;
    for($cp=0;$cp<strlen($somme);$cp++)
    {
        $lettre = substr($somme,$cp,1);
        if($lettre=="."){ $virgule = 1; $lettre=""; }
        $pos[$virgule] .= $lettre;
    }
    return $pos;
}


// Communes

$commune = array ();

$rescom =& $f -> db -> query ($sqlcom);
$f->isDatabaseError($rescom);

while ($rowcom =& $rescom -> fetchRow (DB_FETCHMODE_ASSOC))
	array_push ($commune, $rowcom);

$periode = intval(substr($f -> collectivite ['datetableau'],0,4))-2;

$nom_fichier="factot_".$periode.".csv";
$fic = fopen ($f->getParameter("tmpdir").$nom_fichier,"w");

///// Création du fichier cvs
$lg_xls = "COMMUNE;ADRESSE;BP;INSEE COMMUNE;TITRE;MAIRE 2;MAIRE;TITRE DE CIVILITE;Periode;Electeurs;Px unitaire;Px total;Somme\n";
fwrite ($fic, $lg_xls);



foreach ($commune as $c)
{
    ////////////////////////////////////////////////////////////////////////////////////////////
	$sommeDue = $f->getParameter("fact_electeur_unitaire") * $c['total'];
	if( $sommeDue < $f->getParameter("fact_forfait") ) $sommeDue = $f->getParameter("fact_forfait");
        $sommeDue = number_format($sommeDue,2,'.','');
        $lg_xls = $c['ville'].";";
        $lg_xls .= $c['ad1'].";";               
        $lg_xls .= $c['ad2'].";";               
        $lg_xls .= $c['inseeville'].";";               
        $lg_xls .= $c['civilite'].";";               
        $lg_xls .= $c['maire_de'].";";               
        $lg_xls .= $c['maire'].";";               
        $lg_xls .= $c['civilite']." le maire;";               
        $lg_xls .= $periode.";";               
        $lg_xls .= $c['total'].";";               
        $lg_xls .= str_replace(".",",",$f->getParameter("fact_electeur_unitaire")).";";               
        $lg_xls .= str_replace(".",",",$sommeDue).";";
        $somme = decompose($sommeDue);
        $lettre->init();
        $lg_xls .= $lettre->Conversion($somme[0])."EURO";
        ($somme[1])? $lg_xls.=" ".$somme[1]." CENTIMES\n" : $lg_xls.=";\n";
        $lg_xls = utf8_encode($lg_xls);
        fwrite ($fic, $lg_xls);
}


fclose ($fic);

$msg = "Le fichier a été exporté, vous pouvez l'ouvrir immédiatement en cliquant sur : ";
$msg .= "<a target=\"_blank\" href=\"../app/file.php?fic=".$nom_fichier."&amp;folder=tmp\" rel=\"facebox\"><img src=\"../img/voir.png\" alt=\"Fichier export\" title=\"Fichier export\" /></a>.<br />";
$msg .= "Il est également accessible dans les traces, son nom est : \"".$nom_fichier."\".<br />";
echo $msg;



?>
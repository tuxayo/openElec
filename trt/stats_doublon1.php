<?php
/**
 * Statistique inscription doublon 1
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"statistiques", _("Statistiques"));

// Recuperation des donnees
$data = array();

//
$inc = "stats_doublon1";

$nom = "";
if (isset ($_GET ['nom']))
    $nom = $_GET ['nom'];
$prenom = "";
if (isset ($_GET ['prenom']))
    $prenom = $_GET ['prenom'];
$datenaissance = "";
if (isset ($_GET ['datenaissance']))
    $datenaissance = $_GET ['datenaissance'];
$marital = "";
if (isset ($_GET ['marital']))
    $marital = $_GET ['marital'];
$correct = true;
if ($nom == "" && $prenom == "" && $marital == "" && $datenaissance == "")
    $correct = false;

if ($correct == true) {
    //
    include "../sql/".$f -> phptype."/".$inc.".inc";
    //
    $res = $f -> db -> query ($sql);
    if (database::isError($res))
        die ("Erreur requete ".$res -> getMessage()." [".$sql."].");
        
    $resM = $f -> db -> query ($sqli);
    if (database::isError($resM))
        die ("Erreur requete ".$resM -> getMessage()." [".$sqli."].");
            
    if ($resM -> numRows() == 0) {
        $datas = array(
                        _("Aucun"),
                        "",
                        "",
                        "",
                        );
        $data[0] = $datas;
    } else {
        $numArray = 0;
        while ($row1 =& $resM -> fetchRow (DB_FETCHMODE_ASSOC)) {
            $datas = array(
                            $row1['nom'],
                            $row1['prenom'],
                            $row1['nom_usage'],
                            $row1['naissance'],
                            );
            $data[$numArray] = $datas;
            $numArray++;
        }
    }
    
    // Array
    $tableau = array(
        "format" => "P",
        "title" => _("Statistiques - Doublon"),
        "subtitle" => _("Electeur(s) dans la liste electorale"),
        "column" => array(_("Nom"),
                          _("Prenom(s)"),
                          _("Marital"),
                          _("Ne(e) le")
                          ),
        "data" => $data,
        "output" => "null"
        );
    
    //
    $data = array();
    
    if ($res -> numRows() == 0) {
        $datas = array(
                        _("Aucun"),
                        "",
                        "",
                        "",
                        );
        $data[0] = $datas;
    } else {
        $numArray = 0;
        while ($row2 =& $res -> fetchRow (DB_FETCHMODE_ASSOC)) {
            $datas = array(
                            $row2['nom'],
                            $row2['prenom'],
                            $row2['nom_usage'],
                            $row2['naissance'],
                            );
            $data[$numArray] = $datas;
            $numArray++;
        }
    }
    
    // Array
    $tableau2 = array(
        "format" => "P",
        "title" => _("Statistiques - Doublon"),
        "subtitle" => _("Electeur(s) en mouvement"),
        "column" => array(_("Nom"),
                          _("Prenom(s)"),
                          _("Marital"),
                          _("Ne(e) le")
                          ),
        "data" => $data,
        "output" => "stats-doublon1"
        );
    
} else {
    echo "Vous devez remplir un des champs : nom, prénom, nom_usage, date de naissance.";
}

?>

<?php
/**
 * Ce fichier permet de gerer le module TRT annuel. Il affiche
 * differents onglets contenant chacun un traitement en relation avec
 * le traitement annuel.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"module_traitement_annuel",
               _("Traitement")." -> "._("Module Traitement annuel"));

/**
 * Affichage de la structure de la page
 */
// Affichage
$f->setFlag(NULL);
$f->display();

/**
 * ONGLETS
 */
// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n";
// Affichage de la liste des onglets
echo "\n<ul>\n";
//
if ($f->isAccredited(/*DROIT*/"traitement_annuel")) {
    echo "\t<li><a href=\"../trt/traitement_annuel.php\">"._("Traitement annuel")."</a></li>\n";
}
//
echo "</ul>\n";
// Fermeture de la balise - Conteneur d'onglets
echo "</div>";

?>

<?php
/**
 * STATISTIQUES : Nombre inscription office/nouvelle, et deces par voie
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"statistiques", _("Statistiques"));

// STATISTIQUES : Nombre d'inscription office (code insee 8), d'inscription
//  nouvelle (code insee 1), et de deces (code insee D) par voie pour la liste et la date de tableau en cours
$inc = "stats_mouvement_voie";

// Recuperation des donnees
$data = array();

//
include "../sql/".$f -> phptype."/".$inc.".inc";

$res = $f->db->query ($sql_voie);
$f->isDatabaseError($res);

$codes = array (
    0 => array ('champ' => "codeinscription", 'valeur' => "8"),
    1 => array ('champ' => "codeinscription", 'valeur' => "1"),
    2 => array ('champ' => "coderadiation", 'valeur' => "D")
    );

//
$numArray = 0;
while ($row =& $res->fetchRow (DB_FETCHMODE_ASSOC)) {
    //
    $datas = array();
    //
    array_push($datas, $row['code']);
    array_push($datas, $row['libelle_voie']);
    //
    foreach ($codes as $c) {
        include "../sql/".$f -> phptype."/".$inc.".inc";
        $res1 = $f -> db -> getOne ($sql1);
        $f->isDatabaseError($res1);
        array_push($datas, $res1);
    }
    //
    $data[$numArray] = $datas;
    $numArray++;
}
// Array
$tableau = array(
    "format" => "L",
    "title" => _("Statistiques - Mouvement par voie"),
    "subtitle" => _("Details Inscription (Office,Nouvelle) et Deces."),
    "offset" => array(10,0,50,50,40),
    "column" => array("",
                      _("LISTE")." : ".$_SESSION ['liste']." ".$_SESSION ['libelle_liste']."",
                      _("Inscription D'office (8)"),
                      _("Inscription Nouvelle (1)"),
                      _("Deces (D)")
                      ),
    "data" => $data,
    "output" => "stats-mouvementvoie"
    );

?>

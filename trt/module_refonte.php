<?php
/**
 * Ce fichier permet de gerer le module Refonte de l'application. Il affiche
 * differents onglets contenant chacun un traitement en relation avec
 * la refonte de la liste electorale.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"module_refonte",
               _("Traitement")." -> "._("Module Refonte"));

/**
 * Affichage de la structure de la page
 */
// Affichage
$f->setFlag(NULL);
$f->display();

/**
 * ONGLETS
 */
// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n";
// Affichage de la liste des onglets
echo "\n<ul>\n";
//
if ($f->isAccredited(/*DROIT*/"traitement_refonte")) {
    echo "\t<li><a href=\"../trt/refonte.php\">"._("Refonte")."</a></li>\n";
}
//
echo "</ul>\n";
// Fermeture de la balise - Conteneur d'onglets
echo "</div>";

?>

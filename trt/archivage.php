<?php
/**
 * Ce fichier permet de faire le traitement d'archivage des mouvements.
 * Il recupere les requetes sql dans le fichier: sql/.../trt_archivage.inc
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_archivage",
               _("Traitement")." -> "._("Archivage"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
//
$description = _("L'archivage permet de supprimer les mouvements des ecrans ".
                 "de consultation. Ils sont deplaces vers une table archive ".
                 "qu'il est possible de consulter par le menu \"Consultation ".
                 "-> Archive\". Ce traitement supprime les mouvements de la ".
                 "date de tableau en cours les archive.");
$f->displayDescription($description);

/**
 *
 */
//
$datetableau = $f->collectivite['datetableau'];
$datetableau_show = $f->formatDate ($f->collectivite['datetableau']);
//
include ("../sql/".$f->phptype."/trt_archivage.inc");
//
$nbCnen = $f->db->getone($query_count_mouvement);
// Logger
$f->addToLog("trt/archivage.php: db->getone(\".$query_count_mouvement.\")", VERBOSE_MODE);
// 
$f->isDatabaseError($nbCnen);
// Si il y a des mouvements non transmis a l'insee ou si la date du jour est
// inferieure a la date de tableau
if ($nbCnen > 0 || time () < strtotime ($datetableau)) {
    //
    if ($nbCnen > 0) {
        $f->displayMessage("error", $nbCnen." "._("mouvement(s) a archiver n'ont pas ete transmis a l'INSEE"));
    }
    //
    if (time () < strtotime ($datetableau)) {
        $f->displayMessage("error", _("La commission du tableau du")." ".$datetableau_show." "._("n'est pas effectuee"));
    }
}

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Editions des statistiques et des listings de mouvements");
$f->displaySubTitle($subtitle);
//
$links = array(
    "0" => array(
        "href" => "../pdf/pdffromarray.php?obj=archivage_bureau",
        "target" => "_blank",
        "class" => "om-prev-icon statistiques-16",
        "title" => _("Statistiques des mouvements"),
    ),
    "1" => array(
        "href" => "../pdf/pdf.php?obj=archivage",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Listing des mouvements a archiver a la date de tableau en cours"),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 * Affichage du formulaire de lancement du traitement
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> ". _("Archivage des mouvements");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.archivage.class.php";
$trt = new archivageTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

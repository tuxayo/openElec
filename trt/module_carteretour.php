<?php
/**
 * Ce fichier permet de gerer le module Carte en retour de l'application. Il
 * affiche differents onglets contenant chacun un traitement en relation avec
 * les cartes retour.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"module_carteretour",
               _("Traitement")." -> "._("Module Carte en retour"));

/**
 * Affichage de la structure de la page
 */
// Affichage
$f->setFlag(NULL);
$f->display();

/**
 *
 */
//
$description = _("Les cartes en retour peuvent etre gerees par le logiciel ".
                 "pour repertorier la liste des cartes d'electeurs retournees ".
                 "par la poste. Vous pouvez aussi modifier manuellement les ".
                 "informations de cartes en retour d'un electeur au moyen du ".
                 "menu \"Saisie -> Carte en retour / Jure\".");
$f->displayDescription($description);

/**
 * ONGLETS
 */
// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n";
// Affichage de la liste des onglets
echo "\n<ul>\n";
//
if ($f->isAccredited(/*DROIT*/"traitement_carteretour")) {
    echo "\t<li><a href=\"../trt/carteretour.php\">"._("Saisie par lots")."</a></li>\n";
}
//
if ($f->isAccredited(/*DROIT*/"traitement_carteretour")) {
    echo "\t<li><a href=\"../scr/soustab.php?obj=electeur_carteretour&amp;retourformulaire=module_carteretour&amp;idxformulaire=module_carteretour\">"._("Liste des electeurs")."</a></li>\n";
}
//
if ($f->isAccredited(/*DROIT*/"traitement_carteretour_epuration")) {
    echo "\t<li><a href=\"../trt/carteretour_epuration.php\">"._("Epuration")."</a></li>\n";
}
//
echo "</ul>\n";
// Fermeture de la balise - Conteneur d'onglets
echo "</div>";

?>

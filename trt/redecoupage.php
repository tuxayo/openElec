<?php
/**
 * Ce fichier permet de ...
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_redecoupage",
               _("Traitement")." -> "._("Redecoupage electoral"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Redecoupage electoral");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.redecoupage.class.php";
$trt = new redecoupageTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

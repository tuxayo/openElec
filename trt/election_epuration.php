<?php
/**
 * Ce fichier permet de ...
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_election_epuration",
               _("Traitement")." -> "._("Epuration des procurations"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
//
$description = _("L'epuration signifit la suppression des enregistrements ".
                 "dans la base de donnees. Ce traitement doit etre realise ".
                 "avec precaution.");
$f->displayDescription($description);

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Epuration des procurations");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.election_epuration_procuration.class.php";
$trt = new electionEpurationProcurationTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

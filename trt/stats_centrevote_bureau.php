<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"statistiques", _("Statistiques"));

// Recuperation des donnees
$data = array();

/**
 * STATISTIQUES : Nombre d'inscription office (code insee 8), d'inscription
 * nouvelle (code insee 1), et de décés (code insee D) par voie pour la liste
 * et la date de tableau en cours
 */
$inc = "stats_centrevote_bureau";

/**
 *
 */
//
$correct = true;
//
$dateelection1 = NULL;
if (isset($_GET['dateelection1'])) {
    //
    $date = explode("/", $_GET['dateelection1']);
    if (sizeof($date) == 3 and (checkdate($date[1], $date[0], $date[2]))) {
        $dateelection1 = $date[2]."-".$date[1]."-".$date[0];
    }
}
//
$dateelection2 = NULL;
if (isset($_GET['dateelection2'])) {
    //
    $date = explode("/", $_GET['dateelection2']);
    if (sizeof($date) == 3 and (checkdate($date[1], $date[0], $date[2]))) {
        $dateelection2 = $date[2]."-".$date[1]."-".$date[0];
    }
}
//
if ($dateelection1 == NULL && $dateelection2 == NULL) {
    //
    $dateelection1 = date('Y-m-d');
}
$cv_checked = false;
if (isset ($_GET ['centrevote']) and $_GET ['centrevote'] == "true")
    $cv_checked = true;
$me_checked = false;
if (isset ($_GET ['mairieeurope']) and $_GET ['mairieeurope'] == "true")
    $me_checked = true;

//
if ($correct == true) {

    include ("../sql/".$f -> phptype."/".$inc.".inc");
    $res = $f -> db -> query ($sql_bureau);
    
    if (database::isError($res))
        die ($res -> getMessage ()." erreur sur ".$sql_bureau);

    //
    $totalavant=0;
    $totalcentrevote=0;
    $totalmairieeurope=0;
    $totalapres1=0;
    
    //
    $numArray = 0;
    while ($row =& $res -> fetchRow (DB_FETCHMODE_ASSOC)) {
        include ("../sql/".$f -> phptype."/".$inc.".inc");
        //
        $avant = $f -> db -> getOne ($sqlB);
        if (database::isError($avant))
            die ($avant -> getMessage ()." erreur sur ".$sqlB);
        $totalavant=$totalavant+$avant;
        //
        $centrevote = 0;
        if ($cv_checked == true) {
            // centre vote
            $centrevote = $f -> db -> getOne ($sqlCV);
            if (database::isError($centrevote))
                die ($centrevote -> getMessage ()." erreur sur ".$sqlCV);
            $totalcentrevote=$totalcentrevote+$centrevote;
        }
        //
        $mairieeurope = 0;
        if ($me_checked == true) {
            // mairieeurope
            $mairieeurope = $f -> db -> getOne ($sqlME);
            if (database::isError($mairieeurope))
                die ($mairieeurope -> getMessage ()." erreur sur ".$sqlME);
            $totalmairieeurope=$totalmairieeurope+$mairieeurope;
        }
        //
        $apres1 = $avant - $centrevote - $mairieeurope;
        //
        $totalapres1=$totalapres1+$apres1;
        //
        $datas = array();
        array_push($datas,$row['code']." - ".$row['libelle_bureau']);
        array_push($datas,$avant);
        if ($cv_checked == true) {
            array_push($datas,$centrevote);
        }
        if ($me_checked == true) {
            array_push($datas,$mairieeurope);
        }
        array_push($datas,$apres1);
        $data[$numArray] = $datas;
        $numArray++;
    }
    //
    $datas = array();
    array_push($datas,_("TOTAL"));
    array_push($datas,$totalavant);
    if ($cv_checked == true) {
        array_push($datas,$totalcentrevote);
    }
    if ($me_checked == true) {
        array_push($datas,$totalmairieeurope);
    }
    array_push($datas,$totalapres1);
    $data[$numArray] = $datas;
    // column/offset Array
    $column = array();
    $offset = array();
    $align = array();
    array_push($column,_("LISTE")." : ".$_SESSION ['liste']." ".$_SESSION ['libelle_liste']."");
    array_push($offset,0);
    array_push($align,"L");
    array_push($column,_("Electeur(s) au").date("d/m/Y"));
    array_push($offset,40);
    array_push($align,"R");
    if ($cv_checked == true) {
        array_push($column,_("Centre Vote"));
        array_push($offset,20);
        array_push($align,"R");
    }
    if ($me_checked == true) {
        array_push($column,_("Mairie Europe"));
        array_push($offset,20);
        array_push($align,"R");
    }
    array_push($column,_("Electeur(s) apres mentions"));
    array_push($offset,40);
    array_push($align,"R");
    //
    $subtitle = _("Detail des mentions (Centre Vote et Mairie Europe) valides le");
    //
    if ($dateelection1 == NULL) {
        $subtitle .= " ".$f->formatdate($dateelection2);
    } else {
        $subtitle .= " ".$f->formatdate($dateelection1);
    }
    // Array
    $tableau = array(
        "format" => "L",
        "title" => $subtitle,
        "offset" => $offset,
        "align" => $align,
        "column" => $column,
        "data" => $data,
        "output" => "stats-centrevotebureau"
        );
} else {
    echo "Erreur de date.";
}

?>

<?php
/**
 * Ce fichier permet de gerer le module Jury de l'application. Il affiche
 * differents onglets contenant chacun un traitement en relation avec les
 * electeurs en jury.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils(
    NULL,
    /*DROIT*/"module_prefecture",
    _("Traitement")." -> "._("Module Prefecture")
);

/**
 * Affichage de la structure de la page
 */
// Affichage
$f->setFlag(NULL);
$f->display();

/**
 *
 */
//
$description = _(
	"Ce module permet de gerer les echanges dematerialises de donnees".
	" electorales entre les communes et les prefectures."
);
$f->displayDescription($description);

/**
 * ONGLETS
 */
// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n";
// Affichage de la liste des onglets
echo "\n<ul>\n";
//
if ($f->isAccredited(/*DROIT*/"prefecture_dematerialisation_liste_electorale")) {
    echo "\t<li><a href=\"../trt/prefecture_dematerialisation_liste_electorale.php\">"._("Liste Electorale")."</a></li>\n";
}
//
if ($f->isAccredited(/*DROIT*/"prefecture_dematerialisation_tableau")) {
    echo "\t<li><a href=\"../trt/prefecture_dematerialisation_tableau.php\">"._("Tableau")."</a></li>\n";
}
//
echo "</ul>\n";
// Fermeture de la balise - Conteneur d'onglets
echo "</div>";

?>

<?php
/**
 * Ce fichier permet de gerer le module INSEE de l'application. Il affiche
 * differents onglets contenant chacun un traitement en relation avec
 * l'INSEE.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"module_insee",
               _("Traitement")." -> "._("Module INSEE"));

/**
 * Affichage de la structure de la page
 */
// Affichage
$f->setFlag(NULL);
$f->display();

/**
 * ONGLETS
 */
// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n";
// Affichage de la liste des onglets
echo "\n<ul>\n";
//
if ($f->isAccredited(/*DROIT*/"traitement_insee_export")) {
    echo "\t<li><a href=\"../trt/insee_export.php\">"._("Export")."</a></li>\n";
}
//
if ($f->isAccredited(/*DROIT*/"traitement_insee_inscription_office") && $_SESSION['liste'] === "01") {
    echo "\t<li><a href=\"../trt/insee_inscription_office.php\">"._("Import IO")."</a></li>\n";
}
//
if ($f->isAccredited(/*DROIT*/"traitement_insee_radiation")) {
    echo "\t<li><a href=\"../trt/insee_radiation.php\">"._("Import Radiation")."</a></li>\n";
}
//
if ($f->isAccredited(/*DROIT*/"traitement_insee_export_test")) {
    echo "\t<li><a href=\"../trt/insee_export_test.php\">"._("Test")."</a></li>\n";
}
//
if ($f->isAccredited(/*DROIT*/"traitement_insee_epuration")) {
    echo "\t<li><a href=\"../trt/insee_epuration.php\">"._("Epuration")."</a></li>\n";
}
//
echo "</ul>\n";
// Fermeture de la balise - Conteneur d'onglets
echo "</div>";

?>

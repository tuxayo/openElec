<?php
/**
 * Ce fichier permet de rédiger le message de convocation
 * des membres aux commissions.
 * 
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_commission",
               _("Traitement")." -> "._("Commissions"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
//
$description = _("Le message redige sera le contenu du courrier de convocation a".
                " la commission envoye aux membres.");
$f->displayDescription($description);

/**
 * Rédaction du message de convocation à la commission
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Edition du message de convocation a la commission");
$f->displaySubTitle($subtitle);
// Vérification du nombre de membre participant àla commission
$sql_nb_membre = "SELECT count(*) FROM membre_commission
                        WHERE collectivite = '".$_SESSION["collectivite"]."'";
$nb_membre = $f->db->getOne($sql_nb_membre);
$f->isDatabaseError($nb_membre);
if($nb_membre == 0) {
    $f->displayMessage("error", _("Veuillez ajouter les membres participant a la commission (onglet \"Membres\")."));
} else {
    //
    (defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
    require_once PATH_OPENMAIRIE."formulairedyn.class.php";
    //
    echo "<form name=\"courriercommission\"
                id=\"courriercommission\"
                action=\"../pdf/convocationcommission.php\"
                method=\"post\"
                onsubmit=\"return checkformconvocationcommission()\"
                target=\"_blank\">";
    //
    $champs = array("message");
    //
    $form = new formulaire(NULL, 0, 0, $champs);
    //
    $form->setLib("message", _("Message"));
    $form->setType("message", "textarea");
    $form->setVal("message", _("le ... a ..."));
    $form->setTaille("message", 80);
    $form->setMax("message", 5);
    //
    $form->entete();
    $form->afficher($champs, 0, false, false);
    $form->enpied();
    //
    echo "<div class=\"formControls\">\n";

    echo "<input type=\"submit\" value=\""._("Telecharger l'edition des courriers de convocation")."\" ";

    echo "</div>\n";
    //
    echo "</form>\n";
}
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

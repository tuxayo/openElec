<?php
/**
 * STATISTIQUES : Nombre d'electeurs, par tranche d'age par bureau pour la
 * liste en cours et pour la collectivite en cours
 * 
 * @todo A quoi sert la colonne "Ecart" ?
 * @todo N'est-il pas plus pertinent de calculer l'age en fonction du 1er mars
 * de l'annee de la date de tableau en cours ?
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"statistiques", _("Statistiques"));

// Recuperation des donnees
$data = array();

// Identifiant de la statistique
$inc = "stats_electeur_age_bureau";

//
if (!file_exists("../sql/".$f->phptype."/".$inc.".inc")) {
    $f->notExistsError();
}

// Definition des tranches d'age
$tranche = array (
    0 => array ('titre' => _("18 ans"), 'debut' => 18, 'fin' => 18, 'res' => 0),
    1 => array ('titre' => _("19-21"), 'debut' => 19, 'fin' => 21, 'res' => 0),
    2 => array ('titre' => _("22-30"), 'debut' => 22, 'fin' => 30, 'res' => 0),
    3 => array ('titre' => _("31-40"), 'debut' => 31, 'fin' => 40, 'res' => 0),
    4 => array ('titre' => _("41-50"), 'debut' => 41, 'fin' => 50, 'res' => 0),
    5 => array ('titre' => _("51-60"), 'debut' => 51, 'fin' => 60, 'res' => 0),
    6 => array ('titre' => _("61-70"), 'debut' => 61, 'fin' => 70, 'res' => 0),
    7 => array ('titre' => _("71-80"), 'debut' => 71, 'fin' => 80, 'res' => 0),
    8 => array ('titre' => _("81-90"), 'debut' => 81, 'fin' => 90, 'res' => 0),
    9 => array ('titre' => _("91+"), 'debut' => 91, 'fin' => 150, 'res' => 0)
);

//
$total=0;
$totalecart=0;

//
include "../sql/".$f->phptype."/".$inc.".inc";

//
$res_select_bureau = $f->db->query($query_select_bureau);
$f->isDatabaseError($res_select_bureau);

//
$numArray = 0;
while ($row_select_bureau =& $res_select_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
    
    //
    $datas = array();
    
    array_push($datas, $row_select_bureau['code']);
    array_push($datas, $row_select_bureau['libelle_bureau']);

    //
    include "../sql/".$f->phptype."/".$inc.".inc";
    
    //
    $res_count_electeur_bureau = $f->db->getOne($query_count_electeur_bureau);
    $f->isDatabaseError($res_count_electeur_bureau);
    
    array_push($datas, $res_count_electeur_bureau);
    
    //
    $total += $res_count_electeur_bureau;
    
    //
    $ecart = 0;
    
    //
    foreach ($tranche as $key => $t) {
        //
        include "../sql/".$f->phptype."/".$inc.".inc";
        //
        $res_count_electeur_bureau_age = $f->db->getOne($query_count_electeur_bureau_age);
        $f->isDatabaseError($res_count_electeur_bureau_age);
        //
        array_push($datas, $res_count_electeur_bureau_age);
        //
        $tranche [$key]['res'] += $res_count_electeur_bureau_age;
        $ecart += $res_count_electeur_bureau_age; 
    }
    
    //
    array_push($datas, $ecart);
    
    //
    $totalecart += $ecart;
    
    //
    $data[$numArray] = $datas;
    $numArray++;
}

$datas = array();
array_push($datas, "-");
array_push($datas, _("TOTAL"));
array_push($datas, $total);
foreach ($tranche as $t) {
    array_push($datas, $t['res']);
}
array_push($datas, $totalecart);
$data[$numArray] = $datas;

$column = array();
array_push($column, "");
array_push($column, _("LISTE")." : ".$_SESSION ['liste']." ".$_SESSION ['libelle_liste']);
array_push($column, _("Total"));
foreach ($tranche as $t) {
    array_push($column, $t['titre']);
}
array_push($column, _("Ecart"));

$offset = array();
array_push($offset, 10);
array_push($offset, 0);
array_push($offset, 11);
foreach ($tranche as $t) {
    array_push($offset, 11);
}
array_push($offset, 11);

// Array
$tableau = array(
    "format" => "L",
    "title" => _("Statistiques"),
    "subtitle" => _("Nombre d'electeurs, par tranche d'age par bureau pour la liste en cours"),
    "offset" => $offset,
    "column" => $column,
    "data" => $data,
    "output" => "stats-electeuragebureau"
    );

?>

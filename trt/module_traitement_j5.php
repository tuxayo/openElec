<?php
/**
 * Ce fichier permet de gerer le module TRT j5. Il affiche
 * differents onglets contenant chacun un traitement en relation avec
 * le traitement j5.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"module_traitement_j5",
               _("Traitement")." -> "._("Module Traitement J-5"));

/**
 * Affichage de la structure de la page
 */
// Affichage
$f->setFlag(NULL);
$f->display();

/**
 * ONGLETS
 */
// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n";
// Affichage de la liste des onglets
echo "\n<ul>\n";
//
if ($f->isAccredited(/*DROIT*/"traitement_j5")) {
    echo "\t<li><a href=\"../trt/traitement_j5.php\">"._("Traitement j5")."</a></li>\n";
}
//
echo "</ul>\n";
// Fermeture de la balise - Conteneur d'onglets
echo "</div>";

?>

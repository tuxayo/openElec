<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";

// variable $obj
$obj = "statistiques";

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"statistiques", _("Statistiques"));

// Recuperation des donnees
$data = array();

// STATISTIQUES : Nombre d'electeurs, d'hommes, de femmes par département
// (ou pays) de naissance pour la liste en cours
$inc = "stats_electeur_sexe_departementnaissance";

//
include ("../sql/".$f -> phptype."/".$inc.".inc");

$res = $f -> db -> query ($sql_departement);
if (database::isError($res))
    die($res->getMessage()." erreur sur ".$sql_departement);

//
$total = 0;
$totalres1 = 0;
$totalres2 = 0;

//
$numArray = 0;
while ($row =& $res -> fetchRow (DB_FETCHMODE_ASSOC)) {
    include "../sql/".$f -> phptype."/".$inc.".inc";
    // Total
    $res0 = $f -> db -> getOne ($sqlB);
    if (database::isError($res0))
        die($res0->getMessage()." erreur sur ".$sqlB);
    $total += $res0;
    // Hommes
    $res1 = $f -> db -> getOne ($sql1);
    if (database::isError($res1))
        die($res1->getMessage()." erreur sur ".$sql1);
    $totalres1 += $res1;
    // Femmes
    $res2 = $f -> db -> getOne ($sql2);
    if (database::isError($res2))
        die ($res1->getMessage()." erreur sur ".$sql2);
    $totalres2 += $res2;
    //
    $datas = array(
                   $row['code'],
                   $row['libelle_departement'],
                   $res0,
                   $res1,
                   $res2
                   );
    $data[$numArray] = $datas;
    $numArray++;
}

// Total
$res0 = $f -> db -> getOne ($sqlB_a);
if (database::isError($res0))
    die($res0->getMessage()." erreur sur ".$sqlB_a);
$total += $res0;
// Hommes
$res1 = $f -> db -> getOne ($sql1_a);
if (database::isError($res1))
    die($res1->getMessage()." erreur sur ".$sql1_a);
$totalres1 += $res1;
// Femmes
$res2 = $f -> db -> getOne ($sql2_a);
if (database::isError($res2))
    die ($res1->getMessage()." erreur sur ".$sql2_a);
$totalres2 += $res2;

//
$data[$numArray] = array(
                        "-",
                        _("Autres"),
                        $res0,
                        $res1,
                        $res2
                        );
$numArray++;
$data[$numArray] = array(
                        "-",
                        _("TOTAL"),
                        $total,
                        $totalres1,
                        $totalres2
                        );

// Array
$tableau = array(
    "format" => "P",
    "title" => _("Statistiques - Electeur"),
    "subtitle" => _("Details par departement (ou pays) de naissance pour la liste en cours"),
    "offset" => array(10,0,30,30,30),
    "column" => array("",
                      _("LISTE")." : ".$_SESSION ['liste']." ".$_SESSION ['libelle_liste']."",
                      _("Total"),
                      _("Hommes"),
                      _("Femmes")
                      ),
    "data" => $data,
    "output" => "stats-electeursexedeptnaiss"
    );

?>

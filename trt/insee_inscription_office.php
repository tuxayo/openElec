<?php
/**
 * Ce fichier permet de faire le traitement d'import des inscriptions d'office
 * depuis les fichiers envoyes par l'INSEE. Il recupere les requetes sql dans
 * le fichier: sql/.../trt_inscription_office.inc
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_insee_inscription_office",
               _("Traitement")." -> "._("Inscriptions d'office INSEE"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 * Le traitement insee n'est pas disponible pour les listes complementaires
 * Si l'utilisateur est connecte sur une liste complementaire, il n'a pas acces
 * a l'interface du traitement
 */
if ($_SESSION['liste'] != "01") {
    // Affichage du message
    $message_class= "error";
    $message = _("Ce module n'est pas disponible pour les listes complementaires.");
    $f->displayMessage($message_class, $message);
    // Fin du script
    die();
}

/**
 *
 */
//
$description = _("Ce module permet de gerer l'import et l'edition des inscrits ".
                 "d'office de l'INSEE importes dans le logiciel numeriquement. ".
                 "Il se divise en deux parties : l'import et l'edition. ".
                 "Il suffit de suivre les instructions de chacune de ces parties.");
$f->displayDescription($description);

/**
 * Affichage du formulaire de lancement du traitement
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Import des inscriptions d'office INSEE");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.insee_inscription_office.class.php";
$trt = new inseeInscriptionOfficeTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Editions des listings et des etiquettes");
$f->displaySubTitle($subtitle);
//
$links = array(
    "0" => array(
        "href" => "../app/pdfetiquette.php?obj=inscription_office",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Etiquettes des inscrits d'office INSEE"),
    ),
    "1" => array(
        "href" => "../pdf/pdf.php?obj=inscription_office",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Listing des inscriptions d'office INSEE"),
        "description" => _("Ce listing permet d'editer une liste de toutes les ".
                           "entrees presentes dans la table temporaire des ".
                           "inscriptions d'office INSEE."),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Validation des inscriptions d'office INSEE");
$f->displaySubTitle($subtitle);
//
echo "<p>";
echo _("Une fois l'import effectue, les etiquettes et le listing imprimes, ".
       "il faut valider chacune des inscriptions d'office importee pour ".
       "verifier les donnees importees et transformer ces donnees en ".
       "mouvements d'inscription.");
echo "</p>";
//
$links = array(
    "0" => array(
        "href" => "../scr/tab.php?obj=inscription_office",
        "class" => "om-prev-icon inscription-office-insee-16",
        "title" => _("Validation des inscriptions d'office INSEE"),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

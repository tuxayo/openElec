<?php
/**
 *
 */

/**
 *
 */
require_once("../obj/utils.class.php");

// variable $obj
$obj = "export-fichesnavettes";

// new utils
if (!isset ($f)) $f = new utils ("nohtml", $obj, _("Export -> Fiches Navettes"));
    
//
set_time_limit (1800);

//
global $endline;
$endline = "\r\n";

//
if (!isset ($collectivites)) {
    $collectivites = array ($_SESSION['collectivite']);
}

//
function jump ($nb = 1, $endline = "\r\n") {
    $jump = "";
    for ($i = 0; $i < $nb; $i++) {
        $jump .= $endline;
    }
    return $jump;
}

function space ($nb = 1) {
    $space = "";
    for ($i = 0; $i < $nb; $i++) {
        $space .= " ";
    }
    return $space;
}
//
function separator ($ville, $start = true) {

}

$content = "";
$result_counter = 0;
// ENTETE FICHIER

//
foreach ($collectivites as $collectivite) {   
    //
    $sql = "select * from collectivite where id='".$collectivite."'";
    $res_collectivite = $f->db->query ($sql);
    $f->isDatabaseError($res_collectivite);
    $row_collectivite =& $res_collectivite->fetchrow(DB_FETCHMODE_ASSOC);
    
    if ($row_collectivite['type_interface'] == 1) {
        continue;
    }
    //
    $entredeuxdates = false;
    if (isset($_POST['datedebut']) and isset($_POST['datefin'])) {
        $datedebut = $f->formatDate($_POST['datedebut']);
        $datefin = $f->formatDate($_POST['datefin']);
        if ($datedebut != false and $datefin != false) {
            $entredeuxdates = true;
        }
    }
    //
    $sql = "select * from mouvement ";
    $sql .= "inner join param_mouvement on mouvement.types=param_mouvement.code ";
    $sql .= "where date_tableau='".$f->collectivite['datetableau']."' ";
    $sql .= "and collectivite='".$collectivite."'";
    $sql .= "and (typecat='Inscription' or typecat='Modification') ";
    if ($entredeuxdates) {
        $sql .= "and date_modif>='".$datedebut."' and date_modif<='".$datefin."'";
    }
    $res = $f->db->query ($sql);
    $f->isDatabaseError($res);
    
    if ($res->numrows() == 0){
        continue;
    }

    //
    $result_counter++;

    // ENTETE COLLECTIVITE
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "******** DEBUT ".$row_collectivite['ville']."".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;

    while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
        //
        $content .= chr(12);
        $content .= jump ();
        // 1	Date du jour	8	1 à 8	Format jj.mm.aaaa
        $content .= date("d.m.y");
        // fin de ligne
        $content .= jump ();
        // // 1 ligne vide
        $content .= jump ();
        // 2	Zone d'espaces	21	1 à 21
        $content .= space (21);
        // 3	Code Département	2	22 à 23	Entier
        $content .= substr($row_collectivite ['inseeville'], 0, 2);
        // 4	Zone d'espace	1	24
        $content .= space ();
        // 5	Code commune	3	25 à 27	Entier
        $content .= substr($row_collectivite ['inseeville'], 2, 3);
        // 6	Zone d'espace	1	28
        $content .= space ();
        // 7	Premières lettres du nom	4	29 à 32	4 premières lettres du nom en majuscules
        $content .= strtoupper (str_pad (substr($row['nom'], 0, 4), 4, " "));
        // 8	Zone d'espace	1	33	
        $content .= space ();
        // 9	Premières lettres du prénom	3	34 à 36	3 premières lettres du prénom en majuscules
        $content .= strtoupper (str_pad (substr($row['prenom'], 0, 3), 3, " "));
        // 10	Zone d'espace	1	37	
        $content .= space ();
        // 11	Date de naissance	8	38 à 45	Format jjmmaaaa
        $date = explode ("-", $row['date_naissance']);
        $content .= $date[2].$date[1].$date[0];
        // 12	Zone d'espace	1	46	
        $content .= space ();
        // 13	????	2	47 à 48	Entier
        $content .= "00";
        // 14	Zone d'espace	1	49	
        $content .= space ();
        // 15	Libellé commune	13	50 à 62	Texte alogné à gauche
        $content .= strtoupper (str_pad (substr($row_collectivite['ville'], 0, 13), 13, " "));
        // 16	Zone d'espace	1	63
        $content .= space ();
        // 17	Nom et prénom	13	64 à 76	Format NOM*PRENOM (tronquer le prénom si besoin)
        $content .= strtoupper (str_pad (substr($row['nom']."*".$row['prenom'], 0, 13), 13, " "));
        // fin de ligne
        $content .= jump ();
        // // 25 lignes vides
        $content .= jump (25);
        // 18	Zone d'espace	1	1
        $content .= space (1);
        // 19	Numéro bureau de vote	2	2 à 3	Entier
        $content .= str_pad ($row['code_bureau'], 2, "0", STR_PAD_LEFT);
        // 20	Zone d'espaces	3	4 à 6
        $content .= space (3);
        // 21	Numéro de bureau	2	7 à 8	Entier ?????????????
        $content .= "00";
        // 22	Zone d'espaces	2	9 à 10
        $content .= space (2);
        // 23	Civilité	4	11 à 14	Mr, Mme ou Mlle
        $content .= strtoupper (str_pad ($row['civilite'], 4));
        // 24	Zone d'espaces	4	15 à 18
        $content .= space (4);
        // 25	Nom et prénoms	60	19 à 78	Format NOM*1erPRENOM/2ePRENOM aligné à gauche
        if( $row['nom_usage']!="" ){
            $content .= strtoupper (str_pad (substr($row['nom']."*".str_replace(" ", "/", $row['prenom']."/".$row['nom_usage']), 0, 60), 60, " "));
        } else {
            $content .= strtoupper (str_pad (substr($row['nom']."*".str_replace(" ", "/", $row['prenom']), 0, 60), 60, " "));
        }
        // fin de ligne
        $content .= jump ();
        // // 3 lignes vides
        $content .= jump (3);
        // 26	Numéro (et lettre) de rue	7	1 à 7	Texte aligné à droite
        $content .= strtoupper (str_pad (substr (($row['numero_habitation']==0?"":$row['numero_habitation']).$row['complement_numero'], 0, 7), 7, " ", STR_PAD_LEFT));
        // 27	Zone d'espaces	6	8 à 13
        $content .= space (6);
        // 28	Type de voie	3	14 à 16	Exemple (Rue, Imp...)
        $content .= space (3);
        // 29	Zone d'espaces	5	17 à 21	
        $content .= space (5);
        // 30	Nom de la rue	50	22 à 71	Texte aligné à gauche
        if( $row['complement']!="" ){
            $content .= strtoupper (str_pad (substr ($row['libelle_voie']." ".$row['complement'], 0, 50), 50));
        } else {
            $content .= strtoupper (str_pad (substr ($row['libelle_voie'], 0, 50), 50));
        }
        // fin de ligne
        $content .= jump ();
        // // 8 lignes vides
        $content .= jump (8);
        // 31	Zone d'espace	1	1
        $content .= space ();
        // 32	Jour de naissance	2	2 à 3	Entier
        $content .= $date[2];
        // 33	Zone d'espaces	3	4 à 6
        $content .= space (3);
        // 34	Mois de naissance	2	7 à 8	Entier
        $content .= $date[1];
        // 35	Zone d'espaces	3	9 à 11
        $content .= space (3);
        // 36	Annee de naissance	4	12 à 15	Entier
        $content .= $date[0];
        // 37	Zone d'espaces	4	16 à 19
        $content .= space (4);
        // 38	Département de naissance	2	20 à 21	Entier
        $content .= substr ($row['code_departement_naissance'], 0, 2);
        // 39	Zone d'espaces	3	22 à 24
        $content .= space (3);
        // 40	Libellé commune de naissance	50	25 à 74	Texte aligné à gauche
        $content .= strtoupper (str_pad (substr ($row['libelle_lieu_de_naissance'], 0, 50), 50));
        // fin de ligne
        $content .= jump ();
    }
    // PIED COLLECTIVITE
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "******** FIN ".$row_collectivite['ville']."".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= "--------------------------------------------------------------------------------".$endline;
    $content .= jump () . chr(12);
}

//
if ($result_counter == 0) {
    $f->displayMessage("", _("Aucun enregistrement ne correspond a votre requete."));
}
//
$fichier = $f->getParameter("tmpdir").date("Ymd-Gis")."-FichesLE.txt";
@$inf = fopen ($fichier, "w");
if ($inf == false) {
    $msg = _("Impossible d'ecrire le fichier :");
    $msg .= " ".$fichier.".";
    $msg .= " "._("L'ecriture n'est surement pas autorise sur le dossier.");
    $f->displayMessage ("error", $msg);
} else {
    fwrite ($inf, $content);
    fclose ($inf);
}

?>
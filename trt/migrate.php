<?php
/**
 * Ce fichier permet la migration openelec 3.02 vers 4.00
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
error_reporting (E_ERROR);

/**
 *
 */
class om_setup {

    var $type;
    var $post = array();
    var $db;
    var $dbType = "";
    var $msgerr = "";
    var $numERR = 0;

    function __construct() {
        echo "<div id=\"content\"><fieldset>
            <legend>Setup : Migration OpenElec 3.02 -> OpenElec 4.00</legend><br/>";
        $this->type = "noconf";
        $this->POST_treatment();
        if (isset($this->post['type'])) $this->type = $this->post['type'];
        $type = $this->type;
        $this->$type();
    }

    function __destruct() {
        echo "</fieldset></div>";
    }

    /**
     * Recuperation des donnees envoyes par POST
     */
    function POST_treatment() {
        foreach ($_POST as $postname => $postvalue) {
            $this->post[$postname] = $postvalue;
        }
    }

    /**
     * Formulaire : Choix de la base de donnees
     */
    function noconf() {
        require_once "../dyn/database.inc.php";
        echo"Choix de la base à migrer :";
        echo"<br /><form method=\"post\" action=\"?\" >";
        echo"<input type=\"hidden\" name=\"type\" value=\"migrate\" />
            <select name=\"base\">";
        $cpt = 0;
        foreach ($conn as $cnx) {
            echo "<option value=\"".$cpt."\">Type: ".$cnx[1].
                " &nbsp; &nbsp; Base: ".$cnx[9]."</option>";
            $cpt++;
        }
        echo "</select><input type=\"submit\"
            value=\"Effectuer la migration\" /></form>";
    }

    /**
     * Connexion a la base de donnees
     */
    function dbConnect() {
        $db = "";
        require_once "../dyn/database.inc.php";
        $db_type = $conn[$this->post['base']][1];
        $db_host = $conn[$this->post['base']][6];
        $db_port = $conn[$this->post['base']][7];
        $db_dbname = $conn[$this->post['base']][9];
        $db_user = $conn[$this->post['base']][3];
        $db_password = $conn[$this->post['base']][4];
        $this->dbType = $db_type;
        switch ($db_type) {
            case "pgsql":
                $db = pg_connect("host=".$db_host." port=".$db_port." dbname=".
                    $db_dbname." user=".$db_user." password=".$db_password);
                if (!$db) die("Erreur psql sur la connexion a la base de donnees");
                break;
            case "mysql":
                $db = mysql_connect($db_host.":".$db_port, $db_user, $db_password);
                if (!$db) die("Erreur mysql sur la connexion a la base de donnees");
                if ($db) {
                    $db_selected = mysql_select_db($db_dbname, $db);
                    if (!$db_selected) die("Erreur mysql sur la selection de la base de donnees");
                }
                break;
        }
        $this->db = $db;
    }

    /**
     * Execution de la requete sql
     */
    function dbExecute($sql) {
        $result = "";
        switch ($this->dbType) {
            case "pgsql":
                $result = pg_query($this->db, $sql);
                break;
            case "mysql":
                $result = mysql_query($sql, $this->db);
                break;
        }
        return $result;
    }

    /**
     * Recuperation des donnees de la requete sql
     */
    function dbRows($sql) {
        $result = array();
        $res = $this->dbExecute($sql);
        if ($res) {
            switch($this->dbType){
                case "pgsql":
                    $cpt = 0;
                    while ($row = pg_fetch_row($res)) {
                        foreach($row as $value) {
                            $result[$cpt][] = $value;
                        }
                    }
                    $cpt++;
                    break;
                case "mysql":
                    $cpt = 0;
                    while ($row = mysql_fetch_array($res, MYSQL_NUM)) {
                        foreach($row as $value) {
                            $result[$cpt][] = $value;
                        }
                    }
                    $cpt++;
                    break;
            }
        }
        return $result;
    }

    /**
     * Traitement sql
     */
    function treatmentSQL($sql) {
        echo "<ul>";
        foreach ($sql as $req) {
            $res = $this->dbExecute($req);
            if (!$res) {
                echo "<li><font color=red> [ERROR or ALREADY] : ".$req."</font>";
                $this->numERR++;
            }else{
                echo "<li> [OK] : ".$req;
            }
        }
        echo "</ul>";
    }

    /** 
     * Traitement de la migration
     */
    function migrate() {
        $this->dbConnect();
        
        $sql = array();
        
        ////////////////////// TYPE PGSQL //////////////////
        $sql[] = "ALTER TABLE electeur ADD COLUMN collectivite varchar(3);";
        $sql[] = "UPDATE electeur SET collectivite = '001';";
        $sql[] = "ALTER TABLE electeur ALTER libelle_lieu_de_naissance TYPE CHARACTER VARYING(50);";
        $sql[] = "ALTER TABLE electeur ALTER libelle_departement_naissance TYPE CHARACTER VARYING(50);";
        $sql[] = "ALTER TABLE electeur ALTER libelle_voie TYPE CHARACTER VARYING(50);";

        $sql[] = "ALTER TABLE collectivite ADD COLUMN adresse varchar(50);";
        $sql[] = "ALTER TABLE collectivite ADD COLUMN complement_adresse varchar(50);";
        $sql[] = "ALTER TABLE collectivite ADD COLUMN maire_de varchar(15);";
        $sql[] = "ALTER TABLE collectivite ADD COLUMN civilite varchar(15);";
        $sql[] = "UPDATE collectivite SET type_interface=0;";
        $sql[] = "UPDATE collectivite SET id='001' where id='0';";
        
        $sql[] = "ALTER TABLE archive ADD COLUMN collectivite varchar(3);";
        $sql[] = "UPDATE archive SET collectivite = '001';";
        
        $sql[] = "ALTER TABLE utilisateur ADD COLUMN collectivite varchar(3);";
        $sql[] = "UPDATE utilisateur SET collectivite = '001';";
        
        $sql[] = "ALTER TABLE bureau ADD COLUMN collectivite varchar(3);";
        $sql[] = "UPDATE bureau SET collectivite = '001';";
        
        $sql[] = "ALTER TABLE mouvement ADD COLUMN collectivite varchar(3);";
        $sql[] = "UPDATE mouvement SET collectivite = '001';";
        
        $sql[] = "ALTER TABLE inscription_office ADD COLUMN collectivite varchar(3);";
        $sql[] = "UPDATE inscription_office SET collectivite = '001';";
        
        $sql[] = "ALTER TABLE numerobureau ADD COLUMN collectivite varchar(3);";
        $sql[] = "UPDATE numerobureau SET collectivite = '001';";
        $sql[] = "ALTER TABLE numerobureau RENAME COLUMN idlistebureau TO id;";

        $sql[] = "ALTER TABLE departement ALTER libelle_departement TYPE CHARACTER VARYING(50);";

        $sql[] = "ALTER TABLE voie ALTER libelle_voie TYPE CHARACTER VARYING(50);";
        $sql[] = "ALTER TABLE voie ALTER code TYPE CHARACTER VARYING(6);";
        $sql[] = "UPDATE voie SET code_collectivite = '001';";

        $sql[] = "DROP TABLE IF EXISTS numeroliste;";

        $sql[] = "CREATE TABLE numeroliste (
            id character varying(9) DEFAULT ''::character varying NOT NULL,
            collectivite character varying(3) DEFAULT '0'::character varying NOT NULL references collectivite(id),
            liste character varying(6) DEFAULT '0'::bpchar NOT NULL references liste(liste),
            dernier_numero bigint DEFAULT (0)::bigint NOT NULL,
            PRIMARY KEY (collectivite, liste)
        );";

        $sql[] = "INSERT INTO profil VALUES (99, 'NON UTILISE');";

        $sql[] = "DELETE FROM droit;";

        $sql[] = "INSERT INTO droit VALUES ('menu_decoupage', 4);";
        $sql[] = "INSERT INTO droit VALUES ('nationalite_tab', 4);";
        $sql[] = "INSERT INTO droit VALUES ('departement_tab', 4);";
        $sql[] = "INSERT INTO droit VALUES ('commune_tab', 4);";
        $sql[] = "INSERT INTO droit VALUES ('voie_tab', 4);";
        $sql[] = "INSERT INTO droit VALUES ('bureau_tab', 4);";
        $sql[] = "INSERT INTO droit VALUES ('canton_tab', 4);";
        $sql[] = "INSERT INTO droit VALUES ('bureau', 4);";
        $sql[] = "INSERT INTO droit VALUES ('voie', 4);";
        $sql[] = "INSERT INTO droit VALUES ('canton', 4);";
        $sql[] = "INSERT INTO droit VALUES ('commune', 4);";
        $sql[] = "INSERT INTO droit VALUES ('departement', 4);";
        $sql[] = "INSERT INTO droit VALUES ('nationalite', 4);";
        $sql[] = "INSERT INTO droit VALUES ('menu_saisie', 2);";
        $sql[] = "INSERT INTO droit VALUES ('rechercheinscription', 2);";
        $sql[] = "INSERT INTO droit VALUES ('modification', 2);";
        $sql[] = "INSERT INTO droit VALUES ('radiation', 3);";
        $sql[] = "INSERT INTO droit VALUES ('rechercheelecteur', 99);";
        $sql[] = "INSERT INTO droit VALUES ('electeur', 4);";
        $sql[] = "INSERT INTO droit VALUES ('procuration', 3);";
        $sql[] = "INSERT INTO droit VALUES ('centrevote', 3);";
        $sql[] = "INSERT INTO droit VALUES ('mairieeurope', 3);";
        $sql[] = "INSERT INTO droit VALUES ('menu_consultation', 1);";
        $sql[] = "INSERT INTO droit VALUES ('consult_electeur_tab', 1);";
        $sql[] = "INSERT INTO droit VALUES ('consult_electeur', 2);";
        $sql[] = "INSERT INTO droit VALUES ('consult_archive_tab', 3);";
        $sql[] = "INSERT INTO droit VALUES ('consult_archive', 3);";
        $sql[] = "INSERT INTO droit VALUES ('inscription_tab', 2);";
        $sql[] = "INSERT INTO droit VALUES ('modification_tab', 2);";
        $sql[] = "INSERT INTO droit VALUES ('radiation_tab', 3);";
        $sql[] = "INSERT INTO droit VALUES ('procuration_tab', 3);";
        $sql[] = "INSERT INTO droit VALUES ('centrevote_tab', 3);";
        $sql[] = "INSERT INTO droit VALUES ('mairieeurope_tab', 3);";
        $sql[] = "INSERT INTO droit VALUES ('inscription_office_tab', 3);";
        $sql[] = "INSERT INTO droit VALUES ('inscription_office', 3);";
        $sql[] = "INSERT INTO droit VALUES ('tmp', 4);";
        $sql[] = "INSERT INTO droit VALUES ('menu_edition', 3);";
        $sql[] = "INSERT INTO droit VALUES ('bureau_edition_tab', 3);";
        $sql[] = "INSERT INTO droit VALUES ('reqmo', 3);";
        $sql[] = "INSERT INTO droit VALUES ('bureau_edition', 3);";
        $sql[] = "INSERT INTO droit VALUES ('edition', 3);";
        $sql[] = "INSERT INTO droit VALUES ('statistiques', 3);";
        $sql[] = "INSERT INTO droit VALUES ('menu_traitement', 4);";
        $sql[] = "INSERT INTO droit VALUES ('traitement_j5', 5);";
        $sql[] = "INSERT INTO droit VALUES ('traitement_annuel', 5);";
        $sql[] = "INSERT INTO droit VALUES ('refonte', 5);";
        $sql[] = "INSERT INTO droit VALUES ('redecoupage', 5);";
        $sql[] = "INSERT INTO droit VALUES ('cnen_in', 4);";
        $sql[] = "INSERT INTO droit VALUES ('cnen_out', 4);";
        $sql[] = "INSERT INTO droit VALUES ('mention', 4);";
        $sql[] = "INSERT INTO droit VALUES ('jury', 4);";
        $sql[] = "INSERT INTO droit VALUES ('carteretour', 4);";
        $sql[] = "INSERT INTO droit VALUES ('trt_centrevote', 4);";
        $sql[] = "INSERT INTO droit VALUES ('archivage', 5);";
        $sql[] = "INSERT INTO droit VALUES ('epuration', 5);";
        $sql[] = "INSERT INTO droit VALUES ('decoupage', 5);";
        $sql[] = "INSERT INTO droit VALUES ('menu_parametrage', 5);";
        $sql[] = "INSERT INTO droit VALUES ('param_mouvement_tab', 5);";
        $sql[] = "INSERT INTO droit VALUES ('collectivite_tab', 5);";
        $sql[] = "INSERT INTO droit VALUES ('numerobureau_tab', 5);";
        $sql[] = "INSERT INTO droit VALUES ('numeroliste_tab', 5);";
        $sql[] = "INSERT INTO droit VALUES ('profil_tab', 5);";
        $sql[] = "INSERT INTO droit VALUES ('droit_tab', 5);";
        $sql[] = "INSERT INTO droit VALUES ('utilisateur_tab', 5);";
        $sql[] = "INSERT INTO droit VALUES ('liste_tab', 5);";
        $sql[] = "INSERT INTO droit VALUES ('etat', 5);";
        $sql[] = "INSERT INTO droit VALUES ('sousetat', 5);";
        $sql[] = "INSERT INTO droit VALUES ('utilisateur', 5);";
        $sql[] = "INSERT INTO droit VALUES ('profil', 5);";
        $sql[] = "INSERT INTO droit VALUES ('droit', 5);";
        $sql[] = "INSERT INTO droit VALUES ('collectivite', 5);";
        $sql[] = "INSERT INTO droit VALUES ('numerobureau', 99);";
        $sql[] = "INSERT INTO droit VALUES ('numeroliste', 99);";
        $sql[] = "INSERT INTO droit VALUES ('param_mouvement', 5);";
        $sql[] = "INSERT INTO droit VALUES ('liste', 5);";
        $sql[] = "INSERT INTO droit VALUES ('listedefaut', 1);";
        $sql[] = "INSERT INTO droit VALUES ('collectivitedefaut', 99);";
        $sql[] = "INSERT INTO droit VALUES ('password', 5);";
        $sql[] = "INSERT INTO droit VALUES ('documentation', 1);";

        $sql[] = "DROP TABLE IF EXISTS radiation_insee;";
        $sql[] = "DROP SEQUENCE IF EXISTS radiation_insee_seq;";

        $sql[] = "CREATE TABLE radiation_insee (
            id bigint DEFAULT (0)::bigint NOT NULL,
            nationalite character varying(50) DEFAULT ''::character varying NOT NULL,
            sexe character(1) DEFAULT ''::bpchar NOT NULL,
            nom character varying(63) DEFAULT ''::character varying NOT NULL,
            prenom1 character varying(50) DEFAULT ''::character varying NOT NULL,
            prenom2 character varying(50) DEFAULT ''::character varying,
            prenom3 character varying(50) DEFAULT ''::character varying,
            date_naissance date NOT NULL,
            localite_naissance character varying(30) DEFAULT ''::character varying,
            code_lieu_de_naissance integer DEFAULT 0 NOT NULL,
            pays_naissance  character varying(30) DEFAULT ''::character varying,
            adresse1 character varying(46) DEFAULT ''::character varying,
            adresse2 character varying(46) DEFAULT ''::character varying,
            date_deces date,
            localite_deces character varying(30) DEFAULT ''::character varying,
            code_lieu_de_deces integer DEFAULT 0,
            pays_deces  character varying(30) DEFAULT ''::character varying,
            date_nouvelle_inscription date,
            motif_nouvelle_inscription character varying(30) DEFAULT ''::character varying,
            code_perte_de_nationalite character varying(30) DEFAULT ''::character varying,
            code_autre_motif_de_radiation character varying(50),
            libelle_lieu_de_deces character varying(60),
            libelle_lieu_de_naissance character varying(30) DEFAULT ''::character varying ,
            motif_de_radiation character varying(60),
            type_de_liste character varying(60),
            types character(1) DEFAULT ''::bpchar NOT NULL,
            collectivite character varying(3)
        );";

        // Séquence radiation_insee
        $sql[] = "CREATE SEQUENCE radiation_insee_seq
          INCREMENT BY 1
          MINVALUE 1
          MAXVALUE 9223372036854775807
          START 1
          CACHE 1;
        ";

        $sql[] = "ALTER TABLE ONLY radiation_insee
            ADD CONSTRAINT radiation_insee_pkey PRIMARY KEY (id);";

        // IO/Radiation INSEE - XML EXPORT
        $sql[] = "ALTER TABLE liste ADD liste_insee character varying(10);";

        $sql[] = "DROP TABLE IF EXISTS xml_partenaire;";
        $sql[] = "DROP SEQUENCE IF EXISTS xml_partenaire_seq;";

        $sql[] = "CREATE TABLE xml_partenaire (
            xml_partenaire bigint NOT NULL,
            type_partenaire text,
            identifiant_autorite text,
            identifiant text,
            nom text,
            \"type\" text,
            \"role\" text,
            contact_nom text,
            contact_mail text,
            contact_numerofax text,
            contact_numerotelephone text,
            contact_adresse_numerovoie text,
            contact_adresse_typevoie text,
            contact_adresse_nomvoie text,
            contact_adresse_cedex text,
            contact_adresse_libellebureaucedex text,
            contact_adresse_divisionterritoriale text,
            service_nom text,
            service_fournisseur text,
            service_version text,
            service_accreditation text,
            collectivite character varying(10)
        );";

        $sql[] = "ALTER TABLE ONLY xml_partenaire
            ADD CONSTRAINT xml_partenaire_pkey PRIMARY KEY (xml_partenaire);";

        $sql[] = "CREATE SEQUENCE xml_partenaire_seq START WITH 1;";

        $sql[] = "delete from version;";
        $sql[] = "ALTER TABLE version ALTER version TYPE CHARACTER VARYING(10);";
        $sql[] = "insert into version  (version) VALUES ('4.0.0rc1');";
        //
        $this->treatmentSQL($sql);
        // Mise a jour des donnees
        echo "--- Mise à jour des donnees ---";
        $sql = array();
        $row = $this->dbRows("SELECT COUNT(*) FROM electeur WHERE collectivite='001' AND liste='01';");
        $sql[] = "INSERT INTO numeroliste VALUES ('00101', '001','01','".$row[0][0]."');";
        //
        $this->treatmentSQL($sql);
        echo "<hr />";
        echo "<font size=\"2\">&nbsp;*&nbsp;Migration de la base de
            donnees effectues avec ".$this->numERR." erreur(s).</font>";
    }

}

$instance = new om_setup();

?>

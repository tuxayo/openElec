<?php
/**
 * Statistique centre de vote - Details par sexe et bureau de vote
 * Ce fichier est appelé par statistiques.php
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"statistiques", _("Statistiques"));

// Recuperation des donnees
$data = array();

// STATISTIQUES : Nombre de votants en centres de votes, d'hommes, de femmes par bureau
$inc = "stats_centrevote_sexe_bureau";

//
include "../sql/".$f -> phptype."/".$inc.".inc";

$res = $f -> db -> query ($sql_bureau);
if (database::isError($res))
    die($res->getMessage()." erreur sur ".$sql_bureau);

//
$total = 0;
$totalres1 = 0;
$totalres2 = 0;

$numArray = 0;
while ($row =& $res -> fetchRow (DB_FETCHMODE_ASSOC)) {
    include "../sql/".$f -> phptype."/".$inc.".inc";
    // Total
    $res0 = $f -> db -> getOne ($sqlB);
    if (database::isError($res0))
        die($res0->getMessage()." erreur sur ".$sqlB);
    $total += $res0;
    // Hommes
    $res1 = $f -> db -> getOne ($sql1);
    if (database::isError($res1))
        die($res1->getMessage()." erreur sur ".$sql1);
    $totalres1 += $res1;
    // Femmes
    $res2 = $f -> db -> getOne ($sql2);
    if (database::isError($res2))
        die ($res1->getMessage()." erreur sur ".$sql2);
    $totalres2 += $res2;
    //
    $datas = array(
                   $row['code'],
                   $row['libelle_bureau'],
                   $res0,
                   $res1,
                   $res2
                   );
    $data[$numArray] = $datas;
    $numArray++;
}
//
$data[$numArray] = array(
                        "-",
                        _("TOTAL"),
                        $total,
                        $totalres1,
                        $totalres2
                        );
// Array
$tableau = array(
    "format" => "L",
    "title" => _("Statistiques - Centre de vote"),
    "subtitle" => _("Details par sexe et bureau de vote"),
    "offset" => array(10,0,40,40,40),
    "column" => array("",
                      _("LISTE")." : ".$_SESSION ['liste']." ".$_SESSION ['libelle_liste']."",
                      _("Total"),
                      _("Hommes"),
                      _("Femmes")
                      ),
    "data" => $data,
    "output" => "stats-centrevotesexebureau"
    );

?>

<?php
/**
 * Statistique traitement j5
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";

// new utils
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"traitement_j5", _("Statistiques"));

// Recuperation des donnees
$data = array();

//
$inc = "stats_traitement_j5";

/**
 *
 */
//
$error = false;
//
$action = "";
if (isset($_GET["action"])) {
    $action = $_GET["action"];
}
//
switch ($action) {
    // Appel depuis les modules de traitement
    // Le traitement n'a pas encore ete applique
    case "traitement" : 
        // La date de tableau a utiliser est la date en cours
        $datetableau = $f->collectivite["datetableau"];
        // Si c'est un traitement J5 alors on recupere les parametres des mouvements a traiter
        if (isset($traitement) && $traitement == "j5" 
            || isset($_GET["traitement"]) && $_GET["traitement"] == "j5") {
            //
            $cinqjours = false;
            $additions = array();
            if (isset($_GET["mouvementatraiter"])) {
                //
                $mouvementatraiter = explode(";", $_GET["mouvementatraiter"]);
                //
                foreach($mouvementatraiter as $elem) {
                    if ($elem == "Immediat") {
                        $cinqjours = true;
                    } else {
                        array_push($additions, $elem);
                    }
                }
            }
        }
        //
        break;
    // Appel depuis le module d'edition revision electorale
    // Le traitement a deja ete applique
    case "recapitulatif" :
        // La date de tableau a utiliser est passee en parametre
        (isset($_GET["datetableau"]) ? $datetableau = $_GET["datetableau"] : $error = true);
        // Si c'est un traitement J5 alors on recupere la date de traitement
        if (isset($traitement) && $traitement == "j5" 
            || isset($_GET["traitement"]) && $_GET["traitement"] == "j5") {
            //
            (isset($_GET["datej5"]) ? $datej5 = $_GET["datej5"] : $error = true);
        }
        //
        break;
    //
    default:
        //
        $error = true;
}
//
if ($error == true) {
    die();
}

//
include "../sql/".$f -> phptype."/".$inc.".inc";
$res = $f->db->query($sql_bureau);
$f->isDatabaseError($res);

//
$totalavant=0;
$totalinscription=0;
$totalmodification=0;
$totalradiation=0;
$totalapres=0;
$totalarrive=0;
$totaldepart=0;

/**
 * 
 */
require_once "../obj/statistiques_electeur.class.php";
$statistiques_electeur = new statistiques_electeur($f);


if ($action == "recapitulatif") {
  //
  $from_unix_time = mktime(0, 0, 0, substr($datej5, 5, 2), substr($datej5, 8, 2), substr($datej5, 0, 4));
  $day_before = strtotime("yesterday", $from_unix_time);
  $previous_datej5 = date('Y-m-d', $day_before);
}

//
$numArray = 0;
while ($row=& $res -> fetchRow (DB_FETCHMODE_ASSOC)) {
    //
    include "../sql/".$f -> phptype."/".$inc.".inc";
    //
    if ($action == "traitement") {
      $avant =$f -> db->getOne($sqlB);
      $f->isDatabaseError($avant);
    } else {
      $avant = $statistiques_electeur->calculNombreDelecteursDateParBureau($previous_datej5, $row["code"]);
    }
    //
    $inscription =$f -> db->getOne($sqlI);
    $f->isDatabaseError($inscription);
    //
    $arrive =$f -> db->getOne($sqlM1);
    $f->isDatabaseError($arrive);
    //
    $modification =$f -> db->getOne($sqlM0);
    $f->isDatabaseError($modification);
    //
    $depart =$f -> db->getOne($sqlM2);
    $f->isDatabaseError($depart);
    //
    $radiation =$f -> db->getOne($sqlR);
    $f->isDatabaseError($radiation);
    //
    $apres = $avant + $inscription + $arrive - $depart - $radiation;
    //
    $datas = array(
                   $row['code']." - ".$row['libelle_bureau'],
                   $avant,
                   $inscription,
                   $arrive,
                   $modification,
                   $depart,
                   $radiation,
                   $apres
                   );
    $data[$numArray] = $datas;
    $numArray++;
    //
    $totalavant += $avant;
    $totalinscription += $inscription;
    $totalarrive += $arrive;
    $totalmodification += $modification;
    $totaldepart += $depart;
    $totalradiation += $radiation;
    $totalapres += $apres;
}
//
$datas = array(
                   _("TOTAL"),
                   $totalavant,
                   $totalinscription,
                   $totalarrive,
                   $totalmodification,
                   $totaldepart,
                   $totalradiation,
                   $totalapres
                   );
$data[$numArray] = $datas;

//
if ($action == "recapitulatif") {
    //
    $title = sprintf(_("Detail des mouvements appliques lors du traitement J-5 du %s[Tableau du %s]"),$f->formatdate($datej5),$f->formatdate($datetableau));
} else {
    //
    $title = sprintf(_("Detail des mouvements a appliquer au traitement J-5 du %s [Tableau du %s]"),date("d/m/Y"),$f->formatdate($f->collectivite["datetableau"]));
}

// Array
$tableau = array(
    "format" => "L",
    "title" => $title,
    "offset" => array(110,0,0,0,0,0,0,0),
    "align" => array("L", "R", "R", "R", "R", "R", "R", "R"),
    "column" => array(_("Bureau(x) de vote"),
                      _("Avant TRT"),
                      _("+ Inscription(s)"),
                      _("+ Transfert(s)"),
                      _("Modification(s)"),
                      _("- Transfert(s)"),
                      _("- Radiation(s)"),
                      _("Apres TRT"),
                      ),
    "data" => $data,
    "output" => "stats-traitemenj5"
    );

?>

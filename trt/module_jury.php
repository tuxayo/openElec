<?php
/**
 * Ce fichier permet de gerer le module Jury de l'application. Il affiche
 * differents onglets contenant chacun un traitement en relation avec les
 * electeurs en jury.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"module_jury",
               _("Traitement")." -> "._("Module Jury d'assises"));

/**
 * Affichage de la structure de la page
 */
// Affichage
$f->setFlag(NULL);
$f->display();

/**
 * Le module jury n'est pas disponible pour les listes complementaires
 * Si l'utilisateur est connecte sur une liste complementaire, il n'a pas acces
 * a l'interface du traitement
 */
if ($_SESSION['liste'] != "01") {
    // Affichage du message
    $message_class= "error";
    $message = _("Ce module n'est pas disponible pour les listes complementaires.");
    $f->displayMessage($message_class, $message);
    // Fin du script
    die();
}

/**
 *
 */
//
$description = _("Les jures d'assises peuvent etre gere par le logiciel pour ".
                 "faire le tirage aleatoire, et sortir les etiquettes ou le ".
                 "listing. Vous pouvez aussi modifier manuellement les ".
                 "informations de jure d'assises d'un electeur au moyen de ".
                 "sa fiche electeur : \"Consultation -> Liste electorale\".");
$f->displayDescription($description);

/**
 * ONGLETS
 */
// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n";
// Affichage de la liste des onglets
echo "\n<ul>\n";
//
if ($f->isAccredited(/*DROIT*/"traitement_jury")) {
    echo "\t<li><a href=\"../trt/jury.php\">"._("Tirage aleatoire")."</a></li>\n";
}
//
if ($f->isAccredited(/*DROIT*/"electeur_jury_tab")) {
    echo "\t<li><a href=\"../scr/soustab.php?obj=electeur_jury&amp;retourformulaire=module_jury&amp;idxformulaire=module_jury\">"._("Liste preparatoire courante")."</a></li>\n";
}
//
if ($f->isAccredited(/*DROIT*/"traitement_jury_epuration")) {
    echo "\t<li><a href=\"../trt/jury_epuration.php\">"._("Epuration")."</a></li>\n";
}
//
if ($f->isAccredited(/*DROIT*/"traitement_jury_parametrage")) {
    echo "\t<li><a href=\"../scr/soustab.php?obj=parametrage_nb_jures&amp;retourformulaire=module_jury&amp;idxformulaire=module_jury\">"._("Parametrage")."</a></li>\n";
}
//
echo "</ul>\n";
// Fermeture de la balise - Conteneur d'onglets
echo "</div>";

?>

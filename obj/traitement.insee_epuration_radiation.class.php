<?php
/**
 * Ce fichier declare la classe inseeEpurationRadiationTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 * @todo Optimiser la methode de suppression : une requete suffit 
 */
class inseeEpurationRadiationTraitement extends traitement {
    
    var $fichier = "insee_epuration_radiation";
    
    function getValidButtonValue() {
        //
        return _("Epuration des radiations INSEE");
    }

    function getDescription() {
        //
        return _("Lors de l'import des radiations INSEE, toutes les propositions de ".
                 "l'INSEE sont stockees dans une table temporaire jusqu'a validation. ".
                 "Ce traitement supprime toutes ces radiations temporaires.");
    }
    
    function displayBeforeContentForm() {
        //
        include "../sql/".$this->page->phptype."/trt_insee_epuration.inc";
        //
        $res_radiation = $this->page->db->query($sql_RAD);
        $this->page->isDatabaseError($res_radiation);
        $nb_radiation = $res_radiation->numRows();
        $res_radiation->free();
        //
        echo "\n<div class=\"field\">\n\t<label>";
        echo _("Le nombre de radiations dans la table temporaire a la date du")." ";
        echo date('d/m/Y')." "._("est de")." ".$nb_radiation.".";
        echo "</label>\n</div>\n";
    }
    
    function treatment () {
        //
        $this->LogToFile("start insee_epuration_radiation");
        //
        include "../sql/".$this->page->phptype."/trt_insee_epuration.inc";
        //
        $res = $this->page->db->query($sql_RAD);
        //
        if (database::isError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        } else {
            //
            $i = 0;
            //
            while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                include "../sql/".$this->page->phptype."/trt_insee_epuration.inc";
                //
                $res1 = $this->page->db->query($sql_RAD_1);
                //
                if (database::isError($res1, true)) {
                    //
                    $this->error = true;
                    //
                    $message = $res1->getMessage()." - ".$res1->getUserInfo();
                    $this->LogToFile($message);
                    //
                    $this->addToMessage(_("Contactez votre administrateur."));
                    //
                }
                //
                $i++;
                //
                $this->LogToFile($row['nom']." ".$row['prenom1']);
            }
            //
            $message = $i." "._("radiation(s) INSEE supprimee(s)");
            $this->LogToFile($message);
            $this->addToMessage($message);
        }
        //
        $this->LogToFile("end insee_epuration_radiation");
    }
}

?>

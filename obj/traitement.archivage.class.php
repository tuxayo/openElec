<?php

require_once ("../obj/traitement.class.php");

class archivageTraitement extends traitement {
    
    var $fichier = "archivage";
    
    function getValidButtonValue() {
        //
        return _("Archivage des mouvements");
    }
    
    function displayBeforeContentForm() {
        //
        $datetableau = $this->page->collectivite['datetableau'];
        $datetableau_show = $this->page->formatDate($this->page->collectivite['datetableau']);
        //
        include "../sql/".$this->page->phptype."/trt_archivage.inc";
        // NB mouvements a archiver
        $res = $this->page->db->query($sql);
        if (database::isError($res))
            die($res->getMessage()." erreur sur ".$sql);
        $nbArchive = $res -> numRows ();
        $res->free();
        //
        echo "NOMBRE DE MOUVEMENTS A ARCHIVER a la date du ".$datetableau_show;
        echo " ".$nbArchive."";
    }
    
    function treatment () {
        //
        $this->LogToFile ("start archivage");
        
        //
        $datetableau = $this->page->collectivite['datetableau'];
        
        //
        require_once ("../obj/archive.class.php");
        include ("../sql/".$this->page->phptype."/trt_archivage.inc");
        
        // TRAITEMENT ARCHIVAGE
	$this->LogToFile ("TRAITEMENT ARCHIVAGE");
        $res = $this->page->db->query ($sql);
	if (database::isError($res)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage ()." - ".$res->getUserInfo ();
            $this->LogToFile ($message);
        } else {
            //
            $this->LogToFile ($res->numRows()." mouvements a archiver");
            while ($row =& $res->fetchRow (DB_FETCHMODE_ASSOC)) {
                // ajout ARCHIVE
                $enr = new archive ("]", $this->page->db, 0);
                $enr->ajouterTraitement ($row, $this->page->db, 0, $datetableau) ;
                //
                $message = "=> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom']." ".$enr->msg;
                $this->LogToFile ($message);
                // suppression MOUVEMENT
                include ("../sql/".$this->page->phptype."/trt_archivage.inc");
                $res1 = $this->page->db->query ($sql1);
                if (database::isError($res1)) {
                    //
                    $this->error = true;
                    //
                    $message = $res1->getMessage ()." - ".$res1->getUserInfo ();
                    $this->LogToFile ($message);
                } else {
                    $message = "l'enregistrement ".$row['id']." de la table mouvement est detruit";
                    $this->LogToFile ($message);
                }
            }
            $res->free();
        }
        
        //
        $this->LogToFile ("end archivage");
    }
}

?>
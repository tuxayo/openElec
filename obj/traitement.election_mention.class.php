<?php
/**
 * Ce fichier declare la classe electionMentionTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class electionMentionTraitement extends traitement {
    
    var $fichier = "election_mention";
    
    var $champs = array("dateelection1", "dateelection2", "centrevote", "mairieeurope");
    
    function getValidButtonValue() {
        return _("Appliquer les mentions sur les listes d'emargement");
    }
    
    function getDescription() {
        //
        return _("Ce traitement concerne toutes les listes.");
    }
    
    function setContentForm() {
        //
        $double_emargement = $this->page->getParameter("double_emargement");
        //
        if ($double_emargement == true) {
            $this->form->setLib("dateelection1", _("Date du 1er tour"));
        } else {
            $this->form->setLib("dateelection1", _("Date de l'election"));
        }
        $this->form->setType("dateelection1", "date");
        $this->form->setTaille("dateelection1", 10);
        $this->form->setMax("dateelection1", 10);
        $this->form->setOnchange("dateelection1", "fdate(this)");
        //
        $this->form->setLib("dateelection2", _("Date du 2nd tour"));
        if ($double_emargement == true) {
            $this->form->setType("dateelection2", "date");
        } else {
            $this->form->setType("dateelection2", "hidden");
        }
        $this->form->setTaille("dateelection2", 10);
        $this->form->setMax("dateelection2", 10);
        $this->form->setOnchange("dateelection2", "fdate(this)");
        //
        $this->form->setLib("centrevote", _("Prise en compte des mentions Centre de Vote (les procurations seront desactivees pour ces electeurs)"));
        $this->form->setType("centrevote", "checkbox");
        $this->form->setTaille("centrevote", 10);
        $this->form->setMax("centrevote", 10);
        //
        $this->form->setLib("mairieeurope", _("Prise en compte des mentions Mairie Europe  (les procurations seront desactivees pour ces electeurs)"));
        $this->form->setType("mairieeurope", "checkbox");
        $this->form->setTaille("mairieeurope", 10);
        $this->form->setMax("mairieeurope", 10);
    }
    
    function displayAfterContentForm() {
        //
        $links = array(
            "0" => array(
                "href" => "javascript:recapitulatif_mention();",
                "class" => "om-prev-icon pdf-16",
                "title" => _("Cliquer ici pour visualiser le recapitulatif complet du traitement"),
            ),
        );
        //
        $this->page->displayLinksAsList($links);
    }
    
    function treatment () {
        //
        $this->LogToFile("start election_mention");
        //
        $dateelection1 = NULL;
        if (isset($_POST['dateelection1'])) {
            //
            $date = explode("/", $_POST['dateelection1']);
            if (sizeof($date) == 3 and (checkdate($date[1], $date[0], $date[2]))) {
                $dateelection1 = $date[2]."-".$date[1]."-".$date[0];
            }
        }
        //
        $dateelection2 = NULL;
        if (isset($_POST['dateelection2'])) {
            //
            $date = explode("/", $_POST['dateelection2']);
            if (sizeof($date) == 3 and (checkdate($date[1], $date[0], $date[2]))) {
                $dateelection2 = $date[2]."-".$date[1]."-".$date[0];
            }
        }
        //
        if ($dateelection1 == NULL && $dateelection2 == NULL) {
            //
            $dateelection1 = date('Y-m-d');
        }
        //
        if ($this->error == false) {
            //
            include "../sql/".$this->page->phptype."/trt_mention.inc";
            // On supprime toutes les procurations qui existent dans la table
            // electeur
            $sql = " update electeur ";
            $sql .= " set procuration = null ";
            $sql .= " where procuration <> '' and collectivite='".$_SESSION["collectivite"]."'";
            //
            $res = $this->page->db->query($sql);
            //
            if (database::isError($res, true)) {
                //
                $this->error = true;
                //
                $message = $res->getMessage()." - ".$res->getUserInfo();
                $this->LogToFile($message);
                //
                $this->addToMessage(_("Contactez votre administrateur."));
            } else {
                //
                $msg = $this->page->db->affectedRows()." "._("mention(s) trouvee(s) eliminee(s) toutes listes confondues");
                $this->LogToFile($msg);
                //
                $msg = _("Mentions prises en compte pour le");
                if ($dateelection1 != NULL && $dateelection2 != NULL) {
                    $msg .= " ".$this->page->formatdate($dateelection1);
                    $msg .= " "._("et le")." ";
                    $msg .= " ".$this->page->formatdate($dateelection2);
                } else {
                    if ($dateelection1 != NULL) {
                        $msg .= " ".$this->page->formatdate($dateelection1);
                    } else {
                        $msg .= " ".$this->page->formatdate($dateelection2);
                    }
                }
                $msg .= " : ";
                $this->LogToFile($msg);
                
                // Traitement des mentions 'centre de vote'
                (isset($_POST['centrevote']) ? $centreVote = $_POST['centrevote'] : $centreVote = "");
                // 
                if ($centreVote == "Oui" && $this->error == false) {
                    //
                    $res = $this->page->db->query($sqlCV);
                    //
                    if (database::isError($res, true)) {
                        //
                        $this->error = true;
                        //
                        $message = $res->getMessage()." - ".$res->getUserInfo();
                        $this->LogToFile($message);
                        //
                        $this->addToMessage(_("Contactez votre administrateur."));
                    } else {
                        //
                        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                            //
                            if ($this->error == false) {
                                //
                                $procuration = $this->page->db->getOne("select procuration from electeur where collectivite = '".$_SESSION['collectivite']."' and id_electeur=".$row['id_electeur']);
                                //
                                if (database::isError($procuration, true)) {
                                    //
                                    $this->error = true;
                                    //
                                    $message = $procuration->getMessage()." - ".$procuration->getUserInfo();
                                    $this->LogToFile($message);
                                    //
                                    $this->addToMessage(_("Contactez votre administrateur."));
                                }
                                //
                                if ($this->error == false) {                
                                    //
                                    $msg = $row['id_electeur']." "._("inscrit en centre de vote");
                                    $this->LogToFile($msg);
                                    //
                                    $mention = "*** Vote à l'étranger pour tous les scrutins dont la loi électorale prévoit qu'ils se déroulent en partie à l'étranger ***/";
                                    //
                                    $valF = array("procuration" => $mention." ".$procuration);
                                    $res1 = $this->page->db->autoexecute("electeur", $valF, DB_AUTOQUERY_UPDATE, "id_electeur=".$row['id_electeur']);
                                    //
                                    if (database::isError($res1, true)) {
                                        //
                                        $this->error = true;
                                        //
                                        $message = $res1->getMessage()." - ".$res1->getUserInfo();
                                        $this->LogToFile($message);
                                        //
                                        $this->addToMessage(_("Contactez votre administrateur."));
                                    }
                                }
                            }
                        }
                    }
                }
                // Traitement des mentions 'mairie europe'
                (isset($_POST['mairieeurope']) ? $mairieEurope = $_POST['mairieeurope'] : $mairieEurope = "");
                //
                if ($mairieEurope == "Oui" && $this->error == false) {
                    //
                    $res = $this->page->db->query($sqlME);
                    //
                    if (database::isError($res, true)) {
                        //
                        $this->error = true;
                        //
                        $message = $res->getMessage()." - ".$res->getUserInfo();
                        $this->LogToFile($message);
                        //
                        $this->addToMessage(_("Contactez votre administrateur."));
                    } else {
                        //
                        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                            //
                            if ($this->error == false) {
                                //
                                $procuration = $this->page->db->getOne("select procuration from electeur where collectivite = '".$_SESSION['collectivite']."' and id_electeur=".$row['id_electeur_europe']);
                                //
                                if (database::isError($procuration, true)) {
                                    //
                                    $this->error = true;
                                    //
                                    $message = $procuration->getMessage()." - ".$procuration->getUserInfo();
                                    $this->LogToFile($message);
                                    //
                                    $this->addToMessage(_("Contactez votre administrateur."));
                                }
                                //
                                if ($this->error == false) {
                                    //
                                    $msg = $row['id_electeur_europe']." ".("inscrit en mairie europe");
                                    $this->LogToFile($msg);
                                    //
                                    $mention = "*** Vote à l'étranger pour l'élection européenne ***|";
                                    //
                                    $valF = array("procuration" => $mention." ".$procuration);
                                    $res1 = $this->page->db->autoexecute("electeur", $valF, DB_AUTOQUERY_UPDATE, "id_electeur=".$row['id_electeur_europe']);
                                    //
                                    if (database::isError($res1, true)) {
                                        //
                                        $this->error = true;
                                        //
                                        $message = $res1->getMessage()." - ".$res1->getUserInfo();
                                        $this->LogToFile($message);
                                        //
                                        $this->addToMessage(_("Contactez votre administrateur."));
                                    }
                                }
                            }
                        }
                    }
                }
                //
                if ($this->error == false) {
                    // Traitement des procurations
                    $res = $this->page->db->query($sqlP);
                    //
                    if (database::isError($res, true)) {
                        //
                        $this->error = true;
                        //
                        $message = $res->getMessage()." - ".$res->getUserInfo();
                        $this->LogToFile($message);
                        //
                        $this->addToMessage(_("Contactez votre administrateur."));
                    } else {
                        //
                        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                            //
                            if ($this->error == false) {
                                //
                                $msg = ">"._("Procuration")." ".$row['id_procuration']." ".$row['mandant_nom']." -> ".$row['mandataire_nom'];
                                $this->LogToFile($msg);
                                //
                                $validite = ($row["debut_validite"]!=$row["fin_validite"] ? "-".$this->page->formatdate($row["fin_validite"])."" : "");
                                //
                                $mandant = "- Mandataire ".addslashes($row['mandataire_nom'])." ".addslashes($row['mandataire_prenom'])." (".addslashes($row['mandataire_bureau']).") [".$this->page->formatdate($row["debut_validite"]).$validite."]";
                                $mandataire = "- Mandant ".addslashes($row['mandant_nom'])." ".addslashes($row['mandant_prenom'])." (".addslashes($row['mandant_bureau']).") [".$this->page->formatdate($row["debut_validite"]).$validite."]";
                                // Mandant
                                $procuration_mandant = $this->page->db->getOne("select procuration from electeur where collectivite = '".$_SESSION['collectivite']."' and id_electeur=".$row['mandant']);
                                //
                                if (database::isError($procuration_mandant, true)) {
                                    //
                                    $this->error = true;
                                    //
                                    $message = $procuration_mandant->getMessage()." - ".$procuration_mandant->getUserInfo();
                                    $this->LogToFile($message);
                                    //
                                    $this->addToMessage(_("Contactez votre administrateur."));
                                }
                                //
                                $procuration_mandataire = $this->page->db->getOne("select procuration from electeur where collectivite = '".$_SESSION['collectivite']."' and id_electeur=".$row['mandataire']);
                                //
                                if (database::isError($procuration_mandataire, true)) {
                                    //
                                    $this->error = true;
                                    //
                                    $message = $procuration_mandataire->getMessage()." - ".$procuration_mandataire->getUserInfo();
                                    $this->LogToFile($message);
                                    //
                                    $this->addToMessage(_("Contactez votre administrateur."));
                                }
                                //
                                if ($this->error == false) {
                                    // si une mention existe sur cet electeur on ajoute pas la procuration
                                    if (strpos($procuration_mandant, "***") === false && strpos($procuration_mandataire, "***") === false) {
                                        //
                                        $valF = array("procuration" => ($procuration_mandant != "" ? $procuration_mandant." " : "").$mandant);
                                        $res1 = $this->page->db->autoexecute("electeur", $valF, DB_AUTOQUERY_UPDATE, "id_electeur=".$row['mandant']);
                                        //
                                        if (database::isError($res1, true)) {
                                            //
                                            $this->error = true;
                                            //
                                            $message = $res1->getMessage()." - ".$res1->getUserInfo();
                                            $this->LogToFile($message);
                                            //
                                            $this->addToMessage(_("Contactez votre administrateur."));
                                        }
                                        //
                                        if ($this->error == false) {
                                            // Mandataire
                                            //
                                            $valF = array("procuration" => ($procuration_mandataire != "" ? $procuration_mandataire." " : "").$mandataire);
                                            $res1 = $this->page->db->autoexecute("electeur", $valF, DB_AUTOQUERY_UPDATE, "id_electeur=".$row['mandataire']);
                                            //
                                            if (database::isError($res1, true)) {
                                                //
                                                $this->error = true;
                                                //
                                                $message = $res1->getMessage()." - ".$res1->getUserInfo();
                                                $this->LogToFile($message);
                                                //
                                                $this->addToMessage(_("Contactez votre administrateur."));
                                            }
                                        }
                                    } else {
                                        $msg = "NON APPLIQUEE car mention CV ou ME";
                                        $this->LogToFile($msg);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //
        $this->LogToFile("end election_mention");
    }
}

?>

<?php
/**
 * 
 *
 * @package openmairie_exemple
 * @version SVN : $Id: utils.class.php 294 2010-12-03 08:27:30Z fmichon $
 */

/**
 *
 */
require_once "../dyn/locales.inc.php";

/**
 *
 */
require_once "../dyn/include.inc.php";

/**
 *
 */
require_once "../dyn/debug.inc.php";

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));

/**
 *
 */
require_once PATH_OPENMAIRIE."om_application.class.php";

/**
 *
 */
class utils extends application {

    /**
     * SURCHARGE
     * Cet attribut permet de definir que par defaut si le droit n'existe pas
     * dans la table droit alors l'utilisateur se verra refuser l'acces a
     * l'element.
     */
    var $permission_if_right_does_not_exist = false;

    /**
     * SURCHARGE
     * Ces modifications permettent d'adapter les requetes de login, de gestion
     * des droits a la base specifique de cette application.
     */
    var $table_om_droit = "droit";
    var $table_om_droit_field_id = "droit";
    var $table_om_droit_field_om_profil = "profil";
    var $table_om_utilisateur = "utilisateur";
    var $table_om_utilisateur_field_id = "idutilisateur";
    var $table_om_utilisateur_field_om_collectivite = "collectivite";
    var $table_om_utilisateur_field_om_profil = "profil";
    var $table_om_utilisateur_field_om_type = "type";
    var $table_om_utilisateur_field_login = "login";
    var $table_om_utilisateur_field_password = "pwd";
    var $table_om_profil = "profil";
    var $table_om_profil_field_id = "profil";
    var $table_om_collectivite = "collectivite";
    var $table_om_collectivite_field_id = "id";
    var $table_om_collectivite_field_niveau = "type_interface";

    /**
     * 
     * @var array
     */
    var $vars = array();

    /**
     * SURCHARGE
     * Cette methode permet de definir des parametres supplementaires qui
     * sont stockes dans un fichier de configuration pour qu'ils soient
     * accessibles.
     */
    public function setMoreParams() {
        //
        parent::setMoreParams();
        //
        if (file_exists ("../dyn/var.inc")) {
            require ("../dyn/var.inc");
        }
        //
        $this->vars['debug'] = (isset($debug)?$debug:false);
        $this->vars['demo'] = (isset($demo)?$demo:false);
        $this->vars['session'] = (isset($session)?$session:"openelec");
        $this->vars['code_barre'] = (isset($code_barre)?$code_barre:false);
        $this->vars['pdfdir'] = (isset($pdfdir)?$pdfdir:"../pdfic/");
        $this->vars['tmpdir'] = (isset($tmpdir)?$tmpdir:"../tmp/");
        $this->vars['trsdir'] = (isset($trsdir)?$trsdir:"../trs/");
        $this->vars['chemin_cnen'] = (isset($chemin_cnen)?$chemin_cnen:"../tmp/");
        $this->vars['sans_modification'] = (isset($sans_modification)?$sans_modification:true);
        $this->vars['double_emargement'] = (isset($double_emargement)?$double_emargement:true);
        $this->vars['newline'] = (isset($newline)?$newline:"\n");
        $this->vars['module_voie'] = (isset($module_voie)?$module_voie:true);
        $this->vars['module_decoupage'] = (isset($module_decoupage)?$module_decoupage:true);
        $this->vars['fact_gestionnaire'] = (isset($fact_gestionnaire)?$fact_gestionnaire:"A304");
        $this->vars['fact_attributaire'] = (isset($fact_attributaire)?$fact_attributaire:"A304");
        $this->vars['fact_chapitre'] = (isset($fact_chapitre)?$fact_chapitre:"70/7087801");
        $this->vars['fact_electeur_unitaire'] = (isset($fact_electeur_unitaire)?$fact_electeur_unitaire:.28);
        $this->vars['fact_forfait'] = (isset($fact_forfait)?$fact_forfait:30.49);
        $this->vars['option_tri_liste_emargement'] = (isset($option_tri_liste_emargement)?$option_tri_liste_emargement:"alpha");
        $this->vars['option_generation_as_an_archive'] = (isset($option_generation_as_an_archive)?$option_generation_as_an_archive:"true");
        $this->vars['option_code_voie_auto'] = (isset($option_code_voie_auto)?$option_code_voie_auto:"true");
    }
    
    /**
     * Cette
     */
    function isMulti() {
        if ($this->collectivite['type_interface'] == 0) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     *
     */
    function getRight($obj = NULL) {
        
        if (isset($this->rights[$obj])) {
            return $this->rights[$obj];
        } else {
            return false;
        }
        
    }
    
    /**
     *
     */
    function addRight($obj) {
        $sql = "insert into droit (droit,profil) values('".$obj."', 5)";
        $res = $this->db->query($sql);
        $this->addToLog("addRight()[obj/utils.class.php]: db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->isDatabaseError($res);
    }
    
    /**
     *
     */
    public function isAccredited($obj = NULL, $operator = "AND") {
        
        // Si on se trouve sur une collectivite en mode multi
        if ($_SESSION['multi_collectivite'] == 1 || $_SESSION['multi_utilisateur'] == 1) {
            
            if ($_SESSION['multi_utilisateur'] == 1) {
                $suffixe = "_multi_utilisateur";
            }
            if ($_SESSION['multi_collectivite'] == 1) {
                $suffixe = "_multi";
            }
            
            // Si l'objet a verifier dans les droits est une liste d'objets a verifier
            if (is_array($obj)) {
                
                // 
                if (count($obj) != 0) {
                    
                    //
                    $permission_temporary = NULL;
                    
                    // Pour chaque element du tableau d'objet a verifier
                    foreach ($obj as $elem) {
                        
                        // Si l'objet a verifier concatene avec le suffixe _multi a une
                        // correspondance dans la table des droits
                        $truc = $elem.$suffixe;
                        if (!isset($this->rights[$truc])) {
                            
                            if (isset($this->rights[$elem])) {
                                if ($this->rights[$elem] <= $_SESSION['profil']) {
                                    $permission_to_apply = true;
                                } else {
                                    $permission_to_apply = false;
                                }
                            } else {
                                $permission_to_apply = $this->permission_if_right_does_not_exist;
                            }
                            
                        } else {
                            
                            if ($this->rights[$truc] <= $_SESSION['profil']) {
                                $permission_to_apply = true;
                            } else {
                                $permission_to_apply = false;
                            }
                            
                        }
                        //
                        if ($permission_temporary == NULL) {
                            $permission_temporary = $permission_to_apply;
                        } else {
                            if ($operator == "OR") {
                                $permission_temporary |= $permission_to_apply;
                            } else {
                                $permission_temporary &= $permission_to_apply;
                            }
                        }
                        
                    }
                    
                    //
                    return $permission_temporary;
                    
                }
                
            } else { // Si l'objet a verifier dans les droits est une chaine de caracteres
                
                // Si l'objet a verifier concatene avec le suffixe _multi a une
                // correspondance dans la table des droits
                $truc = $obj.$suffixe;
                if (isset($this->rights[$truc])) {
                    
                    // Si l'utilisateur est autorise, on retourne true
                    if ($this->rights[$truc] <= $_SESSION['profil']) {
                        return true;
                    } else { // Si l'utilisateur n'est pas autorise, on retourne false
                        return false;
                    }
                    
                }
            }
            
        }
        
        // Si on est rentre dans aucun des cas ci-dessus on appelle la methode
        // parent pour verifier les droits d'une maniere standard
        return parent::isAccredited($obj, $operator);
        
    }
    
    /**
     *
     */
    function displayActionCollectivite() {
        if ($_SESSION['multi_utilisateur'] == true) {
            echo " (MULTI)";
        }
        echo "\t\t\t&nbsp;|&nbsp;\n";
        echo "\t\t\t";
        $show_changecollectivite = false;
        if ($this->isAccredited(/*DROIT*/'collectivitedefaut')) {
            include_once("../obj/workcollectivite.class.php");
            $workcollectivite = new workCollectivite($this);
            if ($workcollectivite->countCollectivites() > 1) {
                $show_changecollectivite = true;
            }
        }
        if ($show_changecollectivite) {
            echo "<a href=\"../app/changecollectivite.php\" ";
            echo "title=\""._("Collectivite de travail")."\">";
        }
        echo $_SESSION['libelle_collectivite'];
        if ($show_changecollectivite) {
            echo "</a>";
        }
        if ($_SESSION['multi_collectivite'] == true) {
            echo " (MULTI)";
        }
        echo "\n";
    }
    
    /**
     *
     */
    function displayActionExtras() {

        if ($this->isAccredited(/*DROIT*/'listedefaut')) {
            include_once("../obj/workliste.class.php");
            $workliste = new workListe($this);
            if ($workliste->countListes() > 1) {
                echo "\t\t\t&nbsp;|&nbsp;\n";
                echo "\t\t\t";
                echo "<a href=\"../app/changeliste.php\" ";
                echo "title=\""._("Liste de travail")."\">";
                echo _("Liste")." = ".$_SESSION['liste']." ".$_SESSION['libelle_liste'];
                echo "</a>";
                echo "\n";
            }
        }
    }
    


    /**
     *
     */
    function notExistsError ($explanation = NULL) {
        // message
        $message_class = "error";
        $message = _("Cette page n'existe pas.");
        $this->addToMessage ($message_class, $message);
        //
        if ($this->vars['debug']) {
            // message
            $message_class = "debug";
            if ($explanation != NULL) {
                $message = $explanation;
            } else {
                $message = _("Les parametres sont mauvais.");
            }
            $this->addToMessage ($message_class, $message);
        }
        
        //
        $this->setFlag(NULL);
        $this->display();
        
        //
        die();
    }

    function triggerAfterLogin($utilisateur = NULL) {
        // Cette varibale n'est pas utilisée mais est nécessaire
        // dans le framework openmairie, elle est remplie au moment du login
        // par la valeur du champ type_interface qui correspond lui au bon
        // marqueur de multi collectivité mais n'est pas utilisée dans openelec
        $_SESSION['niveau'] = NULL;
        //
        $_SESSION['liste'] = $utilisateur['listedefaut'];
        $_SESSION['collectivite'] = $utilisateur['collectivite'];
        //
        $sql = "select libelle_liste from liste where liste ='".$_SESSION ['liste']."'";
        $lib = $this -> db -> getOne ($sql);
        if (isset ($lib)) {
            $_SESSION ['libelle_liste'] = $lib;
        } else {
            $_SESSION ['libelle_liste'] = "non d&eacute;fini";
        }
        //
        $sql = "select ville from collectivite where id ='".$_SESSION ['collectivite']."'";
        $lib = $this -> db -> getOne ($sql);
        if (isset ($lib)) {
            $_SESSION ['libelle_collectivite'] = $lib;
        } else {
            $_SESSION ['libelle_collectivite'] = "non d&eacute;fini";
        }
        //

        //
        $sql = "select type_interface from collectivite where id = '".$_SESSION['collectivite']."' ";
        $multi = $this->db->getone ($sql);
        if ($multi == 0) {
            $_SESSION['multi_utilisateur'] = 0;
            $_SESSION['multi_collectivite'] = 0;
        } else {
            $_SESSION['multi_utilisateur'] = 1;
            $_SESSION['multi_collectivite'] = 1;
        }

    }
    
    /**
     *
     */
    function getCollectivite () {
        /* ++ */ $sql = "select * from collectivite where id = '".$_SESSION['collectivite']."' ";
        $res = $this->db->query ($sql);
        if (database::isError($res))
            die ($res->getMessage ());
        if ($res->numrows() == 1) {
            $row =& $res->fetchRow (DB_FETCHMODE_ASSOC);
            $this->collectivite = $row;
            $res->free();
        } else {
            $message_class = "error";
            $message = "Erreur table collectivite.";
            $this->addToMessage ($message_class, $message);
        }
    }
    
    /**
     * Cette méthode permet de renvoyer la valeur d'un paramètre de
     * l'application, on utilise cette méthode car les paramètres peuvent
     * provenir de différentes sources :
     *   - le fichier dyn/var.inc
     *   - le fichier dyn/config.inc.php
     *   - la table om_parametre
     * En regroupant la récupération des paramètres dans une seule méthode :
     *  - on évite les erreurs
     *  - on peut se permettre de gérer des comportements
     * complexes comme : si le paramètre n'est pas disponible pour la
     * collectivité alors on va chercher dans la collectivité de niveau
     * supérieur.
     *  - on est indépendant du stockage de ces paramètres.
     *
     * Si on ne trouve pas de paramètre correspondant alors on retourne NULL
     */
    function getParameter($param = NULL) {
        //
        if ($param == NULL) {
            return NULL;
        }
        //
        if (isset($this->vars[$param])) {
            return $this->vars[$param];
        }
        //
        return NULL;
    }


    // {{{ DISPLAY
    
    function displayLinksAsList($links = array()) {
        //
        echo "\t<div class=\"list\">\n";
        foreach ($links as $link) {
            //
            echo "<div class=\"choice ui-corner-all ui-widget-content\">\n";
            echo "<span>\n";
            //
            echo "<a";
            echo " href=\"".$link["href"]."\"";
            echo (isset($link["class"]) ? " class=\"".$link["class"]."\"" : "");
            echo (isset($link["target"]) ? " target=\"".$link["target"]."\"" : "");
            echo (isset($link["description"]) ? " title=\"".$link["description"]."\"" : "");
            echo ">";
            echo $link["title"];
            echo "</a>\n";
            //
            echo "</span>\n";
            echo "</div>\n";
        }
        echo "</div>\n";
        echo "<div class=\"both\"><!-- --></div>";
    }
    
    // }}}
    
    /**
     *
     */
    public function normalizeString($string_to_normalize = ""){
        //
        $string_to_normalize = str_replace(" ", "", $string_to_normalize);
        $string_to_normalize = str_replace("/", "-", $string_to_normalize);
        return $string_to_normalize;
    }

    public function getPathFolderTrs() {
        return $this->vars["trsdir"];
    }

    /**
     * SURCHARGE
     * Lors de la synchronisation des utilisateurs avec un annuaire on definit
     * une valeur par defaut pour certains parametres.
     */
    function getValFUserToAdd($user) {
        // Logger
        $this->addToLog("getValFUserToAdd()[obj/utils.class.php]: "._("Surcharge"), EXTRA_VERBOSE_MODE);
        //
        $valF = parent::getValFUserToAdd($user);
        //
        $valF["listedefaut"] = "01";
        //
        return $valF;
    }
    
    //
    function startswith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }
    
    function endswith($haystack, $needle)
    {
        $length = strlen($needle);
        $start  = $length * -1; //negative
        return (substr($haystack, $start) === $needle);
    }

}

?>

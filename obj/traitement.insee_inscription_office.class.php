<?php
/**
 * Ce fichier declare la classe inseeInscriptionOfficeTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class inseeInscriptionOfficeTraitement extends traitement {
    
    var $fichier = "insee_inscription_office";
    
    var $champs = array("fic1");
    var $form_name = "f1";
    
    function getValidButtonValue() {
        //
        return _("Import des inscriptions d'office INSEE");
    }
    
    function setContentForm() {
        $this->form->setLib("fic1", _("Fichier recu de l'INSEE a importer"));
        $this->form->setType("fic1", "upload");
    }
    
    function treatment () {
        //
        $this->LogToFile("start insee_inscription_office");
        //
        (isset($_POST['fic1']) ? $fic = $this->page->getParameter("trsdir").$_POST['fic1'] : $fic = "");
        //
        if ($_POST['fic1'] == "" || !file_exists($fic)) {
            //
            $this->error = true;
            //
            $message = _("Le fichier n'est pas valide.");
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Vous devez selectionner un fichier valide."));
        }
        //
        if ($this->error == false) {
            //
            $ext = pathinfo($_POST['fic1'], PATHINFO_EXTENSION);
            //
            if (strtolower($ext) != "txt" && strtolower($ext) != "xml") {
                //
                $this->error = true;
                //
                $message = _("L'extension du fichier n'est pas valide.");
                $this->LogToFile($message);
                //
                $this->addToMessage(_("Le format du fichier n'est pas correct."));
            }
            //
            if ($this->error == false) {
                //
                $this->LogToFile(_("Fichier :")." ".$fic);
                //
                $this->LogToFile(_("Extension :")." ".$ext);
                //
                switch ($ext) {
                    /**
                     * IMPORT XML
                     */
                    case "xml":
                    //
                    require_once "../obj/insee_import.class.php";
                    //
                    $importobj = new insee_import();
                    $importobj->import("inscription_office", $fic, $this->page);
                    //
                    break;
                    /**
                     * IMPORT TXT
                     */
                    case "txt":
                    // Compteur
                    $total_lines = 0;
                    $empty_lines = 0;
                    //
                    $fichier = fopen($fic, "r");
                    //
                    while (!feof($fichier)) {
                        // Compteur
                        $total_lines++;
                        // Recuperation du contenu de la ligne
                        $contenu = fgets($fichier, 4096);
                        // Si la ligne n'est pas vide
                        if ($contenu != "") {
                            // Initialisation de la collectivite
                            $valF["collectivite"] = $_SESSION['collectivite'];
                            //
                            $valF['nom'] = trim(substr($contenu, 5, 63));
                            $valF['nom_usage'] = trim(substr($contenu, 68, 63));
                            $valF['prenom'] = trim(substr($contenu, 131, 50));
                            $valF['sexe'] = trim(substr($contenu, 181, 1));
                            // Initialisation du sexe et de la civilite en
                            // fonction de la valeur recuperee dans le
                            // fichier
                            if ($valF['sexe'] == "1") {
                                $valF['sexe'] = "M";
                                $valF['civilite'] = "M.";
                            } else {
                                $valF['sexe'] = "F";
                                $valF['civilite'] = "Mlle";
                            }
                            //
                            $valF['date_naissance'] = trim(substr($contenu, 182, 8));
                            $valF['code_lieu_de_naissance'] = trim(substr($contenu, 190, 2))." ".trim(substr($contenu, 192, 3));
                            $valF['libelle_lieu_de_naissance'] = trim(substr($contenu, 195, 30));
                            $valF['code_departement_naissance'] = trim(substr($contenu, 190, 2));
                            // Initialisation du code departement de
                            // naissance en fonction de la valeur recuperee
                            // dans le fichier
                            if ($valF['code_departement_naissance']=='99') {
                                $valF['code_departement_naissance'] = trim(substr($contenu, 190, 5));
                            }
                            if ($valF['code_departement_naissance']=='97') {
                                $valF['code_departement_naissance'] = trim(substr($contenu, 190, 3));
                            }
                            if ($valF['code_departement_naissance']=='98') {
                                $valF['code_departement_naissance'] = trim(substr($contenu, 190, 3));
                            }
                            //
                            $valF['libelle_departement_naissance'] = trim(substr($contenu, 225, 30));
                            // Initialisation du libelle departement de
                            // naissance en fonction de la valeur recuperee
                            // dans le fichier
                            if ($valF['libelle_departement_naissance'] == "") {
                                //
                                include "../sql/".$this->page->phptype."/trt_insee_inscription_office.inc";
                                //
                                $libelle_departement = $this->page->db->getOne($sqlc);
                                //
                                if (database::isError($libelle_departement, true)) {
                                    //
                                    $this->error = true;
                                    //
                                    $message = $libelle_departement->getMessage()." - ".$libelle_departement->getUserInfo();
                                    $this->LogToFile($message);
                                    //
                                    $this->addToMessage(_("Contactez votre administrateur."));
                                    //
                                    break;
                                }
                                //
                                if (isset($libelle_departement)) {
                                    $valF['libelle_departement_naissance'] = $libelle_departement;
                                }
                            }
                            //
                            $valF['adresse1'] = trim(substr($contenu, 255, 38));
                            $valF['adresse2'] = trim(substr($contenu, 293, 38));
                            $valF['adresse3'] = trim(substr($contenu, 331, 38));
                            $valF['adresse4'] = trim(substr($contenu, 369, 38));
                            $valF['adresse5'] = trim(substr($contenu, 407, 38));
                            $valF['adresse6'] = trim(substr($contenu, 445, 38));
                            $valF['idcnen'] = trim(substr($contenu, 483, 30));
                            //
                            $message = _("Ligne :")." ".$total_lines." - ".print_r($valF, true);
                            $this->LogToFile($message);
                            //
                            $valF['id'] = $this->page->db->nextId("inscription_office");
                            //
                            $res = $this->page->db->autoExecute("inscription_office", $valF, DB_AUTOQUERY_INSERT);
                            //
                            if (database::isError($res, true)) {
                                //
                                $this->error = true;
                                //
                                $message = $res->getMessage()." - ".$res->getUserInfo();
                                $this->LogToFile($message);
                                //
                                $this->addToMessage(_("Contactez votre administrateur."));
                                //
                                break;
                            }
                        } else {
                            //
                            $message = _("Ligne :")." ".$total_lines." - "._("VIDE");
                            $this->LogToFile($message);
                            //
                            $empty_lines++;
                        }
                    }
                    // Fermeture fichier
                    fclose($fichier);
                    //
                    if ($this->error == false) {
                        $message = ($total_lines - $empty_lines)." "._("inscriptions d'office INSEE importees");
                        $this->addToMessage($message);
                        $this->LogToFile($message);
                    }
                    //
                    break;
                }
            }
        }
        //
        $this->LogToFile ("end insee_inscription_office");
    }
}

?>

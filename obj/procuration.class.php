<?php
/**
 * Ce fichier permet de definir la classe procuration
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class procuration extends dbForm {
    
    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "procuration";
    
    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "id_procuration";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "N";
    
    /**
     * Constructeur 
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function procuration($id, &$db, $DEBUG) {
        //
        $this->constructeur($id, $db, $DEBUG);
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en vue de l'insertion des donnees dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setvalF($val) {
        //
        $this->valF["id_procuration"] = $val["id_procuration"];
        $this->valF["date_modif"] = $this->dateSystemeDB();
        $this->valF["utilisateur"] = $_SESSION["login"];
        $this->valF["mandant"] = $val["mandant"];
        $this->valF["mandataire"] = $val["mandataire"];
        $this->valF["debut_validite"] = $val["debut_validite"];
        $this->valF["fin_validite"] = $val["fin_validite"];
        $this->valF["types"] = $val["types"];
        $this->valF["origine1"] = $val["origine1"];
        $this->valF["origine2"] = $val["origine2"];
        $this->valF["refus"] = $val["refus"];
        $this->valF["motif_refus"] = $val["motif_refus"];
        $this->valF["date_accord"] = $val["date_accord"];
        $this->valF["heure_accord"] = $val["heure_accord"];
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en ajouter la cle primaire en vue de l'insertion des donnees
     * dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setValFAjout($val) {
        // Id automatique donc cette initialisation n'est pas necessaire
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * de recuperer l'element suivant dans la sequence pour les id automatiques.
     * 
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function setId(&$db) {
        //
        $this->valF[$this->clePrimaire] = $db->nextId($this->table);
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * d'effectuer des tests d'integrite sur la cle primaire pour verifier
     * si la cle a bien ete saisie dans le formulaire et si l'objet ajoute
     * n'existe pas deja dans la table pour en empecher l'ajout.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function verifierAjout($val, &$db) {
        // Id automatique donc cette verification n'est pas necessaire
    }
    
    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function verifier($val, &$db, $DEBUG) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // obligatoire
        if ($this->valF['mandant']=="") {
            $this->correct=false;
            $this->msg .= _("Vous devez selectionner un mandant")."<br />";
        } else {
            if (!is_numeric ($this->valF['mandant'])) {
                $this->correct=false;
                $this->msg .= _("L'identifiant du mandant l'electeur doit etre valide")."<br />";
            } else {
                // Test 1 si l'electeur existe
                /* ++ */ $sql = "select * from electeur where id_electeur='".$this->valF['mandant']."' AND collectivite = '".$_SESSION['collectivite']."' ";
                $res = $db -> query ($sql);
                if($DEBUG==1) echo $sql;
                if (database::isError($res))
                    $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                else {
                    $nbligne = $res->numrows(); //XXX
                    if ($nbligne==0) {
                        $this->correct=false;
                        $this->msg .= _("Le mandant ").$this->valF['mandant']._(" n'existe pas")."<br />";
                    }
                }
                if ($this -> correct) {
                    $sql= "select (nom||' - '||prenom||' - '||to_char(date_naissance,'DD/MM/YYYY')||' - '||code_bureau) as z_mandant from electeur where collectivite = '".$_SESSION['collectivite']."' and id_electeur='".$this->valF['mandant']."'";
                    $res = $db -> query ($sql);
                    if($DEBUG==1) echo $sql;
                    if (database::isError($res))
                        $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                    else {
                        $row = $res -> fetchrow (DB_FETCHMODE_ASSOC);
                        if (mb_strtoupper($row ['z_mandant']) != $val['z_mandant']) {
                            $this->correct=false;
                            $this->msg .= _("La correspondance entre les deux champs Identifiant / Nom du mandant n'est pas correcte")."<br />";
                        }
                    }
                }
            }
        }

        if ($this->valF['mandataire']=="") {
            $this->correct=false;
            $this->msg .= _("Vous devez selectionner un mandataire")."<br />";
        } else {
            if (!is_numeric ($this->valF['mandataire'])) {
                $this->correct=false;
                $this->msg .= _("L'identifiant du mandataire doit etre valide")."<br />";
            } else {
                // Test 2 si l'electeur existe
                $sql = "select * from electeur where id_electeur='".$this->valF['mandataire']."' AND collectivite = '".$_SESSION['collectivite']."' ";
                $res = $db -> query ($sql);
                if($DEBUG==1) echo $sql;
                if (database::isError($res))
                    $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                else {
                    $nbligne = $res->numrows(); //XXX
                    if ($nbligne==0) {
                        $this->correct=false;
                        $this->msg .= _("Le mandataire ").$this->valF['mandataire']._(" n'existe pas")."<br />";
                    }
                }
                if ($this -> correct) {
                    $sql= "select (nom||' - '||prenom||' - '||to_char(date_naissance,'DD/MM/YYYY')||' - '||code_bureau) as z_mandataire from electeur where collectivite = '".$_SESSION['collectivite']."' and id_electeur='".$this->valF['mandataire']."'";
                    $res = $db -> query ($sql);
                    if($DEBUG==1) echo $sql;
                    if (database::isError($res))
                        $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                    else {
                        $row = $res -> fetchrow (DB_FETCHMODE_ASSOC);
                        if (mb_strtoupper($row ['z_mandataire']) != $val['z_mandataire']) {
                            $this->correct=false;
                            $this->msg .= _("La correspondance entre les deux champs Identifiant / Nom du mandataire n'est pas correcte")."<br />";
                        }
                    }
                }
            }
        }
        //
        if ($this->valF['debut_validite']=="") {
            $this->correct=false;
            $this->msg .= _("La date de debut de validite est obligatoire")."<br />";
        } else {
            $this->valF['debut_validite'] = $this->dateDB($this->valF['debut_validite']);
        }
        if ($this->valF['fin_validite']=="") {
            $this->valF['fin_validite'] = $this->valF['debut_validite'];
        } else {
            $this->valF['fin_validite'] = $this->dateDB($this->valF['fin_validite']);
        }
        // validite superieur a 1 jour
        if ($this->valF['debut_validite']>$this->valF['fin_validite']) {
            $this->correct=false;
            $this->msg .= _("La date de debut de validite doit etre superieure a la date de fin")."<br />";
        }
        //
        if ($this->valF['date_accord']=="") {
            $this->correct=false;
            $this->msg .= _("La date d'accord est obligatoire")."<br />";
        } else {
            $this->valF['date_accord'] = $this->dateDB($this->valF['date_accord']);
        }
        if ($this->valF['heure_accord']=="") {
            $this->correct=false;
            $this->msg .= _("L'heure d'accord est obligatoire")."<br />";
        } else {
            $this->valF['heure_accord'] = $this->heureDB($this->valF['heure_accord']);
        }
        //
        if ($this->valF['mandant']==$this->valF['mandataire']) {
            $this->correct=false;
            $this->msg .= _("Le mandant est le meme que le mandataire")."<br />";
        }
        if ($this->valF['refus']== "O" AND $this->valF['motif_refus'] == "") {
            $this->correct=false;
            $this->msg .= _("Le motif de refus est obligatoire si la procuration est refusee")."<br />";
        }
        // module de verification des procurations ===============================
        // 1 mandant a droit qu a 1 procuration pour une meme periode
        // 1 mandataire a droit a :
        //              1 procuration en france et/ou 1 hors france
        //           ou   2 procurations hors france
        // =======================================================================
        if ($this->correct AND $this->valF['refus'] == 'N') {
            // verification du mandant sur la periode => 1 seule fois mandant
            $sql = "select * from procuration where mandant =".$this->valF['mandant']."";
            $sql .= " and (fin_validite >= '".$this->valF['debut_validite']."'";
            $sql .= " and debut_validite <= '".$this->valF['fin_validite']."') ";
            $sql .= " and refus <> 'O' ";
            if ($this -> valF ['id_procuration'] != "")
                $sql .= "and id_procuration<>'".$val['id_procuration']."'";
            $res = $db->query($sql);
            if (database::isError($res))
                $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
            else {
                $nbligne=$res->numrows();
                if ($nbligne > 0) {
                    $this->correct=false;
                    $this->msg .= _("L'electeur numero [").$this->valF['mandant']._("] est ");
                    $this->msg .= _("deja mandant pour la meme periode")."<br />";
                }
            }
            //
            if ($val['types'] == "EF") {
                // verification du mandataire sur la periode => (1EF) + (1EF.1HF) + (1HF) + (2HF)
                $sql = "select * from procuration where mandataire =".$this->valF['mandataire']."";
                $sql .= " and (fin_validite >= '".$this->valF['debut_validite']."'";
                $sql .= " and debut_validite <= '".$this->valF['fin_validite']."')";
                $sql .= " and refus <> 'O' ";
                if ($this -> valF ['id_procuration'] != "")
                    $sql .= "and id_procuration<>'".$val['id_procuration']."'";
                $sql .= " and types='EF'";
                $res = $db->query($sql);
                if (database::isError($res))
                    $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                else {
                    $nbligne=$res->numrows();
                    if ($nbligne > 0) {
                        $this->msg .= _("L'electeur numero [").$this->valF['mandataire']."] ";
                        $this->msg .= _("est deja mandataire pour ").$nbligne._(" procuration(s) EN FRANCE pour la meme periode")."<br />";
                        $this->correct=false;
                    }
                }
                // verification du mandataire sur la periode => (1EF) + (1EF.1HF) + (1HF) + (2HF)
                $sql = "select * from procuration where mandataire =".$this->valF['mandataire']."";
                $sql .= " and (fin_validite >= '".$this->valF['debut_validite']."'";
                $sql .= " and debut_validite <= '".$this->valF['fin_validite']."')";
                if ($this -> valF ['id_procuration'] != "")
                    $sql .= "and id_procuration<>'".$val['id_procuration']."'";
                $sql .= " and types='HF'";
                $nbmax = 0;
                $res = $db->query($sql);
                if (database::isError($res))
                    $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                else {
                    $nbligne=$res->numrows();
                    if ($nbligne > 1) {
                        $this->msg .= _("L'electeur numero [").$this->valF['mandataire']."] ";
                        $this->msg .= _("est deja mandataire pour ").$nbligne._(" procuration(s) HORS FRANCE pour la meme periode")."<br />";
                        $this->correct=false;
                    }
                }
            }
            if ($val['types'] == "HF") {
                // verification du mandataire sur la periode => (1EF) + (1EF.1HF) + (1HF) + (2HF)
                $sql = "select * from procuration where mandataire =".$this->valF['mandataire']."";
                $sql .= " and (fin_validite >= '".$this->valF['debut_validite']."'";
                $sql .= " and debut_validite <= '".$this->valF['fin_validite']."')";
                $sql .= " and refus <> 'O' ";
                if ($this -> valF ['id_procuration'] != "")
                    $sql .= "and id_procuration<>'".$val['id_procuration']."'";
                $sql .= " and types='EF'";
                $res = $db->query($sql);
                if (database::isError($res))
                    $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                else {
                    $nbligne_ef=$res->numrows();
                    if ($nbligne_ef > 1) {
                        $this->msg .= _("L'electeur numero [").$this->valF['mandataire']."] ";
                        $this->msg .= _("est deja mandataire pour ").$nbligne_ef._(" procuration(s) EN FRANCE pour la meme periode")."<br />";
                        $this->correct=false;
                    }
                }
                // verification du mandataire sur la periode => (1EF) + (1EF.1HF) + (1HF) + (2HF)
                $sql = "select * from procuration where mandataire =".$this->valF['mandataire']."";
                $sql .= " and (fin_validite >= '".$this->valF['debut_validite']."'";
                $sql .= " and debut_validite <= '".$this->valF['fin_validite']."')";
                $sql .= " and refus <> 'O' ";
                if ($this -> valF ['id_procuration'] != "")
                    $sql .= "and id_procuration<>'".$val['id_procuration']."'";
                $sql .= " and types='HF'";
                $res = $db->query($sql);
                if (database::isError($res))
                    $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                else {
                    $nbligne_hf=$res->numrows();
                    if ($nbligne_hf > 1) {
                        $this->msg .= _("L'electeur numero [").$this->valF['mandataire']."] ";
                        $this->msg .= _("est deja mandataire pour ").$nbligne_hf._(" procuration(s) HORS FRANCE pour la meme periode")."<br />";
                        $this->correct=false;
                    }
                }
                //
                if ($nbligne_hf + $nbligne_ef >= 2) {
                    $this->msg .= _("L'electeur numero [").$this->valF['mandataire']."] ";
                    $this->msg .= _("est deja mandataire pour ").$nbligne_ef._(" procuration(s) EN FRANCE")." ";
                    $this->msg .= _("et pour ").$nbligne_hf._(" procuration(s) HORS FRANCE pour la meme periode")."<br />";
                    $this->correct=false;
                }
            }
            if($this->correct === false) {
                $this->msg .= _("Cette procuration peut etre refusee afin de valider le formulaire")."<br />";
            }
        }
    }
    
    /**
     * Parametrage du formulaire - Groupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setGroupe(&$form, $maj) {
        //
        $form->setGroupe("debut_validite", "D");
        $form->setGroupe("fin_validite", "F");
        //
        $form->setGroupe("date_accord", "D");
        $form->setGroupe("heure_accord", "F");
        //
        $form->setGroupe("mandant", "D");
        $form->setGroupe("z_mandant", "F");
        //
        $form->setGroupe("mandataire", "D");
        $form->setGroupe("z_mandataire", "F");
        //
        if ($maj != 0) {
            $form->setGroupe("date_modif", "D");
            $form->setGroupe("utilisateur", "F");
        }
    }
    
    /**
     * Parametrage du formulaire - Regroupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setRegroupe(&$form, $maj) {
        //
        if ($maj != 0) {
            $form->setRegroupe("id_procuration","D",_("Identification"));
            $form->setRegroupe("date_modif","G","");
            $form->setRegroupe("utilisateur","F","");
        }
        //
        $form->setRegroupe("mandant","D",_("Electeurs (mandant et mandataire)"));
        $form->setRegroupe("z_mandant","G","");
        $form->setRegroupe("mandataire","G","");
        $form->setRegroupe("z_mandataire","F","");
        //
        $form->setRegroupe("debut_validite","D",_("Validite de la procuration"));
        $form->setRegroupe("fin_validite","F","");
        //
        $form->setRegroupe("types","D",_("Caracteristiques"));
        $form->setRegroupe("origine1","G","");

        $form->setRegroupe("origine2","G","");
        $form->setRegroupe("refus","G","");
        $form->setRegroupe("motif_refus","F","");
        //
        $form->setRegroupe("date_accord", "D", _("Date et heure d'accord"));
        $form->setRegroupe("heure_accord", "F", "");
    }
    
    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType(&$form, $maj) {
        //
        if ($maj < 2) { // ajouter et modifier
            $form->setType('types','select');
            $form->setType('debut_validite','date');
            $form->setType('fin_validite','date');
            $form->setType('date_accord','date');
            $form->setType('z_mandant','comboG');
            $form->setType('z_mandataire','comboG');
            $form->setType('mandant','comboD');
            $form->setType('mandataire','comboD');
            if ($maj == 1) { // modifier
                $form->setType('id_procuration','hiddenstatic');
                $form->setType('date_modif','hiddenstatic');
                $form->setType('utilisateur','hiddenstatic');
            } else { // ajouter
                $form->setType('id_procuration','hidden');
                $form->setType('date_modif','hidden');
                $form->setType('utilisateur','hidden');
            }
            $form->setType('refus','select');
        } else { // supprimer
            $form->setType('id_procuration','hiddenstatic');
        }
    }
    
    /**
     * Parametrage du formulaire - Select des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function setSelect(&$form, $maj, $db, $DEBUG) {
        //
        if ($maj < 2) {
            // SELECT
            // type
            $contenu = array();
            $contenu[0] = array("EF", "HF");
            $contenu[1] = array(_("En France"), _("Hors France"));
            $form->setSelect("types", $contenu);
            // refus
            $contenu = array();
            $contenu[0] = array("N", "O");
            $contenu[1] = array(_("Non"), _("Oui"));
            $form->setSelect("refus", $contenu);
            // CORREL
            // mandant
            $contenu = array();
            $contenu[0][0] = "electeur";
            $contenu[0][1] = "id_electeur";
            $contenu[1][0] = "nom";
            $contenu[1][1] = "z_mandant";
            $form->setSelect("mandant", $contenu);
            // mandataire
            $contenu = array();
            $contenu[0][0] = "electeur";
            $contenu[0][1] = "id_electeur";
            $contenu[1][0] = "nom";
            $contenu[1][1] = "z_mandataire";
            $form->setSelect("mandataire", $contenu);
            // z_mandant
            $contenu = array();
            $contenu[0][0] = "electeur";
            $contenu[0][1] = "nom";
            $contenu[1][0] = "id_electeur";
            $contenu[1][1] = "mandant";
            $form->setSelect("z_mandant", $contenu);
            // z_mandataire
            $contenu = array();
            $contenu[0][0] = "electeur";
            $contenu[0][1] = "nom";
            $contenu[1][0] = "id_electeur";
            $contenu[1][1] = "mandataire";
            $form->setSelect("z_mandataire", $contenu);
        }
    }
    
    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setOnchange(&$form, $maj) {
        //
        $form->setOnchange("z_mandant", "this.value=this.value.toUpperCase()");
        $form->setOnchange("z_mandataire", "this.value=this.value.toUpperCase()");
        $form->setOnchange("debut_validite", "fdate(this)");
        $form->setOnchange("fin_validite", "fdate(this)");
        $form->setOnchange("date_accord", "fdate(this)");
        $form->setOnchange("heure_accord", "ftime(this)");
        $form->setOnchange("refus", "triggerMotifRefus()");
    }
    
    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        $form->setLib("id_procuration", _("Procuration"));
        $form->setLib("debut_validite", _("du "));
        $form->setLib("fin_validite", _(" au "));
        $form->setLib("z_mandant", "");
        $form->setLib("z_mandataire", "");
        $form->setLib("mandant", _("Identifiant / Nom du mandant "));
        $form->setLib("mandataire", _("Identifiant / Nom du mandataire "));
        $form->setLib("date_accord", _("Date "));
        $form->setLib("heure_accord", _("Heure "));
        $form->setLib("date_modif", _("Modifie le "));
        $form->setLib("utilisateur", _(" par "));
        $form->setLib("refus", _("Refus"));
        $form->setLib("motif_refus", _("Motif de refus"));
    }
    
    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille(&$form, $maj) {
        //
        $form->setTaille("origine1", 50);
        $form->setTaille("origine2", 50);
        $form->setTaille("refus", 2);
        $form->setTaille("motif_refus", 50);
        $form->setTaille("z_mandant", 50);
        $form->setTaille("z_mandataire", 50);
        $form->setTaille("debut_validite", 10);
        $form->setTaille("fin_validite", 10);
        $form->setTaille("date_accord", 10);
    }
    
    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax(&$form, $maj) {
        //
        $form->setMax("origine1", 50);
        $form->setMax("origine2", 50);
        $form->setMax("refus", 2);
        $form->setMax("motif_refus", 65);
        $form->setMax("z_mandant", 90);
        $form->setMax("z_mandataire", 90);
    }
    
}

?>
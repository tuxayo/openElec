<?php
/**
 * Ce fichier declare la classe inseeEpurationInscriptionOfficeTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 * @todo Optimiser la methode de suppression : une requete suffit 
 */
class inseeEpurationInscriptionOfficeTraitement extends traitement {
    
    var $fichier = "insee_epuration_inscription_office";
    
    function getValidButtonValue() {
        //
        return _("Epuration des inscriptions d'office INSEE");
    }
    
    function getDescription() {
        //
        return _("Lors de l'import des inscriptions d'office INSEE, toutes les ".
                 "propositions de l'INSEE sont stockees dans une table temporaire ".
                 "jusqu'a validation. Ce traitement supprime toutes ces inscriptions ".
                 "d'office temporaires.");
    }

    function displayBeforeContentForm() {
        //
        include "../sql/".$this->page->phptype."/trt_insee_epuration.inc";
        //
        $res_inscriptionoffice = $this->page->db->query ($sql_IO);
        $this->page->isDatabaseError($res_inscriptionoffice);
        $nb_inscriptionoffice = $res_inscriptionoffice->numRows ();
        $res_inscriptionoffice->free ();
        //
        echo "\n<div class=\"field\">\n\t<label>";
        echo _("Le nombre d'inscriptions d'office dans la table temporaire a la date du")." ";
        echo date('d/m/Y')." "._("est de")." ".$nb_inscriptionoffice.".";
        echo "</label>\n</div>\n";
    }

    function treatment () {
        //
        $this->LogToFile("start insee_epuration_inscription_office");
        include "../sql/".$this->page->phptype."/trt_insee_epuration.inc";
        //
        $res = $this->page->db->query($sql_IO);
        //
        if (database::isError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        } else {
            //
            $i = 0;
            //
            while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                include "../sql/".$this->page->phptype."/trt_insee_epuration.inc";
                //
                $res1 = $this->page->db->query($sql_IO_1);
                //
                if (database::isError($res1, true)) {
                    //
                    $this->error = true;
                    //
                    $message = $res1->getMessage()." - ".$res1->getUserInfo();
                    $this->LogToFile($message);
                    //
                    $this->addToMessage(_("Contactez votre administrateur."));
                    //
                }
                //
                $i++;
                //
                $this->LogToFile($row['nom']." ".$row['prenom']);
            }
            //
            $message = $i." "._("inscription(s) d'office INSEE supprimee(s)");
            $this->LogToFile($message);
            $this->addToMessage($message);
        }
        //
        $this->LogToFile("end insee_epuration_inscription_office");
    }
}

?>

<?php
/**
 * Ce fichier permet de declarer la classe formulaire.
 *
 * @package openmairie
 * @link http://www.openmairie.org/
 * @version SVN : $Id: formulaire.class.php 53 2010-08-27 09:53:32Z fmichon $
 */

/**
 * Objet Module de recherche - LAUGIER JeanYves (salamandre) - 20100707
 *
 *   Le module de recherche est totalement parametrable depuis le fichier sql/../[obj].search.inc.php ou
 *   par passage d'arguments ($configTypeRecherche) dans : new search($[class utils],$configTypeRecherche)
 *   
 *      -----> ($configTypeRecherche) ------>
 *      // definitions des types de recherche
 *      $conf = array(
 *      "libelle" => lang("Recherche globale"),
 *      "champAffiche" => array(), /// liste des champs a afficher
 *      "champRecherche" => array(), /// champs de recherche 
 *      "table" => "", // tables
 *      "serie" => 0, // limit (facultatif - par defaut 100 )
 *      "selection" => "", // where
 *      "tri" => "", // order by (facultatif)
 *     "href" => "" // lien (facultatif)
 *      );
 *      array_push( $configTypeRecherche, $conf);
 *
 *  Le module possede une fonction searchInclude permettant d'implementer les resultats de la recherche dans
 *  la meme page courante ou autre selon le lien indique dans $include
 *
 *  Vous pouvez cacher le formulaire de recherche en ajoutant $objRech->showForm(true)
 *
 *
 *  Methode d'implementation sur le tableau de bord avec formulaire et l'affichage des resultats
 *  (exemple tableau de bord ( fichier tdb.php ) ):
 *          require_once "../obj/search.class.php";
 *          $objRech = new search($f);   // $f = class utils
 *          $objRech->searchInclude("../scr/tdb.php?obj=[obj]"); // lien de la page du tableau de bord (envoi formulaire)
 *          $objRech->showForm();
 *          $objRech->showResults();
 *          
 *  Methode d'implementation sur une page quelconque, formulaire seulement
 *  (exemple):
 *          require_once "../obj/search.class.php";
 *          $objRech = new search($f);  /// $f = class utils
 *          $objRech->searchInclude("../app/search.php?obj=[obj]"); // lien de la page du tableau de bord (envoi formulaire)
 *          $objRech->showForm();
 * 
 **/

class search {
    
    //// mode DEBUG /////
    private $DEBUG = 0;
    
    private $langue; // langue
    private $typeRecherche; // configuration des types de recherche
    private $f; // class utils
    private $js; // verification si javascript deja implente
    private $formulaire; // verification si formulaire deja affiche
    private $affichage; // verification si resultats deja affiche
    private $include; // page d'appel du formaulaire (action=)
    private $obj;
    
    /** fonction configure
    *
    * Recuperation du fichier de configuration des types de recherche
    * selon le type de connexion (mysql,pgsql) dans sql/../recherche.inc
    *
    **/
    private function configure($obj = "") {
        if ( file_exists("../sql/".$this->f->phptype."/".$obj.".inc") ) {
            include("../sql/".$this->f->phptype."/".$obj.".inc");
        }
        if ( isset($configTypeRecherche) ) {
            $this->typeRecherche = $configTypeRecherche;
            if ( $this->DEBUG ) {
                echo("<br/>-----configTypeRecherche-------<br/>");
                print_r($configTypeRecherche);
                echo("<br/>-------------------------------<br/>");
            }
            return true;
        } else {
            
            
            
            echo("<div class='erreur'>Class search : \$configTypeRecherche non configure par defaut pour obj=".$obj.".</div>");
            $this->typeRecherche = array();
            return false;
        }
    }

    /**
    * Module de recherche
    *
    *   Permet d'effectuer une recherche selon un type predefini dans $configTypeRecherche de
    *    sql/../recherche.inc
    *
    * @param $utils Objet  Class utils.class.php (Connexion base de donnees)
    * @param $configTypeRecherche Array(Array()) - facultatif, Remplace la configuration des types
    *                                  de recherche par defaut dans le fichier sql/../recherche.inc par
    *                                  les nouvelles valeurs dans $configTypeRecherche
    * @param $langue string langue (falcultatif, par defaut "francais")
    **/
    public function __construct($utils = "", $configTypeRecherche = "", $langue = "francais"){
        $this->include = "";
        $this->formulaire = false;
        $this->affichage = false;
        $obj = "";
        if ( isset($_GET['obj']) ) $obj = $_GET['obj'];
        $this->obj = $obj;
        if ( !$langue ) $langue = "francais";
        if ( !($utils->db) ) {
            echo("<div class='erreur'>Class search : Impossible d'appeler la Fonction 'db' depuis \$utils.</div>");
            return false;
        } else {
            $this->f = $utils;
            //$_SESSION['datetableau'] = $this->f->collectivite['datetableau'];
        }
        
        if ( !$configTypeRecherche ) {
            if ( !$this->configure($obj) ) {
                return false;
            } else {
                $this->include = "../app/search.php?obj=".$obj;
            }
         }   
        
        if( $configTypeRecherche ) $this->typeRecherche = $configTypeRecherche;
        $this->langue = $langue;
    }

    /** Fonction searchInclude
    *
    * Fonction permettant d'inclure la recherche dans la page courante ou autre, (lien href)
    * $objRech -> showRecherche() doit etre present sur cette page
    *
    * @param $include string Lien href
    **/
    public function searchInclude($include = "") {
        $this->include = $include;
    }

    /**
     * Fonction showForm
     *
     *  Affiche le formulaire de recherche
     *
     *  @param $hidden bool cacher le formulaire (facultatif)
     *  @param $obj string nom de l'objet (obligatoire si $configTypeRecherche envoyé
     *                      par le constructeur)
     *
     **/
    public function showForm( $hidden = false, $obj = "" ) {
        if ( $this->formulaire ) return false;
        $this->includeJS();
        $this->js = true;
        $recherche = "";
        $searchType = 0;
        $temp = "";
        //$largeSearch = "on";
        
        if ($obj) $vars['obj'] = $obj;
        foreach($_POST as $val1 => $val2) $vars[$val1] = $val2;
        foreach($_GET as $val1 => $val2) $vars[$val1] = $val2;
        
        if( isset($_SESSION['MS_largeSearch']) && !isset($vars['largeSearch'])) $_SESSION['MS_largeSearch'] = "off";
        if( isset($_SESSION['MS_largeSearch']) && isset($vars['largeSearch'])) $_SESSION['MS_largeSearch'] = "on";
        if( !isset($_SESSION['MS_largeSearch']) && isset($vars['largeSearch'])) $_SESSION['MS_largeSearch'] = "on";
        if( !isset($_SESSION['MS_largeSearch']) && !isset($vars['largeSearch'])) $_SESSION['MS_largeSearch'] = "on";

        $vars['largeSearch'] = $_SESSION['MS_largeSearch'];
        $largeSearch = $vars['largeSearch'];
        
        
        if ( isset($vars['premier']) ) {
            $vars['recherche'] = $_SESSION['MS_recherche'];
            $vars['searchType'] = $_SESSION['MS_searchType'];
        }

        if( isset($vars['recherche']) ) $recherche = $vars['recherche'];
        if( isset($vars['searchType']) ) $searchType = $vars['searchType'];

        $cptsel = 0;
        foreach ( $_POST as $POST => $POSTNAME ) {
            if ( substr($POST,0,4) == "temp" ) {
                $temp[$cptsel] = $_POST[$POST];
                $cptsel++;
            }
        }
        // =============================================================================
        //// cacher le formulaire
        $style = "";
        if( $hidden ) $style = " style=\"display:none;\" ";
        //// formulaire de recherche
        $this->formulaire = true;
        echo("<a name='tabRech'></a>");
        if ( !$this->include) {
            echo("<form action=\"../app/search.php\"  method=\"post\"  id=\"rechercheFormModule\" ".$style." >");
        } else {
            echo("<form action=\"".$this->include."\"  method=\"post\"  id=\"rechercheFormModule\" ".$style." >");
        }
        echo("<input type=\"hidden\" name=\"obj\" value=\"".$vars['obj']."\" />");
        echo("<input type=\"hidden\" name=\"validation\" value=\"1\" />");
        echo("<table  border=\"0\" cellpadding=\"1\"  cellspacing=\"1\">");
        echo("<tr valign=\"bottom\">");
        $selected = "";
        if ( $largeSearch == "on") $selected = "checked=\"checked\"";
        echo("<td><input name=\"largeSearch\" type=\"hidden\" ".$selected." value=\"on\" />"."<br/>");
        echo("<input type=\"text\" id=\"module_recherche\" name=\"recherche\" value=\"".$recherche."\" class=\"champFormulaire\" /></td>");

        //// creation select depuis $configTypeRecherche si existant
        $cptsel = 0;
        foreach ( $this->typeRecherche as $tRecherche) {
                if ( !empty($tRecherche['type']) && $tRecherche['type'] == "select" ) {
                    echo("<td>&nbsp;".($tRecherche['libelle'])."<br/><select class=\"champFormulaire\" name=\"temp".$cptsel."\" class=\"champFormulaire\" >");
                    $resultats = array();
                    $res = $this -> f -> db -> query($tRecherche['requete']);
                    if (database::isError($res)) {
                        echo("Erreur SQL : ".$tRecherche['requete']."<br><br><br>");
                        $correct = false;
                    } else {
                        while($row=& $res->fetchRow())
                            array_push($resultats, $row);  
                    }
                    echo("<option value=\"*\" >*</option>");
                    foreach ( $resultats as $data ) {
                        $selected = "";
                        if( $temp[$cptsel] == $data[$tRecherche['value']] ) $selected = "selected=\"selected\"";
                        echo("<option value=\"".$data[$tRecherche['value']]."\" ".$selected."  >".$data[$tRecherche['list']]."</option>");
                    }
                    echo("</select></td>");
                    $cptsel++;
            }
        }

        echo("<td>&nbsp;"._('Type de recherche')."<br/><select name=\"searchType\" class=\"champFormulaire\" >");
        $compteur = 0;
        /// select type de recherche
        foreach ( $this->typeRecherche as $tRecherche) {
            if ( empty($tRecherche['type']) ) {
                $selected = "";
                if ( isset($tRecherche['default']) && $searchType == "" ) $selected = "selected=\"selected\"";
                if ( $compteur == $searchType ) $selected = "selected=\"selected\"";
                echo("<option value=\"".$compteur."\" ".$selected." >".$tRecherche['libelle']."</option>");
                $compteur++;
            }
        }
        echo("</select></td>");
        echo("<td>&nbsp;<br/><input class=\"boutonFormulaire\" type=\"submit\" value=\""._("Rechercher")."\"/></td>");
        echo("</tr></table></form><br/><br/>");
        // =============================================================================
    }

    /**
     * Fonction d'implementation du javascript
     *
     * Javascript : donne le focus à un objet
     * 
     **/
    private function includeJS() {
        echo("
        <script type='text/javascript'>
            function giveFocus(obj){
                document.getElementById(obj).select();
            }
            setTimeout(\"giveFocus('module_recherche');\",500);
        </script>
        ");
    }

    /**
     * Fonction d'affichage des resultats
     *
     * Traitement de recherche et affichage des resultats
     *
     * Possibilite d'afficher une recherche par passage d'arguments sinon laisser vide
     * pour une recherche par le formulaire
     *
     * @param $recherche string Mot a rechercher (facultatif si automatique)
     * @param $searchType integer Type de recherche (facultatif si automatique)
     * @param $largeSearch off/on Active la recherche elargie
     * @param $validation integer Active la recherche si = 1 et $recherche <> NULL (facultatif si automatique)
     **/
    public function showResults() {
        if ( $this-> affichage ) return false;
        
        $vars['validation'] = 0;
        $vars['recherche'] = array();
        $vars['searchType'] = 0;
        $vars['tricol'] = "";
        $vars['selectioncol'] = "";
        $rechercheTab = "";
        
        foreach ($_POST as $val1 => $val2) $vars[$val1] = $val2;
        foreach ($_GET as $val1 => $val2) $vars[$val1] = $val2;

        if ( !isset($vars['premier']) ) {
            $_SESSION['MS_recherche'] = $vars['recherche'];
            $_SESSION['MS_searchType'] = $vars['searchType'];
            $vars['premier'] = 0;
        } else {
            $vars['recherche'] = $_SESSION['MS_recherche'];
            $vars['validation'] = 1;
            $vars['searchType'] = $_SESSION['MS_searchType'];
            if( isset($_POST['recherche']) ) $rechercheTab = $_POST['recherche'];
        }
        
        if ( !isset($vars['largeSearch']) ) {
            $vars['largeSearch'] = "off";
        } else {
            $vars['largeSearch'] = "on";
        }
       
        if( isset($vars['recherche']) ) $recherche = preg_split("/;/",$vars['recherche']);
        if( isset($vars['searchType']) ) $searchType = $vars['searchType'];
        if( isset($vars['validation']) ) $validation = $vars['validation'];
        if( isset($vars['largeSearch']) ) $largeSearch = $vars['largeSearch'];
        if( !$recherche[0] || !$validation ) return false;

        $temp = "";
        
        $cptsel = 0;
        foreach ( $_POST as $POST => $POSTNAME ) {
            if ( substr($POST,0,4) == "temp" ) {
                $temp[$cptsel] = $_POST[$POST];
                $cptsel++;
            }
        }

        ///// MODE DEBUG
        if ( $this->DEBUG ) {
            echo("<br/>-----POST/GET Variables-------<br/>");
            print_r($vars);
            echo("<br/>--------------------------<br/>");
        }
        
        //// Si formulaire non existant, creation d'un formulaire cacher
        if ( !$this->formulaire ) {
            $this->showForm(true);
            $this->formulaire = true;
        }

        echo "<div id=\"formulaire\">\n\n";
       
        $num = 0;
        foreach ( $this->typeRecherche as $tRecherche ) {
            
            ////// surcharge 
            if ( isset($tRecherche['obj']) && $tRecherche['obj'] ) {
            
                if ( $searchType == $num || ( isset($this->typeRecherche[$searchType]['champAffiche']) && $this->typeRecherche[$searchType]['champAffiche'] == "#")) {
                
                    $_recherche = $recherche;
                    $recherche = str_replace("*","",$_recherche[0]);
                    $recherche1 = $recherche;
                    $premier = $vars["premier"];
                    $selectioncol = $vars['selectioncol'];
                    $tricol = $vars['tricol'];
                    $f = $this->f;
                    $obj =  $tRecherche['obj'];
                   
                    /**
                     *
                     */
                    if ($f->isAccredited($obj)) {
                        if (!isset($href) or $href == array()) {
                            $href[0] = array("lien" => "#", "id" => "", "lib" => "", );
                            $href[1] = array(
                                "lien" => "../scr/form.php?obj=".$obj."&amp;tricol=".$tricol."&amp;idx=",
                                "id" => "&amp;premier=".$premier."&amp;recherche=".$recherche."&amp;selectioncol=".$selectioncol,
                                "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix edit-16\" title=\""._("Modifier")."\">"._("Modifier")."</span>",
                            );
                            $href[2] = array(
                                "lien" => "../scr/form.php?obj=".$obj."&amp;tricol=".$tricol."&amp;idx=",
                                "id" => "&amp;ids=1&amp;premier=".$premier."&amp;recherche=".$recherche."&amp;selectioncol=".$selectioncol,
                                "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix delete-16\" title=\""._("Supprimer")."\">"._("Supprimer")."</span>",
                            );
                        }
                    } else {
                        $href[0] = array("lien" => "#", "id" => "", "lib" => "", );
                        $href[1] = array("lien" => "", "id" => "", "lib" => "", );
                        $href[2] = array("lien" => "#", "id" => "", "lib" => "", );
                    }
                    
                    //
                    require_once "../sql/".$f->phptype."/".$obj.".inc";
                    //
                    require_once PATH_OPENMAIRIE."om_table.class.php";
                    
                    echo "<div class=\"searchSeparator\">";
                    echo "\t<a href=\"#tabs-".$num."\">".ucwords(_("Recherche : ")._($tRecherche['libelle']))."</a>\n";
                    echo "</div>";
                    
                    //
                    echo "\n<div id=\"tabs-".$num."\">\n";
                    //
                    if ($edition != "") {
                        $edition = "../pdf/pdf.php?obj=".$edition;
                    }
                    //
                    if (!isset($options)) {
                        $options = array();
                    }
                    
                    //
                    $tb = new table("../scr/tab.php", $table, $serie, $champAffiche, $champRecherche, $tri, $selection, $edition, $options);
                    //
                    $params = array(
                        "obj" => $obj,
                        "premier" => $premier,
                        "recherche" => $recherche,
                        "selectioncol" => $selectioncol,
                        "tricol" => $tricol
                    );
                    //
                    $tb->display($params, $href, $f->db, "tab", false);
                    //
                    echo "</div>\n";
                    //
                    
                    $recherche = $_recherche;
                }
            
            } else {
            
                if ( empty($tRecherche['type']) ) {
                    /////// Recuperation des champs et des variables pour la requete sql
                    
                    $correct = true;
                    $table = $tRecherche['table'];
                    $champs = $tRecherche['champAffiche'];
                    $where = $tRecherche['selection'];
                    
                    $lien = strtolower($tRecherche['href']);
                    $nomType = $tRecherche['libelle'];
                    $tri = $tRecherche['tri'];
                    $serie = $tRecherche['serie'];
                    $champsRecherche = $tRecherche['champRecherche'];
                    
                    ///// methode recherche entre 2 dates
                    $entreDate = false;
                    if ( isset($tRecherche['modeDate']) ) {
                        $entreDate = $tRecherche['modeDate'];
                    }
                    if ( count($recherche)<=1 ) $entreDate = false;
                    
                    ///// MODE DEBUG
                    if( $this->DEBUG ) {
                        echo("<br/>-----ConfigType Variables-------<br/>");
                        echo("\$correct = ".$correct."<br/>");
                        echo("\$champs = ".$champs."<br/>");
                        echo("\$table = ".$table."<br/>");
                        echo("\$where = ".$where."<br/>");
                        echo("\$lien = ".$lien."<br/>");
                        echo("\$nomType = ".$nomType."<br/>");
                        echo("\$tri = ".$tri."<br/>");
                        echo("\$serie = ".$serie."<br/>");
                        echo("\$champsRecherche = ".$champsRecherche."<br/>");
                        echo("\$entreDate = ".$entreDate."<br/>");
                        echo("count(\$recherche) = ".count($recherche)."<br/>");
                        echo("--------------------------<br/>");
                    }
                    
                    ///// initialisation requete sql
                    $requete = "";
                    if ( $searchType == $num || ( isset($this->typeRecherche[$searchType]['champAffiche']) && $this->typeRecherche[$searchType]['champAffiche'] == "#") ) {
                         if ( $champs != "#" ) {
                               if ( $where ) $requete .= " ".$where;
                               $cpt = 0;
                               foreach ( $champsRecherche as $rech ) {
                                   if (! $cpt ) {
                                       if ( $where ) {
                                           $requete .= " AND (";
                                       } else {
                                           $requete .= " WHERE (";
                                       }
                                   } else {
                                       $requete .= " OR ";
                                   }
                                   $cpt2 = 0;
                                   foreach ( $recherche as $arRech ) {
                                        if( $cpt2 ) {
                                            if ($entreDate) {
                                                $requete .= " AND ";
                                            } else {
                                                $requete .= " OR ";
                                            }
                                        } else {
                                            $requete .= " ( ";
                                        }
                                        ////// Si modeDate = true (Recherche entre 2 dates)
                                        if (!$entreDate) {
                                            //// mysql
                                            if( $this->f->phptype == "mysql" )
                                                $requete .= " CAST(".$rech." AS CHAR(50)) £largeSearch '£recherche[".$cpt2."]' "; // CAST int/date
                                            //// pgsql
                                            if( $this->f->phptype == "pgsql" /* /*$this->f->phptype == "oracle"*/ )
                                                $requete .= " TRANSLATE(CAST(".$rech." AS CHAR(50)),'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY') £largeSearch '£recherche[".$cpt2."]' "; // Translate accent / Cast
                                        } else {
                                            if ( $cpt2 <= 1) {
                                                $requete .= " ".$rech." £largeSearch".$cpt2." '£recherche[".$cpt2."]' ";
                                            } else {
                                                $requete .= " CAST(".$rech." AS CHAR(50)) £largeSearch '£recherche[".$cpt2."]' ";                                            
                                            }
                                        }
                                        $cpt2++;
                                    }
                                    $requete .= " ) ";
                                    $cpt++;
                                }
                                if ($cpt) $requete.= " ) ";
                                ////// Si modeDate = true (Recherche entre 2 dates)
                                if ($entreDate) {
                                    $requete = str_replace('£largeSearch0',">=",$requete);
                                    $requete = str_replace('£largeSearch1',"<=",$requete);
                                }
                                ////// Remplacement de £recherche par $search et £largeSearch 
                                $cpt2 = 0;
                                foreach ( $recherche as $search) {
                                    $search = strtoupper($search);
                                    
                                    if (strlen($search) == 10 ) {
                                        $dateFormat = preg_split("/\//",$search);
                                        if ( (count($dateFormat) == 3 ) ) {
                                            $search = $dateFormat[2]."-".$dateFormat[1]."-".$dateFormat[0];
                                        }
                                    }
                                    
                                    if ($search == "*" && $entreDate) $search="0000-00-00";
                                    if ($search == "*" && !$entreDate) {
                                         $requete = str_replace('£recherche['.$cpt2.']',"%",$requete);
                                         $requete = str_replace('£largeSearch',"like",$requete);
                                    } else {             
                                        if ($largeSearch=="on" && !$entreDate) {
                                            $requete = str_replace('£recherche['.$cpt2.']',"%".$search."%",$requete);
                                            $requete = str_replace('£largeSearch',"like",$requete);
                                        } else {
                                            ///// mysql
                                            if ( $this->f->phptype == "mysql" )
                                                $requete = str_replace('£recherche['.$cpt2.']',$search,$requete);
                                            //// pgsql
                                            if ( $this->f->phptype == "pgsql" && $entreDate ) {
                                                //// si date
                                                $dateFormat = preg_split("/-/",$search);
                                                if ( (count($dateFormat) == 3 ) && strlen($search) == 10 ) {
                                                    //--// "to_date('".$search."','YYYY-MM-DD')" > $search
                                                    $requete = str_replace("'£recherche[".$cpt2."]'",$search,$requete);
                                                } else {
                                                    $requete = str_replace("'£recherche[".$cpt2."]'","NULL",$requete);                                                
                                                }
                                            }
                                            if ( $this->f->phptype == "pgsql" && !$entreDate )
                                                $requete = str_replace('£recherche['.$cpt2.']',$search,$requete);
                                            //// commun
                                            $requete = str_replace('£largeSearch',"=",$requete);
                                        }
                                    }
                                    $cpt2++;
                                }
                              
                                /////////// Ajout de la condition du select depuis $configTypeRecherche si existant
                                $cptsel = 0;
                                foreach ( $this->typeRecherche as $tSelect) {
                                    if ( !empty($tSelect['type']) && $tSelect['type'] == "select" ) {
                                        if ( $temp[$cptsel]!="*" ) $requete .= " AND (CAST(".$tSelect['selection']." AS CHAR(50))='".$temp[$cptsel]."') ";
                                        $cptsel++;
                                    }
                                }
                              
                                ////// Construction de la requete - LIMIT
                                if ( !$serie ) $serie = 50;
                              
                                //// DEBUG /////
                                if ($this->DEBUG) {
                                    echo("<br/>-----Requete SQL (WHERE)-------<br/>");
                                    echo($requete."<br/>");
                                }
    
                                /**
                                 * Appel de TAB.PHP pour l'affichage des resultats
                                */
                                $href[0] = array("lien" => "#", "id" => "", "lib" => "", );
                                $href[1] = array(
                                    "lien" => $lien,
                                    "id" => "",
                                    "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix edit-16\" title=\""._("Modifier")."\">"._("Modifier")."</span>",
                                );
                                $href[2] = array("lien" => "#", "id" => "", "lib" => "", );
                                $edition = "";
                                $selection = $requete;
                                
                                //
                                require_once PATH_OPENMAIRIE."om_table.class.php";
                                //
                                
                                
                                echo "<div class=\"searchSeparator\">";
                                echo "\t<a href=\"#tabs-".$num."\">".ucwords(_("Recherche : ")._($tRecherche['libelle']))."</a>\n";
                                echo "</div>";
                                                                            
                                echo "\n<div id=\"tabs-".$num."\">\n";
                                $tb = new table("../app/search.php", $table, $serie, $champs, $champsRecherche, $tri, $selection, $edition);
                                
                                //
                                $params = array(
                                    "obj" => $vars['obj'],
                                    "premier" => $vars['premier'],
                                    "recherche" => $rechercheTab,
                                    "selectioncol" => $vars['selectioncol'],
                                    "tricol" => $vars['tricol'],
                                );
                                //
                                $tb->display($params, $href, $this->f->db, "tab", false);
                                //
                                echo "</div>\n";
                                //
                        }
                    }
                }
            }
            $num++;
        }
        
        echo "<div class=\"searchSeparator\"></div>";
        echo "\n</div>\n";
    }
}// fin de classe
?>
<?php
/**
 * Ce fichier permet de definir la classe radiation_insee_valider
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "radiation.class.php";

/**
 *
 */
class radiation_insee_valider extends radiation {

    /**
     * Constructeur
     */
    function radiation_insee_valider($id, &$db, $DEBUG = false) {
        //
        $this->constructeur($id, $db);
    }

    /**
     * Bouton retour
     */
    function retour($premier = 0, $recherche = "", $tricol = "") {
        echo "\n<a class=\"retour\" ";
        echo "href=\"";
        //
        echo "../scr/tab.php";
        echo "?";
        echo "obj=radiation_insee";
        echo "&amp;premier=".$this->getParameter("premier");
        echo "&amp;tricol=".$this->getParameter("tricol");
        echo "&amp;recherche=".$this->getParameter("recherche");
        echo "&amp;selectioncol=".$this->getParameter("selectioncol");
        //
        echo "\"";
        echo ">";
        //
        echo _("Retour");
        //
        echo "</a>\n";
    }

    /**
     * Passage de parametres pour action (form)
     */
    function getDataSubmit() {
        //
        $datasubmit = parent::getDataSubmit();
        //
        $datasubmit .= "&amp;idxelecteur=".$this->getParameter("idxelecteur");
        $datasubmit .= "&amp;idrad=".$this->getParameter("idrad");
        //
        return $datasubmit;
    }

    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$db, $DEBUG = false) {
        // Recuperation des valeurs pour le formulaire
        parent::setVal($form, $maj, $validation, $db, $DEBUG);
        // Recuperation du type de mouvement de radiation depuis radiation_insee
        $sql= "select motif_de_radiation from radiation_insee where id = ".$_GET['idrad']." and collectivite = '".$_SESSION['collectivite']."' ";
        $motif_de_radiation = $this->db->getone($sql);
        $this->addToLog("setVal(): db->getone(\"".$sql."\")", VERBOSE_MODE);
        if (database::isError($motif_de_radiation))
            $this->erreur_db($motif_de_radiation->getDebugInfo(),$motif_de_radiation->getMessage(),"");
        $this->addToLog("setVal(): \$motif_de_radiation=".$motif_de_radiation, EXTRA_VERBOSE_MODE);
        // Recuperation du type de mouvement
        $sql = "select code, insee_import_radiation from param_mouvement where typecat='Radiation'";
        $res = $this->db->query($sql);
        $this->addToLog("setVal(): db->query(\"".$sql."\")", VERBOSE_MODE);
        if (database::isError($res)) {
            die();
        } else {
            $param_mouvement = array();
            while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                if ($row["insee_import_radiation"] != "") {
                    $param_mouvement[$row["code"]] = explode(";",$row["insee_import_radiation"]);
                }
            }
            $this->addToLog("setVal(): \$param_mouvement=".print_r($param_mouvement, true), EXTRA_VERBOSE_MODE);
            //
            foreach ($param_mouvement as $key => $insee_import_radiation_old) {
                if (in_array($motif_de_radiation, $insee_import_radiation_old)) {
                    $form->setVal("types", $key);
                }
            }
            
        }
        
    }
    
    /**
     * Apres l'ajout du mouvement de radiation, celui-ci indique a "radiation_insee"
     * que l'enregistrement $idrad a ete traite  
     */
    function triggerajouter($id, &$db, $val, $DEBUG = false) {
        // mise à jour
        $sql = "update radiation_insee set types='1' where id = '".$this->getParameter("idrad")."'";
        $res = $db->query($sql);
        if (database::isError($res))
            $this->erreur_db($res->getDebugInfo(),$res->getMessage(),$tableSelect);
        $this->msg .= _("Radiation insee num")." ".$this->getParameter("idrad")._(" modifiee [").$db->affectedRows()._(" enregistrement(s) mis a jour]")."<br />";
    }

    /**
     * Verifi si type de mouvement non vide
     */
    function verifier($val = array(), &$db = NULL, $DEBUG = false) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Verification du type de mouvement
        if ($this->valF['types'] == "") {
            $this->correct = false;
            $this->addToMessage(_("Le choix du type de mouvement est obligatoire")."<br/>");
        }
        // ???
        $this->valF['date_naissance'] = $this->dateDB($this->valF['date_naissance']);
        $this->valF['date_tableau'] = $this->dateDB($this->valF['date_tableau']);
    }

}

?>

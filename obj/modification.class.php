<?php
/**
 * Ce fichier permet de definir la classe modification
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "mouvement_electeur.class.php";

/**
 * 
 */
class modification extends mouvement_electeur {

    /**
     * Type de mouvement correspondant à la valeur de typecat dans la table
     * param_mouvement
     * @var string Type de mouvement
     */
    var $typeCat = "modification";

    /**
     *
     */
    function modification($id, &$db, $DEBUG = false) {
        //
        $this->constructeur($id, $db);
    }

    /**
     *
     */
    function setvalFAjout($val = array()) {
        //
        $this->valF['id_electeur'] = $val['id_electeur'];
        $this->valF['numero_bureau'] = $val['numero_bureau'];
        $this->valF['numero_electeur'] = $val['numero_electeur'];
    }

    /**
     *
     */
    function verifierAjout($val = array(), &$db = NULL) {
        // Verification du verrou electeur
        if ($this->isVerrouElecteur($val) == true) {
            $this->correct = false;
            $this->addToMessage(_("L'electeur a deja un mouvement en cours.")."<br/>");
        }
    }

    /**
     *
     */
    function triggerajouterapres($id, &$db, $val, $DEBUG) {
        // On active le verrou sur l'electeur
        $this->manageVerrouElecteur($id, $db, $val, true);
        //
        $this->setComputedVal();
    }

    /**
     *
     */
    function triggersupprimerapres($id, &$db, $val, $DEBUG) {
        // On desactive le verrou sur l'electeur
        $this->manageVerrouElecteur($id, $db, $val, false);
    }

    /**
     *
     */
    function triggermodifierapres($id, &$db, $val, $DEBUG) {
        //
        $this->setComputedVal();
    }

    /**
     *
     */
    function setValFNumeroBureau() {
        //
        if ($this->valF['code_bureau'] != $this->electeur['code_bureau']) {
            //
            $this->valF['numero_bureau'] = 0;
            $this->addToMessage(_("Le mouvement de l'electeur entraine un changement de bureau de vote.")."<br/>");
        } else {
            //
            $this->valF['numero_bureau'] = $this->electeur['numero_bureau'];
        }
    }

}

?>

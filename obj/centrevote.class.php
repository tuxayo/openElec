<?php
/**
 * Ce fichier permet de definir la classe centrevote
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class centrevote extends dbForm {
    
    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "centrevote";
    
    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "idcentrevote";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "N";
    
    /**
     * Constructeur 
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function centrevote($id, &$db, $DEBUG) {
        //
        $this->constructeur($id, $db, $DEBUG);
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en vue de l'insertion des donnees dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setvalF($val) {
        //
        $this->valF["idcentrevote"] = $val["idcentrevote"];
        $this->valF["date_modif"] = $this->dateSystemeDB();
        $this->valF["utilisateur"] = $_SESSION["login"];
        $this->valF["id_electeur"] = $val["id_electeur"];
        $this->valF["debut_validite"] = $val["debut_validite"];
        $this->valF["fin_validite"] = $val["fin_validite"];
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en ajouter la cle primaire en vue de l'insertion des donnees
     * dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setValFAjout($val) {
        // Id automatique donc cette initialisation n'est pas necessaire
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * de recuperer l'element suivant dans la sequence pour les id automatiques.
     * 
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function setId(&$db) {
        //
        $this->valF[$this->clePrimaire] = $db->nextId($this->table);
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * d'effectuer des tests d'integrite sur la cle primaire pour verifier
     * si la cle a bien ete saisie dans le formulaire et si l'objet ajoute
     * n'existe pas deja dans la table pour en empecher l'ajout.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function verifierAjout($val, &$db) {
        // Id automatique donc cette verification n'est pas necessaire
    }
    
    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function verifier($val, &$db, $DEBUG) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // obligatoire
        if ($this->valF['id_electeur'] == "") {
            $this->correct=false;
            $this->msg .= _("Vous devez selectionner un electeur")."<br />";
        } else {
            if (!is_numeric ($this->valF['id_electeur'])) {
                $this->correct=false;
                $this->msg .= _("L'identifiant de l'electeur doit etre valide")."<br />";
            } else {
                // Test si l'electeur existe
                $sql = "select * from electeur where id_electeur='".$this->valF['id_electeur']."' AND collectivite = '".$_SESSION['collectivite']."' ";
                $res = $db -> query ($sql);
                if($DEBUG==1) echo $sql;
                if (database::isError($res))
                    $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                else {
                    $nbligne = $res->numrows(); //XXX
                    if ($nbligne==0) {
                        $this->correct=false;
                        $this->msg .= _("L'electeur ").$this->valF['id_electeur']._(" n'existe pas")."<br />";
                    }
                }
                if ($this -> correct) {
                    $sql= "select (nom||' - '||prenom||' - '||to_char(date_naissance, 'DD/MM/YYYY')||' - '||code_bureau) as z_electeur from electeur where id_electeur='".$this->valF['id_electeur']."' and collectivite='".$_SESSION['collectivite']."' ";
                    $res = $db -> query ($sql);
                    if($DEBUG==1) echo $sql;
                    if (database::isError($res))
                        $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                    else {
                        $row = $res -> fetchrow (DB_FETCHMODE_ASSOC);
                        if ($row ['z_electeur'] != $val['z_electeur']) {
                            $this->correct=false;
                            $this->msg .= _("La correspondance entre les deux champs Identifiant / Nom de l'electeur n'est pas correcte")."<br />";
                        }
                    }
                }
            }
        }
        //
        if ($this->valF['debut_validite']=="") {
            $this->correct=false;
            $this->msg .= _("La date de debut de validite est obligatoire")."<br />";
        } else {
            $this->valF['debut_validite'] = $this->dateDB($this->valF['debut_validite']);
        }
        if ($this->valF['fin_validite']=="") {
            $this->valF['fin_validite'] = $this->valF['debut_validite'];
        } else {
            $this->valF['fin_validite'] = $this->dateDB($this->valF['fin_validite']);
        }
        // validite superieur a 1 jour
        if ($this->valF['debut_validite'] > $this->valF['fin_validite']) {
            $this->correct=false;
            $this->msg .= _("La date de debut de validite doit etre superieure a la date de fin")."<br />";
        }
        // module de verification des centre vote ===============================
        // 1 electeur  a droit qu a 1 centre de vote pour une meme periode
        // =======================================================================
        if ($this->correct==true) {
            // verification du mandant sur la periode => 1 seul mandant
            $sql = "select * from centrevote where id_electeur =".$this->valF['id_electeur']." ";
            $sql .= "and (fin_validite >= '".$this->valF['debut_validite']."' ";
            $sql .= "and debut_validite <= '".$this->valF['fin_validite']."') ";
            if ($this -> valF ['idcentrevote'] != "")
                $sql .= "and idcentrevote<>'".$val['idcentrevote']."'";
            $res = $db->query($sql);
            if($DEBUG==1) echo $sql;
            if (database::isError($res))
                $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
            else {
                $nbligne=$res->numrows();
                if ($nbligne > 0) {
                    $this->msg .= _("L'electeur numero [").$this->valF['id_electeur']."] ";
                    $this->msg .= _("est deja inscrit dans ").$nbligne." ";
                    $this->msg .= _("centre de vote pour la meme periode")."<br />";
                    $this->correct=false;
                }
            }
        }
    }
    
    /**
     * Parametrage du formulaire - Groupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setGroupe(&$form, $maj) {
        //
        $form->setGroupe("debut_validite", "D");
        $form->setGroupe("fin_validite", "F");
        $form->setGroupe("date_modif", "D");
        $form->setGroupe("utilisateur", "F");
        $form->setGroupe("id_electeur", "D");
        $form->setGroupe("z_electeur", "F");
    }
    
    /**
     * Parametrage du formulaire - Regroupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setRegroupe(&$form, $maj) {
        //
        if ($maj != 0) {
            $form->setRegroupe("idcentrevote", "D", _("Identification"));
            $form->setRegroupe("date_modif", "G", "");
            $form->setRegroupe("utilisateur", "F", "");
        }
        $form->setRegroupe("id_electeur", "D", _("Electeur"));
        $form->setRegroupe("z_electeur", "F", "");
        $form->setRegroupe("debut_validite", "D", _("Validite de la mention centre de vote"));
        $form->setRegroupe("fin_validite", "F", "");
    }
    
    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType(&$form, $maj) {
        //
        if ($maj < 2) { // ajouter et modifier
            $form->setType('debut_validite','date');
            $form->setType('fin_validite','date');
            $form->setType('z_electeur','comboG');
            $form->setType('id_electeur','comboD');
            if ($maj == 1) { // modifier
                $form->setType('idcentrevote','hiddenstatic');
                $form->setType('date_modif','hiddenstatic');
                $form->setType('utilisateur','hiddenstatic');
            } else { // ajouter
                $form->setType('idcentrevote','hidden');
                $form->setType('date_modif','hidden');
                $form->setType('utilisateur','hidden');
            }
        } else { // supprimer
            $form->setType('idcentrevote','hiddenstatic');
        }
    }
    
    /**
     * Parametrage du formulaire - Select des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function setSelect(&$form, $maj, $db, $DEBUG) {
        //
        if ($maj < 2) {
            // CORREL
            // id_electeur
            $contenu="";
            $contenu[0][0] = "electeur";
            $contenu[0][1] = "id_electeur";
            $contenu[1][0] = "nom";
            $contenu[1][1] = "z_electeur";
            $form->setSelect("id_electeur", $contenu);
            // z_electeur
            $contenu = "";
            $contenu[0][0] = "electeur";
            $contenu[0][1] = "nom";
            $contenu[1][0] = "id_electeur";
            $contenu[1][1] = "id_electeur";
            $form->setSelect("z_electeur", $contenu);
        }
    }
    
    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        $form->setLib("debut_validite", _("du "));
        $form->setLib("fin_validite", _(" au "));
        $form->setLib("z_electeur", "");
        $form->setLib("id_electeur", _("Identifiant / Nom de l'electeur"));
        $form->setLib("date_modif", _("Modifie le"));
        $form->setLib("utilisateur", _(" par "));
    }
    
    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setOnchange(&$form, $maj) {
        //
        $form->setOnchange("z_electeur", "this.value=this.value.toUpperCase()");
        $form->setOnchange("debut_validite", "fdate(this)");
        $form->setOnchange("fin_validite", "fdate(this)");
    }
    
    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille(&$form, $maj) {
        //
        $form->setTaille("id_electeur", 8);
        $form->setTaille("z_electeur", 50);
        $form->setTaille("fin_validite", 10);
        $form->setTaille("debut_validite", 10);
    }
    
    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax(&$form, $maj) {
        //
        $form->setMax("id_electeur", 8);
        $form->setMax("z_electeur", 90);
    }
    
}

?>
<?php

require_once ("../obj/traitement.class.php");

class refonteTraitement extends traitement {
    
    var $fichier = "refonte";
    
    function getValidButtonValue() {
        //
        return _("Renumerotation de la liste electorale");
    }
    
    function displayBeforeContentForm() {
        //
        include "../sql/".$this->page->phptype."/trt_refonte.inc";
        $nbElecteur = $this->page->db->getone ($query_count_electeur);
        $this->page->isDatabaseError($nbElecteur);
        //
        echo "\n<div class=\"field\">\n\t<label>";
        echo _("Le nombre d'electeur(s) a la date du")." ";
        echo date('d/m/Y')." "._("est de")." ".$nbElecteur.".";
        echo "</label>\n</div>\n";
    }
    
    function treatment () {
        //
        $this->LogToFile ("start refonte");
        
        //
        include ("../sql/".$this->page->phptype."/trt_refonte.inc");
        
        // TRAITEMENT 1 - RENUMEROTATION ELECTEUR LISTE
        $this->LogToFile ("RENUMEROTATION ELECTEUR LISTE");
        $res_select_electeur_id = $this->page->db->query ($query_select_electeur_id);
        if (database::isError($res_select_electeur_id)) {
            //
            $this->error = true;
            //
            $message = $res_select_electeur_id->getMessage ()." erreur sur ".$query_select_electeur_id."";
            $this->LogToFile ($message);
        } else {
            //
            $nb_electeur = $res_select_electeur_id->numRows ();
            $this->LogToFile ($nb_electeur." "._("electeur(s) sur la liste a mettre a jour"));
            //
            $i = 0;
            while ($row =& $res_select_electeur_id->fetchRow (DB_FETCHMODE_ASSOC)) {
                //
                $i++;
                //
                $fields_values = array ('numero_electeur' => $i);
                $res_update_electeur = $this->page->db->autoExecute ("electeur", $fields_values,
                                                      DB_AUTOQUERY_UPDATE, "id_electeur=".$row['id_electeur']);
                if (database::isError($res_update_electeur)) {
                    //
                    $this->error = true;
                    //
                    $message = $res_update_electeur->getMessage ()." - ".$res_update_electeur->getUserInfo ();
                    $this->LogToFile ($message);
                    //
                    break;
                }
            }
            //
            $res_select_electeur_id->free ();
            //
            if ($this->error == false) {
                //
                $this->LogToFile ($i." "._("electeur(s) sur la liste mis a jour"));
                //
                $fields_values = array ('dernier_numero' => $i);
                $key = " liste='".$_SESSION['liste']."' and collectivite='".$_SESSION['collectivite']."' ";
                //
                $sql = "select dernier_numero from numeroliste where ".$key;
                $res = $this->page->db->getone($sql);
                if (database::isError($res, true) || $res == NULL) {
                    //
                    $fields_values["liste"] = $_SESSION['liste'];
                    $fields_values["collectivite"] = $_SESSION['collectivite'];
                    //
                    $fields_values["id"] = $_SESSION['collectivite'].$_SESSION['liste'];
                    //
                    $res_update_numeroliste = $this->page->db->autoExecute ("numeroliste", $fields_values, DB_AUTOQUERY_INSERT);
                } else {
                    //
                    $res_update_numeroliste = $this->page->db->autoExecute ("numeroliste", $fields_values, DB_AUTOQUERY_UPDATE, $key);
                }
                
                if (database::isError($res_update_numeroliste)) {
                    //
                    $this->error = true;
                    //
                    $message = $res_update_numeroliste->getMessage ()." - ".$res_update_numeroliste->getUserInfo ();
                    $this->LogToFile ($message);
                } else {
                    //
                    $message = "mise a jour du dernier numero de la liste ".$_SESSION['liste']." ".$_SESSION['libelle_liste']." a ".$i;
                    $this->LogToFile ($message);
                }
            }
        }

        // TRAITEMENT 2 - RENUMEROTATION ELECTEUR BUREAU
        $this->LogToFile ("RENUMEROTATION ELECTEUR BUREAU");
        $res_select_bureau = $this->page->db->query ($query_select_bureau);
        if (database::isError($res_select_bureau)) {
            //
            $this->error = true;
            //
            $message = $res_select_bureau->getMessage ()." - ".$res_select_bureau->getUserInfo ();
            $this->LogToFile ($message);
        } else {
            //
            while ($row_bureau =& $res_select_bureau->fetchRow (DB_FETCHMODE_ASSOC)) {
                //
                include ("../sql/".$this->page->phptype."/trt_refonte.inc");
                //
                $res_select_electeur_id_bureau = $this->page->db->query ($query_select_electeur_id_bureau);
                if (database::isError($res_select_electeur_id_bureau)) {
                    //
                    $this->error = true;
                    //
                    $message = $res_select_electeur_id_bureau->getMessage ()." - ".$res_select_electeur_id_bureau->getUserInfo ();
                    $this->LogToFile ($message);
                } else {
                    //
                    $nb_electeur_bureau = $res_select_electeur_id_bureau->numRows ();
                    $this->LogToFile ("bureau ".$row_bureau['code']." ".$row_bureau['libelle_bureau']." : ".$nb_electeur_bureau." "._("electeur(s) dans le bureau a mettre a jour"));
                    //
                    $i = 0;
                    while ($row_electeur =& $res_select_electeur_id_bureau->fetchRow (DB_FETCHMODE_ASSOC)) {
                        //
                        $i++;
                        //
                        $fields_values = array ('numero_bureau' => $i);
                        $res_update_electeur_bureau = $this->page->db->autoExecute ("electeur", $fields_values,
                                                              DB_AUTOQUERY_UPDATE, "id_electeur=".$row_electeur['id_electeur']);
                        if (database::isError($res_update_electeur_bureau)) {
                            //
                            $this->error = true;
                            //
                            $message = $res_update_electeur_bureau->getMessage ()." - ".$res_update_electeur_bureau->getUserInfo ();
                            $this->LogToFile ($message);
                            //
                            break;
                        }
                    }
                    //
                    $res_select_electeur_id_bureau->free ();
                    //
                    if ($this->error == false) {
                        //
                        $this->LogToFile ($i." "._("electeur(s) dans le bureau ".$row_bureau['code']." sur la liste ".$_SESSION['liste']." mis a jour"));
                        //
                        $fields_values = array ('dernier_numero' => $i, 'dernier_numero_provisoire' => 9000);
                        $key = " bureau='".$row_bureau['code']."' and liste='".$_SESSION['liste']."' and collectivite='".$_SESSION['collectivite']."' ";
                        //
                        $sql = "select dernier_numero from numerobureau where ".$key;
                        $res = $this->page->db->getone($sql);
                        if (database::isError($res, true) || $res == NULL) {
                            //
                            $fields_values["bureau"] = $row_bureau['code'];
                            $fields_values["liste"] = $_SESSION['liste'];
                            $fields_values["collectivite"] = $_SESSION['collectivite'];
                            //
                            $fields_values["id"] = $_SESSION['collectivite'].$_SESSION['liste'].$row_bureau['code'];
                            //
                            $res_update_numerobureau = $this->page->db->autoExecute ("numerobureau", $fields_values, DB_AUTOQUERY_INSERT);
                        } else {
                            //
                            $res_update_numerobureau = $this->page->db->autoExecute ("numerobureau", $fields_values, DB_AUTOQUERY_UPDATE, $key);
                        }

                        if (database::isError($res_update_numerobureau)) {
                            //
                            $this->error = true;
                            //
                            $message = $res_update_numerobureau->getMessage ()." - ".$res_update_numerobureau->getUserInfo ();
                            $this->LogToFile ($message);
                        } else {
                            //
                            $message = "mise a jour du dernier numero du bureau ".$row_bureau['code']." de la liste ".$_SESSION['liste']." ".$_SESSION['libelle_liste']." a ".$i;
                            $this->LogToFile ($message);
                        }
                    }
                }
            }
            //
            $res_select_bureau->free ();
        }
        
        //
        $this->LogToFile ("end annuel");
    }
}

?>
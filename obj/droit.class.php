<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class droit extends dbForm {
    
    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "droit";
    
    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "droit";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "A";
    
    /**
     * Constructeur 
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function droit($id, &$db, $DEBUG) {
        //
        $this->constructeur($id, $db, $DEBUG);
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en vue de l'insertion des donnees dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setvalF($val) {
        //
        $this->valF["droit"] = $val["droit"];
        $this->valF["profil"] = $val["profil"];
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * d'effectuer des tests d'integrite sur la cle primaire pour verifier
     * si la cle a bien ete saisie dans le formulaire et si l'objet ajoute
     * n'existe pas deja dans la table pour en empecher l'ajout.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function verifierAjout($val, &$db) {
        // Si la cle primaire est vide alors on rejette l'enregistrement
        if (trim($this->valF[$this->clePrimaire]) == "") {
            $this->correct = false;
            $this->msg .= $this->form->lib[$this->clePrimaire];
            $this->msg .= _(" est obligatoire")."<br />";
        }
        // Si le test precedent a ete reussi alors on teste si l'identifiant
        // n'existe pas deja dans la table
        if ($this->correct == true) {
            // Construction de la requete
            $sql = "select count(*) from ".$this->table." ";
            if ($this->typeCle == "A") {
                $sql .= "where ".$this->clePrimaire."='".$val[$this->clePrimaire]."'";
            } else {
                $sql .= "where ".$this->clePrimaire."=".$val[$this->clePrimaire]."";
            }
            // Execution de la requete
            $nb = $db->getone($sql);
            if (database::isError($nb)) {
                // Appel de la methode qui gere les erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            } else {
                // Si le resultat de la requete renvoit un ou plusieurs
                // enregistrements
                if ($nb > 0) {
                    // On rejette l'enregistrement
                    $this->correct = false;
                    $this->msg .= _("Il existe deja un enregistrement avec cet identifiant, vous devez en choisir un autre")."<br />";
                }
            }
        }
    }
    
    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @return void
     */
    function verifier() {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Si la valeur est vide alors on rejette l'enregistrement
        $field_required = array("profil");
        foreach($field_required as $field) {
            if (trim($this->valF[$field]) == "") {
                $this->correct = false;
                $this->msg .= $this->form->lib[$field]." ";
                $this->msg .= _("est obligatoire")."<br />";
            }
        }
    }
    
    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType(&$form, $maj) {
        //
        if ($maj < 2) { // ajouter et modifier
            $form->setType("profil", "select");
            if ($maj == 1) { // modifier
                $form->setType("droit", "hiddenstatic");
            } else { // ajouter
            }
        } else { // supprimer
            $form->setType("droit", "hiddenstatic");
        }
    }

    /**
     * Parametrage du formulaire - Select des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function setSelect(&$form, $maj, &$db, $DEBUG) {
        //
        require "../sql/".$db->phptype."/".$this->table.".form.inc";
        //
        if ($maj < 2) {
            $res = $db->query ($sql_profil);
            if (database::isError($res))
                die ($res->getMessage ()." ".$sql_profil);
            else
            {
                if ($DEBUG == 1)
                    echo " La requete ".$sql_profil." est executee.<br />";
                $contenu [0][0] = "";
                $contenu [1][0] = " Choisir un profil ";
                $k = 1;
                while ($row =& $res->fetchRow ())
                {
                    $contenu [0][$k]=$row [0];
                    $contenu [1][$k]=$row [1];
                    $k++;
                }
                $form->setSelect ("profil", $contenu);
            }
        }
    }

    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        $form->setLib("droit", _("Intitule du droit"));
        $form->setLib("profil", _("Profil"));
    }
    
    
    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille(&$form, $maj) {
        //
        $form->setTaille("profil", 2);
        $form->setTaille("droit", 30);
    }
    
    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax(&$form, $maj) {
        //
        $form->setMax("profil", 2);
        $form->setMax("droit", 100);
    }
    
}

?>
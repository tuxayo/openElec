<?php
/**
 * Ce fichier declare la classe carteretourTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class carteretourTraitement extends traitement {
    
    var $fichier = "carteretour";
    
    var $champs = array("id_elec");
    var $form_class = "no_confirmation no_status_update";
    
    function setContentForm() {
        //
        $this->form->setLib("id_elec", _("Presenter un code barre ou saisir un identifiant electeur"));
        $this->form->setType("id_elec", "text");
        $this->form->setTaille("id_elec", 15);
        $this->form->setMax("id_elec", 15);
    }
    
    function getValidButtonContent() {
        //
        return _("Saisie de la carte en retour");
    }
    
    function treatment () {
        //
        $this->LogToFile("start carteretour");
        //
        include "../sql/".$this->page->phptype."/trt_carteretour.inc";
        //
        if (isset($_POST["id_elec"]) and $_POST["id_elec"] != "") {
            //
            $id_elec = $_POST["id_elec"];
            //
            if (!is_numeric($id_elec)) {
                //
                $this->error = true;
                //
                $message = "[".$id_elec."] "._("L'identifiant saisi est incorrect.");
                //
                $this->LogToFile($message);
                $this->addToMessage($message);
            } else {
                //
                $query_select = "select * from electeur ";
                $query_select .= " where collectivite='".$_SESSION['collectivite']."' ";
                $query_select .= " and id_electeur=".$id_elec;
                //
                $res_select = $this->page->db->query($query_select);
                //
                if (database::isError($res_select, true)) {
                    //
                    $this->error = true;
                    //
                    $message = $res_select->getMessage()." - ".$res_select->getUserInfo();
                    $this->LogToFile($message);
                    //
                    $this->addToMessage(_("Contactez votre administrateur."));
                } else {
                    //
                    $nb = $res_select->numRows();
                    //
                    if ($nb != 1) {
                        //
                        $this->error = true;
                        //
                        $message = _("Cet identifiant d'electeur n'existe pas.");
                        $this->LogToFile($message);
                        $this->addToMessage($message);
                    } else {
                        //
                        $row_select = $res_select->fetchRow(DB_FETCHMODE_ASSOC);
                        //
                        $infos = $row_select['nom']." ".$row_select['prenom']." - ".$row_select['nom_usage'];
                        $infos .= " "._("ne(e) le")." ";
                        $infos .= substr($row_select['date_naissance'],8,2)."/".substr($row_select['date_naissance'],5,2)."/".substr($row_select['date_naissance'],0,4)." ";
                        //
                        if ($row_select["carte"] == 1) {
                            //
                            $this->error = true;
                            //
                            $message = $infos._("a deja une carte en retour.");
                            $this->LogToFile($message);
                            $this->addToMessage($message);
                        } else {
                            //
                            $query_update = "update electeur ";
                            $query_update .= " set carte='1' ";
                            $query_update .= " where id_electeur=".$id_elec;
                            //
                            $res_update = $this->page->db->query($query_update);
                            //
                            if (database::isError($res_update, true)) {
                                //
                                $this->error = true;
                                //
                                $message = $res_update->getMessage()." - ".$res_update->getUserInfo();
                                $this->LogToFile($message);
                                //
                                $this->addToMessage(_("Contactez votre administrateur."));
                            } else {
                                //
                                $message = $infos._("a maintenant une carte en retour.");
                                //
                                $this->LogToFile($message);
                                $this->addToMessage($message);
                            }
                        }
                    }
                }
            }
        } else {
            //
            $this->error = true;
            //
            $message = _("L'identifiant saisi est incorrect.");
            //
            $this->LogToFile($message);
            $this->addToMessage($message);
        }
        //
        $this->LogToFile("end carteretour");
    }
}

?>

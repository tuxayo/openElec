<?php
/**
 * Ce fichier declare la classe inseeExportTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class inseeExportTraitement extends traitement {
    
    var $fichier = "insee_export";
    var $champs = array("envoi", "format");
    
    function setContentForm() {
        //
        $this->form->setLib("envoi", _("Numero du transfert INSEE a renvoyer (Ne pas remplir s'il s'agit d'un nouveau transfert)"));
        $this->form->setType("envoi", "text");
        $this->form->setTaille("envoi", 3);
        $this->form->setMax("envoi", 3);
        //
        $this->form->setLib("format", _("Format du fichier d'export"));
        $this->form->setType("format", "select");
        $contenu = array(
            0 => array("txt", "xml"),
            1 => array("txt", "xml"),
            );
        $this->form->setSelect("format",$contenu);
    }
    
    function getValidButtonValue() {
        //
        return _("Generation du fichier Export INSEE");
    }

    function withoutaccent($elem = "") {
        //
        return iconv("UTF-8", "ASCII//TRANSLIT", $elem);
    }

    function treatment () {
        //
        $this->LogToFile ("start insee_export");
        //
        (isset($_POST['envoi']) ? $envoi = $_POST['envoi'] : $envoi = "");
        //
        (isset($_POST['format']) ? $format = $_POST['format'] : $format = "txt");
        //
        $fichier_cnen = "#";
        //
        $nbelecteur = 0;
        //
        $datetableau = $this->page->collectivite['datetableau'];
        //
        $chemin_cnen = $this->page->getParameter("chemin_cnen");
        
        $sql_departement_ville = "SELECT libelle_departement FROM departement WHERE code = '".substr($this->page->collectivite['inseeville'],0,2)."'";
        $departement_ville = $this->page->db->getone($sql_departement_ville);
        
        //
        include "../sql/".$this->page->phptype."/trt_insee_export.inc";
        
        // Execution de la requete
        $this->LogToFile ("REQUETE DE SELECTION DES MOUVEMENTS");
        $res_select_mouvement = $this->page->db->query($query_select_mouvement);
        if (database::isError($res_select_mouvement, true)) {
            //
            $this->error = true;
            //
            $message = $res_select_mouvement->getMessage()." erreur sur ".$query_select_mouvement."";
            $this->LogToFile($message);
        } else {
            //
            $nbcnen = $res_select_mouvement->numRows();
            $this->LogToFile($nbcnen." "._("mouvement(s) a envoyer a l'INSEE"));
            //
            if ($nbcnen == 0) {
                //
                $message = _("Aucun mouvement a envoyer. Aucun fichier n'a ete genere.");
                $this->LogToFile($message);
                $this->addToMessage($message);
            } else {
                //
                if ($format == "txt") {
                                        //
                    if ($envoi == "") { 
                        $envoi = $this->page->db->nextId('cnen');
                    }
                    $numEnvoi = str_pad($envoi, 3, "0", STR_PAD_LEFT);
                    //
                    $this->LogToFile(_("Envoi au format txt numero")." ".$numEnvoi);
                    //
                    $fichier_cnen = $chemin_cnen.$_SESSION['liste']."_tedeco_".date("dmy_Gis")."_".$numEnvoi.".txt";
                    $envoicnen = str_pad($envoi, 5, "0", STR_PAD_LEFT);
                    $nbcnen = str_pad($nbcnen, 5, "0", STR_PAD_LEFT);
                    $expediteurinsee = str_pad($this->page->collectivite['expediteurinsee'], 8, "0", STR_PAD_RIGHT);
                    // Ouverture du fichier
                    $inf = fopen($fichier_cnen, "w");
                    
                    // Composition de la premiere ligne du fichier, seulement
                    // dans le cas d'une liste principale (284 caractères)
                    $bl = "";
                    if ($_SESSION['liste'] === '01') {
                        $bl1 = str_pad($bl, 7, " ", STR_PAD_RIGHT);
                        $bl2 = str_pad($bl, 25, " ", STR_PAD_RIGHT);
                        $bl3 = str_pad($bl, 227, " ", STR_PAD_RIGHT);
                        $cnen = "0".$bl1.$envoicnen.$bl2.date('dmy').$nbcnen.$expediteurinsee.$bl3.$this->page->vars['newline'];
                        // Ecriture de la premiere du fichier
                        fwrite($inf, $cnen);
                    }
                    //
                    $i = 1;
                    //
                    while ($row =& $res_select_mouvement->fetchRow(DB_FETCHMODE_ASSOC)) {
                        //
                        $nbelecteur++;
                        // Mise a jour du mouvement
                        $fields_values = array ('envoi_cnen' => $envoi,
                                                'date_cnen' => date('Ymd'));
                        $res_update_mouvement_cnen = $this->page->db->autoExecute("mouvement", $fields_values, DB_AUTOQUERY_UPDATE, "id=".$row['id']);
                        if (database::isError($res_update_mouvement_cnen, true)) {
                            //
                            $this->error = true;
                            //
                            $message = $res_update_mouvement_cnen->getMessage()." - ".$res_update_mouvement_cnen->getUserInfo();
                            $this->LogToFile($message);
                            //
                            break;
                        }
                        $this->LogToFile($row['id']." ".$row['nom']." ".$row['prenom']);
                        
                        //
                        //
                        //
                        if ($_SESSION['liste'] === '01') {
                            // Type d'enregistrement
                            $typedenregistrement = "1";
                            // Nom et prenoms
                            //
                            $tmpnom = $this->withoutaccent($row['nom']);
                            //
                            $tmpprenom = $this->withoutaccent($row['prenom']);
                            //
                            $nomprenom = str_pad($tmpnom."*".$tmpprenom."/", 68, " ", STR_PAD_RIGHT);
                            //
                            // Sexe 1 = masculin 2 = feminin
                            if ($row['sexe'] == 'M') {
                                $sexe = "1";
                            } elseif ($row['sexe'] == 'F') {
                                $sexe = "2";
                            } else {
                                $sexe = "0";
                            }
                            // Date de naissance JJMMAA
                            $date_naissance = substr($row['date_naissance'],8,2).substr($row['date_naissance'],5,2).substr($row['date_naissance'],2,2);
                            $date_naissance = str_pad($date_naissance, 6, "0", STR_PAD_RIGHT);
                            // Code departement de naissance
                            $departement = substr($row['code_departement_naissance'], 0, 2);
                            $departement = str_pad($departement, 2, "0", STR_PAD_RIGHT);
                            $code_naissance = str_pad($row['code_departement_naissance'], 5, 0, STR_PAD_RIGHT);
                            // Zone reservee
                            $zonereservee1 = "000";
                            // Libelle de la commune de naissance 
                            // Nom du territoire outre-mer ou pays de naissance
                            if (substr($row['code_departement_naissance'],0,2)== "98" or
                                substr($row['code_departement_naissance'],0,2)== "99" ) {
                                // Libelle de la commune de naissance
                                $libellecommune = str_pad(substr($this->withoutaccent($row['libelle_lieu_de_naissance']),0,30),30," ", STR_PAD_RIGHT);
                                // Nom du territoire outre-mer ou pays de naissance
                                $libelledepartement = str_pad(substr($this->withoutaccent($row['libelle_departement_naissance']), 0,12),12," ", STR_PAD_RIGHT);
                            } else {
                                // Libelle de la commune de naissance
                                $libellecommune = str_pad(substr($this->withoutaccent($row['libelle_lieu_de_naissance']), 0,30),30," ", STR_PAD_RIGHT);
                                // Nom du territoire outre-mer ou pays de naissance
                                $libelledepartement = str_pad("",12," ", STR_PAD_RIGHT);
                            }
                            // Code de mise a jour (I = pour inscription avis modele A, R = pour radiation avis modele B2)
                            if ($row['typecat'] == 'Inscription') {
                                $maj = "I";
                            } elseif ($row['typecat'] == 'Radiation') {
                                $maj = "R";
                            } else {
                                $maj = "0";
                            }
                            // Date de reception de la demande en mairie (avis modele A)
                            // Date de l'inscription d'office des jeunes de 18 ans proposes par l'Insee et retenus par la commission administrative
                            // Date de la mise a jour (date indiquee sur le avis modele B2 : fait le ...) 
                            $date_modif = substr($row['date_modif'],8,2).substr($row['date_modif'],5,2).substr($row['date_modif'],2,2);
                            $date_modif = str_pad($date_modif,6,"0", STR_PAD_RIGHT);
                            // Numero d'enregistrement 
                            $enregistrement = str_pad($i, 6, "0", STR_PAD_LEFT);
                            // Code departement de la commune emettrice de l'avis
                            $depville = substr($this->withoutaccent($this->page->collectivite['inseeville']),0,2);
                            $depville = str_pad($depville, 2, "0", STR_PAD_RIGHT);
                            // Zone reservee
                            $zonereservee2 = "000";
                            //
                            $specific = "";
                            if ($row['typecat'] == "Inscription") {
                                //
                                // INSCRIPTION
                                //
                                // Position 142 - Zone reservee
                                $specific .= str_pad($bl, 1," ", STR_PAD_RIGHT);
                                // Position 143 - Code type d'inscription
                                // 1 pour inscription volontaire
                                // 2 pour inscription par decision judiciaire
                                // 8 pour inscription d'office jeunes de 18 ans
                                $codeinscription = substr($row['codeinscription'], 0, 1);
                                $specific .= str_pad($codeinscription, 1, " ", STR_PAD_RIGHT);
                                // Position 144 - Date de prise en compte de la demande (avis modele A)
                                $specific .= str_pad($bl, 6," ", STR_PAD_RIGHT);
                                // Position 150 - Libelle de l'ambassade ou du poste consulaire ou l'electeur
                                // demande sa radiation de la liste electorale consulaire 
                                $specific .= str_pad($bl, 30," ", STR_PAD_RIGHT);
                                // Position 180 - Zone reservee
                                $specific .= str_pad($bl, 13," ", STR_PAD_RIGHT);
                                // Position 193 - Libelle de la commune emettrice de l'avis 
                                $ville = substr($this->withoutaccent($this->page->collectivite['ville']), 0, 30);
                                $specific .= str_pad($ville, 30, " ", STR_PAD_RIGHT);
                                // Position 223 - Code departement de la commune d'inscription anterieure 
                                $provenance = substr($row['provenance'], 0, 2);
                                $specific .= str_pad($provenance, 2, " ", STR_PAD_RIGHT);
                                // Position 225 - Libelle de la commune d'inscription anterieure 
                                $libelle_provenance = substr($this->withoutaccent($row['libelle_provenance']), 0, 30);
                                $specific .= str_pad($libelle_provenance, 30, " ", STR_PAD_RIGHT);
                                // Position 255 - Libelle du DOM ou autre territoire outre-mer d'inscription antrieure
                                $tom = "";
                                if (substr($row['provenance'], 0, 2) == "98") {
                                    $tomdep = substr($row['provenance'], 0, 3);
                                    include "../sql/".$this->page->phptype."/trt_insee_export.inc";
                                    $tom = $this->page->db->getOne($query_libelle_departement);
                                    if (database::isError($tom)) {
                                        die ($tom -> getMessage ()." erreur sur ".$query_libelle_departement);
                                    }
                                    $tom = substr($this->withoutaccent($tom), 0 ,30);
                                }
                                $specific .= str_pad($tom, 30, " ", STR_PAD_RIGHT);
                            } elseif ($row['typecat'] == "Radiation") {
                                //
                                // RADIATION
                                //
                                // Position 142 - Motif de la radiation
                                // - 'P' pour perte des qualités requises par la loi (principalement, depart vers une autre commune)
                                // - 'D' pour deces
                                // - 'J' pour decision du juge du tribunal d'instance ou arret de la cour de cassation
                                // - 'E' pour rectification d'erreur materielle par la commission administrative
                                // Les avis de radiation modele C envoyes par l'Insee ne doivent pas donner lieu ensuite e l'envoi d'avis B.
                                $coderadiation = substr($row['coderadiation'], 0, 1);
                                $specific .= str_pad($coderadiation, 1, " ", STR_PAD_RIGHT);
                                // Position 143 - Zone reservee
                                $specific .= str_pad($bl, 50," ", STR_PAD_RIGHT);
                                // Position 193 - Libelle de la commune emettrice de l'avis 
                                $ville = substr($this->withoutaccent($this->page->collectivite['ville']), 0, 30);
                                $specific .= str_pad($ville, 30, " ", STR_PAD_RIGHT);
                                // Position 223 - Zone reservee
                                $specific .= str_pad($bl, 62," ", STR_PAD_RIGHT);
                            }
                            // Concatenation des champs
                            $cnen = $typedenregistrement.$nomprenom.$sexe.$date_naissance.$code_naissance;
                            $cnen .= $libellecommune.$libelledepartement.$maj.$date_modif;
                            $cnen .= $enregistrement.$depville.$zonereservee2.$specific.$this->page->vars['newline'];
                        } else {
                            // Export liste complémentaire
                            
                            // Position 1 - Type de mouvement
                            if ($row['typecat'] == "Inscription") {
                                // Inscriptions: 
                                // A2M pour une inscription volontaire sur 
                                // liste élections municipales (avis modèle 
                                // ACM, modalité inscription volontaire) 
                                // A2E pour une inscription volontaire sur 
                                // liste élections européennes (avis modèle 
                                // ACE, modalité inscription volontaire) 
                                // A3M pour une inscription par décision 
                                // judiciaire sur liste élections municipales 
                                // (avis modèle ACM, modalité inscription 
                                // judiciaire) 
                                // A3E pour une inscription par décision 
                                // judiciaire sur liste élections européennes 
                                if ($row['codeinscription'] === '2') {
                                    $type_mvmt = "A3";
                                } else {
                                    $type_mvmt = "A2";
                                }
                            } else {
                                
                                // Radiations
                                // Sans changement, code du formulaire utilisé :
                                // B2E, B3E, B2M, B3M
                                // Implémentation :
                                // - B2 Radiation générale
                                // - B3 judiciaire
                                if ($row['coderadiation'] === 'J') {
                                    $type_mvmt = "B3";
                                } else {
                                    $type_mvmt = "B2";
                                }
                            }
                            if ($_SESSION['liste'] === '02') {
                                $type_mvmt .= "E";
                            } else {
                                $type_mvmt .= "M";
                            }
                            $cnen = str_pad($type_mvmt, 3," ", STR_PAD_RIGHT);
                            
                            // Position 4 - Numéro d'envoi du fichier
                            $cnen .= $numEnvoi;
                            
                            // Position 7 - Date de création du fichier
                            $cnen .= date('Ymd');
                            
                            // Position 15 - Identifiant du correspondant (avec clé)
                            $cnen .= $expediteurinsee;
                            
                            // Position 23 - Zone réservée
                            $cnen .= str_pad($bl, 3," ", STR_PAD_RIGHT);
                            
                            // Position 26 - Numéro d'enregistrement
                            $cnen .= str_pad($i, 6, "0", STR_PAD_LEFT);
                            
                            // Position 32 - Code du département d'inscription ou radiation
                            $depville = substr($this->withoutaccent($this->page->collectivite['inseeville']),0,2);
                            $depville = str_pad($depville, 2, "0", STR_PAD_RIGHT);
                            $cnen .= $depville;
                            
                            // Position 34 - Commune inscription/radiation
                            $ville = substr($this->withoutaccent($this->page->collectivite['ville']), 0, 30);
                            $cnen .= str_pad($ville, 30, " ", STR_PAD_RIGHT);
                            
                            // Position 64 - Département inscription/radiation
                            $cnen .= str_pad($departement_ville, 30, " ", STR_PAD_RIGHT);
                            
                            // Position 94 - Nationalité
                            $cnen .= str_pad($row['libelle_nationalite'], 15, " ", STR_PAD_RIGHT);
                            
                            // Position 109 - Nom patronymique
                            $tmpnom = $this->withoutaccent($row['nom']);
                            $cnen .= str_pad($tmpnom, 63, " ", STR_PAD_RIGHT);
                            
                            // Position 172 - Prénoms
                            $tmpprenom = $this->withoutaccent($row['prenom']);
                            $cnen .= str_pad($tmpprenom, 50, " ", STR_PAD_RIGHT);
                            
                            // Position 222 - Sexe
                            $cnen .= $row['sexe'];
                            
                            // Position 223 - Date de naissance
                            $date_naissance = date('Ymd', strtotime($row['date_naissance']));
                            $cnen .= str_pad($date_naissance, 8, "0", STR_PAD_RIGHT);
                            
                            // Position 231 - Pays de naissance
                            $libelle_departement_naissance = substr($row['libelle_departement_naissance'], 0, 30);
                            $cnen .= str_pad($libelle_departement_naissance, 30, " ", STR_PAD_RIGHT);
                            
                            // Position 261 - Département de naissance (code)
                            $code_lieu_de_naissance = substr($row['code_departement_naissance'],0,2);
                            $cnen .= str_pad($code_lieu_de_naissance, 2, " ", STR_PAD_RIGHT);
                            
                            // Position 263 - Subdivision administrative du lieu
                            // de naissance
                            $cnen .= str_pad($bl, 30," ", STR_PAD_RIGHT);
                            
                            // Position 293 - Commune ou localité de naissance
                            $libelle_lieu_de_naissance = substr($row['libelle_lieu_de_naissance'], 0, 30);
                            $cnen .= str_pad($libelle_lieu_de_naissance, 30, " ", STR_PAD_RIGHT);
                            
                            // Position 323 - Date d'inscription ou radiation
                            $date_modif = date('Ymd', strtotime($row['date_modif']));
                            $cnen .= str_pad($date_modif, 8, "0", STR_PAD_RIGHT);
                            
                            // Position 331 - Pays d'autre inscription
                            $cnen .= str_pad($bl, 30," ", STR_PAD_RIGHT);
                            
                            // Position 361 - Subdivision administrative
                            $cnen .= str_pad($bl, 2," ", STR_PAD_RIGHT);
                            
                            // Position 363 - Subdivision administrative
                            $cnen .= str_pad($bl, 30," ", STR_PAD_RIGHT);
                            
                            // Position 393 - Commune ou localité d'autre inscription
                            $cnen .= str_pad($bl, 30," ", STR_PAD_RIGHT);
                            
                            // Position 423 - Commune ou localité d'autre inscription
                            $cnen .= str_pad($bl, 78," ", STR_PAD_RIGHT);
                            
                            $cnen .= $this->page->vars['newline'];

                        }
                        //
                        //
                        //
                        
                        // Ecriture de la ligne
                        fwrite($inf, $cnen);
                        // On incremente le compteur
                        $i++;
                    }
                    // Fermeture du fichier
                    fclose($inf);
                } elseif ($format == "xml") {
                    // Vérification de la configuration des partenaires
                    $sql = "SELECT (SELECT count(*)
                        FROM xml_partenaire 
                        WHERE collectivite='".$_SESSION["collectivite"]."'
                        AND type_partenaire='origine') as origine, 
                        (SELECT count(*)
                        FROM xml_partenaire 
                        WHERE collectivite='".$_SESSION["collectivite"]."'
                        AND type_partenaire='destination') as destination";
                    $res = $this->page->db->query ($sql);
                    if (database::isError($res)) {
                        die ($res -> getMessage ()." erreur sur ".$sql);
                    }
                    while ($row =& $res -> fetchRow (DB_FETCHMODE_ASSOC)) {
                        // Si aucun partenaire n'est configuré
                        if (intval($row['origine']) == 0
                            AND intval($row['destination']) == 0) {
                            //
                            $this->error = true;
                            //
                            $message = _("Les partenaires d'origine et de destination des exports xml n'ont pas ete parametre.");
                            $this->addToMessage($message);
                            $message = "<a href='../scr/tab.php?obj=xml_partenaire'";
                            $message .= " class='om-prev-icon parametrage-16'";
                            $message .= " title=\""._("Formulaire de parametrage des partenaires pour l'export INSEE XML")."\">";
                            $message .= _("Parametrer les partenaires.");
                            $message .= "</a>";
                            $this->addToMessage($message);
                        }elseif (intval($row['origine']) == 0) {
                            // Si origine n'est pas configurée
                            $this->error = true;
                            //
                            $message = _("Le partenaire d'origine des exports xml n'a pas ete parametre");
                            $this->addToMessage($message);
                        } elseif (intval($row['destination']) == 0) {
                            // Si destination n'est pas configurée
                            $this->error = true;
                            //
                            $message = _("Le partenaire de destination des exports xml n'a pas ete parametre");
                            $this->addToMessage($message);
                        }
                    }
                    if($this->error === true) {
                        // Log et affichage des erreurs
                        $this->LogToFile("traitement.insee_export.class.php : 
                            treatment() ".$message);
                    } else {
                        //
                        if ($envoi == "") { 
                            $envoi = $this->page->db->nextId('cnen');
                        }
                        $numEnvoi = str_pad($envoi, 3, "0", STR_PAD_LEFT);
                        // Si pas d'erreurs on effectue le traitement
                        $this->LogToFile(_("Envoi au format xml numero")." ".$numEnvoi);
                        //
                        $fichier_cnen = $chemin_cnen.$_SESSION['liste']."_tedeco_".date("dmy_Gis")."_".$numEnvoi.".xml";
                        
                        // Generation du fichier
                        require_once "../obj/insee_export.class.php";
                        $xml = new insee_export($this->page, $fichier_cnen, $envoi, $_POST['envoi']);
                        $nbelecteur = $xml->cpt();
                        $xml->output();
                    }

                } else {
                    //
                    $this->error = true;
                    //
                    $message = _("Le format d'export selectionne n'existe pas.");
                    $this->LogToFile("traitement.insee_export.class.php : 
                        treatment() ".$message);
                }
                //
                $this->LogToFile(_("Nombre de mouvements en sortie pour l'export :")." ".$nbelecteur);
            }
        }
        //
        $this->LogToFile ("end insee_export");
    }
}

?>

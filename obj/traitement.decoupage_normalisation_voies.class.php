<?php
/**
 * Ce fichier declare la classe DecoupageNormalisationVoiesTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class DecoupageNormalisationVoiesTraitement extends traitement {
    
    var $fichier = "decoupage_normalisation_voies";
    
    /**
     * Description du traitement.
     */
    function getDescription() {
        //
        return _("La normalisation vous permet de 'nettoyer' votre base de 
                 donnees en supprimant les doublons. Pour ce faire il vous 
                 suffit de cocher deux libelles de voie qui font references 
                 en realite a la meme voie puis de saisir le nouveau libelle 
                 pour ces deux voies. Si vous avez plus de deux voies a fusionner 
                 il vous suffit de faire le traitement plusieurs fois.");
    }
    
    /**
     * Libellé du bouton de validation du traitement.
     *
     * @return String Libellé.
     */
    function getValidButtonValue() {
        //
        return _("Fusionner les voies");
    }

    /**
     * Recupère la liste des voies contenu dans la table voies.
     *
     * @return array Liste des voies.
     */
    function get_voies() {
        // Inclusion du script contenant les requêtes
        include "../sql/".$this->page->phptype."/trt_".$this->fichier.".inc";

        // Execution de la requête
        $results = $this->page->db->query($sql_voies);
        $this->page->addToLog(
            "normalisation_voies.php: db->query(\"".$sql_voies."\");",
            VERBOSE_MODE
        );
        // Vérification et log des erreurs
        if (database::isError($results, true)) {
            //
            $this->error = true;
            //
            $message = $results->getMessage()." - ".$results->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        }
        
        // Formatage du tableau des voies
        $voies = array();
        while ($row =& $results->fetchrow(DB_FETCHMODE_ASSOC)) {
            $voies[$row['code']] = $row;
        }
        
        return $voies;
    }

    /**
     * Récupération des informations de la voie passée en paramètre.
     *
     * @param integer $id identifiant de la voie
     *
     * @return array tableau contenant les informations
     */
    function get_voie_infos($id) {
        // Inclusion du script contenant les requêtes
        include "../sql/".$this->page->phptype."/trt_".$this->fichier.".inc";

        // Execution de la requête
        $results = $this->page->db->query(str_replace('<id>', $id, $sql_voie_infos));
        $this->page->addToLog(
            "normalisation_voies.php: db->query(\"".$sql_voie_infos."\");",
            VERBOSE_MODE
        );
        if (database::isError($results, true)) {
            //
            $this->error = true;
            //
            $message = $results->getMessage()." - ".$results->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        }
        
        // Formatage du tableau des voies
        $infos_voie = null;
        while ($row =& $results->fetchrow(DB_FETCHMODE_ASSOC)) {
            $infos_voie = $row;
        }
        
        return $infos_voie;
    }

    /**
     * Permet d'ajouter la nouvelle voie en base de données.
     *
     * @param string $new_label nouveau nom de la voie
     * @param string $cp        code postal de la voie
     * @param string $ville     ville de la voie
     * @param string $code_coll code INSEE de la collectivité
     */
    function add_new_voie($new_label, $cp, $ville, $code_coll) {
        // Increment de la sequence
        $id = $this->page->db->nextId(DB_PREFIXE."voie");
        if (get_magic_quotes_gpc()) {
            $new_label = StripSlashes($new_label);
        }
        // Valeurs des champs de la nouvelle voie
        $valF = array(
            "code" => $id,
            "libelle_voie" => $new_label,
            "cp" => $cp,
            "ville" => $ville,
            "abrege" => null,
            "code_collectivite" => $code_coll,
        );
        // Execution de la requête
        $results = $this->page->db->autoExecute(
            DB_PREFIXE."voie",
            $valF,
            DB_AUTOQUERY_INSERT
        );
        $this->page->addToLog(
            "normalisation_voies.php: db->autoExecute(
                ".DB_PREFIXE."voie, ".print_r($valF, true).",
                DB_AUTOQUERY_INSERT);",
            VERBOSE_MODE
        );
        // Vérification et log d'erreur
        if (database::isError($results, true)) {
            //
            $this->error = true;
            //
            $message = $results->getMessage()." - ".$results->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        } else {
            $this->LogToFile(
                date("Y-m-d_G:i:s")." - Création d'une nouvelle voie code ".$id.
                    " et libellé ".$new_label
            );
            // Pour les voies sélectionnées :
            foreach ($_POST as $code => $state) {
                // On réaffecte les électeurs de cette voie sur la nouvelle
                $this->update_electeurs($id, $code, $new_label);
                // Idem pour les mouvements
                $this->update_mouvements($id, $code, $new_label);
                // On supprime cette voie
                $this->delete_old_voies($code);
            }
            $this->LogToFile(date("Y-m-d_G:i:s")." - Fin");
        }
    }

    /**
     * On réaffecte les électeurs d'une voie a fusionner vers la nouvelle.
     *
     * @param integer $id        identifiant de l'électeur
     * @param integer $old_code  identifiant de la voie qui est fusionnée
     * @param integer $new_label identifiant de la voie resultante de la fusion
     */
    function update_electeurs($id, $old_code, $new_label) {
        // Inclusion du script contenant les requêtes
        include "../sql/".$this->page->phptype."/trt_".$this->fichier.".inc";
        // Execution de la requête de récupération des électeurs de la voie fusionnée
        $results = $this->page->db->query(
            str_replace("<old_code>", $old_code, $sql_update_electeurs)
        );
        $this->page->addToLog(
            "normalisation_voies.php: db->query(\"".
                str_replace("<old_code>", $old_code, $sql_update_electeurs).
            "\");",
            VERBOSE_MODE
        );
        // Vérification et log des erreurs
        if (database::isError($results, true)) {
            //
            $this->error = true;
            //
            $message = $results->getMessage()." - ".$results->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        } else {
            
            // Le tableau permettant l'update est populé
            $users = array();
            while ($row =& $results->fetchrow(DB_FETCHMODE_ASSOC)) {
                array_push($users, $row);
                $this->LogToFile(
                    date("Y-m-d_G:i:s")." - Mise à jour de l'electeur id : ".
                        $row['id_electeur']
                );
            }
            //
            $valF = array(
                "code_voie" => $id,
                "libelle_voie" => $new_label,
            );
            //
            $results = $this->page->db->autoExecute(
                DB_PREFIXE."electeur",
                $valF,
                DB_AUTOQUERY_UPDATE,
                "code_voie = '".$old_code."'"
            );
            $this->page->addToLog(
                "normalisation_voies.php: db->autoExecute(".
                    DB_PREFIXE."electeur, ".
                    print_r($valF, true).",
                    DB_AUTOQUERY_UPDATE,
                    code_voie = '".$old_code."');",
                VERBOSE_MODE
            );
            // Vérification et log des erreurs de l'update
            if (database::isError($results, true)) {
                //
                $this->error = true;
                //
                $message = $results->getMessage()." - ".$results->getUserInfo();
                $this->LogToFile($message);
                //
                $this->addToMessage(_("Contactez votre administrateur."));
            }
        }
    }

    /**
     * On réaffecte les mouvements d'électeurs d'une voie a fusionner vers la nouvelle.
     *
     * @param integer $id        identifiant de l'électeur
     * @param integer $old_code  identifiant de la voie qui est fusionnée
     * @param integer $new_label identifiant de la voie resultante de la fusion
     */
    function update_mouvements($id, $old_code, $new_label) {
        // Inclusion du script contenant les requêtes
        include "../sql/".$this->page->phptype."/trt_".$this->fichier.".inc";
        // Execution de la requête de récupération des électeurs de la voie fusionnée
        $results = $this->page->db->query(
            str_replace("<old_code>", $old_code, $sql_update_mouvements)
        );
        $this->page->addToLog(
            "normalisation_voies.php: db->query(\"".
                str_replace("<old_code>", $old_code, $sql_update_mouvements).
            "\");",
            VERBOSE_MODE
        );
        // Vérification et log des erreurs
        if (database::isError($results, true)) {
            //
            $this->error = true;
            //
            $message = $results->getMessage()." - ".$results->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        } else {
            //
            $moves = array();
            while ($row =& $results->fetchrow(DB_FETCHMODE_ASSOC)) {
                array_push($moves, $row);
                $this->LogToFile(
                    date("Y-m-d_G:i:s")." - Mise à jour du mouvement id : ".
                    $row['id']." correspondant à l'electeur id :".$row['id_electeur']
                );
            }
            // Le tableau permettant l'update est populé
            $valF = array(
                "code_voie" => $id,
                "libelle_voie" => $new_label,
            );    
            //
            $results = $this->page->db->autoExecute(
                DB_PREFIXE."mouvement",
                $valF,
                DB_AUTOQUERY_UPDATE,
                "code_voie = '".$old_code."'"
            );
            $this->page->addToLog(
                "normalisation_voies.php: db->autoExecute(".
                    DB_PREFIXE."mouvement, ".
                    print_r($valF, true).",
                    DB_AUTOQUERY_UPDATE,
                    code_voie = '".$old_code."'
                );",
                VERBOSE_MODE
            );
            // Vérification et log des erreurs
            if (database::isError($results, true)) {
                //
                $this->error = true;
                //
                $message = $results->getMessage()." - ".$results->getUserInfo();
                $this->LogToFile($message);
                //
                $this->addToMessage(_("Contactez votre administrateur."));
            }
        }
    }

    /**
     * Suppression de la voie passée en paramètre.
     *
     * @param integer $old_code identifiant de la voie à supprimer
     */
    function delete_old_voies($old_code) {
        // Inclusion du script contenant les requêtes
        include "../sql/".$this->page->phptype."/trt_".$this->fichier.".inc";
        // Execution de la requête de récupération des électeurs de la voie fusionnée
        $results = $this->page->db->query(
            str_replace("<old_code>", $old_code, $sql_select_old_voies)
        );
        $this->page->addToLog(
            "normalisation_voies.php: db->query(\"".
                str_replace("<old_code>", $old_code, $sql_select_old_voies).
            "\");",
            VERBOSE_MODE
        );
        // Vérification et log des erreurs
        if (database::isError($results, true)) {
            //
            $this->error = true;
            //
            $message = $results->getMessage()." - ".$results->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        } else {
            //
            $voies = array();
            while ($row =& $results->fetchrow(DB_FETCHMODE_ASSOC)) {
                array_push($voies, $row);
                $this->LogToFile(
                    date("Y-m-d_G:i:s")." - Suppression de l'ancinne voie code : ".
                        $row['code']
                );
            }
            //
            $results = $this->page->db->query(
                str_replace("<old_code>", $old_code, $sql_delete_old_voies)
            );
            $this->page->addToLog(
                "normalisation_voies.php: db->query(\"".
                    str_replace("<old_code>", $old_code, $sql_delete_old_voies).
                "\");",
                VERBOSE_MODE
            );
            if (database::isError($results, true)) {
                //
                $this->error = true;
                //
                $message = $results->getMessage()." - ".$results->getUserInfo();
                $this->LogToFile($message);
                //
                $this->addToMessage(_("Contactez votre administrateur."));
            }
        }
    }
    
    /**
     * Méthode d'initialisation du traitement.
     */
    function treatment () {
        //
        $this->LogToFile("start decoupage_normalisation_voies");
        // Vérification qu'un nouveau libellé est renseigné
        if(empty($_POST['new_label'])) {
            //
            $this->error = true;
            //
            $this->addToMessage(
                _("Veuillez renseigner le champ : Libelle de la nouvelle voie.")
            );
            $message = _("Aucun nouveau libellé de voie n'a été fourni");
            $this->LogToFile($message);
        } else {
            // get label and remove it
            // to iterate over all remaining
            // id (keys)
            $new_label = $_POST['new_label'];
            unset($_POST['new_label']);
            unset($_POST['traitement_decoupage_normalisation_voies_form_valid']);
            
            // get informations
            $random_id =  key($_POST);
            // Vérification que 2 voies ont bien été saisies
            if(count($_POST) == 2) {
                // Récupération des infos d'une des 2 voies
                $infos = $this->get_voie_infos($random_id);
                //
                $this->add_new_voie($new_label,
                            $infos['cp'],
                            $infos['ville'],
                            $infos['code_collectivite']);
            } else {
                $this->error = true;
                //
                $this->addToMessage(_("Veuillez selectionner 2 voies a fusionner."));
                $message = _("Les 2 voies n'ont pas ete selectionnees.");
                $this->LogToFile($message);
            }
        }
    }

    /**
     * Surchage de l'affichage du formulaire.
     */
    function displayForm() {
        // Autocommit false
        $this->execute(true);
        // Récupération de la liste des voies
        $voies = $this->get_voies();
        // Vérificatoin de la présence de voies
        if (empty($voies)) {
            //
            $class = "valid";
            $this->page->displayMessage(
                $class,
                "Il n'y a aucune voie dans la commune actuelle."
            );
        } else {
            // Prévention de sélection de plus de 2 voies
            echo "<script type='text/javascript'>
                $(function() {
                    $('input.checkbox').click(function() {
                        if ($(this).attr('checked')) {
                            cpt = 0;
                            $('input.checkbox').each(
                                 function() {
                                    if ($(this).attr('checked')) {
                                        cpt = cpt + 1;
                                    // Insert code here 
                                 }
                                 }
                            );
                            if (cpt > 2) {
                                alert('Vous ne pouvez pas selectionner plus de 2 voies.');
                                return false;
                            }
                        }
                    });
                });
            </script>";
            // -- form
            echo "<form";
            echo " method=\"post\"";
            echo " id=\"traitement_".$this->fichier."_form\"";
            echo " onsubmit=\"trt_form_submit('traitement_".$this->fichier."_result',
                    '".$this->fichier."', this);return false;\"";
            echo " class=\"trt_form".
                    ($this->form_class != "" ? " ".$this->form_class : "")."\"";
            echo " action=\"#\"";
            if ($this->form_name != "") {
                echo " name=\"".$this->form_name."\"";
            }
            echo ">\n";
            
            // Description du traitement
            $description = $this->getDescription();
            if ($description != "") {
                //
                echo "<div class=\"instructions\">\n";
                echo "\t";
                echo $this->getDescription();
                echo "\n";
                echo "</div>\n";
            }
        
            // Affichage du message d'erreur ou validation
            $this->displayResultSection();
            // Affichage du contenu du formulaire
            $this->displayFormSection();
            // Bouton de validation
            echo "<div class=\"formControls\">\n";
            echo "\t<input type=\"submit\" ";
            echo "name=\"traitement.".$this->fichier.".form.valid\" ";
            echo "value=\"".$this->getValidButtonValue()."\" ";
            echo "class=\"boutonFormulaire\" />\n";
            echo "</div>\n";
            echo "</form>";
        }
    }

    /**
     * Affichage du contenu du formulaire.
     *
     * @param boolean $form_only défini si seul le formulaire est affiché ou non
     */
    function displayFormSection($form_only = false) {
        $voies = $this->get_voies();
        // Affichage de contenu avant les champs
        echo "<div id=\"traitement_".$this->fichier."_status\">";
        if ($form_only == false) {
            $this->displayBeforeContentForm();
        }
        
        echo "<fieldset
                id=\"normalisation_voies\"
                class=\"startClosed cadre ui-corner-all ui-widget-content\">
                <legend  class=\"ui-corner-all ui-widget-content ui-state-active\">".
            _("Voies pouvant etre fusionnees :")."</legend><div>";
        // Une case à cocher par voie
        foreach ($voies as $voie) {
            echo "<div class=\"choice\">";
            echo "<input class=\"checkbox\" type=\"checkbox\" id=\"".
                    str_replace(" ", "_", $voie['libelle_voie'])."\" name=\"".$voie['code']."\">&nbsp;
                    <label for=\"".str_replace(" ", "_", $voie['libelle_voie'])."\">".$voie['libelle_voie'].
                    "</label><br/>";
            echo "</div>";
        }
        echo "</div></fieldset>";
        echo "<div class=\"visualClear\"></div>";
        echo "<label for=\"new_label\">".("Libellé de la nouvelle voie").
                "&nbsp;</label><input type=\"text\" id=\"new_label\" name=\"new_label\"
                size=\"28\" onchange=\"this.value=this.value.toUpperCase()\"><br/>";

        // Affichage du contenu après les champs
        if ($form_only == false) {
            $this->displayAfterContentForm();
        }
        // -- end form
        echo "</div>\n";
    }
}

?>

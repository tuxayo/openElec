<?php
/**
 * Ce fichier permet de definir la classe decoupage
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class prefectureDematerialisationListeElectoraleTraitement extends traitement {

    /**
     *
     */
    var $liste_electorale = array();
    var $global_infos = array();
    var $fichier = "prefecture_dematerialisation_liste_electorale";
    var $champs = array("format", );
    
    function setContentForm() {
        //
        $this->form->setLib("format", _("Format du fichier d'export"));
        $this->form->setType("format", "select");
        $contenu = array(
            0 => array("xml", "csv", ),
            1 => array("xml", "csv", ),
            );
        $this->form->setSelect("format",$contenu);
    }
    
    function getValidButtonValue() {
        //
        return _("Generation du fichier");
    }

    function withoutaccent($elem = "") {
        //
        return iconv("UTF-8", "ASCII//TRANSLIT", $elem);
    }

    private function get_global_infos() {
        //
        $this->global_infos = array(
            "CodeDepartement" => substr($this->page->collectivite["inseeville"], 0, 2),
            "CodeCommune" => $this->page->collectivite["inseeville"],
            "NomCommune" => $this->page->collectivite["ville"],
            "TypeListe" => "",
            "InfoListe" => "Liste électorale",
            "DateCreationListe" => date("d/m/Y"),
        );
        // @param $liste Identifiant de la liste sur laquelle travaille l'utilisateur
        // Paramètres
        $liste = $_SESSION["liste"];
        //
        $query = "select UPPER(liste.liste_insee) from liste where liste.liste='".$liste."'";
        $res = $this->page->db->getone($query);
        $this->page->addToLog("traitement.prefecture_dematerialisation_liste_electorale.class.php: db->getone(\"".$query."\");", VERBOSE_MODE);
        //
        if (database::isError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." erreur sur ".$query."";
            $this->LogToFile($message);
        } else {
            //
            $this->global_infos["TypeListe"] = $res;
        }
    }

    /**
     * 
     */
    private function get_liste_electorale() {
        // @param $liste Identifiant de la liste sur laquelle travaille l'utilisateur
        // @param $collectivite Identifant de la collectivité sur laquelle travaille
        //        l'utilisateur
        // Paramètres
        $liste = $_SESSION["liste"];
        $collectivite = $_SESSION["collectivite"];
        // Requête
        $query = "SELECT 

            electeur.nom as NomdeNaissance,

            electeur.prenom as Prenoms,

            CASE electeur.resident
                WHEN 'Non' THEN
                electeur.libelle_voie
                WHEN 'Oui' THEN
                electeur.adresse_resident
            END as LibelleVoie,

            CASE electeur.resident
                WHEN 'Non' THEN
                voie.cp
                WHEN 'Oui' THEN
                electeur.cp_resident
            END as CodePostal,

            CASE electeur.resident
                WHEN 'Non' THEN
                voie.ville
                WHEN 'Oui' THEN
                electeur.ville_resident
            END as VilleLocalite,

            to_char(electeur.date_naissance, 'DD/MM/YYYY') as DateDeNaissance,

            electeur.code_departement_naissance as DeptNaissance, 

            CASE liste.liste_insee
                WHEN 'p'
                    THEN ''
                ELSE 
                    nationalite.libelle_nationalite 
            END AS Nationalite,

            electeur.nom_usage as NomDusage,

            electeur.sexe as Sexe,

            electeur.libelle_lieu_de_naissance as NomComNaissance,
            
            CASE 
                WHEN electeur.code_departement_naissance = '99'
                    OR electeur.code_departement_naissance >= '99000'
                    THEN electeur.libelle_departement_naissance
                ELSE 'FRANCE'
            END AS PaysNaissance,

            electeur.numero_electeur as NoElecteurGeneral,

            bureau.libelle_bureau as NomBureauDeVote,

            electeur.code_bureau as NoBureauDeVote,

            CASE electeur.resident
                WHEN 'Non' THEN
                    electeur.complement
                WHEN 'Oui' THEN
                    electeur.complement_resident
            END as ComplementDeLocalisation1,

            '' as ComplementDeLocalisation2,

            CASE electeur.resident
                WHEN 'Non' THEN
                    CASE electeur.numero_habitation 
                        WHEN 0 THEN '' 
                        ELSE (electeur.numero_habitation||' ') 
                    END
                    ||
                    CASE electeur.complement_numero 
                        WHEN '' THEN ''
                        ELSE (electeur.complement_numero||' ') 
                    END
                WHEN 'Oui' THEN
                    ''
            END as NumeroVoie,

            '' as TypeVoie,

            '' as LieuDit,

            '' as Pays,

            canton.code_insee as CodeCanton,

            '' as CodeCirconscription

        FROM
            electeur
            LEFT JOIN bureau 
                ON (electeur.code_bureau=bureau.code
                    AND electeur.collectivite=bureau.collectivite)
            LEFT JOIN canton ON bureau.code_canton=canton.code
            LEFT JOIN collectivite ON electeur.collectivite=collectivite.id
            LEFT JOIN liste ON electeur.liste=liste.liste
            LEFT JOIN voie ON electeur.code_voie=voie.code
            LEFT JOIN nationalite ON electeur.code_nationalite=nationalite.code
        WHERE
            electeur.liste='".$liste."'
            AND electeur.collectivite='".$collectivite."'";
        $res = $this->page->db->query($query);
        $this->page->addToLog("traitement.prefecture_dematerialisation_liste_electorale.class.php: db->query(\"".$query."\");", VERBOSE_MODE);
        //
        if (database::isError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." erreur sur ".$query."";
            $this->LogToFile($message);
        } else {
            //
            $electeurs = array();
            //
            while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                //
                $electeurs[] = $row;
            }
            $this->liste_electorale = $electeurs;
        }
    }

    function treatment () {
        //
        $this->LogToFile ("start prefecture_dematerialisation_liste_electorale");
        //
        $this->get_global_infos();
        //
        $this->get_liste_electorale();
        $message = "liste electorale : ".count($this->liste_electorale)." electeur(s)";
        $this->LogToFile($message);
        //
        (isset($_POST['format']) ? $format = $_POST['format'] : $format = "xml");
        $message = "format : ".$format;
        $this->LogToFile($message);
        //
        $folder = $this->page->getParameter("tmpdir");
        //
        $output = "";
        //
        if ($format == "xml") {
            $output = $this->output_xml();
        } elseif ($format == "csv") {
            $output = $this->output_csv();
        }
        //
        if ($this->error == false) {
            //
            $filename = $folder.$this->global_infos["CodeCommune"]."_LE_".$this->global_infos["TypeListe"]."_".date("Ymd_His").".".$format;
            //
            @$inf = fopen($filename, "w");
            if ($inf == false) {
                $this->error = true;
            } else {
                @$ret = fwrite($inf, $output);
                if ($ret == false) {
                    $this->error = true;
                } else {
                    @$ret = fclose($inf);
                    if ($ret == false) {
                        $this->error = true;
                    }
                }
            }
            if ($this->error == true) {
                //
                $message = "Impossible d'ecrire le fichier";
                $this->LogToFile($message);
            }
        }
        //
        $this->LogToFile ("end prefecture_dematerialisation_liste_electorale");
    }

    /**
     * La méthode csv n'a jamais été publiée et n'a jamais passée le test de 
     * validation sur le site du ministère. Deux specifications ont été 
     * fournies et sont contradictoires au sujet de l'ordre des colonnes.
     */
    private function output_csv() {
        // Initialisation de la chaine de sortie
        $output = "";
        // Ce tableau de clés définit l'ordre et le titre des 
        // colonnes du fichier CSV. Ces clés correspondent 
        // en CamelCase aux alias de la requête dans get_liste_electorale
        // qui eux sont en minuscules.
        $keys = array(
            //
            "NomdeNaissance",
            "Prenoms",
            "LibelleVoie",
            "CodePostal",
            "VilleLocalite",
            "DateDeNaissance",
            "DeptNaissance",
            "Nationalite",
            //
            "NomDusage",
            "Sexe",
            "NomComNaissance",
            "PaysNaissance",
            "NoElecteurGeneral",
            "NomBureauDeVote",
            "NoBureauDeVote",
            //
            "ComplementDeLocalisation1",
            "ComplementDeLocalisation2",
            "NumeroVoie",
            "TypeVoie",
            "LieuDit",
            "Pays",
            //
            "TypeListe",
            "InfoListe",
            "DateCreationListe",
            "CodeDepartement",
            "CodeCommune",
            "NomCommune",
            "CodeCanton",
            "CodeCirconscription",
        );
        // On écrit la première ligne du fichier
        // Cette ligne ne contient que les entêtes de colonnes
        foreach ($keys as $plop) {
            //
            $output .= $plop.";";
        }
        // Retrait du dernier ;
        $output = substr($output, 0, strlen($output)-1);
        // Caractère de fin de ligne
        $output .= "\n";
        // On écrit toutes les lignes de la liste électorale
        // Une ligne par électeur
        foreach ($this->liste_electorale as $electeur) {
            // Pour chaque clé du tableau de clés on affiche le champ
            // correspondant dans la requête dans l'ordre du tableau
            // de clés
            foreach ($keys as $plop) {
                // 
                if (isset($electeur[strtolower($plop)])) {
                    $output .= $electeur[strtolower($plop)].";";
                } else {
                    $output .= $this->global_infos[$plop].";";
                }
            }
            // Retrait du dernier ;
            $output = substr($output, 0, strlen($output)-1);
            // Caractère de fin de ligne
            $output .= "\n";
        }
        //
        return $output;
    }

    /**
     *
     */
    private function output_xml() {
        //
        $COLLECTIVITE = array(
            "name" => "COLLECTIVITE",
            "elements" => array(
                "CodeDepartement" => $this->global_infos["CodeDepartement"],
                "CodeCommune" => $this->global_infos["CodeCommune"],
                "NomCommune" => $this->global_infos["NomCommune"],
            ),
        );
        //
        $LISTE = array(
            "name" => "LISTE",
            "attributes" => array(
                "id" => "Liste_ID",
                "version" => "1.0",
                "xmlns:xsi" => "http://www.w3.org/2001/XMLSchema-instance",
                "xsi:schemaLocation" => "uri/listeElec.xsd",
            ),
            "elements" => array(
                "TypeListe" => $this->global_infos["TypeListe"],
                "InfoListe" => $this->global_infos["InfoListe"],
                "DateCreationListe" => $this->global_infos["DateCreationListe"],
                "COLLECTIVITE" => $COLLECTIVITE,
            ),
        );
        //
        foreach ($this->liste_electorale as $key => $value) {
            //
            $ADRESSEELECTEUR = array(
                "name" => "ADRESSEELECTEUR",
                "elements" => array(
                    "ComplementDeLocalisation1" => "",
                    "ComplementDeLocalisation2" => "",
                    "NumeroVoie" => "",
                    "TypeVoie" => "",
                    "LibelleVoie" => "",
                    "LieuDit" => "",
                    "CodePostal" => "",
                    "VilleLocalite" => "",
                    "Pays" => "",
                ),
            );
            //
            foreach ($ADRESSEELECTEUR["elements"] as $element_key => $element_value) {
                //
                $ADRESSEELECTEUR["elements"][$element_key] = 
                    $value[strtolower($element_key)];
            }
            //
            $ELECTEUR = array(
                "name" => "ELECTEUR",
                "elements" => array(
                    "NoElecteurGeneral" => "",
                    "Sexe" => "",
                    "NomdeNaissance" => "",
                    "NomDusage" => "",
                    "Prenoms" => "",
                    "DateDeNaissance" => "",
                    "NomComNaissance" => "",
                    "DeptNaissance" => "",
                    "PaysNaissance" => "",
                    "Nationalite" => "",
                    "NomBureauDeVote" => "",
                    "NoBureauDeVote" => "",
                    "CodeCanton" => "",
                    "CodeCirconscription" => "",
                    "ADRESSEELECTEUR" => $ADRESSEELECTEUR,
                ),
            );
            //
            foreach ($ELECTEUR["elements"] as $element_key => $element_value) {
                //
                if (isset($value[strtolower($element_key)])) {
                    $ELECTEUR["elements"][$element_key] = 
                        $value[strtolower($element_key)];
                }
            }
            //
            $LISTE["elements"]["ELECTEUR".$key] = $ELECTEUR;
        }
        //
        $this->my_xml_resource = new XMLWriter();
        $this->my_xml_resource->openMemory();
        // On commente l'indentation du fichier xml car on gagne 25% sur le
        // poids du fichier avec indentation 10Mo sans 7.5Mo
        //$this->my_xml_resource->setIndent(true);
        //$this->my_xml_resource->setIndentString('    ');
        //
        $this->my_xml_resource->startDocument('1.0', 'UTF-8');
        //
        $this->write_xml_bloc($LISTE);
        //
        $this->my_xml_resource->endDocument();
        //
        return $this->my_xml_resource->outputMemory();
    }

    /**
     *
     */
    private function write_xml_bloc($bloc) {
        //
        if (isset($bloc["name"])) {
            //
            $this->my_xml_resource->startElement($bloc["name"]);
            //
            if (isset($bloc["attributes"])) {
                foreach ($bloc["attributes"] as $key => $value) {
                    $this->my_xml_resource->writeAttribute($key, $value);
                }
            }
            //
            if (isset($bloc["elements"])) {
                foreach ($bloc["elements"] as $key => $value) {
                    if (is_array($value)) {
                        $this->write_xml_bloc($value);
                    } else {
                        $this->my_xml_resource->writeElement($key, $value);
                    }
                }
            }
            //
            $this->my_xml_resource->endElement();
        }
    }

}

?>

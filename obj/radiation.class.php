<?php
/**
 * Ce fichier permet de definir la classe radiation
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "mouvement_electeur.class.php";

/**
 * 
 */
class radiation extends mouvement_electeur {

    /**
     * Type de mouvement correspondant à la valeur de typecat dans la table
     * param_mouvement
     * @var string Type de mouvement
     */
    var $typeCat = "radiation";

    /**
     *
     */
    function radiation($id, &$db, $DEBUG = false) {
        //
        $this->constructeur($id, $db);
    }

    /**
     *
     */
    function setvalFAjout($val = array()) {
        //
        $this->valF['id_electeur'] = $val['id_electeur'];
        $this->valF['numero_bureau'] = $val['numero_bureau'];
        $this->valF['numero_electeur'] = $val['numero_electeur'];
    }

    /**
     *
     */
    function verifierAjout($val = array(), &$db = NULL) {
        // Verification du verrou electeur
        if ($this->isVerrouElecteur($val) == true) {
            $this->correct = false;
            $this->addToMessage(_("L'electeur a deja un mouvement en cours.")."<br/>");
        }
    }

    /**
     *
     */
    function verifier($val = array(), &$db = NULL, $DEBUG = false) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        //// Mouvement deja traite
        //// Inutile puisque la selection empeche le chargement des donnees dans
        //// le formulaire lorsque le mouvement est 'trs'
        //if ($this->val[array_search('etat', $this->champs)] == "trs") {
        //    $this->correct = false;
        //    $this->addToMessage(_("Operation impossible. Vous ne pouvez pas ".
        //                          "modifier un mouvement deja traite.")."<br/>");
        //}
        // Verification du type de mouvement
        if ($this->valF['types'] == "") {
            $this->correct = false;
            $this->addToMessage(_("Le choix du type de mouvement est obligatoire")."<br/>");
        }
        // ???
        $this->valF['date_naissance'] = $this->dateDB($this->valF['date_naissance']);
        $this->valF['date_tableau'] = $this->dateDB($this->valF['date_tableau']);
    }

    /**
     *
     */
    function triggerajouterapres($id, &$db, $val, $DEBUG) {
        // On active le verrou sur l'electeur
        $this->manageVerrouElecteur($id, $db, $val, true);
        //
        $this->setComputedVal();
    }

    /**
     *
     */
    function triggermodifierapres($id, &$db, $val, $DEBUG) {
        //
        $this->setComputedVal();
    }

    /**
     *
     */
    function triggersupprimerapres($id, &$db, $val, $DEBUG) {
        // On desactive le verrou sur l'electeur
        $this->manageVerrouElecteur($id, $db, $val, false);
    }

}

?>

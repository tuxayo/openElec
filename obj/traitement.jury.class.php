<?php
/**
 * Ce fichier declare la classe juryTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class juryTraitement extends traitement {

    var $fichier = "jury";

    function getValidButtonValue() {
        //
        return _("Tirage aleatoire");
    }

    function getDescription() {
        //
        return _("Attention, lancer le traitement efface la selection precedente stockee dans le systeme.");
    }

    function displayBeforeContentForm() {
        //
        include ("../sql/".$this->page->phptype."/trt_jury.inc");

        //
        $electeurs = $this->page->db->getone($query_count_electeurs);
        $this->addToLog("treatment(): db->getone(\"".$query_count_electeurs."\");", VERBOSE_MODE);
        database::isError($electeurs);

        //
        $res = $this->page->db->query($query_nb_jures_canton);
        $this->addToLog("treatment(): db->query(\"".$query_nb_jures_canton."\");", VERBOSE_MODE);
        database::isError($res);

        //
        echo "<p>";
        echo _("Le nombre d'electeurs dans la liste a la date du")." ";
        echo date('d/m/Y')." "._("est de")." ".$electeurs.".";
        echo "</p>\n";

        echo "<br/>";
        //
        echo "<p>";
        echo _("Electeur(s) a selectionner pour la liste preparatoire :");
        echo "</p>";
        //
        echo "<ul class=\"alt\">\n";
        //
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            //
            echo "\t<li>";
            echo _("Canton")." ".$row["canton"]." - ".$row["libelle_canton"]." : ".$row["nb_jures"];
            echo "</li>\n";
        }
        //
        echo "</ul>\n";
    }

    function treatment () {
        //
        $this->LogToFile("start jury");
        //
        include "../sql/".$this->page->phptype."/trt_jury.inc";
        //
        $valF = array();
        $valF["jury"] = "0";
        //
        $res = $this->page->db->autoExecute('electeur', $valF, DB_AUTOQUERY_UPDATE, "jury='1' and collectivite='".$_SESSION['collectivite']."'");
        $this->addToLog("treatment(): db->autoExecute(\"electeur\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"jury='1' and collectivite='".$_SESSION['collectivite']."'\");", VERBOSE_MODE);
        //
        if (database::isError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        } else {
            //
            $this->addToLog("treatment(): ".$this->page->db->affectedRows()." "._("electeur(s) jure(s) supprime(s)"), EXTRA_VERBOSE_MODE);
            //
            $message = $this->page->db->affectedRows()." "._("electeur(s) jure(s) supprime(s)");
            $this->LogToFile($message);
            // Couple(s) Canton/Nb Jures pour la collectivite en cours
            $res = $this->page->db->query($query_nb_jures_canton);
            $this->addToLog("treatment(): db->query(\"".$query_nb_jures_canton."\");", VERBOSE_MODE);
            //
            if (database::isError($res, true)) {
                //
                $this->error = true;
                //
                $message = $res->getMessage()." - ".$res->getUserInfo();
                $this->LogToFile($message);
                //
                $this->addToMessage(_("Contactez votre administrateur."));
            } else {
                //
                $where = "";
                $nb_jures_total = 0;
                $nb_cantons = 0;
                //
                while($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                    //
                    $canton = $row["canton"];
                    $njury = $row["nb_jures"];
                    $nb_jures_total += $njury;
                    $nb_cantons += 1;
                    //
                    $message = _("CANTON")." ".$canton;
                    $this->LogToFile($message);
                    $message = $njury." "._("electeur(s) a selectionner");
                    $this->LogToFile($message);
                    // Tirage
                    $selectj = "SELECT id_electeur, nom, nom_usage, prenom ";
                    $selectj .= "FROM electeur ";
                    $selectj .= "LEFT JOIN bureau ON electeur.code_bureau=bureau.code and electeur.collectivite=bureau.collectivite ";
                    $selectj .= "WHERE liste='".$_SESSION ['liste']."' ";
                    $selectj .= "AND code_canton='".$canton."' ";
                    $selectj .= "AND ((date_part('year',age(date_naissance))>=23 AND date_jeffectif IS NULL) ";
                    $selectj .= "    OR (date_jeffectif IS NOT NULL AND date_part('year',age(date_jeffectif))>=5)) ";
                    $selectj .= "AND electeur.collectivite='".$_SESSION['collectivite']."' ";
                    $selectj .= "ORDER BY RANDOM() ";
                    $selectj .= "LIMIT ".$njury.";";
                    //
                    $res_selectj = $this->page -> db->query($selectj);

                    //
                    if (database::isError($res_selectj, true)) {
                        //
                        $this->error = true;
                        //
                        $message = $res_selectj->getMessage()." - ".$res_selectj->getUserInfo();
                        $this->LogToFile($message);
                        //
                        $this->addToMessage(_("Contactez votre administrateur."));
                    } else {
                        //Mettre en jury=1
                        while ($rowj=$res_selectj->fetchRow(DB_FETCHMODE_ASSOC) ) {
                            //
                            $where .= " id_electeur='".$rowj['id_electeur']."' or ";
                            //
                            $message = $rowj['nom']." ".$rowj['prenom']." ".$rowj ['nom_usage']." => "._("selectionne comme jure d'assises");
                            $this->LogToFile($message);
                        }
                    }
                }
                //
                $where = substr($where, 0, strlen($where) - 4);
                //
                $valF = array();
                $valF["jury"] = "1";
                //
                $res_update_jures = $this->page->db->autoExecute('electeur', $valF, DB_AUTOQUERY_UPDATE, "(".$where.") and collectivite='".$_SESSION['collectivite']."'");
                $this->addToLog("treatment(): db->autoExecute(\"electeur\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"(".$where.") and collectivite='".$_SESSION['collectivite']."'\");", VERBOSE_MODE);
                //
                $message = $this->page->db->affectedRows()."/".$nb_jures_total." "._("electeur(s) selectionne(s) sur")." ".$nb_cantons." "._("canton(s)");
                $this->LogToFile($message);
                $this->addToMessage($message);
            }
        }
        //
        $this->LogToFile("end jury");
    }
}

?>

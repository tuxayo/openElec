<?php
/**
 * Ce fichier declare la classe carteretourEpurationTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class carteretourEpurationTraitement extends traitement {
    
    var $fichier = "carteretour_epuration";
    
    function getDescription() {
        //
        return _("Tous les electeurs etant marques avec une carte en retour ".
                 "ne le seront plus. Ce traitement se fait uniquement sur la ".
                 "liste en cours.");
    }
    
    function getValidButtonValue() {
        //
        return _("Epuration des cartes en retour");
    }
    
    function displayBeforeContentForm() {
        //
        include "../sql/".$this->page->phptype."/trt_carteretour_epuration.inc";
        //
        $res_carteretour = $this->page->db->query ($sql_nbcarteretour);
        $this->page->isDatabaseError($res_carteretour);
        $nb_carteretour = $res_carteretour->numRows ();
        $res_carteretour->free();
        //
        echo "\n<div class=\"field\">\n\t<label>";
        echo _("Le nombre d'electeurs etant marques avec une carte en retour a la date du")." ";
        echo date('d/m/Y')." "._("est de")." ".$nb_carteretour.".";
        echo "</label>\n</div>\n";
    }
    
    function treatment () {
        //
        $this->LogToFile("start carteretour_epuration");
        //
        include "../sql/".$this->page->phptype."/trt_carteretour_epuration.inc";
        //
        $res = $this->page->db->query($sql_carteretour);
        //
        if (database::isError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        } else {
            //
            $message = $this->page->db->affectedRows()." "._("carte(s) en retour supprimee(s)");
            $this->LogToFile($message);
            $this->addToMessage($message);
        }
        //
        $this->LogToFile("end carteretour_epuration");
    }
}

?>

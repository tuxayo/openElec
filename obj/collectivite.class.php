<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class collectivite extends dbForm {

    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "collectivite";
    
    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "id";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "A";
    
    /**
     * Constructeur 
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function collectivite($id, &$db, $DEBUG) {
        //
        $this->constructeur($id, $db, $DEBUG);
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en vue de l'insertion des donnees dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setvalF($val) {
        //
        $this->valF['id'] = $val['id'];
        $this->valF['ville'] = $val['ville'];
        $this->valF['adresse'] = $val['adresse'];
        $this->valF['complement_adresse'] = $val['complement_adresse'];
        $this->valF['cp'] = $val['cp'];
        $this->valF['maire'] = $val['maire'];
        $this->valF['civilite'] = $val['civilite'];
        $this->valF['maire_de'] = $val['maire_de'];
        $this->valF['inseeville'] = $val['inseeville'];
        $this->valF['expediteurinsee'] = $val['expediteurinsee'];
        $this->valF['datetableau'] = $this->dateDB($val['datetableau']);
        $this->valF['dateelection'] = $this->dateDB($val['dateelection']);
        $this->valF['siret'] = $val['siret'];
        $this->valF['mail'] = $val['mail'];
        $this->valF['num_tel'] = $val['num_tel'];
        $this->valF['num_fax'] = $val['num_fax'];
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * d'effectuer des tests d'integrite sur la cle primaire pour verifier
     * si la cle a bien ete saisie dans le formulaire et si l'objet ajoute
     * n'existe pas deja dans la table pour en empecher l'ajout.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function verifierAjout($val, &$db) {
        // Si la cle primaire est vide alors on rejette l'enregistrement
        if (trim($this->valF[$this->clePrimaire]) == "") {
            $this->correct = false;
            $this->msg .= $this->form->lib[$this->clePrimaire];
            $this->msg .= _(" est obligatoire")."<br />";
        }
        // Si le test precedent a ete reussi alors on teste si l'identifiant
        // n'existe pas deja dans la table
        if ($this->correct == true) {
            // Construction de la requete
            $sql = "select count(*) from ".$this->table." ";
            if ($this->typeCle == "A") {
                $sql .= "where ".$this->clePrimaire."='".$val[$this->clePrimaire]."'";
            } else {
                $sql .= "where ".$this->clePrimaire."=".$val[$this->clePrimaire]."";
            }
            // Execution de la requete
            $nb = $db->getone($sql);
            if (database::isError($nb)) {
                // Appel de la methode qui gere les erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            } else {
                // Si le resultat de la requete renvoit un ou plusieurs
                // enregistrements
                if ($nb > 0) {
                    // On rejette l'enregistrement
                    $this->correct = false;
                    $this->msg .= _("Il existe deja un enregistrement avec cet identifiant, vous devez en choisir un autre")."<br />";
                }
            }
        }
    }
    
    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @return void
     */
    function verifier() {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Si la valeur est vide alors on rejette l'enregistrement
        $field_required = array("id", "ville", "maire", "datetableau", "cp", "siret", "num_tel", );
        foreach($field_required as $field) {
            if (trim($this->valF[$field]) == "") {
                $this->correct = false;
                $this->msg .= $this->form->lib[$field]." ";
                $this->msg .= _("est obligatoire")."<br />";
            }
        }
    }

    /**
     *
     */
    function cleSecondaire($id, &$db = NULL, $val = array(), $DEBUG = false) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        //
        if ($_SESSION['multi_collectivite'] == false || $_SESSION['collectivite'] == $id) {
            $this->correct = false;
            $this->msg .= _("Vous ne pouvez pas supprimer cette collectivite.")."<br/>";
        }
    }

    /**
     * Parametrage du formulaire - Groupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setGroupe(&$form, $maj) {
        //
        $form->setGroupe("civilite", "D");
        $form->setGroupe("maire_de", "F");
    }
    
    /**
     * Parametrage du formulaire - Reroupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setRegroupe(&$form, $maj) {
        //
        $form->setRegroupe("id", "D", _("Identification"));
        $form->setRegroupe("ville", "G", "");
        $form->setRegroupe("adresse", "G", "");
        $form->setRegroupe("complement_adresse", "G", "");
        $form->setRegroupe("cp", "F", "");

        $form->setRegroupe("mail", "D", _("Contact"));
        $form->setRegroupe("num_tel", "G", "");
        $form->setRegroupe("num_fax", "F", "");
        //
        $form->setRegroupe("maire", "D", _("Maire"));
        $form->setRegroupe("civilite", "G", "");
        $form->setRegroupe("maire_de", "F", "");
        //
        $form->setRegroupe("inseeville", "D", _("INSEE"));
        $form->setRegroupe("siret", "F", "");
        //
        $form->setRegroupe("datetableau", "D", _("Parametrage"));
        $form->setRegroupe("dateelection", "F", "");
    }
    
    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType(&$form, $maj) {
        //
        if ($maj < 2) { // ajouter et modifier
            if ($maj == 1) { // modifier
                $form->setType("id", "hiddenstatic");
                $form->setType("datetableau", "date");
                $form->setType("dateelection", "date");
            } else { // ajouter
                $form->setType("datetableau", "date");
                $form->setType("dateelection", "date");
            }
        } else { // supprimer
            $form->setType("id", "hiddenstatic");
        }
    }
    
    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        $form->setLib("id", _("Identifiant"));
        $form->setLib("ville", _("Nom de la ville"));
        $form->setLib("adresse", _("Adresse"));
        $form->setLib("complement_adresse", _("Complement / BP"));
        $form->setLib("cp", _("Code Postal"));
        $form->setLib("civilite", _("Civilite"));
        $form->setLib("maire", _("Nom du Maire"));
        $form->setLib("maire_de", _("Maire de"));
        $form->setLib("inseeville", _("Code INSEE"));
        $form->setLib("expediteurinsee", _("Code expediteur INSEE"));
        $form->setLib("datetableau", _("Date de tableau"));
        $form->setLib("dateelection", _("Date de l'election en cours"));
        $form->setLib("mail", _("Courriel"));
        $form->setLib("num_tel", _("Numero de telephone"));
        $form->setLib("num_fax", _("Numero de fax"));
        $form->setLib("siret", _("Code siret"));
    }
    
    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setOnchange(&$form, $maj) {
        //
        $form->setOnchange("datetableau", "fdate(this)");
        $form->setOnchange("dateelection", "fdate(this)");
    }
    
    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille(&$form, $maj) {
        //
        $form->setTaille("id", 3);
        $form->setTaille("ville", 30);
        $form->setTaille("maire", 40);
        $form->setTaille("inseeville", 5);
        $form->setTaille("expediteurinsee", 10);
        $form->setTaille("datetableau", 10);
        $form->setTaille("dateelection", 10);
        $form->setTaille("cp", 5);
        $form->setTaille("civilite", 15);
        $form->setTaille("adresse", 50);
        $form->setTaille("complement_adresse", 50);
        $form->setTaille("maire_de", 15);
        $form->setTaille("mail", 46);
    }
    
    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax(&$form, $maj) {
        //
        $form->setMax("id", 3);
        $form->setMax("ville", 30);
        $form->setMax("maire", 40);
        $form->setMax("inseeville", 5);
        $form->setMax("expediteurinsee", 10);
        $form->setMax("datetableau", 10);
        $form->setMax("dateelection", 10);
        $form->setMax("cp", 5);
        $form->setMax("civilite", 15);
        $form->setMax("adresse", 50);
        $form->setMax("complement_adresse", 50);
        $form->setMax("maire_de", 15);
        $form->setMax("siret", 14);
        $form->setMax("mail", 80);
        $form->setMax("num_tel", 10);
        $form->setMax("num_fax", 10);
    }
    
    /**
     * 
     */
    function triggersupprimer($id, &$db, $val, $DEBUG) {
        //
        $sql = "";
        // On recupere la liste des voies en lien avec la collectivite a
        // supprimer pour supprimer les enregistrements lies a ces voies dans
        // la table decoupage
        $sql_voies = "select code as clef ";
        $sql_voies .= "from voie where voie.code_collectivite='".$id."';";
        //
        $res_voies = $db->query($sql_voies);
        //
        if (database::isError($res_voies)) {
            // Appel de la methode qui gere les erreurs
            $this->erreur_db($res_voies->getDebugInfo(), $res_voies->getMessage(), "");
        } else {
            //
            $liste_voies = array();
            while ($row_voies =& $res_voies->fetchRow(DB_FETCHMODE_ASSOC)) {
                array_push($liste_voies, $row_voies);
            }
            //
            foreach($liste_voies as $c){
                $sql .= "DELETE FROM decoupage WHERE code_voie='".$c['clef']."'; ";
            }
        }
        // On recupere la liste des electeurs en lien avec la collectivite a
        // supprimer pour supprimer les enregistrements lies a ces electeurs
        // dans les tables centrevote, mairieeurope et procuration
        $sql_electeurs = "select id_electeur from electeur where collectivite='".$id."';";
        //
        $res_electeurs = $db->query($sql_electeurs);
        //
        if (database::isError($res_electeurs)) {
            // Appel de la methode qui gere les erreurs
            $this->erreur_db($res_electeurs->getDebugInfo(), $res_electeurs->getMessage(), "");
        } else {
            //
            $liste_electeurs = array();
            while ($row_electeurs =& $res_electeurs->fetchRow(DB_FETCHMODE_ASSOC)) {
                array_push($liste_electeurs, $row_electeurs);
            }
            //
            foreach($liste_electeurs as $c) {
                $sql .= "DELETE FROM centrevote WHERE id_electeur=".$c['id_electeur']."; ";
                $sql .= "DELETE FROM procuration WHERE (mandataire=".$c['id_electeur']." or mandant=".$c['id_electeur']."); ";
                $sql .= "DELETE FROM mairieeurope WHERE id_electeur_europe=".$c['id_electeur']."; ";
            }
        }
        //
        $table = array("archive", "inscription_office", "mouvement",
                       "numerobureau", "numeroliste", "bureau", "electeur");
        //
        $sql .= "delete from voie where code_collectivite='".$id."'; ";
        foreach ($table as $t) {
            $sql .= "delete from ".$t." where collectivite='".$id."'; ";
        }
        //
        $res = $db->query($sql);
        if (database::isError($res)) {
            // Appel de la methode qui gere les erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), "");
        }
    }
    
}

?>
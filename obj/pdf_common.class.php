<?php
/**
 * 
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 * 
 */
require_once "fpdf.php";

/**
 * 
 */
class PDF extends FPDF {

    /// {{{ COMMON

    /**
     *
     */
    var $header_method_generic_document_header = "header_generic_document_header";

    /**
     *
     */
    var $header_method_generic_document_title = "header_generic_document_title";

    /**
     *
     */
    var $header_method_specific_page_title = NULL;

    /**
     *
     */
    var $header_method_specific_page_subtitle = NULL;

    /**
     *
     */
    var $header_method_table_head = NULL;

    /**
     *
     */
    var $footer_method_generic_document_footer = "footer_generic_document_footer";

    /**
     *
     */
    var $params = "";

    /**
     *
     */
    var $generic_document_header_left = "";

    /**
     *
     */
    var $generic_document_title = "";

    /**
     *
     */
    function header_generic_document_header() {
        // 
        $this->SetFont('Arial', '', 8);
        //
        if ($this->generic_document_header_left != "") {
            //
            $this->Cell(0, 5, iconv(HTTPCHARSET,"CP1252",$this->generic_document_header_left), 0, 0, 'L', false);
        }
        //
        $this->Cell(0, 5, iconv(HTTPCHARSET,"CP1252",sprintf(_("Edition du %s a %s"),date("d/m/Y"),date("H:i"))), 0, 0, "R", false);
        //
        $this->Ln(5);
    }

    /**
     *
     */
    function header_generic_document_title() {
        //
        if ($this->generic_document_title != "") {
            //
            $this->SetFont('Arial', "B", 8);
            //
            $this->Cell(0, 5, iconv(HTTPCHARSET,"CP1252",$this->generic_document_title), 0, 0, "C", false);
            //
            $this->Ln(5);
        }
    }

    /**
     *
     */
    function Header() {
        //
        if ($this->header_method_generic_document_header != NULL && method_exists($this, $this->header_method_generic_document_header)) {
            $method = $this->header_method_generic_document_header;
            $this->$method();
        }
        //
        if ($this->header_method_generic_document_title != NULL && method_exists($this, $this->header_method_generic_document_title)) {
            $method = $this->header_method_generic_document_title;
            $this->$method();
        }
        //
        if ($this->header_method_specific_page_title != NULL && method_exists($this, $this->header_method_specific_page_title)) {
            $method = $this->header_method_specific_page_title;
            $this->$method();
        }
       //
        if ($this->header_method_specific_page_subtitle != NULL && method_exists($this, $this->header_method_specific_page_subtitle)) {
            $method = $this->header_method_specific_page_subtitle;
            $this->$method();
        }
       //
        if ($this->header_method_table_head != NULL && method_exists($this, $this->header_method_table_head)) {
            $method = $this->header_method_table_head;
            $this->$method();
        }
    }

    /**
     *
     */
    function footer_generic_document_footer() {
        //Positionnement à 1,5 cm du bas
        $this->SetY(-15);
        //Police Arial italique 8
        $this->SetFont('Arial', 'I', 8);
        //Numéro de page
        $this->Cell(0,5,iconv(HTTPCHARSET,"CP1252",sprintf(_('Page %s/{nb}'),$this->PageNo())), 0, 0,'C');
    }

    /**
     *
     */
    function Footer()  {
        //
        if ($this->footer_method_generic_document_footer != NULL && method_exists($this, $this->footer_method_generic_document_footer)) {
            $method = $this->footer_method_generic_document_footer;
            $this->$method();
        }
    }

    /// }}}

    /// {{{ PDFFROMDB ../app/pdf.php

    /**
     *
     */
    function header_specific_page_title_pdffromdb() {
        //
        $this->SetFont($GLOBALS['police'],$GLOBALS['grastitre'],$GLOBALS['sizetitre']);
        $this->SetTextColor($GLOBALS['C1titre'],$GLOBALS['C2titre'],$GLOBALS['C3titre']);
        $this->SetFillColor($GLOBALS['C1titrefond'],$GLOBALS['C2titrefond'],$GLOBALS['C3titrefond']);
        //
        if (isset($GLOBALS['flagsessionliste'])) {
            if ( $GLOBALS['flagsessionliste']==1) {
                $txt = $GLOBALS['libtitre']." ".$GLOBALS['nolibliste'];
            }else{
                $txt = $GLOBALS['libtitre'];
            }
        }else{
            $txt = $GLOBALS['libtitre'];
        }
        if ($this->params["flag_first_page"] == false) {
            $txt .= " (suite)";
        }
        $this->Cell(0,$GLOBALS['heightitre'],iconv(HTTPCHARSET,"CP1252",$txt),$GLOBALS['bordertitre'],1,$GLOBALS['aligntitre'],$GLOBALS['fondtitre']);
        //
        $this->ln(0);
        //
        if ($this->params["flag_first_page"] == true) {
            $this->params["flag_first_page"] = false;
        }
    }

    /**
     *
     */
    function header_table_head_pdffromdb() {
        //
        if ($this->params["flag_entete"] == 1) {
            //
            $this->SetFont($this->params["police"], (isset($GLOBALS["entete_style"]) ? $GLOBALS["entete_style"] : ""), (isset($GLOBALS["entete_size"]) ? $GLOBALS["entete_size"] : 8));
            //
            $this->SetFillColor($GLOBALS['C1fondentete'], $GLOBALS['C2fondentete'], $GLOBALS['C3fondentete']);
            //
            $this->SetTextColor($GLOBALS['C1entetetxt'], $GLOBALS['C2entetetxt'], $GLOBALS['C3entetetxt']);
            //
            for ($i = 0; $i < $this->params["nb_columns"]; $i++) {
                //
                $this->Cell($GLOBALS['l'.$i], $GLOBALS['heightentete'], iconv(HTTPCHARSET,"CP1252",strtoupper($this->params["info"][$i]['name'])), $GLOBALS['be'.$i], 0, $GLOBALS['ae'.$i], $GLOBALS['fondentete']);
            }
            // NEW LINE
            $this->ln();
        }
        //
        $this->SetFont($this->params["police"], '', $this->params["size"]);
        //
        $this->SetTextColor($GLOBALS['C1'], $GLOBALS['C2'], $GLOBALS['C3']);
    }

    /**
     *
     */
    function init_header_pdffromdb() {
        // Initialisation des methodes specifiques a appeler dans la methode Header
        $this->header_method_specific_page_title = "header_specific_page_title_pdffromdb";
        $this->header_method_table_head = "header_table_head_pdffromdb";
    }

    /**
     *
     */
    function reinit_header_pdffromdb() {
        // Re-initialisation des methodes specifiques a appeler dans la methode Header
        $this->header_method_specific_page_title = NULL;
        $this->header_method_table_head = NULL;
    }

    /**
     *
     */
    function init_pdffromdb($query, &$db, $height, $border, $align, $fond, $police, $size, $multiplicateur = NULL, $flag_entete) {
        //
        $this->params = array(
            "flag_first_page" => true,
            "police" => $police,
            "size" => $size,
            "flag_entete" => $flag_entete,
        );
        //
        $res = $db->query($query);
        //
        if (database::isError($res, true)) {
            //
            echo $res->getDebugInfo(). "- ".$res->getMessage();
            //
            die();
        } else {
            //
            $this->params["res"] = $res;
            //
            $this->params["info"] = $res->tableInfo();
            //
            $this->params["nb_columns"] = count($this->params["info"]);
        }
    }

    /**
     *
     */
    function Table($query, &$db, $height, $border, $align, $fond, $police, $size, $multiplicateur = NULL, $flag_entete) {

        //
        $nb_lines = 0;
        //
        $total = array();
        //
        $flagtot=0;
        $flagmoy=0;
        //
        while ($row=& $this->params["res"]->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if ($nb_lines % 2 == 0) {
                //
                $this->SetFillColor((isset($GLOBALS['C1fond1']) ? $GLOBALS['C1fond1'] : 255), (isset($GLOBALS['C2fond1']) ? $GLOBALS['C2fond1'] : 255), (isset($GLOBALS['C3fond1']) ? $GLOBALS['C3fond1'] : 255));
            } else {
                //
                $this->SetFillColor((isset($GLOBALS['C1fond2']) ? $GLOBALS['C1fond2'] : 241), (isset($GLOBALS['C2fond2']) ? $GLOBALS['C2fond2'] : 241), (isset($GLOBALS['C3fond2']) ? $GLOBALS['C3fond2'] : 241));
            }
            //
            for ($i = 0; $i < $this->params["nb_columns"]; $i++) {
                //
                if (isset($GLOBALS['chnd'.$i]) && trim($GLOBALS['chnd'.$i]) != "") {
                    //champs non numerique = 999 , numerique
                    if ($GLOBALS['chnd'.$i] == 999) {
                        //
                        $this->Cell($GLOBALS['l'.$i],$height,iconv(HTTPCHARSET,"CP1252",$row[$this->params["info"][$i]['name']]),$GLOBALS['b'.$i],0,$GLOBALS['a'.$i],$fond);
                    } else {
                        //calcul totaux
                        if (isset($GLOBALS['chtot'.$i]) && trim($GLOBALS['chtot'.$i]) != "") {
                            //
                            if ($GLOBALS['chtot'.$i] == 1 || $GLOBALS['chmoy'.$i] == 1) {
                                //
                                if (!isset($total[$i])) {
                                    //
                                    $total[$i] = 0;
                                }
                                //
                                $total[$i] = $total[$i] + $row[$this->params["info"][$i]['name']];
                                //
                                if ($GLOBALS['chtot'.$i] == 1) {
                                    //
                                    if ($flagtot == 0) {
                                        //
                                        $flagtot = 1;
                                    }
                                }
                                //
                                if ($GLOBALS['chmoy'.$i] == 1) {
                                    //
                                    if ($flagmoy == 0) {
                                        //
                                        $flagmoy=1;
                                    }
                                }
                            }
                        }
                        //
                        $this->Cell($GLOBALS['l'.$i],$height,iconv(HTTPCHARSET,"CP1252",number_format($row[$this->params["info"][$i]['name']]), $GLOBALS['chnd'.$i], ',', ' '),$GLOBALS['b'.$i],0,$GLOBALS['a'.$i],$fond);
                    }
                } else {
                    //
                    $this->Cell($GLOBALS['l'.$i], $height, iconv(HTTPCHARSET,"CP1252",$row[$this->params["info"][$i]['name']]),$GLOBALS['b'.$i],0,$GLOBALS['a'.$i],$fond);
                }
            }
            //
            $nb_lines++;
            // NEW LINE
            $this->ln();
        }
        //
        if ($flagtot == 1) {
            //
            for ($i = 0; $i < $this->params["nb_columns"]; $i++) {
                //
                $txt = "";
                //
                if ($GLOBALS['chtot'.$i] == 1 or $GLOBALS['chtot'.$i] == 2) {
                    //
                    $txt = number_format($total[$i], $GLOBALS['chnd'.$i], ',', ' ');
                } elseif ($i == 0) {
                    //
                    $txt = "TOTAL";
                }
                //
                $this->Cell($GLOBALS['l'.$i], $height, iconv(HTTPCHARSET,"CP1252",$txt), $border, 0, $GLOBALS['a'.$i], $fond);
            }
            // NEW LINE
            $this->ln();
        }
        //
        if (isset($GLOBALS["counter_flag"]) && $GLOBALS["counter_flag"] == true) {
            //
            $txt = (isset($GLOBALS["counter_prefix"]) ? $GLOBALS["counter_prefix"]." " : "");
            $txt .= $nb_lines;
            $txt .= (isset($GLOBALS["counter_suffix"]) ? " ".$GLOBALS["counter_suffix"] : "");
            //
            $this->Cell(0, $height, iconv(HTTPCHARSET,"CP1252",$txt));
            // NEW LINE
            $this->ln();
        }
        //
        if ($flagmoy == 1) {
            //
            for ($i = 0; $i < $this->params["nb_columns"]; $i++) {
                //
                $txt = "";
                //
                if ($GLOBALS['chmoy'.$i] == 1) {
                    //
                    $txt = number_format(($total[$i] / $nb_lines), $GLOBALS['chnd'.$i], ',', ' ');
                } elseif ($i == 0) {
                    //
                    $txt = 'MOYENNE';
                }
                //
                $this->Cell($GLOBALS['l'.$i], $height, iconv(HTTPCHARSET,"CP1252",$txt), $border, 0, $GLOBALS['a'.$i], $fond);
            }
        }

        $this->reinit_header_pdffromdb();
    }

    /// }}}

    /// {{{ PDFFROMARRAY ../pdf/pdffromarray.php

    /**
     *
     */
    function header_specific_page_title_pdffromarray() {
        //
        $this->SetFont('Arial', 'B', 11);
        //
        $this->SetTextColor(75, 79, 81);
        //
        $this->Cell(0, 10, iconv(HTTPCHARSET,"CP1252",$this->params['title']), 0, 1, 'L');
        //
        $this->SetTextColor(0, 0, 0);
    }

    /**
     *
     */
    function header_specific_page_subtitle_pdffromarray() {
        //
        if ($this->params['subtitle'] != "") {
            //
            $this->SetFont('Arial', '', 10);
            //
            $this->Cell(0, 8, iconv(HTTPCHARSET,"CP1252",$this->params['subtitle']), 0, 1, 'L');
        }
    }

    /**
     *
     */
    function header_table_head_pdffromarray() {
        //
        $this->SetFont('Arial', '', 8);
        // 
        $this->SetFillColor(180, 180, 180);
        //
        for ($i = 0; $i < $this->params["nb_columns"]; $i++) {
            //
            $larg = ($this->params["offset"][$i] <= 0 ? $this->params["auto_column_width"] : $this->params["offset"][$i]);
            //
            $this->CellFitScale($larg, 5, $this->params["column"][$i], 1, 0, 'C' ,1);
        }
        $this->Ln();
    }

    /**
     *
     */
    function init_header_pdffromarray() {
        // Initialisation des methodes specifiques a appeler dans la methode Header
        $this->header_method_specific_page_title = "header_specific_page_title_pdffromarray";
        $this->header_method_specific_page_subtitle = "header_specific_page_subtitle_pdffromarray";
        $this->header_method_table_head = "header_table_head_pdffromarray";
    }

    /**
     *
     */
    function reinit_header_pdffromarray() {
        // Re-initialisation des methodes specifiques a appeler dans la methode Header
        $this->header_method_specific_page_title = NULL;
        $this->header_method_specific_page_subtitle = NULL;
        $this->header_method_table_head = NULL;
    }

    /**
     *
     */
    function tableau($tableau) {

        //
        $this->params = $tableau;
        //
        (!isset($this->params["column"]) ? $this->params["column"] = array() : "");
        // nb_columns
        $this->params["nb_columns"] = count($this->params["column"]);
        // offset
        (!isset($this->params["offset"]) ? $this->params["offset"] = array_pad(array(), $this->params["nb_columns"], 0) : "");
        // align
        (!isset($this->params["align"]) ? $this->params["align"] = array_pad(array(), $this->params["nb_columns"], "L") : "");
        // orientation
        (!isset($this->params["format"]) ? $this->params["format"] = "P" : "");
        // title
        (!isset($this->params["title"]) ? $this->params["title"] = "" : "");
        // subtitle
        (!isset($this->params["subtitle"]) ? $this->params["subtitle"] = "" : "");
        // maximum_width
        ($this->params["format"] == "L" ? $this->params["maximum_width"] = 277 : $this->params["maximum_width"] = 190);
        //
        $data = $tableau['data'];
        // Nombre de colonne ne correspond pas
        if ($this->params["nb_columns"] != count($this->params["offset"])) {
            die ("Le nombre de colonnes dans offset ne correspond pas avec column.");
        }
        // Nombre de colonne ne correspond pas
        if ($this->params["nb_columns"] != count($this->params["align"])) {
            die ("Le nombre de colonnes dans align ne correspond pas avec column.");
        }
        if (isset($data[0])) {
            if ($this->params["nb_columns"] != count($data[0])) {
                die ("Le nombre de colonnes dans \$data ne correspond pas avec \$column.");
            }
        }




        $this->AliasNbPages();

        //
        $nb_auto_column_width = 0;
        //
        foreach ($this->params["offset"] as $elem) {
            if ($elem <= 0) {
                $nb_auto_column_width++;
            }
        }
        $this->params["auto_column_width"] = 0;
        if ($nb_auto_column_width > 0) {
            $this->params["auto_column_width"] = (($this->params["maximum_width"] - array_sum($this->params["offset"])) / $nb_auto_column_width);
        }

        $this->AddPage();

        // // $this->Ln(10);

        //
        $this->SetFont('Arial', '', 8);


        //
        $nb_lines = 0;
        //
        foreach ($data as $nameRow => $row) {
            //
            if ($nb_lines % 2 == 0) {
                //
                $this->SetFillColor((isset($GLOBALS['C1fond1']) ? $GLOBALS['C1fond1'] : 255), (isset($GLOBALS['C2fond1']) ? $GLOBALS['C2fond1'] : 255), (isset($GLOBALS['C3fond1']) ? $GLOBALS['C3fond1'] : 255));
            } else {
                //
                $this->SetFillColor((isset($GLOBALS['C1fond2']) ? $GLOBALS['C1fond2'] : 241), (isset($GLOBALS['C2fond2']) ? $GLOBALS['C2fond2'] : 241), (isset($GLOBALS['C3fond2']) ? $GLOBALS['C3fond2'] : 241));
            }
            //
            for ($i = 0; $i < $this->params["nb_columns"]; $i++) {
                //
                $larg = ($this->params["offset"][$i] <= 0 ? $this->params["auto_column_width"] : $this->params["offset"][$i]);
                //
                $this->Cell($larg, 5, iconv(HTTPCHARSET,"CP1252",$row[$i]), 'LBR', 0, $this->params["align"][$i], 1);
            }
            //
            $nb_lines++;
            // NEW LINE
            $this->ln();
        }
        //$this->myFooter();
        $aujourdhui = date('Ymd-His');
        if (!isset($_GET["output"]) || $_GET["output"] != "no") {
            if (isset($tableau['output']) && $tableau['output']!="") {
                if ($tableau['output'] != strtolower("null")) {
                    $this->Output($tableau['output']."-".$aujourdhui.".pdf","I");
                }
            } else {
                $this->Output("stats-".$aujourdhui.".pdf","I");
            }
        }
        //
        $this->reinit_header_pdffromarray();
    }

    /// }}}

    /// {{{ COMMON - Methodes supplementaires pour fpdf

    //Cell with horizontal scaling if text is too wide
    function CellFit($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='', $scale=false, $force=true)
    {
        //Get string width
        $str_width=$this->GetStringWidth($txt);

        //Calculate ratio to fit cell
        if($w==0)
            $w = $this->w-$this->rMargin-$this->x;
        if ($str_width != 0) {
            $ratio = ($w-$this->cMargin*2)/$str_width;
        } else {
            $ratio = 1;
        }

        $fit = ($ratio < 1 || ($ratio > 1 && $force));
        if ($fit)
        {
            if ($scale)
            {
                //Calculate horizontal scaling
                $horiz_scale=$ratio*100.0;
                //Set horizontal scaling
                $this->_out(sprintf('BT %.2F Tz ET',$horiz_scale));
            }
            else
            {
                //Calculate character spacing in points
                $char_space=($w-$this->cMargin*2-$str_width)/max($this->MBGetStringLength($txt)-1,1)*$this->k;
                //Set character spacing
                $this->_out(sprintf('BT %.2F Tc ET',$char_space));
            }
            //Override user alignment (since text will fill up cell)
            $align='';
        }

        //Pass on to Cell method
        $this->Cell($w,$h,iconv(HTTPCHARSET,"CP1252",$txt),$border,$ln,$align,$fill,$link);

        //Reset character spacing/horizontal scaling
        if ($fit)
            $this->_out('BT '.($scale ? '100 Tz' : '0 Tc').' ET');
    }

    //Cell with horizontal scaling only if necessary
    function CellFitScale($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        $this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,true,false);
    }

    //Cell with horizontal scaling always
    function CellFitScaleForce($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        $this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,true,true);
    }

    //Cell with character spacing only if necessary
    function CellFitSpace($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        $this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,false,false);
    }

    //Cell with character spacing always
    function CellFitSpaceForce($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        //Same as calling CellFit directly
        $this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,false,true);
    }

    //Patch to also work with CJK double-byte text
    function MBGetStringLength($s)
    {
        if($this->CurrentFont['type']=='Type0')
        {
            $len = 0;
            $nbbytes = strlen($s);
            for ($i = 0; $i < $nbbytes; $i++)
            {
                if (ord($s[$i])<128)
                    $len++;
                else
                {
                    $len++;
                    $i++;
                }
            }
            return $len;
        }
        else
            return strlen($s);
    }

    /// }}}

}

?>

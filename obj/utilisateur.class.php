<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class utilisateur extends dbForm {
    
    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "utilisateur";
    
    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "idutilisateur";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "N";
    
    /**
     * Constructeur 
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function utilisateur($id, &$db, $DEBUG) {
        //
        $this->constructeur($id, $db, $DEBUG);
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en vue de l'insertion des donnees dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setvalF($val) {
        //
        $this->valF['idutilisateur'] = $val['idutilisateur'];
        $this->valF['nom'] = $val['nom'];
        $this->valF['login'] = $val['login'];
        if ($val['pwd'] == "")
            $this->valF['pwd'] = "";
        elseif ($val['pwd'] == $this->val[array_search('pwd', $this->champs)])
            $this->valF['pwd'] = $this->val[array_search('pwd', $this->champs)];
        else
            $this->valF['pwd'] = md5 ($val['pwd']);
        $this->valF['email'] = $val['email'];
        $this->valF['profil'] = $val['profil'];
        $this->valF['listedefaut'] = $val['listedefaut'];
        $this->valF['collectivite'] = $val['collectivite'];
    } // setvalF ($val)
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en ajouter la cle primaire en vue de l'insertion des donnees
     * dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setValFAjout($val) {
        // Id automatique donc cette initialisation n'est pas necessaire
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * de recuperer l'element suivant dans la sequence pour les id automatiques.
     * 
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function setId(&$db) {
        //
        $this->valF[$this->clePrimaire] = $db->nextId($this->table);
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * d'effectuer des tests d'integrite sur la cle primaire pour verifier
     * si la cle a bien ete saisie dans le formulaire et si l'objet ajoute
     * n'existe pas deja dans la table pour en empecher l'ajout.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function verifierAjout($val, &$db) {
        // Si le login est vide alors on rejette l'enregistrement
        if (trim($this->valF["login"]) == "") {
            $this->correct = false;
            $this->msg .= $this->form->lib["login"];
            $this->msg .= _(" est obligatoire")."<br />";
        }
        // Si le test precedent a ete reussi alors on teste si le login
        // n'existe pas deja dans la table
        if ($this->correct == true) {
            // Construction de la requete
            $sql = "select count(*) from ".$this->table." ";
            $sql .= "where login='".$val["login"]."'";
            // Execution de la requete
            $nb = $db->getone($sql);
            if (database::isError($nb)) {
                // Appel de la methode qui gere les erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            } else {
                // Si le resultat de la requete renvoit un ou plusieurs
                // enregistrements
                if ($nb > 0) {
                    // On rejette l'enregistrement
                    $this->correct = false;
                    $this->msg .= _("Il existe deja un enregistrement avec cet identifiant, vous devez en choisir un autre")."<br />";
                }
            }
        }
    }
    
    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @return void
     */
    function verifier() {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Si la valeur est vide alors on rejette l'enregistrement
        $field_required = array("profil", "login", "nom", "listedefaut", "pwd");
        foreach($field_required as $field) {
            if (trim($this->valF[$field]) == "") {
                $this->correct = false;
                $this->msg .= $this->form->lib[$field]." ";
                $this->msg .= _("est obligatoire")."<br />";
            }
        }
        //
        if ($_SESSION['multi_collectivite'] == 0) {
            if (trim($this->valF["collectivite"]) != $_SESSION["collectivite"]) {
                $this->correct = false;
                $this->msg .= $this->form->lib["collectivite"]." ";
                $this->msg .= _("n'est pas correct. Operation illegale.")."<br />";
            }
        }
    }

    /**
     *
     */
    function cleSecondaire($id, &$db = NULL, $val = array(), $DEBUG = false) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        //
        $this->rechercheLogin($id, $db, $DEBUG);
    }

    /**
     * Recherche si le login a supprimer est identique au login de
     * l'utilisateur connecte
     *
     * @return void
     */
    function rechercheLogin($id, &$db, $DEBUG) {
        //
        $sql = "select * from utilisateur where idutilisateur='".$id."'";
        $res = $db->query($sql);
        if (database::isError($res)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), "");
        } else {
            //
            $row =& $res->fetchRow(DB_FETCHMODE_ASSOC);
            if ($row['login'] == $_SESSION ['login']) {
                $this->msg .= _("Vous ne pouvez pas supprimer votre utilisateur.")."<br/>";
                $this->correct = false;
            }
        }
    }
    
    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType(&$form, $maj) {
        //
        if ($maj < 2) { // ajouter et modifier
            $form->setType("profil", "select");
            $form->setType("idutilisateur", "hiddenstatic");
            $form->setType("listedefaut", "select");
            $form->setType("pwd", "password");
            if($_SESSION['multi_collectivite'] == 1)
                $form->setType('collectivite','select');
			else
				$form->setType('collectivite','hiddenstatic');
            if ($maj == 1) { // modifier
            } else { // ajouter
            }
        } else { // supprimer
            $form->setType("idutilisateur", "hiddenstatic");
            $form->setType("login", "hiddenstatic");
            $form->setType("pwd", "hidden");
        }
    }
    
    /**
     * Parametrage du formulaire - Select des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function setSelect(&$form, $maj, $db, $DEBUG) {
        //
        require "../sql/".$db->phptype."/".$this->table.".form.inc";
        // Select Profil - uniquement en ajout et modification
        if ($maj < 2) {
            $res = $db->query($sql_profil);
            if (database::isError($res)) {
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), "");
            } else {
                $contenu = array();
                $contenu[0][0] = "";
                $contenu[1][0] = _("Choisir un profil");
                $k = 1;
                while ($row =& $res->fetchRow()) {
                    $contenu[0][$k] = $row[0];
                    $contenu[1][$k] = $row[1];
                    $k++;
                }
                $res->free();
                $form->setSelect("profil", $contenu);
            }
        }
        // Select liste - uniquement en ajout et modification
        if ($maj < 2) {
            $res = $db->query($sql_liste);
            if (database::isError($res)) {
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), "");
            } else {
                $contenu = array();
                $contenu[0][0] = "";
                $contenu[1][0] = _("Choisir une liste par defaut");
                $k = 1;
                while ($row =& $res->fetchRow()) {
                    $contenu[0][$k] = $row[0];
                    $contenu[1][$k] = $row[1];
                    $k++;
                }
                $res->free();
                $form->setSelect("listedefaut", $contenu);
            }
        }
        // Select collectivite - uniquement en ajout et modification
        if ($maj < 2) {
            $res = $db->query($sql_collectivite);
            if (database::isError($res)) {
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), "");
            } else {
                $contenu = array();
                $contenu[0][0] = "";
                $contenu[1][0] = _("Choisir une collectivite");
                $k = 1;
                while ($row =& $res->fetchRow()) {
                    $contenu[0][$k] = $row[0];
                    $contenu[1][$k] = $row[1];
                    $k++;
                }
                $res->free();
                $form->setSelect("collectivite", $contenu);
            }
        }
    }
    
    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        $form->setLib("idutilisateur", _("Id de l'utilisateur"));
        $form->setLib("nom", _("Nom et/ou Prenom"));
        $form->setLib("login", _("Identifiant de connexion"));
        $form->setLib("email", _("Courriel"));
        $form->setLib("pwd", _("Mot de passe"));
        $form->setLib("profil", _("Profil (droits d'acces)"));
        $form->setLib("listedefaut", _("Liste par defaut"));
    }
    
    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille(&$form, $maj) {
        //
        $form->setTaille("idutilisateur", 8);
        $form->setTaille("profil", 2);
        $form->setTaille("nom", 30);
        $form->setTaille("login", 30);
        $form->setTaille("email", 30);
        $form->setTaille("pwd", 50);
        $form->setTaille("listedefaut", 6);
    }
    
    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax(&$form, $maj) {
        //
        $form->setMax("idutilisateur", 8);
        $form->setMax("profil", 2);
        $form->setMax("nom", 30);
        $form->setMax("login", 18);
        $form->setMax("email", 100);
        $form->setMax("pwd", 100);
        $form->setMax("listedefaut", 6);
    }

	function setVal(&$form,$maj,$validation,&$db,$DEBUG=null){
		if($validation==0 and $maj==0 and $_SESSION['multi_collectivite']==0) {
			$form->setVal('collectivite', $_SESSION['collectivite']);
		}// fin validation
	}// fin setVal
}

?>
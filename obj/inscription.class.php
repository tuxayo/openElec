<?php
/**
 * Ce fichier permet de definir la classe inscription
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "mouvement.class.php";

/**
 *
 */
class inscription extends mouvement {

    /**
     * Type de mouvement correspondant à la valeur de typecat dans la table
     * param_mouvement
     * @var string Type de mouvement
     */
    var $typeCat = "inscription";

    /**
     *
     */
    function inscription($id, &$db, $DEBUG = false) {
        //
        $this->constructeur($id, $db);
    }

    /**
     *
     */
    function setvalFAjout($val) {
        //
        $this->valF['id_electeur'] = 0;
        $this->valF['numero_bureau'] = 0;
        $this->valF['numero_electeur'] = 0;
    }

    /**
     *
     */
    function getDataSubmit() {
        //
        $datasubmit = parent::getDataSubmit();
        //
        if ($this->getParameter("maj") == 0) {
            $datasubmit .= "&amp;nom=".$this->getParameter("nom");
            $datasubmit .= "&amp;exact=".$this->getParameter("exact");
            $datasubmit .= "&amp;datenaissance=".$this->getParameter("datenaissance");
        }
        //
        return $datasubmit;
    }

    /**
     *
     */
    function triggerajouterapres($id, &$db, $val, $DEBUG) {
        //
        $this->setComputedVal();
    }

    /**
     *
     */
    function triggermodifierapres($id, &$db, $val, $DEBUG) {
        //
        $this->setComputedVal();
    }
    
    /**
     *
     */
    function bouton($maj) {
        //
        if (!$this->correct && $maj == 0) {
            echo "\t";
            echo "<a  class=\"om-prev-icon doublon-16\" href=\"javascript:doublon1()\"";
            echo " title=\""._("Recherche de doublon sur nom, prenom(s), nom usage, date de naissance")."\"> ";
            echo _("Verification des doublons");
            echo "</a><br/>\n";
        }
        //
        parent::bouton($maj);
    }

    /**
     *
     */
    function retour($premier = 0, $recherche = "", $tricol = "") {
        //
        if ($this->getParameter("maj") != 0 || ($this->getParameter("maj") == 0
            && $this->correct && ($this->getParameter("nom") == NULL
            || $this->getParameter("nom") == ""))) {
            //
            parent::retour();
        } else {
            //
            echo "\n<a class=\"retour\" ";
            echo "href=\"";
            //
            if (!$this->correct) {
                //
                echo "../app/inscription.doublon.php";
                echo "?";
                echo "action=cancel";
                echo "&amp;nom=".urlencode($this->getParameter("nom"));
                echo ($this->getParameter("exact")==true?"&amp;exact=".$this->getParameter("exact")."":"");
                echo "&amp;datenaissance=".urlencode($this->getParameter("datenaissance"));
            } else {
                //
                if ($this->getParameter("nom") != NULL && $this->getParameter("nom") != "") {
                    //
                    echo "../scr/tab.php";
                    echo "?";
                    echo "obj=".get_class($this);
                    echo "&amp;recherche=".$this->getParameter("nom");
                }
            }
            //
            echo "\"";
            echo ">";
            //
            echo _("Retour");
            //
            echo "</a>\n";
        }
    }

    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$db = NULL, $DEBUG = false) {
        // Si mode Ajout et premier affichage du formulaire
        if ($maj == 0 && $validation == 0) {
            // On pre-rempli le champ 'nom' avec la valeur passee en parametre
            if ($this->getParameter("nom") != "") {
                $form->setVal('nom', $this->getParameter("nom"));
            }
            // On pre-rempli le champ 'datenaissance' avec la valeur passee en parametre
            if ($this->getParameter("datenaissance") != "" and $this->getParameter("datenaissance") != NULL) {
                $form->setVal('date_naissance', $this->dateDB($this->getParameter("datenaissance")));
            }
            // On pre-reimpli quelques valeurs par defaut
            $form->setVal('civilite', 'M.');
            $form->setVal('situation', '');
            // Si nous sommes sur la liste generale on positionne la
            // nationalite francaise par defaut
            if ($_SESSION["liste"] == "01") {
                $form->setVal('code_nationalite', 'FRA');
            }
            $form->setVal('sexe', 'M');
            $form->setVal('bureauforce', 'Non');
            $form->setVal('liste', $_SESSION['liste']);
            $form->setVal('date_tableau', $this->dateTableau);
            $form->setVal('tableau', "annuel");
            $form->setVal('etat', "actif");
        }
    }

    /**
     *
     */
    function setValFNumeroBureau() {
        //
        $this->valF['numero_bureau'] = 0;
    }

}
?>
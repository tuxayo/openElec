<?php
/**
 *
 * @package openelec
 * @version SVN : $Id$
 */

class traitement extends utils {
    
    var $page = NULL;
    var $log = "";
    var $message = "";
    var $error = false;
    var $fichier = "traitement";
    var $champs = array();
    var $form_name = "";
    var $form_class = "";
    
    function __construct ($page = NULL) {
        $this->page = $page;
        if ($this->page == NULL) {
            $this->error = true;
        }
        $this->LogToFile ("start traitement par ".$_SESSION['login']);
        $this->LogToFile ("collectivite en cours: ".$_SESSION['libelle_collectivite']."[".$_SESSION['collectivite']."]");
        $this->LogToFile ("liste en cours: ".$_SESSION['libelle_liste']."[".$_SESSION['liste']."]");
        $this->setLogFile ();
    }
    
    function now () {
        return date("Y-m-d_G:i:s");
    }
    
    function LogToFile ($msg) {
        $this->log .= $this->now ()." - ".$msg."\n";
    }
    
    function addToMessage ($msg) {
        $this->message .= $msg."<br/>";
    }
    
    function setLogFile () {
        $this->logfile = $this->page->getParameter("tmpdir").date("Ymd-Gis")."-traitement_".$this->fichier."-".$_SESSION['collectivite']."-".$_SESSION['libelle_collectivite'].".txt";
    }
    
    function startTransaction ()  {
        $this->page->db->autoCommit (false);
    }
    
    function commitTransaction () {
        $this->page->db->commit ();
        $this->LogToFile ("commit");
    }
    
    function rollbackTransaction () {
        $this->page->db->rollback ();
        $this->LogToFile ("rollback");
    }
    
    
    // {{{ FORMULAIRE
    
    function displayForm() {
        
        //
        $this->execute(true);
        
        //
        echo "<form";
        echo " method=\"post\"";
        echo " id=\"traitement_".$this->fichier."_form\"";
        echo " onsubmit=\"trt_form_submit('traitement_".$this->fichier."_result', '".$this->fichier."', this);return false;\"";
        echo " class=\"trt_form".($this->form_class != "" ? " ".$this->form_class : "")."\"";
        echo " action=\"#\"";
        if ($this->form_name != "") {
            echo " name=\"".$this->form_name."\"";
        }
        echo ">\n";
        
        //
        $description = $this->getDescription();
        if ($description != "") {
            //
            echo "<div class=\"instructions\">\n";
            echo "\t";
            echo $this->getDescription();
            echo "\n";
            echo "</div>\n";
        }
        
        //
        $this->displayResultSection();
        
        //
        $this->displayFormSection();
        
        //
        echo "<div class=\"formControls\">\n";
        echo "\t<input type=\"submit\" name=\"traitement.".$this->fichier.".form.valid\" ";
        echo "value=\"".$this->getValidButtonValue()."\" ";
        echo "class=\"boutonFormulaire\" />\n";
        echo "</div>\n";
        
        //
        echo "</form>\n";
    }
    
    function displayFormSection($form_only = false) {
        //
        (defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
        require_once PATH_OPENMAIRIE."formulairedyn.class.php";
        //
        echo "<div id=\"traitement_".$this->fichier."_status\">";
        //
        if ($form_only == false) {
            $this->displayBeforeContentForm();
        }
        //
        $this->form = new formulaire(NULL, 0, 0, $this->champs);
        //
        $this->setContentForm();
        //
        $this->displayContentForm();
        //
        if ($form_only == false) {
            $this->displayAfterContentForm();
        }
        //
        echo "</div>\n";
    }
    
    function displayResultSection() {
        //
        echo "<div id=\"traitement_".$this->fichier."_result\">\n";
        echo "\t<!-- -->\n";
        echo "</div>\n";
    }
    
    function getValidButtonValue() {
        //
        return _("Confirmer le traitement");
    }
    
    function getDescription() {
        //
        return "";
    }

    //
    function setContentForm() {
        
    }

    function displayContentForm() {
        //
        if (count($this->champs) != 0) {
            $this->form->entete();
            $this->form->afficher($this->champs, 0, false, false);
            $this->form->enpied();
        }
    }
    
    //
    function displayBeforeContentForm() {
        
    }

    //
    function displayAfterContentForm() {
        
    }

    // }}}
    
    function execute($check_valid_button = false) {
        //
        if ($check_valid_button == false
            || ($check_valid_button == true
                && isset($_POST["traitement_".$this->fichier."_form_valid"]))) {
            //
            $this->startTransaction ();
            $this->treatment ();
            if ($this->error) {
                $this->rollbackTransaction ();
            } else {
                $this->commitTransaction ();
            }
            //
            $this->LogToFile ("fin traitement");
            $this->result ($this->page->tmp($this->logfile, "\n".$this->log."\n", true));
        }
    }
    
    function setParams ($params = array()) {
        $this->params = $params;
    }
    
    function result ($log = false) {
        //
        $message = "";
        if ($log == true) {
            $message .= " <a target=\"_blank\" ";
            $message .= "href=\"../app/file.php?fic=".$this->logfile."&amp;folder=tmp\" ";
            $message .= "class=\"trt_log_link\" ";
            $message .= "rel=\"".$this->fichier."\">";
            $message .= _("Voir le detail");
            $message .= "</a>";
        }
        //
        if ($this->error) {
            $this->page->displayMessage ("error", _("Le traitement a echoue.")." ".$message.($this->message != "" ? "<br/><br/>".$this->message : ""));
        } else {
            $this->page->displayMessage ("valid", _("Le traitement est termine.")." ".$message.($this->message != "" ? "<br/><br/>".$this->message : ""));
        }
    }
    
    function treatment () {
        print "Not implemented";
    }
    
    function __destruct() {
        //
    }
}

?>
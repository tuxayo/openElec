<?php
/**
 * Classe permettant l'ajout de membres aux commissions
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class membre_commission extends dbForm {
    
    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "membre_commission";
    
    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "membre_commission";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "N";
    
    /**
     * Constructeur 
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function membre_commission($id, &$db, $DEBUG) {
        //
        $this->constructeur($id, $db, $DEBUG);
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en vue de l'insertion des donnees dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setvalF($val) {
        //
        $this->valF['membre_commission'] = $val['membre_commission'];
        $this->valF['civilite'] = $val['civilite'];
        $this->valF['nom'] = $val['nom'];
        $this->valF['prenom'] = $val['prenom'];
        $this->valF['adresse'] = $val['adresse'];
        $this->valF['complement'] = $val['complement'];
        $this->valF['cp'] = $val['cp'];
        $this->valF['ville'] = $val['ville'];
        $this->valF['cedex'] = $val['cedex'];
        $this->valF['bp'] = $val['bp'];
        $this->valF['collectivite'] = $val['collectivite'];



    } // setvalF ($val)

    /**
     * Surcharge car clé automatique
     * @param  array  $val [description]
     * @param  [type] $db  [description]
     * @return [type]      [description]
     */
    function verifierAjout($val = array(), &$db = NULL) {

    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * de recuperer l'element suivant dans la sequence pour les id automatiques.
     * 
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function setId(&$db) {
        //
        $this->valF[$this->clePrimaire] = $db->nextId($this->table);
    }

    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @return void
     */
    function verifier() {

        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Si la valeur est vide alors on rejette l'enregistrement
        $field_required = array("nom","prenom","adresse","cp","ville");
        foreach($field_required as $field) {
            if (trim($this->valF[$field]) == "") {
                $this->correct = false;
                $this->msg .= $this->form->lib[$field]." ";
                $this->msg .= _("est obligatoire")."<br />";
            }
        }
        //
        if ($_SESSION['multi_collectivite'] == 0) {
            if (trim($this->valF["collectivite"]) != $_SESSION["collectivite"]) {
                $this->correct = false;
                $this->msg .= $this->form->lib["collectivite"]." ";
                $this->msg .= _("n'est pas correct. Operation illegale.")."<br />";
            }
        }
    }
    
    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType(&$form, $maj) {
        //
        if ($maj < 2) { // ajouter et modifier
            $form->setType("membre_commission", "hiddenstatic");
            $form->setType("civilite", "select");
            if($_SESSION['multi_collectivite'] == 1)
                $form->setType('collectivite','select');
			else
				$form->setType('collectivite','hiddenstatic');

        } else { // supprimer
            $form->setType("membre_commission", "hiddenstatic");
        }
    }
    
    /**
     * Parametrage du formulaire - Select des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function setSelect(&$form, $maj, $db, $DEBUG) {
        //
        require "../sql/".$db->phptype."/".$this->table.".form.inc";
        // Select civilite - uniquement en ajout et modification
        if ($maj < 2) {
            // civilite
            $contenu="";
            $contenu[0]=array('M.','Mme','Mlle');
            $contenu[1]=array(_('Monsieur'),_('Madame'),_('Mademoiselle'));
            $form->setSelect("civilite",$contenu);
        }
        // Select collectivite - uniquement en ajout et modification
        if ($maj < 2) {
            $res = $db->query($sql_collectivite);
            if (database::isError($res)) {
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), "");
            } else {
                $contenu = array();
                $contenu[0][0] = "";
                $contenu[1][0] = _("Choisir une collectivite");
                $k = 1;
                while ($row =& $res->fetchRow()) {
                    $contenu[0][$k] = $row[0];
                    $contenu[1][$k] = $row[1];
                    $k++;
                }
                $res->free();
                $form->setSelect("collectivite", $contenu);
            }
        }
    }
    
    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        $form->setLib("membre_commission", _("Identifiant"));
        $form->setLib("civilite", _("Civilite"));
        $form->setLib("nom", _("Nom"));
        $form->setLib("prenom", _("Prenom"));
        $form->setLib("adresse", _("Adresse"));
        $form->setLib("complement", _("Complement"));
        $form->setLib("cp", _("Code postal"));
        $form->setLib("ville", _("Ville"));
        $form->setLib("cedex", _("Cedex"));
        $form->setLib("bp", _("Bp"));
        $form->setLib("collectivite", _("Collectivite"));

    }
    
    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille(&$form, $maj) {
        //
        $form->setTaille("membre_commission", 8);
        $form->setTaille("civilite", 8);
        $form->setTaille("nom", 50);
        $form->setTaille("prenom", 50);
        $form->setTaille("adresse", 50);
        $form->setTaille("complement", 50);
        $form->setTaille("cp", 5);
        $form->setTaille("ville", 50);
        $form->setTaille("cedex", 20);
        $form->setTaille("bp", 20);
        $form->setTaille("collectivite", 8);

    }
    
    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax(&$form, $maj) {
        //
        $form->setMax("membre_commission", 8);
        $form->setMax("civilite", 8);
        $form->setMax("nom", 80);
        $form->setMax("prenom", 80);
        $form->setMax("adresse", 80);
        $form->setMax("complement", 80);
        $form->setMax("cp", 5);
        $form->setMax("ville", 80);
        $form->setMax("cedex", 20);
        $form->setMax("bp", 20);
        $form->setMax("collectivite", 8);
    }

	function setValSousFormulaire(&$form, $maj, $validation, $idxformulaire,
                        $retourformulaire, $typeformulaire, &$db, $DEBUG = NULL){
        //
        $this->retourformulaire = $retourformulaire;

		if($validation==0 AND $maj==0 AND
            $_SESSION['multi_collectivite']==0 AND
            $retourformulaire == "commission") {
			$form->setVal('collectivite', $_SESSION['collectivite']);
		}// fin validation
	}// fin setVal
}

?>
<?php
/**
 * Ce fichier declare la classe electionEpurationProcurationTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 * @todo Optimiser la methode de suppression : une requete suffit 
 */
class electionEpurationProcurationTraitement extends traitement {
    
    var $fichier = "election_epuration_procuration";
    
    var $champs = array("dateelection");
    
    function getValidButtonValue() {
        return _("Epuration des procurations");
    }
    
    function getDescription() {
        //
        return _("Saisir une date jusqu'a laquelle toutes les procurations ".
                 "vont etre supprimees. Cette suppression se fait sur toutes ".
                 "les listes.");
    }
    
    function setContentForm() {
        //
        $this->form->setLib("dateelection", _("Suppression des procurations actives dont la date de validite est anterieure ou egale au [date avant election en cours]"));
        $this->form->setType("dateelection", "date");
        $this->form->setTaille("dateelection", 10);
        $this->form->setMax("dateelection", 10);
        $this->form->setOnchange("dateelection", "fdate(this)");
    }
    
    function displayAfterContentForm() {
        //
        $links = array(
            "0" => array(
                "href" => "javascript:epuration_procuration_stats('procuration_aepurer');",
                "class" => "om-prev-icon statistiques-16",
                "title" => _("Statistiques details par bureau"),
            ),
        );
        //
        $this->page->displayLinksAsList($links);
    }
    
    function treatment () {
        //
        $this->LogToFile("start election_epuration_procuration");
        //
        $dateElection = date('d/m/Y');
        if (isset ($_POST['dateelection']))
            $dateElection = $_POST['dateelection'];
        if ($this->page->formatdate=="AAAA-MM-JJ") {
            $date = explode("/", $dateElection);
            // controle de date
            if (sizeof($date) == 3 and (checkdate($date[1],$date[0],$date[2]))) {
                $dateElectionR = $date[2]."-".$date[1]."-".$date[0];
            } else {
                //
                $this->error = true;
                //
                $msg = "La date ".$dateElection." n'est pas une date<br />";
                $this->LogToFile($msg);
                $this->addToMessage($msg);
            }
        }
        if ($this->page->formatdate=="JJ/MM/AAAA") {
            $date = explode("/", $dateElection);
            // controle de date
            if (sizeof($date) == 3 and checkdate($date[1],$date[0],$date[2])){
                $dateElectionR = $date[0]."/".$date[1]."/".$date[2];
            } else {
                //
                $this->error = true;
                //
                $msg = "La date ".$dateElection." n'est pas une date<br />";
                $this->LogToFile($msg);
                $this->addToMessage($msg);
            }
        }
        //
        if ($this->error == false) {
            //
            include "../sql/".$this->page -> phptype."/trt_mention.inc";
            //
            $msg = "Epuration prise en compte pour le ".$dateElection." : ";
            $this->LogToFile($msg);
            //
            $res = $this->page -> db -> query ($sqlE);
            //
            if (database::isError($res, true)) {
                //
                $this->error = true;
                //
                $message = $res->getMessage()." - ".$res->getUserInfo();
                $this->LogToFile($message);
                //
                $this->addToMessage(_("Contactez votre administrateur."));
            } else {
                $i=0;
                while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                    //
                    $sql= "delete from procuration where id_procuration=".$row['id_procuration'];
                    //
                    $res1=$this->page->db->query($sql);
                    //
                    if (database::isError($res1, true)) {
                        //
                        $this->error = true;
                        //
                        $message = $res1->getMessage()." - ".$res1->getUserInfo();
                        $this->LogToFile($message);
                        //
                        $this->addToMessage(_("Contactez votre administrateur."));
                        //
                        break;
                    } else {
                        $i++ ;
                        $msg .= $row['mandant']."=>".$row['mandataire']."\n";
                    }
                }
                //
                $msg = $i." "._("procuration(s) supprimee(s) dont la date de fin de validite etait le ou avant le")." ".$dateElection;
                $this->LogToFile($msg);
                $this->addToMessage($msg);
            }
        }

        //
        $this->LogToFile("end election_epuration_procuration");
    }
}

?>

<?php
/**
 * Ce fichier permet de definir la classe radiation_insee
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 * Appel de la classe dbformdyn
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 * 
 */
class radiation_insee extends dbForm {

    /**
     *
     */
    var $table="radiation_insee";
    
    /**
     *
     */
    var $clePrimaire= "id";
    
    /**
     *
     */
    var $typeCle= "N" ;

    /**
     * Constructeur
     */
    function radiation_insee($id,&$db,$DEBUG) {
        $this->constructeur($id,$db,$DEBUG);
    } // fin constructeur

    /**
     *
     */
    function setvalF($val) {
        // zones autorisées en modification sans mouvement
        // Etat civil
        $this->valF['nom'] = $val['nom'];
        $this->valF['sexe'] = $val['sexe'];
        $this->valF['prenom1'] = $val['prenom1'];
        $this->valF['prenom2'] = $val['prenom2'];
        $this->valF['prenom3'] = $val['prenom3']; 
        //  $this->valF['types'] = $val['types'];
        // naissance
        $this->valF['date_naissance'] = $val['date_naissance'];
        $this->valF['code_departement_naissance'] = $val['code_departement_naissance'];
        $this->valF['libelle_departement_naissance'] = $val['libelle_departement_naissance'];
        $this->valF['code_lieu_de_naissance'] = $val['code_lieu_de_naissance'];
        $this->valF['libelle_lieu_de_naissance'] = $val['libelle_lieu_de_naissance'];
        // adresse resident
        $this->valF['adresse1'] = $val['adresse1'];
        // mention et carte retournée
        $this->valF['idcnen'] = $val['idcnen'];
        //  $this->valF['type'] = $val['type'];
    }

    /**
     *
     */
    function setId(&$db){
        // id automatique nextid
        $this->valF['id'] = $db->nextId($this->table);
    }

    /**
     *
     */
    function verifierAjout($val, &$db){
        //
    }

    /**
     *
     */
    function verifier() {
        $this->correct=True;
        // zones obligatoires
        //nom
        if ($this->valF['nom']==""){
           $this->correct=false;
           $this->msg= $this->msg."<br>zone nom obligatoire";
        }
        // Controle date de naissance
        if ($this->valF['date_naissance']==""){
           $this->correct=false;
           $this->msg= $this->msg."<br>date de naissance obligatoire";
           $temp="";
        } else {
        $temp=$this->valF['date_naissance'];
        $temp1=date('d/m/Y');
        $this->valF['date_naissance']=$this->dateDB($this->valF['date_naissance']);
        }
        // controle electeur plus de 18 ans
        $age= $this->anneePHP($temp1)- $this->anneePHP($temp);
        if ($age<18){
           $this->correct=false;
           $this->msg= $this->msg."<br>l'electeur n' a pas  18 ans le ".
           $temp1;
        }
        //
        if ($this->valF['code_departement_naissance']==""){
           $this->correct=false;
           $this->msg= $this->msg."<br>code departement naissance obligatoire";
        }
        if ($this->valF['libelle_departement_naissance']==""){
           $this->correct=false;
           $this->msg= $this->msg."<br>libelle departement naissance obligatoire";
        }
        if ($this->valF['code_lieu_de_naissance']==""){
           $this->correct=false;
           $this->msg= $this->msg."<br>code lieu de naissance obligatoire";
        }
        if ($this->valF['libelle_lieu_de_naissance']==""){
           $this->correct=false;
           $this->msg= $this->msg."<br>libelle lieu de naissance obligatoire";
        }
        
        // verification departement / code lieu de naissance
        // prise en compte des DOM 971 TOM 981 et etranger 99352 ...
        // verification sur les 2 premiers caractères
        if (substr($this->valF['code_departement_naissance'],0,2)!=substr($this->valF['code_lieu_de_naissance'],0,2)){
          $this->correct=false;
           $this->msg= $this->msg."<br>lieu de naissance n'est pas dans le departement de naissance";
        }
    }

    function setType(&$form,$maj) {
        if ($maj < 2) { //ajouter et modifier
            // pas de modification affectation liste bureau et numero electeur et dans le bureau
            $form->setType('id','hiddenstatic');
            // selection et combo
            $form->setType('sexe','select');
            $form->setType('situation','select');
            $form->setType('date_naissance','date');
            $form->setType('code_departement_naissance','comboD');
            $form->setType('libelle_departement_naissance','comboG');
            $form->setType('code_lieu_de_naissance','comboD');
            $form->setType('libelle_lieu_de_naissance','comboG');
            $form->setType('prenom3','hidden');
            $form->setType('Adresse2','hidden');
        }
        else {
            $form->setType('id','hiddenstatic');
        }
    }

    /**
     *
     */
    function setLib (&$form, $maj) {
        $form->setLib('id',' Identifiant : ');
        $form->setLib('types',' type inscription : ');
        $form->setLib('sexe',' sexe : ');
        
        $form->setLib('code_departement_naissance','Departement : ');
        $form->setLib('code_lieu_de_naissance','Lieu Naissance : ');
        
        $form->setLib('nom',' Nom : ');
        
        $form->setLib('date_naissance','Date de naissance : ');
        
        $form->setLib('libelle_lieu_de_naissance','Libelle lieu de naissance');
        $form->setLib('libelle_departement_naissance','');
    } 

}
?>

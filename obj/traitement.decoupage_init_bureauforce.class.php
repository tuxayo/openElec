<?php
/**
 * Ce fichier declare la classe decoupageInitBureauforceTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class decoupageInitBureauforceTraitement extends traitement {
    
    var $fichier = "decoupage_init_bureauforce";
    
    function getDescription() {
        //
        return _("Tous les electeurs etant marques avec un bureau force et ".
                 "etant affectes au bureau correspondant a leur adresse dans ".
                 "le découpage ne seront plus marques en bureau force pour ".
                 "qu'un redecoupage automatique soit possible. Ce traitement ".
                 "se fait uniquement sur la liste en cours.");
    }
    
    function getValidButtonValue() {
        //
        return _("Initialiser les electeurs en bureau force");
    }
    
    function treatment () {
        //
        $this->LogToFile("start decoupage_init_bureauforce");
        //
        include "../sql/".$this->page->phptype."/trt_decoupage_init_bureauforce.inc";
        //
        $res = $this->page->db->query($sql_decoupage_init_bureauforce_electeur);
        //
        if (database::isError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        } else {
            //
            $message = $this->page->db->affectedRows()." "._("electeur(s) modifie(s)");
            $this->LogToFile($message);
            $this->addToMessage($message);
        }
        //
        $res = $this->page->db->query($sql_decoupage_init_bureauforce_mouvement);
        //
        if (database::isError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        } else {
            //
            $message = $this->page->db->affectedRows()." "._("mouvement(s) modifie(s)");
            $this->LogToFile($message);
            $this->addToMessage($message);
        }
        //
        $this->LogToFile("end decoupage_init_bureauforce");
    }
}

?>

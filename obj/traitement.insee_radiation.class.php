<?php
/**
 * Ce fichier declare la classe inseeRadiationTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class inseeRadiationTraitement extends traitement {
    
    var $fichier = "insee_radiation";

    var $champs = array("fic1");
    var $form_name = "f1";
    
    function getValidButtonValue() {
        //
        return _("Import des radiations INSEE");
    }
    
    function setContentForm() {
        $this->form->setLib("fic1", _("Fichier recu de l'INSEE a importer"));
        $this->form->setType("fic1", "upload");
    }

    function treatment () {
        //
        $this->LogToFile("start insee_radiation");
        //
        (isset($_POST['fic1']) ? $fic = $this->page->getParameter("trsdir").$_POST['fic1'] : $fic = "");
        //
        if ($_POST['fic1'] == "" || !file_exists($fic)) {
            //
            $this->error = true;
            //
            $message = _("Le fichier n'est pas valide.");
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Vous devez selectionner un fichier valide."));
        }
        //
        if ($this->error == false) {
            //
            $ext = pathinfo($_POST['fic1'], PATHINFO_EXTENSION);
            //
            if (strtolower($ext) != "txt" && strtolower($ext) != "xml") {
                //
                $this->error = true;
                //
                $message = _("L'extension du fichier n'est pas valide.");
                $this->LogToFile($message);
                //
                $this->addToMessage(_("Le format du fichier n'est pas correct."));
            }
            //
            if ($this->error == false) {
                //
                $this->LogToFile(_("Fichier :")." ".$fic);
                //
                $this->LogToFile(_("Extension :")." ".$ext);
                //
                switch ($ext) {
                    /**
                     * IMPORT XML
                     */
                    case "xml":
                    //
                    require_once "../obj/insee_import.class.php";
                    //
                    $importobj = new insee_import();
                    $importobj->import("radiation_insee", $fic, $this->page);
                    //
                    break;
                    /**
                     * IMPORT TXT
                     */
                    case "txt":
                    // Compteur
                    $total_lines = 0;
                    $empty_lines = 0;
                    // Récupération des listes paramétrées
                    $sqlListes = "select liste_insee from ".DB_PREFIXE."liste
                        WHERE liste = '".$_SESSION['liste']."'";
                    $inseeListe = $this->page->db->getOne($sqlListes);

                    //
                    $fichier = fopen($fic, "r");
                    //
                    while (!feof($fichier)) {
                        // Compteur
                        $total_lines++;
                        // Recuperation du contenu de la ligne
                        $contenu = fgets($fichier, 4096);
                        // Si la ligne n'est pas vide
                        if ($contenu != "") {
                            // Initialisation de la collectivite
                            $valF["collectivite"] = $_SESSION['collectivite'];
                            //
                            $valF['nom'] = "";
                            $valF['prenom1'] = "";
                            $valF['prenom2'] = "";
                            $valF['prenom3'] = "";
                            $valF['sexe'] = "M";
                            $valF['nationalite'] = "FRA";
                            $valF['localite_deces'] = "";
                            $valF['motif_de_radiation'] = "";
                            $valF['motif_nouvelle_inscription'] = "";
                            $valF['date_nouvelle_inscription'] = NULL;
                            $valF['code_lieu_de_deces'] = "0";
                            $valF['libelle_lieu_de_deces'] = "";
                            $valF['pays_deces'] = "";
                            $valF['date_deces'] = NULL;
                            $valF['pays_naissance'] = "";
                            $valF['type_de_liste'] = $inseeListe;
                            $valF['types'] = "";
                            $valF['code_perte_de_nationalite'] = "";
                            $valF['adresse1'] = "";
                            $valF['adresse2'] = "";
                            //
                            $decompose = preg_split('/\*/',trim(substr($contenu,20,66)));
                            $valF['nom'] = $decompose[0];
                            //
                            $decompose = preg_split('/\//',$decompose[1]);
                            $listenom = preg_split('/ /',$decompose[0]);
                            if (isset($listenom[0])) {
                                $valF['prenom1'] = $listenom[0];
                            }
                            if (isset($listenom[1])) {
                                $valF['prenom2'] = $listenom[1];
                            }
                            if (isset($listenom[2])) {
                                $valF['prenom3'] = $listenom[2];
                            }
                            //
                            $val = trim(substr($contenu,88,1));
                            if ($val=="2") {
                                $valF['sexe'] = "F";
                            }
                            //
                            $val = trim(substr($contenu,89,8));
                            $valF['date_naissance'] = substr($val,4,4)."-".substr($val,2,2)."-".substr($val,0,2);
                            if (!checkdate(substr($val,2,2), substr($val,0,2), substr($val,4,4))) {
                                //
                                $this->error = true;
                                //
                                $message = _("Ligne :")." ".$total_lines." - ".print_r($valF, true);
                                $this->LogToFile($message);
                                //
                                $message = _("ERREUR : La date de naissance n'est pas valide.");
                                $message .= " ".substr($val,0,2)."/".substr($val,2,2)."/".substr($val,4,4);
                                $message .= " n'est pas valide. Ligne ".$total_lines;
                                $this->LogToFile($message);
                                //
                                $this->addToMessage(_("Le fichier n'est pas correct."));
                                //
                                $this->addToMessage(_("La date de naissance")." ".substr($val,0,2)."/".substr($val,2,2)."/".substr($val,4,4)
                                                    ." "._("de l'electeur")." ".$valF['nom']." "._("de la ligne")." ".$total_lines
                                                    ." "._("n'est pas valide."));
                                //
                                break;
                            }
                            //
                            $val = trim(substr($contenu,97,2));
                            $valF['code_lieu_de_naissance'] = $val;
                            //
                            $sql = "select libelle_departement from departement where code='".$valF['code_lieu_de_naissance']."'";
                            $libelle_departement = $this->page->db->getOne($sql);
                            //
                            if (database::isError($libelle_departement, true)) {
                                //
                                $this->error = true;
                                //
                                $message = $libelle_departement->getMessage()." - ".$libelle_departement->getUserInfo();
                                $this->LogToFile($message);
                                //
                                $this->addToMessage(_("Contactez votre administrateur."));
                                //
                                break;
                            }
                            //
                            $valF['libelle_lieu_de_naissance'] = $libelle_departement;
                            //
                            $valF['pays_naissance'] = "";
                            //
                            $val = trim(substr($contenu,102,30));
                            $valF['localite_naissance'] = $val;
                            //
                            $valF['adresse1'] = "";
                            $valF['adresse2'] = trim(substr($contenu,132,5))." ".trim(substr($contenu,137,30));
                            //
                            $valF['motif_de_radiation'] = trim(substr($contenu, 167, 1));
                            //
                            switch ($valF['motif_de_radiation']) {
                                case "1":
                                    //
                                    $valF['localite_deces'] = trim(substr($contenu,180,30));
                                    $valF['code_lieu_de_deces'] = trim(substr($contenu,169,2));
                                    //
                                    $sql = "select libelle_departement from departement where code='".$valF['code_lieu_de_deces']."'";
                                    $libelle_departement = $this->page->db->getOne($sql);
                                    //
                                    if (database::isError($libelle_departement, true)) {
                                        //
                                        $this->error = true;
                                        //
                                        $message = $libelle_departement->getMessage()." - ".$libelle_departement->getUserInfo();
                                        $this->LogToFile($message);
                                        //
                                        $this->addToMessage(_("Contactez votre administrateur."));
                                        //
                                        break;
                                    }
                                    $valF['libelle_lieu_de_deces'] = $libelle_departement;
                                    //
                                    $valF['pays_deces'] = "";
                                    //
                                    $val = trim(substr($contenu,174,6));
                                    $valF['date_deces'] = "20".substr($val,4,2)."-".substr($val,2,2)."-".substr($val,0,2);
                                    //
                                    break;
                            }
                            //
                            $message = _("Ligne :")." ".$total_lines." - ".print_r($valF, true);
                            $this->LogToFile($message);
                            //
                            $valF['id'] = $this->page->db->nextId("radiation_insee");
                            //
                            $res = $this->page->db->autoExecute("radiation_insee", $valF, DB_AUTOQUERY_INSERT);
                            //
                            if (database::isError($res, true)) {
                                //
                                $this->error = true;
                                //
                                $message = $res->getMessage()." - ".$res->getUserInfo();
                                $this->LogToFile($message);
                                //
                                $this->addToMessage(_("Contactez votre administrateur."));
                                //
                                break;
                            }
                        } else {
                            //
                            $message = _("Ligne :")." ".$total_lines." - "._("VIDE");
                            $this->LogToFile($message);
                            //
                            $empty_lines++;
                        }
                    }
                    // Fermeture fichier
                    fclose($fichier);
                    //
                    if ($this->error == false) {
                        $this->addToMessage(($total_lines - $empty_lines)." "._("radiations INSEE importees"));
                    }
                    //
                    break;
                }
            }
        }
        //
        $this->LogToFile ("end insee_radiation");
    }
}

?>

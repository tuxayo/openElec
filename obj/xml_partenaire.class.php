<?php
/**
 * Ce fichier permet de definir la classe xml_partenaire
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";;

/**
 *
 */
class xml_partenaire extends dbForm {
    
    /**
     *
     */
    var $table="xml_partenaire";

    /**
     *
     */
    var $clePrimaire= "xml_partenaire";
    
    /**
     *
     */
    var $typeCle= "N" ;

    /**
     *
     */
    function xml_partenaire($id,&$db,$DEBUG) {
        $this->constructeur($id,$db,$DEBUG);
    }

    /**
     *
     */
    function setvalF($val) {
        $this->valF['type_partenaire'] = $val['type_partenaire'];
        $this->valF['identifiant_autorite'] = $val['identifiant_autorite'];
        $this->valF['identifiant'] = $val['identifiant'];
        $this->valF['nom'] = $val['nom'];
        $this->valF['type'] = $val['type'];
        $this->valF['role'] = $val['role'];
        $this->valF['contact_nom'] = $val['contact_nom'];
        $this->valF['contact_mail'] = $val['contact_mail'];
        $this->valF['contact_numerofax'] = $val['contact_numerofax'];
        $this->valF['contact_numerotelephone'] = $val['contact_numerotelephone'];
        $this->valF['contact_adresse_numerovoie'] = $val['contact_adresse_numerovoie'];
        $this->valF['contact_adresse_typevoie'] = $val['contact_adresse_typevoie'];
        $this->valF['contact_adresse_nomvoie'] = $val['contact_adresse_nomvoie'];
        $this->valF['contact_adresse_cedex'] = $val['contact_adresse_cedex'];
        $this->valF['contact_adresse_libellebureaucedex'] = $val['contact_adresse_libellebureaucedex'];
        $this->valF['contact_adresse_divisionterritoriale'] = $val['contact_adresse_divisionterritoriale'];
        $this->valF['service_nom'] = $val['service_nom'];
        $this->valF['service_fournisseur'] = $val['service_fournisseur'];
        $this->valF['service_version'] = $val['service_version'];
        $this->valF['service_accreditation'] = $val['service_accreditation'];
        $this->valF["collectivite"] = $_SESSION["collectivite"];
    }

    /**
     *
     */
    function setValFAjout($val) {
        // pas d initialisation valF pour la cle primaire
    }

    /**
     *
     */
    function setId(&$db) {
        // id automatique nextid
        $this->valF['xml_partenaire'] = $db->nextId($this->table);
    }

    /**
     *
     */
    function verifierAjout ($val, &$db) {
    }

    /**
     *
     */
    function verifier($val, &$db, $DEBUG) {
        $this->correct=True;
    }//verifier

    /**
     *
     */
    function setVal(&$form, $maj, $validation) {
        $form->setVal('collectivite', $_SESSION['collectivite']);
    }

    /**
     *
     */
    function setLib(&$form, $maj) {
        $form->setLib('xml_partenaire', _('Id partenaire'));
        $form->setLib('type_partenaire', _('Type partenaire'));
        $form->setLib('siret', _('SIRET'));
        $form->setLib('nom', _('Nom'));
        $form->setLib('contact_nom', _('Nom'));
        $form->setLib('contact_mail', _('Adresse mail'));
        $form->setLib('contact_numerofax', _('Numero Fax'));
        $form->setLib('contact_numerotelephone', _('Numero Telephone'));
        $form->setLib('contact_adresse_numerovoie', _('Numero de la voie'));
        $form->setLib('contact_adresse_typevoie',_('Type de voie'));
        $form->setLib('contact_adresse_nomvoie',_('Nom de la voie'));
        $form->setLib('contact_adresse_cedex',_('Cedex'));
        $form->setLib('contact_adresse_libellebureaucedex',_('Libelle cedex'));
        $form->setLib('contact_adresse_divisionterritoriale',_('Division territoriale'));
        $form->setLib('service_fournisseur',_('Fournisseur'));
        $form->setLib('service_accreditation',_('Accreditation'));
    }

    /**
     *
     */
    function setType(&$form, $maj) {
        $form->setType('collectivite','hidden');
        $form->setType('xml_partenaire','hiddenstatic');
        if ($maj < 2) {
            //ajouter et modifier
            $form->setType('type_partenaire','select');
            if ($maj==1) {
                //modifier
            } else {
                // ajouter
            }
        } else {
            // supprimer
        }
    }

    /**
     *
     */
    function setSelect(&$form, $maj,$db,$DEBUG) {
        if($maj<2) {
            $contenu="";
            $contenu[0]=array('origine','destination');
            $contenu[1]=array('Origine','Destination');
            $form->setSelect('type_partenaire',$contenu);
        }
    }

    /**
     *
     */
    function setGroupe(&$form, $maj) {
       /* $form->setGroupe('debut_validite','D');
        $form->setGroupe('fin_validite','F');
        $form->setGroupe('date_accord','D');
        $form->setGroupe('heure_accord','F');
        $form->setGroupe('mandant','D');
        $form->setGroupe('z_mandant','F');
        $form->setGroupe('mandataire','D');
        $form->setGroupe('z_mandataire','F');
        $form->setGroupe('date_modif','D');
        $form->setGroupe('utilisateur','F');*/
    }

    /**
     *
     */
    function setTaille(&$form, $maj) {
        $form->setTaille('type_partenaire', 40);
        $form->setTaille('siret', 40);
        $form->setTaille('nom', 40);
        $form->setTaille('contact_nom', 40);
        $form->setTaille('contact_mail', 40);
        $form->setTaille('contact_numerofax', 20);
        $form->setTaille('contact_numerotelephone', 20);
        $form->setTaille('contact_adresse_numerovoie', 10);
        $form->setTaille('contact_adresse_typevoie', 40);
        $form->setTaille('contact_adresse_nomvoie', 40);
        $form->setTaille('contact_adresse_libellebureaucedex', 40);
        $form->setTaille('contact_adresse_cedex', 10);
        $form->setTaille('contact_adresse_divisionterritoriale', 40);
        $form->setTaille('service_fournisseur', 40);
        $form->setTaille('service_accreditation', 40);
        $form->setTaille('xml_partenaire', 40);
    }

    /**
     *
     */
    function setMax(&$form, $maj) {
        $form->setMax('type_partenaire', 50);
        $form->setMax('siret', 50);
        $form->setMax('nom', 50);
        $form->setMax('contact_nom', 50);
        $form->setMax('contact_mail', 50);
        $form->setMax('contact_numerofax', 15);
        $form->setMax('contact_numerotelephone', 15);
        $form->setMax('contact_adresse_numerovoie', 6);
        $form->setMax('contact_adresse_typevoie', 50);
        $form->setMax('contact_adresse_nomvoie', 50);
        $form->setMax('contact_adresse_cedex', 15);
        $form->setMax('contact_adresse_libellebureaucedex', 50);
        $form->setMax('contact_adresse_divisionterritoriale', 50);
        $form->setMax('service_fournisseur', 50);
        $form->setMax('service_accreditation', 50);
        $form->setMax('xml_partenaire', 50);
    }

    /**
     *
     */
    function setRegroupe(&$form,$maj) {
        
        $form->setRegroupe('xml_partenaire','D',_('Partenaire'));
        $form->setRegroupe('type_partenaire','G','');
        $form->setRegroupe('siret','G','');
        $form->setRegroupe('nom','F','');
        
        $form->setRegroupe('contact_nom','D',_('Contact'));
        $form->setRegroupe('contact_mail','G','');
        $form->setRegroupe('contact_numerofax','G','');
        $form->setRegroupe('contact_numerotelephone','F','');
        
        $form->setRegroupe('contact_adresse_numerovoie','D',_('Adresse du contact'));
        $form->setRegroupe('contact_adresse_typevoie','G','');
        $form->setRegroupe('contact_adresse_nomvoie','G','');
        $form->setRegroupe('contact_adresse_cedex','G','');
        $form->setRegroupe('contact_adresse_libellebureaucedex','G','');
        $form->setRegroupe('contact_adresse_divisionterritoriale','F','');

        $form->setRegroupe('service_fournisseur','D',_('Service'));
        $form->setRegroupe('service_accreditation','F','');

    }
    
}
?>
<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
class revisionelectorale {

    /**
     *
     */
    var $f = NULL;

    /**
     *
     */
    var $revisions = null;

    /**
     *
     */
    var $revision = null;

    /**
     *
     */
    function __construct($f = null, $mode = null, $param = null) {

        /**
         *
         */
        //
        if ($f == null) {
            die(_("Parametres incorrects."));
        } else {
            $this->f = $f;
        }

        /**
         *
         */
        //
        $revisions = $this->get_all_revisions();
        //
        if (count($revisions) == 0) {
            //
            $this->f->displayMessage(
                "error", 
                _("Aucune révision paramétrée. Contactez votre administrateur.")
            );
            die();
        }

        /**
         *
         */
        //
        $revision = $this->get_current_revision($mode, $param);
    }

    /**
     *
     */
    function get_current_revision($mode = null, $param = null) {

        //
        if ($this->revision != null) {
            return $this->revision;
        }

        //
        $revisions = $this->get_all_revisions();
        //
        if (count($revisions) == 0) {
            //
            return null;
        }

        //
        if ($mode == "fromid") {
            //
            foreach ($revisions as $key => $value) {
                if ($key == $param) {
                    $this->revision = $value;
                    return $this->revision;
                }
            }
            //
            return null;
        }

        if ($mode == "fromdatetableau") {
            //
            foreach ($revisions as $key => $value) {
                if ($value["date_tr1"] == $param
                    || $value["date_tr2"] == $param) {
                    $this->revision = $value;
                    return $this->revision;
                }
            }
            //
            return null;
        }

        //
        foreach ($revisions as $key => $value) {
            if ($value["date_tr1"] == $this->f->collectivite['datetableau']
                || $value["date_tr2"] == $this->f->collectivite['datetableau']) {
                $this->revision = $value;
                return $this->revision;
            }
        }

        //
        $revisions_ids = array_keys($revisions);
        $this->revision = $revisions[$revisions_ids[0]];
        return $this->revision;
    }

    /**
     *
     */
    function get_all_revisions() {

        //
        if ($this->revisions != null) {
            return $this->revisions;
        }

        // Cette requete permet de lister toutes les révisions présentes dans 
        // le paramétrage.
        $query = " select * ";
        $query .= " from ".DB_PREFIXE."revision ";
        $query .= " order by revision.date_effet DESC ";
        // Liste des tableaux des cinq jours deja traites
        $res = $this->f->db->query($query);
        // Logger
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query."\")", VERBOSE_MODE);
        // Gestion des erreurs
        $this->f->isDatabaseError($res);
        //
        $this->revisions = array();
        //
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            $this->revisions[$row["id"]] = $row;
        }
        //
        $this->f->addToLog(
            __METHOD__."(): ".print_r($this->revisions, true),
            EXTRA_VERBOSE_MODE
        );
        //
        return $this->revisions;
    }

    /** 
     *
     */
    function get_date($date_field, $format = null) {
        //
        $revision = $this->get_current_revision();
        //
        $date = new DateTime($revision["date_".$date_field]);
        //
        if ($format == "day") {
            return $date->format('d');
        } elseif($format == "month") {
            return $date->format('m');
        } elseif($format == "year") {
            return $date->format('Y');
        } elseif($format != null) {
            return $date->format($format);
        } else {
            return $date->format("Y-m-d");
        }
    }

    /**
     *
     */
    function is_previous_exists() {
        //
        $revision = $this->get_current_revision();
        //
        $revisions = $this->get_all_revisions();
        //
        $current_revision_id = $revision["id"];
        $revisions_ids = array_keys($revisions);
        //
        $key = array_search($current_revision_id, $revisions_ids);
        if ($key < count($revisions_ids) - 1) {
            return true;
        }
        //
        return false;
    }

    /**
     *
     */
    function is_next_exists() {
        //
        $revision = $this->get_current_revision();
        //
        $revisions = $this->get_all_revisions();
        //
        $current_revision_id = $revision["id"];
        $revisions_ids = array_keys($revisions);
        //
        $key = array_search($current_revision_id, $revisions_ids);
        if ($key > 0) {
            return true;
        }
        //
        return false;
    }

    function get_previous_id() {
        //
        $revision = $this->get_current_revision();
        //
        $revisions = $this->get_all_revisions();
        //
        $current_revision_id = $revision["id"];
        $revisions_ids = array_keys($revisions);
        //
        $key = array_search($current_revision_id, $revisions_ids);
        return $revisions_ids[$key+1];
    }

    /**
     *
     */
    function get_next_id() {
        //
        $revision = $this->get_current_revision();
        //
        $revisions = $this->get_all_revisions();
        //
        $current_revision_id = $revision["id"];
        $revisions_ids = array_keys($revisions);
        //
        $key = array_search($current_revision_id, $revisions_ids);
        return $revisions_ids[$key-1];
    }

    /**
     *
     */
    function get_title() {
        //
        $revision = $this->get_current_revision();
        //
        return $revision["libelle"];
    }

    /**
     *
     */
    function get_previous_date_tableau($datetableau) {
        //
        $revisions = $this->get_all_revisions();
        //
        foreach ($revisions as $key => $value) {
            if ($value["date_tr1"] == $datetableau
                || $value["date_tr2"] == $datetableau) {
                $revision = $value;
                break;
            }
        }
        //
        if ($revision["date_tr2"] == $datetableau) {
            return $revision["date_tr1"];
        } else {
            $debut = $revision["date_debut"];
            $date = new DateTime($debut);
            $date->modify('-1 day');
            return $date->format('Y-m-d');
        } 
    }

    /**
     *
     */
    function is_date_tableau_valid() {
        //
        $revisions = $this->get_all_revisions();
        //
        foreach ($revisions as $key => $value) {
            //
            if ($value["date_tr1"] == $this->f->collectivite['datetableau']
                || $value["date_tr2"] == $this->f->collectivite['datetableau']) {
                //
                return true;
            }
        }
        //
        return false;
    }

}

?>

<?php
/**
 * Ce fichier declare la classe inseeExportTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class inseeExportTestTraitement extends traitement {
    
    var $fichier = "insee_export_test";
    var $champs = array("format");
    
    function getValidButtonValue() {
        //
        return _("Generation du fichier de verification de la table electeur pour l'INSEE");
    }

    function setContentForm() {
        //
        $this->form->setLib("format", _("Format du fichier d'export"));
        $this->form->setType("format", "select");
        $contenu = array(
            0 => array("txt", "xml"),
            1 => array("txt", "xml"),
            );
        $this->form->setSelect("format",$contenu);
    }

    function treatment () {
        //
        $this->LogToFile ("start insee_export_test");
        // Le traitement insee n'est pas disponible pour les listes complementaires
        $this->LogToFile(_("Envoi INSEE de la table electeur pour verification"));
        //
        (isset($_POST['format']) ? $format = $_POST['format'] : $format = "txt");
        //
        $fichier_cnen = "#";
        //
        $datetableau = $this->page->collectivite['datetableau'];
        //
        $chemin_cnen = $this->page->getParameter("chemin_cnen");
        //
        include "../sql/".$this->page->phptype."/trt_insee_export_test.inc";
        // Execution de la requete
        $this->LogToFile ("REQUETE DE SELECTION DES ELECTEURS");
        $res_select_electeur = $this->page->db->query($sql);
        if (database::isError($res_select_electeur, true)) {
            //
            $this->error = true;
            //
            $message = $res_select_electeur->getMessage()." erreur sur ".$res_select_electeur."";
            $this->LogToFile($message);
        } else {
            //
            $nbelecteur = $res_select_electeur->numRows();
            $this->LogToFile($nbelecteur." "._("electeur(s) a envoyer a l'INSEE"));
            //
            if ($nbelecteur == 0) {
                //
                $message = _("Aucun electeur a envoyer. Aucun fichier n'a ete genere.");
                $this->LogToFile($message);
                $this->addToMessage($message);
            } else {
                //
                if ($format == "txt") {
                    //
                    $this->LogToFile(_("Envoi au format txt"));
                    //
                    $fichier_cnen = $chemin_cnen.$_SESSION['liste']."_cnen_verification_electeur_".date("dmy_Gis").".txt";
                    // Ouverture du fichier
                    $inf = fopen($fichier_cnen, "w");
                    //
                    $this->LogToFile(_("Fichier :")." ".$fichier_cnen);
                    //
                    $i = 0;
                    //
                    while ($row =& $res_select_electeur->fetchRow(DB_FETCHMODE_ASSOC)) {
                        
                        // Donnees                            Position Longueur Type Divers
                        //
                        // Nom                                1        63       A    lettre maj sans accents
                        // Prénom                             64       50       A    lettre maj sans accents
                        // Sexe                               114      1        A    1 ou 2
                        // Date naissance                     115      8        N    jjmmaaaa
                        // Code lieu naissance                123      5        A    92001
                        // Libellé lieu naissance             128      30       A
                        // Libellé DOM ou pays de naissance   158      30       A
                        // Date inscription                   188      8        N    jjmmaaaa
                        // Code du lieu inscription           196      5        A
                        // Numéro inscription                 201      15       A
                        
                        //
                        //
                        //
                        
                        // Nom
                        $nom =  str_pad($row['nom'], 63, " ", STR_PAD_RIGHT);
                        // Prenom
                        $prenom = str_pad($row['prenom'], 50, " ", STR_PAD_RIGHT);
                        // Sexe 1 = masculin 2 = feminin
                        if ($row['sexe'] == 'M') {
                            $sexe = "1";
                        } elseif ($row['sexe'] == 'F') {
                            $sexe = "2";
                        } else {
                            $sexe = "0";
                        }
                        // Date de naissance JJMMAAAA
                        $date_naissance = substr($row['date_naissance'],8,2).substr($row['date_naissance'],5,2).substr($row['date_naissance'],0,4);
                        $date_naissance = str_pad($date_naissance, 8, "0", STR_PAD_RIGHT);
                        // Code departement de naissance
                        $departement = substr($row['code_departement_naissance'], 0, 2);
                        if ($departement != "99") {
                            $departement = substr($row['code_lieu_de_naissance'], 0, 2).substr($row['code_lieu_de_naissance'], 3, 3);
                            $departement = str_pad($departement, 5, " ", STR_PAD_RIGHT);
                        } else {
                            $departement = str_pad($row['code_departement_naissance'], 5, " ", STR_PAD_RIGHT);
                        }
                        // Libelle de la commune de naissance 
                        // Nom du territoire outre-mer ou pays de naissance
                        if (substr($row['code_departement_naissance'],0,2)== "97" or
                            substr($row['code_departement_naissance'],0,2)== "98" or
                            substr($row['code_departement_naissance'],0,2)== "99" ) {
                            // Libelle de la commune de naissance
                            $libellecommune = str_pad($row['libelle_lieu_de_naissance'],30," ", STR_PAD_RIGHT);
                            // Nom du territoire outre-mer ou pays de naissance
                            $libelledepartement = str_pad($row['libelle_departement_naissance'],30," ", STR_PAD_RIGHT);
                        } else {
                            // Libelle de la commune de naissance
                            $libellecommune = str_pad($row['libelle_lieu_de_naissance'],30," ", STR_PAD_RIGHT);
                            // Nom du territoire outre-mer ou pays de naissance
                            $libelledepartement = str_pad("",30," ", STR_PAD_RIGHT);
                        }
                        // Date d'inscription jjmmaaaa
                        $date_inscription = substr($row['date_tableau'],8,2).substr($row['date_tableau'],5,2).substr($row['date_tableau'],0,4);
                        $date_inscription = str_pad($date_inscription,8,"0", STR_PAD_RIGHT);
                        // Code INSEE de la commune
                        $inseeville = str_pad($this->page->collectivite['inseeville'], 5, "0", STR_PAD_RIGHT);
                        // Numero d'inscription
                        $numero_inscription= str_pad($row['id_electeur'], 15, " ", STR_PAD_LEFT);
                        // Concatenation des champs
                        $cnen = $nom.$prenom.$sexe.$date_naissance.$departement;
                        $cnen .= $libellecommune.$libelledepartement.$date_inscription;
                        $cnen .= $inseeville.$numero_inscription.$this->page->getParameter("newline");
                        
                        //
                        //
                        //
                        
                        // Ecriture de la ligne
                        fwrite($inf, $cnen);
                        // On incremente le compteur
                        $i++;
                    }
                    //
                    $this->LogToFile($i." "._("ligne(s) dans le fichier"));
                    // Fermeture du fichier
                    fclose($inf);
                } elseif ($format == "xml") {
                    //
                    $this->error = true;
                    //
                    $this->LogToFile(_("Envoi au format xml non disponible"));
                    //
                } else {
                    //
                    $this->error = true;
                    //
                    $message = _("Le format d'export selectionne n'existe pas.");
                    $this->LogToFile($message);
                }
            }
        }
        //
        $this->LogToFile ("end insee_export_test");
    }
}

?>
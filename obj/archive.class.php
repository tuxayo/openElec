<?php
/**
 * Ce fichier permet de definir la classe archive
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class archive extends dbForm {

    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "archive";

    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "id";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "N";
    
    /**
     * Constructeur 
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function archive($id,$db,$DEBUG) {
        $this->constructeur($id,$db,$DEBUG);
    }

    /**
     *
     */
    function ajouter($val, &$db = NULL, $DEBUG = false) {
        //
        $this->correct = false;
        $this->addToMessage(_("L'ajout d'archive est impossible. Vous devez proceder a un traitement d'archivage.")."<br/>");
    }

    /**
     *
     */
    function modifier($val = array(), &$db = NULL, $DEBUG = false) {
        //
        $this->correct = false;
        $this->addToMessage(_("La modification d'archive est impossible.")."<br/>");
    }

    /**
     *
     */
    function supprimer($val = array(), &$db = NULL, $DEBUG = false) {
        //
        $this->correct = false;
        $this->addToMessage(_("La suppression d'archive est impossible.")."<br/>");
    }

    /**
     * Ajout dans la table archive des mouvements
     */
    function ajouterTraitement($row,&$db,$DEBUG,$dateTableau){
    // parametrage
    $this->msg="";
        $this->setValFTraitement($row,$dateTableau);
        $this->setId($db); // id automatique
        $res= $db->autoExecute($this->table,$this->valF,DB_AUTOQUERY_INSERT);
         if (database::isError($res))
               $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
               else{
               if ($DEBUG == 1)
               echo _("La requete de mise a jour est effectuee")."<br>";
               $this->msg=$this->msg."<br>"._("Enregistrement")." ".
               $this->valF[$this->clePrimaire]._(" de la table ").
               $this->table." [".$db->affectedRows().
                  _(" enregistrement ajoute]") ;
               } // requete maj
    }

    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * de recuperer l'element suivant dans la sequence pour les id automatiques.
     * 
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function setId(&$db){
        // id automatique nextid
        $this->valF['id'] = $db->nextId($this->table);
    }
    
    /**
     *
     */
    function  setValFTraitement($row,$dateTableau){
        // identifiant
        $this->valF['code_bureau'] = $row['code_bureau'];
        $this->valF['ancien_bureau'] = $row['ancien_bureau'];//vide en inscription
        $this->valF['bureauforce'] = $row['bureauforce'];
        $this->valF['numero_bureau'] = $row['numero_bureau']; // provisoire en inscription
        $this->valF['numero_electeur'] = $row['numero_electeur']; // vide en inscription
        $this->valF['id_electeur'] = $row['id_electeur']; //
        // Etat civil
        $this->valF['civilite'] = $row['civilite'];
        $this->valF['sexe'] = $row['sexe'];
        $this->valF['nom'] = $row['nom'];
        $this->valF['nom_usage'] = $row['nom_usage'];
        $this->valF['prenom'] = $row['prenom'];
        $this->valF['situation'] = $row['situation'];
        $this->valF['date_naissance'] = $row['date_naissance'];
        $this->valF['code_departement_naissance'] = $row['code_departement_naissance'];
        $this->valF['libelle_departement_naissance'] = $row['libelle_departement_naissance'];
        $this->valF['code_lieu_de_naissance'] = $row['code_lieu_de_naissance'];
        $this->valF['libelle_lieu_de_naissance'] = $row['libelle_lieu_de_naissance'];
        $this->valF['code_nationalite'] = $row[ 'code_nationalite'];
        //adresse
        $this->valF['code_voie'] = $row['code_voie'];
        $this->valF['libelle_voie'] = $row['libelle_voie'];
        $this->valF['numero_habitation'] = $row['numero_habitation'];
        $this->valF['complement_numero'] = $row['complement_numero'] ;
        $this->valF['complement'] = $row['complement'] ;
        // provennance
        $this->valF['provenance'] = $row['provenance'];
        $this->valF['libelle_provenance'] = $row['libelle_provenance'];
        // adresse resident
        $this->valF['resident'] = $row['resident'];
        $this->valF['adresse_resident'] = $row['adresse_resident'];
        $this->valF['complement_resident'] = $row['complement_resident'];
        $this->valF['cp_resident'] = $row['cp_resident'] ;
        $this->valF['ville_resident'] = $row['ville_resident'] ;
        //  mouvement
        $this->valF['date_modif'] = $row['date_modif'];
        $this->valF['utilisateur']= $row['utilisateur'];
        $this->valF['mouvement']=$row['id'];
        $this->valF['observation']=$row['observation'];
        $this->valF['date_mouvement']=$this->dateSystemeDB();
        $this->valF['types']=$row["types"];
        //cnen
        $this->valF['envoi_cnen']=$row["envoi_cnen"];
        // compatibilite postgreSQL (date vide non acceptees)
        if (!isset($row["date_cnen"]) )
            $this->valF['date_cnen']=null;
        else
           if ($row["date_cnen"]!='' )
              $this->valF['date_cnen']=$row["date_cnen"];
        //tableau
        $this->valF['tableau']=$row['tableau'];
        $this->valF['date_tableau']=$dateTableau;
        //liste
        $this->valF['liste']=$row['liste'];
        $this->valF['etat']=$row['etat'];
        $this->valF['typecat'] = "";
        //collectivite
        /* ++ */ $this->valF['collectivite'] = $row['collectivite'];
    }

}

?>
<?php

require_once ("../obj/traitement.class.php");

class centrevoteTraitement extends traitement {
    
    var $fichier = "centrevote";
    
    var $champs = array("debut_validite");
    
    function getValidButtonValue() {
        //
        return _("Mise a jour de la date de debut de validite");
    }
    
    function setContentForm() {
        //
        $this->form->setLib("debut_validite", _("Date de debut de validite"));
        $this->form->setType("debut_validite", "date");
        $this->form->setTaille("debut_validite", 10);
        $this->form->setMax("debut_validite", 10);
        $this->form->setOnchange("debut_validite", "fdate(this)");
    }
    
    function treatment () {
        //
        $this->LogToFile ("start centrevote");
        
        // queries
        include ("../sql/".$this->page->phptype."/trt_centrevote.inc");
        
        // debut_validite
        //
        (isset($_POST['debut_validite']) ? $debut_validite = $_POST['debut_validite'] : $debut_validite = "");
        $debut_validite = $this->page->formatDate($debut_validite, false);
        if ($debut_validite == false) {
            //
            $this->error = true;
            //
            $message = $_POST['debut_validite']." "._("La date n'est pas correcte.");
            $this->LogToFile($message);
            $this->addToMessage($message);
        } else {
            
            // fin_validite
            $date = explode ("-", $debut_validite);
            $fin_validite = (intval ($date[0]) + 1)."-".$date[1]."-".$date[2];
            
            // liste des id_electeur a traiter
            $list_id_electeur = $this->page->db->getAll ($query_select_centrevote, DB_FETCHMODE_ASSOC);
            $message = count($list_id_electeur)." "._("electeur(s) en centre de vote a mettre a jour");
            $this->LogToFile ($message);
            
            // gestion des erreurs
            if (database::isError($list_id_electeur, true)) {
                //
                $this->error = true;
                //
                $message = $list_id_electeur->getMessage ()." - ".$list_id_electeur->getUserInfo ();
                $this->LogToFile ($message);
            } elseif (count($list_id_electeur) > 0) {
                // clause where sur tous les id_electeur
                $ids = "";
                foreach ($list_id_electeur as $key => $value) {
                    $ids .= " id_electeur = ".$value['id_electeur']." or";
                }
                $ids = substr ($ids, 0, count($ids) - 3);
                
                // mise a jour de la table centrevote
                $fields_values = array(
                    'debut_validite' => $debut_validite,
                    'fin_validite' => $fin_validite);
                $res = $this->page->db->autoExecute ("centrevote", $fields_values, DB_AUTOQUERY_UPDATE, $ids);
                
                // gestion des erreurs
                if (database::isError($res, true)) {
                    //
                    $this->error = true;
                    //
                    $message = $res->getMessage ()." - ".$res->getUserInfo ();
                    $this->LogToFile ($message);
                } else {
                    //
                    $this->LogToFile ("debut validite ".$debut_validite." - fin validite ".$fin_validite);
                    $this->LogToFile (count($list_id_electeur)." electeur(s) en centre de vote mis a jour");
                }
            }
        }
        //
        $this->LogToFile ("end centrevote");
    }
}

?>
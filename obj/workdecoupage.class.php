<?php
/**
 *
 */

class workdecoupage {

    /**
     *
     * @var resource Instance de la classe utils
     */
    var $f = NULL;

    /**
     *
     */
    function __construct() {
        // 
        if (isset($GLOBALS["f"])) {
            $this->f = $GLOBALS["f"];
        } else {
            die();
        }
    }

    function display_result_from_db_load_all($result = array()) {
        //
        $table = "
        <table border=\"1\">
            <thead>
                %s
            </thead>
            <tbody>
                %s
            </tbody>
            <tfoot>
                %s
            </tfoot>
        </table>
        ";
        $head = "
            %s
        ";
        $head_line = "
            <tr>
                %s
            </tr>
        ";
        $head_cell = "
            <th>
                %s
            </th>
        ";
        $body = "
            %s
        ";
        $body_line = "
            <tr>
                %s
            </tr>
        ";
        $body_cell = "
            <td>
                %s
            </td>
        ";
        $foot = "
            %s
        ";
        $foot_line = $body_line;
        $foot_cell = "";
        //
        $head_content = "";
        $body_content = "";
        $foot_content = "";
        //
        foreach($result as $key => $line) {
            //
            if ($key == 0) {
                $colspan = count($line);
                foreach ($line as $title => $value) {
                    $head_content .= sprintf($head_cell, $title);
                }
            }
            //
            $line_content = "";
            foreach ($line as $value) {
                $line_content .= sprintf($body_cell, $value);
            }
            $body_content .= sprintf($body_line, $line_content);
        }
        $foot_content = sprintf($foot_line, sprintf($foot_cell, $colspan, count($result)));
        echo sprintf($table, sprintf($head, sprintf($head_line, $head_content)),
                             sprintf($body, $body_content),
                             sprintf($foot, $foot_content));
    }

    function db_load_all($query = "") {
        //
        $res = $this->f->db->query($query);
        $this->f->addToLog("obj/maintenance.class.php: db->query(\"".$query."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        //
        $result = array();
        while($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            array_push($result, $row);
        }
        $res->free();
        //
        return $result;
    }

    /**
     *
     */
    function simulation() {
        //
        // {{{ Requête de simulation du découpage
        $sql = "
SELECT
    --collectivite as id_collectivite,
    --collectivite.ville as collectivite,
    (bureau.code||' - '||bureau.libelle_bureau) as bureau,
    -- Nombre d'electeurs par bureau avant simulation
    (
    -- Nombre d'electeurs de la liste 1 par bureau
    SELECT
        count(*)
     FROM
        electeur
     WHERE
        code_bureau = bureau.code
        AND liste = '".$_SESSION["liste"]."'
        AND electeur.collectivite = bureau.collectivite
    )
    as \"nb electeurs avant simu\",
    -- Nombre d'electeurs de la liste 1 en bureau force, par bureau
    (
    SELECT
        count(*)
     FROM
        electeur
     WHERE
        bureauforce = 'Oui'
        AND code_bureau = bureau.code
        AND liste = '".$_SESSION["liste"]."'
        AND electeur.collectivite = bureau.collectivite
    )
    as \"nb electeurs bureau force\",
    -- Nombre d'electeurs simulation
    (
    -- Nombre d'electeurs de la liste 1 en bureau force, par bureau
    SELECT
        count(*)
     FROM
        electeur
     WHERE
        electeur.collectivite = bureau.collectivite
        AND liste = '".$_SESSION["liste"]."'
        AND code_bureau = bureau.code
        AND bureauforce = 'Oui'
    ) + (
    -- Nombre d'electeurs affectes au bureau par les decoupages
    SELECT
        count(*)
    FROM
        electeur
        LEFT JOIN voie
            ON electeur.code_voie = voie.code
        JOIN decoupage
            ON  decoupage.code_voie = electeur.code_voie
            AND decoupage.code_bureau = bureau.code
            AND ( CASE numero_habitation % 2
                WHEN 0
                THEN decoupage.premier_pair <= numero_habitation AND numero_habitation <= decoupage.dernier_pair
                WHEN 1
                THEN decoupage.premier_impair <= numero_habitation AND numero_habitation <= decoupage.dernier_impair
                END )
    WHERE
        electeur.collectivite = bureau.collectivite
        AND liste = '".$_SESSION["liste"]."'
        AND decoupage.code_bureau = bureau.code
        AND bureauforce = 'Non'
    )
    as \"nb electeurs apres simu\"
FROM
    bureau
    LEFT OUTER JOIN collectivite ON bureau.collectivite = collectivite.id
    -- Decommenter pour filtrer sur une collectivite
WHERE
    bureau.collectivite = '".$_SESSION["collectivite"]."'
GROUP BY
    collectivite,
    collectivite.ville,
    bureau.code,
    bureau.libelle_bureau
ORDER BY
    collectivite.ville,
    collectivite,
    bureau.code,
    bureau.libelle_bureau
        ";
        // }}}
        $plop = $this->db_load_all($sql);
        $this->display_result_from_db_load_all($plop);

        // Nombre d'electeurs en bureau force
        $sql = "
SELECT
    (
    SELECT 
        count(*)
    FROM
        electeur
    WHERE
        bureauforce='Oui'
        AND liste = '".$_SESSION["liste"]."'
        AND collectivite = '".$_SESSION["collectivite"]."'
    ) as \"nb electeur bureauforce\",
    (
    SELECT
        count(*)
    FROM
        electeur
    WHERE
        liste = '".$_SESSION["liste"]."'
        AND collectivite = '".$_SESSION["collectivite"]."'
    ) as \"nb electeur total\",
    (
        (
        SELECT 
            count(*)
        FROM
            electeur
        WHERE
            bureauforce='Oui'
            AND liste = '".$_SESSION["liste"]."'
            AND collectivite = '".$_SESSION["collectivite"]."'
        )
        *100/
        (
        SELECT
            count(*)
        FROM
            electeur
        WHERE
            liste = '".$_SESSION["liste"]."'
            AND collectivite = '".$_SESSION["collectivite"]."'
        )
        ||'%'
    ) as \"pourcentage electeur en bureauforce\"
;
        ";
        $plop = $this->db_load_all($sql);
        $this->display_result_from_db_load_all($plop);

    }

}

?>

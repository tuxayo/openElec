<?php
/**
 * Ce fichier declare la classe decoupageInitBureauforceTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class decoupageInitDecoupageTraitement extends traitement {
    
    var $fichier = "decoupage_init_decoupage";
    
    function getDescription() {
        //
        return _("Attention, il ne faut appliquer ce traitement qu'en ".
                 "connaissance de cause. Il permet pour une base qui n'a pas ".
                 "ete intialisee en decoupage de parcourir la table electeur ".
                 "pour creer le decoupage de maniere automatique pour toutes ".
                 "les voies qui ne sont affectees qu'a un seul bureau.");
    }
    
    function getValidButtonValue() {
        //
        return _("Initialiser la table de decoupage");
    }
    
    function treatment () {
        //
        $this->LogToFile("start decoupage_init_decoupage");
        //
        include "../sql/".$this->page->phptype."/trt_decoupage_init_decoupage.inc";
        //
        $res = $this->page->db->query($sql_decoupage_init_decoupage);
        //
        if (database::isError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        } else {
            //
            $message = $this->page->db->affectedRows()." "._("decoupage(s) insere(s)");
            $this->LogToFile($message);
            $this->addToMessage($message);
        }
        //
        $this->LogToFile("end decoupage_init_decoupage");
    }
}

?>

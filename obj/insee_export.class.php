<?php
/**
 * Ce fichier permet de definir la classe insee_export
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
class insee_export extends XMLWriter {
    /**
     * Objet classe utils
     */
    var $f;

    /**
     * Donnees des Electeurs
     */
    var $res;
    
    /**
     * 
     */
    var $nb;

    /**
     * Nom du fichier a generer
     */
    var $fichier_cnen;
    
    /**
     * Numero d'envoi a genere
     */
    var $cnen_envoi;
    
    /**
     * Compteur de mouvements exportes
     */
    var $compteur = 0;
    
    /**
     * Numero d'envoi a regenere
     */
    var $envoi = "";
    
    
    /**
     * Constructeur
     *
     * @param $f objet Classe Utils
     * @param $fichier string Nom du fichier
     * @param $cnen_envoi int Numero d'envoi a generer
     * @param $envoi int Numero d'envoi a regenerer
     */
    public function __construct(&$f, $fichier, $cnen_envoi, $envoi = "") {
        $this->envoi = $envoi;
        $this->cnen_envoi = $cnen_envoi;
        $this->fichier_cnen = $fichier;
        $this->f = $f;
        $this->openMemory();
        $this->setIndent(true);
        $this->setIndentString('    ');
        $this->startDocument('1.0', 'UTF-8');
        //
        $this->Query();
        // ENVOI LOT
        $envoilotduniteselectorales =
            array (
                'xml:id' => "ID".uniqid());
        $this->startEnvoiLotDUnitesElectorales($envoilotduniteselectorales);
        // ENVELOPPE
        $enveloppeelectorale =
            array (
                'xml:id' => "ID".uniqid(),
                'version' => "0.6",
                'dateEnvoi' => date("Y-m-d"),
                'dateReception' => "",
                'indicateurTest' => "false");
        $this->Enveloppe($enveloppeelectorale);
        // LOT
        $lotduniteselectorales =
            array (
                'xml:id' => "ID".uniqid());
        $this->startLotDUnitesElectorales($lotduniteselectorales);
        //
        $this->ListAvis();
    }
    
    /**
     * Fonction cpt()
     *
     * @return int Retourne le nombre de mouvements
     */
    public function cpt(){
        return $this->compteur;
    }
    
    /**
     * Fonction _writeElement()
     *
     * @param $a
     * @param $b
     */
    private function _writeElement($a, $b) {
        if ($b != "")
            $this->writeElement($a, $b); 
    }
    
    /**
     * Fonction _writeAttributes()
     * 
     * @param $tab
     */
    private function _writeAttributes ($tab) {
        foreach ($tab as $key => $value)
            if (!is_array ($value))
                $this->writeAttribute($key, $value);
    }
    
    /**
     * Fonction startEnvoiLotDUnitesElectorales()
     * 
     * @param $tab
     */
    private function startEnvoiLotDUnitesElectorales($tab) {
        $this->startElement('el:EnvoiLotDUnitesElectorales'); 
        $this->_writeAttributes($tab);
        $this->writeAttribute('xsi:schemaLocation', 'http://xml.insee.fr/schema/electoral http://xml.insee.fr/schema/electoral/echanges-mairies-insee-unite-electorale.xsd');
        $this->writeAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $this->writeAttribute('xmlns:ie', 'http://xml.insee.fr/schema');
        $this->writeAttribute('xmlns:el', 'http://xml.insee.fr/schema/electoral');
        $this->writeAttribute('xmlns:io', 'http://xml.insee.fr/schema/outils');
    }

    /**
     * Fonction Enveloppe()
     *
     * @param $tab
     */
    private function Enveloppe($tab) {
        $this->startElement('el:Enveloppe'); 
        $this->_writeAttributes($tab);
        $this->Partenaires();
        $this->startElement('el:Contenu');
        $this->writeElement('io:NombreItems', $this->nb);
        $this->endElement();
        $this->startElement('el:ContexteMetier');
        $this->writeElement('io:Convention', "");
        $this->endElement();
        $this->endElement();
    }
    
    /**
     * Fonction Partenaires()
     *
     */
    private function Partenaires() {
        $sql = "select * from xml_partenaire where collectivite='".$_SESSION["collectivite"]."'";
        $res = $this -> f -> db -> query ($sql);
        if (database::isError($res))
                die ($res -> getMessage ()." erreur sur ".$sql);
        while ($row =& $res -> fetchRow (DB_FETCHMODE_ASSOC)) {
            if ($row['type_partenaire'] == "origine")
                $this->startElement('el:Origine');
            else
                $this->startElement('el:Destination');
            $this->startElement('io:Identifiant');
            $this->writeAttribute('autorite', 'http://xml.insee.fr/identifiants/SIRET');
            $this->writeRaw($row ['identifiant']);
            $this->endElement();
            $this->_writeElement('io:Nom', $row ['nom']);  
            $this->_writeElement('io:Type', $row ['type']);  
            $this->_writeElement('io:Role', $row ['role']);
            $this->startElement('io:Contact');
            $this->_writeElement('io:Nom', $row ['contact_nom']);  
            $this->_writeElement('io:Mail', $row ['contact_mail']);
            $this->_writeElement('io:NumeroFax', $row ['contact_numerofax']);
            $this->_writeElement('io:NumeroTelephone', $row ['contact_numerotelephone']);
            $this->startElement('io:Adresse');
            $this->startElement('ie:AdresseGeographique');
            $this->_writeElement('ie:NumeroVoie', $row ['contact_adresse_numerovoie']);
            $this->_writeElement('ie:TypeVoie', $row ['contact_adresse_typevoie']);
            $this->_writeElement('ie:NomVoie', $row ['contact_adresse_nomvoie']);
            $this->_writeElement('ie:Cedex', $row ['contact_adresse_cedex']);
            $this->_writeElement('ie:LibelleBureauCedex', $row ['contact_adresse_libellebureaucedex']);
            $this->_writeElement('ie:DivisionTerritoriale', $row ['contact_adresse_divisionterritoriale']);
            $this->endElement();
            $this->endElement();
            $this->endElement();
            $this->startElement('io:ServiceApplicatif');
            $this->_writeElement('io:Nom', 'openElec');  
            $this->_writeElement('io:Fournisseur', $row ['service_fournisseur']);
            $this->_writeElement('io:Version', $this->f->version);
            $this->_writeElement('io:Accreditation', $row ['service_accreditation']);
            $this->endElement();
            $this->endElement();
        }
    }    

    /**
     * Fonction startLotDUnitesElectorales()
     *
     * @param $tab
     */
    private function startLotDUnitesElectorales($tab) {
        $this->startElement('el:LotDUnitesElectorales'); 
        $this->_writeAttributes($tab);
    }

    /**
     * Fonction PeriodeDeRevision()
     *
     * @param $tab
     */   
    private function PeriodeDeRevision ($tab) {
        //
        $annee = substr ($tab ['datedetableau'], 0, 4);
        //
        $this->startElement('el:PeriodeDeRevision');
        $this->writeElement('el:DateDeDebut', $annee-1);
        $this->writeElement('el:DateDeFin', $annee);
        $this->endElement();
    }

    /**
     * Fonction SituationElectoraleAnterieure()
     *
     * @param $tab
     */
    private function SituationElectoraleAnterieure ($tab) {
        $situationdelelecteur = "";
        $typedeliste = $tab ['liste_insee'];
        $this->startElement('el:SituationElectoraleAnterieure');
        // 1ère inscription dans une commune française=prem, changement de commune d'inscription=cci
        // Les déménagements à l'intérieur d'une même commune ou d'un même arrondissement pour Paris, Lyon et Marseille ne doivent pas être transmis à l'Insee 
        if ($tab ['localite_code'] != "") {
            $situationdelelecteur = "cci";   
        } else {
            $situationdelelecteur = "prem";
        }
        $this->writeElement('el:SituationDeLElecteur', $situationdelelecteur);
        // seulement si situation de l'électeur=cci
        if ($situationdelelecteur == "cci") {
            $this->startElement('el:CommuneDePrecedenteInscription');
            $this->startElement('ie:Localite');
            if(!$this->startsWith($tab ['localite_code'],'99')
                AND strlen($tab ['localite_code']) < 5) {
                $this->writeAttribute('code', str_replace(" ","",$tab ['localite_code']));
            }
            $this->writeRaw($tab ['localite_libelle']);
            $this->endElement();
            $this->startElement('ie:DivisionTerritoriale');
            $this->writeAttribute('code', $tab ['divisionterritoriale_code']);
            $this->writeRaw($tab ['divisionterritoriale_libelle']);
            $this->endElement();
            $this->endElement();
        }
        // seulement si type de liste =p
        // Concerne les personnes également inscrites à l'étranger sur une liste électorale consulaire et qui souhaitent que l'inscription sur la liste de la nouvelle commune entraîne la radiation de cette liste électorale consulaire 
        // La balise Consulat de précédente Inscription ne doit pas apparaître si elle est vide.
        // if ($typedeliste == "p") {
        //     $this->startElement('el:ConsulatDePrecedenteInscription');
        //     $this->endElement();
        // }
        // seulement si type de liste =ce
        if ($typedeliste == "ce") {
            $this->startElement('el:PaysUeDerniereInscription');
            
            $this->endElement();
        }
        // endSituationElectoraleAnterieure
        $this->endElement();
    }

    /**
     * Fonction Inscription()
     *
     * @param $tab
     */
    private function Inscription ($tab) {
        $this->startElement('el:Inscription');
        
        $this->Electeur($tab ['electeur']);
        // principale = p, complémentaire parlement européen=ce, complémentaire municipales=cm
        $this->writeElement('el:TypeDeListe', $tab ['typedeliste']);  
        
        $this->startElement('el:CommuneDInscription');
        $this->startElement('ie:Localite');
        if(!$this->startsWith($tab ['communedinscription']['localite_code'],'99')
            AND strlen($tab ['communedinscription']['localite_code']) < 5) {
            $this->writeAttribute('code', str_replace(" ","", $tab ['communedinscription']['localite_code']));
        }
        $this->writeRaw($tab ['communedinscription']['localite_libelle']);
        $this->endElement();
        $this->startElement('ie:DivisionTerritoriale');
        $this->writeAttribute('code', str_replace(" ","", $tab ['communedinscription']['divisionterritoriale_code']));
        $this->writeRaw($tab ['communedinscription']['divisionterritoriale_libelle']);
        $this->endElement();
        $this->endElement();
        
        $this->endElement();
    }
    
    /**
     * Fonction Electeur
     *
     * @param $tab
     */
    private function Electeur ($tab) {
        $this->compteur++;
        $this->startElement('el:Electeur');
        
        $this->startElement('el:Noms');
        $this->writeElement('ie:NomFamille', $tab ['nomfamille']);
        if ($tab ['nomusage'] != "")
            $this->writeElement('ie:NomUsage', $tab ['nomusage']);
        $this->endElement();
        
        $this->startElement('el:Prenoms');
        $prenoms = explode(" ", $tab ['prenom']);
        foreach ($prenoms as $prenom)
            $this->writeElement('ie:Prenom', $prenom);
        $this->endElement();
        
        $this->writeElement('el:DateDeNaissance', $tab ['datedenaissance']);
        
        $this->startElement('el:LieuDeNaissance');
        $this->startElement('ie:Localite');
        if(!$this->startsWith($tab ['lieudenaissance']['localite_code'],'99')
            AND strlen($tab ['lieudenaissance']['localite_code']) < 5) {
            $this->writeAttribute('code', str_replace(" ","", $tab ['lieudenaissance']['localite_code']));
        }
        $this->writeRaw($tab ['lieudenaissance']['localite_libelle']);
        $this->endElement();
        $this->startElement('ie:DivisionTerritoriale');
        $this->writeAttribute('code', str_replace(" ","", $tab ['lieudenaissance']['divisionterritoriale_code']));
        $this->writeRaw($tab ['lieudenaissance']['divisionterritoriale_libelle']);
        $this->endElement();
        $this->endElement();
        
        $this->writeElement('el:Sexe', $tab ['sexe']);

        $this->writeElement('el:Nationalite', $tab ['nationalite']);
        
        $this->startElement('el:AdresseDeLElecteur');
        if ($tab ['adressedelelecteur']['complement'] != "")
            $this->writeElement('ie:Complement', $tab ['adressedelelecteur']['complement']);
        if ($tab ['adressedelelecteur']['numerovoie'] != "")
            $this->writeElement('ie:NumeroVoie', $tab ['adressedelelecteur']['numerovoie']);
        if ($tab ['adressedelelecteur']['extension'] != "")
            $this->writeElement('ie:Extension', $tab ['adressedelelecteur']['extension']);
        $this->writeElement('ie:NomVoie', $tab ['adressedelelecteur']['nomvoie']);
        $this->writeElement('ie:CodePostal', $tab ['adressedelelecteur']['codepostal']);
        $this->startElement('ie:Localite');
        if(!$this->startsWith($tab ['adressedelelecteur']['localite_code'],'99')
            AND strlen($tab ['adressedelelecteur']['localite_code']) < 5) {
            $this->writeAttribute('code', str_replace(" ","",$tab ['adressedelelecteur']['localite_code']));
        }
        $this->writeRaw($tab ['adressedelelecteur']['localite_libelle']);
        $this->endElement();
        $this->startElement('ie:DivisionTerritoriale');
        $this->writeAttribute('code', str_replace(" ","",$tab ['adressedelelecteur']['divisionterritoriale_code']));
        $this->writeRaw($tab ['adressedelelecteur']['divisionterritoriale_libelle']);
        $this->endElement();
        $this->endElement();
        
        $this->endElement();
    }

    /**
     * Fonction DateDInscription()
     *
     * @param $tab
     */
    private function DateDInscription ($tab) {
        $this->writeElement('el:DateDInscription', $tab['datedinscription']);
    }
    
    /**
     * fonction TypeDInscription()
     *
     * @param $tab
     */
    private function TypeDInscription ($tab) {
        if ($tab ['typedinscription'] == 1 || $tab ['typedinscription'] == "vol")
            $tab ['typedinscription'] = "vol";
        if ($tab ['typedinscription'] == 2 || $tab ['typedinscription'] == "dj")
            $tab ['typedinscription'] = "dj";
        $this->writeElement('el:TypeDInscription', $tab ['typedinscription']);
    }

    /**
     * Fonction DateDeLaRadiation()
     *
     * @param $tab
     */
    private function DateDeLaRadiation ($tab) {
        $this->writeElement('el:DateDeLaRadiation', $tab['datedelaradiation']);
    }

    /**
     * Fonction MotifDeLAvisDeRadiation()
     *
     * @param $tab
     */
    private function MotifDeLAvisDeRadiation ($tab) {
        $this->startElement('el:MotifDeLAvisDeRadiation');
        //
        if ($tab ['typederadiation'] == "P" || $tab ['typederadiation'] == "pqrl")
            $tab ['typederadiation'] = "pqrl";
        if ($tab ['typederadiation'] == "D" || $tab ['typederadiation'] == "dec")
            $tab ['typederadiation'] = "dec";
        if ($tab ['typederadiation'] == "J" || $tab ['typederadiation'] == "dj")
            $tab ['typederadiation'] = "dj";
        if ($tab ['typederadiation'] == "E" || $tab ['typederadiation'] == "rem")
            $tab ['typederadiation'] = "rem";
        //
        if ($tab ['typedeliste'] == 'p')
            $this->writeElement('MotifListePrincipale', $tab ['typederadiation']);
        if ($tab ['typedeliste'] == 'cm' || $tab ['typedeliste'] == 'ce')
            $this->writeElement('MotifListeComplementaire', $tab ['typederadiation']);
        $this->endElement();
    }
    
    /**
     * Fonction NumeroDEnregistrement()
     *
     * @param $tab
     */
    private function NumeroDEnregistrement ($tab) {
        $this->_writeElement('el:NumeroDEnregistrement', $tab ['numerodenregistrement']);
    }
    
    /**
     * Fonction NumeroDeLitige()
     *
     * @param $tab
     */
    private function NumeroDeLitige ($tab) {
        $this->_writeElement('el:NumeroDeLitige', $tab ['numerodelitige']);
    }

    /**
     * Fonction NumeroDeGestionDeLIndividu()
     *
     * @param $tab
     */
    private function NumeroDeGestionDeLIndividu ($tab) {
        $this->_writeElement('el:NumeroDeGestionDeLIndividu', $tab ['numerodegestiondelindividu']);
    }  

    /**
     * Fonction AvisCommun()
     *
     * @param $tab
     * @param $row
     */
    public function AvisCommun($tab,$row) {
        
        $numerodelitige =
            array (
                'numerodelitige' => ''); // Non defini dans openelec
        $this->NumeroDeLitige ($numerodelitige);
        
        $numerodegestiondelindividu =
            array (
                'numerodegestiondelindividu' => ''); // Non defini dans openelec
        $this->NumeroDeGestionDeLIndividu ($numerodegestiondelindividu); 
        // 
        $periodederevision =
            array (
                'datedetableau' => $row ['date_tableau']);
        $this->PeriodeDeRevision ($periodederevision);
        // Ce numéro est attribué par la mairie. Il permet d’identifier l'avis
        // d'inscription, notamment en cas de litige. La numérotation doit être
        // effectuée séparément pour chaque type de liste. Les numéros sont
        // attribués du 1er janvier au 31 décembre, en commençant par 1 et en
        // suivant l’ordre dans lequel les documents sont établis par la mairie
        // puis adressés à l’Insee.
        /*$numerodenregistrement =
            array (
                'numerodenregistrement' => $row ['']);
        $this->NumeroDEnregistrement ($numerodenregistrement);*/
        //
        $sql = "select * from departement where code='".substr($this->f->collectivite ['inseeville'],0,2)."'";
        $res = $this -> f -> db -> query ($sql);
        if (database::isError($res))
                die ($res -> getMessage ()." erreur sur ".$sql);
        $divisionterritoriale = $res -> fetchRow (DB_FETCHMODE_ASSOC);
        $inscription =
            array (
                'electeur' =>
                    array (
                        'nomfamille' => $row ['nom'],
                        'nomusage' => $row ['nom_usage'],
                        'prenom' => $row ['prenom'],
                        'datedenaissance' => $row ['date_naissance'],
                        'sexe' => $row ['sexe'],
                        'lieudenaissance' =>
                            array (
                                'localite_code' => $row ['code_lieu_de_naissance'],
                                'localite_libelle' => $row ['libelle_lieu_de_naissance'],
                                'divisionterritoriale_code' => $row ['code_departement_naissance'],
                                'divisionterritoriale_libelle' => $row ['libelle_departement_naissance']),
                        'nationalite' => $row ['code_nationalite'],
                        'adressedelelecteur' =>
                            array (
                                'numerovoie' => $row ['numero_habitation'],
                                'extension' => $row ['complement_numero'],
                                'nomvoie' => $row ['libelle_voie'],
                                'complement' => $row ['complement'],
                                'codepostal' => $row ['cp'],
                                'localite_code' => $this->f->collectivite ['inseeville'],
                                'localite_libelle' => $this->f->collectivite ['ville'],
                                'divisionterritoriale_code' => $divisionterritoriale ['code'],
                                'divisionterritoriale_libelle' => $divisionterritoriale ['libelle_departement'])),
                'typedeliste' => $row ['liste_insee'],
                'communedinscription' =>
                    array (
                        'localite_code' => $this->f->collectivite ['inseeville'],
                        'localite_libelle' => $this->f->collectivite ['ville'],
                        'divisionterritoriale_code' => $divisionterritoriale ['code'],
                        'divisionterritoriale_libelle' => $divisionterritoriale ['libelle_departement']));
        $this->Inscription ($inscription);

    }

    /**
     * Fonction AvisDInscription()
     *
     * @param $tab
     * @param $row
     */
    public function AvisDInscription($tab, $row) {
        $this->startElement('el:AvisDInscription'); 
        $this->_writeAttributes($tab);
        //
        $this->AvisCommun ($tab, $row);
        // Date de réception de la demande en mairie
        $datedinscription =
            array (
                'datedinscription' => $row["date_modif"]);
        $this->DateDInscription ($datedinscription);
        // inscription volontaire=vol, inscription par décision judiciaire=dj
        $typedinscription =
            array (
                'typedinscription' => $row ['codeinscription']);
        $this->TypeDInscription ($typedinscription);
        //
        $sql = "select * from departement where code='".substr($row ['provenance'],0,2)."'";
        $res = $this -> f -> db -> query ($sql);
        if (database::isError($res))
                die ($res -> getMessage ()." erreur sur ".$sql);
        $divisionterritoriale = $res -> fetchRow (DB_FETCHMODE_ASSOC);
        $situationelectoraleanterieure =
            array (
                'localite_code' => $row ['provenance'],
                'localite_libelle' => $row ['libelle_provenance'],
                'divisionterritoriale_code' => $divisionterritoriale['code'],
                'divisionterritoriale_libelle' => $divisionterritoriale['libelle_departement'],
                'liste_insee' => $row ['liste_insee']);
        $this->SituationElectoraleAnterieure ($situationelectoraleanterieure);
        //
        $this->endElement();
    }

    /**
     * Fonction AvisdeRadiation()
     *
     * @param $tab
     * @param $row
     */
    public function AvisDeRadiation($tab, $row) {
        $this->startElement('el:AvisDeRadiation'); 
        $this->_writeAttributes($tab);
        //
        $this->AvisCommun ($tab, $row);
        //
        $datedelaradiation =
            array (
                'datedelaradiation' => $row["date_modif"]);
        $this->DateDeLaRadiation ($datedelaradiation);
        
        //
        $motidelavisdelaradiation =
            array (
                'typedeliste' => $row ['liste_insee'],
                'typederadiation' => $row ['coderadiation']);
        $this->MotifDeLAvisDeRadiation ($motidelavisdelaradiation);
        //
        $this->endElement();
    }

    /**
     * Fonction Query()
     *
     */
    private function Query () {
        $sql = "select * ";    
        $sql .= "from mouvement inner join param_mouvement ";
        $sql .= "on mouvement.types=param_mouvement.code ";
        $sql .= "inner join liste ";
        $sql .= "on mouvement.liste=liste.liste ";
        $sql .= "inner join voie ";
        $sql .= "on mouvement.code_voie=voie.code "; 
        $sql .= "where mouvement.collectivite='".$_SESSION["collectivite"]."' and date_tableau='".$this->f -> collectivite ['datetableau']."' ";
        if ( $this->envoi != "")
        $sql .= "and mouvement.envoi_cnen='".$this->envoi."' ";
        else
        $sql .= "and mouvement.envoi_cnen is null ";
        $sql .= "and param_mouvement.cnen='Oui' ";
        $sql .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
        
        // Exécution de la requête
        $res = $this -> f -> db -> query ($sql);
        if (database::isError($res))
            die ($res -> getMessage ()." erreur sur ".$sql);
        $this -> nb = $res -> numRows ();
        $this -> res = $res;
    }
    
    /**
     * Fonction ListAvis()
     */
    private function ListAvis () {
        while ($row =& $this -> res -> fetchRow (DB_FETCHMODE_ASSOC))
        {
            if ($row ['typecat'] == "Inscription") {
                
                $sql1 = "update mouvement set envoi_cnen='".$this->cnen_envoi."', date_cnen='".date('Y-m-d')."' where collectivite='".$_SESSION ['collectivite']."' and id=".$row['id'];
                $res1 = $this -> f -> db -> query ($sql1);
                if (database::isError($res1))
                    die ($res1 -> getMessage ()." erreur sur ".$sql1);
                
                $avisdinscription =
                    array (
                        'xml:id' => "ID".uniqid());
                $this -> AvisDInscription($avisdinscription, $row);
            }
            elseif ($row ['typecat'] == "Radiation") {
                
                $sql1 = "update mouvement set envoi_cnen='".$this->cnen_envoi."', date_cnen='".date('Y-m-d')."' where collectivite='".$_SESSION ['collectivite']."' and id=".$row['id'];
                $res1 = $this -> f -> db -> query ($sql1);
                if (database::isError($res1))
                    die ($res1 -> getMessage ()." erreur sur ".$sql1);
                
                $avisderadiation =
                    array (
                        'xml:id' => "ID".uniqid());
                $this -> AvisDeRadiation($avisderadiation, $row);
            }
        }
    }

    /**
     * Permet de definir si une chaîne de caractère commence par $needle
     * @param  string $haystack chaîne à tester
     * @param  string $needle   chaîne à trouver
     * @return boolean          true si $needle est trouvé en début de $haystack
     */
    function startsWith($haystack, $needle)
    {
        return $needle === "" || strpos($haystack, $needle) === 0;
    }
    
    /**
     * Output the content of a current xml document.
     * @access public
     * @param null
     */
    public function output(){
        //header('Content-type: text/xml');
        $this->endElement(); //endLotDUnitesElectorales
        $this->endElement(); //endEnvoiLotDUnitesElectorales
        $this->endDocument();
        //print $this->outputMemory();
        $inf = fopen ($this->fichier_cnen, "w");
        fwrite($inf,$this->outputMemory());
        fclose ($inf);

    }
   

}
?>
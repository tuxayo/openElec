<?php
/**
 * Ce fichier permet de definir la classe electeur
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class electeur extends dbForm {

    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "electeur";

    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "id_electeur";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "N";

    /**
     *
     */
    function electeur($id, &$db, $DEBUG = false) {
        //
        $this->constructeur($id, $db);
    }

    /**
     *
     */
    function ajouter($val = array(), &$db = NULL, $DEBUG = false) {
        //
        $this->correct = false;
        $this->addToMessage(_("L'ajout d'electeur est impossible. Vous devez proceder a une inscription.")."<br/>");
    }

    /**
     *
     */
    function supprimer($val = array(), &$db = NULL, $DEBUG = false) {
        //
        $this->correct = false;
        $this->addToMessage(_("La suppression d'electeur est impossible. Vous devez proceder a une radiation.")."<br/>");
    }

    /**
     *
     */
    function getDataSubmit() {
        //
        $datasubmit = parent::getDataSubmit();
        //
        $datasubmit .= "&amp;nom=".$this->getParameter("nom");
        $datasubmit .= "&amp;exact=".$this->getParameter("exact");
        $datasubmit .= "&amp;prenom=".$this->getParameter("prenom");
        $datasubmit .= "&amp;datenaissance=".$this->getParameter("datenaissance");
        $datasubmit .= "&amp;marital=".$this->getParameter("marital");
        $datasubmit .= "&amp;origin=".$this->getParameter("origin");
        $datasubmit .= "&amp;retour=".$this->getParameter("retour");
        //
        return $datasubmit;
    }

    /**
     * Cette methode est appelee lors d'un traitement
     * Elle appplique un mouvement d'inscription sur un electeur
     * INSERT electeur
     **/
    function ajouterTraitement ($row, &$db, $DEBUG, $dateTableau) {
        // Initialisation message
        $this -> msg = "";
        // Recuperation de l'identifiant de l'electeur
        $this -> setId ($db);
        //
        $this -> setValFTraitement ($row, $dateTableau);
        // calcul du numero definitif dans le bureau
        $this -> rechercheNumeroBureau ($db, $DEBUG);
        // calcul du numero dans la liste
        $this -> rechercheNumeroElecteur ($db, $DEBUG); 
        // stockage des informations d'inscription (date et code)
        $this -> informationInscription ($row); 
        // Insertion ELECTEUR
        $res = $db -> autoExecute ($this -> table, $this -> valF, DB_AUTOQUERY_INSERT);
        if (database::isError($res))
            $this -> erreur_db ($res -> getDebugInfo (), $res -> getMessage (), '');
        $this->msg .= _("Enregistrement ").$this->valF[$this->clePrimaire]._(" de la table ").$this->table." [".$db->affectedRows ()._(" enregistrement(s) ajoute(s)]")."<br />";
    }
    
    /**
     * Cette methode est appelee lors d'un traitement
     * Elle appplique un mouvement de modification sur un electeur
     * UPDATE electeur
     **/
    function modifierTraitement ($row, &$db, $DEBUG, $dateTableau) {
        // Initialisation message
        $this -> msg = "";
        // Recuperation de l'identifiant de l'electeur
        $id = $row [$this -> clePrimaire];
        
        //
        $this -> setValFTraitement ($row, $dateTableau);
        if ($this -> valF ['code_bureau'] != $row ['ancien_bureau'])
            $this -> rechercheNumeroBureau ($db, $DEBUG);
        
        // Modification ELECTEUR
        $cle = $this -> clePrimaire." = ".$id;
        
        /// Verification numero_electeur
        if (!$this -> valF['numero_electeur'] && $row['id_electeur']>=1) {
            $sql = "select numero_electeur from electeur where id_electeur=".$row['id_electeur'];
            $res2 = $db -> query ($sql);
            if (database::isError($res2))
                $this -> erreur_db ($res2 -> getDebugInfo (), $res2 -> getMessage (), '');
            $row2 =& $res2 -> fetchRow (DB_FETCHMODE_ASSOC);
            $this -> valF['numero_electeur'] = $row2['numero_electeur'];
        }
        /// Verification numero_bureau
        if ($this -> valF['numero_bureau'] >= 9000) {
                $this -> rechercheNumeroBureau ($db, $DEBUG);
            }
        
        $res = $db -> autoExecute ($this -> table, $this -> valF, DB_AUTOQUERY_UPDATE, $cle);
        if (database::isError($res))
            $this -> erreur_db ($res -> getDebugInfo (), $res -> getMessage (), '');
        $this -> msg .= _("Enregistrement ").$id._(" de la table ").$this->table." [".$db->affectedRows()._(" enregistrement mis a jour]")."<br />";
    }
    
    /**
     * Cette methode est appelee lors d'un traitement
     * Elle appplique un mouvement de radiation sur un electeur
     * Et supprime tout centrevote ou procuration concernant l'electeur
     * DELETE electeur
     **/
    function supprimerTraitement ($val, &$db, $DEBUG) {
        // Initialisation message
        $this -> msg = "";
        // Recuperation de l'identifiant de l'electeur
        $id = $val [$this -> clePrimaire];
        
        // Destruction CENTRE DE VOTE
        $sql = "delete from centrevote where id_electeur ='".$id."'";
        $res = $db->query ($sql);
        if (database::isError($res))
            $this->erreur_db ($res->getDebugInfo (), $res->getMessage (), '');
        $this->msg .= _("Enregistrement id_electeur = ").$id._(" de la table centrevote [").$db->affectedRows ()._(" enregistrement(s) supprime(s)]")."<br />";

        // Destruction MAIRIE EUROPE
        $sql = "delete from mairieeurope where id_electeur_europe ='".$id."'";
        $res = $db->query ($sql);
        if (database::isError($res))
            $this->erreur_db ($res->getDebugInfo (), $res->getMessage (), '');
        $this->msg .= _("Enregistrement id_electeur_europe = ").$id._(" de la table mairieeurope [").$db->affectedRows ()._(" enregistrement(s) supprime(s)]")."<br />";
        
        // Destruction PROCURATION
        $sql = "delete from procuration where mandant = '".$id."'";
        $res = $db->query ($sql);
        if (database::isError($res))
            $this->erreur_db ($res->getDebugInfo (), $res->getMessage (), '');
        $this->msg .= _("Enregistrement mandant = ").$id._(" de la table procuration [").$db->affectedRows ()._(" enregistrement(s) supprime(s)]")."<br />";
        
        // Destruction PROCURATION
        $sql = "delete from procuration where mandataire = '".$id."'";
        $res = $db->query ($sql);
        if (database::isError($res))
            $this->erreur_db ($res->getDebugInfo (), $res->getMessage (), '');
        $this->msg .= _("Enregistrement mandataire = ").$id._(" de la table procuration [").$db->affectedRows ()._(" enregistrement(s) supprime(s)]")."<br />";
        
        // Suppression ELECTEUR
        $cle = $this->clePrimaire." = '".$id."'";
        $sql = "delete from ".$this->table." where ".$cle;
        $res = $db->query ($sql);
        if (database::isError($res))
            $this->erreur_db ($res->getDebugInfo (), $res->getMessage (), '');
        $this->msg .= _("Enregistrement ").$id._(" de la table ").$this->table." [".$db->affectedRows()._(" enregistrement(s) supprime(s)]")."<br />";
    }

    /**
     *
     */
    function setId(&$db) {
        //
        $this->valF[$this->clePrimaire] = $db->nextId($this->table);
    }

    /**
     *
     */
    function setvalF ($val) {
        // Civilite
        $this->valF['civilite'] = $val['civilite'];
        $this->valF['nom'] = $val['nom'];
        $this->valF['sexe'] = $val['sexe'];
        $this->valF['nom_usage'] = $val['nom_usage'];
        $this->valF['prenom'] = $val['prenom'];
        $this->valF['situation'] = $val['situation'];
        // Naissance & Nationalite
        $this->valF['date_naissance'] = $val['date_naissance'];
        $this->valF['code_departement_naissance'] = $val['code_departement_naissance'];
        $this->valF['libelle_departement_naissance'] = $val['libelle_departement_naissance'];
        $this->valF['code_lieu_de_naissance'] = $val['code_lieu_de_naissance'];
        $this->valF['libelle_lieu_de_naissance'] = $val['libelle_lieu_de_naissance'];
        $this->valF['code_nationalite'] = $val[ 'code_nationalite'];
        // Adresse
        $this->valF['numero_habitation'] = $val['numero_habitation'];
        $this->valF['complement_numero'] = $val['complement_numero'];
        //
        $this->valF['code_voie'] = $val['code_voie'];
        //
        $this->valF['libelle_voie'] = $val['libelle_voie'];
        $this->valF['complement'] = $val['complement'] ;
        //????
        $this->valF['provenance'] = $val['provenance'];
        $this->valF['libelle_provenance'] = $val['libelle_provenance'];
        $this->valF['procuration'] = $val['procuration'];
        // Resident
        $this->valF['resident'] = $val['resident'];
        $this->valF['adresse_resident'] = $val['adresse_resident'];
        $this->valF['complement_resident'] = $val['complement_resident'];
        $this->valF['cp_resident'] = $val['cp_resident'];
        $this->valF['ville_resident'] = $val['ville_resident'];
        // Carte en Retour & Jury
        $this->valF['carte'] = $val['carte'];
        $this->valF['jury'] = $val['jury'];
        $this->valF['jury_effectif'] = $val['jury_effectif'];
        // suppression des info "juré" si l'électeur n'est pas juré efectif
        if ($this->valF['jury_effectif']=="oui") {
            $this->valF['date_jeffectif'] = $val['date_jeffectif'];

        } else {
            $this->valF['date_jeffectif'] = NULL;
        }
        // suppression des info "juré" si l'électeur n'est pas juré
        if ($this->valF['jury']=="1") {
            $this->valF['profession'] = $val['profession'];
            $this->valF['motif_dispense_jury'] = $val['motif_dispense_jury'];
        } else {
            $this->valF['profession'] = "";
            $this->valF['motif_dispense_jury'] = "";
        }
        
    }



    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @return void
     */
    function verifier () {
        // Initialisation de l'attribut correct a true
        $this -> correct = true;
        // Verification sur le nom
        if ($this->valF['nom']=="") {
            $this->correct=false;
            $this->msg .= "Le nom de l'electeur est obligatoire<br />";
        }
        // Verification sur le prenom uniquement si la normalisation est terminee
        if (file_exists ("../dyn/var.inc"))
            include ("../dyn/var.inc");
        if (isset($normalisation) and $normalisation) {
            if ($this -> valF ['prenom'] == "") {
                $this->correct=false;
                $this->msg .= "Le prenom de l'electeur est obligatoire<br />";
            }
        }
        // Verifications Date de naissance
        if ($this->valF['date_naissance']=="") {
            $this->correct=false;
            $this->msg .= "La date de naissance est obligatoire<br />";
        } else {
            $aujourdhui = date ("d/m/Y");
            $temp['y'] = date ('Y');
            $temp['m'] = date ('m');
            $temp['d'] = date ('d');
            $temp0 = $this -> valF ['date_naissance'];
            $temp1['y'] = substr ($temp0, 6, 4);
            $temp1['m'] = substr ($temp0, 3, 2);
            $temp1['d'] = substr ($temp0, 0, 2);
            if ($temp['m']>=$temp1['m']){
                if ($temp['m']==$temp1['m']){
                    if ($temp['d']>=$temp1['d'])
                        $age = $temp['y'] - $temp1['y'];
                    else
                        $age = $temp['y'] - $temp1['y'] - 1;
                } else
                    $age = $temp['y'] - $temp1['y'];
            } else {
                $age = $temp['y'] - $temp1['y'] - 1;
            }
            $this -> valF ['date_naissance'] = $this -> dateDB ($this -> valF ['date_naissance']);
            // controle electeur plus de 18 ans
            if ($age < 18) {
               $this->correct=false;
               $this->msg= $this->msg."L'electeur n'a pas 18 ans le ".$aujourdhui."<br />";
            }
        }
        // Gestion Resident
        if ($this -> valF ['resident'] == "Oui") {
            if ($this -> valF ['adresse_resident'] == "") {
                $this -> correct = false;
		$this -> msg .= "L'adresse de residence est obligatoire<br />";
            }
            if ($this -> valF ['cp_resident'] == "") {
                $this -> correct = false;
                $this -> msg .= "Le code postal de residence est obligatoire<br />";
            }
            if ($this -> valF ['ville_resident'] == "") {
                $this -> correct = false;
                $this -> msg .= "La ville de residence est obligatoire<br />";
            }
        }
        // Gestion Jury
        if ($this -> valF ['jury_effectif'] == "oui") {
            if ($this -> valF ['date_jeffectif'] == "") {
                $this -> correct = false;
                $this -> msg .= "Date de prefecture obligatoire<br />";
            }
            else
                $this -> valF ['date_jeffectif'] = $this -> dateDB ($this -> valF ['date_jeffectif']);
        }
        // Naissance
        if ($this->valF['code_departement_naissance']=="") {
            $this->correct=false;
            $this->msg= $this->msg."Code departement naissance obligatoire<br />";
        }
        if ($this->valF['libelle_departement_naissance']=="") {
            $this->correct=false;
            $this->msg= $this->msg."Libelle departement naissance obligatoire<br />";
        }
        if ($this->valF['code_lieu_de_naissance']=="") {
            $this->correct=false;
            $this->msg= $this->msg."Code lieu de naissance obligatoire<br />";
        }
        if ($this->valF['libelle_lieu_de_naissance']=="") {
            $this->correct=false;
            $this->msg= $this->msg."Libelle lieu de naissance obligatoire<br />";
        }
        // verification departement / code lieu de naissance
        // prise en compte des DOM 971 TOM 981 et etranger 99352 ...
        // verification sur les 2 premiers caracteres
        if (substr($this->valF['code_departement_naissance'],0,2)!=substr($this->valF['code_lieu_de_naissance'],0,2)) {
            $this->correct=false;
            $this->msg= $this->msg."<br>lieu de naissance n'est pas dans le departement de naissance";
        }
    }
    

    
    /**
     *
     */
    function retour($premier = 0, $recherche = "", $tricol = "") {
        
        echo "\n<a class=\"retour\" ";
        echo "href=\"";
        //
        if ($this->getParameter("retour") != "") {
            echo $this->getParameter("retour");
        } else {
            echo "../app/electeur.php";
            echo "?";
            echo "nom=".$this->getParameter("nom");
            echo "&amp;prenom=".$this->getParameter("prenom");
            echo "&amp;exact=".$this->getParameter("exact");
            echo "&amp;datenaissance=".$this->getParameter("datenaissance");
            echo "&amp;marital=".$this->getParameter("marital");
            echo "&amp;obj=".$this->getParameter("origin");
        }
        echo "&amp;premier=".$this->getParameter("premier");
        echo "&amp;tricol=".$this->getParameter("tricol");
        echo "&amp;recherche=".$this->getParameter("recherche");
        echo "&amp;selectioncol=".$this->getParameter("selectioncol");
        //
        echo "\"";
        echo ">";
        //
        echo _("Retour");
        //
        echo "</a>\n";
        
    }

    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType (&$form, $maj) {
        if ($maj < 2) { 
            //ajouter et modifier
            // HYPOTHESE NON MODIFICATION *******************************
            for ($i=0;$i<count($this->champs);$i++)
               $form->setType($this->champs[$i],'hiddenstatic');
            $form->setType('procuration','text');
            // *** demande adullact 01/09/2009 
            /*$form->setType('resident','select');
            $form->setType('adresse_resident','text');
            $form->setType('ville_resident','text');
            $form->setType('cp_resident','text');
            $form->setType('complement_resident','text');*/
            // ***
            $form->setType('date_naissance','hiddenstaticdate');
            $form->setType('date_modif','hiddenstaticdate');
            // **********************************************************
            // aucune information sur le  dernier mouvement
            $form->setType('tableau','hidden');
            $form->setType('date_tableau','hidden');
            $form->setType('envoi_cnen','hidden');
            $form->setType('date_cnen','hidden');
            $form->setType('mouvement','hidden');
            /*// Identification
            $form->setType('id_electeur','hiddenstatic');
            $form->setType('numero_electeur','hiddenstatic');
            $form->setType('liste','hiddenstatic');
            $form->setType('code_bureau','hiddenstatic');
            $form->setType('bureauforce','hiddenstatic');
            $form->setType('numero_bureau','hiddenstatic');
            $form->setType('date_modif','hiddenstaticdate');
            $form->setType('utilisateur','hiddenstatic');
            // Etat civil
            $form->setType('civilite','select');
            $form->setType('sexe','select');
            $form->setType('situation','select');
            // Adresse
            $form->setType('numero_habitation','hiddenstatic');
            $form->setType('code_voie','hiddenstatic');
            $form->setType('libelle_voie','hiddenstatic');
            $form->setType('complement_numero','select');
            // selection et combo
            $form->setType('provenance','comboD');
            $form->setType('libelle_provenance','comboG');
            // Naissance & Nationalite
            $form->setType('date_naissance','date');
            $form->setType('code_departement_naissance','comboD');
            $form->setType('libelle_departement_naissance','comboG');
            $form->setType('code_lieu_de_naissance','comboD');
            $form->setType('libelle_lieu_de_naissance','comboG');
            $form->setType('code_nationalite','select');
            // Resident
            $form->setType('resident', 'select');*/
            // Carte en retour & Jury
            $form->setType('carte', 'select');
            $form->setType('jury', 'select');
            $form->setType('jury_effectif', 'select');    
            $form->setType('date_jeffectif','date');
            $form->setType('profession','text');
            $form->setType('motif_dispense_jury','text');
        }
    }

    /**
     * Parametrage du formulaire - Select des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function setSelect(&$form, $maj, &$db, $DEBUG) {
        //
        require "../sql/".$db->phptype."/".$this->table.".form.inc";
        //
        if ($maj < 2) {
            // COMBO
            // code lieu de naissance
            $contenu="";
            $contenu[0][0]="commune";
            $contenu[0][1]="code";
            $contenu[1][0]="libelle_commune";
            $contenu[1][1]="libelle_lieu_de_naissance";
            $contenu[2][0]="code_departement"; // champ pour le where
            $contenu[2][1]="code_departement_naissance"; // zone du formulaire concerne
            $form->setSelect("code_lieu_de_naissance",$contenu);
            // libelle lieu de naissance
            $contenu="";
            $contenu[0][0]="commune";
            $contenu[0][1]="libelle_commune";
            $contenu[1][0]="code";
            $contenu[1][1]="code_lieu_de_naissance";
            $contenu[2][0]="code_departement"; // champ pour le where
            $contenu[2][1]="code_departement_naissance"; // zone du formulaire concerne
            $form->setSelect("libelle_lieu_de_naissance",$contenu);
            // code commune de provenance
            $contenu="";
            $contenu[0][0]="commune";// table
            $contenu[0][1]="code"; // zone origine
            $contenu[1][0]="libelle_commune";
            $contenu[1][1]="libelle_provenance";
            $form->setSelect("provenance",$contenu);
            // code commune de provenance
            $contenu="";
            $contenu[0][0]="commune";// table
            $contenu[0][1]="libelle_commune"; // zone origine
            $contenu[1][0]="code";
            $contenu[1][1]="provenance";
            $form->setSelect("libelle_provenance",$contenu);
            // code departement
            $contenu="";
            $contenu[0][0]="departement";// table
            $contenu[0][1]="code"; // zone origine
            $contenu[1][0]="libelle_departement";
            $contenu[1][1]="libelle_departement_naissance";
            $form->setSelect("code_departement_naissance",$contenu);
            // libelle departement
            $contenu="";
            $contenu[0][0]="departement";// table
            $contenu[0][1]="libelle_departement"; // zone origine
            $contenu[1][0]="code";
            $contenu[1][1]="code_departement_naissance";
            $form->setSelect("libelle_departement_naissance",$contenu);
            //DYNAMIQUE
            // nationalite
            $contenu="";
            $res = $db->query($sql_nationalite);
            if (database::isError($res))
                die($res->getMessage());
            else {
                if ($DEBUG >= VERBOSE_MODE)
                    echo " La requete ".$sql." est executee.<br>";
                $k=0;
                while ($row=& $res->fetchRow()) {
                    $contenu[0][$k]=$row[0];
                    $contenu[1][$k]=$row[1];
                    $k++;
                }
                $form->setSelect("code_nationalite",$contenu);
            }
            // STATIQUE
            // civilite
            $contenu="";
            $contenu[0]=array('M.','Mme','Mlle');
            $contenu[1]=array(_('Monsieur'),_('Madame'),_('Mademoiselle'));
            $form->setSelect("civilite",$contenu);
            // Sexe
            $contenu="";
            $contenu[0]=array('M','F');
            $contenu[1]=array(_('Masculin'),_('Feminin'));
            $form->setSelect("sexe",$contenu);
            // situation matrimoniale
            $contenu="";
            $contenu[0]=array('','C','M','V','D');
            $contenu[1]=array(_('Sans'),_('Celibataire'),_('Marie(e)'),_('Veuf(ve)'),_('Divorce(e)'));
            $form->setSelect("situation",$contenu);
			// complement
			$contenu="";
			$contenu[0]=array('','bis','ter','quater','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9');
			$contenu[1]=array(_('Sans'),_('bis'),_('ter'),_('quater'),'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9');
			$form->setSelect("complement_numero",$contenu);
            // carte
            $contenu="";
            $contenu[0]=array('0','1');
            $contenu[1]=array(_('Non'),_('Oui'));
            $form->setSelect("carte",$contenu);
            // resident
            $contenu="";
            $contenu[0]=array('Non','Oui');
            $contenu[1]=array(_('Non'),_('Oui'));
            $form->setSelect("resident",$contenu);
            // jury (selection en cours...)
            $contenu="";
            $contenu[0]=array('0','1');
            $contenu[1]=array(_('Non'),_('Oui'));
            $form->setSelect("jury",$contenu);
            // jury_effectif
            $contenu="";
            $contenu[0]=array('non','oui');
            $contenu[1]=array(_('Non'),_('Oui'));
            $form->setSelect("jury_effectif",$contenu);
        }
    }
    
    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setOnchange (&$form, $maj) {
        $form->setOnchange("nom","this.value=this.value.toUpperCase()");
        $form->setOnchange("prenom","this.value=this.value.toUpperCase()");
        $form->setOnchange("nom_usage","this.value=this.value.toUpperCase()");
        
        $form->setOnchange("numero_habitation","VerifNum(this)");
        $form->setOnchange("complement","this.value=this.value.toUpperCase()");
        
        $form->setOnchange("libelle_departement_naissance","this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle_lieu_de_naissance","this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle_provenance","this.value=this.value.toUpperCase()");
        
        $form->setOnchange("adresse_resident","this.value=this.value.toUpperCase()");
        $form->setOnchange("complement_resident","this.value=this.value.toUpperCase()");
        $form->setOnchange("cp_resident","this.value=this.value.toUpperCase()");
        $form->setOnchange("ville_resident","this.value=this.value.toUpperCase()");
        
        $form->setOnchange("civilite","changeSexe()");
        $form->setOnchange("code_departement_naissance","codecommune(this)");
        $form->setOnchange("date_naissance","fdate(this)");
        
        $form->setOnchange("date_jeffectif","fdate(this)");
    }

    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib (&$form, $maj) {
        // Identification
        $form->setLib('id_electeur',_('Id electeur no '));
        $form->setLib('numero_bureau',_(' no '));
        $form->setLib('code_bureau',_(' dans le bureau '));
        $form->setLib('bureauforce',_('Bureau force '));
        $form->setLib('numero_electeur',_(' ne '));
        $form->setLib('liste',_(' dans la liste '));
        $form->setLib('date_modif',_(' Modifie le '));
        $form->setLib('utilisateur',_(' par '));        
        //Etat Civil
        $form->setLib('nom',_('Nom'));
        $form->setLib('prenom',_('Prenom'));
        $form->setLib('civilite',_('Civilite'));
        $form->setLib('situation',_('Situation'));
        $form->setLib('sexe',_('Sexe'));
        $form->setLib('nom_usage',_("Nom d'usage"));
        // Naissance & Nationalite
        $form->setLib('date_naissance',_('Date de naissance'));
        $form->setLib('code_departement_naissance',_('Departement'));
        $form->setLib('libelle_departement_naissance',' ');
        $form->setLib('code_lieu_de_naissance',_('Lieu de naissance'));
        $form->setLib('libelle_lieu_de_naissance',' ');
        $form->setLib('code_nationalite',_('Nationalite'));
            // Adresse
        $form->setLib('numero_habitation',_('No'));
        $form->setLib('code_voie',_('Id/Libelle Voie'));
        $form->setLib('libelle_voie','');
        $form->setLib('complement_numero','');
        $form->setLib('complement','<td>'._('Complement').'</td><td></td>');
        // Resident
        $form->setLib('resident',_('Resident'));
        $form->setLib('adresse_resident',_('Adresse'));
        $form->setLib('complement_resident',_('Complement'));
        $form->setLib('cp_resident',_('Code postal'));
        $form->setLib('ville_resident',_('Ville'));
        // Provenance
        $form->setLib('provenance',_('Code/Libelle commune de provenance '));
        $form->setLib('libelle_provenance','');
        // Carte en retour & Jury
        $form->setLib('carte',_(' Carte en retour '));
        $form->setLib('jury',_(' Jure en cours '));
        $form->setLib('jury_effectif',_(' Jure effectif '));
        $form->setLib('date_jeffectif',_(' Date prefecture: '));
        $form->setLib('profession',_('Profession'));
        $form->setLib('motif_dispense_jury',_('Motif de dispense'));
        // Procuration
        $form->setLib('procuration',_('Procuration'));
    }
    
    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille (&$form, $maj) {
        $form->setTaille('nom',40);
        $form->setTaille('nom_usage',30);
        $form->setTaille('prenom',40); //pgsql
        
        $form->setTaille('code_bureau',4);
        $form->setTaille('liste',6);
        $form->setTaille('bureauforce',3);
        $form->setTaille('date_modif',10);
        $form->setTaille('utilisateur',30);
        
        $form->setTaille('libelle_commune',30);
        $form->setTaille('provenance_commune',30);
        $form->setTaille('code_departement_naissance',5);
        $form->setTaille('libelle_departement_naissance',30);
        $form->setTaille('code_lieu_de_naissance',6);  //pgsql
        $form->setTaille('libelle_lieu_de_naissance',30); //pgsql
        $form->setTaille('date_naissance',10);
        
        $form->setTaille('complement',30);
        $form->setTaille('provenance',6);
        $form->setTaille('libelle_provenance',30);
        $form->setTaille('observation',85);
        
        $form->setTaille('resident',3);
        $form->setTaille('adresse_resident',20);
        $form->setTaille('complement_resident',20);
        $form->setTaille('cp_resident',5);
        $form->setTaille('ville_resident',10);
        
        $form->setTaille('procuration',50);
        $form->setTaille('jury_effectif',3);
        $form->setTaille('date_jeffectif',10);
    }

    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax (&$form, $maj) {
        $form->setMax('nom',40);
        $form->setMax('nom_usage',40);
        $form->setMax('prenom',40);
            
        $form->setMax('code_bureau',4);
        $form->setMax('liste',2);
        $form->setMax('bureauforce',3);
        $form->setMax('date_modif',10);
        $form->setMax('utilisateur',30);
        
        $form->setMax('libelle_commune',30);
        $form->setMax('provenance_commune',30);
        $form->setMax('code_departement_naissance',5);
        $form->setMax('libelle_departement_naissance',30);
        
        $form->setMax('code_lieu_de_naissance',6);
        $form->setMax('libelle_lieu_de_naissance',30);
        
        $form->setMax('complement',80);
        $form->setMax('provenance',6);
        $form->setMax('libelle_provenance',30);
        $form->setMax('observation',100);
        
        $form->setMax('resident',3);
        $form->setMax('adresse_resident',40);
        $form->setMax('complement_resident',40);
        $form->setMax('cp_resident',5);
        $form->setMax('ville_resident',30);
        
        $form->setMax('procuration',100);
        $form->setMax('jury_effectif',3);
        $form->setMax('date_jeffectif',10);
        $form->setMax('profession',30);
        $form->setMax('motif_dispense_jury',30);
    }
    
    /**
     * Parametrage du formulaire - Groupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setGroupe (&$form, $maj) {
        // Identification
        $form->setGroupe('id_electeur','D');
        $form->setGroupe('numero_electeur','G');
        $form->setGroupe('liste','F');
        $form->setGroupe('bureauforce','D');
        $form->setGroupe('numero_bureau','G');
        $form->setGroupe('code_bureau','F');
        $form->setGroupe('date_modif','D');
        $form->setGroupe('utilisateur','F');        
        // Etat civil
        $form->setGroupe('civilite','D');
        $form->setGroupe('sexe','F');
        $form->setGroupe('nom','D');
        $form->setGroupe('nom_usage','F');
        $form->setGroupe('prenom','D');
        $form->setGroupe('situation','F');
        // Adresse
    	$form->setGroupe('numero_habitation','D');
	$form->setGroupe('code_voie','G');
	$form->setGroupe('libelle_voie','F');
	$form->setGroupe('complement_numero','D');
	$form->setGroupe('complement','F');
        // Naissance & Nationalite
        $form->setGroupe('code_departement_naissance','D');
        $form->setGroupe('libelle_departement_naissance','F');
        $form->setGroupe('code_lieu_de_naissance','D');
        $form->setGroupe('libelle_lieu_de_naissance', 'F');
        // Resident
        $form->setGroupe('resident','D');
        $form->setGroupe('adresse_resident','G');
        $form->setGroupe('complement_resident','F');
        $form->setGroupe('cp_resident','D');
        $form->setGroupe('ville_resident','F');
        // Provenance
        $form->setGroupe('provenance','D');
	$form->setGroupe('libelle_provenance','F');
        // Carte en retour & Jury
        $form->setGroupe('jury','D');
        $form->setGroupe('jury_effectif','G');
        $form->setGroupe('date_jeffectif','F');
    }

    /**
     * Parametrage du formulaire - Regroupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setRegroupe (&$form, $maj) {
        // Identification
        $form->setRegroupe('id_electeur','D',_('Identification'));
        $form->setRegroupe('numero_electeur','G','');
        $form->setRegroupe('liste','G',' ');
        $form->setRegroupe('bureauforce','G','');
        $form->setRegroupe('numero_bureau','G','');
        $form->setRegroupe('code_bureau','G','');
        $form->setRegroupe('date_modif','G','');
        $form->setRegroupe('utilisateur','F','');          
        // Etat civil
        $form->setRegroupe('civilite','D',_('Etat Civil'));
        $form->setRegroupe('sexe','G','');
        $form->setRegroupe('nom','G','');
        $form->setRegroupe('nom_usage','G','');
        $form->setRegroupe('prenom','G','');
        $form->setRegroupe('situation','F','');
        // Naissance & Nationalite
        $form->setRegroupe('date_naissance','D',_('Naissance & Nationalite'));
        $form->setRegroupe('code_departement_naissance','G','');
        $form->setRegroupe('libelle_departement_naissance','G','');
        $form->setRegroupe('code_lieu_de_naissance','G','');
        $form->setRegroupe('libelle_lieu_de_naissance','G','');
        $form->setRegroupe('code_nationalite','F','');
        // Adresse
	$form->setRegroupe('numero_habitation','D',_('Adresse'));
	$form->setRegroupe('code_voie','G','');
	$form->setRegroupe('libelle_voie','G','');
	$form->setRegroupe('complement_numero','G','');
	$form->setRegroupe('complement','F','');
	// Resident
	$form->setRegroupe('resident','D',_('Resident'));
	$form->setRegroupe('adresse_resident','G','');
	$form->setRegroupe('complement_resident','G','');
	$form->setRegroupe('cp_resident','G','');
	$form->setRegroupe('ville_resident','F','');
	// Provenance
	$form->setRegroupe('provenance','D',_('Provenance'));
	$form->setRegroupe('libelle_provenance','F','');
        // Carte en retour & Jury
        $form->setRegroupe('carte','D',_("Carte en retour & Jure d'assises"));
        $form->setRegroupe('jury','G','');
        $form->setRegroupe('jury_effectif','G','');
        $form->setRegroupe('date_jeffectif','G','');
        $form->setRegroupe('profession','G','');
        $form->setRegroupe('motif_dispense_jury','F','');
    }

    /**
     * Cette methode permet de remplir les nouvelles donnees de l'electeur
     * en fonction des donnees du mouvement represente par $row
     * 
     * @param array $row Tableau de donnees
     **/
    function setValFTraitement ($row, $dateTableau) {
        // identifiant
        $this->valF['code_bureau'] = $row['code_bureau'];
        $this->valF['bureauforce'] = $row['bureauforce'];
        $this->valF['numero_bureau'] = $row['numero_bureau']; // provisoire en inscription
        $this->valF['numero_electeur'] = $row['numero_electeur']; // vide en inscription
        // Etat civil
        $this->valF['civilite'] = $row['civilite'];
        $this->valF['sexe'] = $row['sexe'];
        $this->valF['nom'] = $row['nom'];
        $this->valF['nom_usage'] = $row['nom_usage'];
        $this->valF['prenom'] = $row['prenom'];
        $this->valF['situation'] = $row['situation'];
        $this->valF['date_naissance'] = $row['date_naissance'];
        $this->valF['code_departement_naissance'] = $row['code_departement_naissance'];
        $this->valF['libelle_departement_naissance'] = $row['libelle_departement_naissance'];
        $this->valF['code_lieu_de_naissance'] = $row['code_lieu_de_naissance'];
        $this->valF['libelle_lieu_de_naissance'] = $row['libelle_lieu_de_naissance'];
        $this->valF['code_nationalite'] = $row[ 'code_nationalite'];
        //adresse
        $this->valF['code_voie'] = $row['code_voie'];
        $this->valF['libelle_voie'] = $row['libelle_voie'];
        $this->valF['numero_habitation'] = $row['numero_habitation'];
        $this->valF['complement_numero'] = $row['complement_numero'] ;
        $this->valF['complement'] = $row['complement'] ;
        // provenance
        $this->valF['provenance'] = $row['provenance'];
        $this->valF['libelle_provenance'] = $row['libelle_provenance'];
        // adresse resident
        $this->valF['resident'] = $row['resident'];
        $this->valF['adresse_resident'] = $row['adresse_resident'];
        $this->valF['complement_resident'] = $row['complement_resident'];
        $this->valF['cp_resident'] = $row['cp_resident'] ;
        $this->valF['ville_resident'] = $row['ville_resident'] ;
        //  mouvement
        $this->valF['date_modif'] = $row['date_modif'];
        $this->valF['utilisateur']= $row['utilisateur'];
        $this->valF['mouvement']=$row['types'];
        $this->valF['date_mouvement']=$this->dateSystemeDB();
        $this->valF['typecat']="";
        //tableau
        $this->valF['date_tableau']=$dateTableau;
        //liste
        $this->valF['liste']=$row['liste'];
        // ========================================
        // mise a 0 du retour carte si modification
        // ========================================
        $this->valF['carte']=0;
        /* ++ */ $this->valF['collectivite']=$row['collectivite'];
    }

    /**
    * Cette methode recherche le dernier numero dans le bureau et pour la liste
    * pour pouvoir recuperer le numero du nouvel electeur et l'incrementer
    * dans la table numerobureau
    * UPDATE numerobureau
    */
    function rechercheNumeroBureau (&$db, $DEBUG) {
        //
        require_once "../obj/numerobureau.class.php";
        $numerobureau = new numerobureau($this->valF['collectivite'].$this->valF['liste'].$this->valF['code_bureau'], $this->db, false);
        $this->valF['numero_bureau'] = $numerobureau->nextDernierNumero();
    }

    /**
     * Cette methode recherche le dernier numero de la liste en cours
     * pour pouvoir recuperer le numero du nouvel electeur et l'incrementer
     * dans la table liste
     * UPDATE numeroliste
     */
    function rechercheNumeroElecteur (&$db, $DEBUG) {
        //
        require_once "../obj/numeroliste.class.php";
        $numeroliste = new numeroliste($this->valF['collectivite'].$this->valF['liste'], $this->db, false);
        $this->valF['numero_electeur'] = $numeroliste->nextDernierNumero();
    }

    /**
     * Cette methode recupere des informations sur l'inscription
     **/
    function informationInscription ($row) {
        // Recuperation d'informations sur l'inscription
        $this -> valF ['code_inscription'] = $row ['types'];
        $this -> valF ['date_inscription'] = $this -> dateSystemeDB ();
    }
    
}

?>
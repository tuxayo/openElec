<?php
/**
 * Ce fichier permet de definir la classe voie
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 * La classe voie represente une rue dans la commune susceptible d'etre
 * utilisee comme adresse d'un electeur
 */
class voie extends dbForm {
    
    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "voie";
    
    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "code";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "A";
    
    /**
     * Constructeur 
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function voie($id, &$db, $DEBUG) {
        //
        $this->constructeur($id, $db, $DEBUG);
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en vue de l'insertion des donnees dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setvalF($val) {
        //
        $this->valF['libelle_voie'] = $val['libelle_voie'];
        $this->valF['cp'] = $val['cp'];
        $this->valF['ville'] = $val['ville'];
        // On insere le code collectivite en fonction de la variable de session
        $this->valF['code_collectivite'] = $_SESSION['collectivite'];
    }

    /**
     *
     * @param array $val
     */
    function setValFAjout($val = array()) {
        /**
         *
         */
        //
        $this->valF[$this->clePrimaire] = $val[$this->clePrimaire];
        /**
         *
         */
        //
        if (file_exists("../dyn/var.inc")) {
            include "../dyn/var.inc";
        }
        if (isset($option_code_voie_auto) and
            $option_code_voie_auto == false){
            if($this->valF[$this->clePrimaire] == "") {
                // Recuperation de l'Id "code" du dernier enregistrement de table voie
                $id = $this->db->nextId(DB_PREFIXE."voie");
                $this->valF[$this->clePrimaire] = $id;
            }
        } else {
            // Increment de la sequence
            $id = $this->db->nextId(DB_PREFIXE."voie");
            $this->valF[$this->clePrimaire] = $id;
        }
    }
    
    /**
     *
     */
    function setVal(&$form, $maj, $validation) {
//
        if (file_exists("../dyn/var.inc")) {
            include "../dyn/var.inc";
        }
        if ($maj == 0 and
            isset($option_code_voie_auto) and
            $option_code_voie_auto == false){
            // Recuperation de l'Id "code" du dernier enregistrement de table voie
            $res = $this->db->nextId(DB_PREFIXE."voie");
            $form->setVal("code",$res);
        }
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * d'effectuer des tests d'integrite sur la cle primaire pour verifier
     * si la cle a bien ete saisie dans le formulaire et si l'objet ajoute
     * n'existe pas deja dans la table pour en empecher l'ajout.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function verifierAjout($val, &$db) {
        // Si la cle primaire est vide alors on rejette l'enregistrement
        if (trim($this->valF[$this->clePrimaire]) == "") {
            $this->correct = false;
            $this->msg .= $this->form->lib[$this->clePrimaire];
            $this->msg .= _(" est obligatoire")."<br />";
        }
        // Si le test precedent a ete reussi alors on teste si l'identifiant
        // n'existe pas deja dans la table
        if ($this->correct == true) {
            // Construction de la requete
            $sql = "select count(*) from ".$this->table." ";
            if ($this->typeCle == "A") {
                $sql .= "where ".$this->clePrimaire."='".$val[$this->clePrimaire]."'";
            } else {
                $sql .= "where ".$this->clePrimaire."=".$val[$this->clePrimaire]."";
            }
            // Execution de la requete
            $nb = $db->getone($sql);
            if (database::isError($nb)) {
                // Appel de la methode qui gere les erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            } else {
                // Si le resultat de la requete renvoit un ou plusieurs
                // enregistrements
                if ($nb > 0) {
                    // On rejette l'enregistrement
                    $this->correct = false;
                    $this->msg .= _("Il existe deja un enregistrement avec cet identifiant, vous devez en choisir un autre")."<br />";
                }
            }
        }
    }
    
    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @return void
     */
    function verifier() {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Si la valeur est vide alors on rejette l'enregistrement
        $field_required = array("libelle_voie", "cp", "ville");
        foreach($field_required as $field) {
            if (trim($this->valF[$field]) == "") {
                $this->correct = false;
                $this->msg .= $this->form->lib[$field]." ";
                $this->msg .= _("est obligatoire")."<br />";
            }
        }
    }

    /**
     *
     */
    function cleSecondaire($id, &$db = NULL, $val = array(), $DEBUG = false) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        //
        $this->rechercheTable($db, "decoupage", "code_voie", $id, $DEBUG);
        $this->rechercheTable($db, "electeur", "code_voie", $id, $DEBUG);
        $this->rechercheTable($db, "mouvement", "code_voie", $id, $DEBUG);
    }

    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType(&$form, $maj) {
        //
        if ($maj < 2) { // ajouter et modifier
            if ($maj == 1) { // modifier
                $form->setType("code", "hiddenstatic");
                $form->setType("libelle_voie", "hiddenstatic");
            } else { // ajouter
                //
                if (file_exists("../dyn/var.inc")) {
                    include "../dyn/var.inc";
                }
                //
                if (isset($option_code_voie_auto) && $option_code_voie_auto == true) {
                    $form->setType("code", "hidden");
                }
            }
        } else { // supprimer
            $form->setType("code", "hiddenstatic");
        }
    }
    
    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        $form->setLib("code", _("Identifiant"));
        $form->setLib("libelle_voie", _("Libelle de la voie"));
        $form->setLib("cp", _("Code Postal"));
        $form->setLib("ville", _("Ville"));
    }
    
    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setOnchange(&$form, $maj) {
        //
        $form->setOnchange("code", "this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle_voie", "this.value=this.value.toUpperCase()");
        $form->setOnchange("cp", "javascript:VerifNum(this)");
        $form->setOnchange("ville", "this.value=this.value.toUpperCase()");
    }
    
    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille(&$form, $maj) {
        //
        $form->setTaille("code", 6);
        $form->setTaille("libelle_voie", 50);
        $form->setTaille("cp", 5);
        $form->setTaille("ville", 50);
    }
    
    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax(&$form, $maj) {
        //
        $form->setMax("code", 6);
        $form->setMax("libelle_voie", 50);
        $form->setMax("cp", 5);
        $form->setMax("ville", 50);
    }
    
    
    /**
     * Cette methode permet d'effectuer un trigger sur l'evenement de
     * modification de l'objet. Ce trigger permet de mettre a jour les infos
     * concernant l'enregistrement modifie dans d'autres tables.
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function triggermodifierapres($id, &$db, $val, $DEBUG) {
        
        // Ajoute d'un saut de ligne pour la mise en forme du message
        $this->msg .= "<br/>";
        
        // Répercution libellé voie sur la table electeur et mouvement
        /*
        // Mise a jour de tous les libelle_voie de tous les electeurs 
        $values = array("libelle_voie" => $this->valF['libelle_voie']);
        $this->updateTable($db, "electeur", "code_voie", $id, $values, $DEBUG);
        
        // Mise a jour de tous les libelle_voie de tous les mouvements
        $values = array("libelle_voie" => $this->valF['libelle_voie']);
        $this->updateTable($db, "mouvement", "code_voie", $id, $values, $DEBUG);
        */
    }
    
}

?>
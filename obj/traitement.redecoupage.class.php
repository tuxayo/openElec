<?php
/**
 * Ce fichier declare la classe redecoupageTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class redecoupageTraitement extends traitement {
    
    var $fichier = "redecoupage";
    var $champs = array("dry_run", );
    
    function getDescription() {
        //
        return _("Ce traitement applique le decoupage en creant les mouvements ".
                 "necessaires pour que les electeurs soient dans le bureau qui ".
                 "correspond a leur adresse. Ce traitement modifie egalement ".
                 "les mouvements en cours.");
    }

    function setContentForm() {
        //
        $this->form->setLib("dry_run", _("Mode \"Essai\" - si cette case est cochee ".
                                         "alors le traitement n'est pas applique ".
                                         "seuls les resultats sont affiches."));
        $this->form->setType("dry_run", "checkbox");
    }

    function getValidButtonValue() {
        //
        return _("Redecoupage electoral");
    }
    
    function treatment () {
        //
        $this->LogToFile("start redecoupage");
        //
        (isset($_POST['dry_run']) ? $dry_run = true : $dry_run = false);
        if ($dry_run === true) {
            echo _("Le mode \"Essai\" est active. Aucune modification ne sera appliquee.");
        }
        //
        $this->LogToFile("dry_run : ".($dry_run == true ? "true" : "false")."");
        
        //
        include "../sql/".$this->page->phptype."/trt_redecoupage.inc";
        //
$DEBUG = 0;
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//            REDECOUPAGE DES BUREAUX                                        //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
if ($DEBUG == 1)
    echo "<pre>";


// Initialisation des compteurs
$cpt = "";
$cpt ['mouv_new'] = 0;
$cpt ['mouv_mod'] = 0; 
$cpt ['mouv_old'] = 0;
$cpt ['mouv_rad'] = 0;
$cpt ['mouv_ins'] = 0;
$cpt ['mouv_ins_mod'] = 0;

///////////////////////////////////////////////////////////////////////////////
// Requête: SELECT on récupère la table bureau
/* ++ */ $sql = "select * from bureau where collectivite = '".$_SESSION['collectivite']."' order by code";
$res = $this->page->db->query($sql);
if (database::isError($res))
    die($res->getMessage()."erreur ".$sql);
$bureaux = array ();
while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC))
    array_push ($bureaux, $row);
if ($DEBUG == 1)
{
    echo "<b>bureaux_</b>";
    print_r ($bureaux);
}
///////////////////////////////////////////////////////////////////////////////    

// Pour chaque bureau
foreach($bureaux as $a)
{
    ///////////////////////////////////////////////////////////////
    // Requête: SELECT on récupère la table électeur pour un bureau : séparation des traitements
    $sql = "select id_electeur, numero_electeur, liste, code_bureau, nom, nom_usage, prenom, situation, date_naissance, code_voie, libelle_voie, numero_habitation ";
    $sql .= "from electeur where liste='".$_SESSION ['liste']."' and code_bureau='".$a ['code']."' and bureauforce<>'Oui' and code_voie<>'' ";
    $res = $this->page->db->query($sql);
    if (database::isError($res))
        die($res->getMessage()."erreur ".$sql);
    $electeur = array ();
    while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC))
        array_push ($electeur, $row);
    if ($DEBUG == 1)
    {
        echo "<b>electeur_".$a ['code']."_</b>";
        print_r ($electeur);
    }
    ///////////////////////////////////////////////////////////////
        
    // Pour chaque électeur
    foreach ($electeur as $e)
    {
        ///////////////////////////////////////////////////////////////
        // Requête: SELECT on vérifie si un mouvement concerne l'électeur
        $sql = "select id, types, typecat from mouvement inner join param_mouvement on mouvement.types=param_mouvement.code ";
        /* ++ */ $sql .= "where id_electeur = '".$e['id_electeur']."' and mouvement.collectivite = '".$_SESSION['collectivite']."' and etat='actif'";
        $res = $this->page->db->query($sql);
        if (database::isError($res))
            die($res->getMessage()."erreur ".$sql);
        $row =& $res->fetchRow(DB_FETCHMODE_ASSOC);
        $mouv = $row;
        if ($DEBUG == 1)
        {
            echo "<b>mouvement_electeur_".$e['id_electeur']."_</b>";
            print_r ($mouv);
            echo "<br />";
        }
        ///////////////////////////////////////////////////////////////    
            
        // Si aucun mouvement ne concerne l'électeur
        if ($mouv['id']=="")
        {
            ///////////////////////////////////////////////////////////////
            // Requête: SELECT on récupère le code bureau correspondant à la voie de l'électeur dans la table découpage
            $sql = "select code_bureau from decoupage where code_voie='".$e['code_voie']."' and ";
            if ($e ['numero_habitation']%2 == 0)
                $sql .= "premier_pair <= ".$e ['numero_habitation']." and dernier_pair >= ".$e ['numero_habitation']."";
            else
                $sql .= "premier_impair <= ".$e ['numero_habitation']." and dernier_impair >= ".$e ['numero_habitation']."";
            
            
            $bureau = $this->page->db -> getOne ($sql);
            if ($DEBUG == 1)
            {
                echo "<b>bureau_decoupage_</b>";
                echo $bureau;
                echo "<br />";
            }
            
            // Si le code du bureau est NULL, c'est qu'il n'y a pas d'entrée
            // dans le découpage pour l'électeur en cours de traitement
            // Le traitement ne peut pas continuer
            if ($bureau == NULL) {
                // 
                $this->LogToFile("impossible de trouver le bureau d'affectation pour l'electeur : ".print_r($e, true));
                //
                $this->error = true;
                $this->addToMessage(_("Probleme dans le decoupage."));
                //
                return;
            }
            
            ///////////////////////////////////////////////////////////////
                
            // Si le code bureau de l'électeur ne correspond pas au code bureau du découpage
            if ($bureau != $e ['code_bureau'])
            {
                ///////////////////////////////////////////////////////////////
                // Requête: SELECT on récupère toutes les infos électeur
                $sql = "select * from electeur where id_electeur = '".$e['id_electeur']."'";
                $res = $this->page->db->query($sql);
                if (database::isError($res))
                    die($res->getMessage()."erreur ".$sql);
                $row=& $res->fetchRow(DB_FETCHMODE_ASSOC);
                $new_mouv = $row;
                if ($DEBUG == 1)
                {
                    echo "<b>electeur_".$e['id_electeur']."_</b>";
                    print_r ($new_mouv);
                }
                ///////////////////////////////////////////////////////////////
                    
                ///////////////////////////////////////////////////////////////
                // Requête: SELECT on récupère le dernier numero provisoire du bureau
                $sql = "select dernier_numero_provisoire from numerobureau where bureau = '".$bureau."' and liste='". $new_mouv['liste']."'";
                $new_numero_bureau = $this->page->db -> getOne ($sql);
                if ($DEBUG == 1)
                {
                    echo "<b>dernier_numero_bureau_</b>";
                    echo $new_numero_bureau;
                    echo " ";
                }
                ///////////////////////////////////////////////////////////////
                
                ///////////////////////////////////////////////////////////////
                // Requête: UPDATE on met à jour le dernier numero provisoire du bureau
                $sql = "update numerobureau set dernier_numero_provisoire = (dernier_numero_provisoire+1) where bureau = '".$bureau."' and liste='". $new_mouv['liste']."'";
                $res = $this->page->db->query($sql);
                if (database::isError($res))
                    die($res->getMessage()."erreur ".$sql);
                if ($DEBUG == 1)
                {
                    echo "OK<br />";
                }
                ///////////////////////////////////////////////////////////////
                        
                ///////////////////////////////////////////////////////////////
                // Requête: SELECT on récupère le n° de séquence de la table mouvement (incrémentation)
                $sql = "select nextval('mouvement_seq')";
                $id = $this->page->db -> getOne ($sql);
                if ($DEBUG == 1)
                {
                    echo "<b>nouveau_mouvement_</b>".$id;
                    echo " ";
                }
                ///////////////////////////////////////////////////////////////
                            
                ///////////////////////////////////////////////////////////////
                // Requête: INSERT on insère le nouveau mouvement
                $valF = array(
                    "id" => $id,
                    "etat" => 'actif',
                    "liste" => $new_mouv['liste'],
                    "types" => 'BR',
                    "id_electeur" => $new_mouv['id_electeur'],
                    "numero_electeur" => $new_mouv['numero_electeur'],
                    "code_bureau" => $bureau,
                    "bureauforce" => $new_mouv['bureauforce'],
                    "numero_bureau" => $new_numero_bureau,
                    "date_modif" => date("Y-m-d"),
                    "utilisateur" => $_SESSION ['login'],
                    "civilite" => $new_mouv['civilite'],
                    "sexe" => $new_mouv['sexe'],
                    "nom" => $new_mouv['nom'],
                    "nom_usage" => $new_mouv['nom_usage'],
                    "prenom" => $new_mouv['prenom'],
                    "situation" => $new_mouv['situation'],
                    "date_naissance" => $new_mouv['date_naissance'],
                    "code_departement_naissance" => $new_mouv['code_departement_naissance'],
                    "libelle_departement_naissance" => $new_mouv['libelle_departement_naissance'],
                    "code_lieu_de_naissance" => $new_mouv['code_lieu_de_naissance'],
                    "libelle_lieu_de_naissance" => $new_mouv['libelle_lieu_de_naissance'],
                    "code_nationalite" => $new_mouv['code_nationalite'],
                    "code_voie" => $new_mouv['code_voie'],
                    "libelle_voie" => $new_mouv['libelle_voie'],
                    "numero_habitation" => $new_mouv['numero_habitation'],
                    "complement_numero" => $new_mouv['complement_numero'],
                    "complement" => $new_mouv['complement'],
                    "provenance" => $new_mouv['provenance'],
                    "libelle_provenance" => $new_mouv['libelle_provenance'],
                    "ancien_bureau" => $new_mouv['code_bureau'],
                    "observation" => _("Modification redecoupage"),
                    "resident" => $new_mouv['resident'],
                    "adresse_resident" => $new_mouv['adresse_resident'],
                    "complement_resident" => $new_mouv['complement_resident'],
                    "cp_resident" => $new_mouv['cp_resident'],
                    "ville_resident" => $new_mouv['ville_resident'],
                    "tableau" => 'annuel',
                    "date_j5" => NULL,
                    "date_tableau" => $this->page->collectivite['datetableau'],
                    "envoi_cnen" => '',
                    "date_cnen" => NULL,
                    "collectivite" => $_SESSION['collectivite'],
                );
                $res = $this->page->db->autoExecute(DB_PREFIXE."mouvement", $valF, DB_AUTOQUERY_INSERT);
                if (database::isError($res))
                    die($res->getMessage()."erreur ".$sql);
                if ($DEBUG == 1)
                {
                    echo "OK<br />";
                }
                ///////////////////////////////////////////////////////////////
                        
                ///////////////////////////////////////////////////////////////
                // Requête: UPDATE on met à jour l'enregistrement de l'électeur pour indiquer qu'il est en modification
                $sql = "update electeur set mouvement = '".$id."', typecat = 'Modification' where id_electeur = '".$new_mouv ['id_electeur']."'";
                $res = $this->page->db->query($sql);
                if (database::isError($res))
                    die ($res->getMessage()."erreur ".$sql);
                if ($DEBUG == 1)
                {
                    echo "<b>flag_electeur</b> OK<br />";
                }
                ///////////////////////////////////////////////////////////////
                            
                // on commite les changements dans la base
                if ($dry_run === false) {
                    $this->page->db -> commit() ;
                } else {
                    $this->page->db -> rollback() ;
                }
                $cpt ['mouv_new']++;
            }
        }
        else
        {
            if ($mouv['typecat']=='Modification')
            {
                $cpt ['mouv_old']++;
                
                ///////////////////////////////////////////////////////////////
                // Requête: SELECT on récupère le numéro de voie et le nouveau bureau du mouvement
                $sql = "select code_voie, code_bureau, bureauforce, numero_habitation from mouvement where id='".$mouv['id']."'";
                $res = $this->page->db->query($sql);
                if (database::isError($res))
                    die($res->getMessage()."erreur ".$sql);
                $row=& $res->fetchRow();
                $code_voie = $row[0];
                $code_bureau = $row[1];
                $bureauforce =$row[2];
                $numero_habitation = $row[3];
                ///////////////////////////////////////////////////////////////    
                
                if ($bureauforce == 'Non')
                {
                    ///////////////////////////////////////////////////////////////
                    // Requête: SELECT on récupère le numéro du bureau correspondant au numéro de voie
                    $sql = "select code_bureau from decoupage where code_voie='".$code_voie."' and ";
                    if ($numero_habitation%2 == 0)
                        $sql .= "premier_pair <= ".$numero_habitation." and dernier_pair >= ".$numero_habitation."";
                    else
                        $sql .= "premier_impair <= ".$numero_habitation." and dernier_impair >= ".$numero_habitation."";
                    $bureau = $this->page->db -> getOne ($sql);
                    if (database::isError($bureau))
                        die($bureau->getMessage()."erreur ".$sql);
                    ///////////////////////////////////////////////////////////////    
                        
                    //
                    if ($bureau != $code_bureau)
                    {
                        ///////////////////////////////////////////////////////////////
                        // Requête: SELECT on récupère le dernier numero provisoire du bureau
                        $sql = "select dernier_numero_provisoire from numerobureau where bureau = '".$bureau."' and liste='01'";
                        $new_numero_bureau = $this->page->db -> getOne ($sql);
                        ///////////////////////////////////////////////////////////////
                        
                        ///////////////////////////////////////////////////////////////
                        // Requête: UPDATE on met à jour le dernier numero provisoire du bureau
                        $sql = "update numerobureau set dernier_numero_provisoire = (dernier_numero_provisoire+1) where bureau = '".$bureau."' and liste='01'";
                        $res = $this->page->db->query($sql);
                        if (database::isError($res))
                            die($res->getMessage()."erreur ".$sql);
                        ///////////////////////////////////////////////////////////////
                            
                        ///////////////////////////////////////////////////////////////
                        // Requête: UPDATE on met à jour la table mouvement
                        $sql = "update mouvement set code_bureau='".$bureau."', numero_bureau='".$new_numero_bureau."' where id='".$mouv['id']."'";
                        $res = $this->page->db->query($sql);
                        if (database::isError($res))
                            die($res->getMessage()."erreur ".$sql);
                        ///////////////////////////////////////////////////////////////
                            
                        // on commite les changements dans la base
                        if ($dry_run === false) {
                            $this->page->db -> commit() ;
                        } else {
                            $this->page->db -> rollback() ;
                        }
                        $cpt ['mouv_mod']++;
                    }
                }
            }
            elseif ($mouv['typecat']=='Radiation')
            {
                $cpt ['mouv_rad']++;
            }
        }
        
    }
}

// Traitement pour les inscriptions
///////////////////////////////////////////////////////////////
// Requête: SELECT on récupère les nouveau inscrits
/* ++ */ $sql = "select id, code_voie, code_bureau, bureauforce, numero_habitation from mouvement inner join param_mouvement on mouvement.types = param_mouvement.code where collectivite = '".$_SESSION['collectivite']."' and etat='actif' and typecat='Inscription'";
$res = $this->page->db->query($sql);
if (database::isError($res))
    die($res->getMessage()."erreur ".$sql);
$inscrit = array ();
while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC))
    array_push ($inscrit, $row);
///////////////////////////////////////////////////////////////
    
foreach ($inscrit as $ins)
{
    $cpt ['mouv_ins']++;
    if ($ins['bureauforce'] == 'Non')
    {
        ///////////////////////////////////////////////////////////////
        // Requête: SELECT on récupère le numéro du bureau correspondant au numéro de voie
        $sql = "select code_bureau from decoupage where code_voie='".$ins ['code_voie']."' and ";
            if ($ins['numero_habitation']%2 == 0)
                $sql .= "premier_pair <= ".$ins['numero_habitation']." and dernier_pair >= ".$ins['numero_habitation']."";
            else
                $sql .= "premier_impair <= ".$ins['numero_habitation']." and dernier_impair >= ".$ins['numero_habitation']."";

        $bureau = $this->page->db -> getOne ($sql);
        ///////////////////////////////////////////////////////////////    
        
        if ($bureau != $ins ['code_bureau'])
        {
            ///////////////////////////////////////////////////////////////
            // Requête: SELECT on récupère le dernier numero provisoire du bureau
            $sql = "select dernier_numero_provisoire from numerobureau where bureau = '".$bureau."' and liste='01'";
            $new_numero_bureau = $this->page->db -> getOne ($sql);
            ///////////////////////////////////////////////////////////////
            
            ///////////////////////////////////////////////////////////////
            // Requête: UPDATE on met à jour le dernier numero provisoire du bureau
            $sql = "update numerobureau set dernier_numero_provisoire = (dernier_numero_provisoire+1) where bureau = '".$bureau."' and liste='01'";
            $res = $this->page->db->query($sql);
            if (database::isError($res))
                die($res->getMessage()."erreur ".$sql);
            ///////////////////////////////////////////////////////////////
            
            ///////////////////////////////////////////////////////////////
            // Requête: UPDATE on met à jour la table mouvement
            $sql = "update mouvement set code_bureau='".$bureau."', numero_bureau='".$new_numero_bureau."' where id='".$ins['id']."'";
            $res = $this->page->db->query($sql);
            if (database::isError($res))
                die($res->getMessage()."erreur ".$sql);
            ///////////////////////////////////////////////////////////////
            
            // on commite les changements dans la base
            if ($dry_run === false) {
                $this->page->db -> commit() ;
            } else {
                $this->page->db -> rollback() ;
            }
            $cpt ['mouv_ins_mod']++;
        }
    }
}

// Affichage du résultat
$message =  "Le nouveau découpage a été appliqué.<br />
- ".$cpt ['mouv_new']." nouveaux mouvements correctements enregistrés,<br />
- ".$cpt ['mouv_mod']." mouvements modifiés sur ".$cpt['mouv_old']." mouvements concernés,<br />
- ".$cpt ['mouv_ins_mod']." inscriptions modifiées sur ".$cpt['mouv_ins']." inscriptions concernées,<br />
- ".$cpt ['mouv_rad']." radiations concernées non modifiées.";
$this->LogToFile($message);
$this->addToMessage($message);
///////////////////////////////////////////////////////////////////////////////
        //
        ////
        //$res = $this->page->db->query($query_update_jury);
        ////
        //if (database::isError($res, true)) {
        //    //
        //    $this->error = true;
        //    //
        //    $message = $res->getMessage()." - ".$res->getUserInfo();
        //    $this->LogToFile($message);
        //    //
        //    $this->addToMessage(_("Contactez votre administrateur."));
        //} else {
        //    //
        //    $message = $this->page->db->affectedRows()." "._("jure(s) de-selectionne(s)");
        //    $this->LogToFile($message);
        //    $this->addToMessage($message);
        //}
        ////
        $this->LogToFile("end redecoupage");
    }
}

?>

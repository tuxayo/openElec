<?php
/**
 * Ce fichier permet de definir la classe bureau
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class bureau extends dbForm {
    
    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "bureau";
    
    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "code";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "A" ;
    
    /**
     * Constructeur 
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function bureau($id, &$db, $DEBUG) {
        //
        $this->constructeur($id, $db, $DEBUG);
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en vue de l'insertion des donnees dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setvalF($val) {
        //
        $this->valF['collectivite']= $_SESSION['collectivite'];
        $this->valF['libelle_bureau'] = $val['libelle_bureau'];
        $this->valF['adresse1'] = $val['adresse1'];
        $this->valF['adresse2'] = $val['adresse2'];
        $this->valF['adresse3'] = $val['adresse3'];
        $this->valF['code_canton'] = $val['code_canton'];
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * d'effectuer des tests d'integrite sur la cle primaire pour verifier
     * si la cle a bien ete saisie dans le formulaire et si l'objet ajoute
     * n'existe pas deja dans la table pour en empecher l'ajout.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function verifierAjout($val, &$db) {
        // Si la cle primaire est vide alors on rejette l'enregistrement
        if (trim($this->valF[$this->clePrimaire]) == "") {
            $this->correct = false;
            $this->msg .= $this->form->lib[$this->clePrimaire];
            $this->msg .= _(" est obligatoire")."<br />";
        }
        // Si le test precedent a ete reussi alors on teste si l'identifiant
        // n'existe pas deja dans la table
        if ($this->correct == true) {
            // Construction de la requete
            $sql = "select count(*) from ".$this->table." ";
            if ($this->typeCle == "A") {
                $sql .= "where ".$this->clePrimaire."='".$val[$this->clePrimaire]."'";
            } else {
                $sql .= "where ".$this->clePrimaire."=".$val[$this->clePrimaire]."";
            }
            $sql .= " and collectivite ='".$_SESSION['collectivite']."' ";
            // Execution de la requete
            $nb = $db->getone($sql);
            if (database::isError($nb)) {
                // Appel de la methode qui gere les erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            } else {
                // Si le resultat de la requete renvoit un ou plusieurs
                // enregistrements
                if ($nb > 0) {
                    // On rejette l'enregistrement
                    $this->correct = false;
                    $this->msg .= _("Il existe deja un enregistrement avec cet identifiant, vous devez en choisir un autre")."<br />";
                }
            }
        }
    }
    
    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @return void
     */
    function verifier() {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Si la valeur est vide alors on rejette l'enregistrement
        $field_required = array("libelle_bureau", "code_canton");
        foreach($field_required as $field) {
            if (trim($this->valF[$field]) == "") {
                $this->correct = false;
                $this->msg .= $this->form->lib[$field]." ";
                $this->msg .= _("est obligatoire")."<br />";
            }
        }
    }

    /**
     *
     */
    function cleSecondaire($id, &$db = NULL, $val = array(), $DEBUG = false) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        //
        $selection = " and decoupage.code_voie= voie.code and voie.code_collectivite='".$_SESSION['collectivite']."' ";
        $this->rechercheTable($db, "decoupage, voie", "code_bureau", $id, $DEBUG, $selection);
        //
        $selection = " and collectivite='".$_SESSION['collectivite']."' ";
        $this->rechercheTable($db, "electeur", "code_bureau", $id, $DEBUG, $selection);
        $this->rechercheTable($db, "mouvement", "code_bureau", $id, $DEBUG, $selection);
    }

    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType(&$form, $maj) {
        //
        if ($maj < 2) { // ajouter et modifier
            $form->setType("code_canton", "select");
            if ($maj == 1) { // modifier
                $form->setType("code", "hiddenstatic");
            } else { // ajouter
            }
        } else { // supprimer
            $form->setType("code", "hiddenstatic");
            $form->setType("code_canton", "hiddenstatic");
        }
    }
    
    /**
     * Parametrage du formulaire - Select des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function setSelect(&$form, $maj, $db, $DEBUG) {
        //
        require "../sql/".$db->phptype."/".$this->table.".form.inc";
        // Select Canton - uniquement en ajout et modification
        if ($maj < 2) {
            $res = $db->query($sql_code_canton);
            if (database::isError($res)) {
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), "");
            } else {
                $contenu[0][0] = "";
                $contenu[1][0] = _("Choisir un canton");
                $k = 1;
                while ($row =& $res->fetchRow()) {
                    $contenu[0][$k] = $row[0];
                    $contenu[1][$k] = $row[1];
                    $k++;
                }
                $res->free();
                $form->setSelect("code_canton", $contenu);
            }
        }
    }
    
    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        $form->setLib("libelle_bureau", _("Libelle"));
        $form->setLib("adresse1", _("Adresse (ligne 1)"));
        $form->setLib("adresse2", _("Adresse (ligne 2)"));
        $form->setLib("adresse3", _("Adresse (ligne 3)"));
        $form->setLib("code", _("Code"));
        $form->setLib("code_canton", _("Canton"));
    }
    
    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setOnchange(&$form, $maj) {
        //
        $form->setOnchange("libelle_bureau", "this.value=this.value.toUpperCase()");
        $form->setOnchange("adresse1", "this.value=this.value.toUpperCase()");
        $form->setOnchange("adresse2", "this.value=this.value.toUpperCase()");
        $form->setOnchange("adresse3", "this.value=this.value.toUpperCase()");
        $form->setOnchange("code", "this.value=this.value.toUpperCase()");
    }
    
    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille(&$form, $maj) {
        //
        $form->setTaille("code", 4);
        $form->setTaille("code_canton", 2);
        $form->setTaille("libelle_bureau", 40);
        $form->setTaille("adresse1", 40);
        $form->setTaille("adresse2", 40);
        $form->setTaille("adresse3", 40);
    }
    
    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax(&$form, $maj) {
        //
        $form->setMax("code", 4);
        $form->setMax("code_canton", 2);
        $form->setMax("libelle_bureau", 40);
        $form->setMax("adresse1", 40);
        $form->setMax("adresse2", 40);
        $form->setMax("adresse3", 40);
    }
    
    
    /**
     * Cette méthode permet d'obtenir une chaine représentant la clause where
     * pour une requête de sélection sur la clé primaire.
     *
     * Surcharge de la méthode getCle pour ajouter la sélection sur la
     * collectivité dans la requête
     * 
     * @param string $id Valeur de la clé primaire
     * @return string Clause where
     */
    function getCle($id = "") {
        //
        $cle = parent::getCle($id);
        //
        return $cle." and collectivite = '".$_SESSION['collectivite']."' ";
    }
    
    /**
     *
     */
    function triggerajouterapres($id, $db, $val, $DEBUG) {
        //
        $this->creerNumeroBureauAutomatique($db);
        $this->updateParametrageNbJures();
    }
    
    /**
     * Permet de mettre à jour le paramétrage des jury.
     *
     * @param integer  $id    identifiant
     * @param database $db    handler dataabase
     * @param array    $val   valeurs de sortie de formulaire
     * @param integer  $DEBUG constante de debug
     */
    function triggermodifierapres($id, $db, $val, $DEBUG) {
        //
        $this->updateParametrageNbJures();
    }
    /**
     *
     */
    function triggersupprimerapres($id, $db, $val, $DEBUG) {
        //
        $this->supprimerNumeroBureauAutomatique($db, $id);
        $this->updateParametrageNbJures();
    }
    
    /**
     *
     */
    function supprimerParametrageNbJures($code_canton) {
        //
        $query = " delete from parametrage_nb_jures ";
        $query .= " where collectivite='".$_SESSION["collectivite"]."' ";
        $query .= " and canton='".$code_canton."' ";
        //
        $res = $this->db->query($query);
        $this->addToLog(
            "supprimerParametrageNbJures(): db->query(\"".$query."\");",
            VERBOSE_MODE
        );
        //
        database::isError($res);
        //
        $this->addToLog(
            "supprimerParametrageNbJures(): ".$this->db->affectedRows().
            " enregistrement(s) supprime(s)",
            EXTRA_VERBOSE_MODE
        );
    }

    /**
     * Permet de mettre à jour le paramétrage (canton) du tirage au sort des jury.
     */
    function updateParametrageNbJures() {

        // Requête de vérification de des canton de bureau
        $query = " select distinct code_canton from bureau";
        $query .= " where collectivite='".$this->valF["collectivite"]."' ";
        // Execution de la requête
        $res_list_canton = $this->db->query($query);
        // Affichage de la requête dans les log
        $this->addToLog(
            "updateParametrageNbJures(): db->getone(\"".$query."\");",
            VERBOSE_MODE
        );
        // Contrôle des erreurs
        database::isError($res_list_canton);
        $canton_bureau=array();
        while ($row_code_canton = $res_list_canton->fetchRow()) {
            $canton_bureau[] = $row_code_canton[0];
        }
        // Requête de vérification de des canton de paramétrage de jury
        $query = " select distinct canton from parametrage_nb_jures";
        $query .= " where collectivite='".$this->valF["collectivite"]."' ";
        // Execution de la requête
        $res_list_canton = $this->db->query($query);
        // Affichage de la requête dans les log
        $this->addToLog(
            "updateParametrageNbJures(): db->getone(\"".$query."\");",
            VERBOSE_MODE
        );
        // Contrôle des erreurs
        database::isError($res_list_canton);
        $canton_jury=array();
        while ($row_code_canton = $res_list_canton->fetchRow()) {
            $canton_jury[] = $row_code_canton[0];
        }
        // Comparaison entre les cantons utilisés dans le paramétrage des bureaux
        // et le paramétrage du jury
        foreach ( array_diff($canton_bureau, $canton_jury) as $code_canton) {
            $this->creerParametrageNbJures($code_canton);
        }
        foreach ( array_diff($canton_jury, $canton_bureau) as $code_canton) {
            $this->supprimerParametrageNbJures($code_canton);
        }

    }
    
    /**
     *
     */
    function creerParametrageNbJures($code_canton) {
            //
        $valF = array();
        $valF["canton"] = $code_canton;
        $valF["collectivite"] = $this->valF["collectivite"];
        //
        $res = $this->db->autoExecute('parametrage_nb_jures', $valF, DB_AUTOQUERY_INSERT);
        $this->addToLog(
            "creerParametrageNbJures(): db->autoExecute(\"parametrage_nb_jures\",
                ".print_r($valF, true).",
                DB_AUTOQUERY_INSERT);",
            VERBOSE_MODE
        );
        //
        database::isError($res);
        //
        $this->addToLog(
            "creerParametrageNbJures(): ".$this->db->affectedRows().
                " enregistrement(s) ajoute(s)",
            EXTRA_VERBOSE_MODE
        );
    }
    
    /**
     *
     */
    function supprimerNumeroBureauAutomatique(&$db, $id) {
        //
        $sql = "delete from numerobureau where bureau = '".$id."' and collectivite = '".$this->valF["collectivite"]."' ";
        $res = $db -> query ($sql);
        if (database::isError($res))
            $this -> erreur_db ($res -> getDebugInfo(), $res -> getMessage (),'');
        else
        {
            $this->msg.= "<br />".$db->affectedRows()." enregistrement(s) de la table numerobureau supprime(s) en lien avec le bureau ".$id."<br />";
        }
    }
    
    /**
     *
     */
    function creerNumeroBureauAutomatique(&$db) {
        // On recupere toutes les listes
        $sql = "select * from liste";
        //
        $res = $db->query($sql);
        //
        if (database::isError($res)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
        } else {
            // Chaque element du tableau $liste correspond a une liste
            $liste = array();
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                array_push($liste, $row);
            }
            // On boucle sur chaque liste pour ajouter une ligne par liste pour
            // ce bureau dans la table numerobureau
            foreach ($liste as $l) {
                //
                $valNB = array();
                $valNB['id']=$this->valF['collectivite'].$l['liste'].$this->valF[$this->clePrimaire];
                $valNB['bureau']=$this->valF[$this->clePrimaire];
                $valNB['collectivite']=$_SESSION['collectivite'];
                $valNB['liste']=$l['liste'];
                $valNB['dernier_numero']=0;
                $valNB['dernier_numero_provisoire']=9000;
                //
                $res = $db->autoExecute('numerobureau', $valNB, DB_AUTOQUERY_INSERT);
                //
                if (database::isError($res)) {
                    $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
                } else {
                    $this->msg=$this->msg."Enregistrement ".$valNB['id']." de la table ". 'numerobureau'." [".$db->affectedRows()." enregistrement ajoute]<br />";
                }
            }
        }
    }
    
}

?>
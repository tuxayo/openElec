<?php
/**
 * Ce fichier permet ...
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "mouvement.class.php";

/**
 *
 */
class mouvement_electeur extends mouvement {
    
    var $electeur = array();

    function setElecteurInfos() {
        
        //
        $idx_electeur = NULL;
        //
        if ($this->getParameter("idxelecteur") != NULL && $this->getParameter("idxelecteur") != "]") {
            //
            $idx_electeur = $this->getParameter("idxelecteur");
        } elseif ($this->getParameter("idx")) {
            //
            $sql = "select id_electeur from mouvement where id=".$this->getParameter("idx");
            //
            $res = $this->db->getone($sql);
            //
            $this->addToLog("mouvement_electeur->setElecteurInfos(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            //
            if (database::isError($res)) {
                // Appel de la méthode de récupération des erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            } else {
                $idx_electeur = $res;
            }
        }
        //
        if ($idx_electeur != NULL) {
            //
            $sql = "select * from electeur where id_electeur=".$idx_electeur;
            //
            $res = $this->db->query($sql);
            //
            if (database::isError($res)) {
                // Appel de la méthode de récupération des erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            } else {
                $results = array();
                while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                    array_push($results, $row);
                }
                //
                if (count($results) == 1) {
                    $this->electeur = $results[0];
                } else {
                    
                }
            }
        }
        
    }
    
    function getDataSubmit() {
        //
        $this->setElecteurInfos();
        //
        $datasubmit = parent::getDataSubmit();
        //
        $datasubmit .= "&amp;maj=".$this->getParameter("maj");
        //
        if ($this->getParameter("origin") == "ficheelecteur") {
            $datasubmit .= "&amp;idxelecteur=".$this->getParameter("idxelecteur");
            $datasubmit .= "&amp;origin=".$this->getParameter("origin");
        } elseif ($this->getParameter("nom") == "" && $this->getParameter("prenom") == "" && $this->getParameter("marital") == "" && $this->getParameter("datenaissance") == "") {
            //
        } else {
            $datasubmit .= "&amp;nom=".$this->getParameter("nom");
            $datasubmit .= "&amp;exact=".$this->getParameter("exact");
            $datasubmit .= "&amp;prenom=".$this->getParameter("prenom");
            $datasubmit .= "&amp;datenaissance=".$this->getParameter("datenaissance");
            $datasubmit .= "&amp;marital=".$this->getParameter("marital");
            $datasubmit .= "&amp;origin=".$this->getParameter("origin");
            $datasubmit .= "&amp;idxelecteur=".$this->getParameter("idxelecteur");
        }
        //
        return $datasubmit;
    }

    /**
     *
     */
    function retour($premier = 0, $recherche = "", $tricol = "") {
        
        echo "\n<a class=\"retour\" ";
        echo "href=\"";
        //
        if ($this->getParameter("origin") == "ficheelecteur") {
            echo "../app/electeur.view.php";
            echo "?";
            echo "id_electeur=".$this->getParameter("idxelecteur");
        } elseif ($this->getParameter("nom") == "" && $this->getParameter("prenom") == "" && $this->getParameter("marital") == "" && $this->getParameter("datenaissance") == "") {
            echo "../scr/tab.php";
            echo "?";
            echo "obj=".get_class($this);
        } else {
            echo "../app/electeur.php";
            echo "?";
            echo "nom=".$this->getParameter("nom");
            echo "&amp;prenom=".$this->getParameter("prenom");
            echo "&amp;exact=".$this->getParameter("exact");
            echo "&amp;datenaissance=".$this->getParameter("datenaissance");
            echo "&amp;marital=".$this->getParameter("marital");
            echo "&amp;origin=".$this->getParameter("origin");
        }
        echo "&amp;premier=".$this->getParameter("premier");
        echo "&amp;tricol=".$this->getParameter("tricol");
        echo "&amp;recherche=".$this->getParameter("recherche");
        echo "&amp;selectioncol=".$this->getParameter("selectioncol");
        //
        echo "\"";
        echo ">";
        //
        echo _("Retour");
        //
        echo "</a>\n";
        
    }

    // {{{ Gestion du verrou électeur 
    
    /**
     * Cette méthode permet de vérifier l'état du verrou electeur, c'est-à-dire
     * de vérifier si l'électeur a déjà un mouvement en cours ou non.
     * 
     * @param string $id
     * @param resource $db
     * @param array $val
     * @param boolean $mark
     */
    function isVerrouElecteur($val = array()) {
        //
        // IMPORTANT
        //
        // Ce verrou était vérifié sur le champ 'mouvement' de la table
        // electeur (si mouvement > 0 alors l'électeur était vérouillé)
        // mais le traitement de fin d'année remplit ce champ avec le code
        // du type de mouvement appliqué sur l'électeur ce qui est source
        // d'erreur. Donc désormais la vérification du verrou se fait sur le
        // champ 'typecat' de la table electeur (si typecat != "" alors
        // l'électeur est vérouillé) puisque depuis toujours les verrous
        // ont été positionnés sur ces deux champs etq ue le champ 'typecat'
        // est bien rempli à vide lors du traitement de fin d'année
        //
        //
        $verrou = NULL;
        //
        if (!isset($val['id_electeur'])) {
            // Log
            $this->addToLog(_("Les parametres ne sont pas corrects : val['id_electeur'] n'est pas defini."), DEBUG_MODE);
            //
            $verrou = NULL;
        } else {
            //
            $sql = "select mouvement, typecat from electeur ";
            $sql .= "where id_electeur=".$val['id_electeur'];
            $sql .= " and collectivite='".$_SESSION['collectivite']."' ";
            //
            $res = $this->db->query($sql);
            //
            if (database::isError($res)) {
                // Appel de la méthode de récupération des erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            } else {
                $results = array();
                while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                    array_push($results, $row);
                }
                //
                if (count($results) == 1) {
                    // L'électeur n'est pas verouillé
                    if ($results[0]['typecat'] == "") {
                        //
                        $verrou = false;
                    } else { // L'électeur est verouillé
                        //
                        $verrou = true;
                    }
                } else {
                    // Log
                    $this->addToLog(_("Probleme de coherence des donnees. Plusieurs identifiants identiques dans la table electeur."), DEBUG_MODE);
                    //
                    $verrou = NULL;
                }
            }
        }
        //
        return $verrou;
    }
    
    /**
     * Cette méthode permet de marquer l'électeur comme vérouillé avec un
     * mouvement en cours et de supprimer ce verrou. En effet un électeur ne
     * peut pas avoir plus d'un mouvement en cours donc à chaque ajout de
     * mouvement (modification ou radiation), on marque l'électeur en
     * remplissant deux champs de la table électeur et à chaque suppression
     * de mouvement (modification ou radiation) on vide ces deux champs.
     * 
     * @param string $id
     * @param resource $db
     * @param array $val
     * @param boolean $mark
     */
    function manageVerrouElecteur($id, &$db, $val, $mark = true) {
        // Initialisation du tableau
        $valF = array();
        if ($mark == true) {
            // Positionnement de l'identifiant du mouvement dans le champ mouvement
            $valF['mouvement'] = $this->valF[$this->clePrimaire];
            // Positionnement de la catégorie du mouvement dans le champ typecat
            $valF['typecat'] = $this->typeCat;
        } else {
            // Positionnement à vide dans le champ mouvement
            $valF['mouvement'] = "";
            // Positionnement à vide dans le champ typecat
            $valF['typecat'] = "";
        }
        // Construction de la clause where de la requête
        $cle = " id_electeur = ".$val['id_electeur'];
        //
        $res = $db->autoExecute(DB_PREFIXE."electeur", $valF, DB_AUTOQUERY_UPDATE, $cle);
        //
        if (database::isError($res)) {
            // Appel de la méthode de récupération des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
        } else {
            //
            $this->addToLog("Requête exécutée", VERBOSE_MODE);
            //
            $message = _("Enregistrement")."&nbsp;".$val['id_electeur']."&nbsp;";
            $message .= _("de la table")."&nbsp;\"electeur\"&nbsp;";
            $message .= "[&nbsp;".$db->affectedRows()."&nbsp;";
            $message .= _("enregistrement(s) mis a jour")."&nbsp;]<br/>";
            $this->addToLog($message, VERBOSE_MODE);
            //
            if ($mark == true) {
                $this->addToLog(_("Un verrou a ete positionne sur l'electeur a ete positionne correctement."), VERBOSE_MODE);
            } else {
                $this->addToLog(_("Le verrou sur l'electeur a ete supprime correctement."), VERBOSE_MODE);
            }
        }
    }
    
    // }}}

}

?>

<?php
/**
 * Ce fichier declare la classe juryEpurationTraitement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class juryEpurationTraitement extends traitement {
    
    var $fichier = "jury_epuration";
    
    function getDescription() {
        //
        return _("Tous les electeurs etant marques comme jures d'assises ne le".
                 "seront plus. Ce traitement se fait uniquement sur la liste ".
                 "en cours.");
    }
    
    function getValidButtonValue() {
        //
        return _("Epuration des jures d'assises");
    }
    
    function displayBeforeContentForm() {
        //
        include "../sql/".$this->page->phptype."/trt_jury_epuration.inc";
        //
        $nb_jury = $this->page->db->getone($query_count_jury);
        $this->page->isDatabaseError($nb_jury);
        //
        echo "\n<p>";
        echo _("Le nombre d'electeurs etant marques comme jures d'assise a la date du")." ";
        echo date('d/m/Y')." "._("est de")." ".$nb_jury.".";
        echo "</p>\n";
    }
    
    function treatment () {
        //
        $this->LogToFile("start jury_epuration");
        //
        include "../sql/".$this->page->phptype."/trt_jury_epuration.inc";
        //
        $res = $this->page->db->query($query_update_jury);
        //
        if (database::isError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(_("Contactez votre administrateur."));
        } else {
            //
            $message = $this->page->db->affectedRows()." "._("jure(s) de-selectionne(s)");
            $this->LogToFile($message);
            $this->addToMessage($message);
        }
        //
        $this->LogToFile("end jury_epuration");
    }
}

?>

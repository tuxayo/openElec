<?php
/**
 * Ce fichier permet de definir la classe decoupage
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class decoupage extends dbForm {
    
    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "decoupage";
    
    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "id";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "N";
    
    /**
     * Constructeur 
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function decoupage($id, &$db, $DEBUG) {
        //
        $this->constructeur($id, $db, $DEBUG);
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en vue de l'insertion des donnees dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setvalF($val) {
        //
        $this->valF['code_voie'] = $val['code_voie'];
        $this->valF['code_bureau'] = $val['code_bureau'];
        $this->valF['premier_impair'] = $val['premier_impair'];
        $this->valF['dernier_impair'] = $val['dernier_impair'];
        $this->valF['premier_pair'] = $val['premier_pair'];
        $this->valF['dernier_pair'] = $val['dernier_pair'];
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en ajouter la cle primaire en vue de l'insertion des donnees
     * dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setValFAjout($val) {
        // Id automatique donc cette initialisation n'est pas necessaire
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * de recuperer l'element suivant dans la sequence pour les id automatiques.
     * 
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function setId(&$db) {
        //
        $this->valF[$this->clePrimaire] = $db->nextId($this->table);
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * d'effectuer des tests d'integrite sur la cle primaire pour verifier
     * si la cle a bien ete saisie dans le formulaire et si l'objet ajoute
     * n'existe pas deja dans la table pour en empecher l'ajout.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function verifierAjout($val, &$db) {
        // Id automatique donc cette verification n'est pas necessaire
    }
    
    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @return void
     */
    function verifier() {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Si la valeur est vide alors on rejette l'enregistrement
        $field_required = array("code_voie", "code_bureau", "premier_impair",
                                "dernier_impair", "premier_pair", "dernier_pair");
        foreach($field_required as $field) {
            if (trim($this->valF[$field]) == "") {
                $this->correct = false;
                $this->msg .= $this->form->lib[$field]." ";
                $this->msg .= _("est obligatoire")."<br />";
            }
        }
        if ($this->valF["premier_impair"]%2 == 0) {
            $this->correct = false;
            $this->msg .= _("Le champ")." "._("premier_impair")." "._("doit être impair")."<br />";
        }
        if ($this->valF["dernier_impair"]%2 == 0) {
            $this->correct = false;
            $this->msg .= _("Le champ")." "._("dernier_impair")." "._("doit être impair")."<br />";
        }
        if ($this->valF["premier_pair"]%2 == 1) {
            $this->correct = false;
            $this->msg .= _("Le champ")." "._("premier_pair")." "._("doit être pair")."<br />";
        }
        if ($this->valF["dernier_pair"]%2 == 1) {
            $this->correct = false;
            $this->msg .= _("Le champ")." "._("dernier_pair")." "._("doit être pair")."<br />";
        }
    }
    
    /**
     * Parametrage du formulaire - Valeurs pre-remplies
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param integer $validation 
     * 
     * @return void
     */
    function setVal(&$form, $maj, $validation) {
        //
        if ($validation == 0 and $maj == 0) {
            $form->setVal("premier_impair", 1);
            $form->setVal("dernier_impair", 90001);
            $form->setVal("premier_pair", 0);
            $form->setVal("dernier_pair", 90000);
        }
    }
    
    /**
     * Parametrage du formulaire - Groupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setGroupe(&$form, $maj) {
        //
        $form->setGroupe("premier_impair", "D");
        $form->setGroupe("dernier_impair", "F");
        $form->setGroupe("premier_pair", "D");
        $form->setGroupe("dernier_pair", "F");
    }
    
    /**
     * Parametrage du formulaire - Regroupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setRegroupe(&$form, $maj) {
        //
        $form->setRegroupe("id", "D", _("Identification"));
        $form->setRegroupe("code_voie", "G", "");
        $form->setRegroupe("code_bureau", "F", "");
        //
        $form->setRegroupe("premier_impair", "D", _("Positionnement"));
        $form->setRegroupe("dernier_impair", "G", "");
        $form->setRegroupe("premier_pair", "G", "");
        $form->setRegroupe("dernier_pair", "F", "");
    }
    
    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType(&$form, $maj) {
        //
        if ($maj < 2) { // ajouter et modifier
            $form->setType("code_voie", "textreadonly");
            $form->setType("code_bureau", "select");
            if ($maj == 1) { // modifier
                $form->setType("id", "hiddenstatic");
            } else { // ajouter
                $form->setType("id", "hidden");
            }
        } else { // supprimer
            $form->setType("id", "hiddenstatic");
            $form->setType("code_voie", "hiddenstatic"); //conserver pour retour
        }
    }
    
    /**
     * Parametrage du formulaire - Select des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function setSelect(&$form, $maj, $db, $DEBUG) {
        //
        require "../sql/".$db->phptype."/".$this->table.".form.inc";
        // Select Bureau - uniquement en ajout et modification
        if($maj < 2) {
            $res = $db->query($sql_bureau);
            if (database::isError($res)) {
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), "");
            } else {
                $contenu[0][0] = "";
                $contenu[1][0] = _("Choisir un bureau");
                $k = 1;
                while ($row =& $res->fetchRow()) {
                    $contenu[0][$k] = $row[0];
                    $contenu[1][$k] = $row[1];
                    $k++;
                }
                $form->setSelect("code_bureau", $contenu);
            }
        }
    }
    
    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        $form->setLib("code_bureau", _("Bureau"));
        $form->setLib("premier_impair", _("Premier numero impair"));
        $form->setLib("dernier_impair", _("Dernier numero impair"));
        $form->setLib("premier_pair", _("Premier numero pair"));
        $form->setLib("dernier_pair", _("Dernier numero pair"));
        $form->setLib("code_voie", _("Voie"));
    }
    
    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setOnchange(&$form, $maj) {
        //
        $form->setOnchange("premier_impair", "VerifNumDecoupage(this)");
        $form->setOnchange("dernier_impair", "VerifNumDecoupage(this)");
        $form->setOnchange("premier_pair", "VerifNumDecoupage(this)");
        $form->setOnchange("dernier_pair", "VerifNumDecoupage(this)");
    }
    
    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille(&$form, $maj) {
        //
        $form->setTaille("code_bureau", 30);
        $form->setTaille("premier_impair", 5);
        $form->setTaille("dernier_impair", 5);
        $form->setTaille("premier_pair", 5);
        $form->setTaille("dernier_pair", 5);
        $form->setTaille("code_voie", 10);
    }
    
    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax(&$form, $maj) {
        //
        $form->setMax("code_bureau", 30);
        $form->setMax("premier_impair", 5);
        $form->setMax("dernier_impair", 5);
        $form->setMax("premier_pair", 5);
        $form->setMax("dernier_pair", 5);
        $form->setMax("code_voie", 10);
    }
    
    /**
     * Parametrage du sous-formulaire - Valeurs pre-remplies
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param
     * @param
     * @param
     * @param
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db, $DEBUG = NULL) {
        //
        $this->retourformulaire = $retourformulaire;
        //
        if ($validation == 0) {
            if ($retourformulaire == "voie") {
                $form->setVal("premier_impair", 1);
                $form->setVal("dernier_impair", 90001);
                $form->setVal("premier_pair", 0);
                $form->setVal("dernier_pair", 90000);
                $form->setVal("code_voie", $idxformulaire);
            }
        }
    }
    
}

?>
<?php

require_once ("../obj/traitement.class.php");

class annuelTraitement extends traitement {
    
    var $fichier = "annuel";
    
    var $champs = array("liste", );

    function getValidButtonValue() {
        //
        return _("Appliquer le traitement annuel");
    }
    
    function setContentForm() {        
        //
        $this->form->setLib("liste", _("Le traitement annuel s'applique sur la liste :"));
        $this->form->setType("liste", "statiq");
        $this->form->setVal("liste", $_SESSION["liste"]." - ".$_SESSION["libelle_liste"]);
    }

    function displayAfterContentForm() {

        // Format Date de tableau
        $datetableau = $this->page->collectivite['datetableau'];
        
        // Inclusion du fichier de requêtes
        include ("../sql/".$this->page->phptype."/trt_annuel.inc");

        //
        echo "<h4>";
        echo _("Recapitulatif du traitement :");
        echo "</h4>";

        // Debut Tableau
        echo "\n<table class='tabCol'>\n";
        
        // NB electeur avant traitement
        $nbElecteur = $this->page->db->getone ($query_count_electeur);
        $this->page->isDatabaseError($nbElecteur);
        echo "\t<tr class='tabCol'>";
        echo "<td class=\"link\" rowspan=\"5\">";
        echo "<div class=\"choice ui-corner-all ui-widget-content\">";
        echo "<span>";
        echo "<a class=\"om-prev-icon edition-16\" ";
        echo "title=\"Ce document contient le detail par bureau des mouvements, le listing des ";
        echo "inscriptions, des modifications et des radiations a appliquer lors du traitement annuel. ";
        echo "Une fois le traitement applique, il est possible de retrouver ce recapitulatif dans le menu 'Edition -> Revision Electorale'.\" ";
        echo "target=\"_blank\" href=\"../app/pdf_recapitulatif_traitement.php?action=traitement&amp;traitement=annuel\">";
        echo "Cliquer ici pour visualiser le recapitulatif complet du traitement";
        echo "</a>";
        echo "</span>";
        echo "</div>";
        echo "</td>";
        echo "<td class=\"libelle\">Nombre d'electeurs avant traitement annuel</td>";
        echo "<td class=\"total\">".$nbElecteur."</td>";
        echo "</tr>\n";
        
        //  NB inscription
        $nbInscription = $this->page->db->getone ($query_count_inscription);
        $this->page->isDatabaseError($nbInscription);
        echo "\t<tr class='tabData'>";
        echo "<td class=\"libelle nb-electeur\">Inscription(s)</td>";
        echo "<td class=\"total\">".$nbInscription."</td>";
        echo "</tr>\n";
        
        // NB modification
        $nbModification = $this->page->db->getone ($query_count_modification);
        $this->page->isDatabaseError($nbModification);
        echo "\t<tr class='tabData'>";
        echo "<td class=\"libelle\">Modification(s)</td>";
        echo "<td class=\"total\">".$nbModification."</td>";
        echo "</tr>\n";

        // NB radiation
        $nbRadiation = $this->page->db->getone ($query_count_radiation);
        $this->page->isDatabaseError($nbRadiation);
        echo "\t<tr class='tabData'>";
        echo "<td class=\"libelle\">Radiation(s)</td>";
        echo "<td class=\"total\">".$nbRadiation."</td>";
        echo "</tr>\n";
        
        // NB electeur apres traitement
        $nbElecteurApres = $nbElecteur + $nbInscription - $nbRadiation;
        echo "\t<tr class='tabCol'>";
        echo "<td class=\"libelle\">Nombre d'electeurs apres traitement annuel</td>";
        echo "<td class=\"total\">".$nbElecteurApres."</td>";
        echo "</tr>\n";
        echo "</table>";

    }
    
    function treatment () {
        //
        $this->LogToFile ("start annuel");
        
        //
        $datetableau = $this->page->collectivite['datetableau'];
        
        //
        require_once ("../obj/electeur.class.php");
        include ("../sql/".$this->page->phptype."/trt_annuel.inc");
        
        // Tableau des inscriptions/modifications/radiations
        $mouvements = array(
            "inscriptions" => array(),
            "modifications" => array(),
            "radiations" => array(),
            );
		// Traitement RADIATIONS
		$res_select_radiation = $this->page->db->query ($query_select_radiation);
		if (database::isError($res_select_radiation)) {
            //
            $this->error = true;
            //
            $message = $res_select_radiation->getMessage ()." erreur sur ".$query_select_radiation."";
            $this->LogToFile ($message);
        } else {
            $this->LogToFile ("TRAITEMENT DES RADIATIONS");
            while ($row =& $res_select_radiation->fetchRow (DB_FETCHMODE_ASSOC)) {
                $mouvements["radiations"][] = $row['id_electeur'];
                // suppression ELECTEUR
                $enr = new electeur ($row['id_electeur'], $this->page->db, 0);
                $enr->supprimerTraitement ($row, $this->page->db, 0) ;
                $message = "-> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom']." - ".$enr->msg."";
                $this->LogToFile ($message);
                // maj MOUVEMENT
                $fields_values = array(
                    'etat'    => 'trs',
                    'tableau' => 'annuel',
                    'date_j5' => ''.$enr->dateSystemeDB ().'');
                $res1 = $this->page->db->autoExecute ("mouvement", $fields_values, DB_AUTOQUERY_UPDATE, "id=".$row['id']);
                if (database::isError($res1)) {
                    //
                    $this->error = true;
                    //
                    $message = $res1->getMessage ()." - ".$res1->getUserInfo ();
                    $this->LogToFile ($message);
                    //
                    break;
                } else {
                    //
                    $message = "l'enregistrement ".$row['id']." de la table Mouvement est modifie";
                    $this->LogToFile ($message);
                }
            }
            $res_select_radiation->free ();
        }
        
		// Traitement INSCRIPTIONS
		$res_select_inscription = $this->page->db->query ($query_select_inscription);
		if (database::isError($res_select_inscription)) {
            //
            $this->error = true;
            //
            $message = $res_select_inscription->getMessage ()." erreur sur ".$query_select_inscription."";
            $this->LogToFile ($message);
        } else {
            $this->LogToFile ("TRAITEMENT DES INSCRIPTIONS");
            while ($row =& $res_select_inscription->fetchRow (DB_FETCHMODE_ASSOC)) {
                // ajout ELECTEUR
                $enr = new electeur ("]", $this->page->db, 0);
                $enr->valF['tableau'] = 'annuel';
                $enr->ajouterTraitement ($row, $this->page->db, 0, $this->page->collectivite['datetableau']);
                $message = "-> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom']." - ".$enr->msg."";
                $this->LogToFile ($message);
                // maj MOUVEMENT
                $fields_values = array(
                    'etat'    => 'trs',
                    'tableau' => 'annuel',
                    'id_electeur' => $enr->valF["id_electeur"],
                    'date_j5' => ''.$enr->dateSystemeDB ().'');
                $res1 = $this->page->db->autoExecute ("mouvement", $fields_values, DB_AUTOQUERY_UPDATE, "id=".$row['id']);
                $mouvements["inscriptions"][] = $enr->valF["id_electeur"];
                if (database::isError($res1)) {
                    //
                    $this->error = true;
                    //
                    $message = $res1->getMessage ()." - ".$res1->getUserInfo ();
                    $this->LogToFile ($message);
                    //
                    break;
                } else {
                    //
                    $message = "l'enregistrement ".$row['id']." de la table Mouvement est modifie";
                    $this->LogToFile ($message);
                }
            }
            $res_select_inscription->free ();
        }
	
		// Traitement MODIFICATIONS
		$res_select_modification = $this->page->db->query ($query_select_modification);
		if (database::isError($res_select_modification)) {
            //
            $this->error = true;
            //
            $message = $res_select_modification->getMessage ()." erreur sur ".$query_select_modification."";
            $this->LogToFile ($message);
        } else {
            $this->LogToFile ("TRAITEMENT DES MODIFICATIONS");
            while ($row =& $res_select_modification->fetchRow (DB_FETCHMODE_ASSOC)) {
                $mouvements["modifications"][] = $row['id_electeur'];
                // maj ELECTEUR
                $enr = new electeur($row['id_electeur'], $this->page->db, 0);
                $enr->valF['tableau'] = 'annuel';
                $enr-> modifierTraitement ($row, $this->page->db, 0, $this->page->collectivite['datetableau']) ;
                $message = "-> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom']." - ".$enr->msg."";
                $this->LogToFile ($message);
                // maj MOUVEMENT
                $fields_values = array(
                    'etat'    => 'trs',
                    'tableau' => 'annuel',
                    'date_j5' => ''.$enr->dateSystemeDB ().'');
                $res1 = $this->page->db->autoExecute ("mouvement", $fields_values, DB_AUTOQUERY_UPDATE, "id=".$row['id']);
                if (database::isError($res1)) {
                    //
                    $this->error = true;
                    //
                    $message = $res1->getMessage ()." - ".$res1->getUserInfo ();
                    $this->LogToFile ($message);
                    //
                    break;
                } else {
                    //
                    $message = "l'enregistrement ".$row['id']." de la table Mouvement est modifie";
                    $this->LogToFile ($message);
                }
            }
            $res_select_modification->free ();
        }
        
        $this->trigger_after_treatment($mouvements);
        //
        $this->LogToFile ("end annuel");
    }


    private function trigger_after_treatment($mouvements) {

    }
}

?>
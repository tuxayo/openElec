<?php
/**
 * Ce fichier permet de definir la classe inscription_office
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class inscription_office extends dbForm {

    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "inscription_office";

    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "id";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "N";
    
    /**
     * Constructeur 
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function inscription_office($id,&$db,$DEBUG) {
        $this->constructeur($id,$db,$DEBUG);
    }

    /**
     *
     */
    function ajouter($val = array(), &$db = NULL, $DEBUG = false) {
        //
        $this->correct = false;
        $this->addToMessage(_("L'ajout d'inscription d'office est impossible. Vous devez proceder a un traitement d'import INSEE.")."<br/>");
    }

    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en vue de l'insertion des donnees dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */   
    function setvalF($val) {
        // zones autorisees en modification sans mouvement
        // Etat civil
        $this->valF['civilite'] = $val['civilite'];
        $this->valF['nom'] = $val['nom'];
        $this->valF['sexe'] = $val['sexe'];
        $this->valF['nom_usage'] = $val['nom_usage'];
        $this->valF['prenom'] = $val['prenom'];
        $this->valF['types'] = $val['types'];
        // naissance
        $this->valF['date_naissance'] = $val['date_naissance'];
        $this->valF['code_departement_naissance'] = $val['code_departement_naissance'];
        $this->valF['libelle_departement_naissance'] = $val['libelle_departement_naissance'];
        $this->valF['code_lieu_de_naissance'] = $val['code_lieu_de_naissance'];
        $this->valF['libelle_lieu_de_naissance'] = $val['libelle_lieu_de_naissance'];
        // adresse resident
        $this->valF['adresse1'] = $val['adresse1'];
        $this->valF['adresse2'] = $val['adresse2'];
        $this->valF['adresse3'] = $val['adresse3'];
        $this->valF['adresse4'] = $val['adresse4'];
        $this->valF['adresse5'] = $val['adresse5'];
        $this->valF['adresse6'] = $val['adresse6'];
        // mention et carte retournee
        $this->valF['idcnen'] = $val['idcnen'];
        // $this->valF['type'] = $val['type'];
    }

    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en ajouter la cle primaire en vue de l'insertion des donnees
     * dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setValFAjout($val) {
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * de recuperer l'element suivant dans la sequence pour les id automatiques.
     * 
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function setId(&$db) {
        // id automatique nextid
        $this->valF['id'] = $db->nextId($this->table);
    }

    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * d'effectuer des tests d'integrite sur la cle primaire pour verifier
     * si la cle a bien ete saisie dans le formulaire et si l'objet ajoute
     * n'existe pas deja dans la table pour en empecher l'ajout.
     * 
     * @return void
     */
    function verifierAjout() {
    }


    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function verifier() {
        $this->correct=True;
        // zones obligatoires
        //nom
        if ($this->valF['nom']=="") {
           $this->correct=false;
           $this->msg .= _("zone nom obligatoire")."<br />";
        } 
        // Controle date de naissance
        if ($this->valF['date_naissance']=="") {
           $this->correct=false;
           $this->msg .= _("date de naissance obligatoire")."<br />";
           $temp="";
        } else {
            $temp=$this->valF['date_naissance'];
            $temp1=date('d/m/Y');
            $this->valF['date_naissance']=$this->dateDB($this->valF['date_naissance']);
        }
        // controle electeur plus de 18 ans
        $age= $this->anneePHP($temp1)- $this->anneePHP($temp);
        if ($age<18) {
           $this->correct=false;
           $this->msg .= _("l'electeur n' a pas  18 ans le ").$temp1."<br />";
        }
        //
        if ($this->valF['code_departement_naissance']=="") {
           $this->correct=false;
           $this->msg .= _("code departement naissance obligatoire")."<br />";
        }
        if ($this->valF['libelle_departement_naissance']=="") {
           $this->correct=false;
           $this->msg .= _("libelle departement naissance obligatoire")."<br />";
        }
        if ($this->valF['code_lieu_de_naissance']=="") {
           $this->correct=false;
           $this->msg .= _("code lieu de naissance obligatoire")."<br />";
        }
        if ($this->valF['libelle_lieu_de_naissance']=="") {
           $this->correct=false;
           $this->msg .= _("libelle lieu de naissance obligatoire")."<br />";
        }
        // verification departement / code lieu de naissance
        // prise en compte des DOM 971 TOM 981 et etranger 99352 ...
        // verification sur les 2 premiers caracteres
        if (substr($this->valF['code_departement_naissance'],0,2)!=substr($this->valF['code_lieu_de_naissance'],0,2)) {
           $this->correct=false;
           $this->msg .= _("lieu de naissance n'est pas dans le departement de naissance")."<br />";
        }
    }
    
    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType(&$form,$maj) {
        if ($maj < 2) {
            //ajouter et modifier
            // pas de modification affectation liste bureau et numero electeur et dans le bureau
            $form->setType('id','hiddenstatic');
            $form->setType('type','hiddenstatic');
            $form->setType('cog','hiddenstatic');
            // selection et combo
            $form->setType('civilite','select');
            $form->setType('sexe','select');
            $form->setType('situation','select');
            $form->setType('date_naissance','date');
            $form->setType('code_departement_naissance','comboD');
            $form->setType('libelle_departement_naissance','comboG');
            $form->setType('code_lieu_de_naissance','comboD');
            $form->setType('libelle_lieu_de_naissance','comboG');
        } else {
            $form->setType('id','hiddenstatic');
        }
    }

    /**
     * Parametrage du formulaire - Select des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function setSelect(&$form, $maj, &$db, $DEBUG) {
        //
        require "../sql/".$db->phptype."/".$this->table.".form.inc";
        //
        if ($maj < 2) {
            // correl
            // code et libelle lieu de naissance
            $contenu="";
            $contenu[0][0]="commune";
            $contenu[0][1]="code";
            $contenu[1][0]="libelle_commune";
            $contenu[1][1]="libelle_lieu_de_naissance";
            $contenu[2][0]="code_departement"; // champ pour le where
            $contenu[2][1]="code_departement_naissance"; // zone du formulaire concerne
            $form->setSelect("code_lieu_de_naissance",$contenu);
            $contenu="";
            $contenu[0][0]="commune";
            $contenu[0][1]="libelle_commune";
            $contenu[1][0]="code";
            $contenu[1][1]="code_lieu_de_naissance";
            $contenu[2][0]="code_departement"; // champ pour le where
            $contenu[2][1]="code_departement_naissance"; // zone du formulaire concerne
            $form->setSelect("libelle_lieu_de_naissance",$contenu);
            // code et libelle departement
            $contenu="";
            $contenu[0][0]="departement";// table
            $contenu[0][1]="code"; // zone origine
            $contenu[1][0]="libelle_departement";
            $contenu[1][1]="libelle_departement_naissance";
            $form->setSelect("code_departement_naissance",$contenu);
            $contenu="";
            $contenu[0][0]="departement";// table
            $contenu[0][1]="libelle_departement"; // zone origine
            $contenu[1][0]="code";
            $contenu[1][1]="code_departement_naissance";
            $form->setSelect("libelle_departement_naissance",$contenu);
            // civilite
            $contenu="";
            $contenu[0]=array('M.','Mme','Mlle');
            $contenu[1]=array(_('Monsieur'),_('Madame'),_('Mademoiselle'));
            $form->setSelect("civilite",$contenu);
            // Sexe
            $contenu="";
            $contenu[0]=array('M','F');
            $contenu[1]=array(_('Masculin'),_('Feminin'));
            $form->setSelect("sexe",$contenu);
        } // maj<2
    }

    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setOnchange(&$form,$maj) {
        $form->setOnchange("nom","this.value=this.value.toUpperCase()");
        $form->setOnchange("prenom","this.value=this.value.toUpperCase()");
        $form->setOnchange("nom_usage","this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle_departement_naissance","this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle_lieu_de_naissance","this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle_provenance","this.value=this.value.toUpperCase()");
        $form->setOnchange("civilite","changeSexe()");
        $form->setOnchange("code_departement_naissance","codecommune(this)");
        $form->setOnchange("date_naissance","fdate(this)");
    }

    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib (&$form, $maj) {
        $form->setLib('id',_(' Identifiant: '));
        $form->setLib('types',_(' type inscription: '));
        $form->setLib('sexe',_(' [sexe:] '));
        $form->setLib('code_departement_naissance',_('Departement : '));
        $form->setLib('code_lieu_de_naissance',_('Lieu Naissance : '));
        $form->setLib('nom_usage',_(' [usage :] '));
        $form->setLib('date_naissance',_('Naissance : '));
        $form->setLib('libelle_lieu_de_naissance','');
        $form->setLib('libelle_departement_naissance','');
    }
    
    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille(&$form,$maj) {
        $form->setTaille('nom',63);
        $form->setTaille('nom_usage',63);
        $form->setTaille('prenom',50);//pgsql
        $form->setTaille('types',2);
        $form->setTaille('libelle_commune',30);
        $form->setTaille('code_departement_naissance',5);
        $form->setTaille('libelle_departement_naissance',30);
        $form->setTaille('code_lieu_de_naissance',6);  //pgsql
        $form->setTaille('libelle_lieu_de_naissance',30); //pgsql
        $form->setTaille('date_naissance',14);
        $form->setTaille('idcnen',9);
        $form->setTaille('adresse1',38);
        $form->setTaille('adresse2',38);
        $form->setTaille('adresse3',38);
        $form->setTaille('adresse4',38);
        $form->setTaille('adresse5',38);
        $form->setTaille('adresse6',38);
    }

    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax(&$form,$maj) {
        // compatibilite postgre ===============================
        $form->setMax('nom',63);
        $form->setMax('nom_usage',63);
        $form->setMax('prenom',50);
        $form->setMax('types',2);
        $form->setMax('libelle_commune',30);
        $form->setMax('code_departement_naissance',5);
        $form->setMax('libelle_departement_naissance',30);
        $form->setMax('code_lieu_de_naissance',6);
        $form->setMax('libelle_lieu_de_naissance',30);
        $form->setMax('adresse1',38);
        $form->setMax('adresse2',38);
        $form->setMax('adresse3',38);
        $form->setMax('adresse4',38);
        $form->setMax('adresse5',38);
        $form->setMax('adresse6',38);
        $form->setMax('idcnen',9);
    }

    /**
     * Parametrage du formulaire - Groupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setGroupe (&$form, $maj) {
        $form->setGroupe('civilite','D');
        $form->setGroupe('sexe','F');
        $form->setGroupe('code_lieu_de_naissance','D');
        $form->setGroupe('libelle_lieu_de_naissance','F');
        $form->setGroupe('date_naissance','D');
        $form->setGroupe('code_departement_naissance','G');
        $form->setGroupe('libelle_departement_naissance','F');
        $form->setGroupe('code_lieu_de_naissance','D');
        $form->setGroupe('libelle_lieu_de_naissance', 'F');
    } 
    
}
?>
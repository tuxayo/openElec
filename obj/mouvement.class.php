<?php
/**
 * Ce fichier permet de definir la classe mouvement
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class mouvement extends dbForm {

    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "mouvement";
    
    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "id";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "N" ;
    
    /**
     *
     * @var string Chaine representant la date de tableau en cours
     */
    var $dateTableau;
    
    /**
     * True = changement de bureau
     * False = pas de changement de bureau
     */
    var $changementBureau = false;

    /**
     * pour changement de bureau conserver la valeur du fichier electeur
     */
    var $archivenumero_habitation;
    var $archivecode_voie;
    var $archivecode_bureau;
    var $archivebureauforce;
    
    /**
     * Message debug
     */
    var $msgDebug = "";

    /**
     *
     */
    function constructeur($id, &$db, $DEBUG = false) {
        if (isset($GLOBALS["f"])) {
            $this->f = $GLOBALS["f"];
        }
        //
        parent::constructeur($id, $db, $DEBUG);
        //
        $this->setDateTableau();
    }

    /**
     *
     */
    function setdateTableau() {
        //
        $sql = "select datetableau from collectivite where id='".$_SESSION['collectivite']."'";
        //
        $res = $this->db->getOne($sql);
        //
        if (database::isError($res)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
        } else {
            //
            $this->dateTableau = $res;
        }
    }

    /**
     *
     */
    function setvalFAjout($val = array()) {
        //
    }

    /**
     *
     */    
    function setvalF($val) {
        //
        $this->addToLog("start setvalF", VERBOSE_MODE);
        //
        $this->valF['date_modif'] = $this->dateSystemeDB();
        $this->valF['utilisateur'] = $_SESSION['login'];
        //
        $this->valF['types'] = $val['types'];
        //
        $this->valF['civilite'] = $val['civilite'];
        $this->valF['sexe'] = $val['sexe'];
        $this->valF['situation'] = $val['situation'];
        $this->valF['nom'] = $val['nom'];
        $this->valF['nom_usage'] = $val['nom_usage'];
        $this->valF['prenom'] = $val['prenom'];
        //
        $this->valF['date_naissance'] = $val['date_naissance'];
        $this->valF['code_departement_naissance'] = trim($val['code_departement_naissance']);
        $this->valF['libelle_departement_naissance'] = trim($val['libelle_departement_naissance']);
        $this->valF['code_lieu_de_naissance'] = trim($val['code_lieu_de_naissance']);
        $this->valF['libelle_lieu_de_naissance'] = trim($val['libelle_lieu_de_naissance']);
        $this->valF['code_nationalite'] = $val[ 'code_nationalite'];
        //
        $this->valF['code_voie'] = $val['code_voie'];
        $this->valF['libelle_voie'] = $val['libelle_voie'];
        // Verification numero_habitation
        // Si le numero_habitation est vide alors on lui affecte 0
        if ($val['numero_habitation'] == "") {
            $val['numero_habitation'] = 0;
        }
        $this->valF['numero_habitation'] = $val['numero_habitation'];
        //
        $this->valF['complement_numero'] = $val['complement_numero'];
        $this->valF['complement'] = $val['complement'];
        //
        $this->valF['provenance'] = $val['provenance'];
        $this->valF['libelle_provenance'] = $val['libelle_provenance'];
        //
        $this->valF['observation'] = $val['observation'];
        // adresse resident
        $this->valF['resident'] = $val['resident'];
        $this->valF['adresse_resident'] = $val['adresse_resident'];
        $this->valF['complement_resident'] = $val['complement_resident'];
        $this->valF['cp_resident'] = $val['cp_resident'];
        $this->valF['ville_resident'] = $val['ville_resident'];
        // variable de traitement
        $this->valF['date_tableau'] = $val['date_tableau'];
        $this->valF['liste'] = $val['liste'];
        $this->valF['etat'] = 'actif';
        $this->valF['tableau'] = 'annuel';
        //
        $this->valF['collectivite'] = $_SESSION['collectivite'];
        //
        $this->valF['ancien_bureau'] = $val['ancien_bureau'];
        // Pas de recalcul du bureau si radiation utilisation du bureau de l'électeur.
        if(isset($this->typeCat) and $this->typeCat!='radiation') {
            $this->manageBureauForceAndCodeBureau($val);
        } else {
            $this->valF['code_bureau'] = $val['code_bureau'];
        }
        //
        $this->addToLog("end setvalF", VERBOSE_MODE);
    }

    /**
     *
     * @param array $val
     * @param object $db Objet de connexion DB
     */
    function verifierAjout($val = array(), &$db = NULL) {
        
        //
        
    }
    
    /**
     *
     */
    function setId(&$db) {
        //
        $this->valF[$this->clePrimaire] = $db->nextId($this->table);
    }

    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function verifier ($val, &$db, $DEBUG) {
        //
        $this->addToLog("start verifier", VERBOSE_MODE);
        //
        $this->correct = true;
        //
        $this->addToLog("this->correct = ".($this->correct?"true":"false"), VERBOSE_MODE);
        // Mouvement deja traite
        if ($this->getParameter("maj") != 0
            && $this->val[array_search('etat', $this->champs)] == "trs") {
            $this->correct = false;
            $this->addToMessage(_("Operation impossible. Vous ne pouvez pas ".
                                  "modifier un mouvement deja traite.")."<br/>");
        }
        // zones obligatoires
        // Mouvement & Bureau
        if ($this->valF['types']=="") {
            $this->correct=false;
            $this->msg .= _("Le type de mouvement est obligatoire")."<br />";
        }
        // Etat Civil
        if ($this->valF['nom']=="") {
            $this->correct=false;
            $this->msg .= _("Le nom de l'electeur est obligatoire")."<br />";
        }
        if ($this->valF['prenom']=="") {
            $this->correct=false;
            $this->msg .= _("Le prenom de l'electeur est obligatoire")."<br />";
        }
        // Verification coherence :  civilite/sexe
        if ($this->valF['sexe']=="F" and $this->valF['civilite']=="M." ) {
            $this->correct=false;
            $this->msg .= _("Incompatibilite civilite = M. et sexe = F")."<br />";
        }
        if ($this->valF['sexe']=="M" and $this->valF['civilite']=="Mme" ) {
            $this->correct=false;
            $this->msg .= _("Incompatibilite civilite = Mme et sexe = M")."<br />";
        }
        if ($this->valF['sexe']=="M" and $this->valF['civilite']=="Mlle" ) {
            $this->correct=false;
            $this->msg .= _("Incompatibilite civilite = Mlle et sexe = M")."<br />";
        }
        // Naissance & Nationalite
        // Verifications Date de naissance
        if ($this->valF['date_naissance']=="") {
            $this->correct=false;
            $this->msg .= _("La date de naissance est obligatoire")."<br />";
        } else {
            // XXX Recalcul de l'âge en fonction de la date d'effet de la révision concernée par la date de tableau.
            //// calcul de l'age
            /**
             *
             */
            //
            $datetableau = $this->f->formatDate($this->valF['date_tableau'], false);
            //
            require_once "../obj/revisionelectorale.class.php";
            $revision = new revisionelectorale($this->f, "fromdatetableau", $datetableau);
            if ($revision->revision == null) {
                $this->msg .= _("La revision n'existe pas. Contactez votre administrateur.");
                $this->correct = false;
            } else {
                $date_effet = $revision->get_date("effet", "Ymd");
                //
                $temp0 = $this->valF['date_naissance'];
                //
                $temp = intval($date_effet);
                $temp1 = intval(substr ($temp0, 6, 4).substr ($temp0, 3, 2).substr ($temp0, 0, 2));
                $age = ($temp - $temp1)/10000;
                ////
                $this -> valF ['date_naissance'] = $this -> dateDB ($this -> valF ['date_naissance']);
                $this -> valF ['date_tableau'] = $this -> dateDB ($this -> valF ['date_tableau']);
                // controle electeur plus de 18 ans
                if ($age <= 18) {
                    $this->msg .= _("L'electeur n'aura pas plus de 18 ans le")." ".$revision->get_date("effet", "d/m/Y")."<br />";
                    //
                    $effet_mouvement = false;
                    if (isset($this->valF['types']) && $this->valF['types'] != "") {
                        //
                        $sql = "select * from param_mouvement where code='".$this->valF['types']."'";
                        $res = $db->query($sql);
                        if (database::isError($res)) {
                            $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                        }
                        // On vérifie qu'il y a un et unique mouvement correspondant
                        if ($res->numrows() != 1) {
                            $this->correct = false;
                            $this->msg .= _("Erreur de parametrage des mouvements.")." <br />";
                        } else {
                            // On récupère l'effet du mouvement en question pour vérifier l'âge ou non 
                            // de l'électeur
                            $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
                            $effet_mouvement = $row["effet"];
                        }
                    }
                    //
                    if ($effet_mouvement != "Election") {
                        $this->correct = false;
                    }
                }
            }
        }



        /**
         * Gestion des lieux de naissance
         */

        // On vérifie que le code_departement_naissance n'est pas vide sinon => INCORRECT
        if ($this->valF['code_departement_naissance'] == "") {
            $this->correct=false;
            $this->msg .= _("Le code du departement de naissance obligatoire")."<br />";
        } else {
            // Vérification de l'existence de code_departement_naissance dans la table departement
            $sql = "select * from departement where code='".$this->valF['code_departement_naissance']."'";
            $res = $db->query($sql);
            if (database::isError($res)) {
                $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
            }
            $nbligne = $res->numrows();
            // Initialisation d'un flag temporaire
            $correct = true;
            // Si aucun résultat à la requête aucun département ne correspond à ce code => INCORRECT
            if ($nbligne == 0) {
                $correct = false;
                $this->correct = false;
                $this->msg .= _("Le code du departement de naissance  ").$this->valF['code_departement_naissance']._(" n'existe pas")."<br />";
            }
            //  Si il y a au moins un résultat
            if ($correct) {
                // Vérification de la correspondance entre libelle_departement_naissance et celui de la table departement
                $sql= "select libelle_departement from departement where code='".$this->valF['code_departement_naissance']."'";
                $res = $db->query($sql);
                if (database::isError($res)) {
                    $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                } else {
                    //
                    $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
                    // Si les deux libellés sont différents => INCORRECT
                    if ($row ['libelle_departement'] != $val['libelle_departement_naissance']) {
                        $this->correct=false;
                        $this->msg .= _("La correspondance entre les deux champs Code du departement de naissance / Libelle du departement de naissance n'est pas correcte")."<br />";
                    }
                }
            }
        }

        // On vérifie que le libelle_departement_naissance n'est pas vide sinon => INCORRECT
        if ($this->valF['libelle_departement_naissance'] == "") {
            $this->correct=false;
            $this->msg .= _("Le libelle du departement de naissance obligatoire")."<br />";
        }
        // On vérifie que le code_lieu_de_naissance n'est pas vide sinon => INCORRECT
        if ($this->valF['code_lieu_de_naissance'] == "") {
            $this->correct=false;
            $this->msg .= _("Le code lieu de naissance obligatoire")."<br/>";
        } else {

            // On ne vérifie pas la correspondance entre le code_lieu_de_naissance
            // et le libelle_lieu_de_naissance lorsque le code_département_naissance est sur 
            // 5 chiffres ou lorsqu'il est égal à 99
            // Dans tous les autres cas on vérifie
            if (!(
                    (strlen($this->valF['code_departement_naissance']) == 2 && $this->valF['code_departement_naissance'] == '99') 
                    || (strlen($this->valF['code_departement_naissance']) == 5)
                )) {
                // Vérification de l'existence de code_departement_naissance dans la table commune
                $sql = "select * from commune where code='".$this->valF['code_lieu_de_naissance']."'";
                $res = $db -> query ($sql);
                if (database::isError($res))
                    $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                $nbligne = $res->numrows();
                // Initialisation d'un flag temporaire
                $correct = true;
                // Si aucun résultat à la requête aucune commune ne correspond à ce code => INCORRECT
                if ($nbligne==0) {
                    $correct = false;
                    $this->correct = false;
                    $this->msg .= _("Le code lieu de naissance ").$this->valF['code_lieu_de_naissance']._(" n'existe pas")."<br />";
                }
                //  Si il y a au moins un résultat
                if ($correct) {
                    // Vérification de la correspondance entre libelle_lieu_de_naissance et celui de la table commune
                    $sql= "select libelle_commune from commune where code='".$this->valF['code_lieu_de_naissance']."'";
                    $res = $db->query($sql);
                    if (database::isError($res)) {
                        $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                    } else {
                        //
                        $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
                        // Si les deux libellés sont différents => INCORRECT
                        if ($row ['libelle_commune'] != $val['libelle_lieu_de_naissance']) {
                            $this->correct=false;
                            $this->msg .= _("La correspondance entre les deux champs Code lieu de naissance / Libelle lieu de naissance n'est pas correcte")."<br />";
                        }
                    }
                }
            }
        }
        // On vérifie que le libelle_lieu_de_naissance n'est pas vide
        // 
        if ($this->valF['libelle_lieu_de_naissance'] == "") {
            $this->correct=false;
            $this->msg .= _("Le libelle du lieu de naissance obligatoire")."<br />";
        }
        
        // XXX 
        // Verification de la correspondance entre le code_departement_naissance et le code_lieu_de_naissance
        // Les règles sont : 
        // - si code_departement_naissance # '99' => code_lieu_de_naissance # '99'
        // - si code_departement_naissance # '971'  => code_lieu_de_naissance # '971 XX'
        // - si code_departement_naissance # '13'  => code_lieu_de_naissance # '13 XXX'
        // - si code_departement_naissance # '99134' => code_lieu_de_naissance # '99134'
        // - si code_departement_naissance # '91352' => code_lieu_de_naissance # '91352'
        if ((
                strlen($this->valF['code_departement_naissance']) == 2
                && $this->valF['code_departement_naissance'] == '99' 
                && $this->valF['code_lieu_de_naissance'] != '99'
            ) || (
                strlen($this->valF['code_departement_naissance']) == 3
                && $this->valF['code_departement_naissance'] != substr($this->valF['code_lieu_de_naissance'], 0, 3)
            ) || (
                strlen($this->valF['code_departement_naissance']) == 2
                && $this->valF['code_departement_naissance'] != substr($this->valF['code_lieu_de_naissance'], 0, 2)
            ) || (
                strlen($this->valF['code_departement_naissance']) == 5
                && $this->valF['code_departement_naissance'] != $this->valF['code_lieu_de_naissance']
            )) {
            $this->correct=false;
            $this->msg .= _("Lieu de naissance n'est pas dans le departement de naissance")."<br />";
        }







        if ($this->valF['code_nationalite']=="") {
            $this->correct=false;
            $this->msg .= _("La nationalite obligatoire")."<br />";
        }

        // Verification numero_habitation
        // Si le numero_habitation n'est pas un entier on rejette la saisie
        if (is_numeric($this->valF['numero_habitation']) == false) {
            $this->correct = false;
            $this->msg .= _("Le numero d'habitation doit etre un entier")."<br />";
        }
        //
        if ($this->valF['code_voie']=="") {
            $this->correct=false;
            $this->msg .= _("Le code de la voie est obligatoire")."<br />";
        } else {
            // Test si la voie existe
            /* ++ */ $sql = "select * from voie where code='".$this->valF['code_voie']."' and code_collectivite = '".$_SESSION['collectivite']."' ";
            $res = $db -> query ($sql);
            if (database::isError($res))
                $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
            $nbligne = $res->numrows(); //XXX
            $correct = true;
            if ($nbligne==0) {
                $this->correct=false;
                $correct = false;
                $this->msg .= _("La voie ").$this->valF['code_voie']._(" n'existe pas")."<br />";
            }
            if ($correct) {
                /* ++ */ $sql= "select libelle_voie from voie where code='".$this->valF['code_voie']."' and code_collectivite = '".$_SESSION['collectivite']."' ";
                $res = $db -> query ($sql);
                if($DEBUG>=VERBOSE_MODE) $this->msgDebug .= $sql."<br />";
                if (database::isError($res)) {
                    $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                } else {
                    $row = $res -> fetchrow (DB_FETCHMODE_ASSOC);
                    if ($row ['libelle_voie'] != $val['libelle_voie']) {
                        $this->correct=false;
                        $this->msg .= _("La correspondance entre les deux champs Id / Libelle de la voie n'est pas correcte")."<br />";
                    }
                }
            }
        }
        /*if ($this->valF['libelle_voie']=="") {
            $this->correct=false;
            $this->msg .= "Le libelle de la voie est obligatoire.<br />";
        }*/
        // Resident
        if ($this->valF['resident']=="Oui") {
            if ($this->valF['adresse_resident']=="") {
                $this->correct=false;
                $this->msg .= _("L'adresse resident est obligatoire")."<br />";
            }
            if ($this->valF['cp_resident']=="") {
                $this->correct=false;
                $this->msg .= _("Le code postal resident est obligatoire")."<br />";
            }
            if ($this->valF['ville_resident']=="") {
                $this->correct=false;
                $this->msg .= _("La ville resident est obligatoire")."<br />";
            }
        }
        // Provenance
        // Obligation de remplir provenance si type = "venant autre commune"
        include ("../dyn/var.inc");
        if ($this->valF['types']==$param_mouvement_venantautrecommune) {
            if ($this->valF['provenance']=="") {
                $this->correct=false;
                $this->msg .= _("Code provenance obligatoire pour type ").$param_mouvement_venantautrecommune."<br />";
            } else {
                    
                //// Test Correspondance avec libelle_lieu_de_naissance
                if ( substr($this->valF['provenance'],0,2)!="99" ) {
                    // Test si le lieu/commune existe
                    /* ++ */ $sql = "select * from commune where code='".$this->valF['provenance']."'";
                    $res = $db -> query ($sql);
                    if (database::isError($res))
                        $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                    $nbligne = $res->numrows(); //XXX
                    $correct = true;
                    if ($nbligne==0) {
                        $this->correct=false;
                        $correct = false;
                        $this->msg .= _("Le code de provenance ").$this->valF['provenance']._(" n'existe pas")."<br />";
                    }
                    if ($correct) {
                        /* ++ */ $sql= "select libelle_commune from commune where code='".$this->valF['provenance']."'";
                        $res = $db -> query ($sql);
                        if($DEBUG>=VERBOSE_MODE) $this->msgDebug .= $sql."<br />";
                        if (database::isError($res)) {
                            $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
                        } else {
                            $row = $res -> fetchrow (DB_FETCHMODE_ASSOC);
                            if ($row ['libelle_commune'] != $val['libelle_provenance']) {
                                $this->correct=false;
                                $this->msg .= _("La correspondance entre les deux champs Code de provenance / Libelle de provenance n'est pas correcte")."<br />";
                            }
                        }
                    }
                }
                
            }
            if ($this->valF['libelle_provenance']=="") {
                $this->correct=false;
                $this->msg .= _("Libelle provenance obligatoire pour type ").$param_mouvement_venantautrecommune."<br />";
            }
        }
        //
        // 
        if ($this->valF['code_bureau'] == "" && $this->valF['bureauforce'] == "") {
            $this->correct = false;
            $this->msg .= _("Il y a une incoherence dans la saisie, il n'est ".
                            "pas possible de forcer le bureau avec la valeur ".
                            "de 'Bureau force' positionnee a 'Non'.")."<br />";
        }
        // Si on choisit de forcer le bureau, il est obligatoire de choisir
        // un bureau
        if ($this->valF['code_bureau'] == "" && $this->valF['bureauforce'] == "Oui") {
            $this->correct = false;
            $this->msg .= _("Il y a une incoherence dans la saisie, il n'est ".
                            "pas possible de ne pas selectionner de bureau ".
                            "avec la valeur de 'Bureau force' positionnee a ".
                            "'Oui'.")."<br />";
        }
        // 
        if ($this->valF['code_bureau'] == "" && $this->valF['bureauforce'] == "Non") {
            $this->correct = false;
            $this->msg .= _("Le choix du bureau ne peut pas etre automatique ".
                            "car il n'y a pas de bureau correspondant dans le ".
                            "decoupage pour cette adresse. Vous devez donc ".
                            "soit saisir une correspondance dans le decoupage ".
                            "soit forcer le bureau en le selectionnant.")."<br />";
        }
        //
        $this->addToLog("this->correct = ".($this->correct?"true":"false"), VERBOSE_MODE);
        //
        $this->addToLog("end verifier", VERBOSE_MODE);
    }

    /**
     *
     */
    function cleSecondaire($id, &$db = NULL, $val = array(), $DEBUG = false) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Mouvement deja traite
        if ($this->val[array_search('etat', $this->champs)] == "trs") {
            $this->correct = false;
            $this->addToMessage(_("Operation impossible. Vous ne pouvez pas ".
                                  "supprimer un mouvement deja traite.")."<br/>");
        }
    }

    /**
     * Cette methode permet de remplir le tableau val attribut de
     * l'objet contenu dans $valE.
     * 
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param integer $validation
     * @param array $valE
     * 
     * @return void
     */
    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$db, $DEBUG = false) {
        $valE = $this->electeur;
        if ($validation==0) {
            if ($maj == 0) {
                // ******************************************
                // ajout => recuperation des donnees ELECTEUR
                // suivant requete electeur.form.inc
                // ******************************************
                $form->setVal('id_electeur', $valE['id_electeur']);
                $form->setVal('numero_electeur', $valE['numero_electeur']);
                $form->setVal('numero_bureau', $valE['numero_bureau']);
                $form->setVal('code_bureau',$valE['code_bureau']);
                $form->setVal('ancien_bureau', $valE['code_bureau']);
                $form->setVal('bureauforce', $valE['bureauforce']);
                // liste et traitement
                $form->setVal('liste', $valE['liste']);
                $form->setVal('date_tableau',$this->dateTableau);
                $form->setVal('tableau',"annuel");
                $form->setVal('etat',"actif");
                // etat civil
                $form->setVal('civilite', $valE['civilite']);
                $form->setVal('sexe', $valE['sexe']);
                $form->setVal('nom', $valE['nom']);
                $form->setVal('nom_usage', $valE['nom_usage']);
                $form->setVal('prenom', $valE['prenom']);
                $form->setVal('situation', $valE['situation']);
                //naissance
                $form->setVal('date_naissance', $valE['date_naissance']);
                $form->setVal('code_departement_naissance', $valE['code_departement_naissance']);
                $form->setVal('libelle_departement_naissance', $valE['libelle_departement_naissance']);
                $form->setVal('code_lieu_de_naissance', $valE['code_lieu_de_naissance']);
                $form->setVal('libelle_lieu_de_naissance', $valE['libelle_lieu_de_naissance']);
                $form->setVal('code_nationalite', $valE['code_nationalite']);
                // adresse dans la commune
                $form->setVal('numero_habitation', $valE['numero_habitation']);
                $form->setVal('libelle_voie', $valE['libelle_voie']);
                $form->setVal('code_voie', $valE['code_voie']);
                $form->setVal('complement_numero', $valE['complement_numero']) ;
                $form->setVal('complement', $valE['complement']) ;
                // provenance
                $form->setVal('provenance', $valE['provenance']);
                $form->setVal('libelle_provenance', $valE['libelle_provenance']);
                // recuperation adresse resident
                $form->setVal('resident', $valE['resident']);
                $form->setVal('adresse_resident', $valE['adresse_resident']);
                $form->setVal('complement_resident', $valE['complement_resident']);
                $form->setVal('cp_resident', $valE['cp_resident']);
                $form->setVal('ville_resident', $valE['ville_resident']);
                //initialiser a vide
                $form->setVal('id','');
                $form->setVal('observation','') ;
                $form->setVal('utilisateur','');
                $form->setVal('types','');
                $form->setVal('date_modif','');
                $form->setVal('envoi_cnen','');
                $form->setVal('date_cnen','');
            }
        }
    }
    
    // {{{ Valorisation du formulaire
    
    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType (&$form, $maj) {
        if ($this -> typeCat == "inscription") {
            $form->setType('etat', 'hidden');
            $form->setType('tableau','hidden');
            $form->setType('ancien_bureau','hidden');
            if($maj<2) {
                // modifier et ajouter
                $form->setType('ancien_bureau','hiddenstatic');
                $form->setType('id_electeur', 'hiddenstatic');
                $form->setType('numero_electeur', 'hiddenstatic');
                $form->setType('etat', 'hidden');
                $form->setType('liste', 'hiddenstatic');
                $form->setType('date_tableau','hiddenstaticdate');
                $form->setType('tableau','hidden');
                $form->setType('numero_bureau', 'hiddenstatic');
                $form->setType('types','select');
                $form->setType('code_nationalite','select');
                $form->setType('complement_numero','select');
                $form->setType('code_bureau','select');
                $form->setType('bureauforce','select');
                $form->setType('numero_habitation','text');
                $form->setType('code_voie','comboD');
                $form->setType('libelle_voie','comboG');
                $form->setType('civilite','select');
                $form->setType('sexe','select');
                $form->setType('situation','select');
                $form->setType('date_naissance','date');
                $form->setType('provenance','comboD');
                $form->setType('libelle_provenance','comboG');
                $form->setType('code_departement_naissance','comboD');
                $form->setType('libelle_departement_naissance','comboG');
                $form->setType('code_lieu_de_naissance','comboD');
                $form->setType('libelle_lieu_de_naissance','comboG');
                $form->setType('resident', 'select');
                $form->setType('ancien_bureau','hidden');
                if ($maj==1) {
                    //modifier
                    $form->setType('id','hiddenstatic');
                    $form->setType('date_modif', 'hiddenstaticdate');
                    $form->setType('utilisateur','static');
                    $form->setType('date_cnen','hiddenstaticdate');
                    $form->setType('envoi_cnen','hiddenstatic');
                } else {
                    // ajouter
                    $form->setType('id','hidden');
                    $form->setType('date_modif','hidden');
                    $form->setType('utilisateur','hidden');
                    $form->setType('envoi_cnen','hidden');
                    $form->setType('date_cnen','hidden');
                    $form->setType('ancien_bureau','hidden');
                    $form->setType('id_electeur', 'hidden');
                    $form->setType('numero_electeur', 'hidden');
                    $form->setType('liste', 'hidden');
                    $form->setType('numero_bureau', 'hidden');
                }
            } else {
                // supprimer
                $form->setType('id','hiddenstatic');
                $form->setType('id_electeur','hiddenstatic');
            }
        }
        elseif ($this -> typeCat == 'radiation') {
            for($i=0;$i<count($this->champs);$i++)
                $form->setType($this->champs[$i],"hiddenstatic");
            $form->setType('date_modif','hiddenstaticdate');
            $form->setType('date_naissance','hiddenstaticdate');
            $form->setType('date_tableau','hiddenstaticdate');
            $form->setType('date_cnen','hiddenstaticdate');
            $form->setType('ancien_bureau','hidden');
            $form->setType('etat','hidden');
            $form->setType('tableau','hidden');
            if($maj==0) {
                $form->setType('id','hidden');
                $form->setType('date_modif','hidden');
                $form->setType('utilisateur','hidden');
            }
            if($maj<2) {
                $form->setType('types','select');
                $form->setType('observation','text');
            }
        } else {
            $form->setType('etat', 'hidden');
            $form->setType('tableau','hidden');
            if($maj<2) {
                // modifier et ajouter
                $form->setType('ancien_bureau','hiddenstatic');
                $form->setType('numero_bureau', 'hiddenstatic');
                $form->setType('id_electeur', 'hiddenstatic');
                $form->setType('numero_electeur', 'hiddenstatic');
                $form->setType('etat', 'hidden');
                $form->setType('liste', 'hiddenstatic');
                $form->setType('date_tableau','hiddenstaticdate');
                $form->setType('tableau','hidden');
                //
                $form->setType('types','select');
                $form->setType('code_nationalite','select');
                $form->setType('complement_numero','select');
                $form->setType('code_bureau','select');
                $form->setType('bureauforce','select');
                $form->setType('numero_habitation','text');
                $form->setType('code_voie','comboD');
                $form->setType('libelle_voie','comboG');
                $form->setType('civilite','select');
                $form->setType('sexe','select');
                $form->setType('situation','select');
                $form->setType('date_naissance','date');
                $form->setType('provenance','comboD');
                $form->setType('libelle_provenance','comboG');
                $form->setType('code_departement_naissance','comboD');
                $form->setType('libelle_departement_naissance','comboG');
                $form->setType('code_lieu_de_naissance','comboD');
                $form->setType('libelle_lieu_de_naissance','comboG');
                $form->setType('resident', 'select');
                if ($maj==1) {
                    //modifier
                    $form->setType('id','hiddenstatic');
                    $form->setType('date_modif', 'hiddenstaticdate');
                    $form->setType('utilisateur','static');
                    $form->setType('date_cnen','hiddenstaticdate');
                    $form->setType('envoi_cnen','hiddenstatic');
                } else { // ajouter
                    $form->setType('id','hidden');
                    $form->setType('date_modif','hidden');
                    $form->setType('utilisateur','hidden');
                    $form->setType('envoi_cnen','hidden');
                    $form->setType('date_cnen','hidden');
                }
            } else {
                // supprimer
                $form->setType('id','hiddenstatic');
                $form->setType('id_electeur','hiddenstatic');
            }
        }
    }

    /**
     * Parametrage du formulaire - Select des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function setSelect(&$form = NULL, $maj, &$db = NULL, $DEBUG = false) {
        // 
        require "../sql/".$this->db->phptype."/".$this->table.".form.inc";
        // Si Mode Ajout ou Modification (on a pas de select en mode Suppression)
        if ($maj < 2) {
            // COMBO
            // Departement ou Pays de naissance - Code & Libelle
            $contenu = array();
            $contenu[0][0] = "departement"; // Table de la recherche
            $contenu[0][1] = "code"; // Champ sur lequel va porter la recherche
            $contenu[1][0] = "libelle_departement"; // Champ correle
            $contenu[1][1] = "libelle_departement_naissance"; // Champ du formulaire a remplir
            $form->setSelect("code_departement_naissance", $contenu);
            $contenu = array();
            $contenu[0][0]="departement";// table
            $contenu[0][1]="libelle_departement"; // zone origine
            $contenu[1][0]="code";
            $contenu[1][1]="code_departement_naissance";
            $form->setSelect("libelle_departement_naissance",$contenu);
            // Lieu de naissance - Code & Libelle
            $contenu = array();
            $contenu[0][0]="commune";
            $contenu[0][1]="code";
            $contenu[1][0]="libelle_commune";
            $contenu[1][1]="libelle_lieu_de_naissance";
            $contenu[2][0]="code_departement"; // champ pour le where
            $contenu[2][1]="code_departement_naissance"; // zone du formulaire concerne
            $form->setSelect("code_lieu_de_naissance",$contenu);
            $contenu = array();
            $contenu[0][0]="commune";
            $contenu[0][1]="libelle_commune";
            $contenu[1][0]="code";
            $contenu[1][1]="code_lieu_de_naissance";
            $contenu[2][0]="code_departement"; // champ pour le where
            $contenu[2][1]="code_departement_naissance"; // zone du formulaire concerne
            $form->setSelect("libelle_lieu_de_naissance",$contenu);
            // Commune de provenance - Code & Libelle
            $contenu = array();
            $contenu[0][0]="commune";// table
            $contenu[0][1]="code"; // zone origine
            $contenu[1][0]="libelle_commune";
            $contenu[1][1]="libelle_provenance";
            $form->setSelect("provenance",$contenu);
            $contenu = array();
            $contenu[0][0]="commune";// table
            $contenu[0][1]="libelle_commune"; // zone origine
            $contenu[1][0]="code";
            $contenu[1][1]="provenance";
            $form->setSelect("libelle_provenance",$contenu);
            // Voie - Code & Libelle
            $contenu = array();
            $contenu[0][0]="voie";// table
            $contenu[0][1]="code"; // zone origine
            $contenu[1][0]="libelle_voie";
            $contenu[1][1]="libelle_voie";
            $form->setSelect("code_voie",$contenu);
            $contenu = array();
            $contenu[0][0]="voie";// table
            $contenu[0][1]="libelle_voie"; // zone origine
            $contenu[1][0]="code";
            $contenu[1][1]="code_voie";
            $form->setSelect("libelle_voie",$contenu);

    
            /**
             * DYNAMIQUES
             */
            // bureau
            $contenu="";
            $res = $db->query($sql_bureau);
            if (database::isError($res)) {
                die($res->getMessage());
            } else {
                if ($DEBUG >= VERBOSE_MODE) 
                    $this->msgDebug .= " La requete ".$sql_bureau." est executee.<br>";

                $contenu[0][0]="";
                $contenu[1][0]="Forcer le choix du bureau ";
                $k=1;
                while ($row=& $res->fetchRow())
                {
                    $contenu[0][$k]=$row[0];
                    $contenu[1][$k]=$row[1];
                    $k++;
                }
                $form->setSelect("code_bureau",$contenu);
            }
            // nationalite
            $contenu="";
            $res = $db->query($sql_nationalite);
            if (database::isError($res)) {
                die($res->getMessage());
            } else {
                if ($DEBUG >= VERBOSE_MODE)
                    $this->msgDebug .= " La requete ".$sql_nationalite." est executee.<br>";
                $contenu="";
                $contenu[0][0]="";
                $contenu[1][0]=_("Choix de la nationalite");
                $k=1;
                while ($row=& $res->fetchRow())
                {
                    $contenu[0][$k]=$row[0];
                    $contenu[1][$k]=$row[1];
                    $k++;
                }
                $form->setSelect("code_nationalite",$contenu);
            }
            // SPECIFIQUE type de categorie de mouvement
            if ($this -> typeCat == 'inscription')
                $sql = $sql_type_I;
            elseif ($this->typeCat == 'radiation')
                $sql = $sql_type_R;
            else
                $sql = $sql_type_M;
            $this->init_select($form, $db, $maj, false, "types", $sql, $sql_type_by_id, true);

            /**
             * STATIQUES
             */
            // civilite
            $contenu="";
            $contenu[0]=array('M.','Mme','Mlle');
            $contenu[1]=array(_('Monsieur'),_('Madame'),_('Mademoiselle'));
            $form->setSelect("civilite",$contenu);
            // Sexe
            $contenu="";
            $contenu[0]=array('M','F');
            $contenu[1]=array(_('Masculin'),_('Feminin'));
            $form->setSelect("sexe",$contenu);
            // situation matrimoniale
            $contenu="";
            $contenu[0]=array('','C','M','V','D');
            $contenu[1]=array(_('Sans'),_('Celibataire'),_('Marie'),_('Veuf'),_('Divorce'));
            $form->setSelect("situation",$contenu);
            // complement
            $contenu="";
            $contenu[0]=array('','bis','ter','quater','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9');
            $contenu[1]=array(_('Sans'),_('bis'),_('ter'),_('quater'),'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9');
            $form->setSelect("complement_numero",$contenu);
            // bureauforce
            $contenu="";
            $contenu[0]=array('Non','Oui');
            $contenu[1]=array(_('Non'),_('Oui'));
            $form->setSelect("bureauforce",$contenu);
            // resident
            $contenu="";
            $contenu[0]=array('Non','Oui');
            $contenu[1]=array(_('Non'),_('Oui'));
            $form->setSelect("resident",$contenu);
        }
    }

    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setOnchange (&$form, $maj, $valE = array()) {
        if( !isset($valE['code_bureau']) ) $valE['code_bureau'] = "";
        $form->setOnchange("nom","this.value=this.value.toUpperCase()");
        $form->setOnchange("prenom","this.value=this.value.toUpperCase()");
        $form->setOnchange("nom_usage","this.value=this.value.toUpperCase()");
        
        $form->setOnchange("numero_habitation","VerifNum(this)");
        $form->setOnchange("libelle_voie","this.value=this.value.toUpperCase()");
        $form->setOnchange("complement","this.value=this.value.toUpperCase()");
        
        $form->setOnchange("libelle_departement_naissance","this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle_lieu_de_naissance","this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle_provenance","this.value=this.value.toUpperCase()");
        
        $form->setOnchange("adresse_resident","this.value=this.value.toUpperCase()");
        $form->setOnchange("complement_resident","this.value=this.value.toUpperCase()");
        $form->setOnchange("cp_resident","this.value=this.value.toUpperCase()");
        $form->setOnchange("ville_resident","this.value=this.value.toUpperCase()");
        
        $form->setOnchange("civilite","changeSexe()");
        $form->setOnchange("code_bureau","forcerlebureau('".$valE['code_bureau']."')");
        $form->setOnchange("bureauforce","nepasforcerlebureau()");
        $form->setOnchange("code_departement_naissance","codecommune(this)");
        $form->setOnchange("date_naissance","fdate(this)");
    }

    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib (&$form, $maj) {
        $form->setLib('id',_('Mouvement numero '));
        $form->setLib('id_electeur',_('Id electeur numero '));
        $form->setLib('numero_bureau',_(' numero '));
        $form->setLib('code_bureau',_('Bureau'));
        $form->setLib('numero_electeur',_(' numero '));
        $form->setLib('liste',_(' dans la liste '));
        $form->setLib('date_modif',_('Modifie le '));
        $form->setLib('utilisateur',_(' par ')); 
        $form->setLib('types',_('Type '));
        //Etat Civil
        $form->setLib('nom',_('Nom'));
        $form->setLib('prenom',_('Prenom'));
        $form->setLib('civilite',_('Civilite'));
        $form->setLib('situation',_('Situation'));
        $form->setLib('sexe',_('Sexe'));
        $form->setLib('nom_usage',_("Nom d'usage"));
        // Naissance & Nationalite
        $form->setLib('date_naissance',_('Date de naissance'));
        $form->setLib('code_departement_naissance',_('Departement'));
        $form->setLib('libelle_departement_naissance',' ');
        $form->setLib('code_lieu_de_naissance',_('Lieu de naissance'));
        $form->setLib('libelle_lieu_de_naissance',' ');
        $form->setLib('code_nationalite',_('Nationalite'));
        // Adresse
        $form->setLib('numero_habitation',_('Numero'));
        $form->setLib('code_voie',_('Id/Libelle Voie'));
        $form->setLib('libelle_voie','');
        $form->setLib('complement_numero','');
        $form->setLib('complement','<td>'._('Complement').'</td><td></td>');
        // Resident
        $form->setLib('resident',_('Resident'));
        $form->setLib('adresse_resident',_('Adresse'));
        $form->setLib('complement_resident',_('Complement'));
        $form->setLib('cp_resident',_('Code postal'));
        $form->setLib('ville_resident',_('Ville'));
        // Provenance
        $form->setLib('provenance',_('Code/Libelle commune de provenance '));
        $form->setLib('libelle_provenance',' ');
        //
        $form->setLib('etat','');
        $form->setLib('tableau',_(' [Tableau] '));
        $form->setLib('date_tableau',_('au tableau du'));
        $form->setLib('envoi_cnen',_(' [INSEE] '));
        $form->setLib('date_cnen','');
        //$form->setLib('code_bureau','Bureau ');
        $form->setLib('bureauforce',_('Bureau force '));
    }

    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille (&$form, $maj) {
        $form->setTaille('nom',40);
        $form->setTaille('nom_usage',30);
        $form->setTaille('prenom',40);
        $form->setTaille('libelle_commune',30);
        $form->setTaille('provenance_commune',30);
        $form->setTaille('code_departement_naissance',5);
        $form->setTaille('libelle_departement_naissance',30);
        $form->setTaille('code_lieu_de_naissance',6);
        $form->setTaille('libelle_lieu_de_naissance',30);
        $form->setTaille('date_naissance',10);
        $form->setTaille('code_voie',6);
        $form->setTaille('libelle_voie',30);
        $form->setTaille('complement',30);
        $form->setTaille('provenance',6);
        $form->setTaille('libelle_provenance',30);
        $form->setTaille('observation',85);
        $form->setTaille('resident',3);
        $form->setTaille('adresse_resident',20);
        $form->setTaille('complement_resident',20);
        $form->setTaille('cp_resident',5);
        $form->setTaille('ville_resident',10);
    }

    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax (&$form, $maj) {
        $form->setMax('nom',40);
        $form->setMax('nom_usage',40);
        $form->setMax('prenom',40);
        $form->setMax('libelle_voie',30);
        $form->setMax('libelle_commune',30);
        $form->setMax('provenance_commune',30);
        $form->setMax('code_departement_naissance',5);
        $form->setMax('libelle_departement_naissance',30);
        $form->setMax('code_lieu_de_naissance',6);
        $form->setMax('libelle_lieu_de_naissance',30);
        $form->setMax('code_voie',6);
        $form->setMax('libelle_voie',30);
        $form->setMax('complement',80);
        $form->setMax('provenance',6);
        $form->setMax('libelle_provenance',30);
        $form->setMax('observation',100);
        $form->setMax('resident',3);
        $form->setMax('adresse_resident',40);
        $form->setMax('complement_resident',80);
        $form->setMax('cp_resident',5);
        $form->setMax('ville_resident',30);
    }

    /**
     * Parametrage du formulaire - Groupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setGroupe (&$form, $maj) {
        // Identification
        $form->setGroupe('id_electeur','D');
        $form->setGroupe('numero_electeur','G');
        $form->setGroupe('liste','F');
        //$form->setGroupe('ancien_bureau','F');
        $form->setGroupe('id','D');
        $form->setGroupe('date_modif','G');
        $form->setGroupe('utilisateur','F');
        //
        if ($this->typeCat == "inscription"
            and $maj == 0) {
            $form->setGroupe('code_bureau','D');
            $form->setGroupe('bureauforce','F');
        } else {
            $form->setGroupe('code_bureau','D');
            $form->setGroupe('bureauforce','G');
            $form->setGroupe('numero_bureau','F');
        }

        // Mouvement
        $form->setGroupe('types','D');
        $form->setGroupe('date_tableau','F');
        // Etat civil
        $form->setGroupe('civilite','D');
        $form->setGroupe('sexe','F');
        $form->setGroupe('nom','D');
        $form->setGroupe('nom_usage','F');
        $form->setGroupe('prenom','D');
        $form->setGroupe('situation','F');
        // Adresse
        $form->setGroupe('numero_habitation','D');
        $form->setGroupe('code_voie','G');
        $form->setGroupe('libelle_voie','F');
        $form->setGroupe('complement_numero','D');
        $form->setGroupe('complement','F');
        // Naissance    
        $form->setGroupe('code_lieu_de_naissance','D');
        $form->setGroupe('libelle_lieu_de_naissance','F');
        $form->setGroupe('code_departement_naissance','D');
        $form->setGroupe('libelle_departement_naissance','F');
        //???
        $form->setGroupe('etat','D');
        $form->setGroupe('tableau','G');
        $form->setGroupe('envoi_cnen','G');
        $form->setGroupe('date_cnen','F');
        // Resident
        $form->setGroupe('resident','D');
        $form->setGroupe('adresse_resident','G');
        $form->setGroupe('complement_resident','F');
        $form->setGroupe('cp_resident','D');
        $form->setGroupe('ville_resident','F');
        // Provenance
        $form->setGroupe('provenance','D');
        $form->setGroupe('libelle_provenance','F');
    } 

    /**
     * Parametrage du formulaire - Regroupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setRegroupe (&$form, $maj) {
        if ($this->typeCat == "inscription") {
            // Identification
            if ($maj != 0) {
                $form->setRegroupe('id_electeur','D',_('Identification'));
                $form->setRegroupe('numero_electeur','G','');
                $form->setRegroupe('liste','G','');
                $form->setRegroupe('ancien_bureau','G','');
                $form->setRegroupe('id','G','');
                $form->setRegroupe('date_modif','G','');
                $form->setRegroupe('utilisateur','F','');
            }   
            // Mouvement & Bureau
            $form->setRegroupe('types','D',_('Mouvement & Bureau'), ($maj == 2?"startClosed":""));
            $form->setRegroupe('date_tableau','G','');
            $form->setRegroupe('code_bureau','G','');
            if ($maj!=0) {
                $form->setRegroupe('bureauforce','G','');
                $form->setRegroupe('numero_bureau','F','');
            } else {
                $form->setRegroupe('bureauforce','F','');   
            }
        }
        elseif ($this->typeCat == "modification") {
            // Identification
            $form->setRegroupe('id_electeur','D',_('Identification'));
            $form->setRegroupe('numero_electeur','G','');
            $form->setRegroupe('liste','G','');
            if ($maj == 0) {
                $form->setRegroupe('ancien_bureau','F','');
            } else {
                $form->setRegroupe('ancien_bureau','G','');
                $form->setRegroupe('id','G','');
                $form->setRegroupe('date_modif','G','');
                $form->setRegroupe('utilisateur','F','');
            }
            //if ($maj!=0)
                //$form->setRegroupe('ancien_bureau','G','');
            // Mouvement & Bureau
            $form->setRegroupe('types','D',_('Mouvement & Bureau'), ($maj == 2?"startClosed":""));
            $form->setRegroupe('date_tableau','G','');
            $form->setRegroupe('code_bureau','G','');
            $form->setRegroupe('bureauforce','G','');
            $form->setRegroupe('numero_bureau','F','');
        } else {
            // Identification
            $form->setRegroupe('id_electeur','D',_('Identification'));
            $form->setRegroupe('numero_electeur','G','');
            if ($maj==0) {
                $form->setRegroupe('liste','F','');
            } else {
                $form->setRegroupe('liste','G','');
                $form->setRegroupe('id','G','');
                $form->setRegroupe('date_modif','G','');
                $form->setRegroupe('utilisateur','F','');
            }
            //if ($maj!=0)
                //$form->setRegroupe('ancien_bureau','G','');
            // Mouvement & Bureau
            $form->setRegroupe('types','D',_('Mouvement & Bureau'), ($maj == 2?"startClosed":""));
            $form->setRegroupe('date_tableau','G','');
            $form->setRegroupe('code_bureau','G','');
            $form->setRegroupe('bureauforce','G','');
            $form->setRegroupe('numero_bureau','F','');
        }
        /**
         * Identique pour tous les types de mouvement
         **/
        // Etat civil
        $form->setRegroupe('civilite','D',_('Etat Civil '));
        $form->setRegroupe('sexe','G','');
        $form->setRegroupe('nom','G','');
        $form->setRegroupe('nom_usage','G','');
        $form->setRegroupe('prenom','G','');
        $form->setRegroupe('situation','F','');
        // Naissance & Nationalite
        $form->setRegroupe('date_naissance','D',_('Naissance & Nationalite'), ($maj == 2?"startClosed":""));
        $form->setRegroupe('code_departement_naissance','G','');
        $form->setRegroupe('libelle_departement_naissance','G','');
        $form->setRegroupe('code_lieu_de_naissance','G','');
        $form->setRegroupe('libelle_lieu_de_naissance','G','');
        $form->setRegroupe('code_nationalite','F','');
        // Adresse
        $form->setRegroupe('numero_habitation','D',_(' Adresse '), ($maj == 2?"startClosed":""));
        $form->setRegroupe('code_voie','G','');
        $form->setRegroupe('libelle_voie','G','');
        $form->setRegroupe('complement_numero','G','');
        $form->setRegroupe('complement','F','');
        // Resident
        $form->setRegroupe('resident','D',_(' Resident '), "startClosed");
        $form->setRegroupe('adresse_resident','G','');
        $form->setRegroupe('complement_resident','G','');
        $form->setRegroupe('cp_resident','G','');
        $form->setRegroupe('ville_resident','F','');
        // Provenance
        $form->setRegroupe('provenance','D',_(' Provenance'), "startClosed");
        $form->setRegroupe('libelle_provenance','F','');
    }
    
    // }}}
    
    // {{{ Gestion du code_bureau, bureauforce et numero_bureau

    /**
     *
     */
    function manageBureauForceAndCodeBureau($val = array()) {
        // Si le bureau n'est pas force
        if ($val['bureauforce'] == "Non") {
            $this->valF['bureauforce'] = "Non";
            // Recuperation du code bureau en fonction du decoupage et de
            // l'adresse
            $code_bureau = $this->getBureauFromDecoupage($this->valF['code_voie'], $this->valF['numero_habitation']);
            // Si le decoupage ne donne aucun resultat
            if ($code_bureau == NULL) {
                // Pas d'entree dans le decoupage pour cette adresse
                // On initialise pour faire en sorte que la methode
                // verifier rejette l'enregistrement
                $this->valF['code_bureau'] = "";
            } else { // Si le decoupage nous donne un bureau
                // On affecte le code bureau calcule en fonction du
                // decoupage
                $this->valF['code_bureau'] = $code_bureau;
                // On affecte le numero dans le bureau
                $this->setValFNumeroBureau();
            }
        } else { // Si le bureau est force
            $this->valF['bureauforce'] = "Oui";
            // Si aucun bureau n'est selectionne
            if ($val['code_bureau'] == "") {
                // Incoherence de saisie
                // On initialise pour faire en sorte que la methode
                // verifier rejette l'enregistrement
                $this->valF['code_bureau'] = "";
            } else { // Si un bureau est selectionne
                // On affecte le code bureau selectionne par l'utilisateur
                $this->valF['code_bureau'] = $val['code_bureau'];
                // On affecte le numero dans le bureau
                $this->setValFNumeroBureau();
            }
        }
    }

    /**
     *
     */
    function setValFNumeroBureau() {}

    /**
     *
     */
    function getBureauFromDecoupage($code_voie = 0, $numero_habitation = 0) {
        //
        $code_bureau = NULL;
        //
        if ($numero_habitation % 2 == 0) {
            // Pair
            $sql = "select code_bureau from decoupage ";
            $sql .= "where code_voie='".$code_voie."' ";
            $sql .= "and (premier_pair<=".$numero_habitation." ";
            $sql .= "and dernier_pair>=".$numero_habitation.")";
        } else {
            // Impair
            $sql = "select code_bureau from decoupage ";
            $sql .= "where code_voie='".$code_voie."' ";
            $sql .= "and (premier_impair<=".$numero_habitation." ";
            $sql .= "and dernier_impair>=".$numero_habitation.")";
        }
        // Log
        $this->addToLog("getBureauFromDecoupage - "._("Requete :")." ".str_replace(",", ", ",$sql), VERBOSE_MODE);
        // Exécution de la requête
        $res = $this->db->getOne($sql);
        // Si une erreur survient
        if (database::isError($res)) {
            // Appel de la méthode de récupération des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
        } else {
            // Log
            $this->addToLog("getBureauFromDecoupage - "._("Requete executee"), VERBOSE_MODE);
            //
            if ($res != NULL) {
                //
                $code_bureau = $res;
            }
        }
        //
        return $code_bureau;
    }

    /**
     * Cette methode permet d'afficher dans le formulaire les valeurs calculees
     * pendant le traitement
     */
    function setComputedVal() {
        //
        if (isset($this->valF['code_bureau'])) {
            $this->form->setVal('code_bureau', $this->valF['code_bureau']);
        }
        if (isset($this->valF['bureauforce'])) {
            $this->form->setVal('bureauforce', $this->valF['bureauforce']);
        }
        if (isset($this->valF['numero_bureau'])) {
            $this->form->setVal('numero_bureau', $this->valF['numero_bureau']);
        }
    }
    
    // }}}

    /**
     *
     */
    function getSelectOldValue(&$form, $maj, &$db, &$contenu, $sql_by_id, $table, $val = null) {

        if ($val == null) {
            $val = $this->form->val[$table];
        }

        $sql_by_id = str_replace('<idx>', $val, $sql_by_id);

        // Recuperation de la valeur depuis la base de donnes.
        $result = $db->query($sql_by_id);

        // Logger
        $this->addToLog("setSelect()[gen/obj]: db->query(\"".$sql_by_id."\");", VERBOSE_MODE);
        if (database::isError($result)) {
            die($res->getMessage().$sql_by_id);
        }

        while ($row =& $result->fetchRow()) {

            //
            if ($contenu[0][0] == '') {
                $contenu[0] = array_merge(array($contenu[0][0]),
                                    array($row[0]),
                                    array_slice($contenu[0], 1));

                $contenu[1] = array_merge(array($contenu[1][0]),
                                    array($row[1]),
                                    array_slice($contenu[1], 1));

            //
            } else {
                $contenu[0] = array_merge(array($row[0]),
                                          $contenu[0]);
                $contenu[1] = array_merge(array($row[1]),
                                          $contenu[1]);
            }
        }
    }


    /**
     * Initialisation des valeurs des champs HTML <select>
     *
     * @param formulaire $form formulaire
     * @param DB $db instance de connexion a la base de donnees
     * @param int $maj type d action (0:ajouter, 1:modifier, etc.)
     * @param boolean $debug mode debug (true:activee)
     * @param string $field nom du champ <select> a initialiser
     * @param string $sql requete de selection des valeurs du <select>
     * @param string $sql_by_id requete de selection valeur par identifiant
     * @param string $om_validite permet de définir si l'objet lié est affecté par une date de validité
     * @param string $multiple permet d'utiliser cette méthode pour configurer l'affichage de select_multiple (widget)
     */

    function init_select(&$form = null, &$db = null, $maj, $debug, $field, $sql,
                         $sql_by_id, $om_validite = false, $multiple = false) {

        // MODE AJOUTER et MODE MODIFIER
        if ($maj < 2) {

            $contenu = array();
            $res = $db->query($sql);
            $this->addToLog("init_select(): db->query(\"".$sql."\");", VERBOSE_MODE);
            // verification d'une eventuelle erreur
            if (database::isError($res)) {
                die($res->getMessage().$sql);
            } else {
                // Initialisation du select
                $contenu[0][0] = '';
                $contenu[1][0] = _('choisir')."&nbsp;"._($field);
                //
                $k=1;
                while($row =& $res->fetchRow()){
                    $contenu[0][$k] = $row[0];
                    $contenu[1][$k] = $row[1];
                    $k++;
                }

                // gestion des objets a date de validite
                if ($om_validite == true) {

                    // ajout de la valeur manquante a $contenu si necessaire
                    if ($maj == 1) {

                        if (!in_array($this->form->val[$field], $contenu[0])) {

                            //
                            $this->getSelectOldValue($form, $maj, $db, $contenu,
                                                     $sql_by_id, $field);
                        } else {

                            $empty = true;
                            foreach ($this->form->val as $f => $value) {
                                if (!empty($value)) {
                                    $empty = false;
                                }
                            }

                            //
                            if ($empty == true and
                                $_POST[$field] != '') {

                                $this->getSelectOldValue($form, $maj, $db,
                                                         $contenu, $sql_by_id,
                                                         $field,
                                                         $_POST[$field]);
                            }
                        }
                    }
                }
                // Initialisation des valeurs dans le formulaire
                $form->setSelect($field, $contenu);
            }
        }

       // MODE SUPPRIMER et MODE CONSULTER
        if ($maj == 2 or $maj == 3) {
            // Initialisation du select
            $contenu[0][0] = '';
            $contenu[1][0] = '';

            if (isset($this->form->val[$field]) and
                !empty($this->form->val[$field]) and $sql_by_id) {
                // Dans le cas d'un select_multiple
                if ($multiple == true) {
                    // Permet de gérer le cas ou les clés primaires sont alphanumériques
                    $val_field = "'".str_replace(",", "','",$this->form->val[$field])."'";
                } else {
                    $val_field = $this->form->val[$field];
                }

                // ajout de l'identifiant recherche a la requete
                $sql_by_id = str_replace('<idx>', $val_field, $sql_by_id);

                // execution
                $result = $db->query($sql_by_id);
                $this->addToLog("init_select(): db->query(".$sql_by_id.");", VERBOSE_MODE);
                if (database::isError($result)) {
                   die($result->getMessage().$sql_by_id);
                }
                // Affichage de la première ligne d'aide à la saisie
                $row =& $result->fetchRow();
                $contenu[0][0] = $row[0];
                $contenu[1][0] = $row[1];
                
                $k=1;
                while($row =& $result->fetchRow()){
                    $contenu[0][$k] = $row[0];
                    $contenu[1][$k] = $row[1];
                    $k++;
                }
            }

            $form->setSelect($field, $contenu);
        }
    }

}

?>

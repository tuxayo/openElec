<?php
/**
 * Ce fichier permet de definir la classe electeur_jury
 * cette classe permet la modification des informations
 * de l'électeur pour le jury d'assises
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once "../obj/electeur.class.php";

/**
 *
 */
class electeur_jury extends electeur {

    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "electeur";

    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "id_electeur";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "N";

    /**
     *
     */
    function electeur_jury($id, &$db, $DEBUG = false) {
        //
        $this->constructeur($id, $db);
    }

    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType (&$form, $maj) {
        if ($maj < 2) { 
            //ajouter et modifier
            // HYPOTHESE NON MODIFICATION *******************************
            for ($i=0;$i<count($this->champs);$i++) {
               $form->setType($this->champs[$i],'hidden');
            }
            
            $form->setType('jury', 'select');
            $form->setType('jury_effectif', 'select');    
            $form->setType('date_jeffectif','date');
            $form->setType('profession','text');
            $form->setType('motif_dispense_jury','text');
        }
    }

    /**
     * Méthode de mise en forme des données retournées par le formulaire
     */
    function setValF($val) {
        parent::setValF($val);
        $this->valF['date_naissance']=$this->dateDBToForm($val['date_naissance']);

    }
    /**
     * Parametrage du formulaire - Groupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setGroupe (&$form, $maj) {
        $form->setGroupe('jury','D');
        $form->setGroupe('jury_effectif','G');
        $form->setGroupe('date_jeffectif','F');
    }

    /**
     * Parametrage du formulaire - Regroupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setRegroupe (&$form, $maj) {
       
        // Carte en retour & Jury
        $form->setRegroupe('jury','D',_("Jure d'assises"), "jure_assise_info");
        $form->setRegroupe('jury_effectif','G','');
        $form->setRegroupe('date_jeffectif','G','');
        $form->setRegroupe('profession','G','');
        $form->setRegroupe('motif_dispense_jury','F','');
        
    }
    /**
     * Méthode pour convertir une date Y-m-d en d/m/Y
     */
    function dateDBToForm($date) {
        if($date == "") {
            return "";
        }
        $dateFormat = new DateTime($date);
        return $dateFormat->format('d/m/Y');
    }



}
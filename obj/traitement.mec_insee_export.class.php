<?php
/**
 * Ce fichier declare la classe mecInseeExportTraitement permettant le traitement
 * des données à exporter ainsi que la création du fichier.
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/traitement.class.php";

/**
 *
 */
class mecInseeExportTraitement extends traitement {
    
    var $fichier = "mec_insee_export";
    var $champs = array("format");
    
    function setContentForm() {
        //
        $this->form->setLib("format", _("Format du fichier d'export"));
        $this->form->setType("format", "select");
        $contenu = array(
            0 => array("txt"),
            1 => array("txt"),
            );
        $this->form->setSelect("format",$contenu);
    }
    
    function getValidButtonValue() {
        //
        return _("Generation du fichier MEC INSEE");
    }
    
    function treatment () {
        //
        $this->LogToFile ("start mec_insee_export");

        $nbelecteur = 0;
        // Récupération de la date tableau
        $datetableau = $this->page->collectivite['datetableau'];
        // Récupération du dossier d'enregistrement des fichiers
        $chemin = $this->page->getParameter("chemin_cnen");
        //
        include "../sql/".$this->page->phptype."/mec_insee_export.inc";
        
        // Execution de la requête 
        $this->LogToFile ("REQUETE DE PRESENTATION DE L'EXPORT");
        // Récupération des infos de la commune
        $res_select_commune = $this->page->db->query($sqlcom);
        // Gestion des erreurs
        if (database::isError($res_select_commune, true)) {
            //
            $this->error = true;
            //
            $message = $res_select_commune->getMessage()." erreur sur ".$sqlcom."";
            $this->LogToFile($message);
        } else {

            $result = $res_select_commune->fetchrow(DB_FETCHMODE_ASSOC);

            // Gestion des champs obligatoires
            if ($result['inseeville']=="" OR
                $result['ville']=="" OR
                $result['tel']=="" OR
                $result['siret']=="") {
                //
                $this->error = true;
                //
                $message = _("Manque d'informations concernant la commune : completer le parametrage.");
                $this->LogToFile($message);
            } else {
                // Création du nom de fichier
                $idmec = "ID".$result['inseeville'];
                if($result['typeliste'] == 'p') {
                    $idmec .= "LP";
                } else {
                    $idmec .= $result['typeliste'];
                }
                $idmec .= intval(substr($datetableau, 0, 4))-1;
                $idmec .= "01";
                $result['id'] = $idmec;
                $nom_fichier = $idmec."_mecinsee_".date("dmy_Gis")."_001.csv";
                $fichier = $chemin.$nom_fichier;
                // Création du fichier
                $inf = fopen($fichier, "w");
                // Insertion de la ligne de présentation
                fputcsv($inf, array_values($result), ";");
                // Récupération des électeurs
                $res_select_elec = $this->page->db->query($sqlelec);
                if (database::isError($res_select_elec, true)) {
                    //
                    $this->error = true;
                    //
                    $message = $res_select_elec->getMessage()." erreur sur ".$sqlelec."";
                    $this->LogToFile($message);
                } else {
                    while($row = $res_select_elec->fetchrow(DB_FETCHMODE_ASSOC)) {
                        $row['numero_electeur'] = "ID".$row['numero_electeur'];
                        fputcsv($inf, $row,";");
                    }
                //
                    $nbelecteur = $res_select_elec->numRows();
                    $this->LogToFile($nbelecteur." "._("electeurs(s) a envoyer a l'INSEE"));
                    return $nom_fichier;
                }
                fclose ($inf);
            }
        }
        //
        $this->LogToFile ("end insee_export");
        if($this->error === false) {
            return $nom_fichier;
        } else {
            return false;
        }
    }
        
}

?>
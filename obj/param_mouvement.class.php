<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class param_mouvement extends dbForm {
    
    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "param_mouvement";
    
    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "code";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "A";
    
    /**
     * Constructeur 
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function param_mouvement($id, &$db, $DEBUG) {
        //
        $this->constructeur($id, $db, $DEBUG);
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en vue de l'insertion des donnees dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setvalF($val) {
        //
        $this->valF["libelle"] = $val["libelle"];
        $this->valF["typecat"] = $val["typecat"];
        $this->valF["effet"] = $val["effet"];
        $this->valF["cnen"] = $val["cnen"];
        $this->valF["codeinscription"] = $val["codeinscription"];
        $this->valF["coderadiation"] = $val["coderadiation"];
        $this->valF["edition_carte_electeur"] = $val["edition_carte_electeur"];
        $this->valF["insee_import_radiation"] = $val["insee_import_radiation"];
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * d'effectuer des tests d'integrite sur la cle primaire pour verifier
     * si la cle a bien ete saisie dans le formulaire et si l'objet ajoute
     * n'existe pas deja dans la table pour en empecher l'ajout.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function verifierAjout($val, &$db) {
        // Si la cle primaire est vide alors on rejette l'enregistrement
        if (trim($this->valF[$this->clePrimaire]) == "") {
            $this->correct = false;
            $this->msg .= $this->form->lib[$this->clePrimaire];
            $this->msg .= _(" est obligatoire")."<br />";
        }
        // Si le test precedent a ete reussi alors on teste si l'identifiant
        // n'existe pas deja dans la table
        if ($this->correct == true) {
            // Construction de la requete
            $sql = "select count(*) from ".$this->table." ";
            if ($this->typeCle == "A") {
                $sql .= "where ".$this->clePrimaire."='".$val[$this->clePrimaire]."'";
            } else {
                $sql .= "where ".$this->clePrimaire."=".$val[$this->clePrimaire]."";
            }
            // Execution de la requete
            $nb = $db->getone($sql);
            if (database::isError($nb)) {
                // Appel de la methode qui gere les erreurs
                $this->erreur_db($nb->getDebugInfo(), $nb->getMessage(), '');
            } else {
                // Si le resultat de la requete renvoit un ou plusieurs
                // enregistrements
                if ($nb > 0) {
                    // On rejette l'enregistrement
                    $this->correct = false;
                    $this->msg .= _("Il existe deja un enregistrement avec cet identifiant, vous devez en choisir un autre")."<br />";
                }
            }
        }
    }
    
    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @return void
     */
    function verifier($val = array(), &$db = NULL, $DEBUG = false) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Si la valeur est vide alors on rejette l'enregistrement
        $field_required = array("libelle", "typecat", "cnen", "effet");
        foreach($field_required as $field) {
            if (trim($this->valF[$field]) == "") {
                $this->correct = false;
                $this->msg .= $this->form->lib[$field]." ";
                $this->msg .= _("est obligatoire")."<br />";
            }
        }
        //
        if ($this->correct == true) {
            $msg = "";
            if ($this->valF['cnen'] == "Oui") {
                if ($this->valF['typecat']=="Inscription") {
                    if ($this->valF['codeinscription']=='' || $this->valF['coderadiation']!='') {
                        $msg .= "* "._("Vous avez selectionne un code RADIATION pour le transfert INSEE alors que la categorie du mouvement est INSCRIPTION.")."<br/>";
                        $this->correct=false;
                    }
    
                }
                if ($this->valF['typecat']=="Radiation") {
                    if ($this->valF['coderadiation']=='' || $this->valF['codeinscription']!='') {
                        $msg .= "* "._("Vous avez selectionne un code INSCRIPTION pour le transfert INSEE alors que la categorie du mouvement est RADIATION.")."<br/>";
                        $this->correct=false;
                    }
                }
                if ($this->valF['typecat']=="Modification") {
                    $msg .= "* "._("Vous ne pouvez pas transferer a l'INSEE des mouvements ayant la categorie MODIFICATION.")."<br/>";
                    $this->correct=false;
                }
            } else {
                if ($this->valF['codeinscription']!='' || $this->valF['coderadiation']!='') {
                    $this->correct=false;
                    if ($this->valF['codeinscription']!='') {
                        $msg .= "* "._("Vous ne pouvez pas selectionner un code INSCRIPTION si vous n'activez pas le transfert INSEE.")."<br/>";
                    }
                    if ($this->valF['coderadiation']!='') {
                        $msg .= "* "._("Vous ne pouvez pas selectionner un code RADIATION si vous n'activez pas le transfert INSEE.")."<br/>";
                    }
                }
            }
            //
            if ($this->valF['insee_import_radiation'] != "") {
                if ($this->valF['typecat']=="Inscription" || $this->valF['typecat']=="Modification") {
                    $this->correct=false;
                    $msg .= "* "._("Vous ne pouvez pas selectionner de code RADIATION en correspondance pour l'import INSEE si la categorie de mouvement n'est pas RADIATION.")."<br/>";
                }
            }
            if ($this->correct==false) {
                $this->msg .= _("Probleme(s) dans les parametre de lien(s) avec l'INSEE")." :<br/>".$msg;
            }
        }
        //
        if ($this->correct == true) {
            $sql = "select code, insee_import_radiation from param_mouvement where typecat='Radiation'";
            $res = $this->db->query($sql);
            if (database::isError($res)) {
                die();
            } else {
                $param_mouvement = array();
                $insee_import_radiation_new = explode(";", $this->valF['insee_import_radiation']);
                while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                    if ($val["code"] != $row["code"] && $row["insee_import_radiation"] != "") {
                        $param_mouvement[$row["code"]] = explode(";",$row["insee_import_radiation"]);
                    }
                }
                foreach ($insee_import_radiation_new as $code_insee) {
                    //
                    foreach ($param_mouvement as $key => $insee_import_radiation_old) {
                        if (in_array($code_insee, $insee_import_radiation_old)) {
                            $this->correct=false;
                            $this->msg.= _("La correspondance du motif de radiation")." ".$code_insee." "._("est deja utilise par un autre type de mouvement")." : ".$key.".<br />";
                        }
                    }
                }
                
            }
        }
        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(_('La date de fin de validite doit etre future a la date de debut de validite.'));
            }
        }
    }

    /**
     *
     */
    function cleSecondaire($id, &$db = NULL, $val = array(), $DEBUG = false) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        //
        $this->rechercheTable($db, "mouvement", "types", $id, $DEBUG);
    }

    /**
     * Parametrage du formulaire - Valeurs pre-remplies
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param integer $validation 
     * 
     * @return void
     */
    function setVal(&$form, $maj, $validation) {
        //
        if ($validation == 0 and $maj == 0) {
            $form->setVal("typecat", "Modification");
            $form->setVal("effet", "1erMars");
            $form->setVal("cnen", "Non");
            $form->setVal("codeinscription", "");
            $form->setVal("coderadiation", "");
            $form->setVal("edition_carte_electeur", 0);
        }
    }
    
    /**
     * Parametrage du formulaire - Groupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setGroupe(&$form, $maj) {
        //
        $form->setGroupe("code", "D");
        $form->setGroupe("libelle", "F");
        //$form->setGroupe("codeinscription", "D");
        //$form->setGroupe("coderadiation", "F");
        $form->setGroupe("typecat", "D");
        $form->setGroupe("effet", "F");
    }
    
    /**
     * Parametrage du formulaire - Regroupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setRegroupe(&$form,$maj) {
        //
        $form->setRegroupe("code", "D", _("Mouvement"));
        $form->setRegroupe("libelle", "F", "");
        //
        $form->setRegroupe("cnen", "D", _("Lien(s) avec l'INSEE"));
        $form->setRegroupe("codeinscription", "G", "");
        $form->setRegroupe("coderadiation", "G", "");
        $form->setRegroupe("insee_import_radiation", "F", "");
        //
        $form->setRegroupe("typecat", "D", _("Parametres"));
        $form->setRegroupe("effet", "G", "");
        $form->setRegroupe("edition_carte_electeur", "F", "");
    }
    
    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType(&$form, $maj) {
        //
        if ($maj < 2) { // ajouter et modifier
            $form->setType("typecat", "select");
            $form->setType("effet", "select");
            $form->setType("edition_carte_electeur", "select");
            $form->setType("cnen", "select");
            $form->setType("codeinscription", "select");
            $form->setType("coderadiation", "select");
            $form->setType("insee_import_radiation", "checkbox_multiple");
            $form->setType('om_validite_debut','date');
            $form->setType('om_validite_fin','date');
            if ($maj == 1) { // modifier
                $form->setType("code", "hiddenstatic");
                $form->setType("typecat", "hiddenstatic");
            } else { // ajouter
            }
        } else { // supprimer
            $form->setType("code", "hiddenstatic");
        }
    }
    
    /**
     * Parametrage du formulaire - Select des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function setSelect(&$form, $maj, $db, $DEBUG) {
        //
        if ($maj < 2) {
            //
            $contenu[0] = array("Inscription", "Radiation", "Modification");
            $contenu[1] = array(_("Inscription"), _("Radiation"), _("Modification"));
            $form->setSelect("typecat",$contenu);
            //
            $contenu[0] = array("1erMars", "Immediat", "Election");
            $contenu[1] = array(_("1erMars"), _("Immediat"), _("Election"));
            $form->setSelect("effet",$contenu);
            //
            $contenu[0] = array("Oui", "Non");
            $contenu[1] = array(_("Oui"), _("Non"));
            $form->setSelect("cnen",$contenu);
            //
            $contenu[0] = array("", "1", "2", "8");
            $contenu[1] = array(_("Sans"), _("1 Normal"), _("2 Inscription juridique"), _("8 Inscription Office"));
            $form->setSelect("codeinscription",$contenu);
            //
            $contenu[0] = array("", "P", "D", "J", "E");
            $contenu[1] = array(_("Sans"), _("(P)erte de qualite requise par la loi"), _("(D)eces"), _("(J)Decision tribunal"), _("(E)rreur materiel"));
            $form->setSelect("coderadiation",$contenu);
            //
            $contenu[0] = array("2", "1", "0");
            $contenu[1] = array(_("Lors de changement de bureau"), _("Toujours"), _("Jamais"));
            $form->setSelect("edition_carte_electeur",$contenu);
            //
            $contenu[0] = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9",);
            $contenu[1] = array(_("0 - inscription d'office dans une autre commune"),
                                _("1 - deces"),
                                _("2 - changement de commune d'inscription (nouvelle inscription sur decision de la commission administrative)"),
                                _("3 - changement de commune d'inscription (nouvelle inscription sur decision judiciaire)"),
                                _("4 - n'a pas atteint l'age electoral"),
                                _("5 - etat civil incontrolable"),
                                _("6 - inscription volontaire annulant l'inscription d'office dans votre commune"),
                                _("7 - decision de tutelle privative de la capacite electorale"),
                                _("8 - condamnation privative de la capacite electorale"),
                                _("9 - perte de nationalite francaise"),
                                );
            $form->setSelect("insee_import_radiation",$contenu);
        }
    }
    
    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        $form->setLib("code", _("Code"));
        $form->setLib("effet", _("Date d'effet"));
        $form->setLib("typecat", _("Categorie du mouvement"));
        $form->setLib("edition_carte_electeur", _("Edition de la carte d'electeur"));
        $form->setLib("cnen", _("Transferer ce type de mouvement a l'INSEE ?"));
        $form->setLib("codeinscription", _("Quelle est la correspondance avec le code motif de l'inscription de l'INSEE ? (A selectionner uniquement si la categorie du mouvement est INSCRIPTION et que le transfert est a OUI)"));
        $form->setLib("coderadiation", _("Quelle est la correspondance avec le code motif de la radiation de l'INSEE ? (A selectionner uniquement si la categorie du mouvement est INSCRIPTION et que le transfert est a OUI)"));
        $form->setLib("insee_import_radiation", _("Quelle sont les correspondances avec les valeurs du code motif de la radiation dans les fichiers d'import de radiations INSEE ?  (A selectionner uniquement si la categorie du mouvement est RADIATION)"));
        $form->setLib('om_validite_debut',_('Debut de validite (avant cette date le mouvement ne sera pas selectionnable)'));
        $form->setLib('om_validite_fin',_('Fin de validite (apres cette date le mouvement ne sera pas selectionnable)'));
    }
    
    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setOnchange(&$form, $maj) {
        //
        $form->setOnchange("code", "this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle", "this.value=this.value.toUpperCase()");
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
    }
    
    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille(&$form, $maj) {
        //
        $form->setTaille("code", 3);
        $form->setTaille("libelle", 40);
        $form->setTaille("insee_import_radiation", 10);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
    }
    
    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax(&$form, $maj) {
        //
        $form->setMax("code", 3);
        $form->setMax("libelle", 40);
        $form->setMax("insee_import_radiation", 10);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
        
    }
    
}

?>
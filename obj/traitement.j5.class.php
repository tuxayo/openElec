<?php

require_once ("../obj/traitement.class.php");

class j5Traitement extends traitement {
    
    var $fichier = "j5";

    var $champs = array("liste", "mouvementatraiter", ); 

    function setContentForm() {
        //
        $this->form->setLib("mouvementatraiter", _("Le traitement J-5 applique le(s) tableau(x) selectionne(s) :"));
        $this->form->setType("mouvementatraiter", "checkbox_multiple_disabled");
        //
        $mouvements_io = "select code, libelle, effet from mouvement inner join param_mouvement ";
        $mouvements_io .= " on mouvement.types=param_mouvement.code ";
        $mouvements_io .= " where (effet='Election' or effet='Immediat') ";
        $mouvements_io .= " and date_tableau='".$this->page->collectivite["datetableau"]."' ";
        $mouvements_io .= " and etat='actif' ";
        $mouvements_io .= " group by code, libelle, effet ";
        $mouvements_io .= " order by effet DESC, code ";
        //
        $res = $this->page->db->query($mouvements_io);
        //
        $this->page->addToLog("trt/traitement_j5.php: db->query(\"".$mouvements_io."\")", EXTRA_VERBOSE_MODE);
        //
        $this->page->isDatabaseError($res);
        //
        $contenu = array();
        $contenu[0] = array("Immediat",);
        $contenu[1] = array(_("Tableau des cinq jours"),);
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $this->page->addToLog("trt/traitement_j5.php: ".print_r($row, true), EXTRA_VERBOSE_MODE);
            if ($row["effet"] == "Election") {
                array_push($contenu[0], $row["code"]);
                array_push($contenu[1], _("Tableau des additions")." - ".$row["libelle"]);
            }
        }
        $this->form->setSelect("mouvementatraiter", $contenu);
        //
        $cinqjours = false;
        $additions = array();
        $mouvementatraiter = "";
        if (isset($_POST["mouvementatraiter"])) {
            $val = "";
            foreach($_POST["mouvementatraiter"] as $elem) {
                if ($elem == "Immediat") {
                    $cinqjours = true;
                } else {
                    array_push($additions, $elem);
                }
                $val .= $elem.";";
            }
            $mouvementatraiter = $val;
            $this->form->setVal("mouvementatraiter", $val);
        }
        $params = array("cinqjours" => $cinqjours,
                        "additions" => $additions,
                        "mouvementatraiter" => $mouvementatraiter);
        $this->setParams($params);
        //
        $this->form->setLib("liste", _("Le traitement J-5 s'applique sur la liste :"));
        $this->form->setType("liste", "statiq");
        $this->form->setVal("liste", $_SESSION["liste"]." - ".$_SESSION["libelle_liste"]);

    }
    
    function getValidButtonValue() {
        //
        return _("Appliquer le traitement J-5");
    }

    function displayAfterContentForm() {
        //
        $cinqjours = $this->params["cinqjours"];
        $additions = $this->params["additions"];
        $mouvementatraiter = $this->params["mouvementatraiter"];

        // Format Date de tableau
        $datetableau = $this->page->collectivite['datetableau'];
        
        // Inclusion du fichier de requêtes
        include ("../sql/".$this->page->phptype."/trt_j5.inc");

        //
        echo "<h4>";
        echo _("Recapitulatif du traitement :");
        echo "</h4>";

        // Debut Tableau
        echo "\n<table class='tabCol'>\n";
        
        // NB electeur avant traitement
        $nbElecteur = $this->page->db->getone ($query_count_electeur);
        $this->page->isDatabaseError($nbElecteur);
        echo "\t<tr class='tabCol'>";
        echo "<td class=\"link\" rowspan=\"5\">";
        echo "<div class=\"choice ui-corner-all ui-widget-content\">";
        echo "<span>";
        echo "<a class=\"om-prev-icon edition-16\" ";
        echo "title=\"Ce document contient le detail par bureau des mouvements, le listing des ";
        echo "inscriptions, des modifications et des radiations a appliquer lors du traitement J-5. ";
        echo "Une fois le traitement applique, il est possible de retrouver ce recapitulatif dans le menu 'Edition -> Revision Electorale'.\" ";
        echo "target=\"_blank\" href=\"../app/pdf_recapitulatif_traitement.php?action=traitement&amp;traitement=j5&amp;mouvementatraiter=".$mouvementatraiter."\">";
        echo "Cliquer ici pour visualiser le recapitulatif complet du traitement";
        echo "</a>";
        echo "</span>";
        echo "</div>";
        echo "</td>";
        echo "<td class=\"libelle\">Nombre d'electeurs avant traitement J-5</td>";
        echo "<td class=\"total\">".$nbElecteur."</td>";
        echo "</tr>\n";
        
        //  NB inscription
        $nbInscription = $this->page->db->getone ($query_count_inscription);
        $this->page->isDatabaseError($nbInscription);
        echo "\t<tr class='tabData'>";
        echo "<td class=\"libelle nb-electeur\">Inscription(s)</td>";
        echo "<td class=\"total\">".$nbInscription."</td>";
        echo "</tr>\n";
        
        // NB modification
        $nbModification = $this->page->db->getone ($query_count_modification);
        $this->page->isDatabaseError($nbModification);
        echo "\t<tr class='tabData'>";
        echo "<td class=\"libelle\">Modification(s)</td>";
        echo "<td class=\"total\">".$nbModification."</td>";
        echo "</tr>\n";

        // NB radiation
        $nbRadiation = $this->page->db->getone ($query_count_radiation);
        $this->page->isDatabaseError($nbRadiation);
        echo "\t<tr class='tabData'>";
        echo "<td class=\"libelle\">Radiation(s)</td>";
        echo "<td class=\"total\">".$nbRadiation."</td>";
        echo "</tr>\n";
        
        // NB electeur apres traitement
        $nbElecteurApres = $nbElecteur + $nbInscription - $nbRadiation;
        echo "\t<tr class='tabCol'>";
        echo "<td class=\"libelle\">Nombre d'electeurs apres traitement J-5</td>";
        echo "<td class=\"total\">".$nbElecteurApres."</td>";
        echo "</tr>\n";
        echo "</table>";
    }

    function treatment () {
        //
        $this->LogToFile ("start j5");
        //
        $datetableau = $this->page->collectivite['datetableau'];
        //
        
        //
        $cinqjours = false;
        $additions = array();
        //
        if (isset($_POST["mouvementatraiter"])) {
            $val = "";
            foreach($_POST["mouvementatraiter"] as $elem) {
                if ($elem == "Immediat") {
                    $cinqjours = true;
                } else {
                    array_push($additions, $elem);
                }
                $val .= $elem.";";
            }
        } else {
            foreach($this->params["mouvementatraiter"] as $elem) {
                if ($elem == "Immediat") {
                    $cinqjours = true;
                } else {
                    array_push($additions, $elem);
                }
                $val .= $elem.";";
            }
        }

        //
        require_once ("../obj/electeur.class.php");
        include ("../sql/".$this->page->phptype."/trt_j5.inc");
        
        // Traitement RADIATIONS
        $res_select_radiation = $this->page->db->query ($query_select_radiation);
        if (database::isError($res_select_radiation)) {
            //
            $this->error = true;
            //
            $message = $res_select_radiation->getMessage ()." erreur sur ".$query_select_radiation."";
            $this->LogToFile ($message);
        } else {
            $this->LogToFile ("TRAITEMENT DES RADIATIONS");
            $this->LogToFile ($query_select_radiation);
            while ($row =& $res_select_radiation->fetchRow (DB_FETCHMODE_ASSOC)) {
                // suppression ELECTEUR
                $enr = new electeur ($row['id_electeur'], $this->page->db, 0);
                $enr->supprimerTraitement ($row, $this->page->db, 0) ;
                $message = "-> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom']." - ".$enr->msg."";
                $this->LogToFile ($message);
                // maj MOUVEMENT
                $fields_values = array(
                    'etat'    => 'trs',
                    'tableau' => 'j5',
                    'date_j5' => ''.$enr->dateSystemeDB ().'');
                $res1 = $this->page->db->autoExecute ("mouvement", $fields_values, DB_AUTOQUERY_UPDATE, "id=".$row['id']);
                if (database::isError($res1)) {
                    //
                    $this->error = true;
                    //
                    $message = $res1->getMessage ()." - ".$res1->getUserInfo ();
                    $this->LogToFile ($message);
                    //
                    break;
                } else {
                    //
                    $message = "l'enregistrement ".$row['id']." de la table Mouvement est modifie";
                    $this->LogToFile ($message);
                }
            }
            $res_select_radiation->free ();
        }
        
        // Traitement INSCRIPTIONS
        $res_select_inscription = $this->page->db->query ($query_select_inscription);
        if (database::isError($res_select_inscription)) {
            //
            $this->error = true;
            //
            $message = $res_select_inscription->getMessage ()." erreur sur ".$query_select_inscription."";
            $this->LogToFile ($message);
        } else {
            $this->LogToFile ("TRAITEMENT DES INSCRIPTIONS");
            $this->LogToFile ($query_select_inscription);
            while ($row =& $res_select_inscription->fetchRow (DB_FETCHMODE_ASSOC)) {
                // ajout ELECTEUR
                $enr = new electeur ("]", $this->page->db, 0);
                $enr->valF['tableau'] = 'j5';
                $enr->ajouterTraitement ($row, $this->page->db, 0, $this->page->collectivite['datetableau']);
                $message = "-> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom']." - ".$enr->msg."";
                $this->LogToFile ($message);
                // maj MOUVEMENT
                $fields_values = array(
                    'etat'    => 'trs',
                    'tableau' => 'j5',
                    'id_electeur' => $enr->valF["id_electeur"],
                    'date_j5' => ''.$enr->dateSystemeDB ().'');
                $res1 = $this->page->db->autoExecute ("mouvement", $fields_values, DB_AUTOQUERY_UPDATE, "id=".$row['id']);
                if (database::isError($res1)) {
                    //
                    $this->error = true;
                    //
                    $message = $res1->getMessage ()." - ".$res1->getUserInfo ();
                    $this->LogToFile ($message);
                    //
                    break;
                } else {
                    //
                    $message = "l'enregistrement ".$row['id']." de la table Mouvement est modifie";
                    $this->LogToFile ($message);
                }
            }
            $res_select_inscription->free ();
        }
        
        // Traitement MODIFICATIONS
        $res_select_modification = $this->page->db->query ($query_select_modification);
        if (database::isError($res_select_modification)) {
            //
            $this->error = true;
            //
            $message = $res_select_modification->getMessage ()." erreur sur ".$query_select_modification."";
            $this->LogToFile ($message);
        } else {
            $this->LogToFile ("TRAITEMENT DES MODIFICATIONS");
            $this->LogToFile ($query_select_modification);
            while ($row =& $res_select_modification->fetchRow (DB_FETCHMODE_ASSOC)) {
                // maj ELECTEUR
                $enr = new electeur($row['id_electeur'], $this->page->db, 0);
                $enr->valF['tableau'] = 'j5';
                $enr-> modifierTraitement ($row, $this->page->db, 0, $this->page->collectivite['datetableau']) ;
                $message = "-> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom']." - ".$enr->msg."";
                $this->LogToFile ($message);
                // maj MOUVEMENT
                $fields_values = array(
                    'etat'    => 'trs',
                    'tableau' => 'j5',
                    'date_j5' => ''.$enr->dateSystemeDB ().'');
                $res1 = $this->page->db->autoExecute ("mouvement", $fields_values, DB_AUTOQUERY_UPDATE, "id=".$row['id']);
                if (database::isError($res1)) {
                    //
                    $this->error = true;
                    //
                    $message = $res1->getMessage ()." - ".$res1->getUserInfo ();
                    $this->LogToFile ($message);
                    //
                    break;
                } else {
                    //
                    $message = "l'enregistrement ".$row['id']." de la table Mouvement est modifie";
                    $this->LogToFile ($message);
                }
            }
            $res_select_modification->free ();
        }
        //
        $this->LogToFile ("end j5");
    }
}

?>
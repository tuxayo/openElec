<?php
/**
 * Ce fichier permet de definir la classe mairieeurope
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."dbformdyn.class.php";

/**
 *
 */
class mairieeurope extends dbForm {
    
    /**
     * Identifiant de la table dans la base de donnees
     * @var string 
     */
    var $table = "mairieeurope";
    
    /**
     * Identifiant du champ cle primaire de cette table dans la base de donnees
     * @var string 
     */
    var $clePrimaire = "id_electeur_europe";
    
    /**
     * Type de donnees du champ cle primaire de cette table dans la base de
     * donnees :
     * - A => alphanumerique
     * - N => numerique
     * @var string 
     */
    var $typeCle = "N";
    
    /**
     * Constructeur 
     *
     * @param string $id Identifiant (cle primaire) de l'objet dans la base
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     *
     * @return void
     */
    function mairieeurope($id, &$db, $DEBUG) {
        //
        $this->constructeur($id, $db, $DEBUG);
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en vue de l'insertion des donnees dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setvalF($val) {
        //
        $this->valF["id_electeur_europe"] = $val["id_electeur_europe"];
        $this->valF["mairie"] = $val["mairie"];
        $this->valF["date_modif"] = $this->dateSystemeDB();
        $this->valF["utilisateur"] = $_SESSION["login"];
    }
    
    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en ajouter la cle primaire en vue de l'insertion des donnees
     * dans la base.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * 
     * @return void
     */
    function setValFAjout($val) {
        // Id automatique donc cette initialisation n'est pas necessaire
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * de recuperer l'element suivant dans la sequence pour les id automatiques.
     * 
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function setId(&$db) {
        //
    }
    
    /**
     * Cette methode est appelee lors de l'ajout d'un objet, elle permet
     * d'effectuer des tests d'integrite sur la cle primaire pour verifier
     * si la cle a bien ete saisie dans le formulaire et si l'objet ajoute
     * n'existe pas deja dans la table pour en empecher l'ajout.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * 
     * @return void
     */
    function verifierAjout($val, &$db) {
        // Id automatique donc cette verification n'est pas necessaire
        // Verification de l'electeur deja existant en Mairie Europe
        if ($this->valF['id_electeur_europe'] != "") {
            $sql = "select * from mairieeurope where id_electeur_europe=".
                $this->valF['id_electeur_europe'];
            $res = $db->query($sql);
            if (database::isError($res))
                $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
            else {
                $nbligne=$res->numrows();
                if ($nbligne==1) {
                    $this->correct=false;
                    $this->msg .= " "._("electeur")." ".
                        $this->valF['id_electeur_europe']." ".
                        _("deja existant en mairie europe");
                }
            }
        }
    }
    
    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     * 
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function verifier($val, &$db, $DEBUG) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Si la valeur est vide alors on rejette l'enregistrement
        $field_required = array("mairie", "id_electeur_europe");
        foreach($field_required as $field) {
            if (trim($this->valF[$field]) == "") {
                $this->correct = false;
                $this->msg .= $this->form->lib[$field]." ";
                $this->msg .= _("est obligatoire")."<br />";
            }
        }
        //
        if ($this->valF['id_electeur_europe'] != "") {
            /* ++ */ $sql = "select * from electeur where collectivite = '".
                $_SESSION['collectivite']."' and id_electeur=".
                $this->valF['id_electeur_europe'];
            $res = $db->query($sql);
            if($DEBUG>=VERBOSE_MODE) echo $sql;
            if (database::isError($res))
                $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
            else {
                $nbligne=$res->numrows();
                if ($nbligne==0) {
                    $this->correct=false;
                    $this->msg .= " "._("electeur")." ".
                        $this->valF['id_electeur_europe']." ".
                        _(" inexistant ");
                }
            }
        }
        
    }
    
    /**
     * Parametrage du formulaire - Groupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setGroupe(&$form, $maj) {
        //
        $form->setGroupe("id_electeur_europe", "D");
        $form->setGroupe("z_electeur", "F");
    }
    
    /**
     * Parametrage du formulaire - Regroupement des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setRegroupe(&$form, $maj) {
        //
        if ($maj != 0) {
            $form->setRegroupe("id_electeur_europe", "D", _("Identification"));
            $form->setRegroupe("date_modif", "G", "");
            $form->setRegroupe("utilisateur", "F", "");
        }
        //
        $form->setRegroupe("id_electeur_europe", "D", _("Electeur"));
        $form->setRegroupe("z_electeur", "F", "");
    }
    
    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setType(&$form, $maj) {
        //
        if ($maj < 2) { // ajouter et modifier
            if ($maj == 1) { // modifier
                $form->setType("id_electeur_europe", "hiddenstatic");
                $form->setType("date_modif", "hiddenstatic");
                $form->setType("utilisateur", "hiddenstatic");
                $form->setType("z_electeur", "hiddenstatic");
            } else { // ajouter
                $form->setType("z_electeur", "comboG");
                $form->setType("id_electeur_europe", "comboD");
                $form->setType("date_modif", "hidden");
                $form->setType("utilisateur", "hidden");
            }
        } else { // supprimer
            $form->setType("id_electeur_europe", "hiddenstatic");
        }
    }
    
    /**
     * Parametrage du formulaire - Select des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * @param object $db Objet Base de donnees
     * @param integer $DEBUG Mode debug :
     *                       - 0 => Pas de Mode debug
     *                       - 1 => Mode debug
     * 
     * @return void
     */
    function setSelect (&$form, $maj, $db, $DEBUG) {
        if ($maj < 2) { // ajouter et modifier
            // CORREL
            // id_electeur
            $contenu="";
            $contenu[0][0] = "electeur";
            $contenu[0][1] = "id_electeur";
            $contenu[1][0] = "nom";
            $contenu[1][1] = "z_electeur";
            $form->setSelect("id_electeur_europe", $contenu);
            // z_electeur
            $contenu="";
            $contenu[0][0] = "electeur";
            $contenu[0][1] = "nom";
            $contenu[1][0] = "id_electeur";
            $contenu[1][1] = "id_electeur_europe";
            $form->setSelect("z_electeur", $contenu);
        }
    }

    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        if ($maj > 0) {
            $form->setLib("z_electeur", " => ");
        } else {
            $form->setLib("z_electeur", "");
        }
        $form->setLib("id_electeur_europe", _("Identifiant / Nom de l'electeur"));
    }
    
    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setOnchange(&$form, $maj) {
        //
        $form->setOnchange("z_electeur", "this.value=this.value.toUpperCase()");
        $form->setOnchange("mairie", "this.value=this.value.toUpperCase()");
    }
    
    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setTaille(&$form, $maj) {
        //
        $form->setTaille("id_electeur_europe", 8);
        $form->setTaille("z_electeur", 50);
        $form->setTaille("mairie", 40);
    }
    
    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     * 
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     * 
     * @return void
     */
    function setMax(&$form, $maj) {
        //
        $form->setMax("id_electeur_europe", 8);
        $form->setMax("z_electeur", 90);
        $form->setMax("mairie", 40);
    }
    
}
?>
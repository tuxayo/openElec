<?php
/**
 * Ce fichier permet de definir la classe inscription_office_valider
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "inscription.class.php";

/**
 *
 */
class inscription_office_valider extends inscription {

    /**
     *
     */
    function inscription_office_valider($id, &$db, $DEBUG = false) {
        //
        $this->constructeur($id, $db);
    }

    /**
     *
     */
    function verifierAjout($val = array(), &$db = NULL) {
        //
        parent::verifierAjout($val, $db);
        //
    }
    
    /**
     *
     */
    function getDataSubmit() {
        //
        $datasubmit = parent::getDataSubmit();
        //
        $datasubmit .= "&amp;io=".$this->getParameter("io");
        //
        return $datasubmit;
    }

    /**
     *
     */
    function retour($premier = 0, $recherche = "", $tricol = "") {
        
        echo "\n<a class=\"retour\" ";
        echo "href=\"";
        //
        echo "../scr/tab.php";
        echo "?";
        echo "obj=inscription_office";
        echo "&amp;premier=".$this->getParameter("premier");
        echo "&amp;tricol=".$this->getParameter("tricol");
        echo "&amp;recherche=".$this->getParameter("recherche");
        echo "&amp;selectioncol=".$this->getParameter("selectioncol");
        //
        echo "\"";
        echo ">";
        //
        echo _("Retour");
        //
        echo "</a>\n";
        
    }

    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$db, $DEBUG = false) {
        if ($validation == 0) {
            $i=0;
            if ($maj == 0) {
                $form->setVal('situation','');
                $form->setVal('code_nationalite','FRA');
                $form->setVal('bureauforce','Non');
                $form->setVal('liste',$_SESSION['liste']);
                $form->setVal('date_tableau',$this->dateTableau);
                $form->setVal('tableau',"annuel");
                $form->setVal('etat',"actif");
                //
                $sql= "select * from inscription_office where id = ".$this->getParameter("io")." and collectivite = '".$_SESSION['collectivite']."' ";
                $res = $this->db->query($sql);
                if (database::isError($res))
                    $this->erreur_db($res->getDebugInfo(),$res->getMessage(),"");
                while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                    if ($row['types']=='') {
                        include ("../dyn/var.inc");
                        $form->setVal('types',$param_mouvement_inscriptionoffice);
                    } else {
                        $form->setVal('types',$row['types']);
                        $this->msg .= _("ATTENTION, cette inscription est deja effectuee")."<br />";
                        $this->msg .= _("Verifiez avec la recherche doublon")."<br />";
                    }
                    $form->setVal('nom',$row['nom']);
                    $form->setVal('prenom',$row['prenom']);
                    $form->setVal('nom_usage',$row['nom_usage']);
                    $form->setVal('civilite',$row['civilite']);
                    $form->setVal('sexe',$row['sexe']);
                    $form->setVal('date_naissance',$row['date_naissance']);
                    $form->setVal('code_departement_naissance',$row['code_departement_naissance']);
                    $form->setVal('code_lieu_de_naissance',$row['code_lieu_de_naissance']);
                    $form->setVal('libelle_departement_naissance',$row['libelle_departement_naissance']);
                    $form->setVal('libelle_lieu_de_naissance',$row['libelle_lieu_de_naissance']);
                    $form->setVal('observation',substr($row['adresse1']." ".$row['adresse2']." ".$row['adresse3']." ".$row['adresse4']." ".$row['adresse5']." ".$row['adresse6'], 0, 100));
                }
            }
        }
    }

    /**
     *
     */
    function triggerajouter($id, &$db, $val, $DEBUG = false) {
        //
        $param_mouvement_inscriptionoffice = "IO";
        if (file_exists("../dyn/var.inc")) {
            include "../dyn/var.inc";
        }
        //
        $sql = "update inscription_office set types='".$param_mouvement_inscriptionoffice."' where id=".$this->getParameter("io")."";
        //
        $res = $this->db->query($sql);
        if (database::isError($res)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), "");
        }
    }

}

?>

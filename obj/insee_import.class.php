<?php
/**
 * Ce fichier permet de definir la classe insee_import
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 * Appel de la classe mouvement
 */
require_once "mouvement.class.php";

/**
 * ???????
 *  Ne doit pas etendre mouvement
 * 
 */
class insee_import extends mouvement {
    
    /**
     * Constructeur.
     */
    function insee_import () {
    }

    /**
     * Fonction import().
     * @param $type
     * @param $fic
     * @param $f 
     */
    function import ($type, $fic, $f) {
        
        if ($type=="radiation_insee") {
        
            $xml = new XMLReader();
            $xml->open($fic);
            $assoc = new insee_import();
            $assoc = $assoc->xml2assoc($xml);
            $xml->close();
    
            $msg = "";
            $rejet = "";
            
            $f-> db -> autoCommit (false);
          
            $LotDUnitesElectorales = $assoc[0]['value'][1]['value'];
            $i=0;
            foreach ($LotDUnitesElectorales as $key => $value) {
                
                $personne[$i]["collectivite"] = $_SESSION['collectivite'];
                
                $nompers = $value['value'][1]['value'][0]['value'][0]['value'];
                
                foreach ($nompers as $key => $valuenom) {
                    $tag = $valuenom['tag'];
                    $val =  $valuenom['value'];
                    $personne[$i]["nom"] = strtoupper($valuenom['value']);
                }
            
            
                $prenoms = $value['value'][1]['value'][0]['value'][1]['value'];
                $j = 0;
                $k = 0;
                foreach ($prenoms as $keyprenom => $valueprenom) {
                    $k = $j+1;
                    if ($k<=3) {
                        $personne[$i]["prenom".$k] = strtoupper($valueprenom['value']);
                    }
                    $j++;
                }
            
                $personne[$i]["date_naissance"] = $value ['value'][1]['value'][0]['value'][2]['value']; 
                $lieunaiss = $value ['value'][1]['value'][0]['value'][3]['value'];
            
            
                foreach ($lieunaiss as $key => $valuelieunaiss) {
                    $tag = $valuelieunaiss['tag'];
                    if ($tag != "ie:DivisionTerritoriale") {
                        if ($tag == "ie:Localite") {
                            $personne[$i]["localite_naissance"] = strtoupper($valuelieunaiss['value']);
                        }
                        if ($tag == "ie:Pays") {
                            $personne[$i]["pays_naissance"] = strtoupper($valuelieunaiss['value']);
                        }
                    }
                    elseif ($tag == "ie:DivisionTerritoriale") {
                    $personne[$i]["code_lieu_de_naissance"] = strtoupper($valuelieunaiss["attributes"]["code"]);
                    }            
                }
            
                $personne[$i]["sexe"] = $value ['value'][1]['value'][0]['value'][4]['value'];
                $personne[$i]["nationalite"] = $value ['value'][1]['value'][0]['value'][5]['value'];
            
                if(isset($value ['value'][1]['value'][0]['value'][6])) {
                    $adresseelec = $value ['value'][1]['value'][0]['value'][6]['value'];

                    foreach ($adresseelec as $key => $valueadresseelec) {
                        $val =  strtoupper($valueadresseelec['value']);
                        $tag = $valueadresseelec['tag'];
                        $personne[$i]["adresse1"] = $val;
                    }
                }
            
                $personne[$i]["motif_de_radiation"] = $value ['value'][2]["value"][0]['tag'];
            
                if ($personne[$i]["motif_de_radiation"] == "el:Deces") {
                    $personne[$i]["motif_de_radiation"] = "Deces";
                    $personne[$i]["Date_deces"] = $value ['value'][2]["value"][0]['value'][0]['value'];    
            
                    $LieuDeDeces = $value ['value'][2]["value"][0]['value'][1]['value'];
            
                    foreach ($LieuDeDeces as $key => $valueLieuDeDeces) {
                            
                        $tag = $valueLieuDeDeces['tag'];
                        
                        if ($tag == "ie:Localite") {
                           $personne[$i]["localite_deces"] = strtoupper($valueLieuDeDeces['value']);
                        }
                        if ($tag == "ie:Pays") {
                           $personne[$i]["pays_deces"] = strtoupper($valueLieuDeDeces['value']);
                        }
                    
                        elseif ($tag == "ie:DivisionTerritoriale") {
                                $personne[$i]["code_lieu_de_deces"] = strtoupper($valueLieuDeDeces["attributes"]["code"]);
                        }            
                    }
                }
                
                elseif ($personne[$i]["motif_de_radiation"] == "el:NouvelleInscription") {
                    $personne[$i]["motif_de_radiation"] = "Nouvelle Inscription";
                    $personne[$i]["date_nouvelle_inscription"] = $value ['value'][2]["value"][0]['value'][0]['value'];    
                    $personne[$i]["motif_nouvelle_inscription"] = ($value ['value'][2]["value"][0]['value'][1]['value']);
            
                }
            
                elseif ($personne[$i]["motif_de_radiation"] == "el:PerteNationalite") {
                    $personne[$i]["motif_de_radiation"] = "Perte de nationalite";
                    $personne[$i]["code_perte_de_nationalite"] = $value ['value'][2]["value"][0]['value'][0]['value'];    
            
                }
            
                elseif ($personne[$i]["motif_de_radiation"] == "el:AutresMotifs") {
                    $personne[$i]["motif_de_radiation"] = "Autre motifs";
                    $personne[$i]["code_autre_motif_de_radiation"] = $value ['value'][2]["value"][0]['value'];

                }
                $personne[$i]["type_de_liste"] = $value['value'][1]['value'][1]['value'];
                $i++;           
            }
            include ("../sql/".$f -> phptype."/trt_radiation_insee.inc");
            
            
            $i = 0;
            while ($i<$radiationmax) { //1ere ligne *********************************
                $pointeur = $personne[$i];
                $pointeur['id']= $f-> db->nextId("radiation_insee");
                $pointeur = $this->convertToUTF8($pointeur);
                $res= $f-> db->autoExecute("radiation_insee",$pointeur,DB_AUTOQUERY_INSERT);
                if (database::isError($res))
                    die($res->getDebugInfo()." => echec requete insertion radiation_insee");
                $i++;
            }
            $f-> db -> commit ();
            
            ////////////////////////////////
            
            echo "Le transfert s'est effectué : ".($i-1)." radiations ont été insérées.<br />";
            echo "Maintenant il faut valider les radiations une par une.<br />";
            
        }
        
        elseif ($type=="inscription_office") {
            $obj = "cnen_in";
            $fichier = $fic;
           
            $msg = "";
            $rejet = "";
            
            ////////////////////////
            $xml = new XMLReader();
        
            $xml->open($fic);
            $assoc = $this->xml2assoc($xml);
            $xml->close();
            
            $f -> db -> autoCommit (false);    

            $i=0;
            foreach ($assoc[0]['value'][1]['value'] as $key => $value) {
                
                if ( $value['tag'] == "el:PropositionDInscription") {
                    ///// Initialisation $valF ////////////////
                    $valF["collectivite"] = $_SESSION['collectivite'];
                    $valF["sexe"] = "M";
                    $valF["civilite"] = "M.";
                    $valF["nom"] = "";
                    $valF["nom_usage"] = "";
                    $valF["prenom"] = "";
                    $valF["date_naissance"] = "";
                    $valF["adresse1"] = "";
                    $valF["adresse2"] = "";
                    $valF["adresse3"] = "";
                    $valF["adresse4"] = "";
                    $valF["adresse5"] = "";
                    $valF["adresse6"] = "";
                    $valF['idcnen'] = ""; // ??
                    $valF['code_lieu_de_naissance'] = "";
                    $valF['libelle_lieu_de_naissance'] = "";
                    ////////////
                    $valF['code_departement_naissance'] = "";
                    $valF['libelle_departement_naissance'] = "";
                    
                    foreach( $value['value'] as $key1 => $value1) {
                        switch ( $value1['tag'] ) {
                            case "el:Jeune":
                                foreach( $value1['value'] as $key2 => $value2) {
                                    
                                    if( $value2['tag'] == "el:Noms" ){
                                        $cpt = 0;
                                        $nompers = $value2['value'];
                                        foreach ($nompers as $key => $valuenom) {
                                            $tag = $valuenom['tag'];
                                            $val =  $valuenom['value'];
                                            if ($cpt) $valF["nom_usage"] .= strtoupper(utf8_decode($valuenom['value']))." ";
                                            if (!$cpt) $valF["nom"] .= strtoupper(utf8_decode($valuenom['value']))." ";
                                            $cpt++;
                                        }
                                    }
                                    
                                    if( $value2['tag'] == "el:Prenoms" ){
                                        $prenoms = $value2['value'];
                                        $j = 0;
                                        $k = 0;
                                        foreach ($prenoms as $keyprenom => $valueprenom) {
                                            $k = $j+1;
                                            if ($k<=3) {
                                                $valF["prenom"] .= strtoupper(utf8_decode($valueprenom['value']))." ";
                                            }
                                            $j++;
                                        }
                                    }
                                    
                                    if( $value2['tag'] == "el:DateDeNaissance" ){
                                        $valF["date_naissance"] = $value2['value'];
                                    }
                                    
                                    if( $value2['tag'] == "el:LieuDeNaissance" ){
                                        foreach ( $value2['value'] as $key3 => $value3 ) {
                                            if ( $value3['tag'] == "ie:Localite" ) {
                                                $valF['code_lieu_de_naissance'] = 
                                                    ((isset($value3['attributes']))?
                                                        $value3['attributes']['code']
                                                        : ""
                                                    );
                                                $valF['libelle_lieu_de_naissance'] = strtoupper($value3['value']);
                                            }
                                            if ( $value3['tag'] == "ie:Pays" ) {
                                                $valF['code_lieu_de_naissance'] = $value3['attributes']['code'];
                                            }
                                        }
                                    }

                                    if( $value2['tag'] == "el:Sexe" ){
                                        $valF['sexe'] = strtoupper($value2['value']);
                                        if ( $valF["sexe"] == "F" ) $valF["civilite"] = "Mlle";
                                    }
                                    
                                    if( $value2['tag'] == "el:Adresse" ){
                                        $adresseelec = $value2['value'];
                                        if ( $adresseelec[0]["tag"] == "ie:AdressePostale" ) {
                                            foreach ($adresseelec[0]["value"] as $key => $valueadresseelec) {
                                                $val =  $valueadresseelec['value'];
                                                $tag = $valueadresseelec['tag'];
                                                if ($tag == "ie:LigneUne") $valF["adresse1"] = strtoupper($val);
                                                if ($tag == "ie:LigneDeux") $valF["adresse2"] = strtoupper($val);
                                                if ($tag == "ie:LigneTrois") $valF["adresse3"] = strtoupper($val);
                                                if ($tag == "ie:LigneQuatre") $valF["adresse4"] = strtoupper($val);
                                                if ($tag == "ie:LigneCinq") $valF["adresse5"] = strtoupper($val);
                                                if ($tag == "ie:LigneSix") $valF["adresse6"] = strtoupper($val);
                                            }
                                        }
                                    }
                                }
                            break;
                        }
                    }
                    
                    //// Recuperation libelle/code lieu naissance et departement
                    if ( $valF['code_lieu_de_naissance'] != "" ) {
                        $code_lieu_de_naissance = substr($valF['code_lieu_de_naissance'],0,2)." ".substr($valF['code_lieu_de_naissance'],2,3);
                        $valF['code_lieu_de_naissance'] = $code_lieu_de_naissance;
                        $valF['code_departement_naissance'] = substr($valF['code_lieu_de_naissance'],0,2);
                        
                        $sql = "select libelle_commune, libelle_departement from commune c, departement d where c.code_departement = d.code and c.code = '".$code_lieu_de_naissance."'";
                        $result = $f -> db -> query($sql);
                        if (database::isError($result)) {
                            die ($result->getMessage());
                        }
                        while ($row = $result->fetchRow()) {
                            $valF['libelle_lieu_de_naissance'] = ($row[0]);
                            $valF['libelle_departement_naissance'] = ($row[1]);
                        }
                        // etranger
                        if ($valF['code_departement_naissance']=='99')
                            $valF['code_departement_naissance'] = trim(substr($valF['code_lieu_de_naissance'],0,5));
                        // dom
                        if ($valF['code_departement_naissance']=='97')
                            $valF['code_departement_naissance'] = trim(substr($valF['code_lieu_de_naissance'],0,3));
                        //
                        if ($valF['code_departement_naissance']=='98')
                            $valF['code_departement_naissance'] = trim(substr($valF['code_lieu_de_naissance'],0,3));
                    }
                    
                    // transfert
                    $valF['id']= $f -> db->nextId("inscription_office");
                    $valF = $this->convertToUTF8($valF);
                    $res= $f -> db->autoExecute("inscription_office",$valF,DB_AUTOQUERY_INSERT);
                    if (database::isError($res))
                        die($res->getDebugInfo()." => echec requete insertion "."inscription_office");
               
                    $i++;
                }
            }

            $f -> db -> commit ();
            ///////////////////////
            
            echo "Le transfert s'est effectué : ".($i)." inscriptions d'office ont été insérées.<br />";
            echo "Maintenant il faut valider les inscriptions une par une pour qu'elles soient basculées dans la table mouvement.<br />";
        }
    }

    function convertToUTF8($val) {
        $encodage = array("UTF-8", "ASCII", "Windows-1252", "ISO-8859-15", "ISO-8859-1");
        if(is_array($val)) {
            $res = array();
            foreach ($val as $key => $value) {
                $res[$key] = iconv(mb_detect_encoding($value, $encodage), HTTPCHARSET, $value);
            }
        } else {
            $res = iconv(mb_detect_encoding($val, $encodage), HTTPCHARSET, $val);
        }
        return $res;
    }
    
    /**
     * fonction xml2assoc()
     *
     * @param $xml
     */
    function xml2assoc($xml) {
        $tree = null;
        while($xml->read())
            switch ($xml->nodeType) {
                case XMLReader::END_ELEMENT: return $tree;
                case XMLReader::ELEMENT:
                    $node = array('tag' => $xml->name, 'value' => $xml->isEmptyElement ? '' : $this->xml2assoc($xml));
                    if($xml->hasAttributes)
                        while($xml->moveToNextAttribute())
                            $node['attributes'][$xml->name] = $xml->value;
                    $tree[] = $node;
                break;
                case XMLReader::TEXT:
                case XMLReader::CDATA:
                    $tree .= $xml->value;
            }
        return $tree;
    } 
}

?>

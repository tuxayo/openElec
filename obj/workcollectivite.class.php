<?php

class workCollectivite {
    
    /**
     *
     */
    var $f = NULL;
    
    /**
     *
     */
    var $action = ".";
    
    /**
     *
     */
    var $collectivites = array ();
    
    function __construct (&$f = NULL, $action = ".") {
        //
        $this->f = $f;
        $this->action = $action;
        //
        $this->calculateAllCollectivites ();
    }
    
    function validateForm () {
        //
        if (isset ($_POST['changecollectivite_action_cancel'])) {
            $this->f->goToDashboard ();
        }
        //
        if (isset ($_POST['collectivite']) or isset ($_POST['changecollectivite_action_valid'])) {
            //
            if (!isset ($this->collectivites[$_POST['collectivite']])) {
                // message
                $message_class = "error";
                $message = _("Cette collectivite n'existe pas.");
                $this->f->addToMessage($message_class, $message);
            } else {
                //
                $_SESSION['collectivite'] = $_POST['collectivite'];
                $_SESSION['libelle_collectivite'] = $this->collectivites[$_POST['collectivite']]['libelle'];
                $this->f->getCollectivite();
                $_SESSION['multi_collectivite'] = $this->f->isMulti();
                // message
                $message_class = "ok";
                $message = _("Vous travaillez maintenant sur la collectivite : ").$_SESSION ['collectivite']." - ".$_SESSION ['libelle_collectivite'].".";
                $this->f->addToMessage($message_class, $message);
            }
        }
    }

    function getCollectivites () {
        return $this->collectivites;
    }
    
    function countCollectivites () {
        return count($this->getCollectivites());
    }
    
    function calculateAllCollectivites () {
        //
        $sql = "select id, ville from collectivite order by ville";
        $res = $this->f->db->query ($sql);
        $this->f->isDatabaseError ($res);
        while ($row =& $res->fetchRow (DB_FETCHMODE_ASSOC)) {
            $this->collectivites[$row['id']] = array("code" => $row['id'],
                                                     "libelle" => $row['ville']);
        }
    }
    
    
    function showForm ($full = true) {
        //
        echo "\n<div id=\"changecollectiviteform\" class=\"formulaire\">\n";
        
        //
        echo "<form method=\"post\" id=\"changecollectivite_form\" action=\"".$this->action."\">\n";
        
        //
        $tabindex = 1;
        
        //
        echo "\t<div class=\"field\">\n";
        //
        if ($full == true) {
            echo "\t\t<label for=\"collectivite\">";
            echo _("Choisissez la collectivite sur laquelle vous souhaitez travailler.");
            echo "</label>\n";
        }
        //
        echo "\t\t<select name=\"collectivite\" tabindex=\"".$tabindex++."\" ";
        echo ($full == false?"onchange=\"submit();\"":"");
        echo " class=\"champFormulaire\" >\n\t\t\t";
        foreach ($this->collectivites as $value) {
            echo "<option value=\"".$value['code']."\"";
            if ($_SESSION['collectivite'] == $value['code']) {
                echo " selected=\"selected\"";
            }
            echo ">".$value['libelle']."</option>";
        }
        echo "\n\t\t</select>\n";
        //
        echo "\t</div>\n";
        
        //
        if ($full == true) {
            echo "\t<div class=\"formControls\">\n";
            echo "\t\t<input name=\"changecollectivite.action.valid\" tabindex=\"".$tabindex++."\" value=\""._("Valider")."\" type=\"submit\" class=\"boutonFormulaire context\" />\n";
            echo "<a class=\"retour\" title=\""._("Retour")."\" ";
            echo "href=\"../scr/dashboard.php\">";
            echo _("Retour");
            echo "</a>";
            echo "\t</div>\n";
        }
        
        //
        echo "</form>\n";
        
        //
        echo "</div>\n";
    }
}

?>
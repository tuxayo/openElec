<?php
/**
 * Ce fichier permet de faire une redirection vers la documentation de l'aaplication.
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

header("Location: http://docs.openmairie.org/?project=openelec&version=4.3&format=html&path=manuel_utilisateur");
exit();

?>

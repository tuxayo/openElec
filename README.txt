openElec
========

Avertissement
-------------

openElec est une application sensible, nécessitant un paramétrage précis.
Un mauvais paramétrage peut entrainer une révision incorrecte de
la liste électorale, des erreurs d'éditions ou de lien avec l'INSEE.
Ni l'équipe du projet openElec ni le chef de projet ne peuvent être tenus
pour responsables d'un éventuel dysfonctionnement comme ceci est précisé dans
la licence jointe. Vous pouvez, si vous le souhaitez, faire appel a un
prestataire spécialisé qui peut fournir support, hot-line, maintenance, et
garantir le fonctionnement en environnement de production.


Licence
-------

Voir le fichier LICENSE.txt.


Description
-----------

openElec est un logiciel de gestion des listes électorales. Il permet la
gestion complète des élections politiques, de l'inscription d'un électeur,
jusqu'à l'édition de sa carte électorale, l'édition des listes d'émargement,
le transfert à l'insee des nouvelles inscription et bien plus encore.


Installation
------------

Voir le fichier INSTALL.txt.


Migration depuis une version antérieure
---------------------------------------

Voir le fichier INSTALL.txt.


Documentation
-------------

Vous trouverez toute la documentation du projet sur le site
http://www.openelec.org/


Bugs
----

Merci de nous informer si vous rencontrez des bugs en utilisant le tracker prévu
à cet effet : https://adullact.net/tracker/?func=browse&group_id=434&atid=1200


Dépendances
-----------

Librairies integrées à l'application et versions nécessaires :

* PEAR 1.9.5
* DB 1.7.14
* fpdf 1.6
* openmairie 4.1.3
* phpmailer 5.1


Contributeurs
-------------

* Jean-Louis BASTIDE
* Thierry BENITA
* Romain BEYLERIAN
* Ronald DELOBEL
* François DESMARETZ
* Danielle DRILLON
* Jean-Yves LAUGIER
* Florent MICHON
* Adrien PENNEC
* Pascal PERILLER
* Joël POIRIER
* François RAYNAUD
* Alexandre REYNAUD
* Jean-François SABUCO
* Serge VIDALI


Crédits
-------

Les crédits sont les mairies et les entreprises qui ont investit dans le
projet :

* Mairie d'Arles
* Conseil général des Bouches-du-Rhône - CG 13
* Conseil général de la Moselle - CG 57
* Ville de Limoges
* atReal


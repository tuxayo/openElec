<?php
/**
 *
 */

/**
 *
 */
require_once("../obj/utils.class.php");

// variable $obj
$obj = "";
if (isset ($_GET['obj'])) {
    $obj = $_GET['obj'];
}

// new utils
$f = new utils ("nohtml", NULL, $obj);

// check $obj
if (strpos ($obj, "/") !== False
    or !file_exists ("../sql/".$f->phptype."/".$obj.".lettretype.inc")) {
    // message
    $message_class = "error";
    $message = _("Cette page n'existe pas.");
    $f->addToMessage($message_class, $message);
    // die
    $f->setFlag(NULL);
    $f->display();
    die ();
}

//
define ('FPDF_FONTPATH', 'font/');
require("fpdf.php");
set_time_limit (180);

//	
class PDF extends FPDF {
	//Pied de page
	function Footer()
	{
		// NUMERO DE PAGE
		//Positionnement à 1,5 cm du bas
		$this->SetY(-15);
		// Police Arial italique 8
		$this->SetFont('Arial','I',8);
		// Numero de page
		$this->Cell(0,10,iconv(HTTPCHARSET,"CP1252",_('Page ').$this->PageNo().'/{nb}'),0,0,'C');
	}
}

//
$idx = "";
if (isset ($_GET['idx'])) {
    $idx = $_GET['idx'];
}

//
$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];

$ville = $f -> collectivite ['ville'];
$dateTableau = substr($f -> collectivite ['datetableau'],8,2).'/'.substr($f -> collectivite ['datetableau'],5,2).'/'.substr($f -> collectivite ['datetableau'],0,4);
$anneeTableau = substr($f -> collectivite ['datetableau'],0,4);
$nom = $f -> collectivite ['maire'];

$sql_courrier="select * from courrier where courrier=".$idx;
$res = $fdb -> query ($sql_courrier);
$f->isDatabaseError($res);
while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
	$obj=$row['lettretype'];
	$destinataire=$row['destinataire'];  // ***
	$datecourrier=$row['datecourrier'];
	$complement=$row['complement'];
}
$res->free();

// format date
if ($f -> formatdate == "AAAA-MM-JJ")
{
	$valTemp=explode("-",$datecourrier);
	$datecourrier= $valTemp[2]."/".$valTemp[1]."/".$valTemp[0];
}

include ("../sql/".$f -> phptype."/".$obj.".lettretype.inc");

$unite="mm";
$pdf=new PDF($lettretype["orientation"],$unite,$lettretype["format"]);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Image("../trs/".$lettretype["logo"],$lettretype["logoleft"],$lettretype["logotop"],0,0,'','http://www.openmairie.org');

// Modification de l'encodage des caractères des états
// vers l'encodage défini dans dyn/locales.inc
$encodage = array("UTF-8", "ASCII", "Windows-1252", "ISO-8859-15", "ISO-8859-1");
$sql= iconv(mb_detect_encoding($lettretype["sql"], $encodage), HTTPCHARSET, $lettretype["sql"]);
$titre= iconv(mb_detect_encoding($lettretype["titre"], $encodage), HTTPCHARSET, $lettretype["titre"]);
$corps= iconv(mb_detect_encoding($lettretype["corps"], $encodage), HTTPCHARSET, $lettretype["corps"]);

include("../dyn/varlettretypepdf.inc");

$res = $f -> db -> query ($sql);
$f->isDatabaseError($res);
while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
	// titre
	$temp = explode("[",$lettretype["titre"]);
	for($i=1;$i<sizeof($temp);$i++)
	{
		$temp1 = explode("]",$temp[$i]);
		$titre=str_replace("[".$temp1[0]."]",$row[$temp1[0]],$titre);
		$temp1[0]="";
	}
	$pdf->SetFont($lettretype["titrefont"],$lettretype["titreattribut"],$lettretype["titretaille"]);
	$pdf->SetXY($lettretype["titreleft"],$lettretype["titretop"]);
	$pdf->MultiCell($lettretype["titrelargeur"],$lettretype["titrehauteur"],iconv(HTTPCHARSET,"CP1252",$titre),$lettretype["titrebordure"],$lettretype["titrealign"],0);
	// corps
	$temp = explode("[",$lettretype["corps"]);
	for($i=1;$i<sizeof($temp);$i++)
	{
		$temp1 = explode("]",$temp[$i]);
		$corps=str_replace("[".$temp1[0]."]",$row[$temp1[0]],$corps);
		$temp1[0]="";
	}
	$pdf->SetFont($lettretype["corpsfont"],$lettretype["corpsattribut"],$lettretype["corpstaille"]);
	$pdf->SetXY($lettretype["corpsleft"],$lettretype["corpstop"]);
	$pdf->MultiCell($lettretype["corpslargeur"],$lettretype["corpshauteur"] ,iconv(HTTPCHARSET,"CP1252",$corps),$lettretype["corpsbordure"],$lettretype["corpsalign"],0);
}
// fermeture pdf
$aujourdhui = date('Ymd-His');
$pdf->Output("lettretype-".$aujourdhui.".pdf","I");

?> 
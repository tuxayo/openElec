<?php
/**
 * Ce fichier permet de realiser l'edition du total des procurations
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"edition");

//
set_time_limit(480);

/**
 *
 */
require_once("../app/fpdf_table.php");

//
include("../sql/".$f->phptype."/pdf_listetotalprocuration.inc");
$res_select_bureau = $f->db->query($query_select_bureau);
$f->isDatabaseError($res_select_bureau);
$bureaux = array();
while ($row_select_bureau =& $res_select_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
    array_push($bureaux, $row_select_bureau);
}

//
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(false);
$pdf->SetFont('Courier','',10);
$pdf->SetDrawColor(4,4,4);
$pdf->SetMargins(25,10,5);
$pdf->SetDisplayMode('real','single');
// Paramètres
$lignebureau = 18;
$heightligne = 9;
$total_ligne = 0;
$cpt = 0;
$total = 0;
//
foreach ($bureaux as $bureau)
{
    //
    if ($cpt >= $lignebureau || $cpt == 0)
    {
	//
	if ($cpt>0)
	    $pdf->Cell(245,$heightligne,'','T',1,'R',0);
	//
	$cpt=0;
	$pdf->AddPage();
	//
	$pdf->Cell(205,10,iconv(HTTPCHARSET,"CP1252",_('Liste ').$_SESSION ['liste']." - ".$_SESSION ['libelle_liste']),'0',0,'L',0);
	$pdf->Cell(40,10,iconv(HTTPCHARSET,"CP1252",_('Page  :  ').$pdf->PageNo()." "),'0',1,'R',0);
	//
	$pdf->Cell(245,15,iconv(HTTPCHARSET,"CP1252",'T O T A L I S A T I O N      D E S      P R O C U R A T I O N S '),'LTRB',1,'C',0);
	$pdf->Cell(245,1,'','LTR',1,'C',0);
    }
    // Récupération du nombre de procurations pour le bureau
    $nb_ligne = 0;
    include ("../sql/".$f -> phptype."/pdf_listetotalprocuration.inc");
    $nb_ligne = $f->db->getOne($query_count_mandant_bureau);
    // On ajoute ce nombre au total
    $total += (int)$nb_ligne;
    // Alternance de couleur pour les lignes
    if ($cpt%2 == 0)
	$pdf->SetFillColor(241,241,241);
    else
	$pdf->SetFillColor(255,255,255);
    // Affichage de la ligne
    $pdf->Cell(215,$heightligne,iconv(HTTPCHARSET,"CP1252",_('Nombre total des procurations pour le bureau No ').$bureau ['code']." - ".$bureau ['libelle_bureau']),'L',0,'L',1);
    $pdf->Cell(30,$heightligne,iconv(HTTPCHARSET,"CP1252",$nb_ligne).' ','R',1,'R',1);
    // Compteur de lignes
    $cpt++;
}
//
if ($cpt < ($lignebureau-1))
{
    //
    $pdf->Cell(215,$heightligne,iconv(HTTPCHARSET,"CP1252",_('  T  O  T  A  L       G  E  N  E  R  A  L ')),'TLB',0,'L',0);
    $pdf->Cell(30,$heightligne,iconv(HTTPCHARSET,"CP1252",$total).'  ','TBR',1,'R',0);
}
else
{
    //
    $pdf->Cell(245,$heightligne,'','T',1,'R',0);
    $pdf->AddPage();
    //
    $pdf->Cell(205,10,iconv(HTTPCHARSET,"CP1252",_('Liste ').$_SESSION ['liste']." - ".$_SESSION ['libelle_liste']),'0',0,'L',0);
    $pdf->Cell(40,10,iconv(HTTPCHARSET,"CP1252",_('Page  :  ').$pdf->PageNo()." "),'0',1,'R',0);
    //
    $pdf->Cell(245,15,iconv(HTTPCHARSET,"CP1252",_('T O T A L I S A T I O N    D E S   P R O C U R A T I O N S ')),'LTRB',1,'C',0);
    $pdf->Cell(245,1,'','LTR',1,'C',0);
    $pdf->Cell(215,$heightligne,iconv(HTTPCHARSET,"CP1252",_('  T  O  T  A  L       G  E  N  E  R  A  L ')),'LB',0,'L',0);
    $pdf->Cell(30,$heightligne,iconv(HTTPCHARSET,"CP1252",$total).'  ','BR',1,'R',0);
}
$aujourdhui = date('Ymd-His');
$pdf->Output("listetotalprocuration-".$aujourdhui.".pdf","I");
$pdf->Close();

?> 
<?php
/**
 * Ce fichier permet de realiser l'edition pour la facturation des communes,
 * cette edition est utile seulement pour le mode MULTI
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"facturation");

//
define('FPDF_FONTPATH','font/');
require_once("fpdf.php");
set_time_limit (480);
$aujourdhui = date("d/m/Y");



$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];


//===========================================================================
include ("../sql/".$f -> phptype."/pdf_facturation_titres_recettes.inc");
//===========================================================================


// Communes

$commune = array ();

$sqlcom = $sqlcom1;

$rescom =& $f -> db -> query ($sqlcom);
$f->isDatabaseError($rescom);

while ($rowcom =& $rescom -> fetchRow (DB_FETCHMODE_ASSOC))
	array_push ($commune, $rowcom);


// Fonction Entete & Tableau
function pdf_stat_entete()
    {
        global $pdf, $f;
        $pdf->addpage();
        // entete
        $pdf->SetFont('courier','',11);
        $pdf->MultiCell(80,5,iconv(HTTPCHARSET,"CP1252",$f->collectivite['ville']),0,'C',0);
	$pdf->ln();
	$pdf->Cell(100,5,iconv(HTTPCHARSET,"CP1252",_('SERVICE GESTIONNAIRE :').$f->getParameter("fact_gestionnaire")),0,0,'L',0);	
        $pdf->SetFont('courier','B',11);
	$pdf->Cell(100,5,iconv(HTTPCHARSET,"CP1252",_('DEMANDE DE TITRE DE RECETTE')),0,1,'C',0);
        $pdf->SetFont('courier','',11);
	$pdf->Cell(100,5,iconv(HTTPCHARSET,"CP1252",_('SERVICE ATTRIBUTAIRE :').$f->getParameter("fact_attributaire")),0,1,'L',0);
	$pdf->Cell(100,5,iconv(HTTPCHARSET,"CP1252",_('CHAPITRE :').$f->getParameter("fact_chapitre")),0,1,'L',0);
	$pdf->SetXY(260,5);
	$pdf->Cell(25,7,iconv(HTTPCHARSET,"CP1252",_(' Page  :  ').$pdf->PageNo()."/{nb} "),'0',1,'R',0);
	$pdf->SetY(40);
        $pdf->MultiCell(20,5,iconv(HTTPCHARSET,"CP1252",_('NUMERO TITRE')),1,'R',0);
	$pdf->SetXY(25,40);
	$pdf->Cell(85,10,iconv(HTTPCHARSET,"CP1252",_('COMMUNE')),1,0,'C',0);
	$pdf->Cell(120,10,iconv(HTTPCHARSET,"CP1252",('OBJET ET DECOMPTE')),1,0,'C',0);
	$pdf->Cell(60,10,iconv(HTTPCHARSET,"CP1252",_('SOMME DUE')),1,1,'C',0);
    }
    
function pdf_stat_pied($tot = 0)
    {
	global $pdf;
	global $tauxPage;
	global $tauxGlobal;
	$pdf->Cell(285,.5,'',1,1,'L',0);	
	$pdf->Cell(225,7,iconv(HTTPCHARSET,"CP1252",_('TOTAL DE LA PAGE  ')),0,0,'R',0);	
	$pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",number_format($tauxPage,2).' '),0,1,'R',0);	
	$tauxGlobal+=$tauxPage;
	$tauxPage = 0;
	if($tot)
	{
	    $pdf->Cell(225,7,iconv(HTTPCHARSET,"CP1252",_('TOTAL GENERAL  ')),0,0,'R',0);	
	    $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",number_format($tauxGlobal,2).' '),0,1,'R',0);
	}
	$pdf->SetXY(180,186);
	$pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",_('LE CHEF DE SERVICE :')),0,1,'L',0);
    }

$pdf=new FPDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(true);
$pdf->SetFont('courier','',11);
$pdf->SetDrawColor(30,7,146);
$pdf->SetMargins(5,5,5);
$pdf->SetDisplayMode('real','single');

pdf_stat_entete();

//  compteur de limite d'affichage
$compteur = 0;
//////////////////////////

$tauxPage = 0;
$tauxGlobal = 0;

foreach ($commune as $c)
{
    ////////////////////////////////////////////////////////////////////////////////////////////
    $compteur++;
    if($compteur>=18) { pdf_stat_pied(); pdf_stat_entete(); $compteur=1;  }
	$pdf->Cell(20,7,'',0,0,'R',0);
	$pdf->Cell(85,7,iconv(HTTPCHARSET,"CP1252",'  '.$c['ville']),0,0,'L',0);
	$pdf->Cell(120,7,iconv(HTTPCHARSET,"CP1252",_('TENUE INFORMATIQUE DU FICHIER ELECTORAL')),0,0,'L',0);
	//// calcul de la "somme du"
	$sommeDue = $f->getParameter("fact_electeur_unitaire") * $c['total'];
	if( $sommeDue < $f->getParameter("fact_forfait") ) $sommeDue = $f->getParameter("fact_forfait");
	$pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",number_format($sommeDue,2).' '),0,1,'R',0);
	$tauxPage+=$sommeDue;  
}

pdf_stat_pied(1);

/////////////////////////////////////////////////////////////////////////////////////

$aujourdhui = date('Y')-1;
$pdf->Output("Titres_de_Recettes-".$aujourdhui.".pdf","I");
$pdf->Close();



?> 
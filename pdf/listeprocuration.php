<?php
/**
 * Ce fichier permet de realiser l'edition de toutes les procurations
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"edition");
//
require('../app/fpdf_table.php');
set_time_limit (480);
$id = "";
$nobureau = "";

//
$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];
//
$id='';
if (isset($_GET['id']))
{
    $id= $_GET['id'];
    if($id=="bureau")
        if (isset($_GET['idx']))
        {
            $nobureau=$_GET['idx'];
            // message aucun enregistrement sélectionné---------------------------
            $message=sprintf(_("LISTE DES PROCURATIONS\nListe %s\nAucun enregistrement selectionne pour le bureau %s"),$nolibliste, $_GET['idx']);
            //--------------------------------------------------------------------
        }
}

//**************************************************************************//
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(false);
$pdf->SetFont('Arial','',8);
$pdf->SetDrawColor(30,7,146);
$pdf->SetMargins(5,10,5);
$pdf->SetDisplayMode('real','single');
//--------------------------------------------------------------------------//
$param=array();
$param=array(9,9,9,1,1,4,4,10,10,0,0);
//--------------------------------------------- nombre enregistrement       //
$ligne=0;
$total_ligne=0;
//--------------------------------------------------------------------------//
//
$pdf->AddPage();
//
//--------------------------------------------------------------------------//
//    SQL                                                                   //
//--------------------------------------------------------------------------//
include ("../sql/".$f -> phptype."/pdf_listeprocuration.inc");
//recup libelle bureau et libelle canton====================================
$libelle_bureau='';
$libelle_canton='';
$resbur =& $f -> db->query($sqlbur);
if (database::isError($resbur)) {
     die($resbur->getMessage());
}
else
{
  $rowbur=& $resbur->fetchRow(DB_FETCHMODE_ASSOC);
  $libelle_bureau=$rowbur['libbur'];
  $libelle_canton=$rowbur['libcanton'];
}
//------------------------titre------------------------------------------//
$aujourdhui = date("d/m/Y");
$heure = date("H:i");
$heighttitre=6;
$titre=array();
$titre=array(
array('<PAGE>',270,$heighttitre,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Gestion des Elections   -   Mairie de : %s'),$f->collectivite['ville']),120,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('LISTE DES PROCURATIONS PAR BUREAU')),90,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Liste %s'),$nolibliste),165,$heighttitre,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Edite le %s a %s'),$aujourdhui,$heure),110,$heighttitre,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Bureau No %s %s'),$nobureau,$libelle_bureau),120,$heighttitre,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Canton %s'),$libelle_canton),50,$heighttitre,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Circonscription'),100,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0)
);
//------------------------entete colonne   ------------------------------------------------------------------------//
$heightentete=8;
$entete=array();
$entete=array(
array(_('MANDANT'),110,$heightentete,2,'LRT','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Nom Patronymique,Prenoms,Nom d\'Usage'),90,$heightentete-4,2,'LRT','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Date et lieu de naissance,Adresse'),90,$heightentete-4,0,'LRB','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('No Elec - Ord'),20,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,4),0,'','NB',0,1,0),
array(_('MANDATAIRE'),110,$heightentete,2,'1','C','0','0','0','255','255','255',array(0,8),0,'','NB',0,1,0),
array(_('Nom Patronymique,Prenoms,Nom d\'Usage'),90,$heightentete-4,2,'LRT','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Date et lieu de naissance,Adresse'),90,$heightentete-4,0,'LRB','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array('<VCELL>',20,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,-4),3,array('No Elec','Bur - Ord'),'NB',0,1,0),
array('<VCELL>',22,$heightentete+8,0,'1','C','0','0','0','255','255','255',array(0,-16),3,array('Validité'),'NB',0,1,0),
array(_('Emargement'),44,$heightentete+8,0,'1','C','0','0','0','255','255','255',array(0,16),0,'','NB',0,1,0)
);
//------------------------page fin ----------------------------------------------------------------------------------
$nbr_enregistrement=0;
if (isset ($f -> collectivite ['dateelection']) && $f -> collectivite ['dateelection'] != '') {
    $message_fin = sprintf(_("Nombre total de procurations election du %s pour le bureau  No %s    -   %s   :"),
        substr($f -> collectivite ['dateelection'],8,2)."/".substr($f -> collectivite ['dateelection'],5,2)."/".substr($f -> collectivite ['dateelection'],0,4),
        $nobureau,
        $libelle_bureau);
} else {
    $message_fin = sprintf(_("Nombre total de procurations pour le bureau  No %s    -   %s   :"),
        $nobureau,
        $libelle_bureau);
}
$pagefin=array();
$pagefin=array(
array('<PAGE>',280,$heighttitre,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Liste  %s'),$nolibliste),285,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('TOTALISATION    DES   PROCURATIONS'),285,$heighttitre+10,1,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',10,1,0),
array('   ', $heighttitre,10,1,'T','C','0','0','0','255','255','255',array(0,0),0,'','NB',9,1,0),
array($message_fin,195,$heighttitre,0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',10,1,0),
array('<LIGNE>',30,$heighttitre,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','B',9,1,0)
);
//--------------------------------------------------------------------------//
$heightligne=5;
$col=array();
$col=array('nom'=>array(90,$heightligne,2,'LRT','L','0','0','0','255','255','255',array(0,0),' ','',
'CELLSUP_NON',0,'NA','NC','','B',8,array('0')),
'prenom'=>array(90,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'    ','',
'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'nom_usage'=>array(90,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'       -    ','',
'CELLSUP_NON',0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
'naissance'=>array(90,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),_('     Ne(e) le '),'',
'CELLSUP_NON',0,'NA','NC','','NB',0,array('0')),
'lieu'=>array(90,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),_('     a  '),'',
'CELLSUP_NON',0,'NA','NC','','NB',0,array('0')),
'adresse'=>array(90,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'     ','',
'CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
'complement'=>array(90,$heightligne,0,'LBR','L','0','0','0','255','255','255',array(0,0),'     ','',
'CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
'numero_electeur'=>array(20,$heightligne+7,2,'LRT','C','0','0','0','255','255','255',array(0,30),'','',
'CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
'numero_bureau'=>array(20,$heightligne+18,0,'LRB','C','0','0','0','255','255','255',array(0,0),'','',
'CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
'nomm'=>array(90,$heightligne,2,'LRT','L','0','0','0','255','255','255',array(0,12),' ','',
'CELLSUP_NON',0,'NA','NC','','B',8,array('0')),
'prenomm'=>array(90,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'    ','',
'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'nom_usagem'=>array(90,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'       -    ','',
'CELLSUP_NON',0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
'naissancem'=>array(90,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),_('     Ne(e) le '),'',
'CELLSUP_NON',0,'NA','NC','','NB',0,array('0')),
'lieum'=>array(90,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),_('     a  '),'',
'CELLSUP_NON',0,'NA','NC','','NB',0,array('0')),
'adressem'=>array(90,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'     ','',
'CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
'complementm'=>array(90,$heightligne,0,'LBR','L','0','0','0','255','255','255',array(0,0),'     ','',
'CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
'numelec'=>array(20,$heightligne+7,2,'LTR','C','0','0','0','255','255','255',array(0,30),'','',
'CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
'codeburm'=>array(20,$heightligne,2,'LR','C','0','0','0','255','255','255',array(0,0),'','','CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),

'numburm'=>array(20,$heightligne+13,0,'LBR','C','0','0','0','255','255','255',array(0,0),'','','CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),

'debutvalidm'=>array(22,$heightligne+13,2,'LTR','C','0','0','0','255','255','255',array(0,17),'du ','','CELLSUP_NON',array(44,$heightligne+30,0,'1','C','0','0','0','255','255','255',array(0,18)),0,'NA','NC','','NB',0,array('0')),

'finvalidm'=>array(22,$heightligne+12,0,'LBR','C','0','0','0','255','255','255',array(0,0),'au ','','CELLSUP_VIDE',array(44,$heightligne+30,0,'1','C','0','0','0','255','255','255',array(0,18)),0,'NA','NC','','NB',0,array('0')),

);
//
//echo $sql;
$enpied=array();
//echo $sql;
$pdf->Table($titre,$entete,$col,$enpied,$sql,$f->db,$param,$pagefin);
// message  aucun enregistrement--------------------------------------------
if ($pdf->msg==1 and $message!=''){
   $pdf->SetFont('Arial','',10);
   $pdf->SetDrawColor(0,0,0);
   $pdf->SetFillColor(213,8,26);
   $pdf->SetTextColor(255,255,255);
   $pdf->MultiCell(120,5,iconv(HTTPCHARSET,"CP1252",$message),'1','C',1);
}
//--------------------------------------------------------------------------
$aujourdhui = date('Ymd-His');
$pdf->Output("listeprocuration-".$aujourdhui.".pdf","I");
$pdf->Close();

?> 
<?php
/**
 * Ce fichier permet de realiser l'edition des convocations de membres
 * aux commissions
 *
 * @package openelec
 * @version SVN : $Id$
 */

if(!isset($_POST["message"]) OR empty($_POST["message"])) {
	header("Location: ../trt/module_commission.php");
}
require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"traitement_commission");

// Création du tableau des identifiants des membres convoqué à la commission
$idx = array();
$sql_membre = "SELECT membre_commission FROM membre_commission WHERE collectivite = '".$_SESSION["collectivite"]."'";
$res_membre = $f->db->query($sql_membre);
$f->isDatabaseError($res_membre);
while ($row_membre=& $res_membre->fetchRow(DB_FETCHMODE_ASSOC)) {
	$idx[] = $row_membre["membre_commission"];
}

// Preparation des paramètres à passer au script pdfetat.php
$_GET["idx"] = implode(';',$idx);
$_GET["obj"] = "convocationcommission";
$_GET["id"] = "convocationcommission";
$_GET["complement"] = $_POST["message"];
$_GET["footerdisplay"] = false;

include "../pdf/pdfetat.php";

?>
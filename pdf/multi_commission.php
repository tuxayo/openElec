<?php
/**
 * Ce fichier permet de realiser une edition recapitulant tous les mouvements
 * d'inscriptions et de radiation et de changement de bureau pour une commune
 * ou pour le mode multi.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) { $f = new utils("nohtml", /*DROIT*/"edition-commission"); }

/**
 *
 */
// 
$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];
// Gestion des dates
$aujourdhui = date ("d/m/Y");

/**
 * Ouverture du fichier pdf
 */
//
require_once "../app/fpdf_table.php";
set_time_limit(480);
//
$pdf = new PDF('L', 'mm', 'A4');
//
$pdf->Open();


//
$pdf->SetFont('Arial', '');
//
$pdf->SetDrawColor(30, 7, 146);
//
$pdf->SetMargins(5, 5, 5);
//
$pdf->SetDisplayMode('real', 'single');
//
$pdf->multi = true;


// Inclusion du fichier de requete
require "../sql/".$f->phptype."/pdf_multi_commission.inc";

// Liste des bureaux de vote
$res = $f->db->query($sql_bureau_a);

// $f->isDatabaseError($res);
if (DB::isError($res))
    die($res->getMessage());

$k = 0;
$bureau = array();
while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
    $bureau[$k] = $row;
    $k++;
}

// Page de garde commune
$pdf->AddPage();
$pdf->SetFont('Arial','',28);
$pdf->Cell(280,160,iconv(HTTPCHARSET,"CP1252",$f->collectivite['ville']),0,0,"C",0);
$pdf->SetFont('Arial','',12);
$pdf->Text(250,180,iconv(HTTPCHARSET,"CP1252",_("Page ").$pdf->PageNo()));

//
foreach ($bureau as $b) {
    
    // Inclusion du fichier de requete
    require "../sql/".$f->phptype."/pdf_multi_commission.inc";
    
    //
    $pdf->AddPage();
    $pdf->AliasNbPages();
    $pdf->SetAutoPageBreak(false);
    
    // Requête : nombre d'inscriptions
    $res = $f -> db -> query ($nbins);
    if (DB :: isError ($res))
        die($res->getMessage()." erreur ".$nbins);
    $row =& $res -> fetchRow (DB_FETCHMODE_ASSOC);
    $nb_add = $row ['nbr'];

    // Requête : nombre de radiations
    $res = $f -> db -> query ($nbrad);
    if (DB :: isError ($res))
        die($res->getMessage()." erreur ".$nbrad);
    $row =& $res -> fetchRow (DB_FETCHMODE_ASSOC);
    $nb_rad = $row ['nbr'];
    
    // Type de catégorie de mouvements
    $typecat = array (0 => "Inscription", 1 => "Radiation");

    // Inscriptions
    $res = $f -> db -> query ($nbins_actif);
    if (DB :: isError ($res))
        die ($res -> getMessage () ." erreur ". $nbins_actif);
    $row =& $res -> fetchRow();
    $nb_add_actif = $row [0];
    
    // Radiations
    $res = $f -> db -> query ($nbrad_actif);
    if (DB :: isError ($res))
        die ($res -> getMessage () ." erreur ". $nbrad_actif);
    $row =& $res -> fetchRow();
    $nb_rad_actif = $row [0];
    
    // Electeurs
    $res = $f -> db -> query ($nbelec);
    if (DB :: isError ($res))
        die ($res -> getMessage () ." erreur ". $nbelec);
    $row =& $res -> fetchRow();
    $nb_elec = $row [0];

    // Recuperation du nombre d'additions traitees aux dates de tableau superieures
    // ou egales a la date de tableau actuelle
    $res_nb_addition_dtsup = $f->db->getOne($nb_addition_dtsup);
    if (DB::isError($res_nb_addition_dtsup))
        die ($res_nb_addition_dtsup->getMessage() ." erreur ". $nb_addition_dtsup);
    // Recuperation du nombre de radiations traitees aux dates de tableau
    // superieures ou egales a la date de tableau actuelle 
    $res_nb_radiation_dtsup = $f->db->getOne($nb_radiation_dtsup);
    if (DB::isError($res_nb_radiation_dtsup))
        die ($res_nb_radiation_dtsup->getMessage() ." erreur ". $nb_radiation_dtsup);
    
    /**
     * Calcul du nombre d'electeur a la date de tableau precedente et a la date de
     * tableau suivante
     */
    // Au tableau precedent le nombre d'electeurs est egal au nombre d'electeurs
    // dans la table electeur a l'heure actuelle auquel on enleve toutes les
    // additions traitees aux dates de tableau superieures ou egales a la date
    // de tableau actuelle et auquel on ajoute toutes les radiations traitees
    // aux dates de tableau superieures ou egales a la date de tableau actuelle 
    $nb = $nb_elec - $res_nb_addition_dtsup + $res_nb_radiation_dtsup;
    // Au tableau suivant le nombre d'electeurs est egal au nombre d'electeurs
    // au tableau precedent calcule juste au dessus auquel on ajoute toutes
    // les additions de la date de tableau en cours et auquel on enleve toutes
    // les radiations de la date de tableau en cours
    $nb_newelec = $nb + $nb_add - $nb_rad;
    
    foreach ($typecat as $key => $t) {
        
        $var = "sql_".$t;
        //
        $param = array ();
        $param = array (9, 9, 10, 1, 1, 7, 7, 5, 10, 1, 1);
        //------------------------page de garde par bureau ----------------------------------------------------------------------------------
        $pagedebut = array ();
        $pagedebut = array (
        array (_('BUREAU No ').$b ['code'], 260, 10, 2, '0', 'R', '0', '0', '0', '255', '255', '255', array (0, 0), 0, '', 'NB', 18, 1, 0),
        array (_('LISTE ').$_SESSION ['liste']." - ".$_SESSION ['libelle_liste']." - ".$f -> collectivite ['ville'],260,20,2,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array (_('Nombre d\'electeurs inscrits au dernier tableau : ').$nb,260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array (_('Inscriptions : ').$nb_add,260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array (_('Radiations : ').$nb_rad,260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array (sprintf(_('Arrete le tableau du bureau no %s au nombre de %s electeurs.'),$b['code'],$nb_newelec),260,15,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array ('',260,25,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array ($f -> collectivite ['ville']._(' le ').substr($f -> collectivite ['datetableau'],8,2).'/'.substr($f -> collectivite ['datetableau'],5,2).'/'.substr($f -> collectivite ['datetableau'],0,4).'.',260,15,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array (_('Le Maire                         Le Delegue du Prefet                    Le Delegue du TGI'),260,15,1,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array (_('Page ').$pdf->PageNo(), 260, 15, 1, '0', 'R', '0', '0', '0', '255', '255', '255', array (0, 0), 0, '', 'NB', 12, 1, 0)
        );
        //------------------------entete page ----------------------------------------------------------------------------------
        $heighttitre=4;
        $titre=array();
        $titre=array(
        array(_('BUREAU No ').$b ['code'].' '.$b ['libelle_bureau'],285,$heighttitre+1,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(_('ETAT POUR LA COMMISSION, MOUVEMENTS TABLEAU DU ').substr($f -> collectivite ['datetableau'],8,2).'/'.substr($f -> collectivite ['datetableau'],5,2).'/'.substr($f -> collectivite ['datetableau'],0,4),200,$heighttitre+5,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(' ',10,$heighttitre+5,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(_('Canton  ').$b ['libelle_canton'],280,$heighttitre,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array('',100,4,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(_('Famille de Changements : '). $t,270,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        );
        //------------------------entete colonne   ------------------------------------------------------------------------//
        $heightentete=8;
        $entete=array();
        $entete=array(
        array(_('Nom Patronymique,Prenoms'),90,$heightentete-4,2,'LRT','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(_('Nom d\'Usage,Date et lieu de naissance'),90,$heightentete-4,0,'LRB','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(_('Adresse'),90,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,4),0,'','NB',0,1,0),
        array('<VCELL>',15,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),3,array('No','Ordre'),'NB',0,1,0),
        array(_('Motif du mouvement'),90,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,8),0,'','NB',0,1,0)
        );
        //------------------------page fin ----------------------------------------------------------------------------------
        $nbr_enregistrement=0;
        $pagefin=array();
        $pagefin=array(
        array(_('Arrete le tableau du '),45,$heighttitre+5,0,'T','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(substr($f -> collectivite ['datetableau'],8,2).'/'.substr($f -> collectivite ['datetableau'],5,2).'/'.substr($f -> collectivite ['datetableau'],0,4),25,$heighttitre+5,0,'T','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(_(' a '),10,$heighttitre+5,0,'T','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array('<LIGNE>',18,$heighttitre+5,0,'T','C','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
        array(strtoupper($t).'(S)',45,$heighttitre+5,0,'T','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        //array('<PAGE>',140,$heighttitre+5,0,'T','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        );
        //------------------------enregistrements ----------------------------------------------------------------------------------
        $heightligne=20;
        $col=array();
        $col=array(
        'nom'=>array(90,$heightligne-16,2,'LRT','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','B',0,array('0')),
        'prenom'=>array(90,$heightligne-16,2,'LR','L','0','0','0','255','255','255',array(0,0),'     ','','CELLSUP_NON',0,'NA','NC','','NB',0,array('0')),
        'nom_usage'=>array(90,$heightligne-16,2,'LR','L','0','0','0','255','255','255',array(0,0),'       -    ','','CELLSUP_NON',0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
        'naissance'=>array(90,$heightligne-16,2,'LR','L','0','0','0','255','255','255',array(0,0),_('     Ne(e) le '),'','CELLSUP_NON',0,'NA','NC','','NB',0,array('0')),
        'lieu'=>array(90,$heightligne-16,0,'LRB','L','0','0','0','255','255','255',array(0,0),_('     a  '),'','CELLSUP_NON',0,'NA','NC','','NB',0,array('0')),
        'adresse'=>array(90,$heightligne-16,2,'LRT','L','0','0','0','255','255','255',array(0,16),'','','CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
        'complement'=>array(90,$heightligne-16,2,'LR','L','0','0','0','255','255','255',array(0,0),'','','CELLSUP_VIDE',array(90,$heightligne-8,0,'LRB','C','0','0','0','255','255','255',array(0,0)),'NA','NC','','NB',0,array('0')),
        'numero_bureau'=>array(15,$heightligne,0,'1','C','0','0','0','255','255','255',array(0,8),'','','CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
        'libmvt'=>array(90,$heightligne-16,2,'LTR','L','0','0','0','255','255','255',array(0,0),'','','CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
        'ancien_bureau'=>array(90,$heightligne-16,2,'LR','L','0','0','0','255','255','255',array(0,0),'','','CELLSUP_VIDE',array(90,$heightligne-8,0,'LRB','C','0','0','0','255','255','255',array(0,0)),'NA','CONDITION',array('NN'),'NB',0,array('0'))
        );
        //
        $enpied = array();

        if ($key==0)
            $pdf->Table($titre,$entete,$col,$enpied,${$var},$f -> db,$param,$pagefin,$pagedebut);
        else 
            $pdf->Table($titre,$entete,$col,$enpied,${$var},$f -> db,$param,$pagefin);
    }
}

/**
 *
 */
// Nom du fichier
$filename = date("Ymd-His");
$filename .= "-commission";
$filename .= "-".$_SESSION['collectivite'];
$filename .= "-".$f->normalizeString($_SESSION['libelle_collectivite']);
$filename .= "-liste".$_SESSION['liste'];
$filename .= ".pdf";
// Definition du mode de sortie
$mode = (isset($_GET["mode"]) ? $_GET["mode"] : "inline");
// Sortie du pdf
if (isset($_GET['multi']) and $_GET['multi'] == true) {
    // S : renvoyer le document sous forme de chaine
    $filename = $f->getParameter("pdfdir").$filename;
    $contenu = $pdf->Output($filename, "S");
} elseif ($mode == "save") {
    //
    $filename_with_path = $f->getParameter("pdfdir").$filename;
    $pdf->Output($filename_with_path, "F");
} else {
    // I : envoyer en inline au navigateur
    header('Cache-Control: private, max-age=0, must-revalidate');
    $pdf->Output($filename, "I");
}
// Destruction de l'objet PDF
$pdf->Close();

?>

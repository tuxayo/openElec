<?php
/**
 * Ce fichier permet de realiser l'edition des statistiques sur les
 * inscriptions, cette edition est utile seulement pour le mode MULTI
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"edition");

//
define('FPDF_FONTPATH','font/');
require_once("fpdf.php");
set_time_limit (480);
$aujourdhui = date("d/m/Y");


$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];


//===========================================================================
include ("../sql/".$f -> phptype."/pdf_statistique_inscription.inc");
//===========================================================================



// Communes
$commune = array ();

$rescom =& $f -> db -> query ($sqlcom1);
$f->isDatabaseError($rescom);

while ($rowcom =& $rescom -> fetchRow (DB_FETCHMODE_ASSOC))
	array_push ($commune, $rowcom);


$pdf=new FPDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(true);
$pdf->SetFont('courier','',11);
$pdf->SetDrawColor(30,7,146);
$pdf->SetMargins(5,5,5);
$pdf->SetDisplayMode('real','single');


foreach ($commune as $c)
{

        $pdf->addpage();
        // entete
	$pdf->SetFont('courier','',11);
	$pdf->Cell(190,5, iconv(HTTPCHARSET,"CP1252",_("COMMUNE      : ").$c['ville']),0,0,'L',0);
	$pdf->Cell(95,5, iconv(HTTPCHARSET,"CP1252",_("EDITEE LE : ").$aujourdhui),0,1,'R',0);
	$pdf->Cell(190,5, iconv(HTTPCHARSET,"CP1252",_("DATE TABLEAU : ").$f->formatDate($f -> collectivite ['datetableau'])),0,1,'L',0);
	$pdf->Cell(0,10,"",0,1,'L',0);
        $pdf->SetFont('courier','B',11);
        $pdf->Cell(190,5,iconv(HTTPCHARSET,"CP1252",_("STATISTIQUES INSCRIPTIONS D'OFFICE ET INSCRIPTIONS PAR SEXE ")),0,1,'C',0);
        $pdf->SetFont('courier','',11);
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
	// Tableau
	$pdf->Cell(80,7,"",0,0,'L',0);
	$pdf->Cell(80,7,iconv(HTTPCHARSET,"CP1252",_("Inscription d'office (Code 8)")),1,0,'C',0);
	$pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",_("Autres inscriptions")),1,0,'C',0);
	$pdf->Cell(40,7,iconv(HTTPCHARSET,"CP1252",_("Total")),1,1,'C',0);
	// Calcul totaux
	$totaux_homme = $c['totalm'] + $c['totalm8'];
	$totaux_femme = $c['totalf'] + $c['totalf8'];
	$totaux_autres = $c['totalm'] + $c['totalf'];
	$totaux_8 = $c['totalm8'] + $c['totalf8'];
	$totaux = $totaux_8 + $totaux_autres;
	// Affichage
	$pdf->Cell(30,7,"",0,0,'L',0);	
	$pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",_("Hommes")),1,0,'C',0);
	$pdf->Cell(80,7,iconv(HTTPCHARSET,"CP1252",$c['totalm8']),1,0,'C',0);
	$pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",$c['totalm']),1,0,'C',0);
	$pdf->Cell(40,7,iconv(HTTPCHARSET,"CP1252",$totaux_homme),1,1,'C',0);
	$pdf->Cell(30,7,"",0,0,'L',0);	
	$pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",_("Femmes")),1,0,'C',0);
	$pdf->Cell(80,7,iconv(HTTPCHARSET,"CP1252",$c['totalf8']),1,0,'C',0);
	$pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",$c['totalf']),1,0,'C',0);
	$pdf->Cell(40,7,iconv(HTTPCHARSET,"CP1252",$totaux_femme),1,1,'C',0);
	$pdf->Cell(30,7,"",0,0,'L',0);	
	$pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",_("Total")),1,0,'C',0);
	$pdf->Cell(80,7,iconv(HTTPCHARSET,"CP1252",$totaux_8),1,0,'C',0);
	$pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",$totaux_autres),1,0,'C',0);
	$pdf->Cell(40,7,iconv(HTTPCHARSET,"CP1252",$totaux),1,1,'C',0); 
}


/////////////////////////////////////////////////////////////////////////////////////
$aujourdhui = date('Ymd-His');
$pdf->Output("stats_inscription-".$aujourdhui.".pdf","I");
$pdf->Close();

?> 
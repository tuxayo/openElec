<?php
/**
 * Ce fichier permet de realiser l'edition du total du registre des
 * procurations
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"edition");

//
require('../app/fpdf_table.php');
set_time_limit (480);

include ("../sql/".$f -> phptype."/pdf_listeregistretotproc.inc");
//
$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];
//
$id='';
$message="Aucun enregistrement sélectionné";
if (isset($_GET['id']))
{
    $id= $_GET['id'];
    if($id=="bureau")
    if (isset($_GET['idx']))
    {
	$bureau_debut=number_format($_GET['idx'],0);
	$bureau_fin=number_format($_GET['idx'],0);
	// message aucun enregistrement sélectionné---------------------------
	$message=sprintf(_("EDITION DU REGISTRE DES PROCURATIONS\nListe %s  %s\nAucun enregistrement selectionne pour le bureau %s"),$_SESSION['liste'],$_SESSION['libelle_liste'],$_GET['idx']);
	//--------------------------------------------------------------------
    }
}
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(false);
$pdf->SetFont('Arial','',8);
$pdf->SetDrawColor(30,7,146);
$pdf->SetMargins(5,10,5);
$pdf->SetDisplayMode('real','single');
//--------------------------------------------------------------------------//
$param=array();
$param=array(9,7,8,1,1,10,10,10,5,1,0);
//--------------------------------------------- nombre enregistrement       //
$ligne=0;
$total_ligne=0;
//--------------------------------------------------------------------------//
$pdf->AddPage();
//
$aujourdhui = date("d/m/Y");
$heighttitre=6;
$titre=array();
$titre=array(
array('<PAGE>',270,$heighttitre,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Mairie de :  ').$f -> collectivite ['ville'],120,$heighttitre,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('EDITION DU REGISTRE DES PROCURATIONS'),50,$heighttitre,1,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Liste ').$nolibliste,120,$heighttitre,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Le ').$aujourdhui,50,$heighttitre,1,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0)
);
//------------------------entete colonne   ------------------------------------------------------------------------//
$heightentete=10;
$entete=array();
$entete=array(
array(_('M A N D A N T'),45,$heightentete,0,'LRT','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('No Bur.'),10,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('M A N D A T A I R E'),45,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('No Bur.'),10,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('O R I G I N E   DE   LA   D E M A N D E'),50,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('REFUS'),10,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('MOTIF'),45,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('DATE ET HEURE'),24,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('DUREE'),24,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('OBSERVATIONS'),24,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0)
);
//------------------------page fin ----------------------------------------------------------------------------------
$nbr_enregistrement=0;
$pagefin=array();
//--------------------------------------------------------------------------//
$heightligne=5;
$col=array();
$col=array('nom'=>array(45,$heightligne,2,'LRT','L','0','0','0','255','255','255',array(0,0),' ','',
'CELLSUP_NON',0,'NA','NC','','B',8,array('0')),
'nom_usage'=>array(45,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'       -    ','',
'CELLSUP_NON',0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
'prenom'=>array(45,$heightligne,0,'LBR','L','0','0','0','255','255','255',array(0,0),'    ','',
'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'bureau'=>array(10,$heightligne,2,'LR','C','0','0','0','255','255','255',array(0,5),'','',
'CELLSUP_VIDE',array(10,$heightligne,0,'LRB','C','0','0','0','255','255','255',array(0,0)),0,'NA','NC','','NB',8,array('0')),
'nomm'=>array(45,$heightligne,2,'LRT','L','0','0','0','255','255','255',array(0,10),' ','',
'CELLSUP_NON',0,'NA','NC','','B',8,array('0')),
'nom_usagem'=>array(45,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'       -    ','',
'CELLSUP_NON',0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
'prenomm'=>array(45,$heightligne,0,'LBR','L','0','0','0','255','255','255',array(0,0),'    ','',
'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'bureaum'=>array(10,$heightligne,2,'LR','C','0','0','0','255','255','255',array(0,5),'','',
'CELLSUP_VIDE',array(10,$heightligne,0,'LRB','C','0','0','0','255','255','255',array(0,0)),0,'NA','NC','','NB',8,array('0')),
'origine1_1'=>array(50,$heightligne-1.25,2,'LRT','L','0','0','0','255','255','255',array(0,10),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'origine1_2'=>array(50,$heightligne-1.25,2,'LR','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'origine2_1'=>array(50,$heightligne-1.25,2,'LR','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'origine2_2'=>array(50,$heightligne-1.25,0,'LBR','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'refus'=>array(10,$heightligne+10,0,'1','L','0','0','0','255','255','255',array(0,11.25),' ','',
'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'motif_refus_1'=>array(45,$heightligne,2,'LTR','L','0','0','0','255','255','255',array(0,0),' ','',
'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'motif_refus_2'=>array(45,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),' ','',
'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'motif_refus_3'=>array(45,$heightligne,0,'LBR','L','0','0','0','255','255','255',array(0,0),' ','',
'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'dateaccord'=>array(24,$heightligne+2.5,2,'LTR','C','0','0','0','255','255','255',array(0,10),' Le ','',
'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'heureaccord'=>array(24,$heightligne+2.5,0,'LBR','C','0','0','0','255','255','255',array(0,0),_('  a '),'',
'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'debutvalidm'=>array(24,$heightligne+2.5,2,'LTR','C','0','0','0','255','255','255',array(0,7.5),' Du ','',
'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
'finvalidm'=>array(24,$heightligne+2.5,0,'LBR','C','0','0','0','255','255','255',array(0,0),'  Au ','',
'CELLSUP_VIDE',array(24,$heightligne+10,0,'1','C','0','0','0','255','255','255',array(0,7.5)),0,'NA','NC','','NB',8,array('0'))
);
//
$enpied=array();
$pdf->Table($titre,$entete,$col,$enpied,$sql,$f -> db,$param,$pagefin);
// message  aucun enregistrement--------------------------------------------
if ($pdf->msg==1 and $message!='')
{
    $pdf->SetFont('Arial','',10);
    $pdf->SetDrawColor(0,0,0);
    $pdf->SetFillColor(213,8,26);
    $pdf->SetTextColor(255,255,255);
    $pdf->MultiCell(120,5,iconv(HTTPCHARSET,"CP1252",$message),'1','C',1);
}
//--------------------------------------------------------------------------
$aujourdhui = date('Ymd-His');
$pdf->Output("listeregistretotproc-".$aujourdhui.".pdf","I");
$pdf->Close();

?> 

<?php
/**
 * Ce fichier permet de realiser l'edition des statistiques sur les
 * saisies par utilisateur, cette edition est utile seulement pour le mode
 * MULTI
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"edition");

//
define('FPDF_FONTPATH','font/');
require_once ("fpdf.php");
set_time_limit (480);
$aujourdhui = date("d/m/Y");



$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];


//===========================================================================
include ("../sql/".$f -> phptype."/pdf_statistique_saisie_utilisateur.inc");
//===========================================================================


/////////////////////////// Requete Mouvement
$mouvements = array ();
$resmov =& $f -> db -> query ($sqlsaisie0);
$f->isDatabaseError($resmov);
while ($rowmov =& $resmov -> fetchRow (DB_FETCHMODE_ASSOC))
	array_push ($mouvements, $rowmov);
//////////////////////////

$utilisateurs = array();
//////////////////////////


function pdf_utilisateur_entete()
    {
	global $pdf;
        global $aujourdhui;
	global $f;
	$pdf->AddPage();
	// entete
	$pdf->SetFont('courier','B',11);
	$pdf->Cell(200,7,iconv(HTTPCHARSET,"CP1252",_('STATISTIQUE A PROPOS DES SAISIES UTILISATEURS SUR LE TABLEAU EN COURS DU ').$f->formatDate($f -> collectivite ['datetableau'])),'0',0,'L',0);
	$pdf->SetFont('courier','',11);
	$pdf->Cell(40,7, iconv(HTTPCHARSET,"CP1252",$aujourdhui),'0',0,'C',0);
	$pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",_(' Page  :  ').$pdf->PageNo()."/{nb} "),'0',1,'R',0);
	$pdf->ln();
	// Tableau
	$pdf->Cell(105,7,iconv(HTTPCHARSET,"CP1252",_('UTILISATEURS')),1,'0','C',0);
	$pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",_('NOMBRES INSCRIPTIONS')),1,0,'C',0);
	$pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",_('NOMBRES RADIATIONS')),1,0,'C',0);
	$pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",_('NOMBRES MODIFICATIONS')),1,1,'C',0);	
    }


$pdf=new FPDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(true);
$pdf->SetFont('courier','',11);
$pdf->SetDrawColor(30,7,146);
$pdf->SetMargins(5,5,5);
$pdf->SetDisplayMode('real','single');


foreach( $mouvements as $mouvement  ){

    $existant = false;
    foreach ($utilisateurs as $utilisateur){
        if ($utilisateur[0] == $mouvement['utilisateur']){
            $existant = true;
            break;
        }
    }
    
    if($existant){
        $nbr_user = count($utilisateurs);
        $compteur = 0;
        for ($compteur = 0; $compteur < $nbr_user; $compteur++){
            if ($utilisateurs[$compteur][0] == $mouvement['utilisateur']){
                switch($mouvement['typecat']){
                    case "Inscription":
                        $utilisateurs[$compteur][1]++;
                        break;
                    case "Modification":
                        $utilisateurs[$compteur][2]++;
                        break;
                    case "Radiation":
                        $utilisateurs[$compteur][3]++;
                        break;
                }
                break;
            }
        }
    }else{
        $ins = 0;
        $mod = 0;
        $rad = 0;
        switch($mouvement['typecat']){
            case "Inscription":
                $ins++;
                break;
            case "Modification":
                $mod++;
                break;
            case "Radiation":
                $rad++;
                break;
        }
        $uarray = array($mouvement['utilisateur'],$ins,$mod,$rad);
        array_push ($utilisateurs,  $uarray);
    }

}

/// mise en tableau - mouvement utilisateurs
pdf_utilisateur_entete();
$nb_radiation = 0;
$nb_modification = 0;
$nb_inscription = 0;
$cpt = 0;
foreach ( $utilisateurs as $utilisateur){
    $cpt++;
    if($cpt >= 22){
        $cpt=1;
        pdf_utilisateur_entete();
    } 
    
    $pdf->Cell(105,7,'    '.iconv(HTTPCHARSET,"CP1252",$utilisateur[0]),0,'0','L',0);
    $pdf->Cell(60,7,''.iconv(HTTPCHARSET,"CP1252",$utilisateur[1]),0,0,'C',0);
    $pdf->Cell(60,7,''.iconv(HTTPCHARSET,"CP1252",$utilisateur[3]),0,0,'C',0);
    $pdf->Cell(60,7,''.iconv(HTTPCHARSET,"CP1252",$utilisateur[2]),0,1,'C',0);
    $nb_radiation = $nb_radiation + $utilisateur[3];
    $nb_modification = $nb_modification + $utilisateur[2];
    $nb_inscription =  $nb_inscription + $utilisateur[1];
}
    $pdf->Cell(105,7,iconv(HTTPCHARSET,"CP1252",_('TOTAL')),1,'0','C',0);
    $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",$nb_inscription),1,0,'C',0);
    $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",$nb_radiation),1,0,'C',0);
    $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",$nb_modification),1,1,'C',0);

/////////////////////////////////////////////////////////////////////////////////////

$aujourdhui = date('Ymd-His');
$pdf->Output("Stats_saisies_utilisateurs-".$aujourdhui.".pdf","I");
$pdf->Close();

?> 
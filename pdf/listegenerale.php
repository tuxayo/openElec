<?php
/**
 * Ce fichier permet de realiser l'edition des listes electorales
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"edition");

//
require_once ("../app/fpdf_table.php");
set_time_limit (480);
$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];
if (isset($_GET['idx'])) 
{
	$nobureau=$_GET['idx'];
	$message=sprintf(_("LISTE GENERALE\nListe %snAucun enregistrement selectionne pour le bureau %s"),$nolibliste,$_GET['idx']);
}

$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(false);
$pdf->AddPage();
$pdf->SetFont('Arial','');
$pdf->SetDrawColor(30,7,146);
$pdf->SetMargins(10,10,10);
$pdf->SetDisplayMode('real','single');

$param=array();
$param=array(8,8,8,1,1,10,10,10,10,0,0);

//--------------------------------------------- nombre enregistrement//
$ligne=0;
//------------------------titre---------------------------------------------//
$aujourdhui = date("d/m/Y");
$heighttitre=4;
$titre=array();
$titre=array(
array(sprintf(_('Le %s'),$aujourdhui),210,$heighttitre,0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array('<PAGE>',40,$heighttitre,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('LISTE ELECTORALE   -   COMMUNE :  %s'),$f -> collectivite ['ville']),120,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array($nolibliste,90,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0)
);
//------------------------entete colonne   ---------------------------------------------
$heightentete=8;
$entete=array();
$entete=array(
array(_('Nom Patronymique, Prenoms'),125,$heightentete-4,2,'LRT','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Nom d\'Usage, Date et lieu de naissance%s'),($_SESSION["liste"]!="01" ? _(", Nationalite"): "")),125,$heightentete-4,0,'LRB','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Adresse'),90,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,4),0,'','NB',0,1,0),
array('<VCELL>',15,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),3,array('Code','Electeur'),'NB',0,1,0),
array(_('Bureau'),15,$heightentete-4,2,'LRT','C','0','0','0','255','255','255',array(0,8),0,'','NB',0,1,0),
array(_('No Ordre'),15,$heightentete-4,0,'LRB','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Observation(s)'),30,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,4),0,'','NB',0,1,0)
);
//------------------------page fin -----------------------------------------------------
$pagefin=array();
$pagefin=array(
array(sprintf(_('Le %s'),$aujourdhui),210,$heighttitre,0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',8,1,0),
array('<PAGE>',40,$heighttitre,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',8,1,0),
array(sprintf(_('LISTE ELECTORALE   -   COMMUNE :  %s'),$f -> collectivite ['ville']),120,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','B',8,1,0),
array(sprintf(_('Liste %s'),$nolibliste),90,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',8,1,0),
array(_(' *  *  *  A R R E T E  *  *  *'),260,10,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','B',8,1,0),
array(_('LA LISTE ELECTORALE AU NOMBRE DE '),160,10,0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array('<LIGNE>',13,10,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',9,1,0),
array(_(' ELECTEURS.'),25,10,2,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('%s , le %s'),$f -> collectivite ['ville'], date("d/m/Y")),75,10,1,'0','C','0','0','0','255','255','255',array(50,0),0,'','NB',0,1,0),
array(' ',260,50,1,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',8,1,0),
array(_('Le Maire'),86,10,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','B',8,1,0),
array(_('Le delegue du prefet '),86,10,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',8,1,0),
array(_('Le delegue du T.G.I '),86,10,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',8,1,0)
);
//
//------------------------page debut -----------------------------------------------------
$pagedebut=array();
$annee=(string)((int)date("Y")-1)." - ".date("Y");
$libbureau="";
include ("../sql/".$f->phptype."/listebureau.inc");
$res=$f->db->query($sql);
if (database::isError($res))
	die($res->getMessage()."erreur ".$res->getUserinfo());
while ($row=& $res->fetchRow())
	$libbureau=$row[0];

$pagedebut=array(
	array(_('FICHIER ELECTORAL'),260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
	array(sprintf(_('ANNEE : %s'),$annee),260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
	array(_('LISTE ELECTORALE'),260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
	array('',260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
	array(sprintf(_('COMMUNE: %s'),$f -> collectivite ['ville']),260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
	array(sprintf(_('LISTE: %s'),$nolibliste),260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0)
);

if (isset($_GET['idx']))
	$pagedebut[]= array(sprintf(_('BUREAU: %s %s'),$nobureau, $libbureau),260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0);

include ("../sql/".$f -> phptype."/pdf_listegenerale.inc");

$heightligne=15;


//
$line_height = 4;
//
$col = array();
$col = array(
    //
    'nomprenom' => array(
        125,
        $line_height,
        2,
        'LRT', // Border
        'L', // Align
        '0', '0', '0', // Text Color
        '255', '255', '255', // Fill Color
        array(0, 0),
        ' ',
        '',
        'CELLSUP_NON',
        '0',
        'NA',
        'NC',
        '',
        'B',
        9,
        array('0'),
    ),
    //
	'nom_usage' => array(
        125,
        $line_height,
        2,
        'LR', // Border
        'L', // Align
        '0', '0', '0', // Text Color
        '255', '255', '255', // Fill Color
        array(0, 0),
        '       -    ',
        '',
        'CELLSUP_NON',
        0,
        'NA',
        'CONDITION',
        array('NN'),
        'NB',
        0,
        array('0'),
    ),
    //
	'naissance' => array(
        31,
        $line_height,
        0,
        'L', // Border
        'L', // Align
        '0', '0', '0', // Text Color
        '255', '255', '255', // Fill Color
        array(0, 0),
        _('     Ne(e) le '),
        '',
        'CELLSUP_NON',
        '0',
        'NA',
        'NC',
        '',
        'NB',
        0,
        array('0'),
    ),
    //
	'lieu' => array(
        94,
        $line_height,
        1,
        'R', // Border
        'L', // Align
        '0', '0', '0', // Text Color
        '255', '255', '255', // Fill Color
        array(0, 0),
        _(' a  '),
        '',
        'CELLSUP_NON',
        '0',
        'NA',
        'NC',
        '',
        'NB',
        0,
        array('0'),
    ),
    //
	'nationalite' => array(
        125,
        $line_height,
        0,
        'LRB', // Border
        'L', // Align
        '0', '0', '0', // Text Color
        '255', '255', '255', // Fill Color
        array(0, 0),
        '     ',
        '',
        'CELLSUP_NON',
        0,
        'NA',
        'CONDITION',
        array('NN'),
        'NB',
        0,
        array('0'),
    ),
    //
	'adresse' => array(
        90,
        $line_height,
        2,
        'LRT', // Border
        'L', // Align
        '0', '0', '0', // Text Color
        '255', '255', '255', // Fill Color
        array(0, 12),
        '',
        '',
        'CELLSUP_NON',
        0,
        'NA',
        'NC',
        '',
        'NB',
        0,
        array('0'),
    ),
	//
    'complement' => array(
        90,
        $line_height,
        2,
        'LR', // Border
        'L', // Align
        '0', '0', '0', // Text Color
        '255', '255', '255', // Fill Color
        array(0, 0),
        '',
        '',
        'CELLSUP_NON',
        0,
        0,
        'NA',
        'NC',
        '',
        'NB',
        0,
        array('0'),
    ),
	//
    'blank2' => array(
        90,
        $line_height,
        2,
        'LR', // Border
        'L', // Align
        '0', '0', '0', // Text Color
        '255', '255', '255', // Fill Color
        array(0, 0),
        '',
        '',
        'CELLSUP_NON',
        0,
        0,
        'NA',
        'NC',
        '',
        'NB',
        0,
        array('0'),
    ),
	//
    'blank3' => array(
        90,
        $line_height,
        0,
        'LRB', // Border
        'L', // Align
        '0', '0', '0', // Text Color
        '255', '255', '255', // Fill Color
        array(0, 0),
        '',
        '',
        'CELLSUP_NON',
        0,
        0,
        'NA',
        'NC',
        '',
        'NB',
        0,
        array('0'),
    ),
	//
    'numero_electeur' => array(
        15,
        $line_height*4,
        0,
        '1', // Border
        'R', // Align
        '0', '0', '0', // Text Color
        '255', '255', '255', // Fill Color
        array(0, 12),
        '',
        '',
        'CELLSUP_NON',
        '0',
        'NA',
        'NC',
        '',
        'NB',
        0,
        array('0'),
    ),
	//
    'code_bureau' => array(
        15,
        $line_height*2,
        2,
        'LRT', // Border
        'R', // Align
        '0', '0', '0', // Text Color
        '255', '255', '255', // Fill Color
        array(0, 0),
        '',
        '',
        'CELLSUP_NON',
        '0',
        'NA',
        'NC',
        '',
        'NB',
        0,
        array('0'),
    ),
	//
    'numero_bureau' => array(
        15,
        $line_height*2,
        0,
        'LRB', // Border
        'R', // Align
        '0', '0', '0', // Text Color
        '255', '255', '255', // Fill Color
        array(0, 0),
        '',
        '',
        'CELLSUP_VIDE',
        array(
            30,
            $line_height*4,
            0,
            '1', // Border
            'C', // Align
            '0', '0', '0', // Text Color
            '255', '255', '255', // Fill Color
            array(0, 8),
        ),
        'NA',
        'NC',
        '',
        'NB',
        0,
        array('0'),
    )
);

$enpied=array();
$pdf->Table($titre,$entete,$col,$enpied,$sql,$f -> db,$param,$pagefin,$pagedebut);

if ($pdf->msg==1 and $message!='')
{
	$pdf->SetFont('Arial','',10);
	$pdf->SetDrawColor(0,0,0);
	$pdf->SetFillColor(213,8,26);
	$pdf->SetTextColor(255,255,255);
	$pdf->MultiCell(120,5,iconv(HTTPCHARSET,"CP1252",$message),'1','C',1);
}

if (isset($_GET['id'])) {
    // Construction du nom du fichier
    $filename = date("Ymd-His");
    $filename .= "-listegenerale";
    $filename .= "-".$_SESSION['collectivite'];
    $filename .= "-".$f->normalizeString($_SESSION['libelle_collectivite']);
    $filename .= "-liste".$_SESSION['liste'];
    if ($nobureau != "") {
        $filename .= "-bureau".$nobureau;
    }
    $filename .= ".pdf";
    //
    $pdf->Output($filename, "I");
    // Destruction de l'objet PDF
    $pdf->Close();
} else {
    // Construction du nom du fichier
    $filename = date("Ymd-His");
    $filename .= "-listegenerale";
    $filename .= "-".$_SESSION['collectivite'];
    $filename .= "-".$f->normalizeString($_SESSION['libelle_collectivite']);
    $filename .= "-liste".$_SESSION['liste'];
    $filename .= ".pdf";
    //
    if (isset($_GET['multi']) and $_GET['multi'] == true) {
        $filename = $f->getParameter("pdfdir").$filename;
        $contenu = $pdf->Output("", "S");
    } elseif (isset($_GET['mode']) and $_GET['mode'] == "save") {
        $filename_with_path = $f->getParameter("pdfdir").$filename;
        $pdf->Output($filename_with_path, "F");
    } else {
	    $filename = $f->getParameter("pdfdir")."listegenerale-".$_SESSION['collectivite']."-".$_SESSION['liste'].".pdf";
        $pdf->Output($filename, "F");
    }
    // Destruction de l'objet PDF
    $pdf->Close();
    // Redirect
    @header ("Location: ../app/editions.php");
}

?>

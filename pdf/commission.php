<?php
/**
 * Ce fichier permet de générer une édition au format PDF.
 * 
 * L'édition générée ici est soit : un tableau rectificatif, 
 * un tableau des additions, un tableau pour la commission. Les 
 * différentes possibilités d'impression sont : pour un bureau, 
 * sur toute la commune sans rupture par bureau, entre deux dates, ...
 * Le principe est que l'édition est un listing de mouvements
 * regroupé par catégories.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) {
    $f = new utils("nohtml", /*DROIT*/"edition"); 
}

/**
 *
 */
//
$query_electeurs_actuels_groupby_bureau = " SELECT electeur.code_bureau as bureau, electeur.sexe as sexe, count(electeur.id_electeur) as total ";
$query_electeurs_actuels_groupby_bureau .= " FROM electeur ";
$query_electeurs_actuels_groupby_bureau .= " WHERE electeur.collectivite='".$_SESSION['collectivite']."' ";
$query_electeurs_actuels_groupby_bureau .= " AND electeur.liste='".$_SESSION['liste']."' ";
$query_electeurs_actuels_groupby_bureau .= " GROUP BY electeur.code_bureau, electeur.sexe";
//
$query_inscriptions_radiations_stats_groupby_bureau = " SELECT mouvement.code_bureau as bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe as sexe, count(*) as total";
$query_inscriptions_radiations_stats_groupby_bureau .= " FROM mouvement INNER JOIN param_mouvement ";
$query_inscriptions_radiations_stats_groupby_bureau .= " ON mouvement.types=param_mouvement.code ";
$query_inscriptions_radiations_stats_groupby_bureau .= " WHERE mouvement.collectivite='".$_SESSION['collectivite']."' ";
$query_inscriptions_radiations_stats_groupby_bureau .= " AND mouvement.liste='".$_SESSION['liste']."' ";
$query_inscriptions_radiations_stats_groupby_bureau .= " AND (param_mouvement.typecat='Inscription' OR param_mouvement.typecat='Radiation') ";
$query_inscriptions_radiations_stats_groupby_bureau .= " GROUP BY mouvement.code_bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe ";
$query_inscriptions_radiations_stats_groupby_bureau .= " ORDER BY mouvement.code_bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe ";
//
$query_transferts_plus_stats_groupby_bureau = " SELECT mouvement.code_bureau as bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe as sexe, count(*) as total";
$query_transferts_plus_stats_groupby_bureau .= " FROM mouvement INNER JOIN param_mouvement ";
$query_transferts_plus_stats_groupby_bureau .= " ON mouvement.types=param_mouvement.code ";
$query_transferts_plus_stats_groupby_bureau .= " WHERE mouvement.collectivite='".$_SESSION['collectivite']."' ";
$query_transferts_plus_stats_groupby_bureau .= " AND mouvement.liste='".$_SESSION['liste']."' ";
$query_transferts_plus_stats_groupby_bureau .= " AND mouvement.code_bureau <> mouvement.ancien_bureau ";
$query_transferts_plus_stats_groupby_bureau .= " AND param_mouvement.typecat='Modification' ";
$query_transferts_plus_stats_groupby_bureau .= " AND mouvement.ancien_bureau<>'' ";
$query_transferts_plus_stats_groupby_bureau .= " GROUP BY mouvement.code_bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe ";
//
$query_transferts_moins_stats_groupby_bureau = " SELECT mouvement.ancien_bureau as bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe as sexe, count(*) as total";
$query_transferts_moins_stats_groupby_bureau .= " FROM mouvement INNER JOIN param_mouvement ";
$query_transferts_moins_stats_groupby_bureau .= " ON mouvement.types=param_mouvement.code ";
$query_transferts_moins_stats_groupby_bureau .= " WHERE mouvement.collectivite='".$_SESSION['collectivite']."' ";
$query_transferts_moins_stats_groupby_bureau .= " AND mouvement.liste='".$_SESSION['liste']."' ";
$query_transferts_moins_stats_groupby_bureau .= " AND mouvement.code_bureau <> mouvement.ancien_bureau ";
$query_transferts_moins_stats_groupby_bureau .= " AND param_mouvement.typecat='Modification' ";
$query_transferts_moins_stats_groupby_bureau .= " AND mouvement.ancien_bureau<>'' ";
$query_transferts_moins_stats_groupby_bureau .= " GROUP BY mouvement.ancien_bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe ";
// Nombre actuel d'electeurs dans la base de donnees par bureau
// array("<BUREAU>" => array("M" => <>, "F" => <>, "total" => <>),);
$electeurs_actuels_groupby_bureau = array();
$res_electeurs_actuels_groupby_bureau = $f->db->query($query_electeurs_actuels_groupby_bureau);
$f->addToLog("trt/stats_recapitulatif_tableau.php: db->query(\"".$query_electeurs_actuels_groupby_bureau."\")", VERBOSE_MODE);
$f->isDatabaseError($res_electeurs_actuels_groupby_bureau);
while ($row_electeurs_actuels_groupby_bureau =& $res_electeurs_actuels_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
    //
    if (!isset($electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]])) {
        $electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]] = array();
    }
    //
    $electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]][$row_electeurs_actuels_groupby_bureau["sexe"]] = $row_electeurs_actuels_groupby_bureau["total"];
}
foreach ($electeurs_actuels_groupby_bureau as $key => $value) {
    $electeurs_actuels_groupby_bureau[$key]["total"] = (isset($electeurs_actuels_groupby_bureau[$key]["M"]) ? $electeurs_actuels_groupby_bureau[$key]["M"] : 0) + (isset($electeurs_actuels_groupby_bureau[$key]["F"]) ? $electeurs_actuels_groupby_bureau[$key]["F"] : 0);
}
//
$inscriptions_radiations_stats_groupby_bureau = array();
$res_inscriptions_radiations_stats_groupby_bureau = $f->db->query($query_inscriptions_radiations_stats_groupby_bureau);
$f->addToLog("trt/stats_recapitulatif_tableau.php: db->query(\"".$query_inscriptions_radiations_stats_groupby_bureau."\")", VERBOSE_MODE);
$f->isDatabaseError($res_inscriptions_radiations_stats_groupby_bureau);
while ($row_inscriptions_radiations_stats_groupby_bureau =& $res_inscriptions_radiations_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
    //
    if (!isset($inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]])) {
        $inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]] = array();
    }
    //
    array_push($inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]], $row_inscriptions_radiations_stats_groupby_bureau);
}
$f->addToLog("trt/stats_recapitulatif_tableau.php: ".print_r($inscriptions_radiations_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);
//
$transferts_plus_stats_groupby_bureau = array();
$res_transferts_plus_stats_groupby_bureau = $f->db->query($query_transferts_plus_stats_groupby_bureau);
$f->addToLog("trt/stats_recapitulatif_tableau.php: db->query(\"".$query_transferts_plus_stats_groupby_bureau."\")", VERBOSE_MODE);
$f->isDatabaseError($res_transferts_plus_stats_groupby_bureau);
while ($row_transferts_plus_stats_groupby_bureau =& $res_transferts_plus_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
    //
    if (!isset($transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]])) {
        $transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]] = array();
    }
    //
    array_push($transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]], $row_transferts_plus_stats_groupby_bureau);
}
$f->addToLog("trt/stats_recapitulatif_tableau.php: ".print_r($transferts_plus_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);
//
$transferts_moins_stats_groupby_bureau = array();
$res_transferts_moins_stats_groupby_bureau = $f->db->query($query_transferts_moins_stats_groupby_bureau);
$f->addToLog("trt/stats_recapitulatif_tableau.php: db->query(\"".$query_transferts_moins_stats_groupby_bureau."\")", VERBOSE_MODE);
$f->isDatabaseError($res_transferts_moins_stats_groupby_bureau);
while ($row_transferts_moins_stats_groupby_bureau =& $res_transferts_moins_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
    //
    if (!isset($transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]])) {
        $transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]] = array();
    }
    //
    array_push($transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]], $row_transferts_moins_stats_groupby_bureau);
}
$f->addToLog("trt/stats_recapitulatif_tableau.php: ".print_r($transferts_moins_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);
//
if (!function_exists('calculNombreDelecteursDateTableauBureau')) {
    /**
     * 
     */
    function calculNombreDelecteursDateTableauBureau($date_tableau_reference, $bureau, $sexe = "ALL") {
        //
        global $electeurs_actuels_groupby_bureau;
        global $inscriptions_radiations_stats_groupby_bureau;
        global $transferts_plus_stats_groupby_bureau;
        global $transferts_moins_stats_groupby_bureau;
        //
        if ($sexe == "ALL") {
            $cpt_electeur = $electeurs_actuels_groupby_bureau[$bureau]["total"];
        } else {
            $cpt_electeur = $electeurs_actuels_groupby_bureau[$bureau][$sexe];
        }
        //
        if (isset($inscriptions_radiations_stats_groupby_bureau[$bureau])) {
            foreach ($inscriptions_radiations_stats_groupby_bureau[$bureau] as $element) {
                if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                    //
                    if ($element["date_tableau"] > $date_tableau_reference) {
                        if ($element["etat"] == "trs") {
                            if ($element["typecat"] == "Inscription") {
                                $cpt_electeur -= $element["total"];
                            } elseif ($element["typecat"] == "Radiation") {
                                $cpt_electeur += $element["total"];
                            }
                        }
                    } else {
                        if ($element["etat"] == "actif") {
                            if ($element["typecat"] == "Inscription") {
                                $cpt_electeur += $element["total"];
                            } elseif ($element["typecat"] == "Radiation") {
                                $cpt_electeur -= $element["total"];
                            }
                        }
                    }
                }
            }
        }
        //
        if (isset($transferts_plus_stats_groupby_bureau[$bureau])) {
            foreach ($transferts_plus_stats_groupby_bureau[$bureau] as $element) {
                if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                    //
                    if ($element["date_tableau"] > $date_tableau_reference) {
                        if ($element["etat"] == "trs") {
                            $cpt_electeur -= $element["total"];
                        }
                    } else {
                        if ($element["etat"] == "actif") {
                            $cpt_electeur += $element["total"];
                        }
                    }
                }
            }
        }
        //
        if (isset($transferts_moins_stats_groupby_bureau[$bureau])) {
            foreach ($transferts_moins_stats_groupby_bureau[$bureau] as $element) {
                if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                    //
                    if ($element["date_tableau"] > $date_tableau_reference) {
                        if ($element["etat"] == "trs") {
                                $cpt_electeur += $element["total"];
                        }
                    } else {
                        if ($element["etat"] == "actif") {
                            $cpt_electeur -= $element["total"];
                        }
                    }
                }
            }
        }
        return $cpt_electeur;
    }
}

/**
 * Gestion du nombre d'electeurs a des dates anterieures
 */
//
$query_count_all_mouvement_by_typecat = " select date_tableau, date_j5, typecat, effet, types, libelle, etat, count(*) ";
$query_count_all_mouvement_by_typecat .= " from mouvement as m inner join param_mouvement as pm on m.types=pm.code ";
$query_count_all_mouvement_by_typecat .= " where liste='".$_SESSION['liste']."' ";
$query_count_all_mouvement_by_typecat .= " and collectivite='".$_SESSION['collectivite']."' ";
$query_count_all_mouvement_by_typecat .= " group by date_tableau, date_j5, typecat, effet, types, libelle, etat ";
$query_count_all_mouvement_by_typecat .= " order by date_tableau, date_j5, typecat, effet, types, libelle, etat ";
// 
$res_count_all_mouvement_by_typecat = $f->db->query($query_count_all_mouvement_by_typecat);
// Logger
$f->addToLog("app/revision_electoral.php: db->query(\"".$query_count_all_mouvement_by_typecat."\")", VERBOSE_MODE);
// Gestion des erreurs
$f->isDatabaseError($res_count_all_mouvement_by_typecat);
//
$all_mouvement_by_typecat = array();
while ($row_count_all_mouvement_by_typecat =& $res_count_all_mouvement_by_typecat->fetchRow(DB_FETCHMODE_ASSOC)) {
    array_push($all_mouvement_by_typecat, $row_count_all_mouvement_by_typecat);
}
$f->addToLog("app/revision_electorale.php : ".print_r($all_mouvement_by_typecat, true), EXTRA_VERBOSE_MODE);
//
if (!function_exists('calculNombreDelecteursDate')) {
    /**
     * 
     */
    function calculNombreDelecteursDate($date_tableau_reference, $electeur_aujourdhui) {
        //
        global $all_mouvement_by_typecat;
        //
        $cpt_electeur = $electeur_aujourdhui;
        //
        foreach ($all_mouvement_by_typecat as $element) {
            //
            if ($element["date_tableau"] > $date_tableau_reference) {
                if ($element["etat"] == "trs") {
                    if ($element["typecat"] == "Inscription") {
                        $cpt_electeur -= $element["count"];
                    } elseif ($element["typecat"] == "Radiation") {
                        $cpt_electeur += $element["count"];
                    }
                }
            } else {
                if ($element["etat"] == "actif") {
                    if ($element["typecat"] == "Inscription") {
                        $cpt_electeur += $element["count"];
                    } elseif ($element["typecat"] == "Radiation") {
                        $cpt_electeur -= $element["count"];
                    }
                }
            }
        }
        return $cpt_electeur;
    }
}

/**
 *
 */
//
$edition_avec_recapitulatif = true;
if (isset($_GET["edition_avec_recapitulatif"])) {
    $edition_avec_recapitulatif = ($_GET["edition_avec_recapitulatif"] == "false" ? false : true);
}
$mode_edition = "";
$tableau = "";
if (isset($_GET["datetableau"])) {
    $datetableau = $_GET["datetableau"];
} else {
    $datetableau = $f->collectivite["datetableau"];
}
if (isset($_GET["datej5"])) {
    $datej5 = $_GET["datej5"];
} else {
    $datej5 = false;
}
if (isset($_GET["globale"])) {
    $globale = true;
} else {
    $globale = false;
}
//
$date_debut = "";
$date_fin = "";
// Gestion des dates Pour INTERNET EXPLORER 7
if (isset($_GET['first']) && isset($_GET['last'])) {
    //
    if (preg_match("#([0-9]{1,2})([0-9]{1,2})([0-9]{4})#", $_GET ['first'], $regs)
        || preg_match("#([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})#", $_GET ['first'], $regs)
    ) {
        $annee = $regs[3];
        $mois = $regs [2];
        $jour = $regs [1];
        if (checkdate($mois, $jour, $annee)) {
            $date_debut = $annee."-".$mois."-".$jour;
        }
    }
    //
    if (preg_match("#([0-9]{1,2})([0-9]{1,2})([0-9]{4})#", $_GET ['last'], $regs) 
        || preg_match("#([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})#", $_GET ['last'], $regs)
    ) {
        $annee = $regs[3];
        $mois = $regs [2];
        $jour = $regs [1];
        if (checkdate($mois, $jour, $annee)) {
            $date_fin = $annee."-".$mois."-".$jour;
        }
    }
    //
    if ($date_fin < $date_debut) {
        $date_debut = "";
        $date_fin = "";
    }
    //
    if ($date_debut == "" || $date_fin == "") {
        die("Les dates sont incorrectes");
    }
}
$libelle_commune = $f->collectivite["ville"];
$libelle_liste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];
$pagedebut_commission = array();

/**
 * Recuperation des informations sur les bureaux de vote
 */

// Initialisation 
$liste_bureaux = array();

// Si ($globale == false), alors l'édition doit se faire avec une rupture par bureau
// Donc on remplit la variable $liste_bureaux avec tous les bureaux de vote de la commune
if ($globale == false) {
    //
    if ($edition_avec_recapitulatif == true) {
        $liste_bureaux = array(
            0 => array(
                "bureau" => "ALL",
                "libelle_bureau" => "",
                "libelle_canton" => "",
            )
        );
    }
    // Inclut les requêtes
    // passage == 0 signifit que c'est la première inclusion du fichier
    // cette première inclusion vise à récupérer la requête :
    //  - $query_liste_bureaux
    $passage = 0;
    include "../sql/".OM_DB_PHPTYPE."/pdf_commission.inc";
    //
    $res = $f->db->query($query_liste_bureaux);
    //
    $f->addToLog("pdf/commission.php: db->query(\"".$query_liste_bureaux."\")", VERBOSE_MODE);
    //
    $f->isDatabaseError($res);
    //
    while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
        array_push($liste_bureaux, $row);
    }
    //
    $res->free();
    //
    $f->addToLog("pdf/commission.php: \$liste_bureaux = ".print_r($liste_bureaux, true), EXTRA_VERBOSE_MODE);
    
    /**
     *
     */
    //
    if (isset($_GET['idx'])) {
        //
        $liste_bureaux_temp = array();
        foreach ($liste_bureaux as $bureau) {
            if ($bureau["bureau"] == $_GET["idx"]) {
                array_push($liste_bureaux_temp, $bureau);
            }
        }
        if (count($liste_bureaux_temp) == 1) {
            $liste_bureaux = $liste_bureaux_temp;
        } else {
            $f->notExistsError(_("Les parametres ne sont pas corrects."));
        }
    }
} else {
    // Si ($globale == true), alors l'édition se fait sans rupture par bureau
    // Donc on remplit la variable $liste_bureaux avec une unique ligne : "ALL"
    $liste_bureaux = array(0 => array("bureau" => "ALL",
                                      "libelle_bureau" => "",
                                      "libelle_canton" => ""));
}

//
(!isset($_GET["mode_edition"]) ? $_GET["mode_edition"] = "commission" : "");
//
switch($_GET["mode_edition"]) {
//
case "tableau":
    //
    $mode_edition = "tableau";
    //
    (!isset($_GET["tableau"]) ? $tableau = "tableau_rectificatif" : "");
    //
    switch($_GET["tableau"]) {
    //
    case "tableau_rectificatif":
        //
        $tableau = "tableau_rectificatif";
        //
        $filename_objet = "tableau-rectificatif";
        $titre_libelle_ligne1 = _("TABLEAU RECTIFICATIF DU")." ".$f->formatDate($datetableau);
        $titre_libelle_ligne2 = "";
        $pagefin_libelle = sprintf(
            _("Arrete le tableau du %s a :"), 
            $f->formatDate($datetableau)
        );
        //
        $mouvement_categories = array("Inscription", "Radiation",);
        //
        break;
    //
    case "tableau_des_cinq_jours":
        //
        $tableau = "tableau_des_cinq_jours";
        //
        $filename_objet = "tableau-des-cinq-jours";
        $titre_libelle_ligne1 = _("TABLEAU DES CINQ JOURS DU")." ".($datej5 == false ? date("d/m/Y") : $f->formatDate($datej5));
        $titre_libelle_ligne2 = sprintf(
            _("[Pour le tableau rectificatif du %s]"), 
            $f->formatDate($datetableau)
        );
        $pagefin_libelle = _("Nombre :");
        //
        $mouvement_categories = array("Inscription", "Radiation",);
        //
        break;
    //
    case "tableau_des_additions_des_jeunes":
        //
        (!isset($_GET["type"]) ? $f->notExistsError(_("Les parametres ne sont pas corrects.")) : "");
        //
        $query_libelle_mouvement = "select libelle from param_mouvement where code='".addslashes($_GET["type"])."'";
        $libelle_mouvement = $f->db->getone($query_libelle_mouvement);
        $f->isDatabaseError($libelle_mouvement);
        //
        $tableau = "tableau_des_additions_des_jeunes";
        //
        $filename_objet = "tableau-des-additions-des-jeunes";
        $titre_libelle_ligne1 = _("TABLEAU DES ADDITIONS : JEUNES DE 18 ANS (L11-2)");
        $titre_libelle_ligne2 = sprintf(
            _("%s [Pour le tableau rectificatif du %s]"), 
            $libelle_mouvement, 
            $f->formatDate($datetableau)
        );
        $pagefin_libelle = _("Nombre :");
        //
        $mouvement_categories = array("Inscription",);
        //
        $type_mouvement_io = $_GET["type"];
        //
        break;
    };
    break;
//
case "commission":
    //
    $mode_edition = "commission";
    //
    (!isset($_GET["commission"]) ? $_GET["commission"] = "globale" : "");
    //
    switch($_GET["commission"]) {
    //
    case "globale":
        //
        $commission = "globale";
        //
        $filename_objet = "commission";
        $titre_libelle_ligne1 = sprintf(
            _("ETAT POUR LA COMMISSION, TABLEAU DU %s"), 
            $f->formatDate($datetableau)
        );
        $titre_libelle_ligne2 = "";
        $pagefin_libelle = sprintf(
            _("Arrete le tableau du %s a"), 
            $f->formatDate($datetableau)
        );
        //
        $mouvement_categories = array("Inscription", "Radiation",);
        break;
    case "entre_deux_dates":
        //
        $commission = "entre_deux_dates";
        //
        $filename_objet = "commission-entre-deux-dates";
        $titre_libelle_ligne1 = sprintf(
            _("ETAT POUR LA COMMISSION, TABLEAU DU %s"), 
            $f->formatDate($datetableau)
        );
        $titre_libelle_ligne2 = sprintf(
            _("Mouvements du %s au %s"), 
            $f->formatDate($date_debut),
            $f->formatDate($date_fin)
        );
        $pagefin_libelle = sprintf(
            _("Arrete le tableau du %s a"), 
            $f->formatDate($datetableau)
        );
        //
        $mouvement_categories = array("Inscription", "Radiation",);
        break;
    };
    break;
};


//


//
$sans_modification = $f->getParameter("sans_modification");

/**
 * Ouverture du fichier pdf
 */
//
require_once "../app/fpdf_table.php";
set_time_limit(480);
//
$pdf = new PDF('L', 'mm', 'A4');
//
$pdf->Open();
//
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(false);
$pdf->SetFont('Arial', '');
$pdf->SetDrawColor(30, 7, 146);
$pdf->SetMargins(5, 5, 5);
$pdf->SetDisplayMode('real', 'single');

/**
 * BOUCLE SUR CHAQUE BUREAU
 */
//
foreach ($liste_bureaux as $bureau) {
    /**
     * 
     */
    // Initialise les variables du bureau et du canton
    $nobureau = $bureau["bureau"];
    $libelle_bureau = $bureau["libelle_bureau"];
    $libelle_canton = $bureau["libelle_canton"];

    /**
     * COMPOSITION DES PREMIERES PAGES
     */
    // Inclut les requêtes
    // passage == 1 signifit que c'est la seconde inclusion du fichier
    // cette seconde inclusion vise à récupérer les requêtes :
    //  - $nbins
    //  - $nbins_actif
    //  - $nbrad
    //  - $nbrad_actif
    //  - $nbelec
    //  - $nb_addition_dtsup
    //  - $nb_radiation_dtsup
    //  - $query_nb_additions
    //  - $query_nb_radiations
    $passage = 1;
    include "../sql/".OM_DB_PHPTYPE."/pdf_commission.inc";
    //
    if ($mode_edition == "tableau" && $tableau == "tableau_rectificatif"
        || $mode_edition == "commission" && $commission == "globale"
    ) {
        //// L'édition pour la commission globale ou pour le tableau 
        //// rectificatif sont identiques sauf le titre de la page.
        //// XXX Deux titres pour une même édition, est-ce vraiment utile ?
        /**
         *
         */
        require_once "../obj/revisionelectorale.class.php";
        $revision = new revisionelectorale($f);
        $datetableau_precedente = $revision->get_previous_date_tableau($datetableau);
        // Inscriptions
        $nb_add = $f->db->getone($nbins);
        $f->isDatabaseError($nb_add);
        // Inscriptions dans l'etat actif
        $nb_add_actif = $f->db->getone($nbins_actif);
        $f->isDatabaseError($nbins_actif);
        // Radiations
        $nb_rad = $f->db->getone($nbrad);
        $f->isDatabaseError($nb_rad);
        // Radiations dans l'etat actif
        $nb_rad_actif = $f->db->getone($nbrad_actif);
        $f->isDatabaseError($nb_rad_actif);
        // Electeurs
        $nb_elec = $f->db->getone($nbelec);
        $f->isDatabaseError($nb_elec);
        // Recuperation du nombre d'additions traitees aux dates de tableau superieures
        // ou egales a la date de tableau actuelle
        $res_nb_addition_dtsup = $f->db->getOne($nb_addition_dtsup);
        $f->isDatabaseError($res_nb_addition_dtsup);
        // Recuperation du nombre de radiations traitees aux dates de tableau
        // superieures ou egales a la date de tableau actuelle 
        $res_nb_radiation_dtsup = $f->db->getOne($nb_radiation_dtsup);
        $f->isDatabaseError($res_nb_radiation_dtsup);
        // Calcul du nombre d'électeurs à la date de tableau précédente
        // Au tableau precedent le nombre d'electeurs est egal au nombre d'electeurs
        // dans la table electeur a l'heure actuelle auquel on enleve toutes les
        // additions traitees aux dates de tableau superieures ou egales a la date
        // de tableau actuelle et auquel on ajoute toutes les radiations traitees
        // aux dates de tableau superieures ou egales a la date de tableau actuelle
        if ($nobureau == "ALL") {
            $nb = calculNombreDelecteursDate($datetableau_precedente, $nb_elec);
        } else {
            $nb = calculNombreDelecteursDateTableauBureau($datetableau_precedente, $nobureau);
        }
        // Calcul du nombre d'électeurs à la date de tableau suivante
        // Au tableau suivant le nombre d'electeurs est egal au nombre d'electeurs
        // au tableau precedent calcule juste au dessus auquel on ajoute toutes
        // les additions de la date de tableau en cours et auquel on enleve toutes
        // les radiations de la date de tableau en cours
        $nb_newelec = $nb + $nb_add - $nb_rad;
        //
        $pagedebut = array(
            //
            array(sprintf(_("Nombre d'inscrits au %s : "), $f->formatdate($datetableau_precedente)),
                  160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array($nb,
                  30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array(_("Additions : "),
                  160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array($nb_add,
                  30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array(_("Radiations : "),
                  160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array($nb_rad,
                  30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array(sprintf(_("Nombre d'inscrits au %s : "), $f->formatDate($datetableau)),
                  160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array($nb_newelec,
                  30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array(sprintf(_("Arrete le tableau du %s %s au nombre de %s electeurs."), $f->formatDate($datetableau), ($nobureau != "ALL" ? _(" du bureau no").$nobureau : ""), $nb_newelec),
                  260, 20, 0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array("",
                  260, 25, 2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        );
    } elseif ($mode_edition == "commission" && $commission == "entre_deux_dates"
        || ($mode_edition == "tableau" && $tableau == "tableau_des_cinq_jours")
    ) {
        //// L'édition pour la commission entre deux dates et le tableau rectificatif des cinq jours
        //// ont la même page de garde
        //
        $nb_additions = $f->db->getone($query_nb_additions);
        $f->addToLog("pdf/commission.php: db->getone(\"".$query_nb_additions."\")", VERBOSE_MODE);
        $f->isDatabaseError($nb_additions);
        //
        $nb_radiations = $f->db->getone($query_nb_radiations);
        $f->addToLog("pdf/commission.php: db->getone(\"".$query_nb_radiations."\")", VERBOSE_MODE);
        $f->isDatabaseError($nb_radiations);
        //
        $pagedebut = array(
            //
            array("",
                  160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array("",
                  30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array(_("Additions : "),
                  160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array($nb_additions,
                  30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array(_("Radiations : "),
                  160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array($nb_radiations,
                  30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array("",
                  260, 35, 2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        );

    } elseif ($mode_edition == "tableau" && $tableau == "tableau_des_additions_des_jeunes") {
        //
        $nb_additions = $f->db->getone($query_nb_additions);
        $f->addToLog("pdf/commission.php: db->getone(\"".$query_nb_additions."\")", VERBOSE_MODE);
        $f->isDatabaseError($nb_additions);
        //
        $pagedebut = array(
            //
            array("",
                  160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array("",
                  30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array(_("Additions : "),
                  160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array($nb_additions,
                  30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array("",
                  260, 45, 2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        );
    }

    /**
     * COMPOSITION DU FICHIER PDF
     *
     * $mouvement_categories
     * $titre_libelle_ligne1
     * $titre_libelle_ligne2
     * $libelle_commune
     * $libelle_canton
     * $libelle_bureau
     * $libelle_liste
     * $nobureau
     * $pagefin_libelle
     * $pagedebut
     * 
     */
    // PARAM
    $param = array(9, 9, 10, 1, 1, 7, 7, 5, 10, 1, 1);
    // TITRE
    $titre_height = 4;
    $titre_border = "0";
    // ENTETE DES COLONNES
    $entete_height = 8;
    // PAGEFIN
    $pagefin_height = 9;
    $pagefin_border = "T";
    // DATAS
    $datas_height = 20;
    //
    // TITRE
    $titre = array(
        //
        array($titre_libelle_ligne1,
              185, $titre_height, 0, $titre_border,'L','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
        //
        array("<PAGE>",
              100, $titre_height, 1, $titre_border,'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        //
        array($titre_libelle_ligne2,
              285, $titre_height, 1, $titre_border,'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        //
        array($libelle_liste,
              285, $titre_height, 1, $titre_border,'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        //
        array(sprintf(_("Commune : %s"), $libelle_commune),
              145, $titre_height, 0, $titre_border,'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        //
        array(($libelle_canton != "" ? sprintf(_("Canton : %s"), $libelle_canton) : ""),
              140, $titre_height, 1, $titre_border,'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        //
        array(($nobureau != "ALL" ? sprintf(_("Bureau : %s - %s"), $nobureau, $libelle_bureau) : ""),
              185, $titre_height, 0, $titre_border,'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
    );
    // FIRSTPAGE
    $firstpage_border = "0";
    $firstpage = array_merge(
        array(
            //
            array(" ",
                  285, 1, 1, "B", 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            //
            array(" ",
                  285, 1, 1, "T", 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            //
            array($titre_libelle_ligne1,
                  285, 10, 1, "0", 'C','0','0','0','255','255','255',array(0,0),0,'','B',18,1,0),
            //
            array($titre_libelle_ligne2,
                  285, 6, 1, "0", 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array("",
                  285, 5, 1, "0", 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            //
            array(sprintf(_("Commune : %s"), $libelle_commune),
                  285, 6, 1, "0", 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
            //
            array(($nobureau != "ALL" ? sprintf(_("Bureau : %s - %s"), $nobureau, $libelle_bureau) : ""),
                  285, 6, 1, "0", 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
            //
            array("",
                  285, 16, 1, "0", 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
        ),
        //
        $pagedebut,
        //
        array(
            //
            array("",
                  285, $pagefin_height, 1, "", 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            //
            array(" ",
                  285, 1, 1, "B", 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            //
            array(" ",
                  285, 1, 1, "T", 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            //
            array($libelle_commune.' le '.date("d/m/Y"),
                  230, $pagefin_height+5, 1, "0", 'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            //
            array(_("Le Maire"),
                  95, $pagefin_height, 0, $firstpage_border, 'C','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
            //
            array(_("Le delegue du prefet"),
                  95, $pagefin_height, 0, $firstpage_border, 'C','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
            //
            array(_("Le delegue du TGI"),
                  95, $pagefin_height, 0, $firstpage_border, 'C','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
        )
    );
    $entete = array();
    //aucune donnee
    $datas = array("test" => array(
                        90, $datas_height-16, 2, '0', 'L', '0', '0', '0', '255', '255', '255', array(0,0),     ' ',
                        '', 'CELLSUP_NON', 0, 'NA','NC','','B',0,array('0')),  );
    $enpied = array();
    // AFFICHAGE DE LA PREMIERE PAGE
    $pdf->Table(
        $titre, $entete, $datas, $enpied, 
        "select ' ' as test from collectivite where id='00000'", 
        $f->db, $param, $firstpage
    );
    // XXX mettre une variable pour configurer ça
    if ($globale == false && $nobureau == "ALL" 
        && $mode_edition != "commission"
    ) {
        continue;
    }
    // BOUCLE SUR CHAQUE CATEGORIE DE MOUVEMENTS
    foreach ($mouvement_categories as $mouvement_categorie) {
        // TITRE
        $titre = array(
            //
            array($titre_libelle_ligne1,
                  185, $titre_height, 0, $titre_border,'L','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
            //
            array("<PAGE>",
                  100, $titre_height, 1, $titre_border,'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            //
            array($titre_libelle_ligne2,
                  285, $titre_height, 1, $titre_border,'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            //
            array($libelle_liste,
                  285, $titre_height, 1, $titre_border,'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            //
            array(sprintf(_("Commune : %s"), $libelle_commune),
                  145, $titre_height, 0, $titre_border,'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            //
            array(($libelle_canton != "" ? sprintf(_("Canton : %s"), $libelle_canton) : ""),
                  140, $titre_height, 1, $titre_border,'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            ////
            array(($nobureau != "ALL" ? sprintf(_("Bureau : %s- %s"), $nobureau, $libelle_bureau) : ""),
                  185, $titre_height, 0, $titre_border,'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            //
            array(_("Famille de Changements : ").($mouvement_categorie == "Inscription" ? _("Addition") : $mouvement_categorie),
                  100, $titre_height, 1, $titre_border,'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0)
        );
        // ENTETE
        $entete = array(
            //
            array(_("Nom patronymique, Prenoms, Nom d'usage,"),
                  90, $entete_height/2, 2,'LRT','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            //
            array(_("Date et lieu de naissance"),
                  90, $entete_height/2, 0,'LRB','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            //
            array(_('Adresse'),
                  90, $entete_height, 0, '1','C','0','0','0','255','255','255',array(0,4),0,'','NB',0,1,0),
            //
            array('<VCELL>',
                  15, $entete_height, 0, '1','C','0','0','0','255','255','255',array(0,0),3,array('Bureau,','No ordre'),'NB',0,1,0),
            //
            array(_('Observations'),
                  90, $entete_height, 0, '1','C','0','0','0','255','255','255',array(0,8),0,'','NB',0,1,0)
        );
        // PAGEFIN
        $pagefin = array(
            //
            array($pagefin_libelle,
                  200, $pagefin_height, 0, $pagefin_border, 'R','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
            //
            array('<LIGNE>',
                  20, $pagefin_height, 0, $pagefin_border, 'R','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
            //
            array(strtoupper(($mouvement_categorie == "Inscription" ? _("Addition") : $mouvement_categorie)).'(s)',
                  65, $pagefin_height, 1, $pagefin_border, 'L','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
        );
        // INCLUDE
        include "../sql/".OM_DB_PHPTYPE."/pdf_commission.inc";
        // DATAS
        $datas = array(
            // COLONNE 1
            'nom' => array(
                        90, $datas_height-16, 2, 'LRT', 'L', '0', '0', '0', '255', '255', '255', array(0,0),     ' ',
                        '', 'CELLSUP_NON', 0, 'NA','NC','','B',0,array('0')),        
            'prenom' => array(
                        90, $datas_height-16, 2,  'LR', 'L', '0', '0', '0', '255', '255', '255', array(0,0), '     ',
                        '', 'CELLSUP_NON', 0, 'NA','NC','','NB',0,array('0')),
            'nom_usage' => array(
                        90, $datas_height-16, 2,  'LR', 'L', '0', '0', '0', '255', '255', '255', array(0,0), '       -    ',
                        '', 'CELLSUP_NON', 0, 'NA','CONDITION',array('NN'),'NB',0,array('0')),
            'naissance' => array(
                        90, $datas_height-16, 2,  'LR', 'L', '0', '0', '0', '255', '255', '255', array(0,0), _('     Ne(e) le '),
                        '', 'CELLSUP_NON', 0, 'NA','NC','','NB',0,array('0')),
            'lieu' => array(
                        90, $datas_height-16, 0, 'LRB', 'L', '0', '0', '0', '255', '255', '255', array(0,0), _('     a  '),
                        '', 'CELLSUP_NON', 0, 'NA','NC','','NB',0,array('0')),
            // COLONNE 2
            'adresse' => array(
                        90, $datas_height-16, 2, 'LRT', 'L', '0', '0', '0', '255', '255', '255', array(0,16), '',
                        '', 'CELLSUP_NON', 0, 'NA','NC','','NB',0,array('0')),
            'complement' => array(
                        90, $datas_height-16, 2,  'LR', 'L', '0', '0', '0', '255', '255', '255', array(0,0), '',
                        '', 'CELLSUP_VIDE', array(
                        90, $datas_height- 8, 0, 'LRB', 'C', '0', '0', '0', '255', '255', '255', array(0,0)),
                                              'NA','NC','','NB',0,array('0')),
            // COLONNE 3
            'bureau' => array(
                        15, $datas_height-16, 2, 'LRT', 'C', '0', '0', '0', '255', '255', '255', array(0,8), '',
                        '', 'CELLSUP_NON', 0, 'NA','NC','','B',0,array('0')),
            'numero_bureau' => array(
                        15, $datas_height-16, 2,  'LR', 'C', '0', '0', '0', '255', '255', '255', array(0,0), '',
                        '', 'CELLSUP_VIDE', array(
                        15, $datas_height- 8, 0, 'LRB', 'C', '0', '0', '0', '255', '255', '255', array(0,0)),
                                              'NA','NC','','NB',0,array('0')),
            // COLONNE 4
            'libmvt' => array(
                        90, $datas_height-16, 2, 'LTR', 'L', '0', '0', '0', '255', '255', '255', array(0,8), '',
                        '', 'CELLSUP_NON', 0, 'NA','NC','','NB',0,array('0')),
            'observations' => array(
                        90, $datas_height-16, 2,  'LR', 'L', '0', '0', '0', '255', '255', '255', array(0,0), '',
                        '', 'CELLSUP_VIDE', array(
                        90, $datas_height- 8, 0, 'LRB', 'C', '0', '0', '0', '255', '255', '255', array(0,0)),
                                              'NA','CONDITION',array('NN'),'NB',0,array('0'))
        );
        // ENPIED
        $enpied = array();
        // AFFICHAGE DU TABLEAU DE MOUVEMENTS
        $pdf->Table(
            $titre, $entete, $datas, $enpied, $sql, $f->db, $param, $pagefin
        );
    }

}

/**
 * GENERATION DU FICHIER PDF
 */
// Nom du fichier
$filename = "";
if (isset($_GET["filename_with_date"]) && $_GET["filename_with_date"] == "false") {
    $filename .= "";
} else {
    $filename .= date("Ymd-His")."-";
}
$filename .= $filename_objet;
$filename .= "-".$_SESSION['collectivite'];
$filename .= "-".$f->normalizeString($_SESSION['libelle_collectivite']);
if (isset($filename_more)) {
    $filename .= $filename_more;
}
$filename .= ".pdf";
// Definition du mode de sortie
$mode = (isset($_GET["mode"]) ? $_GET["mode"] : "inline");
// Sortie du pdf
if ($mode == "chaine") {
    // S : renvoyer le document sous forme de chaine
    $filename = $f->getParameter("pdfdir").$filename;
    $contenu = $pdf->Output($filename, "S");
} elseif ($mode == "save") {
    // F : sauver dans un fichier local
    $filename = $f->getParameter("pdfdir").$filename;
    $pdf->Output($filename, "F");
    if (isset($_GET["redirect"])) {
        @header("Location:".$_GET["redirect"]);
    }
} elseif ($mode == "download") {
    // D : envoyer au navigateur en forcant le telechargement
    header('Cache-Control: private, max-age=0, must-revalidate');
    $pdf->Output($filename, "D");
} else {
    // I : envoyer en inline au navigateur
    header('Cache-Control: private, max-age=0, must-revalidate');
    $pdf->Output($filename, "I");
}
// Destruction de l'objet PDF
$pdf->Close();

?>

<?php
/**
 *
 *
 * @package openmairie_exemple
 * @version SVN : $Id: pdf.php 72 2010-09-01 17:52:25Z fmichon $
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");

/**
 *
 */
// Nom de l'objet metier
(isset($_GET['obj']) ? $obj = $_GET['obj'] : $obj = "");
//
$multiplicateur = 1;
//
$nolibliste = $_SESSION['liste']." ".strtolower ($_SESSION['libelle_liste']);
$dateTableau = $f -> collectivite ['datetableau'];
if ($f -> formatdate == "AAAA-MM-JJ") {
	$temp = $dateTableau;
	$dateTableauFr = substr($temp,8,2)."/".substr($temp,5,2)."/".substr($temp,0,4);
}
$ville = $f -> collectivite ['ville'];

/**
 * Verification des parametres
 */
if (strpos($obj, "/") !== false
    or !file_exists("../sql/".$f->phptype."/".$obj.".pdf.inc")) {
    $class = "error";
    $message = _("L'objet est invalide.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}

/**
 *
 */
$f->setRight($obj."_pdf");
$f->isAuthorized();

/**
 *
 */
require_once "../sql/".$f->phptype."/".$obj.".pdf.inc";

/**
 *
 */
//
set_time_limit(180);
//
require_once PATH_OPENMAIRIE."db_fpdf.php";
//
$pdf = new PDF($orientation, 'mm', $format);
$pdf->Open();
$pdf->SetMargins($margeleft, $margetop, $margeright);
$pdf->AliasNbPages();
$pdf->SetDisplayMode('real', 'single');
$pdf->SetDrawColor($C1border, $C2border, $C3border);
$pdf->AddPage();
$pdf->Table($sql, $f->db, $height, $border, $align, $fond, $police, $size,
            $multiplicateur, $flag_entete);
//
$filename = str_replace(" ", "-", strtolower($f->collectivite['ville'])."-".$obj.".pdf");
//
$pdf->Output ($filename, "I");

?>
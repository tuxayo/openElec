<?php
/*
L'unité est le millimetre pour les coordonnées
*/
$param=array(0,0,0,-1,2,2,148,105,3,3.5,0,0,10,0,0);
$texte=array(array($f -> collectivite ['maire'],33,90,40,0,9),
             array($f -> collectivite ['ville'],79,90,30,0,9)//,
             //array('POUR VOTER SE MUNIR OBLIGATOIREMENT D\'UNE PIÈCE D\'IDENTITÉ.',5,98,115,1,8)
);
$champs=array('code_bureau'=>array('','',array(140,48,8,1,0),1),              
              'canton'=>array('','',array(25,5,114,0,9),0), // 'CANTON' ne doit pas etre ecrit s'il n'y en a pas, le libelle doit etre dans la requete
              'numero_bureau'=>array('','',array(8,48,15,1,0),0),
              'id_electeur'=>array('','',array(800,800,0,0,0),0),
              'libur'=>array('','',array(25,8,114,0,9),0),
              'adr1'=>array('','',array(25,11,114,0,9),0),
              'adr2'=>array('','',array(25,14,114,0,9),0),
              'adr3'=>array('','',array(25,17,114,0,9),0),
              'nomprenom'=>array('','',array(25,40,114,1,10),0),
              'adresse'=>array('','',array(25,48,114,1,10),0),
              'complement'=>array('','',array(25,52,114,1,10),0),
              'voiecp'=>array('','',array(25,56,15,1,10),0),
              'voieville'=>array('','',array(40,56,99,1,10),0),
              'naissance'=>array('','',array(3,71,28,1,9),0),
              'code_departement_naissance'=>array('','',array(40,71,15,1,9),0),
              'libelle_lieu_de_naissance'=>array('','',array(60,71,85,1,9),0),
              'nationalite'=>array('','',array(60,74,85,1,9),0),
);
?>
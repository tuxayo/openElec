<?php
/**
 *
 *
 * @package openmairie_exemple
 * @version SVN : $Id: pdfetat.php 72 2010-09-01 17:52:25Z fmichon $
 */

require_once "../obj/utils.class.php";
(!isset($f) ? $f = new utils("nohtml") : "");

/**
 *
 */
// Nom de l'objet metier
(isset($_GET['obj']) ? $obj = $_GET['obj'] : $obj = "");
// Liste des identifiants
(isset($_GET['idx']) ? $idxlist = explode(';',$_GET['idx']) : $idxlist = "");
// Récupération du paramètre complement
(isset($_GET['complement']) ? $complement = $_GET['complement'] : $complement = "");
// Récupération du flag permettant de cacher le footer
(isset($_GET['footerdisplay']) ? $footerdisplay = $_GET['footerdisplay'] : $footerdisplay = true);


//
$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];
//
$ville = $f -> collectivite ['ville'];
$dateTableau = substr($f -> collectivite ['datetableau'],8,2).'/'.substr($f -> collectivite ['datetableau'],5,2).'/'.substr($f -> collectivite ['datetableau'],0,4);
$anneeTableau = substr($f -> collectivite ['datetableau'],0,4);
$nom = $f -> collectivite ['maire'];

/**
 * Verification des parametres
 */
if (strpos($obj, "/") !== false
    or !file_exists("../sql/".$f->phptype."/".$obj.".etat.inc")) {
    $class = "error";
    $message = _("L'objet est invalide.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}

/**
 *
 */
$f->setRight($obj);
$f->isAuthorized();

/**
 *
 */
require_once "../sql/".$f->phptype."/".$obj.".etat.inc";

/**
 *
 */
//
set_time_limit(180);
//
require_once PATH_OPENMAIRIE."fpdf_etat.php";
//
$unite="mm";
$pdf=new PDF($etat["orientation"],$unite,$etat["format"]);
//
$pdf->footerfont=$etat["footerfont"];
$pdf->footertaille=$etat["footertaille"];
$pdf->footerdisplay=$footerdisplay;
$pdf->footerattribut=$etat["footerattribut"];
$pdf->SetMargins($etat['se_margeleft'],$etat['se_margetop'],$etat['se_margeright']); //marge gauche,haut,droite par defaut 10mm
$pdf->SetDisplayMode('real','single');
// methode fpdf calcul nombre de page
$pdf->AliasNbPages();
$image="../trs/".$etat['logo'];

// Modification de l'encodage des caractères des états
// vers l'encodage défini dans dyn/locales.inc
// Liste des encodage pour la finction mb_detect_encoding
$encodage = array("UTF-8", "ASCII", "Windows-1252", "ISO-8859-15", "ISO-8859-1");

// Pour chaque idx passé en paramètre on génère un état
foreach ($idxlist as $idx) {
    // Réinitialisation de l'état pour chaque élément.
    $sql= iconv(mb_detect_encoding($etat["sql"], $encodage), HTTPCHARSET, $etat["sql"]);
    $titre= iconv(mb_detect_encoding($etat["titre"], $encodage), HTTPCHARSET, $etat["titre"]);
    $corps= iconv(mb_detect_encoding($etat["corps"], $encodage), HTTPCHARSET, $etat["corps"]);

    $_GET['idx'] = $idx;
    // methode de creation de page
    $pdf->AddPage();

    // LOGO
    // Récupération du path vers l'image
    $path_logo = $image;
    // Placement d'une image uniquement si l'image existe
    if (file_exists($path_logo) && !is_dir($path_logo)) {
        // On vérifie si on peut récupérer la résolution de l'image
        // "nomdufichier_dpi300.png"
        $resolution = NULL;
        $image_filename = $etat['logo'];
        if (preg_match('/_dpi([0-9]+)/', $image_filename, $arr)) {
            $resolution = $arr[1];
        }
        // Si on a une résolution on affiche l'image de manière correcte
        if ($resolution != NULL) {
            // récupération de la taille de l'image en pixels
            $size = getimagesize($path_logo);
            // résolution explicite
            $pdf->Image($path_logo,
                        $etat["logoleft"],
                        $etat["logotop"],
                        ($size[0]/($resolution/25.4)),
                        ($size[1]/($resolution/25.4)));
        } else {
            // aucune dimension explicite
            $pdf->Image($path_logo,
                        $etat["logoleft"],
                        $etat["logotop"],
                        0,
                        0);
        }
    }

    include("../dyn/varetatpdf.inc");
    $res = $f->db->query($sql);
    $f->isDatabaseError($res);
    while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC))
    {
        // titre
        $temp = explode("[",$etat["titre"]);
        for($i=1;$i<sizeof($temp);$i++)
        {
            $temp1 = explode("]",$temp[$i]);
            $titre=str_replace("[".$temp1[0]."]",$row[$temp1[0]],$titre);
            $temp1[0]="";
        }
        $pdf->SetFont($etat["titrefont"],$etat["titreattribut"],$etat["titretaille"]);
        $pdf->SetXY($etat["titreleft"],$etat["titretop"]);
        $pdf->MultiCell($etat["titrelargeur"],$etat["titrehauteur"],iconv(HTTPCHARSET,"CP1252",$titre),$etat["titrebordure"],$etat["titrealign"],0);
        // corps
        $temp = explode("[",$etat["corps"]);
        for($i=1;$i<sizeof($temp);$i++)
        {
            $temp1 = explode("]",$temp[$i]);
            $corps=str_replace("[".$temp1[0]."]",$row[$temp1[0]],$corps);
            $temp1[0]="";
        }
        $pdf->SetFont($etat["corpsfont"],$etat["corpsattribut"],$etat["corpstaille"]);
        $pdf->SetXY($etat["corpsleft"],$etat["corpstop"]);
        $pdf->MultiCell($etat["corpslargeur"],$etat["corpshauteur"] ,iconv(HTTPCHARSET,"CP1252",$corps),$etat["corpsbordure"],$etat["corpsalign"],0);
    }
    // affichage des sous etats
    if($etat['sousetat']!="") 
    {
        foreach($etat['sousetat'] as $elem)
        {
            include ("../sql/".$f -> phptype."/".$elem.".sousetat.inc");
            // =========================================================================
            // traitementde variables : &
            $sql=$sousetat['sql'];
            $titre=$sousetat['titre'];
            include("../dyn/varetatpdf.inc");
            $sousetat['sql']=$sql;
            $sousetat['titre']=$titre;
            //imprime  les colonnes de la requête
            $pdf->sousetat($db,$etat,$sousetat);
        }
    }

}
$filename = str_replace(" ", "-", strtolower($f->collectivite['ville'])."-".$obj."-".$idx.".pdf");
$pdf->Output ($filename, "I");

?> 

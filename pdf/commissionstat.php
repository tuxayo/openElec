<?php
/**
 * Ce fichier permet de realiser l'edition statistique pour la commission, il
 * recapitule le nombre de mouvements par type et par categorie
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"edition");

//
define('FPDF_FONTPATH','font/');
require_once("fpdf.php");
set_time_limit (480);
$aujourdhui = date("d/m/Y");


/**
 * Edition multi/mono collectivite
 */ 
$multi = $f->isMulti();
$collectivite_list = array();

if ( $multi ) {
    // Si multi-collectivité recupere toutes les collectivites
    $sql = "SELECT id,ville FROM collectivite  WHERE id<>'".$_SESSION['collectivite']."' order by ville";
    $res =& $f -> db->query($sql);
        if (database::isError($res))
            die($res->getMessage());
    while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC))
        array_push( $collectivite_list , array($row['id'], $row['ville']) );
} else {
    // Si mono-collectivité recupere seulement la collectivite en session
    array_push( $collectivite_list , array($_SESSION['collectivite'], $f->collectivite['ville'] ) );
}

//
$pdf=new FPDF('P','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(true);
$pdf->SetFont('courier','',11);
$pdf->SetDrawColor(30,7,146);
$pdf->SetMargins(5,5,5);
$pdf->SetDisplayMode('real','single');

foreach ($collectivite_list as $arrayMairie) {
    // Variable collectivite (id et nom de la ville)
    $mairie = $arrayMairie[0];
    $ville = $arrayMairie[1];
    $nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];
    $typecat=array();
    $typecat=array(
        array("Inscription",array('','',''),0,'CATEGORIE',1,"+"),
        array("TransfertInscription",array('','',''),0,3,1,"+"),
        array("Modification",array('','',''),0,'CATEGORIE',1,"0"),
        array("Transfert",array('','',''),0,3,1,"0"),
        array("Radiation",array('','',''),0,'CATEGORIE',1,"-"),
        array("TransfertRadiation",array('','',''),0,3,1,"-")
    );
    //****************************************************************************
    $nb_typecat = COUNT ($typecat);
    //
    for ($k=0; $k<$nb_typecat; $k++) {
        if (strtolower($typecat[$k][3])=='categorie') {
            $sql = "SELECT code, libelle  FROM param_mouvement  WHERE typecat='".$typecat[$k][0]."' order by libelle";
            $res =& $f -> db->query($sql);
            if (database::isError($res))
                die($res->getMessage());
            $nb_code = 0;
            $cpt = 0;
            $nb_code = $res->numrows();
            while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                $cpt++;
                if ($cpt==1) {
                    $typecat[$k][1][0] = $typecat[$k][1][0].$row['code'];
                    $typecat[$k][1][1] = $typecat[$k][1][1].$row['libelle'];
                } else {
                    if ($cpt <$nb_code || $cpt==$nb_code) {
                        $typecat[$k][1][0] = $typecat[$k][1][0]."#".$row['code'];
                        $typecat[$k][1][1] = $typecat[$k][1][1]."#".$row['libelle'];
                    }
                }
            }
        }
    }
    
    for ($x=0; $x<$nb_typecat; $x++) {
        if (strtolower($typecat[$x][3])!='categorie') {
            $typecat[$x][1][0] = $typecat[$typecat[$x][3]][1][0];
            $typecat[$x][1][1] = $typecat[$typecat[$x][3]][1][1];
        }
    }
    
    //
    include "../sql/".$f -> phptype."/pdf_commissionstat.inc";
    
    // inscription j-5 
    $nb_insj5 = 0;
    $resinsj5 = $f -> db->query ($sqlinsj5);
    //
    if (database::isError($resinsj5))
        die($resinsj5->getMessage());
    $rowinsj5=& $resinsj5->fetchRow(DB_FETCHMODE_ASSOC);
    $nb_insj5=$rowinsj5['nbrinsj5'];
    
    // radiation j-5
    $nb_radj5 = 0;
    $resradj5 = $f -> db->query ($sqlradj5);
    //
    if (database::isError($resradj5))
        die($resradj5->getMessage());
    $rowradj5=& $resradj5->fetchRow(DB_FETCHMODE_ASSOC);
    $nb_radj5 = $rowradj5['nbradj5'];
    
    // nbr electeur table electeur
    $nb_electeur = 0;
    $nb_electeur_ir = 0;
    $sqlelec = "select count(*) as nbrelecteur from electeur where collectivite='".
        $mairie."' and liste='".$_SESSION['liste']."'";
    $reselec = $f -> db->query($sqlelec);
    //
    if (database::isError($reselec))
        die($reselec->getMessage());
    $rowelec =& $reselec->fetchRow(DB_FETCHMODE_ASSOC);
    $nb_electeur=$rowelec['nbrelecteur'];

    // Recuperation du nombre d'additions traitees aux dates de tableau superieures
    // ou egales a la date de tableau actuelle
    $res_nb_addition_dtsup = $f->db->getOne($nb_addition_dtsup);
    if (DB::isError($res_nb_addition_dtsup))
        die ($res_nb_addition_dtsup->getMessage() ." erreur ". $nb_addition_dtsup);
    // Recuperation du nombre de radiations traitees aux dates de tableau
    // superieures ou egales a la date de tableau actuelle 
    $res_nb_radiation_dtsup = $f->db->getOne($nb_radiation_dtsup);
    if (DB::isError($res_nb_radiation_dtsup))
        die ($res_nb_radiation_dtsup->getMessage() ." erreur ". $nb_radiation_dtsup);
    
    /**
     * Calcul du nombre d'electeur a la date de tableau precedente et a la date de
     * tableau suivante
     */
    // Au tableau precedent le nombre d'electeurs est egal au nombre d'electeurs
    // dans la table electeur a l'heure actuelle auquel on enleve toutes les
    // additions traitees aux dates de tableau superieures ou egales a la date
    // de tableau actuelle et auquel on ajoute toutes les radiations traitees
    // aux dates de tableau superieures ou egales a la date de tableau actuelle 
    $nb_electeur_ir = $nb_electeur - $res_nb_addition_dtsup + $res_nb_radiation_dtsup;

    //
    // table mouvement========================================================
    // totaux par sous categorie (detail)                                   //
    // =======================================================================
    //
    for ($j=0; $j<$nb_typecat; $j++) {
        //
        $codes_mouvement = array();
        $libelles_mouvement = array();
        $nb_codes_mouvement = 0;
        $nb_libelles_mouvement = 0;
        $codes_mouvement = explode("#",$typecat[$j][1][0]);
        $libelles_mouvement = explode("#",$typecat[$j][1][1]);
        if (isset($codes_mouvement)) {
            $nb_codes_mouvement=count($codes_mouvement);
        }
        if ($nb_codes_mouvement>0) {
            $cpt=0;
            for ($y=0; $y<$nb_codes_mouvement; $y++) {
                //
                $sql = "";
                $sql .= "SELECT ";
                $sql .= "  count(*)";
                $sql .= "  FROM mouvement";
                $sql .= "  WHERE collectivite='".$mairie."' and date_tableau='".$f -> collectivite ['datetableau']."'";
                if (strtolower($typecat[$j][0])=="radiation" || strtolower($typecat[$j][0])=="inscription" || strtolower($typecat[$j][0])=="modification") {
                    // debut type categorie mouvement
                    $sql .= "  AND types ='".$codes_mouvement[$y]."' and liste='".$_SESSION['liste']."' ";
                } else {
                    if (strtolower($typecat[$j][0])=="transfertinscription") {
                        $sql .= "  AND types = '".$codes_mouvement[$y]."' AND liste='".$_SESSION['liste']."' ";//inscription  transfert
                    } else {
                        if (strtolower($typecat[$j][0])=="transfertradiation") {
                            $sql .= "  AND types = '".$codes_mouvement[$y]."'  AND liste='".$_SESSION['liste']."'"; // radiation transfert
                        }
                    }
                }
                $nb_ligne = 0;
                $nb_ligne = $f -> db->getOne($sql);
                //
                $cpt++;
                if ($cpt==1) {
                    $typecat[$j][1][2] = $nb_ligne;
                } else {
                    $typecat[$j][1][2] = $typecat[$j][1][2]."#".$nb_ligne;
                }
            }
        }
    }
    //
    $pdf->SetFont('courier','',11);
    $pdf->SetDrawColor(30,7,146);
    $pdf->addpage();
    // entete
    $pdf->Cell(120,7,iconv(HTTPCHARSET,"CP1252",sprintf(_(' Gestion des Elections  -  Mairie de %s'),$ville)),'0',0,'L',0);
    $pdf->Cell(80,7,iconv(HTTPCHARSET,"CP1252",_('Page  :  ').$pdf->PageNo()." "),'0',1,'R',0);
    $pdf->Cell(200,7,iconv(HTTPCHARSET,"CP1252",sprintf(_(' STATISTIQUES GENERALES DES MOUVEMENTS AU %s'),substr($f -> collectivite ['datetableau'],8,2).'/'.substr($f -> collectivite ['datetableau'],5,2).'/'.substr($f -> collectivite ['datetableau'],0,4))),'0',1,'L',0);
    $pdf->Cell(120,7,iconv(HTTPCHARSET,"CP1252",' '.$nolibliste),'0',0,'L',0);
    $pdf->Cell(80,7,iconv(HTTPCHARSET,"CP1252",sprintf(_('Edite le %s'),$aujourdhui)),'0',1,'R',0);
    $pdf->Ln(2);
    // ligne debut
    $pdf->Cell(200,10,'','0',1,'L',0);
    //ligne
    $pdf->Cell(25,10,'','0',0,'R',0);
    $pdf->SetFillColor(210,216,249);
    $pdf->Cell(130,10,iconv(HTTPCHARSET,"CP1252",mb_strtoupper(_('Nombre d\'electeurs au dernier tableau '),'UTF-8')),'LTB',0,'L',1);
    $pdf->Cell(20,10,iconv(HTTPCHARSET,"CP1252",$nb_electeur_ir.' '),'TRB',0,'R',1);
    //verification
    $pdf->Cell(25,10,'','0',1,'L',0);
    //$pdf->Cell(85,10,'( E->'.$nb_electeur. ' I-> '.$nb_insj5.' R-> '.$nb_radj5.')','0',1,'L',0);
    //
    //ligne
    $plus = 0;
    $moins = 0;
    $mod = 0;
    for ($i=0; $i<$nb_typecat; $i++) {
        //ligne titre categorie
        if (strtolower($typecat[$i][3])=='categorie' && ($typecat[$i][4])==1) {
            //ligne vide
            $pdf->Cell(200,4,'','0',1,'L',0);
            //
            $pdf->Cell(25,7,'','0',0,'R',0);
            $pdf->Cell(150,7,' '.iconv(HTTPCHARSET,"CP1252",mb_strtoupper($typecat[$i][0])),'0',0,'L',0);
            $pdf->Cell(25,7,'','0',1,'L',0);
        }
        //
        if ($typecat[$i][4]==1) {
            // flag affichage = 1
            $code_mvt = array();
            $libelles_mvt = array();
            $total_mvt = array();
            $nb_mvt = 0;
            $code_mvt = explode("#", $typecat[$i][1][0]);
            $libelle_mvt = explode("#", $typecat[$i][1][1]);
            $total_mvt = explode("#", $typecat[$i][1][2]);
            if (isset($total_mvt)) {
                $nb_mvt=count($code_mvt);
            }
            //*
            $w = 0;
            $tot = 0;
            /*echo "<pre>";
            print_r ($libelle_mvt);
            echo "</pre>";*/
            for ($w=0; $w<$nb_mvt; $w++) {
                // pour test $pdf->Cell(50,5,$typecat[$i][1][2],'1',0,'L',0);
                $pdf->Cell(25,5,'','0',0,'L',0);
                //
                // pour test  $pdf->Cell(130,5,' '.$code_mvt[$w].' '.$libelle_mvt[$w],'1',0,'L',0);
                if ($libelle_mvt[$w]=="") {
                    $pdf->SetFillColor(200,200,200);
                    $pdf->Cell(130,5,iconv(HTTPCHARSET,"CP1252",_(' TOTAL')),'1',0,'L',1);
                } else
                    $pdf->Cell(130,5,iconv(HTTPCHARSET,"CP1252",' '.$libelle_mvt[$w]),'1',0,'L',0);
                //
                if (isset($total_mvt[$w])) {
                    if ($libelle_mvt[$w]=="") {
                        $pdf->SetFillColor(200,200,200);
                        if ($typecat[$i][5]=='+')
                            $pdf->Cell(20,5,iconv(HTTPCHARSET,"CP1252",$plus).' ','1',0,'R',1);
                        if ($typecat[$i][5]=='-')
                            $pdf->Cell(20,5,iconv(HTTPCHARSET,"CP1252",$moins).' ','1',0,'R',1);
                        if ($typecat[$i][5]=='0')
                            $pdf->Cell(20,5,iconv(HTTPCHARSET,"CP1252",$mod).' ','1',0,'R',1);
                    } else
                        $pdf->Cell(20,5,iconv(HTTPCHARSET,"CP1252",$total_mvt[$w].' '),'1',0,'R',0);
                    // total electeur avec mouvements
                    if ($typecat[$i][5]=='+') {
                        $plus = $plus + $total_mvt[$w];
                    } else {
                        if ($typecat[$i][5]=='-') {
                            $moins = $moins + $total_mvt[$w];
                        } else {
                            if ($typecat[$i][5]=='0') {
                                $mod = $mod + $total_mvt[$w];
                            }
                        }
                    }
                } else {
                    $pdf->Cell(20,5,iconv(HTTPCHARSET,"CP1252",'-'.' '),'1',0,'R',0);
                }
                $pdf->Cell(25,5,'','0',1,'L',0);
            }
        }
    }
    //ligne vide
    $pdf->Cell(200,5,'','0',1,'L',0);
    //ligne  fin
    $pdf->Cell(25,10,'','0',0,'R',0);
    $pdf->SetFillColor(210,216,249);
    $pdf->Cell(130,10,iconv(HTTPCHARSET,"CP1252",mb_strtoupper(_(' Nombre d\'electeurs au tableau du '),'UTF-8').
        substr($f -> collectivite ['datetableau'],8,2).'/'.substr($f -> collectivite ['datetableau'],5,2).'/'.substr($f -> collectivite ['datetableau'],0,4)),'LTB',0,'L',1);
    //
    $nb_electeur_mvt = 0;
    $nb_electeur_mvt = ($nb_electeur_ir+$plus)-$moins;
    $pdf->Cell(20,10,iconv(HTTPCHARSET,"CP1252",$nb_electeur_mvt.' '),'TRB',0,'R',1);
    //verification
    $pdf->Cell(25,10,'','0',1,'L',0);
    //$pdf->Cell(85,10,'( E->'.$nb_electeur_ir. ' plus-> '.$plus.' moins-> '.$moins.')','0',1,'L',0);
    //
}
$aujourdhui = date('Ymd-His');
$pdf->Output("stats-generales-mouvements-".$aujourdhui.".pdf","I");
$pdf->Close();

?>

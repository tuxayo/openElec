<?php
/**
 * Ce script permet de générer 2 courriers pour un refus de procuration,
 * un pour le mandant et le 2nd pour le mandataire
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", "refusprocuration");

/**
 *
 */
// Identifiant du mandant
(isset($_GET['idx']) ? $idx = $_GET['idx'] : $idx = "");
// Récupération du paramètre complement
(isset($_GET['complement']) ? $complement = $_GET['complement'] : $complement = "");
//
$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];
//
$ville = $f -> collectivite ['ville'];
$dateTableau = substr($f -> collectivite ['datetableau'],8,2).'/'.
                substr($f -> collectivite ['datetableau'],5,2).'/'.
                substr($f -> collectivite ['datetableau'],0,4);
$anneeTableau = substr($f -> collectivite ['datetableau'],0,4);
$nom = $f -> collectivite ['maire'];

/**
 * Verification des états
 */
if (!file_exists("../sql/".$f->phptype."/refusprocurationmandant.etat.inc")) {
    $class = "error";
    $message = _("L'etat refusprocurationmandant n'existe pas.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}
if (!file_exists("../sql/".$f->phptype."/refusprocurationmandataire.etat.inc")) {
    $class = "error";
    $message = _("L'etat refusprocurationmandataire n'existe pas.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}


/**
 *
 */
//
set_time_limit(180);
//
require_once PATH_OPENMAIRIE."fpdf_etat.php";
/**
 *
 */

require_once "../sql/".$f->phptype."/refusprocurationmandataire.etat.inc";
//
$unite="mm";
$pdf=new PDF($etat["orientation"],$unite,$etat["format"]);
//
$pdf->footerfont=$etat["footerfont"];
$pdf->footertaille=$etat["footertaille"];
$pdf->footerdisplay=false;
$pdf->footerattribut=$etat["footerattribut"];
$pdf->SetMargins($etat['se_margeleft'],$etat['se_margetop'],$etat['se_margeright']); //marge gauche,haut,droite par defaut 10mm
$pdf->SetDisplayMode('real','single');
// methode fpdf calcul nombre de page
$pdf->AliasNbPages();
$image="../trs/".$etat['logo'];

// Modification de l'encodage des caractères des états
// vers l'encodage défini dans dyn/locales.inc
// Liste des encodage pour la finction mb_detect_encoding
$encodage = array("UTF-8", "ASCII", "Windows-1252", "ISO-8859-15", "ISO-8859-1");
// Réinitialisation de l'état pour chaque élément.
$sql= iconv(mb_detect_encoding($etat["sql"], $encodage), HTTPCHARSET, $etat["sql"]);
$titre= iconv(mb_detect_encoding($etat["titre"], $encodage), HTTPCHARSET, $etat["titre"]);
$corps= iconv(mb_detect_encoding($etat["corps"], $encodage), HTTPCHARSET, $etat["corps"]);
// pdf mandant
// methode de creation de page
$pdf->AddPage();
$pdf->Image($image,$etat["logoleft"],$etat["logotop"],0,0,'','');
include("../dyn/varetatpdf.inc");

$res = $f->db->query($sql);
$f->isDatabaseError($res);
while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC))
{
    // titre
    $temp = explode("[",$etat["titre"]);
    for($i=1;$i<sizeof($temp);$i++)
    {
        $temp1 = explode("]",$temp[$i]);
        $titre=str_replace("[".$temp1[0]."]",$row[$temp1[0]],$titre);
        $temp1[0]="";
    }
    $pdf->SetFont($etat["titrefont"],$etat["titreattribut"],$etat["titretaille"]);
    $pdf->SetXY($etat["titreleft"],$etat["titretop"]);
    $pdf->MultiCell($etat["titrelargeur"],$etat["titrehauteur"],iconv(HTTPCHARSET,"CP1252",$titre),$etat["titrebordure"],$etat["titrealign"],0);
    // corps
    $temp = explode("[",$etat["corps"]);
    for($i=1;$i<sizeof($temp);$i++)
    {
        $temp1 = explode("]",$temp[$i]);
        $corps=str_replace("[".$temp1[0]."]",$row[$temp1[0]],$corps);
        $temp1[0]="";
    }
    $pdf->SetFont($etat["corpsfont"],$etat["corpsattribut"],$etat["corpstaille"]);
    $pdf->SetXY($etat["corpsleft"],$etat["corpstop"]);
    $pdf->MultiCell($etat["corpslargeur"],$etat["corpshauteur"] ,iconv(HTTPCHARSET,"CP1252",$corps),$etat["corpsbordure"],$etat["corpsalign"],0);
}
$_GET['idx']=$idx;
/**
 *
 */
require_once "../sql/".$f->phptype."/refusprocurationmandant.etat.inc";

// pdf mandataire
$pdf->AddPage();
$pdf->Image($image,$etat["logoleft"],$etat["logotop"],0,0,'','');


// Réinitialisation de l'état pour chaque élément.
$sql= iconv(mb_detect_encoding($etat["sql"], $encodage), HTTPCHARSET, $etat["sql"]);
$titre= iconv(mb_detect_encoding($etat["titre"], $encodage), HTTPCHARSET, $etat["titre"]);
$corps= iconv(mb_detect_encoding($etat["corps"], $encodage), HTTPCHARSET, $etat["corps"]);

include("../dyn/varetatpdf.inc");
$res = $f->db->query($sql);
$f->isDatabaseError($res);
while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC))
{
    // titre
    $temp = explode("[",$etat["titre"]);
    for($i=1;$i<sizeof($temp);$i++)
    {
        $temp1 = explode("]",$temp[$i]);
        $titre=str_replace("[".$temp1[0]."]",$row[$temp1[0]],$titre);
        $temp1[0]="";
    }
    $pdf->SetFont($etat["titrefont"],$etat["titreattribut"],$etat["titretaille"]);
    $pdf->SetXY($etat["titreleft"],$etat["titretop"]);
    $pdf->MultiCell($etat["titrelargeur"],$etat["titrehauteur"],iconv(HTTPCHARSET,"CP1252",$titre),$etat["titrebordure"],$etat["titrealign"],0);
    // corps
    $temp = explode("[",$etat["corps"]);
    for($i=1;$i<sizeof($temp);$i++)
    {
        $temp1 = explode("]",$temp[$i]);
        $corps=str_replace("[".$temp1[0]."]",$row[$temp1[0]],$corps);
        $temp1[0]="";
    }
    $pdf->SetFont($etat["corpsfont"],$etat["corpsattribut"],$etat["corpstaille"]);
    $pdf->SetXY($etat["corpsleft"],$etat["corpstop"]);
    $pdf->MultiCell($etat["corpslargeur"],$etat["corpshauteur"] ,iconv(HTTPCHARSET,"CP1252",$corps),$etat["corpsbordure"],$etat["corpsalign"],0);
}

$filename = str_replace(" ", "-", strtolower($f->collectivite['ville'])."-refusprocuration-".$idx.".pdf");
$pdf->Output ($filename, "I");
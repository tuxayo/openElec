<?php
/**
 * Ce fichier permet de realiser l'edition de tous les electeurs ayant une
 * carte en retour
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"edition");

//
require('../app/fpdf_table.php');
set_time_limit (480);
$aujourdhui = date("d/m/Y");
$id='';
$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];
if (isset($_GET['id'])) 
{
	$id= $_GET['id'];
	if($id=="bureau")
		if (isset($_GET['idx'])) 
		{
			$nobureau=$_GET['idx'];
			$message=sprintf(_("LISTE DES CARTES RETOURNEES\nListe %s\nAucun enregistrement selectionne pour le bureau %s"),$nolibliste,$nobureau);
		}
}

$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(false);
$pdf->SetFont('Arial','',8);
$pdf->SetDrawColor(30,7,146);
$pdf->SetMargins(5,10,5);
$pdf->SetDisplayMode('real','single');

$param=array();
$param=array(8,8,8,1,1,10,10,10,10,0,0);

$ligne=0;
$total_ligne=0;
$pdf->AddPage();

include ("../sql/".$f -> phptype."/pdf_listecarteretour.inc");

$libelle_bureau='';
$libelle_canton='';
$resbur = $f->db->query($sqlbur);
if (database::isError($resbur))
	die($resbur->getMessage());
$rowbur=& $resbur->fetchRow(DB_FETCHMODE_ASSOC);
$libelle_bureau=$rowbur['libbur'];
$libelle_canton=$rowbur['libcanton'];

//------------------------titre------------------------------------------//
$heighttitre=4;
$titre=array();
$titre=array(
array(sprintf(_('Le %s'),$aujourdhui),250,$heighttitre,0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array('<PAGE>',20,$heighttitre,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('LISTE DES CARTES RETOURNEES  -   COMMUNE : %s'),$f -> collectivite ['ville']),150,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(' ',120,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Bureau No %s %s'),$nobureau, $libelle_bureau),120,$heighttitre,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Canton  %s'),$libelle_canton),50,$heighttitre,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Circonscription '),100,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Liste %s'), $nolibliste),165,$heighttitre,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(' ',120,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0)
);
//------------------------entete colonne   ------------------------------------------------------------------------//
$heightentete=8;
$entete=array();
$entete=array(
array(_('Nom Patronymique, Prenoms'),80,$heightentete-4,2,'LRT','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Nom d\'Usage,Date et lieu de naissance'),80,$heightentete-4,0,'LRB','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Adresse'),110,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,4),0,'','NB',0,1,0),
array('<VCELL>',12,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),3,array('No','Ordre'),'NB',0,1,0),
array(' ',80,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,8),0,'','NB',0,1,0)
);
//------------------------page fin ----------------------------------------------------------------------------------
$pagefin=array();
$pagefin=array(
array('<PAGE>',270,$heighttitre,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_(' *  *  *     A  R  R  E  T  E     *  *  *'),260,10,1,'0','C','0','0','0','255','255','255',array(0,0),0,'','B',10,1,0),
array(' ',60,10,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('La liste des cartes retournees du BUREAU No %s - %s'),$nobureau, $libelle_bureau),180,10,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(' ',60,10,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('au nombre de '),25,10,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array('<LIGNE>',20,10,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',9,1,0),
array(_(' electeur(s)'),25,10,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(' ',155,10,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('%s , le %s'),$f -> collectivite ['ville'],date("d/m/Y")),75,10,1,'0','L','0','0','0','255','255','255',array(50,0),0,'','NB',0,1,0),
array(' ',155,20,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Le Maire,') ,75,20,1,'0','L','0','0','0','255','255','255',array(50,0),0,'','NB',0,1,0),
array(' ',260,30,1,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Le President,'),96,10,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Le Secretaire, '),96,10,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Les  Assesseurs,'),96,10,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0)
);
//--------------------------------------------------------------------------//
$heightligne=15;
$col=array();
$col=array('nom'=>array(80,$heightligne-10,2,'LRT','L','0','0','0','255','255','255',array(0,0),' ','',
'CELLSUP_NON',0,'NA','NC','','B',8,array('0')),
'nom_usage'=>array(80,$heightligne-10,2,'LR','L','0','0','0','255','255','255',array(0,0),'       -    ','',
'CELLSUP_NON',0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
'naissance'=>array(31,$heightligne-10,0,'LB','L','0','0','0','255','255','255',array(0,0),_('     Ne(e) le '),'',
'CELLSUP_NON',0,'NA','NC','','NB',0,array('0')),
'lieu'=>array(49,$heightligne-10,0,'B','L','0','0','0','255','255','255',array(0,0),_(' a  '),'',
'CELLSUP_NON',0,'NA','NC','','NB',0,array('0')),
'adresse'=>array(110,$heightligne-10,2,'LRT','L','0','0','0','255','255','255',array(0,10),'','',
'CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
'complement'=>array(110,$heightligne-10,2,'LR','L','0','0','0','255','255','255',array(0,0),'','',
'CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
'procuration'=>array(110,$heightligne-10,0,'LRB','L','0','0','0','255','255','255',array(0,0),'','',
'CELLSUP_NON',0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
'numero_bureau'=>array(12,$heightligne,0,'1','C','0','0','0','255','255','255',array(0,10),'','',
'CELLSUP_VIDE',array(80,$heightligne,0,'1','C','0','0','0','255','255','255',array(0,0)),0,'NA','NC','','NB',0,array('0'))
);
//
$enpied=array();
$pdf->Table($titre,$entete,$col,$enpied,$sql,$f -> db,$param,$pagefin);
// message  aucun enregistrement--------------------------------------------
if ($pdf->msg==1 and $message!='')
{
	$pdf->SetFont('Arial','',10);
	$pdf->SetDrawColor(0,0,0);
	$pdf->SetFillColor(213,8,26);
	$pdf->SetTextColor(255,255,255);
	$pdf->MultiCell(120,5, iconv(HTTPCHARSET,"CP1252",$message),'1','C',1);
}
//--------------------------------------------------------------------------
//
$aujourdhui = date('Ymd-His');
$pdf->Output("listecarteretour-".$aujourdhui.".pdf","I");
$pdf->Close();

?> 

<?php
/**
 * Ce fichier permet de realiser l'edition de tous les electeurs presents en
 * centre de vote
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"edition");

//
set_time_limit(480);

/**
 *
 */
require_once("../app/fpdf_table.php");

//
include("../sql/".$f->phptype."/pdf_listecentrevote.inc");
$res_select_bureau_canton = $f->db->query($query_select_bureau_canton);
$f->isDatabaseError($res_select_bureau_canton);
$bureaux = array();
while ($row_select_bureau_canton =& $res_select_bureau_canton->fetchRow(DB_FETCHMODE_ASSOC)) {
	array_push($bureaux, $row_select_bureau_canton);
}

$nolibliste = $_SESSION ['liste']." - ".$_SESSION ['libelle_liste'];

$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(false);
$pdf->SetFont('Arial','',10);
$pdf->SetDrawColor(30,7,146);
$pdf->SetMargins(5,10,5);
$pdf->SetDisplayMode('real','single');

$param=array();
$param=array(9,9,9,1,1,20,20,5,10,1,1);

//--------------------------------------------- nombre enregistrement //
$ligne=0;
$total=0;
//--------------------------------------------------------------------------//
foreach ($bureaux as $b)
{
	$nobureau = $b ['code'];

	//------------------------------------------------------------------------//
	//    SQL                                                                 //
	//------------------------------------------------------------------------//
	include("../sql/".$f->phptype."/pdf_listecentrevote.inc");
	
	//---titre et entete-------------------------------------------------------//
	$aujourdhui = date("d/m/Y");
	$heure = date("H:i");
	$heighttitre=6;
	$titre=array();
	$titre=array(
	array('<PAGE>',270,$heighttitre,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
	array(sprintf(_('Gestion des Elections   -   Mairie de  :  %s'),$f -> collectivite ['ville']),270,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
	array(_('Table des renseignements divers'),270,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
	array(sprintf(_('Liste %s'),$nolibliste),165,$heighttitre,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
	array(sprintf(_('Edite le %s a %s'),$aujourdhui,$heure),110,$heighttitre,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
	array(_('Rubrique CV'),30,$heighttitre+5,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
	array(_('Inscription en centre de vote'),110,$heighttitre+5,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0)
	);
	//------------------------entete colonne   -------------------------------//
	$heightentete=10;
	$entete=array();
	$entete=array(
	array(_('Affectation'),25,$heightentete-5,2,'LTR','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
	array(_('Bur.          Num.'),25,$heightentete-5,0,'LBR','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
	array(_('Nom Patronymique et Prenoms'),110,$heightentete,0,'1','L','0','0','0','255','255','255',array(0,5),0,'','NB',0,1,0),
	array(_('Texte Libre'),105,$heightentete,0,'1','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
	array(_('Date'),25,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
	array(_('Total'),20,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0)
	);
	//------------------------page fin ---------------------------------------//
	$nbr_enregistrement=0;
	$pagefin=array();
	$pagefin=array(
	array(sprintf(_('Total pour le bureau  No %s    -   %s'),$b ['code'], $b ['libelle_bureau']),265,10,0,'1','L','0','0','0','255','255','255',array(0,0),0,'','NB',9,1,0),
	array('<LIGNE>',18,10,0,'LBT','R','0','0','0','255','255','255',array(0,0),0,'','NB',9,1,0),
	array(' ',2,10,1,'TBR','R','0','0','0','255','255','255',array(0,0),0,'','NB',9,1,0)
	);

	//------------------------tableau  --------------------------------------//
	$heightligne=5;
	$col=array();
	$col=array('code_bureau'=>array(12,$heightligne,0,'TLB','L','0','0','0','255','255','255',array(0,0),' ','',
	'CELLSUP_NON',0,'NA','NC','','B',8,array('0')),
	'numero_bureau'=>array(13,$heightligne,0,'TBR','L','0','0','0','255','255','255',array(0,0),'    ','',
	'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
	'nom'=>array(110,$heightligne,0,'1','L','0','0','0','255','255','255',array(0,0),' ','',
	'CELLSUP_VIDE',array(105,$heightligne,0,'1','C','0','0','0','255','255','255',array(0,0)),'NA','NC',array('NN'),'NB',0,array('0')),
	'datefin'=>array(25,$heightligne,0,'1','C','0','0','0','255','255','255',array(0,0),' ','',
	'CELLSUP_VIDE',array(20,$heightligne,0,'1','C','0','0','0','255','255','255',array(0,0)),'NA','NC',array('NN'),'NB',0,array('0'))
	);
	//
	$enpied=array();
	//
	$pdf->Table($titre,$entete,$col,$enpied,$query_select_centrevote,$f->db,$param,$pagefin);
	//------------------------------------------------------------------------//
	//    dans FPDF_table $nbrligne egal <LIGNE>                              //
	//------------------------------------------------------------------------//
	$total=$total+$pdf->nbrligne;
	//
}//fin boucle for

if ($total==0)
{
$pdf->addpage();
$pdf->SetTextColor(245,34,108);
$pdf->SetDrawColor(245,34,108);
$pdf->SetXY(10, 10);
$pdf->MultiCell(100,10,iconv(HTTPCHARSET,"CP1252",_('   Aucun enregistrement selectionne')),1);
}
else
{
// ----------------------total boucle -------------------------------------//
$heightligne=10;
$pdf->Cell(265,$heightligne,iconv(HTTPCHARSET,"CP1252",_('T   O   T   A   L          R   U   B   R   I   Q   U   E')),'1',0,'L',1);
$pdf->Cell(18,$heightligne,iconv(HTTPCHARSET,"CP1252",$total),'LTB',0,'R',1);
$pdf->Cell(2,$heightligne,'','TRB',0,'R',1);
//-------------------------------------------------------------------------//
}
$aujourdhui = date('Ymd-His');
$pdf->Output("listecentrevote-".$aujourdhui.".pdf","I");
$pdf->Close();

?>
<?php
/**
 * Ce fichier permet de generer un fichier pdf (tableau) a partir d'un array
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"edition");

//
if (isset($_GET['obj'])) {
    $obj = $_GET['obj'];
} else {
    die("Un objet est requis.");
}

//
if (!file_exists("../trt/stats_".$obj.".php")) {
    $f->notExistsError();
}

// Ajout d'une limite haute pour la génération des fichiers statistiques
set_time_limit(180);

//
include "../trt/stats_".$obj.".php";

//
require_once "../obj/pdf_common.class.php";

//
if (isset($tableau)) {
	if (!isset($pdf)) {
    	$pdf=new PDF($tableau['format'],'mm','A4');
        $pdf->generic_document_header_left = $f->collectivite["ville"];
    }
    $pdf->init_header_pdffromarray();    
    //$pdf->AddPage();

    if (isset($tableau)) $pdf->tableau($tableau);
    if (isset($tableau2)) $pdf->tableau($tableau2);
} else{
    die(_("Aucune donnees transmises."));
}

?>

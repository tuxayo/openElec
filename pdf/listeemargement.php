<?php
/**
 * Ce fichier permet de realiser l'edition des listes d'emargement
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"edition");

//
require_once("../app/fpdf_table.php");
set_time_limit(480);

//
$libelle_entete=_("LISTE D'EMARGEMENT");
$id = "";
$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];
//
if (isset($_GET['id'])) {
    //
    $id = $_GET['id'];
    //
    if ($id == "bureau") {
        if (isset($_GET['idx'])) {
            //
            $nobureau = $_GET['idx'];
            $message = sprintf(_("LISTE EMARGEMENT\nListe %s\nAucun enregistrement selectionne pour le bureau %s"),
                $nolibliste,
                $_GET['idx']);
        }
    }
}

//
$option_tri_liste_emargement = $f->getParameter("option_tri_liste_emargement");

//
$double_emargement = $f->getParameter("double_emargement");

//
include "../sql/".OM_DB_PHPTYPE."/pdf_listeemargement.inc";

//
$pdf=new PDF('L', 'mm', 'A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(false);
// Fixe la police utilisée pour imprimer les chaînes de caractères
$pdf->SetFont(
    'Arial', // Famille de la police.
    '', // Style de la police.
    7 // Taille de la police en points. 
);
// Fixe la couleur pour toutes les opérations de tracé
$pdf->SetDrawColor(30, 7, 146);
// Fixe les marges
$pdf->SetMargins(
    10, // Marge gauche.
    10, // Marge haute.
    10 // Marge droite.
);
$pdf->SetDisplayMode('real', 'single');
$pdf->AddPage();

//
$param=array();
//
$aujourdhui = date("d/m/Y");
//
$ligne=0;
$total_ligne=0;
$total=0;
$decompte=0;
$nbr_enregistrement=0;
//
$libelle_bureau = "";
$libelle_canton = "";
$resbur = $f->db->query($sqlbur);
$f->isDatabaseError($resbur);
$rowbur =& $resbur -> fetchRow (DB_FETCHMODE_ASSOC);
$libelle_bureau=$rowbur['libbur'];
$libelle_canton=$rowbur['libcanton'];
$libbureau=$libelle_bureau;

// Variable de developpement pour afficher les bordures de toutes les cellules
$allborders = false;
//------------------------titre------------------------------------------//
//
$heighttitre=4;
//
$titre = array();
$titre = array(
    //
    array('Le '.$aujourdhui,
        210,$heighttitre,0, ($allborders == true ? 1 : 0),'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
    //
    array('<PAGE>',
        67,$heighttitre,1, ($allborders == true ? 1 : 0),'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
    //
    array(sprintf(_('LISTE D\'EMARGEMENT   -   COMMUNE :  %s'),$f -> collectivite ['ville']),
        120,$heighttitre,1, ($allborders == true ? 1 : 0),'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
    //
    array(sprintf(_('Liste %s'),$nolibliste),
        90,$heighttitre,1, ($allborders == true ? 1 : 0),'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
    //
    array(sprintf(_('Bureau No %s %s'),$nobureau, $libelle_bureau),
        90,$heighttitre,1, ($allborders == true ? 1 : 0),'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
);
//------------------------entete colonne   ------------------------------------------------------------------------//
//
$heightentete=8;
//
$entete=array();

//------------------------page fin ----------------------------------------------------------------------------------

//
$pagefin = array();
$pagefin = array(
    //
    array('Le '.$aujourdhui,
        210,$heighttitre,0, ($allborders == true ? 1 : 0),'R','0','0','0','255','255','255',array(0,0),0,'','NB',8,1,0),
    //
    array('<PAGE>',
        67,$heighttitre,1, ($allborders == true ? 1 : 0),'R','0','0','0','255','255','255',array(0,0),0,'','NB',8,1,0),
    //
    array(sprintf(_('LISTE D\'EMARGEMENT   -   COMMUNE :  %s'),$f -> collectivite ['ville']),
        120,$heighttitre,1, ($allborders == true ? 1 : 0),'L','0','0','0','255','255','255',array(0,0),0,'','B',8,1,0),
    //
    array(sprintf(_('Liste %s'),$nolibliste),
        90,$heighttitre,1, ($allborders == true ? 1 : 0),'L','0','0','0','255','255','255',array(0,0),0,'','NB',8,1,0),
    //
    array(_(' *  *  *  A R R E T E  *  *  *'),
        260,10,2, ($allborders == true ? 1 : 0),'C','0','0','0','255','255','255',array(0,0),0,'','B',8,1,0),
    //
    array(_('La liste d \' emargement '),
        260,10,2, ($allborders == true ? 1 : 0),'C','0','0','0','255','255','255',array(0,0),0,'','NB',10,1,0),
    //
    array('  ',
        50,10,0, ($allborders == true ? 1 : 0),'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
    //
    array(sprintf(_(' du bureau No %s  %s   -   au nombre de '),$nobureau,$libelle_bureau),
        130,10,0, ($allborders == true ? 1 : 0),'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
    //
    array('<LIGNE>',
        30,10,0, ($allborders == true ? 1 : 0),'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
    //
    array(_(' electeur(s).'),
        25,10,2, ($allborders == true ? 1 : 0),'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0)
);

//
$col=array();
$enpied=array();
$pagedebut=array();

//
$param = array(
    8, // Titre : Taille de la police en points. 
    7, // Entete : Taille de la police en points. 
    7, // Taille de la police en points. 
    1, // 
    1, // 
    10, // Nombre maximum d'enregistrements par page pour la première page
    10, // Nombre maximum d'enregistrements par page pour les autres pages
    10, // Position de départ - X - La valeur de l'abscisse.
    10, // Position de départ - Y - La valeur de l'ordonnée.
    0, // 
    0 // 
);

//
$annee = (string)((int)date("Y")-1)." - ".date("Y");

//
$pagedebut = array(
    //
    array(sprintf(_('Le %s'),$aujourdhui), 
        210, $heighttitre, 0, ($allborders == true ? 1 : 0), 'R', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0),
    //
    array('<PAGE>', 
        67, $heighttitre, 1, ($allborders == true ? 1 : 0), 'R', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0),
    //
    array(_('FICHIER ELECTORAL'), 
        0, 20, 2, ($allborders == true ? 1 : 0), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 18, 1, 0),
    //
    array(sprintf(_('ANNEE : %s'),$annee), 
        0, 20, 2, ($allborders == true ? 1 : 0), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 18, 1, 0),
    //
    array($libelle_entete, 
        0, 20, 2, ($allborders == true ? 1 : 0), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 18, 1, 0),
    //
    array(sprintf(_('COMMUNE: %s'),$f -> collectivite ['ville']), 
        0, 20, 1, ($allborders == true ? 1 : 0), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 18, 1, 0),
    //
    array(sprintf(_('BUREAU: %s %s'), $nobureau, $libbureau), 
        0, 20, 1, ($allborders == true ? 1 : 0), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 18, 1, 0),
);

//
$entete = array(
    //
    array(_('Nom Patronymique,Prenoms'),
        85, $heightentete-4, 2, ($allborders == true ? 1 : 'LRT'), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0),
    //
    array(sprintf(_('Nom d\'Usage,Date et lieu de naissance%s'),($_SESSION["liste"] != "01" ? _(", Nationalite") : "")),
        85, $heightentete-4, 0, ($allborders == true ? 1 : 'LRB'), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0),
    //
    array(_('Adresse'),
        95, $heightentete, 0, ($allborders == true ? 1 : 1), 'C', '0', '0', '0', '255', '255', '255', array(0, 4), 0, '', 'NB', 0, 1, 0),
    //
    array(_('No Ordre'),
        12, $heightentete, 0, ($allborders == true ? 1 : 1), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0),
);
//
if ($double_emargement == true) {
    //
    array_push($entete,
        //
        array(_('Emargement'),
            85, $heightentete-4, 2, ($allborders == true ? 1 : 'LRT'), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0)
    );
    //
    array_push($entete,
        //
        array('Tour 2                                              Tour 1',
            85, $heightentete-4, 0, ($allborders == true ? 1 : 'LRB'), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0)
    );
} else {
    //
    array_push($entete,
        //
        array(_('Emargement'),
            85, $heightentete, 0, ($allborders == true ? 1 : 1), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0)
    );
}

//
$heightligne = 15;
$line_height = 3;
$line_nb = 5;
//
$col = array(
    // colonne 1 - ligne 1
    'nomprenom' => array(
        85, $line_height, 2, ($allborders == true ? 1 : 'LRT'), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), ' ', '', 'CELLSUP_NON', 0, 'A', 'NC', '', 'B', 7, array('0')),
    // colonne 1 - ligne 2
    'nom_usage' => array(
        85, $line_height, 2, ($allborders == true ? 1 : 'LR'), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), '       -    ', '', 'CELLSUP_NON', 0, 'NA', 'CONDITION', array('NN'), 'NB', 0, array('0')),
    // colonne 1 - ligne 3
    'naissance' => array(
        27, $line_height, 0, ($allborders == true ? 1 : 'L'), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), _('     Ne(e) le '), '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'NB', 0, array('0')),
    // colonne 1 - ligne 3
    'lieu' => array(
        58, $line_height, 1, ($allborders == true ? 1 : 'R'), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), _(' a  '), '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'NB', 0, array('0'))
);
//
if ($_SESSION["liste"] == "01") {
    // colonne 1 - ligne 4
   $col["colonne1_ligne4"] = array(
        85, $line_height, 2, ($allborders == true ? 1 : 'LR'), 'C', '213', '8', '26', '255', '255', '255', array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0'));
} else {
    // colonne 1 - ligne 4
   $col["colonne1_ligne4"] = array(
        85, $line_height, 2, ($allborders == true ? 1 : 'LR'), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), '     ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'NB', 0, array('0'));
}
//
$col = array_merge($col, array(
    // colonne 1 - ligne 4
    'colonne1_ligne5' => array(
        85, $line_height, 0, ($allborders == true ? 1 : 'LRB'), 'C', '213', '8', '26', '255', '255', '255', array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
    // colonne 2 - ligne 1
    'adresse' => array(
        95, $line_height, 2, ($allborders == true ? 1 : 'LRT'), 'L', '0', '0', '0', '255', '255', '255', array(0, $line_height*($line_nb-1)), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'NB', 0, array('0')),
    // colonne 2 - ligne 2
    'complement' => array(
        95, $line_height, 2, ($allborders == true ? 1 : 'LR'), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'NB', 0, array('0')),
    // colonne 2 - ligne 3
    'colonne2_ligne3' => array(
        95, $line_height, 2, ($allborders == true ? 1 : 'LR'), 'L', '213', '8', '26', '255', '255', '255', array(0, 0), ' ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
    // colonne 2 - ligne 4
    'colonne2_ligne4' => array(
        95, $line_height, 2, ($allborders == true ? 1 : 'LR'), 'L', '213', '8', '26', '255', '255', '255', array(0, 0), ' ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
    // colonne 2 - ligne 4
    'colonne2_ligne5' => array(
        95, $line_height, 0, ($allborders == true ? 1 : 'LRB'), 'L', '213', '8', '26', '255', '255', '255', array(0, 0), ' ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
    // colonne 3 - ligne 1
    'numero_bureau' => array(
        12, $line_height*$line_nb, 0, ($allborders == true ? 1 : 1), 'R', '0', '0', '0', '255', '255', '255', array(0, $line_height*($line_nb-1)), ' ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'NB', 0, array('0')),
));
//
if ($double_emargement == true) {
    //
    $col = array_merge($col, array(
        // colonne 3 - ligne 1
        'tour2' => array(
            42, $line_height*$line_nb, 0, ($allborders == true ? 1 : 'LTBR'), 'C', '210', '210', '210', '255', '255', '255', array(0, 0), ' ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
        // colonne 3 - ligne 1
        'tour1' => array(
            41, $line_height*$line_nb, 0, ($allborders == true ? 1 : 'LTB'), 'C', '210', '210', '210', '255', '255', '255', array(0, 0), ' ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),

        // colonne 4 - ligne
        'emargement' => array(
            1, $line_height*$line_nb, 0, ($allborders == true ? 1 : 'TB'), 'L', '24', '4', '148', '255', '255', '255', array(0, 0), '', '', '0', 
                //
                array(1, $line_height*$line_nb, 0, ($allborders == true ? 1 : 'RTB'), 'LDCELL', 
                    '24', '4', '148', 
                    '255', '255', '255', 
                    array(0, 0), -1, $line_nb, 'B', 7, 1, 0),
            'NA', 'NC', '', 'B', 0, array('0')),
    ));
} else {
    //
    $col = array_merge($col, array(
        // colonne 4 - ligne
        'emargement' => array(
            1, $line_height*$line_nb, 0, ($allborders == true ? 1 : 'LTB'), 'L', '24', '4', '148', '255', '255', '255', array(0, 0), '', '', '0', 
                //
                array(84, $line_height*$line_nb, 0, ($allborders == true ? 1 : 'RTB'), 'LDCELL', 
                    '24', '4', '148', 
                    '255', '255', '255', 
                    array(0, 0), -1, $line_nb, 'B', 7, 1, 0),
            'NA', 'NC', '', 'B', 0, array('0')),
    ));
}

//
$pdf->Table($titre, $entete, $col, $enpied, $sql, $f->db, $param, $pagefin, $pagedebut);

//
$total=$pdf->nbrligne;

// pas de prise en compte de centre de vote si pas d inscrit en centre de vote
if ($total > 0) {
    //
    $nb_ligneproc = 0;
    $nb_ligneproc = $f->db->getOne($sqlproc);
    $f->isDatabaseError($sqlproc);
    //
    if ($nb_ligneproc > 0) {
        //
        $decompte = $total - $nb_ligneproc;
        $heightligne = 10;
        //
        $pdf->Ln(0);
        $pdf->Cell(50,$heightligne,' ', ($allborders == true ? 1 : 0),0,'R',1);
        $pdf->Cell(130,$heightligne,iconv(HTTPCHARSET,"CP1252",_('  Nombre inscrit(s) en centre de vote ou mairie europe ')), ($allborders == true ? 1 : 0),0,'L',1);
        $pdf->Cell(30,$heightligne,iconv(HTTPCHARSET,"CP1252",$nb_ligneproc), ($allborders == true ? 1 : 0),0,'R',1);
        $pdf->Cell(25,$heightligne,iconv(HTTPCHARSET,"CP1252",_(' electeur(s)')), ($allborders == true ? 1 : 0),1,'L',1);
        $pdf->Cell(50,$heightligne,' ', ($allborders == true ? 1 : 0),0,'R',1);
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(130,$heightligne,iconv(HTTPCHARSET,"CP1252",_(' TOTAL avec deduction inscrit(s) en centre de vote ou mairie europe ')), ($allborders == true ? 1 : 0),0,'L',1);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(30,$heightligne,iconv(HTTPCHARSET,"CP1252",$decompte), ($allborders == true ? 1 : 0),0,'R',1);
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(25,$heightligne,iconv(HTTPCHARSET,"CP1252",_(' electeur(s)')), ($allborders == true ? 1 : 0),1,'L',1);
    } else {
        //
        $heightligne=10;
        //
        $pdf->Ln(0);
        $pdf->Cell(5,$heightligne,' ', ($allborders == true ? 1 : 0),0,'R',1);
        $pdf->Cell(5,$heightligne,' ', ($allborders == true ? 1 : 0),0,'L',1);
        $pdf->Cell(5,$heightligne,' ', ($allborders == true ? 1 : 0),0,'R',1);
        $pdf->Cell(5,$heightligne,' ', ($allborders == true ? 1 : 0),1,'L',1);
        $pdf->Cell(5,$heightligne,' ', ($allborders == true ? 1 : 0),0,'R',1);
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(5,$heightligne,' ', ($allborders == true ? 1 : 0),0,'L',1);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(5,$heightligne,' ', ($allborders == true ? 1 : 0),0,'R',1);
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(5,$heightligne,' ', ($allborders == true ? 1 : 0),1,'L',1);
    }
    //
    $pdf->SetFont('Arial','',8);
    $pdf->Cell(50,$heightligne,' ', ($allborders == true ? 1 : 0),0,'R',1);
    $pdf->Cell(130,$heightligne+5,iconv(HTTPCHARSET,"CP1252",sprintf(_(' %s, le %s'),$f -> collectivite ['ville'],date("d/m/Y"))), ($allborders == true ? 1 : 0),0,'L',1);
    // Ajout du tampon de la mairie et de la signature du maire si l'image est
    // présente sur le filesystem
    if (file_exists("../trs/signature-liste-emargement.png")) {
        $pdf->Ln();
        $pdf->Image('../trs/signature-liste-emargement.png', 45, 90, 105, 28,'png');
    }
}

// message  aucun enregistrement--------------------------------------------
if ($pdf->msg==1 and $message!='') {
    //
    $pdf->SetFont('Arial','',10);
    $pdf->SetDrawColor(0,0,0);
    $pdf->SetFillColor(213,8,26);
    $pdf->SetTextColor(255,255,255);
    $pdf->MultiCell(120,5,iconv(HTTPCHARSET,"CP1252",$message), ($allborders == true ? 1 : 1),'C',1);
}

/**
 * OUTPUT
 */
// Construction du nom du fichier
$filename = date("Ymd-His");
$filename .= "-emargement";
$filename .= "-".$_SESSION['collectivite'];
$filename .= "-".$f->normalizeString($_SESSION['libelle_collectivite']);
$filename .= "-liste".$_SESSION['liste'];
$filename .= "-bureau".$_GET['idx'];
$filename .= ".pdf";

// Sortie du pdf
if (isset($_GET['multi']) and $_GET['multi'] == true) {
    //
    $filename = $f->getParameter("pdfdir").$filename;
    $contenu = $pdf->Output("", "S");
} elseif (isset($_GET['mode']) and $_GET['mode'] == "save") {
    //
    $filename_with_path = $f->getParameter("pdfdir").$filename;
    $pdf->Output($filename_with_path, "F");
} else {
    //
    $pdf->Output($filename, "I");
}

// Destruction de l'objet PDF
$pdf->Close();

?>

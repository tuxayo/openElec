<?php
/**
 * Ce fichier permet de realiser l'edition des statistiques sur les
 * saisies, cette edition est utile seulement pour le mode MULTI
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"edition");

//
define('FPDF_FONTPATH','font/');
require_once("fpdf.php");
set_time_limit (480);
$aujourdhui = date("d/m/Y");
$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];

//
include "../sql/".$f -> phptype."/pdf_statistique_saisie.inc";

// Communes
$commune = array ();

$sqlcom = $sqlcom0;
if ($f->isMulti() == true) $sqlcom = $sqlcom1;

$rescom =& $f -> db -> query ($sqlcom);
$f->isDatabaseError($rescom);

while ($rowcom =& $rescom -> fetchRow (DB_FETCHMODE_ASSOC))
    array_push ($commune, $rowcom);

// Fonction Entete & Tableau
function pdf_stat_entete() {
    global $pdf;
    global $aujourdhui;
    global $f;
    $pdf->addpage();
    // entete
    $pdf->SetFont('courier','B',11);
    $pdf->Cell(190,7,iconv(HTTPCHARSET,"CP1252",_('SAISIES SUR LE TABLEAU EN COURS DU ').
        $f->formatDate($f -> collectivite ['datetableau']).' - '.$_SESSION['libelle_liste']),'0',0,'L',0);
    $pdf->SetFont('courier','',11);
    $pdf->Cell(50,7, iconv(HTTPCHARSET,"CP1252",$aujourdhui),'0',0,'C',0);
    $pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",_(' Page  :  ').$pdf->PageNo()."/{nb} "),'0',1,'R',0);
    $pdf->ln();
    // Tableau
    $pdf->Cell(100,7,iconv(HTTPCHARSET,"CP1252",_('COMMUNE')),1,'0','L',0);
    $pdf->MultiCell(50,7,iconv(HTTPCHARSET,"CP1252",_("NOMBRE D'ELECTEURS AU DERNIER TABLEAU")),1,'C',0);
    $pdf->SetXY(155,19);
    $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",_('INSCRIPTIONS')),1,0,'L',0);
    $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",_('RADIATIONS')),1,0,'L',0);
    $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",_('MODIFICATIONS')),1,1,'L',0);
    $pdf->Cell(25,7,iconv(HTTPCHARSET,"CP1252",_('CODE')),1,0,'L',0);
    $pdf->Cell(75,7,iconv(HTTPCHARSET,"CP1252",_('NOM')),1,0,'L',0);
    $pdf->Cell(50,7,'',0,0,'L',0);
    $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",_('NOMBRE')),1,0,'C',0);
    $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",_('NOMBRE')),1,0,'C',0);
    $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",_('NOMBRE')),1,1,'C',0);
}

$pdf=new FPDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(true);
$pdf->SetFont('courier','',11);
$pdf->SetDrawColor(30,7,146);
$pdf->SetMargins(5,5,5);
$pdf->SetDisplayMode('real','single');

pdf_stat_entete();

//  compteur de limite d'affichage
$compteur = 0;

//  Totaux
$cpt_totElecteur = 0;
$cpt_totInscription = 0;
$cpt_totModification = 0;
$cpt_totRadiation = 0;

// Requete Mouvement
$mouvements = array ();
$resmov =& $f -> db -> query ($sqlmov);
$f->isDatabaseError($resmov);
while ($rowmov =& $resmov -> fetchRow (DB_FETCHMODE_ASSOC))
    array_push ($mouvements, $rowmov);

//
foreach ($commune as $c) {
    // Statistique de la Commune
    // Mouvements
    $nb_insj5 = 0;
    $nb_radj5 = 0;
    $nb_modj5 = 0;
    $nb_radtrs = 0;
    $nb_instrs = 0;
    
    foreach ($mouvements as $m) {
        // Mouvement Inscription / Radiation / Modification
        if( $m['typecat']=='Inscription' && $m['collectivite']==$c['id'] ) $nb_insj5++;
        if( $m['typecat']=='Radiation' && $m['collectivite']==$c['id'] ) $nb_radj5++;
        if( $m['typecat']=='Modification' && $m['collectivite']==$c['id'] ) $nb_modj5++;
        // Mouvement trs Inscription / Radiation
        if ($m['etat']=='trs') {
            if( $m['typecat']=='Inscription' && $m['collectivite']==$c['id'] ) $nb_instrs++;
            if( $m['typecat']=='Radiation' && $m['collectivite']==$c['id'] ) $nb_radtrs++;
        }
    }
    
    //
    $totalElecteur = 0;
    $totalElecteur = ( $c['total'] + $nb_radtrs ) - $nb_instrs;

    // Compteurs Totaux
    $cpt_totElecteur += $totalElecteur;
    $cpt_totInscription += $nb_insj5;
    $cpt_totModification += $nb_modj5;
    $cpt_totRadiation += $nb_radj5;
    
    // Ajout Page et Entete - limite affichage atteint //////////////
    $compteur++;
    if($compteur>=24){ pdf_stat_entete(); $compteur=1; }
    //
    $pdf->Cell(25,7,' '.iconv(HTTPCHARSET,"CP1252",$c['id']),0,0,'L',0);
    $pdf->Cell(75,7,' '.iconv(HTTPCHARSET,"CP1252",$c['ville']),0,0,'L',0);
    $pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",$totalElecteur).'      ',0,0,'R',0);
    $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",$nb_insj5).'      ',0,0,'R',0);
    $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",$nb_radj5).'      ',0,0,'R',0);
    $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",$nb_modj5).'      ',0,1,'R',0); 

}

/// Totaux
$pdf->Cell(100,7,iconv(HTTPCHARSET,"CP1252",'TOTAL    '),1,0,'R',0);
$pdf->SetFont('courier','B',11);
$pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",$cpt_totElecteur).'      ',1,0,'R',0);
$pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",$cpt_totInscription).'      ',1,0,'R',0);
$pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",$cpt_totRadiation).'      ',1,0,'R',0);
$pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",$cpt_totModification).'      ',1,1,'R',0); 

//
$aujourdhui = date('Ymd-His');
$pdf->Output("stats_saisie-".$aujourdhui.".pdf","I");
$pdf->Close();

?>

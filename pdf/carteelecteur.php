<?php
/**
 * Ce fichier permet de realiser l'edition des cartes electorales
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"carteelecteur");

//
require_once("../app/fpdf_table_position.php");
set_time_limit (480);

//
global $codebar_on;
if ($f->getParameter("code_barre"))
    $codebar_on = true;
else
    $codebar_on = false;

//
$mode_edition = "";
if (isset($_GET["mode_edition"])) {
    $mode_edition = $_GET["mode_edition"];
    if ($mode_edition == "traitement") {
        // Les nouvelles cartes electorales suites a un traitement
        //
        $titre = _("NOUVELLES CARTES ELECTORALES");
        //
        $traitement = "";
        if (isset($_GET["traitement"])) {
            //
            $traitement = $_GET["traitement"];
            //
            $datetableau = "";
            if (isset($_GET["datetableau"])) {
                $datetableau = $_GET["datetableau"];
            }
            //
            $annee = (string)((int)substr($datetableau, 0, 4)-1)." - ".substr($datetableau, 0, 4);
            //
            if ($traitement == "j5") {
                //
                $datej5 = "";
                if (isset($_GET["datej5"])) {
                    $datej5 = $_GET["datej5"];
                }
                //
                $titre .= " "._("SUITE AU TRAITEMENT J-5 DU");
                $titre .= " ".$f->formatdate($datej5);
            } elseif ($traitement == "annuel") {
                //
                $titre .= " "._("SUITE AU TRAITEMENT ANNUEL DU");
                $titre .= " ".$f->formatdate($datetableau);
            }
        }
    } elseif ($mode_edition == "parbureau") {
        // Toutes les cartes electorales du bureau de vote
        $nobureau = "";
        if (isset ($_GET['idx'])) {
            $nobureau = $_GET['idx'];
        }
    } elseif ($mode_edition == "electeur") {
        // La carte electorale d'un seul electeur
        $id_electeur = "";
        if (isset ($_GET['idx'])) {
            $id_electeur = $_GET['idx'];
        }
    } elseif ($mode_edition == "commune") {
        // Toutes les cartes electorales de la commune
    } elseif ($mode_edition == "multielecteur") {
    }
    //
    $multi = false;
    if (isset($_GET["multi"])) {
        $multi = $_GET["multi"]; 
    }
}

//
$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];

include ("../sql/".$f->phptype."/pdf_carteelecteur.inc");

$pdf=new PDF('L','mm','A4');
$pdf->SetFont('Courier','');
$pdf->SetMargins(0,0);
$pdf->SetAutoPageBreak(false);
$pdf->Open();
$pdf->AddPage();
$pdf->SetDisplayMode('real','single');

$param=array();

$champs_compteur=array_fill(0,5,0);

$img=array();

$texte=array();

include ("param_position_carte_electeur.php");

//************************PAGE DEBUT******************
$pagedebut=array();
if (!isset($annee)) {
    $annee=(string)((int)date("Y")-1)." - ".date("Y");
}
$libbureau="";

if ($mode_edition == "commune") {
    //
    $pagedebut=array(
        array(_('FICHIER ELECTORAL'),260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array(_('ANNEE : ').$annee,260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array(_('CARTES ELECTORALES'),260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array('',260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array(_('COMMUNE: ').$f -> collectivite ['ville'],260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array(_('LISTE: ').$nolibliste,260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0)
    );
}
elseif ($mode_edition == "traitement") {
    //
    $pagedebut=array(
        array(_('FICHIER ELECTORAL'),260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array(_('ANNEE : ').$annee,260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array((isset($titre) ? $titre : _('CARTES ELECTORALES DES NOUVEAUX ELECTEURS')),260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array('',260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array(_('COMMUNE: ').$f -> collectivite ['ville'],260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array(_('LISTE: ').$nolibliste,260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0)
    );
}
elseif ($mode_edition == "parbureau") {
    //
    $pagedebut=array(
        array(_('FICHIER ELECTORAL'),260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array(_('ANNEE : ').$annee,260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array(_('CARTES ELECTORALES'),260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array('',260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array(_('COMMUNE: ').$f -> collectivite ['ville'],260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array(_('LISTE: ').$nolibliste,260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
        array(_('BUREAU: ').$nobureau.' '.$libbureau,260,20,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0)
    );
} elseif ($mode_edition == "electeur" or $mode_edition == "multielecteur") {
    //
    $pagedebut=array();
}
//
$paramPageDebut=array();
$paramPageDebut=array(8,8,8,1,1,10,10,10,10,0,0);
//
$pdf->Table_position($sql,$f -> db,$param,$champs,$texte,$champs_compteur,$img,$pagedebut,$paramPageDebut);


/**
 * OUTPUT
 */
// Construction du nom du fichier
if ($mode_edition == "commune" && $multi == false && $output = "file") {
    $filename = "carteelecteur-".$_SESSION['collectivite']."-".$_SESSION['liste'].".pdf";
} else {
    $filename = date("Ymd-His");
    $filename .= "-carteelecteur";
    $filename .= "-".$_SESSION['collectivite'];
    $filename .= "-".$f->normalizeString($_SESSION['libelle_collectivite']);
    $filename .= "-liste".$_SESSION['liste'];
    if ($mode_edition == "parbureau") {
        $filename .= "-bureau".$nobureau;
    }
    $filename .= ".pdf";
}
//
$output = "";
if (isset($_GET["output"])) {
    $output = $_GET["output"];
}
if (!in_array($output, array("string", "file", "download", "inline", "no"))) {
    $output = "inline"; // Valeur par defaut
}
//
if ($output == "string") {
    // S : renvoyer le document sous forme de chaine. name est ignore.
    $pdf_output = $pdf->Output("", "S");
} elseif ($output == "file") {
    // F : sauver dans un fichier local, avec le nom indique dans name
    // (peut inclure un repertoire).
    $pdf->Output($f->getParameter("pdfdir").$filename, "F");
} elseif ($output == "download") {
    // D : envoyer au navigateur en forcant le telechargement, avec le nom
    // indique dans name.
    $pdf->Output($filename, "D");
} elseif ($output == "inline") {
    // I : envoyer en inline au navigateur. Le plug-in est utilise s'il est
    // installe. Le nom indique dans name est utilise lorsque l'on selectionne
    // "Enregistrer sous" sur le lien generant le PDF.
    $pdf->Output($filename, "I");
} elseif ($output == "no") {
    //
}
//
if ($mode_edition == "commune" && $multi == false && $output = "file") {
    //
    @header ("Location: ../app/editions.php");
} elseif (($mode_edition == "commune" && $multi == true && $output = "string")
          || ($mode_edition == "traitement" && $multi == true && $output = "string")) {
    //
    $filename = $f->getParameter("pdfdir").$filename;
    $contenu = $pdf_output;
}

?>

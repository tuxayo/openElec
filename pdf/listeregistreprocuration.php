<?php
/**
 * Ce fichier permet de realiser l'edition du registres des procurations
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) $f = new utils("nohtml", /*DROIT*/"edition");

//
require('../app/fpdf_table.php');
set_time_limit (480);
include ("../sql/".$f -> phptype."/pdf_listeregistretotproc.inc");
//
$nolibliste = $_SESSION['liste']." - ".$_SESSION['libelle_liste'];
//
$id='';
if (isset($_GET['id']))
{
    $id= $_GET['id'];
    if($id=="bureau")
    if (isset($_GET['idx']))
    {
	$nobureau=$_GET['idx'];
	// message aucun enregistrement sélectionné---------------------------
	$message=sprintf(_("EDITION DU REGISTRE DES PROCURATIONS\nListe %s\nAucun enregistrement selectionne pour le bureau %s"),$nolibliste,$nobureau);
	//--------------------------------------------------------------------
    }
}
//**************************************************************************//
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(false);
$pdf->SetFont('Arial','',8);
$pdf->SetDrawColor(30,7,146);
$pdf->SetMargins(5,10,5);
$pdf->SetDisplayMode('real','single');
//--------------------------------------------------------------------------//
$param=array();
$param=array(9,7,8,1,1,8,8,10,5,1,0);
//--------------------------------------------- nombre enregistrement       //
$ligne=0;
$total_ligne=0;
//--------------------------------------------------------------------------//
//
$pdf->AddPage();
//
//--------------------------------------------------------------------------//
//    SQL                                                                   //
//--------------------------------------------------------------------------//
include ("../sql/".$f->phptype."/pdf_listeregistreprocuration.inc");
//recup libelle bureau et libelle canton====================================
$libelle_bureau='';
$libelle_canton='';
$resbur = $f->db->query($sqlbur);
if (database::isError($resbur)) {
die($resbur->getMessage());
}
else
{
$rowbur=& $resbur->fetchRow(DB_FETCHMODE_ASSOC);
$libelle_bureau=$rowbur['libbur'];
$libelle_canton=$rowbur['libcanton'];
}
//------------------------titre------------------------------------------//
$aujourdhui = date("d/m/Y");
$heighttitre=4;
$titre=array();
$titre=array(
array('<PAGE>',270,$heighttitre,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Mairie de :  %s'),$f->collectivite ['ville']),120,$heighttitre,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('EDITION DU REGISTRE DES PROCURATIONS'),50,$heighttitre,1,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Liste %s'),$nolibliste),120,$heighttitre,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Le %s'),$aujourdhui),50,$heighttitre,1,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Bureau No %s %s'),$nobureau, $libelle_bureau),120,$heighttitre+1,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(sprintf(_('Canton  %s'),$libelle_canton),50,$heighttitre+1,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('Circonscription '),100,$heighttitre+1,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0)
);
//------------------------entete colonne   ------------------------------------------------------------------------//
$heightentete=8;
$entete=array();
$entete=array(
array(_('M A N D A N T'),55,$heightentete,0,'LRT','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('M A N D A T A I R E'),55,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('O R I G I N E   DE   LA   D E M A N D E'),50,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('REFUS'),10,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('MOTIF'),45,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('DATE ET HEURE'),25,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('DUREE'),22,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
array(_('OBSERVATIONS'),22,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0)
);
//------------------------page fin ----------------------------------------------------------------------------------
$nbr_enregistrement=0;
$pagefin=array();
//--------------------------------------------------------------------------//
$heightligne=5;
$col=array();
$col=array(
	'nom'=>array(55,$heightligne,2,'LRT','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','B',8,array('0')),
	'nom_usage'=>array(55,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'       -    ','','CELLSUP_NON',0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
	'prenom'=>array(55,$heightligne,0,'LBR','L','0','0','0','255','255','255',array(0,0),'    ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
	'nomm'=>array(55,$heightligne,2,'LRT','L','0','0','0','255','255','255',array(0,10),' ','','CELLSUP_NON',0,'NA','NC','','B',8,array('0')),
	'nom_usagem'=>array(55,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'       -    ','','CELLSUP_NON',0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
	'prenomm'=>array(55,$heightligne,0,'LBR','L','0','0','0','255','255','255',array(0,0),'    ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
    'origine1_1'=>array(50,$heightligne-1.25,2,'LRT','L','0','0','0','255','255','255',array(0,10),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
	'origine1_2'=>array(50,$heightligne-1.25,2,'LR','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
	'origine2_1'=>array(50,$heightligne-1.25,2,'LR','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
	'origine2_2'=>array(50,$heightligne-1.25,0,'LBR','L','0','0','0','255','255','255',array(0,0),' ','', 'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
	'refus'=>array(10,$heightligne+10,0,'1','L','0','0','0','255','255','255',array(0,11.25),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
	'motif_refus_1'=>array(45,$heightligne,2,'LRT','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
	'motif_refus_2'=>array(45,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
	'motif_refus_3'=>array(45,$heightligne,0,'LBR','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
	'dateaccord'=>array(25,$heightligne+2.5,2,'LTR','C','0','0','0','255','255','255',array(0,10),' Le ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
	'heureaccord'=>array(25,$heightligne+2.5,0,'LBR','C','0','0','0','255','255','255',array(0,0),_('  a '),'','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
	'debutvalidm'=>array(22,$heightligne+2.5,2,'LTR','C','0','0','0','255','255','255',array(0,7.5),' Du ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
	'finvalidm'=>array(22,$heightligne+2.5,0,'LBR','C','0','0','0','255','255','255',array(0,0),'  Au ','','CELLSUP_VIDE',array(22,$heightligne+10,0,'1','C','0','0','0','255','255','255',array(0,7.5)),0,'NA','NC','','NB',8,array('0')),
);
//
//echo $sql;
$enpied=array();
$pdf->Table($titre,$entete,$col,$enpied,$sql,$f->db,$param,$pagefin);
// message  aucun enregistrement--------------------------------------------
if ($pdf->msg==1 and $message!=''){
$pdf->SetFont('Arial','',10);
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(213,8,26);
$pdf->SetTextColor(255,255,255);
$pdf->MultiCell(120,5,iconv(HTTPCHARSET,"CP1252",$message),'1','C',1);
}
//--------------------------------------------------------------------------
$aujourdhui = date('Ymd-His');
$pdf->Output("listeregistreprocuration-".$aujourdhui.".pdf","I");
$pdf->Close();

?> 


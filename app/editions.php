<?php
/**
 * Ce fichier permet l'editions generales
 *
 * @package openelec
 * @version SVN : $Id$
 */


/**
 *
 */
require_once "../obj/utils.class.php";

//
$obj = "edition";

//
$f = new utils (NULL, $obj, _("Editions Generales"));

$description = _("Ces editions portent sur tous les electeurs, bureaux de vote confondus.");
$f->displayDescription($description);

// Liste des éditions
$tab_editions = array (
    0 => array ('img' => "<span class=\"om-icon om-icon-25 om-icon-fix listegenerale-25\"><!-- --></span>",
        'titre' => "LISTE ELECTORALE", 
        'pdf' => "../pdf/listegenerale.php",
        'generer' => "OUI",
        'fichier' => "listegenerale-".$_SESSION['collectivite']."-".$_SESSION['liste'].".pdf",
        'folder' => "pdf"),
    1 => array ('img' => "<span class=\"om-icon om-icon-25 om-icon-fix carteelecteur-25\"><!-- --></span>",
        'titre' => "CARTES D'ELECTEUR",
        'pdf' => "../pdf/carteelecteur.php?mode_edition=commune&output=file",
        'generer' => "OUI",
        'fichier' => "carteelecteur-".$_SESSION['collectivite']."-".$_SESSION['liste'].".pdf",
        'folder' => "pdf"),
    2 => array ('img' => "<span class=\"om-icon om-icon-25 om-icon-fix etiquetteelecteur-25\"><!-- --></span>",
        'titre' => "ETIQUETTES DE PROPAGANDE",
        'pdf' => "../app/pdfetiquette.php?obj=electeur&amp;mode=save&amp;redirect=../app/editions.php&amp;filename_with_date=false",
        'generer' => "OUI",
        'fichier' => "etiquettes-electeur-".$_SESSION['collectivite']."-".$f->normalizeString($_SESSION['libelle_collectivite'])."-liste".$_SESSION['liste'].".pdf",
        'folder' => "pdf"),
    3 => array ('img' => "<span class=\"om-icon om-icon-25 om-icon-fix commissionstat-25\"><!-- --></span>",
        'titre' => "STATISTIQUES GENERALES DES MOUVEMENTS",
        'pdf' => "../pdf/commissionstat.php",
        'generer' => "NON"),
    4 => array ('img' => "<span class=\"om-icon om-icon-25 om-icon-fix listeregistretotproc-25\"><!-- --></span>",
        'titre' => "REGISTRE DES PROCURATIONS CLASSEES PAR MANDANT",
        'pdf' => "../pdf/listeregistretotproc.php",
        'generer' => "NON"),
    5 => array ('img' => "<span class=\"om-icon om-icon-25 om-icon-fix listetotalprocuration-25\"><!-- --></span>",
        'titre' => "STATISTIQUES PROCURATION",
        'pdf' => "../pdf/listetotalprocuration.php",
        'generer' => "NON"),
    6 => array ('img' => "<span class=\"om-icon om-icon-25 om-icon-fix listecentrevote-25\"><!-- --></span>",
        'titre' => "LISTE DES ELECTEURS EN CENTRE DE VOTE",
        'pdf' => "../pdf/listecentrevote.php",
        'generer' => "NON"),
    7 => array ('img' => "<span class=\"om-icon om-icon-25 om-icon-fix etiquettecentrevote-25\"><!-- --></span>",
        'titre' => "ETIQUETTE DES ELECTEURS EN CENTRE DE VOTE",
        'pdf' => "../app/pdfetiquette.php?obj=centrevote",
        'generer' => "NON")
);

//
echo "<table class=\"editions\">\n";

//
foreach ($tab_editions as $key => $edition) {
    echo "\t<tr class=\"editions ".($key%2?"odd":"even")."\">";
    echo "<td class=\"icone\">".$edition ['img']."</td>";
    echo "<td class=\"titre\">".$edition ['titre']."</td>";
    echo "<td class=\"icone\">";
    if ($edition['generer'] == "NON") {
        echo " <a class=\"lien\" href=\"".$edition['pdf']."\"";
        if (!preg_match("#javascript#", $edition['pdf'])) { 
            echo " target=\"_blank\"";
        }
        echo ">";
        echo "<span class=\"om-icon om-icon-25 om-icon-fix pdf-25\"><!-- --></span>";
        echo "<br />";
        echo _("Visualiser");
        echo "</a>";
        echo "</td><td>&nbsp;";
    } else {
        $filename = $f->getParameter($edition['folder'].'dir')."/".$edition['fichier'];
        $tstat = array();
        if (file_exists($filename)) {
            echo "<a class=\"lien\" href=\"../app/file.php?fic=".$edition['fichier']."&amp;folder=".$edition['folder']."\" target=\"_blank\">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix pdf-25\"><!-- --></span>";
            echo "<br />";
            echo _("Visualiser");
            echo "</a>";
            echo "</td><td>";
            echo " <a class=\"lien\" href=\"".$edition ['pdf']."\">"._("Generer")."</a><br />";
            $tstat = stat($filename);
            echo " Taille : [".number_format ($tstat[7], 0, ',' ,' ')." Octets]<br />";
            echo " Date : [". date("d/m/Y  à H:i:s", $tstat[9])."]";
        } else {
            echo "&nbsp;</td><td>";
            echo " <a class=\"lien\" href=\"".$edition ['pdf']."\">"._("Generer")."</a><br />";
            echo _("Aucun fichier n'a ete genere");
        }
    }
    echo "</td>";
    echo "</tr>\n";
}
echo "</table>";

?>

<?php
/**
 * Ecran permettant l'acces aux editions PDF sur les statistiques de saisie
 * pour les communes multi
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once("../obj/utils.class.php");

//
$f = new utils(NULL,
               "statistiques_multi",
               _("Module statistiques"));

//
echo "<div id=\"module-statistiques\">";

echo "<div class=\"instructions\">";
echo "<p>";
echo _("Ce module permet de generer les documents necessaires aux statistiques sur la saisie.");
echo " "._("Ces traitements portant sur toutes les communes, les temps de chargement peuvent etre longs.");
echo "</p>";
echo "</div>";

// 
echo "<div class=\"#\">";
echo "<h3>";
echo _("Statistique sur la saisie");
echo "</h3>";
echo "<p>";
echo _("Nombre d'electeurs au dernier tableau, et nombre d'inscriptions, de modifications et de radiations par commune a la date de tableau en cours.");
echo "</p>";
echo "<p>";
echo "<a class=\"om-prev-icon pdf-16\" target=\"_blank\" href=\"../pdf/statistique_saisie.php\">";
echo _("Telecharger le fichier");
echo "</a>";
echo "</p>";
echo "</div>";
echo "<br/>";

// 
echo "<div class=\"#\">";
echo "<h3>";
echo _("Statistique sur la saisie par utilisateur");
echo "</h3>";
echo "<p>";
echo _("Nombre d'inscriptions, de modifications et de radiations par utilisateur a la date de tableau en cours.");
echo "</p>";
echo "<p>";
echo "<a class=\"om-prev-icon pdf-16\" target=\"_blank\" href=\"../pdf/statistique_saisie_utilisateur.php\">";
echo _("Telecharger le fichier");
echo "</a>";
echo "</p>";
echo "</div>";
echo "<br/>";

// 
echo "<div class=\"#\">";
echo "<h3>";
echo _("Statistique sur les inscriptions par sexe");
echo "</h3>";
echo "<p>";
echo _("Nombre d'inscriptions d'office, d'inscriptions par sexe a la date de tableau en cours.");
echo "</p>";
echo "<p>";
echo "<a class=\"om-prev-icon pdf-16\" target=\"_blank\" href=\"../pdf/statistique_inscription.php\">";
echo _("Telecharger le fichier");
echo "</a>";
echo "</p>";
echo "</div>";
echo "<br/>";

// 
echo "<div class=\"#\">";
echo "<h3>";
echo _("Statistiques generales des mouvements");
echo "</h3>";
echo "<p>";
echo _("Nombre d'inscriptions, de modifications et de radiations par collectivite a la date de tableau en cours.");
echo "</p>";
echo "<p>";
echo "<a class=\"om-prev-icon pdf-16\" target=\"_blank\" href=\"../pdf/commissionstat.php\">";
echo _("Telecharger le fichier");
echo "</a>";
echo "</p>";
echo "</div>";
echo "<br/>";

echo "</div>";
?>
<?php
/**
 *
 *
 * @package openmairie_exemple
 * @version SVN : $Id: txtab.php 137 2010-10-08 12:45:00Z fmichon $
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");

/**
 *
 */
// Nom de l'objet metier
(isset($_GET['obj']) ? $obj = $_GET['obj'] : $obj = "");

/**
 * Verification des parametres
 */
$objs_valid = array("etat", "lettretype", "sousetat");
if (!in_array($obj, $objs_valid)) {
    $class = "error";
    $message = _("L'objet est invalide.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}

/**
 *
 */
//
$f->setTitle(_("Parametrage")." -> ".$obj);
$f->setIcon("ico_parametrage.png");
//
$f->setRight($obj);
$f->isAuthorized();
//
$f->setFlag(NULL);
$f->display();

/**
 *
 */
$description = _("Tous les etats, sous etats et lettres types generiques de ".
                 "l'application peuvent etre geres ici. Vous pouvez ajouter, ".
                 "modifier ou supprimer ces editions grace aux actions ".
                 "disponibles ci-dessous. Attention lors de la suppression ".
                 "veillez de bien supprimer les liens correspondants dans ".
                 "l'application pour ne pas obtenir de liens casses.");
$f->displayDescription($description);

/**
 *
 */
//
$dir = getcwd();
$dir = substr($dir, 0, strlen($dir) - 4)."/sql/".$f->phptype."/";
$dossier = opendir($dir);
$tab = array();
while ($entree = readdir($dossier)) {
    if ($entree == "." || $entree == "..") {
        continue;
    }
    $temp = explode(".", $entree);
    if (!isset($temp[1])) {
        $temp[1] = "";
    }
    if ($temp[1] == "inc") {
        $temp[1] = "tab";
    }
    if ($temp[1] == $obj) {
        array_push($tab, array('file' => $temp[0]));
    }
}
closedir($dossier);
asort($tab);

/**
 * Formulaire d'ajout
 */
//
echo "\n<div id=\"txtab-form-add\">\n";
//
echo "<form action=\"../app/txform.php?obj=".$obj."&amp;maj=0\" method=\"post\" id=\"f1\" name=\"f1\">\n";
echo "<input type='text' name='idx' value='' class='champFormulaire' />";
echo "<input type='submit' name='s1' value=\""._("Ajouter")."&nbsp;'"._($obj)."'\" class=\"boutonFormulaire\" />";
echo "</form>\n";
//
echo "</div>\n";

/**
 * Liste avec modification et suppression
 */
//
echo "\n<div id=\"txtab\">\n";
//
echo "<fieldset class=\"cadre ui-corner-all ui-widget-content\">\n";
//
echo "\t<legend class=\"ui-corner-all ui-widget-content ui-state-active\">";
echo _($obj);
echo "</legend>\n";
//
echo "\t<div class=\"list\">\n";
if (count($tab) == 0) {
    echo "<p>";
    echo _("Il n'y a aucun element de ce type dans l'application.");
    echo "</p>";
}
foreach ($tab as $elem) {
    //
    echo "<div class=\"choice ui-corner-all ui-widget-content\">\n";
    //
    echo "<span>\n";
    //
    echo "<a href=\"../app/txform.php?obj=".$obj."&amp;idx=".$elem['file']."&amp;maj=1\">";
    echo "<span class=\"om-icon om-icon-16 om-icon-fix edit-16\" title=\""._("Modifier")."\">"._("Modifier")."</span>";
    echo "</a>\n";
    //
    echo "&nbsp;\n";
    //
    echo "<a href=\"../app/txform.php?obj=".$obj."&amp;idx=".$elem['file']."&amp;maj=1\">";
    echo $elem['file'];
    echo "</a>\n";
    //
    echo "</span>\n";
    //
    echo "</div>\n";
}
echo "\t</div>\n";
//
echo "</fieldset>\n";
//
echo "</div>\n";

?>
<?php
/**
 *
 */

/**
 *
 */
require_once("../obj/utils.class.php");


// variable $obj
$obj = "file";

// new utils
$f = new utils ("nohtml", NULL, $obj);
$f->disableLog();

//// Fonction file_mime_content_type (au cas ou non existant)
if(!function_exists('file_mime_content_type')) {

    function file_mime_content_type($filename) {

        $mime_types = array(

            'txt' => 'text/plain',
            'csv' => 'text/comma-separated-values',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }
}



//
$file = "";
if (isset ($_GET['fic'])) {
    $file = $_GET['fic'];
    $fic = explode ("/", $_GET['fic']);
    $file = array_pop($fic);
}

if ($file == "") {
    $f->notExistsError(_("Les parametres ne sont pas corrects."));
}

//
$folder = "tmp";
if (isset ($_GET['folder'])) {
    $folder = $_GET['folder'];
}

if ($folder != "tmp" and $folder != "pdf" and $folder != "trs") {
    $f->notExistsError(_("Les parametres ne sont pas corrects."));
}

$folder = $f->getParameter($folder."dir");

if (file_exists ($folder.$file)) {
    //
    $mime = file_mime_content_type ($folder.$file);
    //
    header ('Content-type: '.$mime.'');
    header ('Content-Length: '.filesize($folder.$file));
    if ($mime == "'text/comma-separated-values'") {
        header ('Content-Disposition: attachment; filename="'.$file.'"');
    } else {
        header ('Content-Disposition: inline; filename="'.$file.'"');
    }
    header ('Cache-Control: private, max-age=0, must-revalidate'); 
    header ('Pragma: public');
    //
    ini_set ('zlib.output_compression','0'); 
    
    readfile ($folder.$file);    
} else {
    $f->notExistsError(_("Le fichier n'existe pas."));
}

?>
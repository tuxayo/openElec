<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");

/**
 * Initialisation des variables
 */
// Nom de l'objet metier
(isset($_GET['obj']) ? $obj = $_GET['obj'] : $obj = "");
if ($obj == "") {
    (isset($_GET['origin']) ? $obj = $_GET['origin'] : $obj = "");
}
// Premier enregistrement a afficher
(isset($_GET['premier']) ? $premier = $_GET['premier'] : $premier = 0);
// Colonne choisie pour le tri
(isset($_GET['tricol']) ? $tricol = $_GET['tricol'] : $tricol = "");

// Chaine recherchee
if (isset($_POST['recherche'])) {
    $recherche = $_POST['recherche'];
    if (get_magic_quotes_gpc()) {
        $recherche1 = StripSlashes($recherche);
    } else {
        $recherche1 = $recherche;
    }
} else {
    if (isset($_GET['recherche'])) {
        $recherche = $_GET['recherche'];
        if (get_magic_quotes_gpc()) {
            $recherche1 = StripSlashes($recherche);
        } else {
            $recherche1 = $recherche;
        }
    } else {
        $recherche = "";
        $recherche1 = "";
    }
}
// Colonne choisie pour la selection
if (isset($_POST['selectioncol'])) {
	$selectioncol = $_POST['selectioncol'];
} else {
	if (isset($_GET['selectioncol'])) {
    	$selectioncol = $_GET['selectioncol'];
	} else {
    	$selectioncol = "";
	}
}
// ???
$ico = "";
// ???
$hiddenid = 0;

/**
 * Verification des parametres
 */
if (strpos($obj, "/") !== false
    or !file_exists("../sql/".$f->phptype."/".$obj.".inc")) {
    $class = "error";
    $message = _("L'objet est invalide.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}

/**
 *
 */
if ($f->isAccredited($obj)) {
    if (!isset($href) or $href == array()) {
        $href[0] = array(
            "lien" => "../scr/form.php?obj=".$obj."&amp;tricol=".$tricol,
            "id" => "",
            "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix add-16\" title=\""._("Ajouter")."\">"._("Ajouter")."</span>",
        );
        $href[1] = array(
            "lien" => "./scr/form.php?obj=".$obj."&amp;tricol=".$tricol."&amp;idx=",
            "id" => "&amp;premier=".$premier."&amp;recherche=".$recherche1."&amp;selectioncol=".$selectioncol,
            "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix edit-16\" title=\""._("Modifier")."\">"._("Modifier")."</span>",
        );
        $href[2] = array(
            "lien" => "./scr/form.php?obj=".$obj."&amp;tricol=".$tricol."&amp;idx=",
            "id" => "&amp;ids=1&amp;premier=".$premier."&amp;recherche=".$recherche1."&amp;selectioncol=".$selectioncol,
            "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix delete-16\" title=\""._("Supprimer")."\">"._("Supprimer")."</span>",
        );
    }
} else {
    $href[0] = array("lien" => "#", "id" => "", "lib" => "", );
    $href[1] = array("lien" => "", "id" => "", "lib" => "", );
    $href[2] = array("lien" => "#", "id" => "", "lib" => "", );
}

/**
 *
 */
//
(isset($_GET['nom']) ? $nom = $_GET['nom'] : $nom = "");
(isset($_GET['prenom']) ? $prenom = $_GET['prenom'] : $prenom = "");
(isset($_GET['marital']) ? $marital = $_GET['marital'] : $marital = "");
(isset($_GET['datenaissance']) ? $datenaissance = $_GET['datenaissance'] : $datenaissance = "");
(isset($_GET['exact']) && $_GET['exact'] == true ? $exact = true : $exact = false);

//
if ($datenaissance != "")
{
    if (preg_match ("#([0-9]{4})-([0-9]{2})-([0-9]{2})#", $datenaissance, $regs)) 
    {
        $datenaissanceFR = $regs[3]."/".$regs[2]."/".$regs[1];
    }
    elseif (preg_match ("#([0-9]{2})/([0-9]{2})/([0-9]{4})#", $datenaissance, $regs)) 
    {
        $datenaissanceFR = $datenaissance;
        $datenaissance = $regs[3]."-".$regs[2]."-".$regs[1];
    }
}

//
$id_electeur = 0;
if (isset ($_POST ['id_electeur']))
    $id_electeur = $_POST ['id_electeur'];

/**
 *
 */
require_once "../sql/".$f->phptype."/".$obj.".inc";

/**
 *
 */
//
$f->setRight($obj);
$f->isAuthorized();
//
$f->setTitle($ent);
$f->setIcon($ico);
$f->setHelp($obj);
//
$f->setFlag(NULL);
$f->display();



/**
 *
 */
//
echo "<div id=\"formulaire\">\n\n";
//
require_once PATH_OPENMAIRIE."om_table.class.php";
//
echo "<ul>\n";
echo "\t<li><a href=\"#tabs-1\">";
echo "<span class=\"om-icon ui-icon ui-icon-search\"><!-- --></span>";
echo _("Resultats de recherche d'electeur");
echo "</a></li>\n";
echo "</ul>\n";    
//
echo "\n<div id=\"tabs-1\">\n";
//
if ($edition != "") {
    $edition = "../pdf/pdf.php?obj=".$edition;
}
//
if (!isset($options)) {
    $options = array();
}
//
$tb = new table("../app/electeur.php", $table, $serie, $champAffiche, $champRecherche, $tri, $selection, $edition, $options);
//
$params = array(
    "obj" => $obj,
    "premier" => $premier,
    "recherche" => $recherche,
    "selectioncol" => $selectioncol,
    "tricol" => $tricol,
    "nom" => $nom,
    "prenom" => $prenom,
    "marital" => $marital,
    "datenaissance" => $datenaissance,
);
if ($exact == true) {
    $params["exact"] = 1;
}
//
$tb->display($params, $href, $f->db, "tab", false);
//
$params = "&amp;nom=".urlencode($nom);
$params .= ($exact == true ? "&amp;exact=".$exact : "");
$params .= "&amp;prenom=".urlencode($prenom);
$params .= "&amp;marital=".urlencode($marital);
$params .= "&amp;datenaissance=".urlencode($datenaissance);
//
echo "\t<div class=\"formControls\">\n";
echo "<a class=\"retour\" title=\""._("Retour")."\" ";
echo "href=\"../app/electeur.search.php?obj=".$obj.$params."\">";
echo _("Retour");
echo "</a>";
echo "</div>\n";
//
echo "</div>\n";
//
echo "\n</div>\n";

?>
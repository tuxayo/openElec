<?php
/**
 * Ce fichier permet de ...
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"decoupage_initialisation",
               _("Traitement")." -> "._("Redecoupage electoral"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Decoupage electoral");
$f->displaySubTitle($subtitle);
$description = _("L'edition suivante permet de visualiser le decoupage ".
                 "electoral parametre dans le logiciel. Il est compose de la ".
                 "liste des voies qui sont associees a un bureau de vote en ".
                 "fonction des numeros d'habitation.");
$f->displayDescription($description);
//
$links = array(
    "0" => array(
        "href" => "../pdf/pdf.php?obj=decoupage",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Parametrage du decoupage electoral"),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Aide a la saisie");
$f->displaySubTitle($subtitle);
$description = _("L'edition suivante permet de visualiser la liste de toutes ".
                 "les voies qui n'ont pas de parametrage dans le decoupage. ".
                 "Elle presente la liste des voies separees par bureau de vote ".
                 "avec le nombre d'electeurs associe à ce bureau et cette voie.");
$f->displayDescription($description);
//
$links = array(
    "0" => array(
        "href" => "../pdf/pdf.php?obj=decoupage_manquant",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Liste des voies qui ne sont pas parametrees dans le decoupage"),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Normalisation des voies");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.decoupage_normalisation_voies.class.php";
$trt = new decoupageNormalisationVoiesTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Initialisation des electeurs en bureau force");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.decoupage_init_bureauforce.class.php";
$trt = new decoupageInitBureauForceTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Initialisation de la table decoupage");
$f->displaySubTitle($subtitle);
//
require_once "../obj/traitement.decoupage_init_decoupage.class.php";
$trt = new decoupageInitDecoupageTraitement($f);
$trt->displayForm();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";


?>

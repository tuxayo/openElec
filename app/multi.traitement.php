<?php
/**
 * Ce fichier permet ...
 *
 * @package openelec
 * @version SVN : $Id$
 */


/**
 *
 */
require_once("../obj/utils.class.php");

// variable $obj
$obj = "traitement_multi";

// new utils
$f = new utils (NULL, $obj, _("Traitement -> Multi Collectivites"));

//
set_time_limit (1800);

/**
 * GESTION DES TABS
 */
//
$tab = 0;
if (isset ($_GET['tab']))
    $tab = $_GET['tab'];
// Boutons RETOUR
if (isset ($_POST['return'])) {
    if ($_POST['return'] == 1) {
        $tab = 0;
    }
    if ($_POST['return'] == 2) {
        $tab = 1;
    }
    if ($_POST['return'] == 3) {
        $tab = 2;
    }
}
// Boutons ETAPE SUIVANTE
if (isset ($_POST['trtmulti_form_0_action_valid'])) {
    if (!isset ($_POST ['collectivites'])) {
        $tab = 0;
    } else {
        $tab = 1;
    }
}
if (isset ($_POST['trtmulti_form_1_action_valid'])) {
    $tab = 2;
}
if (isset ($_POST['trtmulti_form_2_action_valid'])) {
    $tab = 3;
}

/**
 * JAVASCRIPT
 */
?>
<script type="text/javascript">
$(document).ready(function(){
    $('#formulaire').tabs('select', <?php echo $tab; ?>);
    $('#formulaire').tabs('option', 'disabled', [0, 1, 2, 3]);
    $("a#unselectall").click(function(){
  	// make the element selectable again
  	$("#collectivites").removeAttr("disabled");
  	// deselect all options
  	$("#collectivites").each(function(){
            $("#collectivites option").removeAttr("selected");
	});
    });
    $('a#selectall').click(function(){
        $('#collectivites').each(function(){
            $('#collectivites option').attr('selected','selected')
        });
    });
});
</script>
<?php

/**
 * ONGLETS
 */
// start #tabs
echo "<div id=\"formulaire\">\n";

// onglets
echo "\n<ul>\n";
echo "\t<li><a href=\"#tabs-0\">"._("Etape 1")."</a></li>\n";
echo "\t<li><a href=\"#tabs-1\">"._("Etape 2")."</a></li>\n";
echo "\t<li><a href=\"#tabs-2\">"._("Etape 3")."</a></li>\n";
echo "\t<li><a href=\"#tabs-3\">"._("Etape 4")."</a></li>\n";
echo "</ul>\n";

//
$selection_traitement = (isset($_POST["selection_traitement"]) ? $_POST["selection_traitement"] : "");
$selection_traitement_explode = explode("_", $selection_traitement);
$selection_traitement_datetableau = (isset($selection_traitement_explode[0]) ? $selection_traitement_explode[0] : "");
$selection_traitement_datej5 = (isset($selection_traitement_explode[2]) ? $selection_traitement_explode[2] : "");
$selection_traitement_traitement = (isset($selection_traitement_explode[1]) ? $selection_traitement_explode[1] : "");

/**
 * TRAITEMENTS
 */
$traitements = array (
    "annuel" => array (
    "type" => "trt",
    "name" => "Traitement annuel",
    "selected" => false),
    //
    "j5" => array(
        "type" => "trt",
        "name" => "Traitement J-5",
        "params" => array(
            "mouvementatraiter" => (isset($_POST["mouvementatraiter"]) ? $_POST["mouvementatraiter"] : ""),
        ),
        "confirmation" => "mouvementatraiter",
        "selected" => false,
    ),
    //
    "archivage" => array (
	"type" => "trt",
	"name" => "Archivage",
	"selected" => false),
    "refonte" => array (
	"type" => "trt",
	"name" => "Refonte",
	"selected" => false),
    //
    "election_mention" => array(
        "type" => "trt",
        "class" => "electionMentionTraitement",
        "name" => "Election Mentions",
        "confirmation" => "election_mention",
        "selected" => false,
    ),
    //
    "listegenerale" => array (
	"type" => "pdf",
	"name" => "Liste generale",
	"url" => "../pdf/listegenerale.php",
	"selected" => false),
    //
    "etiquetteelecteur" => array (
        "type" => "pdf",
        "get" => array(
            "obj" => "electeur",
        ),
        "name" => "Etiquettes",
        "url" => "../app/pdfetiquette.php",
        "selected" => false,),
    //
    "etiquetteadditions" => array (
        "type" => "pdf",
    	"name" => "Etiquettes Jeunes",
        "get" => array(
            "obj" => "additions_des_jeunes",
            "type" => (isset($_POST["mouvementatraiter"]) ? $_POST["mouvementatraiter"] : ""),
        ),
    	"url" => "../app/pdfetiquette.php",
    	"selected" => false,
        "confirmation" => "additions_des_jeunes",
    ),
    "emargement" => array (
	"type" => "pdf",
	"name" => "Liste d'emargement",
	"url" => "../pdf/listeemargement.php",
	"bureau" => true,
	"selected" => false),
    //
    "carteelecteur" => array(
        "type" => "pdf",
        "name" => "Cartes electorales",
        "url" => "../pdf/carteelecteur.php",
        "get" => array(
            "mode_edition" => "commune",
            "multi" => true,
            "output" => "string",
        ),
        "selected" => false,
    ),
    //
    "nouvellecarteelecteur" => array (
        "type" => "pdf",
        "name" => "Nouvelles cartes electorales",
        "url" => "../pdf/carteelecteur.php",
        "get" => array(
            "mode_edition" => "traitement",
            "multi" => true,
            "output" => "string",
            "traitement" => $selection_traitement_traitement,
            "datetableau" => $selection_traitement_datetableau,
            "datej5" => $selection_traitement_datej5,
        ),
        "selected" => false,
        "confirmation" => "selection_traitement",
    ),
    //
    "commission" => array (
        "type" => "pdf",
        "name" => "Commission par bureau",
        "url" => "../pdf/multi_commission.php",
        "selected" => false,),
    //
    "tableau-commune" => array (
        "type" => "pdf",
        "name" => "Tableau rectificatif (commune)",
        "url" => "../pdf/commission.php",
        "get" => array("mode_edition" => "tableau",
                       "tableau" => "tableau_rectificatif",
                       "globale" => "true",
                       ),
        "selected" => false,),
    //
    "tableau-bureau" => array (
        "type" => "pdf",
        "name" => "Tableau rectificatif (bureau)",
        "url" => "../pdf/commission.php",
        "get" => array("mode_edition" => "tableau",
                       "tableau" => "tableau_rectificatif",
                       ),
        "selected" => false,),
    //
    "fichenavette" => array (
	"type" => "txt",
	"name" => "Fiches navettes",
	"url" => "export.fichenavette.php",
	"selected" => false,
	"confirmation" => "2dates"),
    "email" => array (
	"type" => "email",
	"name" => "Envoi de courriels",
	"selected" => false,
    "confirmation" => "email"),
    );
$optgroup_trt = array("annuel", "j5", "archivage", "refonte", "election_mention");
$optgroup_pdf = array("listegenerale", "etiquetteelecteur", "etiquetteadditions", "carteelecteur", "nouvellecarteelecteur", "emargement", "commission", "tableau-commune", "tableau-bureau");
$optgroup_misc = array("fichenavette", "email");


/**
 * COLLECTIVITES
 */
//
$query_collectivites = "select id, ville from collectivite where type_interface=0 order by ville";
$res_collectivites = $f->db->query ($query_collectivites);
$f->isDatabaseError($res_collectivites);
$collectivites = array();
while ($row =& $res_collectivites->fetchrow(DB_FETCHMODE_ASSOC)) {
    $collectivites [$row['id']] = array('ville' => $row['ville'], 'selected' => false);
}

/**
 *
 */
// start .formulaire
echo "<div id=\"formulaire\">\n";
echo "<form method=\"post\" id=\"trtmulti_form\" name=\"trtmulti\" action=\"../app/multi.traitement.php\">\n";
echo "\t\t<input name=\"return\" type=\"hidden\" />\n";

/**
 * ETAPE 1
 */
// start #tabs-0
echo "<div id=\"tabs-0\">";
echo "<h2>"._("Etape 1: Choix de la ou des collectivite(s)")."</h2>\n";
//
if (isset ($_POST['trtmulti_form_0_action_valid'])) {
    if (!isset ($_POST ['collectivites'])) {
        $f->displayMessage ("erreur", _("Il faut selectionner au moins une collectivite."));
    }
}
//
$tabindex = 1;
// SELECT COLLECTIVITES
echo "\n<div class=\"field\">\n";
echo "\t<label for=\"collectivites\">"._("Collectivites")." - ";
echo "<a id=\"selectall\">"._("Tout selectionner")."</a> - ";
echo "<a id=\"unselectall\">"._("Tout deselectionner")."</a>";
echo "</label>\n";
if (isset ($_POST['collectivites'])) {
    foreach ($_POST['collectivites'] as $selected) {
        $collectivites[$selected]['selected'] = true;
    }
}
echo "<select id=\"collectivites\" name=\"collectivites[]\" multiple=\"multiple\" size=\"15\">\n";
foreach ($collectivites as $key => $collectivite) {
    echo "<option ".($collectivite['selected']==true?"selected=\"selected\"":"")." value=\"".$key."\">";
    echo $collectivite['ville'];
    echo "</option>";
}
echo "</select>\n";
echo "\n</div>\n";

// ETAPE SUIVANTE - RETOUR
echo "\t<div class=\"formControls\">\n";
echo "\t\t<input name=\"trtmulti_form_0.action.valid\" tabindex=\"".$tabindex++."\" value=\""._("Etape suivante")."\" type=\"submit\" class=\"boutonFormulaire context\" />\n";
echo "\t\t<a class=\"retour\" title=\""._("Retour")."\" ";
echo "href=\"../scr/dashboard.php\">";
echo _("Retour");
echo "</a>\n";
echo "\t</div>\n";
// end #tabs-0
echo "</div>\n";

/**
 * ETAPE 2
 */
// start #tabs-1
echo "<div id=\"tabs-1\">";
echo "<h2>"._("Etape 2: Choix du traitement")."</h2>\n";
//
$tabindex = 1;
// SELECT TRAITEMENT
echo "\n<div class=\"field\">\n";
echo "\t<label for=\"traitement\">"._("Traitements")."</label>\n";
if (isset ($_POST['traitement'])) {
    $traitements[$_POST['traitement']]['selected'] = true;
}
echo "<select name=\"traitement\">\n";
echo "<optgroup label=\"Traitements\">";
foreach ($optgroup_trt as $trt) {
    echo "<option ".($traitements[$trt]['selected']==true?"selected=\"selected\"":"")." value=\"".$trt."\">";
    echo $traitements[$trt]['name'];
    echo "</option>";   
}
echo "</optgroup>";
echo "<optgroup label=\"Editions\">";
foreach ($optgroup_pdf as $pdf) {
    echo "<option ".($traitements[$pdf]['selected']==true?"selected=\"selected\"":"")." value=\"".$pdf."\">";
    echo $traitements[$pdf]['name'];
    echo "</option>";
}
echo "</optgroup>";
echo "<optgroup label=\"Divers\">";
foreach ($optgroup_misc as $misc) {
    echo "<option ".($traitements[$misc]['selected']==true?"selected=\"selected\"":"")." value=\"".$misc."\">";
    echo $traitements[$misc]['name'];
    echo "</option>";
}
echo "</optgroup>";
/*foreach ($traitements as $key => $traitement) {
    echo "<option ".($traitement['selected']==true?"selected=\"selected\"":"")." value=\"".$key."\">";
    echo $traitement['name'];
    echo "</option>";
}*/
echo "</select>\n";
echo "\n</div>\n";
// ETAPE SUIVANTE - RETOUR
echo "\t<div class=\"formControls\">\n";
echo "\t\t<input name=\"trtmulti_form_1.action.valid\" tabindex=\"".$tabindex++."\" value=\""._("Etape suivante")."\" type=\"submit\" class=\"boutonFormulaire context\" />\n";
echo "\t\t<a class=\"retour\" title=\""._("Retour")."\" onclick=\"window.document.trtmulti.return.value='1';window.document.trtmulti.submit()\" ";
echo "href=\"#\">";
echo _("Retour");
echo "</a>\n";
echo "\t</div>\n";
// end #tabs-1
echo "</div>\n";

/**
 * ETAPE 3
 */
// start #tabs-2
echo "<div id=\"tabs-2\">";
echo "<h2>"._("Etape 3: Confirmation")."</h2>\n";
if (isset($traitements[$_POST['traitement']]['confirmation'])) {
    
    if ($traitements[$_POST['traitement']]['confirmation'] == "2dates") {
	
	$form_entre2dates = " "._("Date debut");
	$form_entre2dates .= " <input type=\"text\" name=\"datedebut\" tabindex=\"1\" size=\"15\" class=\"datepicker champFormulaire\" id=\"datedebut\" onchange=\"fdate(this)\" ";
	$form_entre2dates .= (isset($_POST['datedebut'])?"value=\"".$_POST['datedebut']."\" ":"");
	$form_entre2dates .= "/>";
	$form_entre2dates .= " "._("Date fin");
	$form_entre2dates .= " <input type=\"text\" name=\"datefin\" tabindex=\"2\" size=\"15\" class=\"datepicker champFormulaire\" id=\"datefin\" onchange=\"fdate(this)\" ";
	$form_entre2dates .= (isset($_POST['datefin'])?"value=\"".$_POST['datefin']."\" ":"");
	$form_entre2dates .= "/>";
	
	echo $form_entre2dates;
    } elseif ($traitements[$_POST['traitement']]['confirmation'] == "email") {
        //
        (defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
        require_once PATH_OPENMAIRIE."formulairedyn.class.php";
        //
        $validation = 0;
        $maj = 0;
        $champs = array("object", "message");
        //
        $form = new formulaire(NULL, $validation, $maj, $champs);
        //
        $form->setLib("object", _("Objet"));
        $form->setType("object", "text");
        $form->setTaille("object", 100);
        $form->setMax("object", 100);
        $form->setVal("object", (isset($_POST['object'])?$_POST['object']:""));
        //
        $form->setLib("message", _("Message"));
        $form->setType("message", "textarea");
        $form->setTaille("message", 100);
        $form->setMax("message", 20);
        $form->setVal("message", (isset($_POST['message'])?$_POST['message']:""));
        //
        $form->entete();
        $form->afficher($champs, $validation, false, false);
        $form->enpied();
    } elseif ($traitements[$_POST['traitement']]['confirmation'] == "additions_des_jeunes") {
        //
        (defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
        require_once PATH_OPENMAIRIE."formulairedyn.class.php";
        //
        $validation = 0;
        $maj = 0;
        $champs = array("mouvementatraiter");
        //
        $form = new formulaire(NULL, 0, 0, $champs);
        //
        $form->setLib("mouvementatraiter", _("Selectionner ici le tableau dont vous souhaitez imprimer les etiquettes"));
        $form->setType("mouvementatraiter", "select");
        //
        $mouvements_io = "select code, libelle from param_mouvement ";
        $mouvements_io .= " where effet='Election' ";
        $mouvements_io .= " group by code, libelle ";
        $mouvements_io .= " order by code ";
        //
        $res = $f->db->query($mouvements_io);
        //
        $f->addToLog("trt/traitement_j5.php: db->query(\"".$mouvements_io."\")", EXTRA_VERBOSE_MODE);
        //
        $f->isDatabaseError($res);
        //
        $contenu = array();
        $contenu[0] = array();
        $contenu[1] = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            array_push($contenu[0], $row["code"]);
            array_push($contenu[1], _("Tableau des additions")." - ".$row["libelle"]);
        }
        $form->setSelect("mouvementatraiter", $contenu);
        //
        
        $additions = array();
        if (isset($_POST["mouvementatraiter"])) {
            $form->setVal("mouvementatraiter", $_POST["mouvementatraiter"]);
        }
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
    } elseif ($traitements[$_POST['traitement']]['confirmation'] == "selection_traitement") {
        //
        (defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
        require_once PATH_OPENMAIRIE."formulairedyn.class.php";
        //
        $validation = 0;
        $maj = 0;
        $champs = array("selection_traitement");
        //
        $form = new formulaire(NULL, 0, 0, $champs);
        //
        $form->setLib("selection_traitement", _("Selectionner ici le traitement dont vous souhaitez imprimer les nouvelles cartes electorales"));
        $form->setType("selection_traitement", "select");
        //
        $contenu = array();
        $contenu[0] = array();
        $contenu[1] = array();
        //
        $query_distinct_traitement = "select date_tableau, tableau, date_j5, etat from mouvement group by date_tableau, tableau, date_j5, etat order by date_tableau DESC";
        $res_distinct_traitement = $f->db->query($query_distinct_traitement);
        $f->addToLog("app/multi.traitement.php: db->query(\"".$query_distinct_traitement."\")", EXTRA_VERBOSE_MODE);
        $f->isDatabaseError($res_distinct_traitement);
        $distinct_traitement = array();
        while ($row_distinct_traitement =& $res_distinct_traitement->fetchrow(DB_FETCHMODE_ASSOC)) {
            if (!isset($distinct_traitement[$row_distinct_traitement["date_tableau"]])) {
                //
                $distinct_traitement[$row_distinct_traitement["date_tableau"]] = array("j5" => array(), "annuel" => NULL);
            }
            if ($row_distinct_traitement["etat"] == "actif") {
                ///
                $distinct_traitement[$row_distinct_traitement["date_tableau"]]["annuel"] = "NO";
            } else {
                if ($row_distinct_traitement["tableau"] == "annuel" && $distinct_traitement[$row_distinct_traitement["date_tableau"]]["annuel"] != "NO") {
                    $distinct_traitement[$row_distinct_traitement["date_tableau"]]["annuel"] = "YES";
                } elseif ($row_distinct_traitement["tableau"] == "j5") {
                    array_push($distinct_traitement[$row_distinct_traitement["date_tableau"]]["j5"], $row_distinct_traitement["date_j5"]);
                }
            }
        }
        //
        foreach($distinct_traitement as $key => $elem) {
            //
            if ($elem["annuel"] == "YES") {
                array_push($contenu[0], $key."_annuel");
                array_push($contenu[1], "Date de Tableau : ".$f->formatdate($key)." / Traitement ANNUEL");
            }
            //
            foreach ($elem["j5"] as $elem_j5) {
                array_push($contenu[0], $key."_j5_".$elem_j5);
                array_push($contenu[1], "Date de Tableau : ".$f->formatdate($key)." / Traitement J-5 du ".$f->formatdate($elem_j5));
            }
        }
        $form->setSelect("selection_traitement", $contenu);
        //
        
        $additions = array();
        if (isset($_POST["selection_traitement"])) {
            $form->setVal("selection_traitement", $_POST["selection_traitement"]);
        }
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
    } elseif ($traitements[$_POST['traitement']]['confirmation'] == "mouvementatraiter") {
        //
        (defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
        require_once PATH_OPENMAIRIE."formulairedyn.class.php";
        //
        $validation = 0;
        $maj = 0;
        $champs = array("mouvementatraiter");
        //
        $form = new formulaire(NULL, 0, 0, $champs);
        //
        $form->setLib("mouvementatraiter", _("Selectionner ici le ou les tableau(x) ".
                                               "qui entre(nt) en vigueur a la date de l'election"));
        $form->setType("mouvementatraiter", "checkbox_multiple");
        //
        $mouvements_io = "select code, libelle, effet from mouvement inner join param_mouvement ";
        $mouvements_io .= " on mouvement.types=param_mouvement.code ";
        $mouvements_io .= " where (effet='Election' or effet='Immediat') ";
        $mouvements_io .= " and date_tableau='".$f->collectivite["datetableau"]."' ";
        $mouvements_io .= " and etat='actif' ";
        $mouvements_io .= " group by code, libelle, effet ";
        $mouvements_io .= " order by effet DESC, code ";
        //
        $res = $f->db->query($mouvements_io);
        //
        $f->addToLog("trt/traitement_j5.php: db->query(\"".$mouvements_io."\")", EXTRA_VERBOSE_MODE);
        //
        $f->isDatabaseError($res);
        //
        $contenu = array();
        $contenu[0] = array("Immediat",);
        $contenu[1] = array(_("Tableau des cinq jours"),);
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $f->addToLog("trt/traitement_j5.php: ".print_r($row, true), EXTRA_VERBOSE_MODE);
            if ($row["effet"] == "Election") {
                array_push($contenu[0], $row["code"]);
                array_push($contenu[1], _("Tableau des additions")." - ".$row["libelle"]);
            }
        }
        $form->setSelect("mouvementatraiter", $contenu);
        //
        $cinqjours = false;
        $additions = array();
        $mouvementatraiter = "";
        if (isset($_POST["mouvementatraiter"])) {
            $val = "";
            foreach($_POST["mouvementatraiter"] as $elem) {
                if ($elem == "Immediat") {
                    $cinqjours = true;
                } else {
                    array_push($additions, $elem);
                }
                $val .= $elem.";";
            }
            $mouvementatraiter = $val;
            $form->setVal("mouvementatraiter", $val);
        }
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
    } elseif ($traitements[$_POST['traitement']]['confirmation'] == "election_mention") {
        //
        require_once "../obj/traitement.".$_POST['traitement'].".class.php";
        //
        if (isset($traitements[$_POST['traitement']]['class'])) {
            $traitement_class = $traitements[$_POST['traitement']]['class'];
        } else {
            $traitement_class = $_POST['traitement']."Traitement";
        }
        //
        $trt = new $traitement_class($f);
        $trt->displayFormSection(true);

    }
}

// ETAPE SUIVANTE - RETOUR
echo "\t<div class=\"formControls\">\n";
echo "\t\t<input name=\"trtmulti_form_2.action.valid\" tabindex=\"".$tabindex++."\" value=\""._("Confirmer le traitement")."\" type=\"submit\" class=\"boutonFormulaire context\" />\n";
echo "\t\t<a class=\"retour\" title=\""._("Retour")."\" name=\"retour\" value=\"test\" onclick=\"window.document.trtmulti.return.value='2';window.document.trtmulti.submit()\" ";
echo "href=\"#\">";
echo _("Retour");
echo "</a>\n";
echo "\t</div>\n";
// end #tabs-2
echo "</div>\n";

/**
 * ETAPE 4
 */
// start #tabs-3
echo "<div id=\"tabs-3\">";
echo "<h2>"._("Etape 4: Resultat")."</h2>\n";
//
if ($tab == 3) {
    //
    $collectivite_tmp = $f->collectivite['id'];
    $libelle_collectivite_tmp = $f->collectivite['ville'];
    
    // TRT TYPE
    if ($traitements[$_POST['traitement']]['type'] == "trt") {
        //
        require_once "../obj/traitement.".$_POST['traitement'].".class.php";
        //
        foreach ($_POST ['collectivites'] as $collectivite) {
            //
            $_SESSION['collectivite'] = $collectivite;
            $_SESSION['libelle_collectivite'] = $collectivites[$collectivite]['ville'];
            //
            if (isset($traitements[$_POST['traitement']]['class'])) {
                $traitement_class = $traitements[$_POST['traitement']]['class'];
            } else {
                $traitement_class = $_POST['traitement']."Traitement";
            }
            echo $traitement_class." - ".$_SESSION['collectivite']." - ".$_SESSION['libelle_collectivite']."<br/>";
            //
            $trt = new $traitement_class($f);
            if (isset($traitements[$_POST['traitement']]['params'])) {
                $trt->setParams($traitements[$_POST['traitement']]['params']);
            }
            $trt->execute();
        }
    }
    
    // PDF TYPE
    if ($traitements[$_POST['traitement']]['type'] == "pdf") {
        //
        // option_generation_as_an_archive
        //
        if ($f->vars['option_generation_as_an_archive'] == true) {
            //
            require_once("../app/php/misc/zipfile.class.php");
            $zip = new zipfile ();
            foreach ($_POST ['collectivites'] as $collectivite) {
                $_SESSION['collectivite'] = $collectivite;
                $_SESSION['libelle_collectivite'] = $collectivites[$collectivite]['ville'];
                $f->getCollectivite();
                //
                $traitement_class = $_POST['traitement']."Edition";
                //
                if (isset($traitements[$_POST['traitement']]['bureau'])) {
                    //
                    $sql_bureau = "select * from bureau where collectivite='".$_SESSION['collectivite']."'";
                    $res_bureau = $f->db->query ($sql_bureau);
                    $f->isDatabaseError($res_bureau);
                    while ($row_bureau =& $res_bureau->fetchrow(DB_FETCHMODE_ASSOC)) {
                        //
                        if (isset($traitements[$_POST['traitement']]['get'])) {
                            foreach($traitements[$_POST['traitement']]['get'] as $key => $value) {
                                $_GET[$key] = $value;
                            }
                        }
                        //
                        $_GET['multi'] = true;
                        $_GET['id'] = "bureau";
                        $_GET['idx'] = $row_bureau['code'];
                        //$_GET['obj'] = "electeur";
                        $_GET['code_bureau'] = $row_bureau['code'];
                        $_GET['mode'] = "chaine";
                        //
                        include ($traitements[$_POST['traitement']]['url']);
                        $file = explode ("/", $filename);
                        //
                        $zip->addfile ($contenu, $file[2]);
                    }
                    $res_bureau->free ();
                } else {
                    //
                    $traitement_class = $_POST['traitement']."Edition";
                    if (isset($traitements[$_POST['traitement']]['get'])) {
                        foreach($traitements[$_POST['traitement']]['get'] as $key => $value) {
                            $_GET[$key] = $value;
                        }
                    }
                    $_GET['multi'] = true;
                    //$_GET['obj'] = "electeur";
                    $_GET['mode'] = "chaine";
                    if (isset($traitements[$_POST['traitement']]['id'])) {
                        $_GET['id'] = $traitements[$_POST['traitement']]['id'];
                    }
                    include ($traitements[$_POST['traitement']]['url']);
                    $file = explode ("/", $filename);
                    //
                    $zip->addfile ($contenu, $file[2]);
                }
                
            }
            $archive = $zip->file ();
            $archive_filename = date("Ymd-Gis")."-".$_POST['traitement'].".zip";
            $open = fopen($f->getParameter("pdfdir").$archive_filename , "wb");
            fwrite($open, $archive);
            fclose($open);
            //
            $f->displayMessage("valid", _("Le traitement est termine."));
            //
            echo "\t<div class=\"formControls\">\n";
            echo "\t\t<a class=\"om-prev-icon zip-16\" target=\"_blank\" href=\"../app/file.php?fic=".$archive_filename."&folder=pdf\"> ";
            echo _("Telecharger l'archive resultat");
            echo "</a>\n";
            echo "</div>";
        } else {
            // GESTION DU MAIL
            //
            $collectivites = $_POST['collectivites'];
            //
            $sql = "select collectivite, email from utilisateur where ";
            foreach ($collectivites as $key => $collectivite) {
                $sql .= " collectivite='".$collectivite."' ";
                if ($key != count($collectivites) - 1) {
                    $sql .= " or ";
                }
            }
            $res = $f->db->query($sql);
            $f->isDatabaseError($res);
            $emails = array();
            while ($row=&$res->fetchrow(DB_FETCHMODE_ASSOC)) {
                if (!isset($emails[$row['collectivite']])) {
                    $emails[$row['collectivite']] = array();
                }
                array_push($emails[$row['collectivite']], $row['email']);
            }
            //
            $cpt = array("sended" => 0, "ok" => 0, "error" => 0);
            //
            
            
            //
            $message_start = _("<p>Bonjour, veuillez trouver ci-dessous le ou ".
                               "les liens vers la ou les editions nouvellement ".
                               "disponible(s) dans le logiciel openElec.</p>");
            $came_from = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on" ? "https://":"http://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            str_replace("multi.traitement.php", "", $came_from);
            $file_path = substr($came_from, 0, count($came_from)-21);
            $file_path = $file_path."file.php?folder=pdf&fic=";
            $message_end = _("<p>Votre administrateur openElec.</p>")."<br/>";
            //
            foreach ($_POST ['collectivites'] as $collectivite) {
                $_SESSION['collectivite'] = $collectivite;
                $_SESSION['libelle_collectivite'] = $collectivites[$collectivite]['ville'];
                $f->getCollectivite();
                //
                $traitement_class = $_POST['traitement']."Edition";
                //
                $links = "";
                //
                if (isset($traitements[$_POST['traitement']]['bureau'])) {
                    //
                    $sql_bureau = "select * from bureau where collectivite='".$_SESSION['collectivite']."'";
                    $res_bureau = $f->db->query ($sql_bureau);
                    $f->isDatabaseError($res_bureau);
                    while ($row_bureau =& $res_bureau->fetchrow(DB_FETCHMODE_ASSOC)) {
                        //
                        $_GET['mode'] = "save";
                        $_GET['id'] = "bureau";
                        $_GET['idx'] = $row_bureau['code'];
                        $_GET['obj'] = "electeur";
                        $_GET['code_bureau'] = $row_bureau['code'];
                        //
                        include ($traitements[$_POST['traitement']]['url']);
                        //
                        $links .= "<a href=\"".$file_path.$filename."\">".$file_path.$filename."</a><br/>";
                    }
                    $res_bureau->free ();
                } else {
                    //
                    $traitement_class = $_POST['traitement']."Edition";
                    $_GET['mode'] = "save";
                    $_GET['obj'] = "electeur";
                    if (isset($traitements[$_POST['traitement']]['id'])) {
                        $_GET['id'] = $traitements[$_POST['traitement']]['id'];
                    }
                    include ($traitements[$_POST['traitement']]['url']);
                    //
                    $links .= "<a href=\"".$file_path.$filename."\">".$file_path.$filename."</a><br/>";
                }
                //
                $message =  $message_start.$links.$message_end;
                $object = _("Nouvelle edition disponible dans openElec");
                $recipient = "";
                //
                foreach ($emails[$collectivite] as $email) {
                    $sended =false;
                    $sended = $f->sendMail($object, $message, $email);
                    $cpt["sended"]++;
                    if ($sended) {
                        $cpt["ok"]++;
                    } else {
                        $cpt["error"]++;
                    }
                }
            }
            //
            if ($cpt_error == 0) {
                //
                $f->displayMessage("valid", _("Le traitement est termine."));
            } else {
                //
                $f->displayMessage("error", _("Il y a eu des erreurs pendant l'envoi de mail. Contactez votre administrateur."));
            }
        }
    }
    
    
    // TXT TYPE
    if ($traitements[$_POST['traitement']]['type'] == "txt") {
	//
	$collectivites = $_POST['collectivites'];
	//
	include ("../trt/".$traitements[$_POST['traitement']]['url']);
	//
	if (isset($result_counter) and $result_counter != 0
	    and isset($inf) and $inf != false) {
	    //
	    $f->displayMessage("valid", _("Le traitement est termine."));
	    //
	    echo "\t<div class=\"formControls\">\n";
	    echo "\t\t<a class=\"om-prev-icon txt-16\" target=\"_blank\" href=\"../app/file.php?fic=".$fichier."&folder=tmp\"> ";
	    echo _("Telecharger le fichier resultat");
	    echo "</a>\n";
	    echo "</div>";
	}
    }

    // EMAIL TYPE
    if ($traitements[$_POST['traitement']]['type'] == "email") {
        //
        $collectivites = $_POST['collectivites'];
        //
        $sql = "select email from utilisateur where ";
        foreach ($collectivites as $key => $collectivite) {
            $sql .= " collectivite='".$collectivite."' ";
            if ($key != count($collectivites) - 1) {
                $sql .= " or ";
            }
        }
        $res = $f->db->query($sql);
        $f->isDatabaseError($res);
        $emails = array();
        while ($row=&$res->fetchrow(DB_FETCHMODE_ASSOC)) {
            array_push($emails, $row['email']);
        }
        //
        $cpt = array("sended" => 0, "ok" => 0, "error" => 0);
        //
        
        foreach ($emails as $email) {
            $sended =false;
            $sended = $f->sendMail($_POST['object'], $_POST['message'], $email);
            $cpt["sended"]++;
            if ($sended) {
                $cpt["ok"]++;
            } else {
                $cpt["error"]++;
            }
            
        }
        //
        if ($cpt_error == 0) {
            //
            $f->displayMessage("valid", _("Le traitement est termine."));
        } else {
            //
            $f->displayMessage("error", _("Il y a eu des erreurs pendant l'envoi. Contactez votre administrateur."));
        }
    }
    
    //
    $_SESSION['collectivite'] = $collectivite_tmp;
    $_SESSION['libelle_collectivite'] = $libelle_collectivite_tmp;
    $f->getCollectivite();
    
    //
    echo "\t<div class=\"formControls\">\n";
    echo "\t\t<a class=\"retour\" title=\""._("Retour")."\" name=\"retour\" value=\"test\" onclick=\"window.document.trtmulti.return.value='3';window.document.trtmulti.submit()\" ";
    echo "href=\"#\">";
    echo _("Retour");
    echo "</a>\n";
    echo "</div>";
}
// end #tabs-3
echo "</div>\n";

// end .formulaire
echo "</form>\n";
echo "</div>\n";

// end #tabs
echo "</div>";

?>
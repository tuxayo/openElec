<?php
/**
 * Ce fichier permet ...
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once("../obj/utils.class.php");

$f = new utils (NULL, "facturation_multi", _("Module facturation"));

echo "<div id=\"module-facturation\">";

//echo "<script type=\"text/javacript\">\n";
?>
<script type="text/javascript">

// <![CDATA[

    $(document).ready(function(){
	
	function ajax_call(id)
	{
            $("#"+id).html(msg_loading);
            $.ajax({
               type: "GET",
               url: "../trt/"+id+".php",
               dataType: "html",
               error:function(msg){
                 alert( "Error !: " + msg );
               },
               success:function(data){
                    $('#'+id).html('<div id="message"><p>'+data+'</p></div>');
            }});
	}
	
	$("#facturation-transfert-budget-link").click(
            function()
            {
                ajax_call("facturation-transfert-budget");	
            }
	);
	$("#facturation-publipostage-link").click(
            function()
            {
                ajax_call("facturation-publipostage");	
            }
	);
	
    });

// ]]>

</script>
<?php
//echo "</script>";

echo "<div class=\"instructions\">";
echo "<p>";
echo _("Ce module permet de generer les documents necessaires a la facturation des communes.");
echo " "._("Ces traitements portant sur toutes les communes, les temps de chargement peuvent etre longs.");
echo "</p>";
echo "</div>";

// Titres de recettes
echo "<div class=\"#\">";
echo "<h3>";
echo _("Titres de recettes");
echo "</h3>";
echo "<p>";
echo _("Ce fichier est destine a la division des finances. C'est un document PDF recapitulant le montant du par chaque commune ainsi que le total.");
echo "</p>";
echo "<p>";
echo "<a class=\"om-prev-icon pdf-16\" href=\"../pdf/facturation_titres_recettes.php\">";
echo _("Telecharger le fichier");
echo "</a>";
echo "</p>";
echo "</div>";
echo "<br/>";

// Transfert des informations budgetaires
echo "<div class=\"#\">";
echo "<h3>";
echo _("Transfert des informations budgetaires");
echo "</h3>";
echo "<p>";
echo _("Ce fichier est un fichier txt de transfert des informations budgetaires pour l'application astre.");
echo "</p>";
echo "<p>";
echo "<a class=\"om-prev-icon traitement-16\" id=\"facturation-transfert-budget-link\" href=\"#\">";
echo _("Generer le fichier");
echo "</a>";
echo "</p>";
echo "<span id=\"facturation-transfert-budget\" class=\"both\"></span>";
echo "</div>";
echo "<br/>";

// Publipostage
echo "<div class=\"#\">";
echo "<h3>";
echo _("Publipostage");
echo "</h3>";
echo "<p>";
echo _("Ce fichier est destine au publipostage pour la generation du courrier aux differentes communes.");
echo "</p>";
echo "<p>";
echo "<a class=\"om-prev-icon traitement-16\" id=\"facturation-publipostage-link\" href=\"#\">";
echo _("Generer le fichier");
echo "</a>";
echo "</p>";
echo "<span id=\"facturation-publipostage\" class=\"both\"></span>";
echo "</div>";

echo "</div>";
?>
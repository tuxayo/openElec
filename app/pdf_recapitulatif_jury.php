<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";
if (!isset($f)) { $f = new utils("nohtml"); }

/**
 *
 */
//


/**
 *
 */
//
$pdf_recapitulatif_traitement_output = "";
if (isset($_GET["output"])) {
    $pdf_recapitulatif_traitement_output = $_GET["output"];
}

/**
 *
 */
//
$_GET["output"] = "no";
//
require_once "../obj/pdf_common.class.php";
//
$pdf = new PDF("L", "mm", "A4");
//
$pdf->generic_document_header_left = $f->collectivite["ville"];
//
$generic_document_title = "";
$pdf->generic_document_title = $generic_document_title;
//
$pdf->SetMargins(10, 5, 10);
//
$pdf->AliasNbPages();
//
include "../sql/".$f->phptype."/trt_jury.inc";
//
$res = $f->db->query($query_nb_jures_canton);
$f->addToLog("pdf_recapitulatif_jury.php: db->query(\"".$query_nb_jures_canton."\");", VERBOSE_MODE);
database::isError($res);
//
while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
    //
    $_GET["canton"] = $row["canton"];
    $_GET["obj"] = "jury_liste_preparatoire";
    include "../app/pdffromdb.php";
}

/**
 *
 */
// Construction du nom du fichier
$filename = date("Ymd-His");
$filename .= "-jury";
$filename .= "-liste-preparatoire";
$filename .= "-".$_SESSION['collectivite'];
$filename .= "-".$f->normalizeString($_SESSION['libelle_collectivite']);
$filename .= ".pdf";
//
$output = $pdf_recapitulatif_traitement_output;
if (!in_array($output, array("string", "file", "download", "inline", "no"))) {
    $output = "inline"; // Valeur par defaut
}
//
if ($output == "string") {
    // S : renvoyer le document sous forme de chaine. name est ignore.
    $pdf_output = $pdf->Output("", "S");
} elseif ($output == "file") {
    // F : sauver dans un fichier local, avec le nom indique dans name
    // (peut inclure un repertoire).
    $pdf->Output($f->getParameter("pdfdir").$filename, "F");
} elseif ($output == "download") {
    // D : envoyer au navigateur en forcant le telechargement, avec le nom
    // indique dans name.
    $pdf->Output($filename, "D");
} elseif ($output == "inline") {
    // I : envoyer en inline au navigateur. Le plug-in est utilise s'il est
    // installe. Le nom indique dans name est utilise lorsque l'on selectionne
    // "Enregistrer sous" sur le lien generant le PDF.
    $pdf->Output($filename, "I");
} elseif ($output == "no") {
   
}

?>
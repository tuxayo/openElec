/**
 * Ce script javascript ...
 *
 * @package openelec
 * @version SVN : $Id$
 */

// Valeur initiale du bureau
var origine_codebureau = "-1";


function traces(fichier) {
    if (fenetreouverte == true) pfenetre.close ();
    pfenetre = window.open ("../app/file.php?fic="+fichier+"&folder=tmp", "Traces", "toolbar=no, scrollbars=yes, status=no, width=600, height=400, top=120, left=120");
    fenetreouverte = true;
}

function stats (fichier) {
    if (fenetreouverte == true) pfenetre.close ();
    pfenetre = window.open ("../trt/stats_"+fichier+".php", "Statistiques", "toolbar=no, scrollbars=yes, status=no, width=600, height=400, top=120, left=120");
    fenetreouverte = true;
}

function doublon1() {
    var nom=document.f1.nom.value;
    var prenom=document.f1.prenom.value;
    var marital=document.f1.nom_usage.value;
    var datenaissance=document.f1.date_naissance.value;
    if(fenetreouverte==true) pfenetre.close();
    pfenetre=window.open("../pdf/pdffromarray.php?obj=doublon1&nom="+nom+"&prenom="+prenom+"&marital="+marital+"&datenaissance="+datenaissance,"rechercheDoublon","toolbar=no,scrollbars=yes,status=no, width=640,height=400,top=120,left=120");
    fenetreouverte=true;
}

function entre2dates() {
    if (fenetreouverte==true) pfenetre.close();
    deb = document.f1.datedebut.value;
    fin = document.f1.datefin.value;
    if (deb == "" || fin == "")
        alert("Vous devez saisir une date de debut et une date de fin correctes!");
    else {
        d = deb.substring(0,2)+deb.substring(3,5)+deb.substring(6,10);
        f = fin.substring(0,2)+fin.substring(3,5)+fin.substring(6,10);
        pfenetre=window.open("../pdf/commission.php?mode_edition=commission&commission=entre_deux_dates&edition_avec_recapitulatif=false&first="+d+"&last="+f,"mouvement","toolbar=no,scrollbars=yes,status=no, width=650,height=600,top=80,left=80");
        fenetreouverte=true;
    }
}

/**
 * fonction de validation du formulaire d'édition des courriers
 * de convocation aux commissions
 */
function checkformconvocationcommission() {
    message = $("#courriercommission textarea[name=message]").val();
    // Si pas de message on empeche la validation du formulaire
    // puis en affiche un message d'erreur
    if (message == "") {
        alert("Vous devez saisir le message du courrier de convocation");
        return false;
    }
}

//
function recapitulatif_mention() {
    //
    if (fenetreouverte == true) {
        pfenetre.close();
    }
    //
    var dateelection1 = document.getElementById("traitement_election_mention_form").dateelection1;
    var dateelection2 = document.getElementById("traitement_election_mention_form").dateelection2;
    var centrevote = document.getElementById("traitement_election_mention_form").centrevote;
    var mairieeurope = document.getElementById("traitement_election_mention_form").mairieeurope;
    //
    pfenetre = window.open("../app/pdf_recapitulatif_mention.php?dateelection1="+dateelection1.value+"&dateelection2="+dateelection2.value+"&centrevote="+centrevote.checked+"&mairieeurope="+mairieeurope.checked,"statbureau","toolbar=no,scrollbars=yes,status=no, width=650,height=600,top=80,left=80"); 
    //
    fenetreouverte = true;
}

//
function epuration_procuration_stats(obj) {
    //
    if (fenetreouverte == true) {
        pfenetre.close();
    }
    //
    var dateelection = document.getElementById("traitement_election_epuration_procuration_form").dateelection;
    //
    pfenetre=window.open("../pdf/pdffromarray.php?obj="+obj+"&dateElection="+dateelection.value,"statbureau","toolbar=no,scrollbars=yes,status=no, width=650,height=600,top=80,left=80"); 
    //
    fenetreouverte = true;
}

//
function VerifNum (champ) {
    if  (isNaN (champ.value)) {
        alert ("Vous devez entrer uniquement des chiffres!");
        champ.value = "";
        return;
    }
    champ.value = champ.value.replace (".", "");
}

function VerifNumDecoupage(champ) {
    if (isNaN(champ.value)) {
        alert("vous ne devez entrer \ndes chiffres\nuiniquement");
        champ.value=0;
        return;
    }
    champ.value=champ.value.replace(".","");
    if (champ.value>90001) {
        alert("La valeur du champ \ndoit etre inferieure \no 90001");
        champ.value=90001;
        return;
    }
}

/**
 * Permet de cacher le champ motif de refus du formulaire des procurations selon
 * la valeur du champ refus.
 */
function triggerMotifRefus() {
    if($('select[name=refus]').val() == 'N') {
        $('input[name=motif_refus]').closest('tr').fadeOut('400');
    } else {
        $('input[name=motif_refus]').closest('tr').fadeIn('400');
    }
}




function changeSexe() {
    // Civilite change la zone sexe ************************************************
    // M. => M
    // Mme ou Mlle => F
    // *****************************************************************************
    if (document.f1.civilite.value=="Mme") document.f1.sexe.value="F";
    if (document.f1.civilite.value=="Mlle") document.f1.sexe.value="F";
    if (document.f1.civilite.value=="M.") document.f1.sexe.value="M";
}

function forcerlebureau(bureau) {
    // ================================================================
    // si un bureau est choisi, le bureau est force
    // ================================================================
    if (origine_codebureau=="-1") origine_codebureau = bureau;
    if (document.f1.code_bureau.value=="") {
        document.f1.bureauforce.value="Non";
    } else {
        document.f1.bureauforce.value="Oui";
    }
}

function nepasforcerlebureau() {
    // ================================================================
    // si le bureau n'est plus force, le bureau revient à sa valeur initiale
    // ================================================================
    if (origine_codebureau=="-1") origine_codebureau = document.f1.code_bureau.value;
    if (document.f1.bureauforce.value=="Non") document.f1.code_bureau.value=origine_codebureau;
}

/**
 * Cette fonction permet de remplir le champ code_lieu_de_naissance de manière
 * automatique au moment de la corrélation du département de naissance.
 * 
 * En fonction de la valeur saisie dans le champ code_departement_naissance la 
 * valeur préremplie est différente :
 *  - si il y a déjà une valeur dans le champ code_lieu_de_naissance alors 
 *    on ne fait rien
 *  - si le code département est sur 5 caractères (par exemple pays 99352) ou 
 *    si il est égal à '99' alors on recopie la même valeur dans le champ 
 *    code_lieu_de_naissance
 *  - si le code département est sur 2 caractères (par exemple département 01 
 *    à 95) ou sur 3 caractères (par exemple dom ou tom 971 à 976 ou 987) 
 *    alors on recopie la même valeur  dans le champ code_lieu_de_naissance
 *    auquel on rajoute un espace pour permettre la corrélation
 */
function codecommune(champ) {
    if (document.f1.code_lieu_de_naissance.value == "") {
        if (champ.value == '99' || champ.value.length == 5) {
            document.f1.code_lieu_de_naissance.value=champ.value;
        } else {
            if (champ.value.length == 2 || champ.value.length == 3) {
                document.f1.code_lieu_de_naissance.value=champ.value+" ";
            }
        }
    }
}


/**
 * TRT
 */
// Cette fonction permet d'afficher une invite de confirmation a l'utilisateur
// pour verifier si il est sur de vouloir executer le traitement
function trt_confirm() {
    //
    if (confirm("Etes-vous sur de vouloir confirmer cette action ?")) {
        //
        return true;
    } else {
        //
        return false;
    }
}

// 
function trt_form_trigger_succes() {
    //
}

//
function trt_form_submit(id, link, formulaire) {
    // Si le formulaire n'a pas une classe no_confirmation
    if (!$(formulaire).hasClass("no_confirmation")) {
        // Alors on execute la fonction trt_confirm qui permet de demander
        // a l'utilisateur si il est sur de vouloir continuer
        if (trt_confirm() == false) {
            // Si l'utilisateur n'est pas sur alors on sort de la fonction
            return false;
        }
    }
    // On affiche le spinner pour que l'utilisateur voit de l'animation a
    // l'ecran pendant l'execution du traitement
    $("#"+id).html(msg_loading);
    // Composition de la chaine data en fonction des elements du formulaire
    var data = ""
    if (formulaire) {
        for (i=0;i<formulaire.elements.length;i++) {
            if ((formulaire.elements[i].type == "checkbox" && formulaire.elements[i].checked == true) ||
                formulaire.elements[i].type != "checkbox") {
                data+=formulaire.elements[i].name+"="+formulaire.elements[i].value+"&";
            }
        }
    }
    //
    url = "../app/trt_ajax.php?obj="+link
    // Execution de la requete en POST
    $.ajax({
        type: "POST",
        url: url,
        cache: false,
        data: data,
        success: function(html){
            $("#"+id).empty();
            $("#"+id).append(html);
            trt_form_trigger_succes();
            om_initialize_content();
        }
    });
    // Si le formulaire n'a pas une classe no_status_update
    if (!$(formulaire).hasClass("no_status_update")) {
        // execution de la requete en GET qui met a jour le formulaire du
        // traitement
        $.ajax({
            type: "GET",
            url: url+"&action=traitement_"+link,
            cache: false,
            success: function(html){
                $("#traitement_"+link+"_status").empty();
                $("#traitement_"+link+"_status").append(html);
                om_initialize_content();
            }
        });
    }
    //
    return true;
}

//
function ajaxItForm(container_id, link, formulaire) {
    // composition de la chaine data en fonction des elements du formulaire
    var data = ""
    if (formulaire) {
        for (i=0;i<formulaire.elements.length;i++) {
            if (formulaire.elements[i].type == "checkbox" && formulaire.elements[i].checked == false) {
                //
            } else {
                data+=formulaire.elements[i].name+"="+formulaire.elements[i].value+"&";
            }
        }
    }
    // On affiche le spinner pour que l'utilisateur voit de l'animation a
    // l'ecran pendant l'execution du traitement
    $("#"+container_id).html(msg_loading);
    // execution de la requete en POST
    $.ajax({
        type: "POST",
        url: link,
        cache: false,
        data: data,
        success: function(html){
            $('#'+container_id).empty();
            $('#'+container_id).append(html);
            om_initialize_content();
        }
    });
}


/**
 * Au chargement de la page
 */
$(function() {
    //
    $( ".tooltip" ).dialog({
        autoOpen: false,
        width: 460,
        show: "blind",
        hide: "explode",
        modal: true
    });

    $( ".tooltip-opener" ).click(function() {
        ;
        $("#"+$(this).attr("rel")).dialog( "open" );
        
        return false;
    });
    triggerMotifRefus();
});

//
// Cette fonction permet d'afficher une invite de confirmation a l'utilisateur
// pour verifier si il est sur de vouloir soumettre le formulaire
function form_submit_confirm(form) {
    //
    if (confirm("Etes-vous sur de vouloir confirmer cette action ?")) {
        //
        form.submit();
    } else {
        //
        return false;
    }
}

/**
 * Cette fonction permet de charger dans un dialog jqueryui un formulaire tel
 * qu'il aurait été chargé avec ajaxIt
 * 
 * @param objsf string : objet de sousformulaire
 * @param link string : lien vers un sousformulaire (../scr/sousform.php...)
 * @param width integer: width en px
 * @param height integer: height en px
 * @param callback function (optionel) : nom de la méthode à appeler
 *                                       à la fermeture du dialog
 * @param callbackParams mixed (optionel) : paramètre à traiter dans la function
 *                                          callback 
 *
 **/
function popupIt(objsf, link, width, height, callback, callbackParams) {
    // Insertion du conteneur du dialog
    var dialog = $('<div id=\"sousform-'+objsf+'\"></div>').insertAfter('#tabs-1 .formControls');
    $('<input type=\"text\" name=\"recherchedyn\" id=\"recherchedyn\" value=\"\" class=\"champFormulaire\" style=\"display:none\" />').insertAfter('#sousform-'+objsf);
    
    // execution de la requete passee en parametre
    // (idem ajaxIt + callback)
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            //Suppression d'un precedent dialog
            dialog.empty();
            //Ajout du contenu recupere
            dialog.append(html);

            //Initialisation du theme OM
            om_initialize_content();
            //Creation du dialog
            $(dialog).dialog({
            //OnClose suppression du contenu
            close: function(ev, ui) {
                // Si le formulaire est submit et valide on execute la méthode
                // passée en paramètre
                if (typeof(callback) === "function") {
                    callback(callbackParams);
                }
                $(this).remove();
            },
            resizable: true,
            modal: true,
            width: 'auto',
            height: 'auto',
            position: 'left top',
          });
        },
        async : false
    });
    //Fermeture du dialog lors d'un clic sur le bouton retour
    $('#sousform-'+objsf).delegate('a.retour', 'click', function() {
        $(dialog).dialog('close').remove();
        return false;
    });
}

// fonction permettant de recharger le contenu des infos du juré d'assises
function displayJureAssises(id_electeur) {
    $.get('../app/electeur.view.php', 
        { "id_electeur": id_electeur, "jure": "true" },
        function(data) {
            $('#jury_assise').html(data);
        }
    );
    
}
<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";
if (!isset($f)) { $f = new utils("nohtml"); }

/**
 *
 */
//
$traitement = "";
if (isset($_GET["traitement"])) {
	$traitement = $_GET["traitement"];
}
//
if (!in_array($traitement, array("j5", "annuel"))) {
    $class = "error";
    $message = _("L'objet est invalide.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}


/**
 *
 */
//
$pdf_recapitulatif_traitement_output = "";
if (isset($_GET["output"])) {
    $pdf_recapitulatif_traitement_output = $_GET["output"];
}

/**
 *
 */
//
$_GET["output"] = "no";
//
require_once "../obj/pdf_common.class.php";
//
$pdf = new PDF("L", "mm", "A4");
//
$pdf->generic_document_header_left = $f->collectivite["ville"];
$pdf->generic_document_title = $_SESSION["liste"]." - ".$_SESSION["libelle_liste"];
//
$pdf->SetMargins(10, 5, 10);
//
$pdf->AliasNbPages();
//
$_GET["obj"] = "traitement_".$traitement;
include "../pdf/pdffromarray.php";
//
$_GET["obj"] = "traitement_".$traitement."_inscription";
include "../app/pdffromdb.php";
//
$_GET["obj"] = "traitement_".$traitement."_modification";
include "../app/pdffromdb.php";
//
$_GET["obj"] = "traitement_".$traitement."_radiation";
include "../app/pdffromdb.php";



/**
 *
 */
// Construction du nom du fichier
$filename = date("Ymd-His");
$filename .= "-recapitulatif";
if (isset($datetableau) && $datetableau != "") {
    $filename .= "-".$datetableau;
}
$filename .= "-traitement_".$traitement;
if (isset($datej5) && $datej5 != "") {
    $filename .= "-".$datej5;
}
$filename .= "-".$_SESSION['collectivite'];
$filename .= "-".$f->normalizeString($_SESSION['libelle_collectivite']);
$filename .= "-liste".$_SESSION['liste'];
$filename .= ".pdf";
//
$output = $pdf_recapitulatif_traitement_output;
if (!in_array($output, array("string", "file", "download", "inline", "no"))) {
    $output = "inline"; // Valeur par defaut
}
//
if ($output == "string") {
    // S : renvoyer le document sous forme de chaine. name est ignore.
    $pdf_output = $pdf->Output("", "S");
} elseif ($output == "file") {
    // F : sauver dans un fichier local, avec le nom indique dans name
    // (peut inclure un repertoire).
    $pdf->Output($f->getParameter("pdfdir").$filename, "F");
} elseif ($output == "download") {
    // D : envoyer au navigateur en forcant le telechargement, avec le nom
    // indique dans name.
    $pdf->Output($filename, "D");
} elseif ($output == "inline") {
    // I : envoyer en inline au navigateur. Le plug-in est utilise s'il est
    // installe. Le nom indique dans name est utilise lorsque l'on selectionne
    // "Enregistrer sous" sur le lien generant le PDF.
    $pdf->Output($filename, "I");
} elseif ($output == "no") {
   
}

?>
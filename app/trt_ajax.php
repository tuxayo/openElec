<?php
/**
 *
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");

// Correction d'un bug rencontré sur PHP5.3
// Il apparaît que les clés dans les POST n'étaient pas
// correctement reconnus si elles étaient trop longues
// et qu'elle contenaient des points. Ce qui est le cas
// dans les formulaires de traitement.
function getRealPOST() {
    $pairs = explode("&", file_get_contents("php://input"));
    $vars = array();
    foreach ($pairs as $pair) {
        $nv = explode("=", $pair);
        if (isset($nv[0])) {
            $name = urldecode($nv[0]);
        } else {
            continue;
        }
        if (isset($nv[1])) {
            $value = urldecode($nv[1]);
        } else {
            $value = "";
        }
        $vars[$name] = $value;
    }
    return $vars;
}
$_POST_alternative = getRealPOST();

/**
 * Definition du charset de la page
 */
header("Content-type: text/html; charset=".HTTPCHARSET."");

//
$obj = "";
if (isset($_GET["obj"])) {
    $obj = $_GET["obj"];
}

//
if (strpos($obj, "/") !== false
    or !file_exists("../obj/traitement.".$obj.".class.php")) {
    die();
}

//
if (isset($_POST["traitement_".$obj."_form_valid"])
    || isset($_POST_alternative["traitement.".$obj.".form.valid"])) {
    //
    require_once "../obj/traitement.".$obj.".class.php";
    //
    $constructeur = str_replace(" ", "", ucwords(str_replace("_", " ", $obj)))."Traitement";
    //
    $trt = new $constructeur($f);
    $trt->execute();
}

//
if (isset($_GET["action"]) && $_GET["action"] = "traitement_".$obj) {
    //
    require_once "../obj/traitement.".$obj.".class.php";
    //
    $constructeur = str_replace(" ", "", ucwords(str_replace("_", " ", $obj)))."Traitement";
    //
    $trt = new $constructeur($f);
    $trt->displayFormSection();
}

?>

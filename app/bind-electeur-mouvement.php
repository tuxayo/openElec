<?php
/**
 *
 *
 * @package openelec
 * @version SVN: $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");

//
$erreur = false;

//
(isset($_GET["idx_electeur"]) ? $idx_electeur = $_GET["idx_electeur"] : $erreur = true);
if ($erreur == true) {
    $f->addTolog("L'identifiant d'electeur est obligatoire", EXTRA_VERBOSE_MODE);
    $f->notExistsError();
}

//
$query_mouvement = "select * from mouvement as m inner join param_mouvement as pm ";
$query_mouvement .= "on m.types=pm.code where id_electeur=".$idx_electeur;
$query_mouvement .= " and collectivite='".$_SESSION["collectivite"]."' and etat='actif'";
$res_mouvement = $f->db->query($query_mouvement);
$f->isDatabaseError($res_mouvement);
if ($res_mouvement->numrows() != 1) {
    $f->notExistsError();
}
$row_mouvement =& $res_mouvement->fetchrow(DB_FETCHMODE_ASSOC);

//
(isset($_GET["bind"]) ? $bind = $_GET["bind"] : $_bind = "");

//
switch ($bind) {
    case "attestationmodification" :
        if ($row_mouvement["typecat"] != "Modification") {
            $f->notExistsError();
        }
        $_GET["idx"] = $row_mouvement["id"];
        $_GET["obj"] = "attestationmodification";
        $_GET["id"] = "mouvement";
        include "../pdf/pdfetat.php";
        break;
    case "attestationradiation" :
        if ($row_mouvement["typecat"] != "Radiation") {
            $f->notExistsError();
        }
        $_GET["idx"] = $row_mouvement["id"];
        $_GET["obj"] = "attestationradiation";
        $_GET["id"] = "mouvement";
        include "../pdf/pdfetat.php";
        break;
    default:
        die("ERREUR");
    
}
?>
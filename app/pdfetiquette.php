<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";
if (!isset($f)) { $f = new utils("nohtml"); }

/**
 *
 */
// Nom de l'objet metier
(isset($_GET['obj']) ? $obj = $_GET['obj'] : $obj = "");
// Verifications des parametres
if (strpos($obj, "/") !== false
    or !file_exists("../sql/".OM_DB_PHPTYPE."/".$obj.".pdfetiquette.inc")) {
    $class = "error";
    $message = _("L'objet est invalide.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}
// Verification des permissions
$f->isAuthorized($obj."_pdfetiquette");

/**
 *
 */
//
define ('FPDF_FONTPATH', 'font/');
//
require_once "../obj/fpdf_etiquette.class.php";
//
if (file_exists("../dyn/var.inc")) { include "../dyn/var.inc"; }
//
if (!isset($parametrage_etiquettes)) {
    //
    $parametrage_etiquettes = array(
        "etiquette_bordure_etiquette" => 1,
        "etiquette_bordure_texte" => 0,
        "planche_marge_ext_haut" => 1,
        "planche_marge_ext_gauche" => 0,
        "planche_nb_colonnes" => 2,
        "planche_nb_lignes" => 7,
        "etiquette_largeur" => 105,
        "etiquette_hauteur" => 42, 
        "etiquette_marge_int_haut" => 8,
        "etiquette_marge_int_gauche" => 8,
        "etiquette_marge_int_droite" => 8,
        "etiquette_espace_entre_colonnes" => 0,
        "etiquette_espace_entre_lignes" => -1, // -1 correspond Ã  0
        "etiquette_hauteur_de_ligne_du_texte" => 4,
        "etiquette_taille_du_texte" => 10,
    );
} else {
    (!isset($parametrage_etiquettes["etiquette_bordure_etiquette"]) ? $parametrage_etiquettes["etiquette_bordure_etiquette"] = 1 : "");
    (!isset($parametrage_etiquettes["etiquette_bordure_texte"]) ? $parametrage_etiquettes["etiquette_bordure_texte"] = 0 : "");
    (!isset($parametrage_etiquettes["planche_marge_ext_haut"]) ? $parametrage_etiquettes["planche_marge_ext_haut"] = 1 : "");
    (!isset($parametrage_etiquettes["planche_marge_ext_gauche"]) ? $parametrage_etiquettes["planche_marge_ext_gauche"] = 0 : "");
    (!isset($parametrage_etiquettes["planche_nb_colonnes"]) ? $parametrage_etiquettes["planche_nb_colonnes"] = 2 : "");
    (!isset($parametrage_etiquettes["planche_nb_lignes"]) ? $parametrage_etiquettes["planche_nb_lignes"] = 7 : "");
    (!isset($parametrage_etiquettes["etiquette_largeur"]) ? $parametrage_etiquettes["etiquette_largeur"] = 105 : "");
    (!isset($parametrage_etiquettes["etiquette_hauteur"]) ? $parametrage_etiquettes["etiquette_hauteur"] = 42 : "");
    (!isset($parametrage_etiquettes["etiquette_marge_int_haut"]) ? $parametrage_etiquettes["etiquette_marge_int_haut"] = 8 : "");
    (!isset($parametrage_etiquettes["etiquette_marge_int_gauche"]) ? $parametrage_etiquettes["etiquette_marge_int_gauche"] = 8 : "");
    (!isset($parametrage_etiquettes["etiquette_marge_int_droite"]) ? $parametrage_etiquettes["etiquette_marge_int_droite"] = 8 : "");
    (!isset($parametrage_etiquettes["etiquette_espace_entre_colonnes"]) ? $parametrage_etiquettes["etiquette_espace_entre_colonnes"] = 0 : "");
    (!isset($parametrage_etiquettes["etiquette_espace_entre_lignes"]) ? $parametrage_etiquettes["etiquette_espace_entre_lignes"] = -1 : "");
    (!isset($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"]) ? $parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] = 4 : "");
    (!isset($parametrage_etiquettes["etiquette_taille_du_texte"]) ? $parametrage_etiquettes["etiquette_taille_du_texte"] = 10 : "");
}
//
$DEBUG=0;
//******************************************************************************
//                          OBJET PDF                                         //
//******************************************************************************
$orientation="P";// orientation P-> portrait L->paysage
$format="A4"; // format A3 A4 A5
//******************************************************************************
//                          PARAMETRES GENERAUX                               //
//******************************************************************************
$size = $parametrage_etiquettes["etiquette_taille_du_texte"]; // taille police
$police = "courier"; // police courier,times,arial,helvetica
$gras = ""; //$gras="B" -> BOLD OU $gras=""
$C1 = "0";// couleur texte  R
$C2 = "0";// couleur texte V
$C3 = "0";// couleur texte B
//
$cadre = $parametrage_etiquettes["etiquette_bordure_etiquette"]; // cadre etiquette 1 -> oui 0 -> non
$cadrechamps = $parametrage_etiquettes["etiquette_bordure_texte"]; // cadre  zones affichees 1 -> oui 0 -> non
//******************************************************************************
//                      PARAMETRES ETIQUETTE                                  //
//******************************************************************************
$_x_number = $parametrage_etiquettes["planche_nb_colonnes"]; // Nombre d'etiquettes sur la largeur de la page
$_y_number = $parametrage_etiquettes["planche_nb_lignes"]; // Nombre d'etiquettes sur la hauteur de la page
$_margin_left = $parametrage_etiquettes["planche_marge_ext_gauche"]; // Marge de gauche de l'etiquette
$_margin_top = $parametrage_etiquettes["planche_marge_ext_haut"]; // Marge en haut de la page avant la premiere etiquette
$_x_space = $parametrage_etiquettes["etiquette_espace_entre_colonnes"]; //Espace horizontal entre 2 bandes d'Ã©tiquettes
$_y_space = $parametrage_etiquettes["etiquette_espace_entre_lignes"]; //Espace vertical entre 2 bandes d'Ã©tiquettes
$_width = $parametrage_etiquettes["etiquette_largeur"]; // Largeur de chaque etiquette
$_height = $parametrage_etiquettes["etiquette_hauteur"]; //Hauteur de chaque etiquette
$_char_size = $parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"]; // Hauteur des caracteres
$_line_height = $parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"]; // Hauteur par defaut interligne
//
include "../sql/".OM_DB_PHPTYPE."/".$obj.".pdfetiquette.inc";

/**
 *
 */
//
$pdf = new pdfetiquette($orientation, 'mm', $format);
//
$pdf->SetFont($police, $gras, $size);
$pdf->SetTextColor($C1, $C2, $C3);
$pdf->SetMargins(0, 0);
$pdf->SetAutoPageBreak(false);
$pdf->Open();
$pdf->AddPage();
$pdf->SetDisplayMode('real', 'single');
//
$param=array();
$param=array($_margin_left,
             $_margin_top,
             $_x_space,
             $_y_space,
             $_x_number,
             $_y_number,
             $_width,
             $_height,
             $_char_size,
             $_line_height,
             0,
             0,
             $size,
             $cadrechamps,
             $cadre);
//
$pdf->Table_position($sql, $f->db, $param, $champs, $texte, $champs_compteur, $img);

/**
 *
 */

// Construction du nom du fichier

$filename = "";
if (isset($_GET["filename_with_date"]) && $_GET["filename_with_date"] == "false") {
    $filename .= "";
} else {
    $filename .= date("Ymd-His")."-";
}
$filename .= "etiquettes";
$filename .= "-".$obj;
$filename .= "-".$_SESSION['collectivite'];
$filename .= "-".$f->normalizeString($_SESSION['libelle_collectivite']);
if (isset($filename_more)) {
    $filename .= $filename_more;
}
$filename .= ".pdf";

//
$mode = (isset($_GET["mode"]) ? $_GET["mode"] : "inline");
//
if ($mode == "chaine") {
    // S : renvoyer le document sous forme de chaine
    $filename = $f->getParameter("pdfdir").$filename;
    $contenu = $pdf->Output($filename, "S");
} elseif ($mode == "save") {
    // F : sauver dans un fichier local
    $filename = $f->getParameter("pdfdir").$filename;
    $pdf->Output($filename, "F");
    if (isset($_GET["redirect"])) {
       @header("Location:".$_GET["redirect"]);
    }
} elseif ($mode == "download") {
    // D : envoyer au navigateur en forcant le telechargement
    header('Cache-Control: private, max-age=0, must-revalidate');
    $pdf->Output($filename, "D");
} else {
    // I : envoyer en inline au navigateur
    header('Cache-Control: private, max-age=0, must-revalidate');
    $pdf->Output($filename, "I");
}

//
$pdf->Close();

?>

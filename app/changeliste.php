<?php
/**
 * Formulaire de changement de liste
 *
 * @package openElec
 */

/**
 *
 */
require_once("../obj/utils.class.php");

//
$f = new utils("nohtml",
               "listedefaut",
               _("Choix de la liste de travail"));

/**
 *
 */
require_once("../obj/workliste.class.php");

//
$workliste = new workListe($f, "../app/changeliste.php");
$workliste->validateForm();

//
$f->setFlag(NULL);
$f->display();

//
$workliste->showForm();

?>
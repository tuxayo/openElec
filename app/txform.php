<?php
/**
 *
 *
 * @package openmairie_exemple
 * @version SVN : $Id: txform.php 137 2010-10-08 12:45:00Z fmichon $
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");

/**
 *
 */
// Nom de l'objet metier
(isset($_GET['obj']) ? $obj = $_GET['obj'] : $obj = "");
// Identifiant de l'enregistrement
(isset($_GET['idx']) ? $idx = $_GET['idx'] : (isset($_POST['idx']) ? $idx = $_POST['idx'] : $idx = ""));
// Flag de mise a jour
(isset($_GET['maj']) ? $maj = $_GET['maj'] : $maj = 1);
// Validation
(isset($_GET['validation']) ? $validation = $_GET['validation'] : $validation = 0);

/**
 * Verification des parametres
 */
$objs_valid = array("etat", "lettretype", "sousetat");
if (!in_array($obj, $objs_valid)
    or $idx == "") {
    $class = "error";
    $message = _("L'objet est invalide.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}

/**
 *
 */
//
$f->setTitle(_("Parametrage")." -> "._($obj)." -> "._($idx));
$f->setIcon("ico_parametrage.png");
$f->setHelp($obj);
//
$f->setRight($obj);
$f->isAuthorized();
//
$f->setFlag(NULL);
$f->display();

/**
 *
 */
//
echo "<div id=\"formulaire\">\n\n";
//
require_once PATH_OPENMAIRIE."om_".$obj.".class.php";
//
$enr = new $obj($idx, 0, $maj, $f->db);
//
echo "\t<ul>\n";
echo "\t\t<li><a href=\"#tabs-1\">".ucwords(_($obj))."</a></li>";
echo "\t</ul>\n";
//
echo "\t<div id=\"tabs-1\">\n";
//
$validation++;
//
$enr->formulaire($validation, $maj, $f->db, $_POST, $obj, 0, $idx);
//
echo "\t</div>\n";
//
echo "\n</div>\n";

?>
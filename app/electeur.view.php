<?php
/**
 * Ce fichier permet l'affichage d'une fiche electeur. Cette fiche regroupe
 * toutes les informations de l'electeur, un historique ainsi que les actions
 * disponibles sur cet electeur.
 *
 * @package openElec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";

if(isset($_GET['jure'])) {
    $f = new utils ("nohtml", "ficheelecteur");
} else {
    $f = new utils (NULL, "ficheelecteur",
                _("Consultation")." -> "._("Fiche de l'electeur"));
}


/**
 * Gestion des erreurs
 */
//
if (!isset($_GET['id_electeur']) or !is_numeric($_GET['id_electeur'])) {
    $f->notExistsError ();
}
//
$class = "../obj/electeur.class.php";
if (!file_exists($class)) {
    $f->notExistsError();
}
require_once $class;
//
$inc = "../sql/".$f->phptype."/electeur.form.inc";
if (!file_exists($inc)) {
    $f->notExistsError();
}
require_once $inc;
//
if (isset($_POST["electeur_carteretour_action"]) && $f->isAccredited(/*DROIT*/"carteretour")) {
    //
    $id_elec = $_GET['id_electeur'];
    //
    if (isset($_POST["electeur_carteretour_action"][0])) {
        //
        if ($_POST["electeur_carteretour_action"][0] == "add") {
            $valF = array("carte" => '1');
        } elseif ($_POST["electeur_carteretour_action"][0] == "remove") {
            $valF = array("carte" => '0');
        } else {
            $valF = NULL;
        }
        //
        if ($valF != NULL) {
            //
            $res_update = $f->db->autoexecute("electeur", $valF, DB_AUTOQUERY_UPDATE, "id_electeur=".$id_elec);
            $f->isDatabaseError($res_update);
        }
    }
}
//
$electeur = new electeur($_GET['id_electeur'], $f->db, true);
if (!isset($electeur->val[0]) || $electeur->val[0] == "") {
    $f->notExistsError(_("Cet identifiant electeur n'existe pas dans votre collectivite."));
}

// Premier enregistrement a afficher sur le tableau de la page precedente (tab.php?premier=)
(isset($_GET['premier']) ? $premier = $_GET['premier'] : $premier = 0);
// Colonne choisie pour le tri sur le tableau de la page precedente (tab.php?tricol=)
(isset($_GET['tricol']) ? $tricol = $_GET['tricol'] : $tricol = "");
// Colonne choisie pour la selection sur le tableau de la page precedente (tab.php?selectioncol=)
(isset($_GET['selectioncol']) ? $selectioncol = $_GET['selectioncol'] : $selectioncol = "");
// Chaine recherchee
if (isset($_GET['recherche'])) {
    $recherche = $_GET['recherche'];
    if (get_magic_quotes_gpc()) {
        $recherche1 = StripSlashes($recherche);
    } else {
        $recherche1 = $recherche;
    }
} else {
    $recherche = "";
    $recherche1 = "";
}

if(isset($_GET['jure'])) {
    displayJureAssises($electeur->val, $champs);
    die();
}

function displayJureAssises($val, $champs) {
    // Info concernant l'aspect juré de l'électeur
    if($val[array_search("jury", $champs)] == 1 OR
        $val[array_search("jury_effectif", $champs)] == "oui") {
        echo "<h3>";
        echo _("Jury d'assises :");
        echo "</h3>";
        echo "<p class=\"contenu\">";
        if($val[array_search("jury", $champs)] == 1) {
            echo _("L'electeur fait parti de la liste provisoire courante.");
            echo "<br/>";
        }
        if($val[array_search("profession", $champs)] != "") {
            echo _("Profession de l'electeur :");
            echo " ".$val[array_search("profession", $champs)];
            echo "<br/>";
        }
        if($val[array_search("jury_effectif", $champs)] == "oui") {
            $dateFormat = new DateTime($val[array_search("date_jeffectif", $champs)]);
            echo _("L'electeur est jure effectif le :");
            echo " ".$dateFormat->format('d/m/Y');
            echo "<br/>";
        }
        if($val[array_search("motif_dispense_jury", $champs)] != "") {
            echo _("Motif de dispense :");
            echo " ".$val[array_search("motif_dispense_jury", $champs)];
            echo "<br/>";
        }

        echo "</p>";
    }
}

/**
 *
 */
//
$query_electeur = "select * from electeur where id_electeur=".$_GET['id_electeur'];
$res_electeur = $f->db->query($query_electeur);
$f->isDatabaseError($res_electeur);
$row_electeur =& $res_electeur->fetchrow(DB_FETCHMODE_ASSOC);


function displayFicheElecteur($infos = array()) {
    //
    global $f;
    //
    echo "<div class=\"ficheelecteur\">";
    //
    echo "<div class=\"etatcivil\">";
    echo "<h2>";
    echo $infos["civilite"];
    echo " ";
    echo "<span class=\"nom\" title=\""._("nom")."\">";
    echo $infos["nom"];
    echo "</span>";
    echo " ";
    if ($infos["nom_usage"] != "") {
        echo "- ";
        echo "<span class=\"nom_usage\" title=\""._("nom d'usage")."\">";
        echo $infos["nom_usage"];
        echo "</span>";
        echo " ";
    }
    echo "<span class=\"prenom\" title=\""._("prenom(s)")."\">";
    echo $infos["prenom"];
    echo "</span>";


    echo "</h2>";
    echo "</div>";
    
    //
    echo "<div class=\"naissance\">";
    echo "<p>";
    echo _("Ne(e) le")." ";
    echo $f->formatDate ($infos["date_naissance"], true);
    echo "<br/>";
    echo " "._("a")." ";
    echo $infos["libelle_lieu_de_naissance"];
    echo " [";
    echo $infos["libelle_departement_naissance"];
    echo " (";
    echo $infos["code_departement_naissance"];
    echo ")]";
    echo "</p>";
    echo "<p>";
    echo _("Sexe")." : ";
    $sexe = $infos["sexe"];
    if ($sexe == "M") {
        echo _("Masculin");
    } elseif ($sexe == "F") {
        echo _("Feminin");
    } else {
        echo "-";
    }
    echo "</p>";
    echo "<p>";
    echo _("Nationalite")." : ";
    $query_nationalite = "select * from nationalite where code='".$infos["code_nationalite"]."'";
    $res_nationalite = $f->db->query($query_nationalite);
    $f->isDatabaseError($res_nationalite);
    $row_nationalite =& $res_nationalite->fetchrow(DB_FETCHMODE_ASSOC);
    echo $row_nationalite['libelle_nationalite'];
    echo "</p>";
    echo "</div>";
    echo "</div>";
}

/**
 *
 */
// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n\n";

// Affichage de la liste des onglets
echo "<ul>\n";
echo "\t<li><a href=\"#tabs-1\">";
echo _("Electeur - Id")." : ".$electeur->val[array_search("id_electeur", $champs)];
echo "</a></li>\n";
echo "</ul>\n";

// Ouverture de la balise - Onglet 1
echo "\n<div id=\"tabs-1\">\n";
//
echo "<div id=\"ficheelecteur\">\n";

if ($row_electeur['typecat'] != "") {
$f->displayMessage("error", _("Cet electeur a un mouvement en cours"));
}

//
echo "<div class=\"col_9\">\n";
//
$infos = array(
    "civilite" => $electeur->val[array_search("civilite", $champs)],
    "nom" => $electeur->val[array_search("nom", $champs)],
    "prenom" => $electeur->val[array_search("prenom", $champs)],
    "nom_usage" => $electeur->val[array_search("nom_usage", $champs)],
    "date_naissance" => $electeur->val[array_search("date_naissance", $champs)],
    "libelle_lieu_de_naissance" => $electeur->val[array_search("libelle_lieu_de_naissance", $champs)],
    "libelle_departement_naissance" => $electeur->val[array_search("libelle_departement_naissance", $champs)],
    "code_departement_naissance" => $electeur->val[array_search("code_departement_naissance", $champs)],
    "sexe" => $electeur->val[array_search("sexe", $champs)],
    "code_nationalite" => $electeur->val[array_search("code_nationalite", $champs)],
);
displayFicheElecteur($infos);


//
if ($electeur->val[array_search("carte", $champs)] == "1") {
    echo "<div class=\"message  ui-widget-content ui-corner-all carteretour\">";
    echo "<p class=\"titre carteretour-32\">";
    echo _("Une carte en retour est enregistree pour cet electeur.");
    echo "</p>";
    echo "</div>";
}
//
echo "<div class=\"adresse\">";
echo "<p class=\"titre\">";
echo _("Adresse dans la commune :");
echo "</p>";
echo "<p class=\"contenu\">";
if ($electeur->val[array_search("numero_habitation", $champs)] != 0) {
    echo $electeur->val[array_search("numero_habitation", $champs)];
    echo " ";
}
echo $electeur->val[array_search("complement_numero", $champs)];
echo " ";
echo $electeur->val[array_search("libelle_voie", $champs)];
if ($electeur->val[array_search("complement", $champs)] != "") {
echo "<br/>";
echo $electeur->val[array_search("complement", $champs)];
}
echo "<br/>";
$query_voie = "select * from voie where code='".$electeur->val[array_search("code_voie", $champs)]."'";
$res_voie = $f->db->query($query_voie);
$f->isDatabaseError($res_voie);
$row_voie =& $res_voie->fetchrow(DB_FETCHMODE_ASSOC);
echo $row_voie['cp']." ".$row_voie['ville'];
echo "</p>";
echo "</div>";

//
if ($electeur->val[array_search("resident", $champs)] == 'Oui') {
echo "<div class=\"residence\">";
echo "<p class=\"titre\">";
echo _("Adresse de residence :");
echo "</p>";
echo "<p class=\"contenu\">";
echo $electeur->val[array_search("adresse_resident", $champs)];
if ($electeur->val[array_search("complement_resident", $champs)] != "") {
echo "<br/>";
echo $electeur->val[array_search("complement_resident", $champs)];
}
echo "<br/>";
echo $electeur->val[array_search("cp_resident", $champs)]." ".$electeur->val[array_search("ville_resident", $champs)];
echo "</p>";
echo "</div>";
}

//
echo "<div class=\"bureau\">";
echo "<h3>";
echo _("Bureau de vote :");
echo " ";
$query_bureau = "select * from bureau where code='".$electeur->val[array_search("code_bureau", $champs)]."' and collectivite='".$_SESSION['collectivite']."'";
$res_bureau = $f->db->query($query_bureau);
$f->isDatabaseError($res_bureau);
$row_bureau =& $res_bureau->fetchrow(DB_FETCHMODE_ASSOC);
echo $electeur->val[array_search("code_bureau", $champs)] ." ".$row_bureau['libelle_bureau'];
echo "</h3>";
echo "<p class=\"contenu\">";
echo _("Numero de l'electeur dans le bureau :");
echo " ".$electeur->val[array_search("numero_bureau", $champs)];
echo "<br/>";
echo _("Numero de l'electeur dans la liste :");
echo " ".$electeur->val[array_search("numero_electeur", $champs)];
echo "</p>";
echo "</div>";

echo "<div id=\"jury_assise\">";
displayJureAssises($electeur->val, $champs);
echo "</div>";
//
echo "<div class=\"historique\">";
echo "<h3>";
echo _("Historique");
echo "</h3>";
//
$historique = array(
    "actif" => array(),
    "trs" => array(),
    "archive" => array(),
);
//
$query_mouvement = "select * from mouvement as m inner join param_mouvement as pm on m.types=pm.code where id_electeur=".$electeur->val[array_search("id_electeur", $champs)]." order by date_tableau DESC";
$res_mouvement = $f->db->query ($query_mouvement);
$f->isDatabaseError($res_mouvement);
while ($row_mouvement =& $res_mouvement->fetchrow(DB_FETCHMODE_ASSOC)) {
    $mouvement = array(
        "row" => $row_mouvement,
        "etat" => $row_mouvement['etat'],
        "categorie" => $row_mouvement['typecat'],
        "mouvement" => $row_mouvement['libelle'],
        "datetableau" => $row_mouvement['date_tableau'],
        "tableau" => $row_mouvement['tableau'],
        "datemouvement" => $row_mouvement['date_modif'],
    );
    array_push ($historique[$row_mouvement['etat']], $mouvement);
}
$query_archive = "select * from archive as a inner join param_mouvement as pm on a.types=pm.code where id_electeur=".$electeur->val[array_search("id_electeur", $champs)]." order by date_tableau DESC, date_mouvement DESC";
$res_archive = $f->db->query ($query_archive);
$f->isDatabaseError($res_archive);
while ($row_archive =& $res_archive->fetchrow(DB_FETCHMODE_ASSOC)) {
    $archive = array(
        "etat" => "archive",
        "categorie" => $row_archive['typecat'],
        "mouvement" => $row_archive['libelle'],
        "datetableau" => $row_archive['date_tableau'],
        "tableau" => $row_archive['tableau'],
        "datemouvement" => $row_archive['date_mouvement'],
    );
    array_push ($historique['archive'], $archive);
}
//
echo "<table class=\"tabCol\">";

foreach ($historique as $key => $categorie) {
    echo "<tr class=\"fenData\">";
    echo "<td>";
    echo "<b>";
    switch ($key) {
        case "actif": echo _("Mouvement(s) en cours"); break;
        case "trs": echo _("Mouvement(s) traite(s)"); break;
        case "archive": echo _("Mouvement(s) archive(s)"); break;
        default:;
    }
    echo "</b>";
    echo "</td>";
    echo "</tr>";
    foreach ($categorie as $mouvement) {
        echo "<tr>";
        echo "<td>";
        echo $mouvement['categorie'];
        echo " [";
        echo $mouvement['mouvement'];
        echo "] ";
        if ($mouvement['etat'] == 'actif') {
            echo _("Mouvement en cours modifie le")." ";
            echo $f->formatDate ($mouvement['datemouvement'], true);
        } elseif ($mouvement['etat'] == 'archive') {
            echo " "._("valide lors du traitement")." ";
            echo $mouvement['tableau'];
            echo " "._("a la date de tableau du")." ";
            echo $f->formatDate ($mouvement['datetableau'], true);
            echo " ";
            echo _("Mouvement archive le")." ";
            echo $f->formatDate ($mouvement['datemouvement'], true);
        } elseif ($mouvement['etat'] == 'trs') {
            echo " "._("valide lors du traitement")." ";
            echo $mouvement['tableau'];
            echo " "._("a la date de tableau du")." ";
            echo $f->formatDate ($mouvement['datetableau'], true);
            //
            echo " (<a class=\"tooltip-opener\" rel=\"mouvement-".$mouvement["row"]["id"]."\" href=\"#\">"._("Voir le detail")."</a>)";
            //
            echo "<div class=\"tooltip\" id=\"mouvement-".$mouvement["row"]["id"]."\">";
            //echo "<pre>";
            //print_r($mouvement["row"]);
            //echo "</pre>";
            echo "<div class=\"message ui-state-highlight ui-corner-all\">";
            echo $mouvement['categorie'];
            echo " [";
            echo $mouvement['mouvement'];
            echo "] ";
                echo " "._("valide lors du traitement")." ";
            echo $mouvement['tableau'];
            echo " "._("a la date de tableau du")." ";
            echo $f->formatDate ($mouvement['datetableau'], true);
            echo "</div>";
            echo "<div id=\"ficheelecteur\">";
            displayFicheElecteur($mouvement["row"]);
            //
            echo "<div class=\"adresse\">";
            echo "<p class=\"titre\">";
            echo _("Adresse dans la commune :");
            echo "</p>";
            echo "<p class=\"contenu\">";
            if ($mouvement["row"]["numero_habitation"] != 0) {
                echo $mouvement["row"]["numero_habitation"];
                echo " ";
            }
            echo $mouvement["row"]["complement_numero"];
            echo " ";
            echo $mouvement["row"]["libelle_voie"];
            if ($mouvement["row"]["complement"] != "") {
            echo "<br/>";
            echo $mouvement["row"]["complement"];
            }
            echo "<br/>";
            $query_voie = "select * from voie where code='".$mouvement["row"]["code_voie"]."'";
            $res_voie = $f->db->query($query_voie);
            $f->isDatabaseError($res_voie);
            $row_voie =& $res_voie->fetchrow(DB_FETCHMODE_ASSOC);
            echo $row_voie['cp']." ".$row_voie['ville'];
            echo "</p>";
            echo "</div>";
            //
            echo "<div class=\"bureau\">";
            echo "<h3>";
            echo _("Bureau de vote :");
            echo " ";
            $query_bureau = "select * from bureau where code='".$mouvement["row"]["code_bureau"]."' and collectivite='".$_SESSION['collectivite']."'";
            $res_bureau = $f->db->query($query_bureau);
            $f->isDatabaseError($res_bureau);
            $row_bureau =& $res_bureau->fetchrow(DB_FETCHMODE_ASSOC);
            echo $mouvement["row"]["code_bureau"] ." ".$row_bureau['libelle_bureau'];
            echo "</h3>";
            //echo "<p class=\"contenu\">";
            //echo _("Numero de l'electeur dans le bureau :");
            //echo " ".$mouvement["row"]["numero_bureau"];
            //echo "<br/>";
            //echo _("Numero de l'electeur dans la liste :");
            //echo " ".$mouvement["row"]["numero_electeur"];
            //echo "</p>";
            echo "</div>";
            echo "</div>";
            //
            echo "</div>";

        }
        echo "</td>";
        echo "</tr>";
    }
}
echo "</table>";
$sql = "select * from electeur ";
$sql .= "left join param_mouvement on electeur.code_inscription=param_mouvement.code ";
$sql .= "where id_electeur=".$_GET['id_electeur'];
$res = $f->db->query($sql);
$f->addToLog("app/electeur.view.php: db->query(\"".$sql."\");", VERBOSE_MODE);
$f->isDatabaseError($res);
if ($res->numrows() == 1) {
    $row =& $res->fetchrow(DB_FETCHMODE_ASSOC);
    $f->addToLog("app/electeur.view.php: ".print_r($row, true), EXTRA_VERBOSE_MODE);
    if ($row["date_inscription"] != "") {
        echo "<h4>";
        echo _("Inscrit le :");
        echo " ".$f->formatDate($row["date_inscription"], true);
        if ($row["code_inscription"] != "") {
            echo " - ";
            echo _("Motif :");
            echo " ".$row["libelle"];
        }
        echo "</h4>";
    }
} else {
    $f->addToLog("app/electeur.view.php: resultat de la requete : ".$res->numrows(), EXTRA_VERBOSE_MODE);
}
echo "</div>";
//
echo "</div>";

//
echo "<div class=\"actions col_3\">";

//
if ($f->isAccredited(array(/*DROIT*/"attestationelecteur",
                           /*DROIT*/"carteelecteur",), "OR")) {
    echo "<div class=\"actions boite ui-widget-content ui-corner-all ui-accordion-header ui-state-default\">";
    echo "<ul class=\"menu-friendly\">";
    if ($f->isAccredited(/*DROIT*/"attestationelecteur")) {
        echo "<li>";
        echo "<a";
        echo " target=\"_blank\"";
        echo " class=\"om-prev-icon pdf-16\"";
        echo " href=\"../pdf/pdfetat.php?obj=attestationelecteur&amp;idx=".$electeur->val[array_search("id_electeur", $champs)]."&amp;id=electeur\"";
        echo ">";
        echo _("Attestation d'electeur");
        echo "</a>";
        echo "</li>";
    }
    if ($f->isAccredited(/*DROIT*/"carteelecteur")) {
        echo "<li>";
        echo "<a";
        echo " target=\"_blank\"";
        echo " class=\"om-prev-icon pdf-16\"";
        echo " href=\"../pdf/carteelecteur.php?mode_edition=electeur&amp;idx=".$electeur->val[array_search("id_electeur", $champs)]."\"";
        echo ">";
        echo _("Carte d'electeur");
        echo "</a>";
        echo "</li>";
    }
    echo "</ul>";
    echo "</div>";
}
//
if ($f->isAccredited(array(/*DROIT*/"carteretour",), "OR")) {
    echo "<div class=\"actions boite ui-widget-content ui-corner-all ui-accordion-header ui-state-default\">";
    //
    echo "<form name=\"electeur_form_carteretour\" action=\"../app/electeur.view.php?id_electeur=".$electeur->val[array_search("id_electeur", $champs)]."&amp;origin=ficheelecteur&amp;tricol=".$tricol."&amp;premier=".$premier."&amp;recherche=".$recherche1."&amp;selectioncol=".$selectioncol."\" ";
    echo " method=\"POST\">\n";
    //
    echo "<ul class=\"menu-friendly\">";
    if ($electeur->val[array_search("carte", $champs)] == "0") {
        //
        echo "<input type=\"hidden\" name=\"electeur_carteretour_action[]\" value=\"add\" />\n";
        //
        echo "<li>";
        echo "<a";
        echo " class=\"om-prev-icon carteretour-add-16\"";
        echo " href=\"#\"";
        //echo " href=\"../app/electeur.view.php?id_electeur=".$electeur->val[array_search("id_electeur", $champs)]."&amp;obj=carteretour&amp;action=add&amp;origin=ficheelecteur&amp;tricol=".$tricol."&amp;premier=".$premier."&amp;recherche=".$recherche1."&amp;selectioncol=".$selectioncol."\"";
        echo " onclick=\"javascript:form_submit_confirm(electeur_form_carteretour);\"";
        echo ">";
        echo _("Enregistrer une carte en retour pour cet electeur");
        echo "</a>";
        echo "</li>";
    } else {
        //
        echo "<input type=\"hidden\" name=\"electeur_carteretour_action[]\" value=\"remove\" />\n";
        //
        echo "<li>";
        echo "<a";
        echo " class=\"om-prev-icon carteretour-remove-16\"";
        //echo " href=\"../app/electeur.view.php?id_electeur=".$electeur->val[array_search("id_electeur", $champs)]."&amp;obj=carteretour&amp;action=remove&amp;origin=ficheelecteur&amp;tricol=".$tricol."&amp;premier=".$premier."&amp;recherche=".$recherche1."&amp;selectioncol=".$selectioncol."\"";
        echo " href=\"#\"";
        echo " onclick=\"javascript:form_submit_confirm(electeur_form_carteretour);\"";
        echo ">";
        echo _("Supprimer la carte en retour enregistree");
        echo "</a>";
    }
    echo "</ul>";
    //
    echo "</form>\n";
    //
    echo "</div>";
}
//
if ($row_electeur['typecat'] == "") {
    if ($f->isAccredited(array(/*DROIT*/"electeur_modification",
                               /*DROIT*/"electeur_radiation",), "OR")) {
        echo "<div class=\"actions boite ui-widget-content ui-corner-all ui-accordion-header ui-state-default\">";
        echo "<ul class=\"menu-friendly\">";
        if ($f->isAccredited(/*DROIT*/"electeur_modification")) {
            echo "<li>";
            echo "<a";
            echo " class=\"om-prev-icon modification-16\"";
            echo " href=\"../scr/form.php?obj=modification&amp;idxelecteur=".$electeur->val[array_search("id_electeur", $champs)]."&amp;maj=0&amp;origin=ficheelecteur&amp;tricol=".$tricol."&amp;premier=".$premier."&amp;recherche=".$recherche1."&amp;selectioncol=".$selectioncol."\"";
            echo ">";
            echo _("Modifier l'electeur");
            echo "</a>";
            echo "</li>";
        }
        if ($f->isAccredited(/*DROIT*/"electeur_radiation")) {    
            echo "<li>";
            echo "<a";
            echo " class=\"om-prev-icon radiation-16\"";
            echo " href=\"../scr/form.php?obj=radiation&amp;idxelecteur=".$electeur->val[array_search("id_electeur", $champs)]."&amp;maj=0&amp;origin=ficheelecteur&amp;tricol=".$tricol."&amp;premier=".$premier."&amp;recherche=".$recherche1."&amp;selectioncol=".$selectioncol."\"";
            echo ">";
            echo _("Radier l'electeur");
            echo "</a>";
            echo "</li>";
        }
        echo "</ul>";
        echo "</div>";
    }
} else {
    $query_mouvement = "select * from mouvement as m inner join param_mouvement as pm ";
    $query_mouvement .= "on m.types=pm.code where id_electeur=".$electeur->val[array_search("id_electeur", $champs)];
    $query_mouvement .= " and etat='actif'";
    $res_mouvement = $f->db->query ($query_mouvement);
    $f->isDatabaseError($res_mouvement);
    $row_mouvement =& $res_mouvement->fetchrow(DB_FETCHMODE_ASSOC);
    if ($f->isAccredited(strtolower($row_mouvement['typecat']))) {
        echo "<div class=\"actions boite ui-widget-content ui-corner-all ui-accordion-header ui-state-default\">";
        echo "<ul class=\"menu-friendly\">";
        echo "<li>";
        echo "<a";
        echo " class=\"om-prev-icon edit-16\"";
        echo " href=\"../scr/form.php?obj=".strtolower($row_mouvement['typecat'])."&amp;idxelecteur=".$electeur->val[array_search("id_electeur", $champs)]."&amp;maj=1&amp;origin=ficheelecteur&amp;tricol=".$tricol."&amp;premier=".$premier."&amp;recherche=".$recherche1."&amp;selectioncol=".$selectioncol."\"";
        echo ">";
        echo _("Modifier le mouvement en cours");
        echo "</a>";
        echo "</li>";
        echo "<li>";
        echo "<a";
        echo " class=\"om-prev-icon delete-16\"";
        echo " href=\"../scr/form.php?obj=".strtolower($row_mouvement['typecat'])."&amp;idxelecteur=".$electeur->val[array_search("id_electeur", $champs)]."&amp;maj=2&amp;ids=1&amp;origin=ficheelecteur&amp;tricol=".$tricol."&amp;premier=".$premier."&amp;recherche=".$recherche1."&amp;selectioncol=".$selectioncol."\"";
        echo ">";
        echo _("Supprimer le mouvement en cours");
        echo "</a>";
        echo "</li>";
        echo "<li>";
        echo "<a";
        echo " target=\"_blank\"";
        echo " class=\"om-prev-icon pdf-16\"";
        echo " href=\"../pdf/pdfetat.php?obj=attestation".strtolower($row_mouvement['typecat'])."&amp;idx=".$row_mouvement['id']."&amp;id=mouvement\"";
        echo ">";
        echo _("Attestation de")." ".strtolower($row_mouvement['typecat']);
        echo "</a>";
        echo "</li>";
        echo "</ul>";
        echo "</div>";
    }
}
// Lien ouvrant un overlay permettant à l'utilisateur de saisir les info "juré" de l'électeur
if ($f->isAccredited("electeur_jury")) {
    echo "<div class=\"actions boite ui-widget-content ui-corner-all ui-accordion-header ui-state-default\">";
    echo "<ul class=\"menu-friendly\">";
        echo "<li>";
        echo "<a";
        echo " target=\"_blank\"";
        echo " class=\"om-prev-icon jury-16 tooltip-opener\"";
        echo " href=\"#\"";
        echo " onclick=\"popupIt('electeur_jury',
                '../scr/sousform.php?obj=electeur_jury&action=1&retourformulaire=electeur_view&idx=".$_GET["id_electeur"]."', 860, 'auto', displayJureAssises, ".$_GET['id_electeur'].");\"";
        echo ">";
        echo _("Jure d'assises");
        echo "</a>";
        echo "</li>";
    echo "</ul>";
    echo "</div>";
}
echo "</div>";


//
echo "<div class=\"formControls col_12\">";
echo "<a class=\"retour\" title=\""._("Retour")."\" ";
echo "href=\"../scr/tab.php?obj=consult_electeur&amp;premier=".(isset($_GET['premier'])?$_GET['premier']:0)."&amp;recherche=".(isset($_GET['recherche'])?$_GET['recherche']:"")."&amp;selectioncol=".$selectioncol."&amp;tricol=".$tricol."\">";
echo _("Retour");
echo "</a>";
echo "</div>";

echo "<div class=\"visualClear\"><!-- --></div>";

//
echo "</div>";

// Fermeture de la balise - Onglet 1
echo "</div>\n";

// Fermeture de la balise - Conteneur d'onglets
echo "\n</div>\n";

?>
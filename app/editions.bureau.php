<?php
/**
 * Ce fichier permet l'edition de documents par bureau
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils (NULL, "bureau_edition", _("Editions par bureau"));

/**
 * Description de la page
 */
$description = _("Cette interface vous permet d'editer des documents par ".
                 "bureau (liste d'emargement, carte d'electeur, liste des ".
                 "procurations, ...).");
$f->displayDescription($description);

/**
 * Editions disponibles
 */
//
$documents = array();
//
$doc = array("title" => _("Liste d'emargement"),
             "icon" => "listeemargement-25",
             "tooltip" => _("Liste d'emargement"),
             "href" => "../pdf/listeemargement.php?id=bureau&amp;idx=",
             );
array_push($documents, $doc);
$doc = array("title" => _("Liste generale"),
             "icon" => "listegenerale-25",
             "tooltip" => _("Liste generale"),
             "href" => "../pdf/listegenerale.php?id=bureau&amp;idx=",
             );
array_push($documents, $doc);
$doc = array("title" => _("Etiquettes d'electeur"),
             "icon" => "etiquetteelecteur-25",
             "tooltip" => _("Etiquettes d'electeur"),
             "href" => "../app/pdfetiquette.php?obj=electeur&amp;code_bureau=",
             );
array_push($documents, $doc);
$doc = array("title" => _("Cartes d'electeur"),
             "icon" => "carteelecteur-25",
             "tooltip" => _("Cartes d'electeur"),
             "href" => "../pdf/carteelecteur.php?mode_edition=parbureau&amp;idx=",
             );
array_push($documents, $doc);
$doc = array("title" => _("Commissions details"),
             "icon" => "commission-25",
             "tooltip" => _("Commissions details"),
             "href" => "../pdf/commission.php?id=bureau&amp;idx=",
             );
array_push($documents, $doc);
$doc = array("title" => _("Liste des procurations"),
             "icon" => "listeprocuration-25",
             "tooltip" => _("Liste des procurations"),
             "href" => "../pdf/listeprocuration.php?id=bureau&amp;idx=",
             );
array_push($documents, $doc);
$doc = array("title" => _("Liste des cartes en retour"),
             "icon" => "listecarteretour-25",
             "tooltip" => _("Liste des cartes en retour"),
             "href" => "../pdf/listecarteretour.php?id=bureau&amp;idx=",
             );
array_push($documents, $doc);
$doc = array("title" => _("Registre des procurations"),
             "icon" => "listeregistreprocuration-25",
             "tooltip" => _("Registre des procurations"),
             "href" => "../pdf/listeregistreprocuration.php?id=bureau&amp;idx=",
             );
array_push($documents, $doc);

/**
 * Affichage des editions par bureau
 */
//
echo "<div id=\"bureau_edition\">";
// Tableau des editions
echo "<table class=\"tab-tab\" >";
echo "<tbody>";
echo "<tr class=\"ui-tabs-nav ui-accordion ui-state-default tab-title\" >";
echo "<td class=\"firstcol\">&nbsp;</td>";
foreach ($documents as $doc) {
    echo "<th>&nbsp;".$doc["title"]."</th>";
}
echo "</tr>";
// 
require_once "../sql/".$f->phptype."/bureau_edition.inc";
$res = $f->db->query($sql_bureau);
$f->isDatabaseError($res);
//
$class = "even";
while ($row=& $res->fetchRow()) {
    ($class == "even" ? $class = "odd" : $class = "even");
    echo "<tr class=\"tab-data ".$class."\" >";
    echo "<td>".$row[0]." - ".$row[1]."</td>";
    $cpt = 1;
    foreach ($documents as $doc) {
        echo "<td><a href=\"".$doc["href"].$row[0]."\" target=\"_blank\">";
        echo "<span class=\"om-icon om-icon-25 om-icon-fix ".$doc["icon"]."\"";
        echo " title=\"".$doc["tooltip"]."\">";
        echo $doc["tooltip"];
        echo "</span>";
        echo "</a></td>";
        $cpt++;
    }
    echo "</tr>";
}
echo "</tbody>";
echo "</table>";
//
echo "</div>\n";

?>

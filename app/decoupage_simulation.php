<?php
/**
 * Ce fichier permet de ...
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml", /*DROIT*/"decoupage_simulation",
               _("Traitement")." -> "._("Redecoupage electoral"));

/**
 *
 */
if ($f->isAjaxRequest()) {
    // Definition du charset de la page
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    // Si ce n'est pas une requete Ajax on affiche la structure de la page
    $f->setFlag(NULL);
    $f->display();
}

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Decoupage electoral");
$f->displaySubTitle($subtitle);
$description = _("L'edition suivante permet de visualiser le decoupage ".
                 "electoral parametre dans le logiciel. Il est compose de la ".
                 "liste des voies qui sont associees a un bureau de vote en ".
                 "fonction des numeros d'habitation.");
$f->displayDescription($description);
//
$links = array(
    "0" => array(
        "href" => "../pdf/pdf.php?obj=decoupage",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Parametrage du decoupage electoral"),
    ),
);
//
$f->displayLinksAsList($links);
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

/**
 *
 */
// Ouverture de la balise - DIV paragraph
echo "<div class=\"paragraph\">\n";
// Affichage du titre du paragraphe
$subtitle = "-> "._("Simulation de redecoupage");
$f->displaySubTitle($subtitle);
$description = _("Cette simulation se base uniquement sur les electeurs ".
                 "effectivement inscrits sur la liste electorale. Elle ne ".
                 "prend pas en compte les eventuelles inscriptions, ".
                 "modifications ou radiations en cours. Le tableau presente ".
                 "le nombre d'electeurs actuellement inscrits dans chacun ".
                 "des bureaux de vote avec parmi eux le nombre d'electeurs ".
                 "qui sont inscrits en bureau force. La derniere colonne ".
                 "indique le nombre d'electeurs qui seraient inscrits dans ".
                 "chacun des bureaux si on appliquait le découpage électoral ".
                 "actuellement paramétré sur les electeurs qui ne sont pas en ".
                 "bureau force.");
$f->displayDescription($description);
//
require_once "../obj/workdecoupage.class.php";
$wd = new workdecoupage();
$result = $wd->simulation();
// Fermeture de la balise - DIV paragraph
echo "</div>\n";

?>

<?php
/**
 * Ce fichier surcharge la classe FPDF pour etre utilise ...
 * XXX - Cette classe ne devrait pas se trouver dans le dossier spg.
 *
 * @package openelec
 * @version SVN : $Id$
 */

// XXX - Ce define est-il necessaire ? Deja present dans la classe FPDF non ?
define('FPDF_FONTPATH', 'font/');

/**
 * Inclusion de la classe FPDF qui permet de generer des fichiers PDF.
 */
require("fpdf.php");

/**
 * Cette methode surcharge la classe standard fpdf
 */
class PDF extends FPDF
{
    //
     var $msg=0;
    //
    // Give the height for a char size given.
    function Get_Height_Chars($pt) {
        // Tableau de concordance entre la hauteur des caractÃšres et de l'espacement entre les lignes
        $_Table_Hauteur_Chars = array(6=>2, 7=>2.5, 8=>3, 9=>4, 10=>5, 11=>6, 12=>7, 13=>8, 14=>9, 15=>10);
        if (in_array($pt, array_keys($_Table_Hauteur_Chars))) {
            return $_Table_Hauteur_Chars[$pt];
        } else {
            return 100; // There is a prob..
        }
    }
    
    
    /** MÃ©thode qui permet de modifier la taille des caractÃšres
     *  Cela modifiera aussi l'espace entre chaque ligne
     */
    function Set_Font_Size($pt,$Char_Size,$Line_Height) {
        if ($pt > 3) {
            $this->$Char_Size = $pt;
            $this->$Line_Height = $this->Get_Height_Chars($pt);
            $this->SetFontSize($this->$Char_Size);
        }
    }
    
    /**
     *
     */
    function AffCells($paramcells,$param,$tailledefault,$page,$ligne) {
        $nbrcell=count($paramcells);
        $p=0;
        if ($nbrcell>0) {
            for($p=0;$p<$nbrcell;$p++) {
                $page=$this->PageNo();
                //
                $position=0;
                $width =0;
                $height=0;
                $border=0;
                $align='C';
                $flagB='';
                $flagtaille=$tailledefault;
                //
                $LibOuMotCle= $paramcells[$p][0];
                if (strtoupper($LibOuMotCle)=='<PAGE>') {
                    $LibOuMotCle="Page  :  ".$page.' ';
                }
                //
                $this->SetFillColor($paramcells[$p][9],$paramcells[$p][10],$paramcells[$p][11]);
                $this->SetTextColor($paramcells[$p][6],$paramcells[$p][7],$paramcells[$p][8]);
                $width =$paramcells[$p][1];
                $height=$paramcells[$p][2];
                $position=$paramcells[$p][3];
                $border=$paramcells[$p][4];
                $align=$paramcells[$p][5];
                //-------  taille -----------------------
                if ($paramcells[$p][15]=='B') {
                    $flagB='B';
                }
                if ($paramcells[$p][16]>0){
                    $flagtaille=$paramcells[$p][16];
                }
                $this->SetFont('',$flagB,$flagtaille);
        
                //-------------------------------------//
                //  Differents type de cellule         //
                //-------------------------------------//
                //
                //-------- type Vcell -------------------
                if ($LibOuMotCle=='<VCELL>') {
                    $heightlettre=0;
                    $deduire=0;
                    $heightlettre=$paramcells[$p][13];
                    $lettre=$paramcells[$p][14];
                    $nblettre=0;
                    $demiheightentete=0;
                    $nblettre=count($lettre);
                    $demiheightentete=floor(($height-$nblettre*$heightlettre)/2) ;
                    //
                    $tx=$this->Getx();
                    $ty=$this->Gety();
                    $ty=$ty+$paramcells[$p][12][1];
                    $tx=$tx+$paramcells[$p][12][0];
                    $this->Setxy($tx,$ty);
                    //
                    $this->Cell($width,$demiheightentete,'','LTR',2,$align,1);
                    $deduire+=$demiheightentete;
                    $j=0;
                    for($j=0;$j<$nblettre;$j++) {
                        $nom=$paramcells[$p][14][$j];
                        $this->Cell($width,$heightlettre,iconv(HTTPCHARSET,"CP1252",$nom),'LR',2,$align,1);
                        $deduire+=$heightlettre;
                    }
                    $this->Cell($width,$height-$deduire,' ','LBR',2,$align,1);
                    $deduire+=$height-$deduire;
                    $this->Cell($width,0,'','0',$position,$align,1);
                    //
                    $tx=$this->Getx();
                    $ty=$this->Gety();
                    $this->Setxy($tx,$ty);
                    //--------------- fin Vcell ---------------------------------
                } else {
                    //--------------- TYPE image png ---------------------------
                    if (strtoupper($LibOuMotCle)=='<IMG>') {
                    $x=$this->Getx();
                    $y=$this->Gety();
                    $y=$y-$paramcells[$p][12][1];
                    $x=$x-$paramcells[$p][12][0];
                    $this->Cell($width+2,$height+2,'',$border,$position,$align,1);
                    $this->Image($paramcells[$p][14],$x+1,$y+1);
                    //--------------- fin image png ---------------------------
                    } else {
                        if (strtoupper($LibOuMotCle)=='<LIGNE>') {
                        $tx=$this->Getx();
                        $ty=$this->Gety();
                        $ty=$ty-$paramcells[$p][12][1];
                        $tx=$tx-$paramcells[$p][12][0];
                        $this->Setxy($tx,$ty);
                        $this->Cell($width,$height,iconv(HTTPCHARSET,"CP1252",$ligne),$border,$position,$align,1);
                        $this->nbrligne=$ligne;
                        } else {
                            if ($align=='<UDCELL>' || $align=='<RCELL>') {
                                if ($align=='<UDCELL>') $align='U'; 
                                if ($align=='<RCELL>') $align='45'; 
                                $l=0;
                                $l=strlen($LibOuMotCle);
                                $tx=$this->Getx();
                                $ty=$this->Gety();
                                $ty=$ty-$paramcells[$p][12][1];
                                $tx=$tx-$paramcells[$p][12][0];
                                $this->Setxy($tx,$ty);
                                $this->Cell($width,$height,' ',$border,$position,$align,1);
                                $posx=0;
                                $posy=0;
                                $posx=$paramcells[$p][13];
                                $posy=$paramcells[$p][14];
                                $demiheightD=0;
                                $demiheightD=floor(($height-$l)/2);
                                $demiwidthD=0;
                                $demiwidthD=floor(($width/2));
                                $tx=$tx+$demiwidthD+$posx;
                                $ty=$ty+$l+$demiheightD+$posy;
                                if ($align=='U') $this->Dcell($tx,$ty,$LibOuMotCle,$align);
                                if ($align=='45') $this->Rcell($tx,$ty,$LibOuMotCle,45,0);
                            } else {
                                // --------- TYPE cellule simple -------------------------
                                $tx=$this->Getx();
                                $ty=$this->Gety();
                                $ty=$ty-$paramcells[$p][12][1];
                                $tx=$tx-$paramcells[$p][12][0];
                                $this->Setxy($tx,$ty);
                                if ($page==1) {
                                    $this->Cell($width,$height,iconv(HTTPCHARSET,"CP1252",$LibOuMotCle),$border,$position,$align,1);
                                } else {
                                    if ($page>1 and $paramcells[$p][17]==1) {
                                        $this->Cell($width,$height,iconv(HTTPCHARSET,"CP1252",$LibOuMotCle),$border,$position,$align,1);
                                    } else {
                                        if ($paramcells[$p][17]==0 && $paramcells[$p][18]==1) {  $this->Cell($width,$height,' ',$border,$position,$align,1); }
                                    }
                                }
                            }
                            // --------- FIN  TYPE cellule simple -------------------------
                        } 
                    }
                }
                $this->SetFont('','',$tailledefault);
            }
        } //nbr cellule > 0
    }// FIN function

    /**
     *
     */
    function Table_position($sql,$db,$param,$champs,$texte,$champs_compteur,$img,$pagedebut=array(),$paramPageDebut=array()) {
        $_Margin_Left=$param[0];
        $_Margin_Top=$param[1];
        $_X_Space=$param[2];
        $_Y_Space=$param[3];
        $_X_Number=$param[4];
        $_Y_Number=$param[5];
        $_Width=$param[6];
        $_Height=$param[7];
        $_Char_Size=$param[8];
        $_Line_Height=$param[9];
        $_cptx=$param[10];
        $_cpty=$param[11];
        $_cadre=$param[13];
        $_cadrezone=$param[14];
        $this->Set_Font_Size($param[12],$param[8],$param[9]);
        //
        $res =& $db->query($sql);
        //
        if (database::isError($res)) {
             $this->erreur_db($res->getDebugInfo(),$res->getMessage(),'');
        } else {
            if ($pagedebut != array()) {
                $this->SetFont('','',$param[2]);
                //il manque AffCells (qui vient de fpdf_table) => A IMPORTER
                //mettre les parametres correctement : $param ne correspond pas la meme chose : 
                //la doc de $paramPageDebut qui doit etre envoye a AffCells est dans pdf/listegenerale.php
                $this->AffCells($pagedebut,$paramPageDebut,$paramPageDebut[2],$this->PageNo(),0);
                $this->Addpage();
	    }	
            $nbChamp=0;
            $nbtexte=0;
            $nbimg=0;
            $_PosX =0;
            $_PosY =0;
            $info=$res->tableInfo();
            $nbChamp=count($info);
            $nbtexte=count($texte);
            $nbimg=count($img);
            //$compteur=0;
	    /* numerotation sur etiquettes electeurs */
	    if (isset($champs_compteur[6])) {
                $compteur=$champs_compteur[6];
		$csix=$champs_compteur[6];
	    } else {
		$compteur=0;
		$csix=0;
	    }
            $tmpchamps="";
            $nb_txt=0;
            //
            $nbrow=0;
            $nbrow=$res->numrows();
            //
            while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                $compteur++;
                $k=0;
                $j=0;
                if ($_cptx==0) {
                //-------------------------------------------------
                    $_PosX =$_Margin_Left;
                    $_PosY =$_Margin_Top+($_cpty*($_Height+$_Y_Space))+$_cpty;
                    $this->SetXY($_PosX, $_PosY);
                    //cadre zone definie
                    $this->MultiCell($_Width,$_Height,"",$_cadrezone);
                    if ($champs_compteur[0]==1) {
                        $archx= $_PosX;
                        $archy= $_PosY;
                        $champ_bold='';
                        $champ_size=$param[12];
                        $this->SetXY($archx+$champs_compteur[1],$archy+$champs_compteur[2]);
                        //bold et size
                        if ($champs_compteur[4]==1) $champ_bold='B';
                        if ($champs_compteur[5]>0) $champ_size=$champs_compteur[5];
                        $this->SetFont('Courier',$champ_bold,$champ_size);
                        $this->Set_Font_Size($champ_size,$param[8],$param[9]);
                        //
                        $this->MultiCell($champs_compteur[3],$_Line_Height,iconv(HTTPCHARSET,"CP1252",$compteur),$_cadre);
                        $this->SetXY($archx,$archy);
                    }
                    for ($j=0;$j<$nbChamp;$j++) {
                        $archx= $_PosX;
                        $archy= $_PosY;
                        $champ_bold='';
                        $champ_size=$param[12];
                        // ---------------------------------------$this->MultiCell($_Width,$_Height,"",$_cadrezone);
                        $this->SetXY($archx+$champs[$info[$j]['name']][2][0],$archy+$champs[$info[$j]['name']][2][1]);
                        //bold et size
                        if ($champs[$info[$j]['name']][2][3]==1) {$champ_bold='B';}
                        if ($champs[$info[$j]['name']][2][4]>0) {$champ_size=$champs[$info[$j]['name']][2][4];}
                        $this->SetFont('Courier',$champ_bold,$champ_size);
                        $this->Set_Font_Size($champ_size,$param[8],$param[9]);
                        //
                        if ($champs[$info[$j]['name']][3]==1) {
                            $champs_num="";
                            $champs_num=number_format($row[$info[$j]['name']],0);
                            $this->MultiCell($champs[$info[$j]['name']][2][2],$_Line_Height,iconv(HTTPCHARSET,"CP1252",$champs[$info[$j]['name']][0].$champs_num.$champs[$info[$j]['name']][1]),$_cadre);
                        } else {
                            $this->MultiCell($champs[$info[$j]['name']][2][2],$_Line_Height,iconv(HTTPCHARSET,"CP1252",$champs[$info[$j]['name']][0].$row[$info[$j]['name']].$champs[$info[$j]['name']][1]),$_cadre);
                        }
                        $this->SetXY($archx,$archy);
                    }
                    /**
                     * Code barre
                     **/
                    global $codebar_on;
                    if ($codebar_on) {
                        $archx= $_PosX+100;
                        $archy= $_PosY+5;
                        $id_elec=$row['id_electeur'];
                        if (isset ($_SERVER ['https']))
                            $server="https://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF'])."/";
                         else
                            $server="http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF'])."/";
                        $this->Image(''.$server.'barcode.php?type=C39&code='.$id_elec.'&height=10&readable=N&showtype=N', $archx,$archy,0,0,'png');
            		//$this->Image('../pdf/barcode.php?barcode='.$id_elec.'&height=25&width=250&text=0', $archx,$archy,0,0,'png');
                    }
                    if (file_exists("../trs/signature-carte-electeur.png")) {
                        $archx= $_PosX+0;
                        $archy= $_PosY+77;
                        $id_elec=$row['id_electeur'];
                        if (isset ($_SERVER ['https']))
                            $server="https://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF'])."/";
                         else
                            $server="http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF'])."/";
                        $this->Image('../trs/signature-carte-electeur.png', $archx, $archy, 105, 28,'png');
                    }
                    //
                    for ($i=0;$i<$nbimg;$i++) {
                        //
                        $archx= $_PosX;
                        $archy= $_PosY;
                        $this->SetXY($archx+$img[$i][1],$archy+$img[$i][2]);
                        //
                        $this->Image($img[$i][0],$archx+$img[$i][1],$archy+$img[$i][2],$img[$i][3],$img[$i][4],$img[$i][5]);
                        $this->SetXY($archx,$archy);
                    }
                    for ($k=0;$k<$nbtexte;$k++) {
                        $archx= $_PosX;
                        $archy= $_PosY;
                        $champ_bold='';
                        $champ_size=$param[12];
                        $this->SetXY($archx+$texte[$k][1],$archy+$texte[$k][2]);
                        //bold et size
                        if ($texte[$k][4]==1) $champ_bold='B';
                        if ($texte[$k][5]>0) $champ_size=$texte[$k][5];
                        $this->SetFont('Courier',$champ_bold,$champ_size);
                        $this->Set_Font_Size($champ_size,$param[8],$param[9]);
                        //
                        $this->MultiCell($texte[$k][3],$_Line_Height,iconv(HTTPCHARSET,"CP1252",$texte[$k][0]),$_cadre);
                        $this->SetXY($archx,$archy);
                    }
                    $_cptx++;
                    if ($_cptx==$_X_Number) {
                        $_cptx=0;
                        $_cpty++;
                        if ($_cpty==$_Y_Number) {
                            $_cptx=0;
                            $_cpty=0;
                            //
                            if ($compteur-$csix<$nbrow) $this->AddPage();
                        }
                    }
                    //---------------------------------------------------------------
                } else {
                    //---------------------------------------------------------------
                    $_PosX =$_Margin_Left+($_cptx*($_Width+$_X_Space));
                    $_PosY =$_Margin_Top+($_cpty*( $_Height+$_Y_Space))+$_cpty;
                    $this->SetXY($_PosX, $_PosY);
                    //cadre zone definie
                    $this->MultiCell($_Width,$_Height,"",$_cadrezone);
                    if ($champs_compteur[0]==1) {
                        $archx= $_PosX;
                        $archy= $_PosY;
                        $champ_bold='';
                        $champ_size=$param[12];
                        $this->SetXY($archx+$champs_compteur[1],$archy+$champs_compteur[2]);
                        //bold et size
                        if ($champs_compteur[4]==1) $champ_bold='B';
                        if ($champs_compteur[5]>0) $champ_size=$champs_compteur[5];
                        $this->SetFont('Courier',$champ_bold,$champ_size);
                        $this->Set_Font_Size($champ_size,$param[8],$param[9]);
                        //
                        $this->MultiCell($champs_compteur[3],$_Line_Height,iconv(HTTPCHARSET,"CP1252",$compteur),$_cadre);
                        $this->SetXY($archx,$archy);
                    }
                    for ($j=0;$j<$nbChamp;$j++) {
                        $archx= $_PosX;
                        $archy= $_PosY;
                        $champ_bold='';
                        $champ_size=$param[12];
                        //-------------------------$this->MultiCell($_Width,$_Height,"",$_cadrezone);
                        $this->SetXY($archx+$champs[$info[$j]['name']][2][0],$archy+$champs[$info[$j]['name']][2][1]);
                        //bold et size
                        if ($champs[$info[$j]['name']][2][3]==1) $champ_bold='B';
                        if ($champs[$info[$j]['name']][2][4]>0) $champ_size=$champs[$info[$j]['name']][2][4];
                        $this->SetFont('Courier',$champ_bold,$champ_size);
                        $this->Set_Font_Size($champ_size,$param[8],$param[9]);
                        //
                        if ($champs[$info[$j]['name']][3]==1) {
                            $champs_num="";
                            $champs_num=number_format($row[$info[$j]['name']],0);
                            $this->MultiCell($champs[$info[$j]['name']][2][2],$_Line_Height,iconv(HTTPCHARSET,"CP1252",$champs[$info[$j]['name']][0].$champs_num.$champs[$info[$j]['name']][1]),$_cadre);
                        } else {
                            $this->MultiCell($champs[$info[$j]['name']][2][2],$_Line_Height,iconv(HTTPCHARSET,"CP1252",$champs[$info[$j]['name']][0].$row[$info[$j]['name']].$champs[$info[$j]['name']][1]),$_cadre);
                        }
                        $this->SetXY($archx,$archy);
                    }
                    /**
                     * Code barre
                     **/
                    global $codebar_on;
                    if ($codebar_on) {
                        $archx= $_PosX+100;
                        $archy= $_PosY+5;
                        $id_elec=$row['id_electeur'];
                        if (isset ($_SERVER ['https']))
                            $server="https://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF'])."/";
                        else
                            $server="http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF'])."/";
                        $this->Image(''.$server.'barcode.php?type=C39&code='.$id_elec.'&height=10&readable=N&showtype=N', $archx,$archy,0,0,'png');
            		//$this->Image(''.$server.'barcode.php?barcode='.$id_elec.'&height=25&width=250&text=0', $archx,$archy,0,0,'png');
                    }
                    if (file_exists("../trs/signature-carte-electeur.png")) {
                        $archx= $_PosX+0;
                        $archy= $_PosY+77;
                        $id_elec=$row['id_electeur'];
                        if (isset ($_SERVER ['https']))
                            $server="https://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF'])."/";
                         else
                            $server="http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF'])."/";
                        $this->Image('../trs/signature-carte-electeur.png', $archx, $archy, 105, 28,'png');
                    }
                    //
                    for ($i=0;$i<$nbimg;$i++) {
                        //
                        $archx= $_PosX;
                        $archy= $_PosY;
                        $this->SetXY($archx+$img[$i][1],$archy+$img[$i][2]);
                        //
                        $this->Image($img[$i][0],$archx+$img[$i][1],$archy+$img[$i][2],$img[$i][3],$img[$i][4],$img[$i][5]);
                        $this->SetXY($archx,$archy);
                    }
                    for ($k=0;$k<$nbtexte;$k++) {
                        $archx= $_PosX;
                        $archy= $_PosY;
                        $champ_bold='';
                        $champ_size=$param[12];
                        $this->SetXY($archx+$texte[$k][1],$archy+$texte[$k][2]);
                        //bold et size
                        if ($texte[$k][4]==1) $champ_bold='B';
                        if ($texte[$k][5]>0) $champ_size=$texte[$k][5];
                        $this->SetFont('Courier',$champ_bold,$champ_size);
                        $this->Set_Font_Size($champ_size,$param[8],$param[9]);
                        //
                        $this->MultiCell($texte[$k][3],$_Line_Height,iconv(HTTPCHARSET,"CP1252",$texte[$k][0]),$_cadre);
                        $this->SetXY($archx,$archy);
                    }
                    $_cptx++;
                    if ($_cptx==$_X_Number) {
                        $_cptx=0;
                        $_cpty++;
                        if ($_cpty==$_Y_Number) {
                            $_cptx=0;
                            $_cpty=0;
                            // mo
			    // au
                            if ($compteur-$csix<$nbrow) $this->AddPage();
                        }
                    }
                    // fin else $_cptx//----------------------------------------------
                }
                // fin while
            } 
            // aucun enregistrement
            if ($nbrow==0) {
                $this->SetTextColor(245,34,108);
                $this->SetDrawColor(245,34,108);
                $_PosX =$_Margin_Left;
                $_PosY =$_Margin_Top+($_cpty*($_Height+$_Y_Space))+$_cpty;
                $this->SetXY($_PosX+10, $_PosY+10);
                $this->MultiCell(100,10,iconv(HTTPCHARSET,"CP1252",_('Aucun enregistrement selectionne')),1);
            }
            $res->free();
            // DB
        }
        // fonction table etiquette
   }

    /** 
     * Traitement d erreur
     * transfert Ã  l ecran des erreurs de bases de donnees
     * $debuginfo : info table de donnÃ©es
     * $messageDB : message d erreur DB pear
     * $table = table concernÃ©e
     */
    function erreur_db($debuginfo,$messageDB,$table) {
        include (PATH_OPENMAIRIE."error_db.inc");
        $this->SetFont('arial','','9');
        $this->ln();
        $this->Cell(0,10,iconv(HTTPCHARSET,"CP1252",_('Attention, Erreur de base de donnees')),0,1,'L');
        $this->Cell(0,10,iconv(HTTPCHARSET,"CP1252",$requete),0,1,'L');
        $this->Cell(0,10,iconv(HTTPCHARSET,"CP1252",$erreur_origine),0,1,'L');
        $this->Cell(0,10,iconv(HTTPCHARSET,"CP1252",$messageDB),0,1,'L');
        $this->Cell(0,10,iconv(HTTPCHARSET,"CP1252",$msgfr),0,1,'L');
        $this->Cell(0,10,iconv(HTTPCHARSET,"CP1252",_('Contactez votre administrateur')),0,1,'L');
    }
}
?>
<?php
/**
 * Formulaire de changement de date de tableau
 *
 * @package openElec
 */

/**
 *
 */
require_once("../obj/utils.class.php");

//
$f = new utils("nohtml",
               /*DROIT*/"collectivite",
               _("Changement de la date de tableau"));

//
if (isset ($_POST['changedatetableau_action_valid'])) {
    // 
    $datetableau = $f->formatDate($_POST['datetableau'], false);

    //
    if ($datetableau == false) {
	// message 'erreur' 
	$message_class = "error";
	$message = _("La date n'est pas valide.");
    } else {
        //
        $cle = " id='".$_SESSION['collectivite']."' ";
        if ($_SESSION['multi_collectivite'] == 1) {
            $cle = "";
        }
        //
        $valF["datetableau"] = $datetableau;
        $res = $f->db->autoExecute("collectivite", $valF, DB_AUTOQUERY_UPDATE, $cle);
        
        //
        $message_class = "ok";
        $message = _("La date de tableau est changee.");
    }
    
    // message
    $f->addToMessage($message_class, $message);
}

//
$f->getCollectivite();

//
$f->setFlag(NULL);
$f->display();

//
echo "<div class=\"instructions\">";
echo "<p>";
echo _("Ici vous pouvez mettre a jour votre date de tableau.");
echo "</p>";
echo "</div>";
//
echo "\n<div id=\"changedatetableauform\" class=\"formulaire\">\n";
//
echo "<form method=\"post\" id=\"changedatetableauform_form\" action=\"../app/changedatetableau.php\">\n";
//
echo "\t<div class=\"field\">\n";
echo "\t\t<label for=\"datetableau\">"._("Date de tableau")."</label>\n";
echo "\t\t<input type=\"text\" onchange=\"fdate(this)\" name=\"datetableau\" ";
echo "maxlength=\"10\" size=\"10\" class=\"champFormulaire datepicker\" ";
echo "tabindex=\"1\" size=\"15\" ";
echo "value=\"".$f->formatdate($f->collectivite['datetableau'], true)."\"/>\n";
echo "\t</div>\n";
//
echo "\t<div class=\"formControls\">\n";
echo "\t\t<input name=\"changedatetableau.action.valid\" tabindex=\"4\" value=\""._("Valider")."\" type=\"submit\" class=\"boutonFormulaire context\" />\n";
echo "<a class=\"retour\" title=\""._("Retour")."\" ";
echo "href=\"../scr/dashboard.php\">";
echo _("Retour");
echo "</a>";
echo "\t</div>\n";
//
echo "</form>\n";
//
echo "</div>\n";

?>

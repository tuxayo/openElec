<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
require_once "../obj/utils.class.php";
if (!isset($f)) { $f = new utils("nohtml"); }

/**
 *
 */
//
$dateelection1 = NULL;
if (isset($_GET['dateelection1'])) {
    //
    $date = explode("/", $_GET['dateelection1']);
    if (sizeof($date) == 3 and (checkdate($date[1], $date[0], $date[2]))) {
        $dateelection1 = $date[2]."-".$date[1]."-".$date[0];
    }
}
//
$dateelection2 = NULL;
if (isset($_GET['dateelection2'])) {
    //
    $date = explode("/", $_GET['dateelection2']);
    if (sizeof($date) == 3 and (checkdate($date[1], $date[0], $date[2]))) {
        $dateelection2 = $date[2]."-".$date[1]."-".$date[0];
    }
}
//
if ($dateelection1 == NULL && $dateelection2 == NULL) {
    //
    $dateelection1 = date('Y-m-d');
}


/**
 *
 */
//
$pdf_recapitulatif_traitement_output = "";
if (isset($_GET["output"])) {
    $pdf_recapitulatif_traitement_output = $_GET["output"];
}

/**
 *
 */
//
$_GET["output"] = "no";
//
require_once "../obj/pdf_common.class.php";
//
$pdf = new PDF("L", "mm", "A4");
//
$pdf->generic_document_header_left = $f->collectivite["ville"];
//
$generic_document_title = "Recapitulatif du traitement des mentions pour l'election du";
if ($dateelection1 != NULL && $dateelection2 != NULL) {
    $generic_document_title .= " ".$f->formatdate($dateelection1);
    $generic_document_title .= " "._("et du");
    $generic_document_title .= " ".$f->formatdate($dateelection2);
} else {
    if ($dateelection1 != NULL) {
        $generic_document_title .= " ".$f->formatdate($dateelection1);
    } else {
        $generic_document_title .= " ".$f->formatdate($dateelection2);
    }
}
$pdf->generic_document_title = $generic_document_title;
//
$pdf->SetMargins(10, 5, 10);
//
$pdf->AliasNbPages();
//
$_GET["obj"] = "centrevote_bureau";
include "../pdf/pdffromarray.php";
//
$_GET["obj"] = "procurationok";
include "../app/pdffromdb.php";
//
$_GET["obj"] = "procurationnonok";
include "../app/pdffromdb.php";



/**
 *
 */
// Construction du nom du fichier
$filename = date("Ymd-His");
$filename .= "-recapitulatif";
$filename .= "-mention";
$filename .= "-".$_SESSION['collectivite'];
$filename .= "-".$f->normalizeString($_SESSION['libelle_collectivite']);
$filename .= ".pdf";
//
$output = $pdf_recapitulatif_traitement_output;
if (!in_array($output, array("string", "file", "download", "inline", "no"))) {
    $output = "inline"; // Valeur par defaut
}
//
if ($output == "string") {
    // S : renvoyer le document sous forme de chaine. name est ignore.
    $pdf_output = $pdf->Output("", "S");
} elseif ($output == "file") {
    // F : sauver dans un fichier local, avec le nom indique dans name
    // (peut inclure un repertoire).
    $pdf->Output($f->getParameter("pdfdir").$filename, "F");
} elseif ($output == "download") {
    // D : envoyer au navigateur en forcant le telechargement, avec le nom
    // indique dans name.
    $pdf->Output($filename, "D");
} elseif ($output == "inline") {
    // I : envoyer en inline au navigateur. Le plug-in est utilise s'il est
    // installe. Le nom indique dans name est utilise lorsque l'on selectionne
    // "Enregistrer sous" sur le lien generant le PDF.
    $pdf->Output($filename, "I");
} elseif ($output == "no") {
   
}

?>
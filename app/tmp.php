<?php
/**
 *
 */

/**
 *
 */
require_once("../obj/utils.class.php");

// variable $obj
$obj = "tmp";

// new utils
$f = new utils (NULL, $obj,  _("Traces & Editions"));

//
$tab = array ();

//
$dir = $f->getParameter("tmpdir");
$dossier = opendir ($dir);
//
while ($entree = readdir ($dossier)) {
    if ($entree != "." && $entree != ".." && $entree != "index.php" && $entree != ".htaccess" && $entree != "readme.txt" && $entree != "CVS" && $entree != ".svn")
	array_push ($tab, array ('date' => filemtime ($f->getParameter("tmpdir").$entree), 'folder' => 'tmp', 'file' => $entree, 'ext' => array_pop(explode (".", $entree))));  
}
//
closedir ($dossier);
//
$dir = $f->getParameter("pdfdir");
$dossier = opendir ($dir);
//
while ($entree = readdir ($dossier)) {
    if ($entree != "." && $entree != ".." && $entree != "index.php" && $entree != ".htaccess" && $entree != "readme.txt" && $entree != "CVS" && $entree != ".svn")
	array_push ($tab, array ('date' => filemtime ($f->getParameter("pdfdir").$entree), 'folder' => 'pdf', 'file' => $entree, 'ext' => array_pop(explode (".", $entree)))); 
}
//
closedir ($dossier);
//
arsort ($tab);

$col = 0;
echo "<table class=\"file_list\"><tr>";
foreach ($tab as $elem) {
    $col ++;
	echo "<td>";
	echo "<a class=\"om-prev-icon ".$elem['ext']."-16\" href=\"../app/file.php?fic=".$elem ['file']."&amp;folder=".$elem['folder']."\" target=\"_blank\"> ".$elem ['file']."</a>";
	echo "</td>";
	if ($col == 2) 
	{
		echo "</tr><tr>";
		$col = 0;
	}
}
echo "</tr></table>";

?>
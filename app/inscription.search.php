<?php
/**
 * Ce script permet d'afficher le formulaire de recherche de doublon avant
 * d'acceder au formulaire d'inscription.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");

/**
 * Parametrage de la page
 */
//
$page = "inscription_search";
//
$onglet = _("Recherche de doublon");
//
$obj = "inscription";
$ent = _("Saisie")." -> "._("Nouvelle inscription");
$description = _("Ce formulaire de recherche vous permet de faire une ".
                 "verification de doublon sur les inscriptions en cours et ".
                 "sur les electeurs de la liste electorale avant de pouvoir ".
                 "acceder au formulaire d'inscription. Pour effectuer la ".
                 "recherche, il vous faut saisir le nom patronymique et/ou ".
                 "la date de naissance de l'electeur a inscrire puis valider ".
                 "en cliquant sur le bouton. La case a cocher vous permet ".
                 "d'effectuer une recherche exacte sur le nom de l'electeur, ".
                 "si vous la decochez, la recherche s'effectuera seulement ".
                 "sur une partie du nom. Le bouton \"Retour\" vous permet de ".
                 "retourner au tableau de bord.");
$bouton = _("Inscrire");
//
$action = "../app/inscription.search.php";

/**
 * Initialisation des variables
 */
// Initialisation des variables du formulaire
$nom = "";
$datenaissance = "";
// Si les variables arrivent en $_GET
if (isset($_GET['nom']) or isset($_GET['datenaissance'])
    or isset($_GET['exact'])) {
    // Initialisation des variables du formulaire
    (isset($_GET['nom']) ? $nom = $_GET['nom'] : $nom = "");
    (isset($_GET['exact']) && $_GET['exact']==true ? $exact = true : $exact = false);
    (isset($_GET['datenaissance']) ? $datenaissance = $_GET['datenaissance'] : $datenaissance = "");
}
// Si les variables arrivent en $_POST
if (isset($_POST['nom']) or isset($_POST['datenaissance'])
    or isset($_POST['exact'])) {
    // Initialisation des variables du formulaire
    (isset($_POST['nom']) ? $nom = $_POST['nom'] : $nom = "");
    (isset($_POST['exact']) && $_POST['exact']==true ? $exact = true : (isset($exact) ? $exact = $exact : $exact = false));
    (isset($_POST['datenaissance']) ? $datenaissance = $_POST['datenaissance'] : $datenaissance = "");
}

/// Decoche recherche exact si * detecte
if (substr($nom,strlen($nom)-1,1) == '*') {
    $nom = str_replace("*","",$nom);
    if (strlen($nom) >= 1) $exact = false;
}

// Initialisation des variables du formulaire
(!isset($exact) ? $exact = true : $exact = $exact);
// Condition d'erreur
$error_empty = ($nom == "" and $datenaissance == "" ? true : false);
$error_date = ($datenaissance != "" and $f->formatDate($datenaissance) == false ? true : false);

/**
 * Validation du formulaire
 */
//
if (isset($_POST[$page.'_form_action_valid'])) {
    //
    if (!$error_empty) {
        //
        $params = "nom=".urlencode($nom);
        $params .= ($exact == true ? "&exact=".$exact : "");
        $params .= "&datenaissance=".urlencode($datenaissance);
        //
        header ("location:../app/inscription.doublon.php?".$params);
    }
}

/**
 * Parametrage du formulaire
 */
//
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."formulairedyn.class.php";
//
$validation = 0;
$maj = 0;
$champs = array("nom", "exact", "datenaissance");
//
$form = new formulaire(NULL, $validation, $maj, $champs);
//
$form->setLib("nom", _("Nom patronymique"));
$form->setType("nom", "text");
$form->setTaille("nom", 40);
$form->setMax("nom", 60);
$form->setVal("nom", $nom);
$form->setOnchange("nom", "this.value=this.value.toUpperCase()");
//
$form->setLib("exact", _("Recherche exacte"));
$form->setType("exact", "checkbox");
$form->setTaille("exact", 3);
$form->setMax("exact", 3);
$form->setVal("exact", $exact);
//
$form->setLib("datenaissance", _("Date de Naissance"));
$form->setType("datenaissance", "date");
$form->setTaille("datenaissance", 10);
$form->setMax("datenaissance", 10);
$form->setVal("datenaissance", $datenaissance);
$form->setOnchange("datenaissance", "fdate(this)");
//
$form->setGroupe("nom", "D");
$form->setGroupe("exact", "F");

/**
 * Affichage
 */
// Gestion des droits
$f->setRight($obj);
$f->isAuthorized();

// Parametrage du titre de la page
$f->setTitle($ent);

// Affichage de la structure de la page
$f->setFlag(NULL);
$f->display();

// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n\n";

// Affichage de la liste des onglets
echo "<ul>\n";
echo "\t<li><a href=\"#tabs-1\">";
echo $onglet;
echo "</a></li>\n";
echo "</ul>\n";

// Ouverture de la balise - Onglet 1
echo "\n<div id=\"tabs-1\">\n";

// Affichage du message d'erreur si besoin
if (isset($_POST[$page.'_form_action_valid'])) {
    //
    if ($error_empty) {
        $message_class = "error";
        $message = _("Vous devez saisir au moins un critere de recherche.");
        $f->displayMessage($message_class, $message);
    }
}
if ($error_date) {
    $message_class = "error";
    $message = _("La date de naissance n'est pas valide.");
    $f->displayMessage($message_class, $message);
}

// Instructions et description du contenu de l'onglet
$f->displayDescription($description);

// Ouverture de la balise - Formulaire
echo "\n<div id=\"".$page."\" class=\"formulaire\">\n";
echo "<form method=\"post\" id=\"".$page."_form\" ";
echo "name=\"".$page."_form\" ";
echo "action=\"".$action."\">\n";

// Affichage du formulaire
$form->entete();
$form->afficher($champs, $validation, false, false);
$form->enpied();

// Ouverture de la balise - Controles du formulaire
echo "\t<div class=\"formControls\">\n";
// Bouton
echo "\t\t<input name=\"".$page."_form.action.valid\" ";
echo "value=\"".$bouton."\" ";
echo "type=\"submit\" class=\"boutonFormulaire\" />\n";
// Lien retour
echo "<a class=\"retour\" title=\""._("Retour")."\" ";
echo "href=\"../scr/dashboard.php\">";
echo _("Retour");
echo "</a>";
// Fermeture de la balise - Controles du formulaire
echo "\t</div>\n";

// Fermeture de la balise - Formulaire
echo "</form>\n";
echo "</div>\n";

// Fermeture de la balise - Onglet 1
echo "</div>\n";

// Fermeture de la balise - Conteneur d'onglets
echo "\n</div>\n";

?>
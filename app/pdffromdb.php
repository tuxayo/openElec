<?php
/**
 *
 *
 * @package openmairie_exemple
 * @version SVN : $Id: pdf.php 72 2010-09-01 17:52:25Z fmichon $
 */

require_once "../obj/utils.class.php";
if (!isset($f)) { $f = new utils("nohtml"); }

/**
 *
 */
// Nom de l'objet metier
(isset($_GET['obj']) ? $obj = $_GET['obj'] : $obj = "");
//
$multiplicateur = 1;
//
$nolibliste = $_SESSION['liste']." ".strtolower ($_SESSION['libelle_liste']);
$dateTableau = $f -> collectivite ['datetableau'];
if ($f -> formatdate == "AAAA-MM-JJ") {
	$temp = $dateTableau;
	$dateTableauFr = substr($temp,8,2)."/".substr($temp,5,2)."/".substr($temp,0,4);
}
$ville = $f -> collectivite ['ville'];

/**
 * Verification des parametres
 */
if (strpos($obj, "/") !== false
    or !file_exists("../sql/".$f->phptype."/".$obj.".pdf.inc")) {
    $class = "error";
    $message = _("L'objet est invalide.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}

/**
 *
 */
$f->setRight($obj."_pdf");
$f->isAuthorized();

/**
 *
 */
require "../sql/".$f->phptype."/".$obj.".pdf.inc";

/**
 *
 */
//
set_time_limit(180);
//
require_once "../obj/pdf_common.class.php";
//
if (!isset($pdf)) {
    $pdf = new PDF($orientation, 'mm', $format); 
    $pdf->Open();
    $pdf->SetMargins($margeleft, $margetop, $margeright);
    $pdf->AliasNbPages();
    $pdf->SetDisplayMode('real', 'single');
    $pdf->SetDrawColor($C1border, $C2border, $C3border);
}
$pdf->init_pdffromdb($sql, $f->db, $height, $border, $align, $fond, $police, $size, $multiplicateur, $flag_entete);
$pdf->init_header_pdffromdb();
$pdf->AddPage();
$pdf->Table($sql, $f->db, $height, $border, $align, $fond, $police, $size,
            $multiplicateur, $flag_entete);

// Construction du nom du fichier
$filename = date("Ymd-His");
$filename .= "-".$obj;
$filename .= "-".$_SESSION['collectivite'];
$filename .= "-".$f->normalizeString($_SESSION['libelle_collectivite']);
$filename .= "-liste".$_SESSION['liste'];
$filename .= ".pdf";
//
$output = "";
if (isset($_GET["output"])) {
    $output = $_GET["output"];
}
if (!in_array($output, array("string", "file", "download", "inline", "no"))) {
    $output = "inline"; // Valeur par defaut
}
//
if ($output == "string") {
    // S : renvoyer le document sous forme de chaine. name est ignore.
    $pdf_output = $pdf->Output("", "S");
} elseif ($output == "file") {
    // F : sauver dans un fichier local, avec le nom indique dans name
    // (peut inclure un repertoire).
    $pdf->Output($f->getParameter("pdfdir").$filename, "F");
} elseif ($output == "download") {
    // D : envoyer au navigateur en forcant le telechargement, avec le nom
    // indique dans name.
    $pdf->Output($filename, "D");
} elseif ($output == "inline") {
    // I : envoyer en inline au navigateur. Le plug-in est utilise s'il est
    // installe. Le nom indique dans name est utilise lorsque l'on selectionne
    // "Enregistrer sous" sur le lien generant le PDF.
    $pdf->Output($filename, "I");
} elseif ($output == "no") {
    //
    $pdf->reinit_header_pdffromdb();    
}

?>
<?php
/**
 * Ce fichier permet l'affichage d'un recapitulatif des chiffres de la revision
 * electorale en cours en fonction de la date de tableau. Cette page permet
 * d'acceder aux differentes editions de tableaux et a des statistiques.
 *
 * @package openElec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils (NULL, "revision_electorale", _("Revision electorale"));


/**
 *
 */
require_once "../obj/revisionelectorale.class.php";
$revision = new revisionelectorale($f, "fromid", (isset($_GET["revision"]) ? $_GET["revision"] : NULL));

/**
 * Affichage de la page
 */
// Ouverture du conteneur specifique de la page
echo "<div id=\"revision-electorale\">\n";

/**
 *
 */
//
echo "<div class=\"subtitle\">\n";
//
echo "\t<h3 class=\"subtitle-pagination\">\n";
//
if ($revision->is_previous_exists()) {
    echo "\t\t<a class=\"pagination-prev\" title=\""._("Revision precedente")."\" href=\"../app/revision_electorale.php?revision=".$revision->get_previous_id()."\">";
    echo "<span class=\"ui-icon ui-icon-circle-triangle-w\">"._("Revision precedente")."</span>";
    echo "</a>\n";
}
//
echo "\t\t<span class=\"subtitle-pagination\">";
echo $revision->get_title();
echo "</span>\n";
//
if ($revision->is_next_exists()) {
    echo "\t\t<a class=\"pagination-next\" title=\""._("Revision suivante")."\" href=\"../app/revision_electorale.php?revision=".$revision->get_next_id()."\">";
    echo "<span class=\"ui-icon ui-icon-circle-triangle-e\">"._("Revision suivante")."</span>";
    echo "</a>\n";
}

echo "\t</h3>\n";
//
echo "\t<div class=\"visualClear\"><!-- --></div>\n";
//
echo "</div>\n";

//
echo "<div class=\"commune\">";
echo $f->collectivite["ville"];
echo "</div>\n";

//
echo "<div class=\"liste\">";
echo $_SESSION["liste"]." - ".$_SESSION["libelle_liste"];
echo "</div>\n";

/**
 *
 */
//
function showDate($key) {
    global $dates;
    return $dates[$key]["day"]."/".$dates[$key]["month"]."/".$dates[$key]["year"];
}
//
function getDateForDB($key) {
    global $dates;
    return $dates[$key]["year"]."-".$dates[$key]["month"]."-".$dates[$key]["day"];
}
//
$dates = array(
    "aujourdhui" => array(
        "asort" => "",
        "libelle" => _("Aujourd'hui"),
        "day" => date("d"),
        "month" => date("m"),
        "year" => date("Y"),
    ),
    "premierjour" => array(
        "asort" => "",
        "libelle" => _("Premier jour de la revision"),
        "day" => $revision->get_date("debut", "day"),
        "month" => $revision->get_date("debut", "month"),
        "year" => $revision->get_date("debut", "year"),
    ),
    "premiertableau" => array(
        "asort" => "",
        "libelle" => _("Premier tableau rectificatif"),
        "day" => $revision->get_date("tr1", "day"),
        "month" => $revision->get_date("tr1", "month"),
        "year" => $revision->get_date("tr1", "year"),
    ),
    "secondtableau" => array(
        "asort" => "",
        "libelle" => _("Second tableau rectificatif"),
        "day" => $revision->get_date("tr2", "day"),
        "month" => $revision->get_date("tr2", "month"),
        "year" => $revision->get_date("tr2", "year"),
    ),
);
//
$date_tableau_premier_tableau = getDateForDB("premiertableau");
$date_tableau_second_tableau = getDateForDB("secondtableau");
$date_premier_jour = getDateForDB("premierjour");
$aujourdhui = getDateForDB("aujourdhui");

$dpt_cpt_additions = 0;
$dpt_cpt_radiations = 0;

/**
 *
 */
//
function calculNombreDelecteursDate($date_tableau_reference, $electeur_aujourdhui) {
    //
    global $all_mouvement_by_typecat;
    //
    $cpt_electeur = $electeur_aujourdhui;
    //
    foreach ($all_mouvement_by_typecat as $element) {
        //
        if ($element["date_tableau"] > $date_tableau_reference) {
            if ($element["etat"] == "trs") {
                if ($element["typecat"] == "Inscription") {
                    $cpt_electeur -= $element["count"];
                } elseif ($element["typecat"] == "Radiation") {
                    $cpt_electeur += $element["count"];
                }
            }
        } else {
            if ($element["etat"] == "actif") {
                if ($element["typecat"] == "Inscription") {
                    $cpt_electeur += $element["count"];
                } elseif ($element["typecat"] == "Radiation") {
                    $cpt_electeur -= $element["count"];
                }
            }
        }
    }
    return $cpt_electeur;
}
/**
 *
 */
require "../sql/".OM_DB_PHPTYPE."/revision_electorale.inc";

/**
 *
 */
// Liste des tableaux des cinq jours deja traites
$res_list_tableau_des_cinq_jours = $f->db->query($query_list_tableau_des_cinq_jours);
// Logger
$f->addToLog("app/revision_electoral.php: db->query(\"".$query_list_tableau_des_cinq_jours."\")", VERBOSE_MODE);
// Gestion des erreurs
$f->isDatabaseError($res_list_tableau_des_cinq_jours);
//
$cpt = 0;
while ($row_list_tableau_des_cinq_jours =& $res_list_tableau_des_cinq_jours->fetchRow(DB_FETCHMODE_ASSOC)) {
    //
    $cpt++;
    $dates["election".$cpt] = array(
        "asort" => $row_list_tableau_des_cinq_jours["date_j5"],
        "libelle" => _("Tableau des cinq jours"),
        "day" => substr($row_list_tableau_des_cinq_jours["date_j5"], 8, 2),
        "month" => substr($row_list_tableau_des_cinq_jours["date_j5"], 5, 2),
        "year" => substr($row_list_tableau_des_cinq_jours["date_j5"], 0, 4),);
}
//
$f->addToLog("app/revision_electoral.php: ".print_r($dates, true), VERBOSE_MODE);


// Nombre actuel d'electeurs dans la base de donnees
$res_count_electeur = $f->db->getone($query_count_electeur);
// Logger
$f->addToLog("app/revision_electoral.php: db->getone(\"".$query_count_electeur."\")", VERBOSE_MODE);
// Gestion des erreurs
$f->isDatabaseError($res_count_electeur);

// 
$res_count_all_mouvement_by_typecat = $f->db->query($query_count_all_mouvement_by_typecat);
// Logger
$f->addToLog("app/revision_electoral.php: db->query(\"".$query_count_all_mouvement_by_typecat."\")", VERBOSE_MODE);
// Gestion des erreurs
$f->isDatabaseError($res_count_all_mouvement_by_typecat);
//
$all_mouvement_by_typecat = array();
while ($row_count_all_mouvement_by_typecat =& $res_count_all_mouvement_by_typecat->fetchRow(DB_FETCHMODE_ASSOC)) {
    array_push($all_mouvement_by_typecat, $row_count_all_mouvement_by_typecat);
}
$f->addToLog("app/revision_electorale.php : ".print_r($all_mouvement_by_typecat, true), EXTRA_VERBOSE_MODE);



/**
 *
 */
foreach ($dates as $key => $date) {
    //
    $dates[$key]["asort"] = getDateForDB($key);
}
asort($dates);


echo "\n<!-- -->\n";
echo "<table class=\"revisionelectorale\">\n";


echo "<tr class=\"nb-electeur";
if ($aujourdhui > $date_premier_jour) {
    echo " past";
} else {
    echo " futur";
}
echo "\">";
echo "<th>";
echo showDate("premierjour");
echo "</th>";
echo "<td>";
echo "</td>";
echo "<td class=\"type\">";
echo _("Nombre d'electeur(s)");
echo "</td>";
echo "<td class=\"signe egal\">";
echo "=";
echo "</td>";
echo "<td class=\"chiffre\">";
echo calculNombreDelecteursDate(
    $revision->get_date("debut"),
    $res_count_electeur, 
    getDateForDB("aujourdhui")
);
echo "</td>";
echo "</tr>\n";


////////
// DETAIL DU PREMIER TABLEAU
////////
//
//
echo "<tr class=\"";
if ($aujourdhui > $date_tableau_premier_tableau) {
    echo " past";
} else {
    echo " futur";
}
echo "\">";
//
echo "<th rowspan=\"3\">";
echo "</th>";
//
echo "<td colspan=\"3\" class=\"detail-premier-tableau\">\n";
//
echo "<fieldset class=\"cadre ui-corner-all ui-widget-content collapsible\">\n";
echo "<legend class=\"ui-corner-all ui-widget-content ui-state-active\">";
echo _("Detail du premier tableau rectificatif");
echo "</legend>\n";
//
echo "<table class=\"detail-premier-tableau\">\n";
////// Titre du tableau
//echo "<tr>";
//echo "<th colspan=\"5\">";
//echo _("Detail du premier tableau rectificatif");
//echo "</th>";
//echo "</tr>";
////// Nombre d'electeurs au premier jour

echo "<tr class=\"nb-electeur";
if ($aujourdhui > $date_premier_jour) {
    echo " past";
} else {
    echo " futur";
}
echo "\">";
echo "<th>";
echo showDate("premierjour");
echo "</th>";
echo "<td>";
echo "</td>";
echo "<td class=\"type\">";
echo _("Nombre d'electeur(s)");
echo "</td>";
echo "<td class=\"signe egal\">";
echo "=";
echo "</td>";
echo "<td class=\"chiffre\">";
$dpt_nb_electeur_courant = calculNombreDelecteursDate(
    $revision->get_date("debut"),
    $res_count_electeur, 
    getDateForDB("aujourdhui")
);
echo $dpt_nb_electeur_courant;
echo "</td>";
echo "</tr>\n";
////// Gestion des tableaux j5 deja traites
foreach ($dates as $key => $date) {
    //
    if ($f->startswith($key, "election")) {
        ////// Comptage
        $cpt_addition = 0;
        $cpt_radiation = 0;
        $cpt_jeune = 0;
        $cpt_jeune_libelle = "";
        foreach ($all_mouvement_by_typecat as $element) {
            if ($element["date_tableau"] == $date_tableau_premier_tableau) {
                if ($element["date_j5"] == getDateForDB($key)) {
                    if ($element["etat"] == "trs") {
                        if ($element["typecat"] == "Inscription") {
                            //echo "- ".$element["date_tableau"]." ".$element["typecat"]."<br/>";
                            if ($element["effet"] == "Election") {
                                $cpt_jeune += $element["count"];
                                $cpt_jeune_code = $element["types"];
                                $cpt_jeune_libelle = $element["libelle"];
                            } else {
                                $cpt_addition += $element["count"];
                            }
                        } elseif ($element["typecat"] == "Radiation") {
                            //echo "+ ".$element["date_tableau"]." ".$element["typecat"]."<br/>";
                            $cpt_radiation += $element["count"];
                        }
                    }
                }
            }
        }
        ////// Tableau des additions
        // Seulement si il y en a
        if ($cpt_jeune != 0) {
            //
            echo "<tr class=\"past\">";
            echo "<th>";
            echo "</th>";
            echo "<td class=\"tableau\">";
            echo "<span class=\"tableau\">";
            echo _("Tableau des additions")."<br/>";
            echo "</span>";
            echo $cpt_jeune_libelle;
            //
            $links = array(
                "2" => array(
                    "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_des_additions_des_jeunes&amp;type=".$cpt_jeune_code."&amp;datetableau=".getDateForDB("premiertableau")."&amp;datej5=".getDateForDB($key),
                    "target" => "_blank",
                    "class" => "om-prev-icon edition-16",
                    "title" => _("Tableau (par bureau)"),
                    "description" => _("Tableau des additions avec coupure par bureau"),
                ),
                "1" => array(
                    "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_des_additions_des_jeunes&amp;type=".$cpt_jeune_code."&amp;globale=true&amp;datetableau=".getDateForDB("premiertableau")."&amp;datej5=".getDateForDB($key),
                    "target" => "_blank",
                    "class" => "om-prev-icon edition-16",
                    "title" => _("Tableau (commune)"),
                    "description" => _("Tableau des additions sur toute la commune"),
                ),
                "0" => array(
                    "href" => "../app/pdfetiquette.php?obj=additions_des_jeunes&amp;type=".$cpt_jeune_code."&amp;datetableau=".getDateForDB("premiertableau")."&amp;datej5=".getDateForDB($key),
                    "target" => "_blank",
                    "class" => "om-prev-icon edition-16",
                    "title" => _("Etiquettes"),
                    "description" => _("Etiquettes"),
                ),
            );
            //
            echo "<ul style=\"list-style-type: none;\">\n";
            foreach ($links as $link) {
                echo "\t<li style=\"float:right;margin:5px;\">";
                echo "<a title=\"".$link["description"]."\" class=\"".$link["class"]."\" href=\"".$link["href"]."\" target=\"".$link["target"]."\">".$link["title"]."</a>";
                echo "</li>\n";
            }
            echo "</ul>\n";
            //
            echo "</td>";
            echo "<td class=\"type\">";
            echo "Addition(s)";
            echo "</td>";
            echo "<td class=\"signe plus\">";
            echo "+";
            echo "</td>";
            echo "<td class=\"chiffre\">";
            echo $cpt_jeune;
            $dpt_nb_electeur_courant += $cpt_jeune;
            $dpt_cpt_additions += $cpt_jeune;
            echo "</td>";
            echo "</tr>\n";
        }
        ////// Tableau des cinq jours - Additions
        echo "<tr class=\"past\">";
        echo "<th rowspan=\"2\">";
        echo "</th>";
        echo "<td rowspan=\"2\" class=\"tableau\">";
        echo "<span class=\"tableau\">";
        echo _("Tableau des cinq jours");
        echo "</span>";
        echo "<br/>".showDate($key);
        //
        $links = array(
            "2" => array(
                "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_des_cinq_jours&amp;datetableau=".getDateForDB("premiertableau")."&amp;datej5=".getDateForDB($key),
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => _("Tableau (par bureau)"),
                "description" => _("Tableau des cinq jours avec coupure par bureau"),
            ),
            "1" => array(
                "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_des_cinq_jours&amp;globale=true&amp;datetableau=".getDateForDB("premiertableau")."&amp;datej5=".getDateForDB($key),
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => _("Tableau (commune)"),
                "description" => _("Tableau des cinq jours sur toute la commune"),
            ),
        );
        //
        echo "<ul style=\"list-style-type: none;\">\n";
        foreach ($links as $link) {
            echo "\t<li style=\"float:right;margin:5px;\">";
            echo "<a title=\"".$link["description"]."\" class=\"".$link["class"]."\" href=\"".$link["href"]."\" target=\"".$link["target"]."\">".$link["title"]."</a>";
            echo "</li>\n";
        }
        echo "</ul>\n";
        //
        echo "</td>";
        echo "<td class=\"type\">";
        echo "Addition(s)";
        echo "</td>";
        echo "<td class=\"signe plus\">";
        echo "+";
        echo "</td>";
        echo "<td class=\"chiffre\">";
        echo $cpt_addition;
        $dpt_nb_electeur_courant += $cpt_addition;
        $dpt_cpt_additions += $cpt_addition;
        echo "</td>";
        echo "</tr>\n";
        ////// Tableau des cinq jours - Radiations
        echo "<tr class=\"past\">";
        echo "<td class=\"type\">";
        echo "Radiation(s)";
        echo "</td>";
        echo "<td class=\"signe moins\">";
        echo "-";
        echo "</td>";
        echo "<td class=\"chiffre\">";
        echo $cpt_radiation;
        $dpt_nb_electeur_courant -= $cpt_radiation;
        $dpt_cpt_radiations += $cpt_radiation;
        echo "</td>";
        echo "</tr>\n";
        ////// Nombre d'electeurs a la date des cinq jours
        echo "<tr class=\"nb-electeur past\">";
        echo "<th>";
        echo showDate($key);
        echo "</th>";
        echo "<td>";
        echo _("Traitement J-5");
        $links = array(
            "1" => array(
                "href" => "../app/pdf_recapitulatif_traitement.php?action=recapitulatif&amp;traitement=j5&amp;datetableau=".getDateForDB("premiertableau")."&amp;datej5=".getDateForDB($key),
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => _("Recapitulatif"),
                "description" => _("..."),
            ),
            "0" => array(
                "href" => "../pdf/carteelecteur.php?mode_edition=traitement&amp;traitement=j5&amp;datetableau=".getDateForDB("premiertableau")."&amp;datej5=".getDateForDB($key),
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => _("Nouvelles cartes electorales"),
                "description" => _("Toutes les nouvelles cartes d'electeur qui doivent etre editees suite au traitement J-5 de cette date"),
            ),
        );
        //
        echo "<ul style=\"list-style-type: none;\">\n";
        foreach ($links as $link) {
            echo "\t<li style=\"float:right;margin:5px;\">";
            echo "<a title=\"".$link["description"]."\" class=\"".$link["class"]."\" href=\"".$link["href"]."\" target=\"".$link["target"]."\">".$link["title"]."</a>";
            echo "</li>\n";
        }
        echo "</ul>\n";
        echo "</td>";
        echo "<td class=\"type\">";
        echo _("Nombre d'electeur(s)");
        echo "</td>";
        echo "<td class=\"signe egal\">";
        echo "=";
        echo "</td>";
        echo "<td class=\"chiffre\">";
        echo $dpt_nb_electeur_courant;
        echo "</td>";
        echo "</tr>\n";
    }
}
////// Gestion des tableaux futurs
////// Comptage
$cpt_addition = 0;
$cpt_radiation = 0;
//
$tableaux_additions = array();
//
foreach ($all_mouvement_by_typecat as $element) {
    //
    if ($element["date_tableau"] == $date_tableau_premier_tableau) {
        if ($element["date_j5"] == "") {
            if ($element["etat"] == "actif") {
                if ($element["typecat"] == "Inscription") {
                    if ($element["effet"] == "Election") {
                        $tableaux_additions[$element["types"]] = array(
                            "libelle" => $element["libelle"],
                            "count" => $element["count"]
                        );
                    } elseif ($element["effet"] == "Immediat") {
                        $cpt_addition += $element["count"];
                    }
                } elseif ($element["typecat"] == "Radiation") {
                    if ($element["effet"] == "Immediat") {
                        $cpt_radiation += $element["count"];
                    }
                }
            }
        }
    }
}
// Seulement si la date de tableau est superieure a la date du jour
// ou si il y aun tableau a effet election
if ($date_tableau_premier_tableau > $aujourdhui
    || (count($tableaux_additions) != 0 
    && $date_tableau_premier_tableau <= $aujourdhui)
) {
    ////// Tableau des additions
    foreach ($tableaux_additions as $type => $tableau_additions) {
        //
        echo "<tr class=\"futur\">";
        echo "<th>";
        echo "</th>";
        echo "<td class=\"tableau\">";
        echo "<span class=\"tableau\">";
        echo _("Tableau des additions")."<br/>";
        echo "</span>";
        echo $tableau_additions["libelle"];
        //
        $links = array(
            "2" => array(
                "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_des_additions_des_jeunes&amp;type=".$type."&amp;datetableau=".getDateForDB("premiertableau"),
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => _("Tableau (par bureau)"),
                "description" => _("Tableau des additions avec coupure par bureau"),
            ),
            "1" => array(
                "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_des_additions_des_jeunes&amp;type=".$type."&amp;globale=true&amp;datetableau=".getDateForDB("premiertableau"),
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => _("Tableau (commune)"),
                "description" => _("Tableau des additions sur toute la commune"),
            ),
            "0" => array(
                "href" => "../app/pdfetiquette.php?obj=additions_des_jeunes&amp;type=".$type."&amp;datetableau=".getDateForDB("premiertableau"),
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => _("Etiquettes"),
                "description" => _("Etiquettes"),
            ),
        );
        //
        echo "<ul style=\"list-style-type: none;\">\n";
        foreach ($links as $link) {
            echo "\t<li style=\"float:right;margin:5px;\">";
            echo "<a title=\"".$link["description"]."\" class=\"".$link["class"]."\" href=\"".$link["href"]."\" target=\"".$link["target"]."\">".$link["title"]."</a>";
            echo "</li>\n";
        }
        echo "</ul>\n";
        //
        echo "</td>";
        echo "<td class=\"type\">";
        echo "Addition(s)";
        echo "</td>";
        echo "<td class=\"signe plus\">";
        echo "+";
        echo "</td>";
        echo "<td class=\"chiffre\">";
        echo $tableau_additions["count"];
        $dpt_nb_electeur_courant += $tableau_additions["count"];
        $dpt_cpt_additions += $tableau_additions["count"];
        echo "</td>";
        echo "</tr>\n";
        //
    }
    if ($date_tableau_premier_tableau > $aujourdhui) {
        ////// Tableau des cinq jours - Additions
        echo "<tr class=\"futur\">";
        echo "<th rowspan=\"2\">";
        echo "</th>";
        echo "<td rowspan=\"2\" class=\"tableau\">";
        echo "<span class=\"tableau\">";
        echo _("Tableau des cinq jours");
        echo "</span>";
        echo "<br/>"."XX/XX/XXXX";
        //
        $links = array(
            "2" => array(
                "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_des_cinq_jours&amp;datetableau=".getDateForDB("premiertableau"),
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => _("Tableau (par bureau)"),
                "description" => _("Tableau des cinq jours avec coupure par bureau"),
            ),
            "1" => array(
                "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_des_cinq_jours&amp;globale=true&amp;datetableau=".getDateForDB("premiertableau"),
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => _("Tableau (commune)"),
                "description" => _("Tableau des cinq jours sur toute la commune"),
            ),
        );
        //
        echo "<ul style=\"list-style-type: none;\">\n";
        foreach ($links as $link) {
            echo "\t<li style=\"float:right;margin:5px;\">";
            echo "<a title=\"".$link["description"]."\" class=\"".$link["class"]."\" href=\"".$link["href"]."\" target=\"".$link["target"]."\">".$link["title"]."</a>";
            echo "</li>\n";
        }
        echo "</ul>\n";
        //
        echo "</td>";
        echo "<td class=\"type\">";
        echo "Addition(s)";
        echo "</td>";
        echo "<td class=\"signe plus\">";
        echo "+";
        echo "</td>";
        echo "<td class=\"chiffre\">";
        echo $cpt_addition;
        $dpt_nb_electeur_courant += $cpt_addition;
        $dpt_cpt_additions += $cpt_addition;
        echo "</td>";
        echo "</tr>\n";
        ////// Tableau des cinq jours - Radiations
        echo "<tr class=\"futur\">";
        echo "<td class=\"type\">";
        echo "Radiation(s)";
        echo "</td>";
        echo "<td class=\"signe moins\">";
        echo "-";
        echo "</td>";
        echo "<td class=\"chiffre\">";
        echo $cpt_radiation;
        $dpt_nb_electeur_courant -= $cpt_radiation;
        $dpt_cpt_radiations += $cpt_radiation;
        echo "</td>";
        echo "</tr>\n";
        ////// Nombre d'electeurs a la date des cinq jours
        echo "<tr class=\"futur nb-electeur\">";
        echo "<th>";
        echo "XX/XX/XXXX";
        echo "</th>";
        echo "<td>";
        echo "</td>";
        echo "<td class=\"type\">";
        echo _("Nombre d'electeur(s)");
        echo "</td>";
        echo "<td class=\"signe egal\">";
        echo "=";
        echo "</td>";
        echo "<td class=\"chiffre\">";
        echo $dpt_nb_electeur_courant;
        echo "</td>";
        echo "</tr>\n";
    }
}
//

////// Comptage
$cpt_addition = array("total" => 0, "trs" => 0, "actif" => 0);
$cpt_radiation = array("total" => 0, "trs" => 0, "actif" => 0);
foreach ($all_mouvement_by_typecat as $element) {
    if ($element["date_tableau"] == getDateForDB("premiertableau")) {
        if ($element["typecat"] == "Inscription") {
            $cpt_addition["total"] += $element["count"];
            if ($element["etat"] == "trs") {
                $cpt_addition["trs"] += $element["count"];
            } else {
                $cpt_addition["actif"] += $element["count"];
            }
        } elseif ($element["typecat"] == "Radiation") {
            $cpt_radiation["total"] += $element["count"];
            if ($element["etat"] == "trs") {
                $cpt_radiation["trs"] += $element["count"];
            } else {
                $cpt_radiation["actif"] += $element["count"];
            }
        }
        //elseif ($element["typecat"] == "Modification") {
        //    if ($element["code_bureau"] != $element["ancien_bureau"]) {
        //        if ($element["etat"] == "trs") {
        //            $cpt_radiation["trs"] += $element["count"];
        //            $cpt_addition["trs"] += $element["count"];
        //        } else {
        //            $cpt_radiation["actif"] += $element["count"];
        //            $cpt_addition["actif"] += $element["count"];
        //        }
        //    }
        //}
    }
}
//
$sql = "select count(id) 
        from mouvement inner join param_mouvement 
        on mouvement.types=param_mouvement.code
        where date_tableau='".getDateForDB("premiertableau")."' 
        and etat='actif' 
        and effet<>'Election'
        and collectivite='".$_SESSION["collectivite"]."' 
        and liste='".$_SESSION["liste"]."'";
$mouvements_actifs_premier_tableau = $f->db->getone($sql);
$f->isDatabaseError($mouvements_actifs_premier_tableau);
//
if ($aujourdhui > $date_tableau_premier_tableau && $mouvements_actifs_premier_tableau == 0) {
    echo "<tr class=\"past\">";
} else {
    echo "<tr class=\"futur\">";
}
echo "<th rowspan=\"2\">";
echo "</th>";
echo "<td rowspan=\"2\" class=\"tableau\">";
echo "<span class=\"tableau\">";
echo _("Autre(s) mouvement(s)");
echo "</span>";
echo "</td>";
echo "<td class=\"type\">";
echo "Addition(s)";
echo "</td>";
echo "<td class=\"signe plus\">";
echo "+";
echo "</td>";
echo "<td class=\"chiffre\">";
echo ($cpt_addition["total"] - $dpt_cpt_additions);
echo "</td>";
echo "</tr>\n";
////// Tableau des cinq jours - Radiations
if ($aujourdhui > $date_tableau_premier_tableau && $mouvements_actifs_premier_tableau == 0) {
    echo "<tr class=\"past\">";
} else {
    echo "<tr class=\"futur\">";
}
echo "<td class=\"type\">";
echo "Radiation(s)";
echo "</td>";
echo "<td class=\"signe moins\">";
echo "-";
echo "</td>";
echo "<td class=\"chiffre\">";
echo ($cpt_radiation["total"] - $dpt_cpt_radiations);
echo "</td>";
echo "</tr>\n";
//////
echo "<tr class=\"nb-electeur\">";
echo "<th>";
echo showDate("premiertableau");
echo "</th>";
echo "<td>";
//
if ($aujourdhui > $date_tableau_premier_tableau && $mouvements_actifs_premier_tableau == 0) {
    //
    echo _("Traitement annuel");
    //
    $links = array(
        "1" => array(
            "href" => "../app/pdf_recapitulatif_traitement.php?action=recapitulatif&amp;traitement=annuel&amp;datetableau=".getDateForDB("premiertableau"),
            "target" => "_blank",
            "class" => "om-prev-icon edition-16",
            "title" => _("Recapitulatif"),
            "description" => _("..."),
        ),
        "0" => array(
            "href" => "../pdf/carteelecteur.php?mode_edition=traitement&amp;traitement=annuel&amp;datetableau=".getDateForDB("premiertableau"),
            "target" => "_blank",
            "class" => "om-prev-icon edition-16",
            "title" => _("Nouvelles cartes electorales"),
            "description" => _("Toutes les nouvelles cartes d'electeur qui doivent etre editees suite au traitement annuel"),
        ),
    );
    //
    echo "<ul style=\"list-style-type: none;\">\n";
    foreach ($links as $link) {
        echo "\t<li style=\"float:right;margin:5px;\">";
        echo "<a title=\"".$link["description"]."\" class=\"".$link["class"]."\" href=\"".$link["href"]."\" target=\"".$link["target"]."\">".$link["title"]."</a>";
        echo "</li>\n";
    }
    echo "</ul>\n";
}
echo "</td>";
echo "<td class=\"type\">";
echo _("Nombre d'electeur(s)");
echo "</td>";
echo "<td class=\"signe egal\">";
echo "=";
echo "</td>";
echo "<td class=\"chiffre\">";
echo calculNombreDelecteursDate(getDateForDB("premiertableau"), $res_count_electeur, getDateForDB("aujourdhui"));
echo "</td>";
echo "</tr>\n";
//
echo "</table>";
//
echo "</fieldset>";
//
echo "</td>";
//
echo "<td>";
echo "</td>";
//
echo "</tr>";
////////
// FIN DETAIL DU PREMIER TABLEAU
////////


if ($aujourdhui > $date_tableau_premier_tableau && $mouvements_actifs_premier_tableau == 0) {
    echo "<tr class=\"past\">";
} else {
    echo "<tr class=\"futur\">";
}

//echo "<th rowspan=\"2\">";
//echo "</th>";
echo "<td rowspan=\"2\" class=\"tableau\">";
echo "<span class=\"tableau\">";
echo _("Premier tableau rectificatif");
echo "</span>";
echo "<br/>".showDate("premiertableau");
//
$links = array(
    "1" => array(
        "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_rectificatif&amp;datetableau=".getDateForDB("premiertableau"),
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Tableau (par bureau)"),
        "description" => _("Tableau rectificatif avec coupure par bureau"),
    ),
    "2" => array(
        "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_rectificatif&amp;datetableau=".getDateForDB("premiertableau")."&amp;globale=true",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Tableau (commune)"),
        "description" => _("Tableau rectificatif avec coupure par bureau"),
    ),
    "0" => array(
        "href" => "../pdf/pdffromarray.php?obj=recapitulatif_tableau&amp;tableau=".getDateForDB("premiertableau"),
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Recapitulatif"),
        "description" => _("Cette edition recapitule le nombre d'electeurs au tableau precedent, ".
                           "le nombre d'inscriptions, le nombre de transferts entre bureau, le ".
                           "nombre de radiations et le nombre d'electeurs a la date de tableau ".
                           "du tableau rectificatif en question. Tous ces nombres etant detailles ".
                           "par bureau."),
    ),


);
//
echo "<ul style=\"list-style-type: none;\">\n";
foreach ($links as $link) {
    echo "\t<li style=\"float:right;margin:5px;\">";
    echo "<a title=\"".$link["description"]."\" class=\"".$link["class"]."\" href=\"".$link["href"]."\" target=\"".$link["target"]."\">".$link["title"]."</a>";
    echo "</li>\n";
}
echo "</ul>\n";
//
echo "</td>";
echo "<td class=\"type\">";
echo "Addition(s)";
echo "</td>";
echo "<td class=\"signe plus\">";
echo "+";
echo "</td>";
echo "<td class=\"chiffre\">";
echo $cpt_addition["total"];
echo "</td>";
echo "</tr>";
//
if ($aujourdhui > $date_tableau_premier_tableau && $mouvements_actifs_premier_tableau == 0) {
    echo "<tr class=\"past\">";
} else {
    echo "<tr class=\"futur\">";
}
echo "<td class=\"type\">";
echo "Radiation(s)";
echo "</td>";
echo "<td class=\"signe moins\">";
echo "-";
echo "</td>";
echo "<td class=\"chiffre\">";
echo $cpt_radiation["total"];
echo "</td>";
echo "</tr>";

//
echo "<tr class=\"nb-electeur";
if ($aujourdhui > $date_tableau_premier_tableau && $mouvements_actifs_premier_tableau == 0) {
    echo " past";
} else {
    echo " futur";
}
echo "\">";
echo "<th>";
echo showDate("premiertableau");
echo "</th>";
echo "<td>";
echo "</td>";
echo "<td class=\"type\">";
echo _("Nombre d'electeur(s)");
echo "</td>";
echo "<td class=\"signe egal\">";
echo "=";
echo "</td>";
echo "<td class=\"chiffre\">";
echo calculNombreDelecteursDate(getDateForDB("premiertableau"), $res_count_electeur, getDateForDB("aujourdhui"));
echo "</td>";
echo "</tr>";

//
$sql = "select count(id) from mouvement where date_tableau='".getDateForDB("secondtableau")."' and etat='actif' and collectivite='".$_SESSION["collectivite"]."' and liste='".$_SESSION["liste"]."'";
$mouvements_actifs_second_tableau = $f->db->getone($sql);
$f->isDatabaseError($mouvements_actifs_premier_tableau);
//
$cpt_addition = 0;
$cpt_radiation = 0;
$flag_actif = false;
foreach ($all_mouvement_by_typecat as $element) {
    if ($element["date_tableau"] == getDateForDB("secondtableau")) {
        if ($element["typecat"] == "Inscription") {
            $cpt_addition += $element["count"];
        } elseif ($element["typecat"] == "Radiation") {
            $cpt_radiation += $element["count"];
        }
    }
}
echo "<tr class=\"";
if ($aujourdhui > $date_tableau_second_tableau && $mouvements_actifs_second_tableau == 0) {
    echo " past";
} else {
    echo " futur";
}
echo "\">";
echo "<th rowspan=\"2\">";
echo "</th>";
echo "<td rowspan=\"2\" class=\"tableau\">";
echo "<span class=\"tableau\">";
echo _("Second tableau rectificatif");
echo "</span>";
echo "<br/>".showDate("secondtableau");
$links = array(

    "1" => array(
        "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_rectificatif&amp;datetableau=".getDateForDB("secondtableau"),
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Tableau (par bureau)"),
        "description" => _("Tableau rectificatif avec coupure par bureau"),
    ),
    "2" => array(
        "href" => "../pdf/commission.php?mode_edition=tableau&amp;tableau=tableau_rectificatif&amp;datetableau=".getDateForDB("secondtableau")."&globale=true",
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Tableau (commune)"),
        "description" => _("Tableau rectificatif"),
    ),
    "0" => array(
        "href" => "../pdf/pdffromarray.php?obj=recapitulatif_tableau&amp;tableau=".getDateForDB("secondtableau"),
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Recapitulatif"),
        "description" => _("Cette edition recapitule le nombre d'electeurs au tableau precedent, ".
                           "le nombre d'inscriptions, le nombre de transferts entre bureau, le ".
                           "nombre de radiations et le nombre d'electeurs a la date de tableau ".
                           "du tableau rectificatif en question. Tous ces nombres etant detailles ".
                           "par bureau."),
    ),
    "3" => array(
        "href" => "../pdf/carteelecteur.php?mode_edition=traitement&amp;traitement=annuel&amp;datetableau=".getDateForDB("secondtableau"),
        "target" => "_blank",
        "class" => "om-prev-icon edition-16",
        "title" => _("Nouvelles cartes electorales"),
        "description" => _("Toutes les nouvelles cartes d'electeur qui doivent etre editees suite au traitement annuel"),
    ),
);
//
echo "<ul style=\"list-style-type: none;\">\n";
foreach ($links as $link) {
    echo "\t<li style=\"float:right;margin:5px;\">";
    echo "<a title=\"".$link["description"]."\" class=\"".$link["class"]."\" href=\"".$link["href"]."\" target=\"".$link["target"]."\">".$link["title"]."</a>";
    echo "</li>\n";
}
echo "</ul>\n";
//
echo "</td>";
echo "<td class=\"type\">";
echo "Addition(s)";
echo "</td>";
echo "<td class=\"signe plus\">";
echo "+";
echo "</td>";
echo "<td class=\"chiffre\">";
echo $cpt_addition;
echo "</td>";
echo "</tr>";
echo "<tr class=\"";
if ($aujourdhui > $date_tableau_second_tableau && $mouvements_actifs_second_tableau == 0) {
    echo " past";
} else {
    echo " futur";
}
echo "\">";
echo "<td class=\"type\">";
echo "Radiation(s)";
echo "</td>";
echo "<td class=\"signe moins\">";
echo "-";
echo "</td>";
echo "<td class=\"chiffre\">";
echo $cpt_radiation;
echo "</td>";
echo "</tr>";

//
echo "<tr class=\"nb-electeur";
if ($aujourdhui > $date_tableau_second_tableau && $mouvements_actifs_second_tableau == 0) {
    echo " past";
} else {
    echo " futur";
}
echo "\">";
echo "<th>";
echo showDate("secondtableau");
echo "</th>";
echo "<td>";
echo "</td>";
echo "<td class=\"type\">";
echo _("Nombre d'electeur(s)");
echo "</td>";
echo "<td class=\"signe egal\">";
echo "=";
echo "</td>";
echo "<td class=\"chiffre\">";
echo calculNombreDelecteursDate(getDateForDB("secondtableau"), $res_count_electeur, getDateForDB("aujourdhui"));
echo "</td>";
echo "</tr>";
//
echo "</table>";


/**
 *
 */
//
echo "<table class=\"revisionelectorale legende\">";
//
echo "<tr class=\"nb-electeur\">";
echo "<td>";
echo _("Legende");
echo "</td>";
echo "</tr>";
//
echo "<tr class=\"past\">";
echo "<td>";
echo _("Chiffres definitifs : date de tableau dans le passe");
echo "</td>";
echo "</tr>";
//
echo "<tr class=\"futur\">";
echo "<td>";
echo _("Chiffres provisoires non definitifs : date de tableau dans le futur");
echo "</td>";
echo "</tr>";
echo "</table>";

/**
 * Affichage de la page
 */
// Fermeture du conteneur specifique de la page
echo "\n</div>\n";

?>

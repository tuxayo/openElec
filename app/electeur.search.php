<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");

/**
 * Parametrage de la page
 */
//
$page = "electeur_search";
//
$onglet = _("Recherche d'electeur");
//
(isset($_GET['obj']) ? $obj = $_GET['obj'] : $obj = "electeur_mouvement");
switch ($obj) {
    //
    case "electeur_modification" :
        $ent = _("Saisie")." -> "._("Modification");
        $description = _("Ce formulaire de recherche vous permet de saisir le nom ".
                         "patronymique et/ou le nom d'usage/marital et/ou le prenom ".
                         "et/ou la date de naissance de l'electeur sur lequel vous ".
                         "souhaitez creer un mouvement de modification. En cliquant ".
                         "sur le bouton vous obtiendrez la liste des electeurs ".
                         "correspondants a votre recherche.");
        $bouton = _("Rechercher l'electeur a modifier");
        break;
    //
    case "electeur_radiation" :
        $ent = _("Saisie")." -> "._("Radiation");
        $description = _("Ce formulaire de recherche vous permet de saisir le nom ".
                         "patronymique et/ou le nom d'usage/marital et/ou le prenom ".
                         "et/ou la date de naissance de l'electeur sur lequel vous ".
                         "souhaitez creer un mouvement de radiation. En cliquant ".
                         "sur le bouton vous obtiendrez la liste des electeurs ".
                         "correspondants a votre recherche.");
        $bouton = _("Rechercher l'electeur a radier");
        break;
    case "electeur" :
        $ent = _("Saisie")." -> "._("Carte en Retour / Jure");
        $description = _("Ce formulaire de recherche vous permet de saisir le nom ".
                         "patronymique et/ou le nom d'usage/marital et/ou le prenom ".
                         "et/ou la date de naissance de l'electeur sur lequel vous ".
                         "souhaitez modifier les informations concernant sa carte en ".
                         "retour ou sa participation a un jury d'assise. En cliquant ".
                         "sur le bouton vous obtiendrez la liste des electeurs ".
                         "correspondants a votre recherche.");
        $bouton = _("Rechercher l'electeur a modifier");
        break;
    //
    default :
        $obj = "electeur_mouvement";
        $ent = _("Saisie")." -> "._("Modification / Radiation");
        $description = _("Ce formulaire de recherche vous permet de saisir le nom ".
                         "patronymique et/ou le nom d'usage/marital et/ou le prenom ".
                         "et/ou la date de naissance de l'electeur sur lequel vous ".
                         "souhaitez creer un mouvement de modification/radiation. En ".
                         "cliquant sur le bouton vous obtiendrez la liste des ".
                         "electeurs correspondants a votre recherche.");
        $bouton = _("Rechercher l'electeur a modifier / radier");
}
$action = "../app/electeur.search.php?obj=".$obj;

/**
 * Initialisation des variables
 */
// Initialisation des variables du formulaire
$nom = "";
$prenom = "";
$marital = "";
$datenaissance = "";


// Si les variables arrivent en $_GET
if (isset($_GET['nom']) or isset($_GET['exact']) or isset($_GET['datenaissance'])
    or isset($_GET['prenom']) or isset($_GET['marital'])) {
    // Initialisation des variables du formulaire
    (isset($_GET['nom']) ? $nom = $_GET['nom'] : $nom = "");
    (isset($_GET['exact']) && $_GET['exact']==true ? $exact = true : $exact = false);
    (isset($_GET['prenom']) ? $prenom = $_GET['prenom'] : $prenom = "");
    (isset($_GET['marital']) ? $marital = $_GET['marital'] : $marital = "");
    (isset($_GET['datenaissance']) ? $datenaissance = $_GET['datenaissance'] : $datenaissance = "");
}
// Si les variables arrivent en $_POST
if (isset($_POST['nom'])or isset($_POST['exact']) or isset($_POST['datenaissance'])
    or isset($_POST['prenom']) or isset($_POST['marital'])) {
    // Initialisation des variables du formulaire
    (isset($_POST['nom']) ? $nom = $_POST['nom'] : $nom = "");
    (isset($_POST['exact']) && $_POST['exact']==true ? $exact = true : (isset($exact) ? $exact = $exact : $exact = false));
    (isset($_POST['prenom']) ? $prenom = $_POST['prenom'] : $prenom = "");
    (isset($_POST['marital']) ? $marital = $_POST['marital'] : $marital = "");
    (isset($_POST['datenaissance']) ? $datenaissance = $_POST['datenaissance'] : $datenaissance = "");
}

(!isset($exact) ? $exact = true : $exact = $exact);

/// Decoche recherche exact si * detecte
if (substr($nom,strlen($nom)-1,1) == '*' && strlen($nom) >=2){
    $nom = str_replace("*","",$nom);
    $exact = false;
}
if (substr($prenom,strlen($prenom)-1,1) == '*' && strlen($nom) >=2) {
    $prenom = str_replace("*","", $prenom);
    $exact = false;
}
if (substr($marital,strlen($marital)-1,1) == '*' && strlen($nom) >=2) {
    $marital = str_replace("*","", $marital);
    $exact = false;
}

// Condition d'erreur
$error = ($nom == "" and $datenaissance == "" and $prenom == "" and $marital == "" ? true : false);

/**
 * Validation du formulaire
 */
//
if (isset($_POST[$page.'_form_action_valid'])) {
    //
    if (!$error) {
        //
        $params = "&nom=".urlencode($nom);
        $params .= ($exact == true ? "&exact=".$exact : "");
        $params .= "&prenom=".urlencode($prenom);
        $params .= "&marital=".urlencode($marital);
        $params .= "&datenaissance=".urlencode($datenaissance);
        //
        header ("location:../app/electeur.php?obj=".$obj.$params);
    }
}

/**
 * Parametrage du formulaire
 */
//
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."formulairedyn.class.php";
//
$validation = 0;
$maj = 0;
$champs = array("nom", "exact", "marital", "prenom", "datenaissance");
//
$form = new formulaire(NULL, $validation, $maj, $champs);
//
$form->setLib("nom", _("Nom patronymique"));
$form->setType("nom", "text");
$form->setTaille("nom", 40);
$form->setMax("nom", 60);
$form->setVal("nom", $nom);
$form->setOnchange("nom", "this.value=this.value.toUpperCase()");
//
$form->setLib("exact", _("Recherche exacte"));
$form->setType("exact", "checkbox");
$form->setTaille("exact", 3);
$form->setMax("exact", 3);
$form->setVal("exact", $exact);
//
$form->setLib("marital", _("Nom d'usage ou nom marital"));
$form->setType("marital", "text");
$form->setTaille("marital", 40);
$form->setMax("marital", 60);
$form->setVal("marital", $marital);
$form->setOnchange("marital", "this.value=this.value.toUpperCase()");
//
$form->setLib("prenom", _("Prenom"));
$form->setType("prenom", "text");
$form->setTaille("prenom", 40);
$form->setMax("prenom", 60);
$form->setVal("prenom", $prenom);
$form->setOnchange("prenom", "this.value=this.value.toUpperCase()");
//
$form->setLib("datenaissance", _("Date de Naissance"));
$form->setType("datenaissance", "date");
$form->setTaille("datenaissance", 10);
$form->setMax("datenaissance", 10);
$form->setVal("datenaissance", $datenaissance);
$form->setOnchange("datenaissance", "fdate(this)");
//
$form->setGroupe("nom", "D");
$form->setGroupe("exact", "F");
/**
 * Affichage
 */
// Gestion des droits
$f->setRight($obj);
$f->isAuthorized();

// Parametrage du titre de la page
$f->setTitle($ent);

// Affichage de la structure de la page
$f->setFlag(NULL);
$f->display();

// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n\n";

// Affichage de la liste des onglets
echo "<ul>\n";
echo "\t<li><a href=\"#tabs-1\">";
echo "<span class=\"om-icon ui-icon ui-icon-search\"><!-- --></span>";
echo $onglet;
echo "</a></li>\n";
echo "</ul>\n";

// Ouverture de la balise - Onglet 1
echo "\n<div id=\"tabs-1\">\n";

// Affichage du message d'erreur si besoin
if (isset($_POST[$page.'_form_action_valid'])) {
    //
    if ($error) {
        $message_class = "error";
        $message = _("Vous devez saisir au moins un critere de recherche.");
        $f->displayMessage($message_class, $message);
    }
}

// Instructions et description du contenu de l'onglet
$f->displayDescription($description);

// Ouverture de la balise - Formulaire
echo "\n<div id=\"".$page."\" class=\"formulaire\">\n";
echo "<form method=\"post\" id=\"".$page."_form\" ";
echo "name=\"".$page."_form\" ";
echo "action=\"".$action."\">\n";

// Affichage du formulaire
$form->entete();
$form->afficher($champs, $validation, false, false);
$form->enpied();

// Ouverture de la balise - Controles du formulaire
echo "\t<div class=\"formControls\">\n";
// Bouton
echo "\t\t<input name=\"".$page."_form.action.valid\" ";
echo "value=\"".$bouton."\" ";
echo "type=\"submit\" class=\"boutonFormulaire\" />\n";
// Lien retour
echo "<a class=\"retour\" title=\""._("Retour")."\" ";
echo "href=\"../scr/dashboard.php\">";
echo _("Retour");
echo "</a>";
// Fermeture de la balise - Controles du formulaire
echo "\t</div>\n";

// Fermeture de la balise - Formulaire
echo "</form>\n";
echo "</div>\n";

// Fermeture de la balise - Onglet 1
echo "</div>\n";

// Fermeture de la balise - Conteneur d'onglets
echo "\n</div>\n";

?>
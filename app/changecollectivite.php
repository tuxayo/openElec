<?php
/**
 * Formulaire de changement de collectivite
 *
 * @package openElec
 */

/**
 *
 */
require_once("../obj/utils.class.php");

//
$f = new utils("nohtml",
               "collectivitedefaut",
               _("Choix de la collectivite de travail"));

/**
 *
 */
require_once("../obj/workcollectivite.class.php");

//
$workcollectivite = new workCollectivite($f, "../app/changecollectivite.php");
$workcollectivite->validateForm();

//
$f->setFlag(NULL);
$f->display();

//
$workcollectivite->showForm();

?>
<?php
/**
 * Ce fichier surcharge la classe FPDF pour etre utilise ...
 * XXX - Cette classe ne devrait pas se trouver dans le dossier spg.
 *
 * @package openelec
 * @version SVN : $Id$
 */

// XXX - Ce define est-il necessaire ? Deja present dans la classe FPDF non ?
define('FPDF_FONTPATH', 'font/');

/**
 * Inclusion de la classe FPDF qui permet de generer des fichiers PDF.
 */
require("fpdf.php");

/**
 * Cette methode surcharge la classe standard fpdf
 */
class PDF extends FPDF {
    
    /**
     *
     */
    var $msg = 0;
    
    /**
     *
     */
    var $nbrligne = 0;
    
    /**
     *
     */
    var $multi = false;
    
    /**
     * Footer de la table
     */
    function TableFooter() { 
        //Positionnement à 1,5 cm du bas 
        $this->SetY(-13);
        //Police Arial italique 8 
        $this->SetFont('', 'I', '');
        //Numéro de page 
        $this->Cell(0,10, iconv(HTTPCHARSET,"CP1252",sprintf('Page %s / {nb}',$this->PageNo())), 0, 0, 'C');
    }
    
    /**
     *
     */
    function AffCells($paramcells, $param, $tailledefault, $page, $ligne) {
        $nbrcell = count($paramcells);
        $p = 0;
        if ($nbrcell > 0) {
            for ($p=0; $p < $nbrcell; $p++) {
                $page = $this->PageNo();
                //
                $position= 0;
                $width = 0;
                $height = 0;
                $border = 0;
                $align = 'C';
                $flagB = '';
                $flagtaille = $tailledefault;
                //
                $LibOuMotCle = $paramcells[$p][0];
                if (strtoupper($LibOuMotCle) == '<PAGE>') {
                    $LibOuMotCle = "Page  :  ".$page.' ';
                }
                //
                $this->SetFillColor($paramcells[$p][9], $paramcells[$p][10], $paramcells[$p][11]);
                $this->SetTextColor($paramcells[$p][6], $paramcells[$p][7], $paramcells[$p][8]);
                $width = $paramcells[$p][1];
                $height = $paramcells[$p][2];
                $position = $paramcells[$p][3];
                $border = $paramcells[$p][4];
                $align = $paramcells[$p][5];
                //-------  taille -----------------------
                if ($paramcells[$p][15] == 'B') {
                    $flagB = 'B';
                }
                if ($paramcells[$p][16] > 0){
                    $flagtaille = $paramcells[$p][16];
                }
                $this->SetFont('', $flagB, $flagtaille);
                //-------------------------------------//
                //  Differents type de cellule         //
                //-------------------------------------//
                //
                //-------- type Vcell -------------------
                if ($LibOuMotCle == '<VCELL>') {
                    $heightlettre = 0;
                    $deduire = 0;
                    $heightlettre = $paramcells[$p][13];
                    $lettre = $paramcells[$p][14];
                    $nblettre = 0;
                    $demiheightentete = 0;
                    $nblettre = count($lettre);
                    $demiheightentete = floor(($height-$nblettre*$heightlettre)/2) ;
                    //
                    $tx = $this->Getx();
                    $ty = $this->Gety();
                    $ty = $ty + $paramcells[$p][12][1];
                    $tx = $tx + $paramcells[$p][12][0];
                    $this->Setxy($tx, $ty);
                    //
                    $this->Cell($width, $demiheightentete, '', 'LTR', 2, $align, 1);
                    $deduire += $demiheightentete;
                    $j = 0;
                    for ($j = 0; $j < $nblettre; $j++) {
                        $nom = $paramcells[$p][14][$j];
                        $this->Cell($width, $heightlettre, iconv(HTTPCHARSET,"CP1252",$nom), 'LR', 2, $align, 1);
                        $deduire += $heightlettre;
                    }
                    $this->Cell($width, $height - $deduire, ' ', 'LBR', 2, $align, 1);
                    $deduire += $height - $deduire;
                    $this->Cell($width, 0, '', '0', $position, $align, 1);
                    //
                    $tx = $this->Getx();
                    $ty = $this->Gety();
                    $this->Setxy($tx, $ty);
                    //--------------- fin Vcell ---------------------------------
                } else {
                        //--------------- TYPE image png ---------------------------
                        if (strtoupper($LibOuMotCle) == '<IMG>') {
                        $x = $this->Getx();
                        $y = $this->Gety();
                        $y = $y - $paramcells[$p][12][1];
                        $x = $x - $paramcells[$p][12][0];
                        $this->Cell($width + 2, $height + 2,'' , $border, $position, $align, 1);
                        $this->Image($paramcells[$p][14], $x + 1, $y + 1);
                        //--------------- fin image png ---------------------------
                    } else {
                        if (strtoupper($LibOuMotCle) == '<LIGNE>') {
                            $tx = $this->Getx();
                            $ty = $this->Gety();
                            $ty = $ty - $paramcells[$p][12][1];
                            $tx = $tx - $paramcells[$p][12][0];
                            $this->Setxy($tx, $ty);
                            $this->Cell($width, $height, iconv(HTTPCHARSET,"CP1252",$ligne), $border, $position, $align, 1);
                            $this->nbrligne = $ligne;
                        } else {
                            if ($align == '<UDCELL>' || $align == '<RCELL>') {
                                if ($align == '<UDCELL>') $align = 'U';
                                if ($align == '<RCELL>') $align = '45';
                                $l = 0;
                                $l = strlen($LibOuMotCle);
                                $tx = $this->Getx();
                                $ty = $this->Gety();
                                $ty = $ty - $paramcells[$p][12][1];
                                $tx = $tx - $paramcells[$p][12][0];
                                $this->Setxy($tx, $ty);
                                $this->Cell($width, $height, ' ', $border, $position, $align, 1);
                                $posx = 0;
                                $posy = 0;
                                $posx = $paramcells[$p][13];
                                $posy = $paramcells[$p][14];
                                $demiheightD = 0;
                                $demiheightD = floor(($height - $l) / 2);
                                $demiwidthD = 0;
                                $demiwidthD = floor(($width / 2));
                                $tx = $tx + $demiwidthD + $posx;
                                $ty = $ty + $l+$demiheightD + $posy;
                                if ($align == 'U') $this->Dcell( $tx, $ty, $LibOuMotCle, $align);
                                if ($align == '45') $this->Rcell( $tx, $ty, $LibOuMotCle, 45, 0);
                            } else {
                                // --------- TYPE cellule simple -------------------------
                                $tx = $this->Getx();
                                $ty = $this->Gety();
                                $ty = $ty - $paramcells[$p][12][1];
                                $tx = $tx - $paramcells[$p][12][0];
                                $this->Setxy($tx, $ty);
                                if ($page == 1) {
                                    $this->Cell($width, $height, iconv(HTTPCHARSET,"CP1252",$LibOuMotCle), $border, $position, $align, 1);
                                } else {
                                    if ($page > 1 && $paramcells[$p][17] == 1) {
                                        $this->Cell($width, $height, iconv(HTTPCHARSET,"CP1252",$LibOuMotCle), $border, $position, $align, 1);
                                    } else {
                                        if ($paramcells[$p][17] == 0 && $paramcells[$p][18] == 1) $this->Cell($width, $height, ' ', $border, $position, $align, 1);
                                    }
                                }
                            }
                            // --------- FIN  TYPE cellule simple -------------------------
                        }
                    }
                }
                $this->SetFont('' ,'' , $tailledefault);
            }
            //nbr cellule > 0
       }
       // FIN function
    }

    /**
     *
     */
    function Dcell($x, $y, $txt, $direction) {
        $txt = str_replace(')','\\)',str_replace('(', '\\(', str_replace('\\', '\\\\', $txt)));
        if ($direction == 'R')
            $s = sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET', 1, 0, 0, 1, $x * $this->k, ($this->h - $y) * $this->k, $txt);
        elseif ($direction == 'L')
            $s = sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET', -1, 0, 0, -1, $x * $this->k, ($this->h - $y) * $this->k, $txt);
        elseif ($direction == 'U')
            $s = sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET', 0, 1, -1, 0, $x * $this->k, ($this->h - $y) * $this->k, $txt);
        elseif ($direction == 'D')
            $s = sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET', 0, -1, 1, 0, $x * $this->k, ($this->h - $y) * $this->k, $txt);
        else
            $s = sprintf('BT %.2F %.2F Td (%s) Tj ET', $x * $this->k, ($this->h - $y) * $this->k, $txt);
        if ($this->ColorFlag) $s = 'q '.$this->TextColor.' '.$s.' Q';
        $this->_out($s);
    }
    
    /** x,y,txt,angle text,deformation------------------
     *  ex Rcell(50,65,'Hello',45,-45);
     * -------------------------------------------------
     */
    function Rcell($x, $y, $txt, $txt_angle, $font_angle) {
        $txt = str_replace(')', '\\)', str_replace('(', '\\(', str_replace('\\', '\\\\', $txt)));
    
        $font_angle += 90 + $txt_angle;
        $txt_angle *= M_PI / 180;
        $font_angle *= M_PI / 180;
    
        $txt_dx = cos($txt_angle);
        $txt_dy = sin($txt_angle);
        $font_dx = cos($font_angle);
        $font_dy = sin($font_angle);
    
        $s = sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET', $txt_dx, $txt_dy, $font_dx, $font_dy,
            $x * $this->k, ($this->h - $y) * $this->k, $txt);
        if ($this->ColorFlag) $s = 'q '.$this->TextColor.' '.$s.' Q';
        $this->_out($s);
    }

    /**
     *
     */
    function Table($titre, $entete, $col, $enpied, $sql, $db, $param, $pagefin, $pagedebut = array()) {
        $res =& $db->query($sql);
        //
        if (database::isError($res)) {
           $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
        } else {
            if ($pagedebut != array()) {
                $this->SetFont('', '', $param[2]);
                $this->AffCells($pagedebut, $param, $param[2], $this->PageNo(), 0);
                if ($param[9] == 0) {
                   $this->Addpage();
                } else {
                    $this->Ln();
                }
            }
            $nbchamp = 0;
            $nbentete = 0;
            $nbpied = 0;
            $info = $res->tableInfo();
            $nbChamp = count($info);
            $nbpied = count($enpied);
            //
            $ligne = 0;
            //
            $this->nbrligne = 0;
            $this->msg = 0;
            //
            $k = 0;
            $i = 0;
            $j = 0;
            $cpt = 1;
            $fill = 0;
            //
            $flagtitre = 1;
            $flagentete = 1;
            //
            $x = 0;
            $y = 0;
            $tx = 0;
            $ty = 0;
            $maxligne = 0;
            $lettre = array();
            $nblettre = 0;
            $heightlettre = 0;
            $deduire = 0;
            $afftitre = 0;
            $affgentete = 0;
            $nbtitre = 0;
            $compteur = 0;
            //
            $nbrow = 0;
            $nbrow = $res->numrows();
            if ($param[10] == 1) {
                if ($nbrow > 0) $this->AddPage();
            }
            //
            while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                $compteur++;
                $ligne++;
                if ($cpt == 1) {
                    $debutx = $param[7];
                    $debuty = $param[8];
                    $this->Setxy($debutx, $debuty);
                    $page = $this->PageNo();
                    //-----------------------------------------//
                    //  titre                                  //
                    //-----------------------------------------//
                    if ($flagtitre ==1 ) {
                       $this->SetFont('', '', $param[0]);
                       $this->AffCells($titre, $param, $param[0], $page, 0);
                       $this->Ln();
                       $afftitre = 1;
                       if ($param[3] == 0) $flagtitre = 0;
                    }
                    //-----------------------------------------//
                    //  entête tableau                         //
                    //-----------------------------------------//
                    if ($flagentete == 1) {
                        $this->SetFont('', '', $param[1]);
                        $this->AffCells($entete, $param, $param[1], $page, 0);
                        $this->Ln();
                        $affentete = 1;
                        if ($param[4] == 0) $flagentete = 0;
                    }
                }
                //
                if ($affentete == 0 && $afftitre == 0) {
                    // $this->SetY($titre[1]);
                }
                //
                $this->SetFont('', '', $param[2]);
                $archive = array();
                for ($k=0; $k < $nbChamp; $k++) {
                    //-------------------------------------------------------------//
                    // cellule supplémentaire VIDE AVANT AFFICHAGE DATA            //
                    //-------------------------------------------------------------//
                    if (trim($col[$info[$k]['name']][14]) == 'PRE_CELLSUP_VIDE') {
                       $this->SetFillColor($col[$info[$k]['name']][15][8], $col[$info[$k]['name']][15][9], $col[$info[$k]['name']][15][10]);
                       $this->SetTextColor($col[$info[$k]['name']][15][5], $col[$info[$k]['name']][15][6], $col[$info[$k]['name']][15][7]);
                       $position = 0;
                       $width = $col[$info[$k]['name']][15][0];
                       $height = $col[$info[$k]['name']][15][1];
                       $position = $col[$info[$k]['name']][15][2];
                       $border = $col[$info[$k]['name']][15][3];
                       $align = $col[$info[$k]['name']][15][4];
                       $x = $this->Getx();
                       $y = $this->Gety();
                       $x = $x - $col[$info[$k]['name']][15][11][0];
                       $y = $y - $col[$info[$k]['name']][15][11][1];
                       $this->Setxy($x, $y);
                       $this->Cell($width, $height, '', $border, $position, $align, $fill);
                       // fin cellule vide
                    }
                    //
                    $data = $row[$info[$k]['name']];
                    //
                    //-------------------------------------------------------------//
                    // archivage                                                   //
                    //-------------------------------------------------------------//
                    if (trim($col[$info[$k]['name']][16]) == 'A') {
                        $archive[$k] = $data;
                    }
                    //---------------------------------------------------------------
                    //
                    //-------------------------------------------------------------//
                    // condition affichage data                                    //
                    //-------------------------------------------------------------//
                    $flag_condition = 1;
                    if (trim($col[$info[$k]['name']][17]) == 'CONDITION') {
                        // AFFICHAGE SI  CHAMPS Not Null
                        if ($col[$info[$k]['name']][18][0] == 'NN') {
                            if (trim($data) == '') {
                                $flag_condition = 0;
                                $data = '';
                            }
                        }
                    }
                    //--------------------------------------------------------------
                    if ($flag_condition == 1) {
                        $data = $col[$info[$k]['name']][12].$data.$col[$info[$k]['name']][13];
                    }
                    //--------------------------------------------------------------
                    $this->SetFillColor($col[$info[$k]['name']][8],$col[$info[$k]['name']][8],$col[$info[$k]['name']][10]);
                    $this->SetTextColor($col[$info[$k]['name']][5],$col[$info[$k]['name']][6],$col[$info[$k]['name']][7]);
                    $position = 0;
                    $width = $col[$info[$k]['name']][0];
                    $height = $col[$info[$k]['name']][1];
                    $position = $col[$info[$k]['name']][2];
                    $border = $col[$info[$k]['name']][3];
                    $align = $col[$info[$k]['name']][4];
                    //
                    $x = $this->Getx();
                    $y = $this->Gety();
                    $x = $x - $col[$info[$k]['name']][11][0];
                    $y = $y - $col[$info[$k]['name']][11][1];
                    $this->Setxy($x, $y);
                    //------- taille et gras pour champs --------------------------
                    $flagB = '';
                    if ($col[$info[$k]['name']][19] == 'B') {
                        $flagB = 'B';
                    }
                    $flagtaille = $param[2];
                    if ($col[$info[$k]['name']][20] > 0){
                        $flagtaille = $col[$info[$k]['name']][20];
                    }
                    //-------------------------------------------------------------
                    $this->SetFont('', $flagB, $flagtaille);
                    $this->Cell($width, $height, iconv(HTTPCHARSET,"CP1252",$data), $border, $position, $align, $fill);
                    $this->SetFont('', '', $param[2]);
                    // --------------------------------------------------------------
                    if (trim($col[$info[$k]['name']][14]) != 'CELLSUP_NON' && trim($col[$info[$k]['name']][14]) != 'PRE_CELLSUP_VIDE') {
                        //-------------------------------------------------------------//
                        // cellule supplémentaire VIDE APRES DATA                      //
                        //-------------------------------------------------------------//
                        if (trim($col[$info[$k]['name']][14]) == 'CELLSUP_VIDE' || trim($col[$info[$k]['name']][14]) == 'CELLSUP_TXT') {
                            $this->SetFillColor($col[$info[$k]['name']][15][8], $col[$info[$k]['name']][15][9], $col[$info[$k]['name']][15][10]);
                            $this->SetTextColor($col[$info[$k]['name']][15][5], $col[$info[$k]['name']][15][6], $col[$info[$k]['name']][15][7]);
                            $position = 0;
                                $width = $col[$info[$k]['name']][15][0];
                                $height = $col[$info[$k]['name']][15][1];
                                $position = $col[$info[$k]['name']][15][2];
                                $border = $col[$info[$k]['name']][15][3];
                                $align = $col[$info[$k]['name']][15][4];
                                $x = $this->Getx();
                                $y = $this->Gety();
                                $x = $x - $col[$info[$k]['name']][15][11][0];
                                $y = $y - $col[$info[$k]['name']][15][11][1];
                                $this->Setxy($x, $y);
                                if (trim($col[$info[$k]['name']][14]) == 'CELLSUP_VIDE') {
                                    $this->Cell($width, $height, '', $border, $position, $align, $fill);
                                } else {
                                    // affichage texte supplémentaire  CELLSUP_TXT
                                    $this->Cell($width, $height, iconv(HTTPCHARSET,"CP1252",$col[$info[$k]['name']][15][12]), $border, $position, $align, $fill);
                                }
                            // fin cellule vide
                        } else {
                            //-------------------------------------------------------------//
                            // cellule supplémentaire depuis archivage                     //
                            // (nom colonne[14])-> numérique = no colonne archivée         //
                            //-------------------------------------------------------------//
                            $this->SetFillColor($col[$info[$k]['name']][15][8], $col[$info[$k]['name']][15][9], $col[$info[$k]['name']][15][10]);
                            $this->SetTextColor($col[$info[$k]['name']][15][5], $col[$info[$k]['name']][15][6], $col[$info[$k]['name']][15][7]);
                            $position = 0;
                            $width = $col[$info[$k]['name']][15][0];
                            $height = $col[$info[$k]['name']][15][1];
                            $position = $col[$info[$k]['name']][15][2];
                            $border = $col[$info[$k]['name']][15][3];
                            $align = $col[$info[$k]['name']][15][4];
                            //
                            $x = $this->Getx();
                            $y = $this->Gety();
                            $x = $x - $col[$info[$k]['name']][15][11][0];
                            $y = $y - $col[$info[$k]['name']][15][11][1];
                            $this->Setxy($x, $y);
                            if ($align == 'LDCELL') {
                                // appel fonction Dcell
                                $align = 'L';
                                $this->Cell($width, $height, ' ', $border, $position, $align, $fill);
                                $x = $this->Getx();
                                $y = $this->Gety();
                                $l = 0;
                                $l = strlen($archive[$col[$info[$k]['name']][14]]);
                                $posx = 0;
                                $posy = 0;
                                // à ajouter ou retrancher
                                $posx = $col[$info[$k]['name']][15][12];
                                $posy = $col[$info[$k]['name']][15][13];
                                //
                                $demiheightD = 0;
                                $demiheightD = floor($height / 2);

                                $x = $x + $posx;
                                $y = $y + $demiheightD + $posy;
                                 // ***
                                $flagB = '';
                                if ($col[$info[$k]['name']][15][14] == 'B') {
                                    $flagB = 'B';
                                }
                                $flagtaille = $param[2];
                                if ($col[$info[$k]['name']][15][15] > 0) {
                                    $flagtaille = $col[$info[$k]['name']][15][15];
                                }
                                $this->SetFont('', $flagB, $flagtaille);
                                //****
                                $this->Dcell($x, $y, iconv(HTTPCHARSET,"CP1252",$archive[$col[$info[$k]['name']][14]]), $align);
                                $this->SetFont('', '', $param[2]);
                            }
                        }
                    }
                    //fin cellule différent cellsup_non
                }
                $fill=!$fill;
                $this->Ln();
                $cpt++;
                $page = $this->PageNo();
                    if ($page == 1) {
                        $maxligne = $param[5];
                    } else {
                        $maxligne = $param[6];
                    }
                    if ($cpt > $maxligne) {
                        if ($this->multi) {
                            $this->Text(250,195,"Page ".$this->PageNo());
                        }
                        if ($compteur < $nbrow) {
                            $this->AddPage();
                        }
                        $cpt = 1;
                    }
                }
                if ($nbrow > 0) {
                    //---------------------------------------//
                    //    enpied col                         //
                    //---------------------------------------//
                    if ($nbpied > 0) {
                        if ($this->multi) {
                            $this->Text(250,195,"Page ".$this->PageNo());
                        }
                        $this->SetFont('', '', $param[1]);
                        $this->AffCells($enpied, $param, $param[1], $page, 0);
                        $this->Ln();
                    }
                    //---------------------------------------//
                    //    DERNIÈRE PAGE  sélection           //
                    //---------------------------------------//
                    if ($param[9] == 0){
                        $this->Addpage();
                    } else {
                        $this->Ln(0);
                    }
                    if ($this->multi) {
                        $this->Text(250,195,"Page ".$this->PageNo());
                    }
                    $this->SetFont('', '', $param[2]);
                    $this->AffCells($pagefin, $param, $param[2], $page, $ligne);
                } else {
                // aucun enregistrement selectionné
                //
                if ($nbrow == 0) {
                    $this->msg = 1;
                }
            }
            $res->free();
        }
   }//fin db
 
    /**
     * Traitement d erreur
     * transfert à l ecran des erreurs de bases de donnees
     * $debuginfo : info table de données
     * $messageDB : message d erreur DB pear
     * $table = table concernée
     */
    function erreur_db($debuginfo, $messageDB, $table) {
        include(PATH_OPENMAIRIE."error_db.inc");
        $this->SetFont('arial', '', '9');
        $this->ln();
        $this->Cell(0, 10, iconv(HTTPCHARSET,"CP1252",_('Attention, Erreur de base de donnees')), 0, 1, 'L');
        $this->Cell(0, 10, iconv(HTTPCHARSET,"CP1252",$requete), 0, 1, 'L');
        $this->Cell(0, 10, iconv(HTTPCHARSET,"CP1252",$erreur_origine), 0, 1, 'L');
        $this->Cell(0, 10, iconv(HTTPCHARSET,"CP1252",$messageDB), 0, 1, 'L');
        $this->Cell(0, 10, iconv(HTTPCHARSET,"CP1252",$msgfr), 0, 1, 'L');
        $this->Cell(0, 10, iconv(HTTPCHARSET,"CP1252",_('Contactez votre administrateur')), 0, 1, 'L');
    }

}

?>
<?php
// $Id: search.php,v 1.2 2010-07-23 13:54:57 salamandre Exp $
/**
 * Fichiers requis
 */



// Parametrage [parameters]=================================================== 
 $obj="search";

 set_time_limit (1800);

 if ( isset($_GET['obj']) )
 require_once("../sql/pgsql/".$_GET['obj'].".inc");
 $objet = "";
 
 if ( isset($_POST['searchType']) ) {
 if ( isset( $configTypeRecherche[$_POST['searchType']]['obj'] ) )
    $objet = $configTypeRecherche[$_POST['searchType']]['obj'];
 }
 
 if ( $objet != "" ) {
    
    $recherche = str_replace("*","",$_POST["recherche"]);
    
    header("location:../scr/tab.php?obj=".$objet."&recherche=".$recherche);
 } else {
    
    require_once ("../dyn/var.inc");
    require_once ("../obj/utils.class.php");
    
// Parametrage [parameters]===================================================
$obj="search";
set_time_limit (1800);

/**
 * Classe utils ()
 */
$f = new utils ("", $obj, _("Recherche"));
 
require_once ("../obj/search.class.php");
 $objRech = new search($f);
 $objRech->showForm();
 $objRech->showResults();

    
 }

?>
<?php
/**
 * Ce fichier permet de faire une redirection vers le fichier index.php a la
 * racine de l'application.
 *
 * @package openElec
 * @version SVN : $Id$
 */

header("Location: ../index.php");
exit();

?>
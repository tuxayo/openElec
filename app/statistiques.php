<?php
/**
 * Ce fichier permet de lister les statistiques disponibles
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils(NULL, /*DROIT*/"statistiques", _("Statistiques"));

/**
 *
 */
$tab = array (
    0 => array (
        'lien' => "mouvement_bureau",
        'titre' => _("Mouvements details par bureau"),
        'description' => _("Nombre d'inscription office (code insee 8), d'inscription nouvelle (code insee 1), et de deces (code insee D) par bureau pour la liste et la date de tableau en cours")),
    1 => array (
        'lien' => "mouvement_voie",
        'titre' => _("Mouvements details par voie"),
        'description' => _("Nombre d'inscription office (code insee 8), d'inscription nouvelle (code insee 1), et de deces (code insee D) par voie pour la liste et la date de tableau en cours")),
    2 => array (
        'lien' => "electeur_age_bureau",
        'titre' => _("Electeurs par tranche d'age / details par bureau"),
        'description' => _("Nombre d'electeurs, par tranche d'age par bureau pour la liste en cours")),
    3 => array (
        'lien' => "electeur_sexe_bureau",
        'titre' => _("Electeurs par sexe / details par bureau"),
        'description' => _("Nombre d'electeurs, d'hommes, de femmes par bureau pour la liste en cours")),
    4 => array (
        'lien' => "electeur_sexe_nationalite",
        'titre' => _("Electeurs par sexe / details par nationalite"),
        'description' => _("Nombre d'electeurs, d'hommes, de femmes par nationalite pour la liste en cours")), 
    5 => array (
        'lien' => "electeur_sexe_departementnaissance",
        'titre' => _("Electeurs par sexe / details par departement de naissance"),
        'description' => _("Nombre d'electeurs, d'hommes, de femmes par departement (ou pays) de naissance pour la liste en cours")),  
    6 => array (
        'lien' => "electeur_sexe_voie",
        'titre' => _("Electeurs par sexe / details par voie"),
        'description' => _("Nombre d'electeurs, d'hommes, de femmes par voie pour la liste en cours")),
    7 => array (
        'lien' => "centrevote_sexe_bureau",
        'titre' => _("Electeurs en centre de vote par sexe / details par bureau"),
        'description' => _("Nombre d'electeurs en centre de vote, d'hommes, de femmes par bureau")),
    8 => array (
        'lien' => "io_sexe",
        'titre' => _("Mouvements inscrits office (8) par sexe / details par bureau"),
        'description' => _("Nombre d'electeurs en inscrits d'office, d'hommes, de femmes par bureau"))
);

/**
 *
 */
echo "<table class=\"editions\">\n";
foreach ($tab as $key => $s) {
    echo "\t<tr class=\"stats ".($key%2==0?"odd":"even")."\">";
    echo "<td class=\"titre\"> ";
    echo "<a  class=\"om-prev-icon statistiques-16\" target=\"_blank\" href=\"../pdf/pdffromarray.php?obj=".$s ['lien']."\">";
    echo $s ['titre'];
    echo "</a>";
    echo "</td>";
    echo "<td class=\"description\">".$s ['description']."</td>";
    echo "</tr>\n";
}
echo "</table>";

?>

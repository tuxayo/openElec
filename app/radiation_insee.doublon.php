<?php
/**
 * ce script permet d'afficher le resultat du formulaire de recherche en cas de
 * doublon sur un electeur de meme nom, prenom et date de naissance.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");

/**
 * Initialisation des variables
 */
$nom = "";
$prenom = "";
$datenaissance = "";
(isset($_GET['idrad']) ? $idrad = $_GET['idrad'] : $idrad = "");
(isset($_GET['recherche']) ? $recherche = $_GET['recherche'] : $recherche = "");
(isset($_GET['premier']) ? $premier = $_GET['premier'] : $premier = "0");
(isset($_GET['action']) ? $action = $_GET['action'] : $action = false);

// Passage de parametres par url (href)
$url = "&amp;premier=".$premier;
$url .= "&amp;recherche=".$recherche;
// Passage de parametres par url (header)
$params = "&premier=".$premier;
$params .= "&recherche=".$recherche;

/**
 * Validation du formulaire
 */
//
if ($idrad == "") {
    // Si pas de $idrad retour au tableau "radiation_insee"
    header("location:../scr/tab.php?obj=radiation_insee&".$params);
}

/**
 * Calcul des resultats de la recherche de doublon
 */
//
require "../sql/".$f->phptype."/radiation_insee_doublon.inc";

/**
 * Recuperation des informations de l'electeur dans radiation_insee depuis
 * idx
 */
$res_radiation_insee = $f->db->query($query_radiation_electeur);
$f->isDatabaseError($res_radiation_insee);
$row=& $res_radiation_insee->fetchRow(DB_FETCHMODE_ASSOC);

$nom = $row['nom'];
$prenom = trim($row['prenom1']." ".$row['prenom2']." ".$row['prenom3']);
$datenaissance = $row['date_naissance'];

/**
 * Rechargement du fichier "radiation_insee_doublon.inc" avec les variables
 * $nom, $prenom et $datenaissance
 */

require "../sql/".$f->phptype."/radiation_insee_doublon.inc";
//
$res_doublon_electeur = $f->db->query($query_doublon_electeur);
$f->isDatabaseError($res_doublon_electeur);

//
$res_doublon_mouvement = $f->db->query($query_doublon_mouvement);
$f->isDatabaseError($res_doublon_mouvement);

// Si les recherches ne donnent aucun resultat
if ($res_doublon_electeur->numrows () == 1
    and $res_doublon_mouvement->numrows() == 0) {
    //
    if ($action != "cancel") {
        $row_elec = & $res_doublon_electeur->fetchRow();
        // Redirection vers le formulaire d'inscription
        header ("location:../scr/form.php?obj=radiation_insee_valider&maj=0&idxelecteur=".$row_elec[0]."&idrad=".$idrad.$params);
    } else {
        // Redirection vers le formulaire de recherche
        header ("location:../scr/tab.php?obj=radiation_insee".$params);
    }
}


/**
 * Affichage
 */
// Gestion des droits
$f->setRight(/*DROIT*/"radiation_insee");
$f->isAuthorized();

// Parametrage du titre de la page
$f->setTitle(_("Saisie")." -> "._("Ajout de mouvement radiation INSEE"));

// Affichage de la structure de la page
$f->setFlag(NULL);
$f->display();

// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n\n";

// Affichage de la liste des onglets
echo "<ul>\n";
echo "\t<li><a href=\"#tabs-1\">";
echo _("Resultats de la recherche de doublon");
echo "</a></li>\n";
echo "</ul>\n";

// Ouverture de la balise - Onglet 1
echo "\n<div id=\"tabs-1\">\n";

// Instructions et description du contenu de l'onglet
$description = _("Voici la liste des electeurs presents dans la liste ".
                 "electorale ayant deja une radiation en cours ou plusieurs ".
                 "electeurs avec un meme nom, prenom et date de naissance ".
                 "identique a votre recherche.");
$f->displayDescription($description);

/**
 *
 */
//
require_once PATH_OPENMAIRIE."om_table.class.php";
// Premier enregistrement a afficher
(isset($_GET['premier']) ? $premier = $_GET['premier'] : $premier = 0);
// Colonne choisie pour le tri
(isset($_GET['tricol']) ? $tricol = $_GET['tricol'] : $tricol = "");
$params = array(
    "idrad" => $idrad,
    "premier" => 0,
    "recherche" => "",
    "selectioncol" => "",
    "tricol" => $tricol,
    "nom" => $nom,
    "datenaissance" => $datenaissance,
);

// Entete element
$element_recherche = " -> ".$nom ." ".$prenom." "._("ne(e) le")." -> ".$f->formatDate($datenaissance)."";

//
require "../sql/".$f->phptype."/radiation_insee_doublon_radiation.inc";
$tb = new table("../app/radiation_insee.doublon.php", $table, $serie, $champAffiche, $champRecherche, $tri, $selection, $edition, $options);
$f->displaySubTitle($ent.$element_recherche);
$tb->display($params, $href, $f->db, "tab", false);

//
require "../sql/".$f->phptype."/radiation_insee_doublon_electeur.inc";
$tb = new table("../app/radiation_insee.doublon.php", $table, $serie, $champAffiche, $champRecherche, $tri, $selection, $edition, $options);
$f->displaySubTitle($ent.$element_recherche);
$tb->display($params, $href, $f->db, "tab", false);

// Ouverture de la balise - Formulaire
echo "\n<div id=\"radiation_doublon\" class=\"formulaire\">\n";

// Ouverture de la balise - Controles du formulaire
echo "\t<div class=\"formControls\">\n";

// Lien retour
echo "<a class=\"retour\" title=\""._("Retour")."\" ";
echo "href=\"../scr/tab.php?obj=radiation_insee".$url."\">";
echo _("Retour");
echo "</a>";

// Fermeture de la balise - Controles du formulaire
echo "\t</div>\n";

// Fermeture de la balise - Formulaire
echo "</div>\n";

// Fermeture de la balise - Onglet 1
echo "</div>\n";

// Fermeture de la balise - Conteneur d'onglets
echo "\n</div>\n";

?>

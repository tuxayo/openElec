<?php
/**
 * ce script permet d'afficher le resultat du formulaire de recherche en cas de
 * doublon probable sur une nouvelle inscription.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");

/**
 * Initialisation des variables
 */
// Initialisation des variables du formulaire
$nom = "";
$datenaissance = "";
$exact = true;
// Si les variables arrivent en $_GET
if (isset($_GET['nom']) or isset($_GET['datenaissance'])
    or isset($_GET['exact'])) {
    // Initialisation des variables du formulaire
    (isset($_GET['nom']) ? $nom = $_GET['nom'] : $nom = "");
    (isset($_GET['exact']) && $_GET['exact']==true ? $exact = true : $exact = false);
    (isset($_GET['datenaissance']) ? $datenaissance = $_GET['datenaissance'] : $datenaissance = "");
}


//
(isset($_GET['action']) ? $action = $_GET['action'] : $action = false);
//
$url = "nom=".urlencode($nom);
$url .= ($exact == true ? "&amp;exact=".$exact : "");
$url .= "&amp;datenaissance=".urlencode($datenaissance);
//
$params = "nom=".urlencode($nom);
$params .= ($exact == true ? "&exact=".$exact : "");
$params .= "&datenaissance=".urlencode($datenaissance);

/**
 * Validation du formulaire
 */
//
if ($nom == "" and $datenaissance == ""
    or $f->formatDate($datenaissance) == false and $datenaissance != "") {
    //
    header("location:../app/inscription.search.php?".$params);
}

/**
 * Validation du formulaire
 */
//
if (isset($_POST['inscription_doublon_form_action_valid'])) {
    //
    header ("location:../scr/form.php?obj=inscription&".$params);
}

/**
 * Calcul des resultats de la recherche de doublon
 */
//
require "../sql/".$f->phptype."/inscription_doublon.inc";

//
$res_doublon_electeur = $f->db->query($query_doublon_electeur);
$f->isDatabaseError($res_doublon_electeur);

//
$res_doublon_mouvement = $f->db->query($query_doublon_mouvement);
$f->isDatabaseError($res_doublon_mouvement);

// Si les recherches ne donnent aucun resultat
if ($res_doublon_electeur->numrows () == 0
    and $res_doublon_mouvement->numrows() == 0) {
    //
    if ($action != "cancel") {
        // Redirection vers le formulaire d'inscription
        header ("location:../scr/form.php?obj=inscription&".$params);
    } else {
        // Redirection vers le formulaire de recherche
        header ("location:../app/inscription.search.php?".$params);
    }
}

/**
 * Affichage
 */
// Gestion des droits
$f->setRight(/*DROIT*/"inscription");
$f->isAuthorized();

// Parametrage du titre de la page
$f->setTitle(_("Saisie")." -> "._("Nouvelle inscription"));

// Affichage de la structure de la page
$f->setFlag(NULL);
$f->display();

// Ouverture de la balise - Conteneur d'onglets
echo "<div id=\"formulaire\">\n\n";

// Affichage de la liste des onglets
echo "<ul>\n";
echo "\t<li><a href=\"#tabs-1\">";
echo _("Resultats de la recherche de doublon");
echo "</a></li>\n";
echo "</ul>\n";

// Ouverture de la balise - Onglet 1
echo "\n<div id=\"tabs-1\">\n";

// Instructions et description du contenu de l'onglet
$description = _("Voici la liste des electeurs deja presents dans la liste ".
                 "electorale ou ayant deja une inscription en cours avec un ".
                 "nom et/ou date de naissance identique a votre saisie. ".
                 "Maintenant, c'est a vous de realiser l'arbitrage : si ce ".
                 "n'est pas un doublon il faut cliquer sur le bouton ".
                 "\"Inscrire\" en bas de la page pour acceder au formulaire ".
                 "d'inscription ou alors si c'est effectivement un doublon ".
                 "il faut cliquer sur le bouton \"Retour\" pour revenir au ".
                 "formulaire de recherche initial.");
$f->displayDescription($description);

/**
 *
 */
//
require_once PATH_OPENMAIRIE."om_table.class.php";
// Premier enregistrement a afficher
(isset($_GET['premier']) ? $premier = $_GET['premier'] : $premier = 0);
// Colonne choisie pour le tri
(isset($_GET['tricol']) ? $tricol = $_GET['tricol'] : $tricol = "");
$params = array(
    "premier" => $premier,
    "recherche" => "",
    "selectioncol" => "",
    "tricol" => $tricol,
    "nom" => $nom,
    "datenaissance" => $datenaissance,
);
if ($exact == true) {
    $params["exact"] = true;
}
//
$element_recherche = "";
if ($nom != "" && $datenaissance != "") {
    $element_recherche .= " -> ".$nom ." "._("ne(e) le")." -> ".$datenaissance."";
} elseif ($datenaissance == "") {
    $element_recherche .= " -> ".$nom ."";
} elseif ($nom == "") {
    $element_recherche .= _("ne(e) le")." -> ".$datenaissance."";
}

//
require "../sql/".$f->phptype."/inscription_doublon_inscription.inc";
$tb = new table("../app/inscription.doublon.php", $table, $serie, $champAffiche, $champRecherche, $tri, $selection, $edition, $options);
$f->displaySubTitle($ent.$element_recherche);
$tb->display($params, $href, $f->db, "tab", false);

//
require "../sql/".$f->phptype."/inscription_doublon_electeur.inc";
$tb = new table("../app/inscription.doublon.php", $table, $serie, $champAffiche, $champRecherche, $tri, $selection, $edition, $options);
$f->displaySubTitle($ent.$element_recherche);
$tb->display($params, $href, $f->db, "tab", false);

// Ouverture de la balise - Formulaire
echo "\n<div id=\"inscription_doublon\" class=\"formulaire\">\n";
echo "<form method=\"post\" id=\"inscription_doublon_form\" ";
echo "name=\"inscription_doublon_form\" ";
echo "action=\"../app/inscription.doublon.php?".$url."\">\n";

// Ouverture de la balise - Controles du formulaire
echo "\t<div class=\"formControls\">\n";
// Bouton
echo "\t\t<input name=\"inscription_doublon_form.action.valid\" ";
echo "value=\""._("Inscrire")."\" ";
echo "type=\"submit\" class=\"boutonFormulaire\" />\n";
// Lien retour
echo "<a class=\"retour\" title=\""._("Retour")."\" ";
echo "href=\"../app/inscription.search.php?".$url."\">";
echo _("Retour");
echo "</a>";
// Fermeture de la balise - Controles du formulaire
echo "\t</div>\n";

// Fermeture de la balise - Formulaire
echo "</form>\n";
echo "</div>\n";

// Fermeture de la balise - Onglet 1
echo "</div>\n";

// Fermeture de la balise - Conteneur d'onglets
echo "\n</div>\n";

?>

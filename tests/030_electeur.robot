*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Test suite de recherche d'électeurs

*** Test Cases ***
Recherche électeurs

    Depuis la page d'accueil  admin  admin
    # Consultation → Liste électorale
    Go To Submenu In Menu  consultation  electeurs

    # Recherche générale avec une date complète
    Use Simple Search  Tous  10/03/1980
    Page Should Contain  HANANE INNOVA
    # Recherche générale avec une date tronquée
    Use Simple Search  Tous  05/1968
    Page Should Contain  GEORGES
    Page Should Contain  JEAN
    # Recherche générale avec l'année
    Use Simple Search  Tous  1994
    Page Should Contain  ABAY
    Page Should Contain  ABEUDJE
    Page Should Contain  DURAND
    Page Should Contain  FABRE
    # Recherche sur date de naissance avec une date tronquée
    Use Simple Search  Date de naissance  05/1968
    Page Should Contain  GEORGES
    Page Should Contain  JEAN


Recherche électeurs archivés

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  consultation  archive

    # Recherche générale avec une date tronquée
    Use Simple Search  Tous  03/1981
    Page Should Contain  TRAWINSKI
    # Recherche sur date de naissance avec une date tronquée
    Use Simple Search  Date de naissance  05/1984
    Page Should Contain  TESTOU

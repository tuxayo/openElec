*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Test suite des procurations

*** Test Cases ***
Refus procuration
    [Documentation]  Ce test case ajoute une procuration puis la refuse.
    ...  Prérequis: Dans la base on compte sur le fait qu'il y a
    ...  déjà une procuration avec les dates 01/04/2014 à 16/04/2014 pour les
    ...  mêmes personnes que celles utilisées ici.

    Depuis la page d'accueil  admin  admin

    &{args_procuration} =  Create Dictionary
    ...  nom_mandant=BIG
    ...  nom_mandataire=DADOU
    ...  debut_validite=02/04/2014
    ...  fin_validite=15/04/2014
    ...  date_accord=15/08/2013
    ...  heure_accord=15:00:00
    Ajouter une procuration  ${args_procuration}

    Page Should Contain  L'électeur numéro [100034] est déjà mandant pour la même période
    Page Should Contain  L'électeur numéro [100042] est déjà mandataire pour 1 procuration(s) EN FRANCE pour la même période

    # Refus de procuration sans motif
    Select From List By Label  refus  Oui
    Click Button  css=[type=submit]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Page Should Contain  Le motif de refus est obligatoire si la procuration est refusée

    Input Text  motif_refus  Ceci est un motif de refus en bonne et due forme
    Click Button  css=[type=submit]
    Page Should Contain  Vos modifications ont bien été enregistrées.

    # Retour au tableau pour vérification du lien vers le courrier de refus
    Click Link  Retour
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Use Simple Search  Tous  BOB
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  css=[href*=refus_procuration]

    ${contenu_pdf} =  Create List  Objet : Vote par procuration
    ...  M. BIG BOB
    ...  M. DADOU DAVID
    ...  Ceci est un motif de refus en bonne et due forme
    Vérifier Que Le PDF Contient Des Strings  refus_procuration  ${contenu_pdf}

    # Lancement du traitement élection afin de mettre les pdf à jour
    Click Link  Traitement
    Sleep  1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  Élection

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Input Text  dateelection1  08/04/2014
    Click Button  traitement.election_mention.form.valid
    Confirm Action
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Page Should Contain  Le traitement est terminé

    # Vérification des éditions par bureau
    Click Link  Édition
    Sleep  1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  Par Bureau

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  css=[href*=listeemargement][href$="3"]
    ${contenu_pdf} =  Create List  LISTE D'ÉMARGEMENT
    # dates de la 1re procuration qui était déjà en base
    # c'est celle qui faisait conflit avec celle qu'on à voulu créer
    ...  01/04/2014  16/04/2014
    ${ne_doit_pas_contenir} =  Create List  02/04/2014  # 2e procuration
    Vérifier Que Le PDF Contient Des Strings Mais Pas D'Autres
    ...  listeemargement  ${contenu_pdf}  ${ne_doit_pas_contenir}

    Click Link  css=[href*=listeprocuration][href$="3"]
    ${contenu_pdf} =  Create List  LISTE DES PROCURATIONS
     ...  BIG  BOB  DADOU  DAVID
    ...  01/04/2014  16/04/2014
    ${ne_doit_pas_contenir} =  Create List  02/04/2014  15/04/2014
    Vérifier Que Le PDF Contient Des Strings Mais Pas D'Autres
    ...  listeprocuration  ${contenu_pdf}  ${ne_doit_pas_contenir}

    Click Link  css=[href*=listeregistreprocuration][href$="3"]
    ${contenu_pdf} =  Create List  ÉDITION DU REGISTRE DES PROCURATIONS
    # on veut qu'il y ait les deux procurations dont celle refusée
    ...  Ceci est un motif
    ...  01/04/2014  16/04/2014
    ...  02/04/2014  15/04/2014
    Vérifier Que Le PDF Contient Des Strings  listeregistreprocuration  ${contenu_pdf}

    # Vérification du registre général des procurations
    Click Link  Édition
    Sleep  1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  Générales
    # Registre des procurations classées par mandant
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  css=[href*=listeregistretotproc]
    ${contenu_pdf} =  Create List  ÉDITION DU REGISTRE DES PROCURATIONS
    # on veut qu'il y ait les deux procurations dont celle refusée
    ...  Ceci est un motif
    ...  01/04/2014  16/04/2014
    ...  02/04/2014  15/04/2014
    Vérifier Que Le PDF Contient Des Strings  listeregistretotproc  ${contenu_pdf}


Création procuration (avec prénom contenant une lettre minuscule)
    [Documentation]  Test de non regression sur un bug qui faisait échouer la
    ...  création d'une procuration si un mandataire/mandatant avait une lettre
    ...  en minuscule dans son prénom. L'IHM ne laisse pas arriver cela mais
    ...  lors d'un import INSEE, il arrive que les lettres accentuées dans le
    ...  prénom/nom soient en minuscules.

    Depuis la page d'accueil  admin  admin

    Importer le fichier d'inscriptions d'office  io_import_tnr_prenom_minuscule.xml

    # On va sur la liste et on vérifie que notre inscription est là
    Click Link  Validation des inscriptions d'office INSEE
    Page Should Contain  LéA MEVEL 15 BOULEVARD MAQUIS MARCEAU 00000 OPENELEC
    Valider une inscription d'office

    Appliquer le traitement de fin d'année

    &{args_procuration} =  Create Dictionary
    ...  nom_mandant=DADOU
    ...  nom_mandataire=MEVEL
    ...  debut_validite=01/01/2015
    ...  fin_validite=01/01/2015
    ...  date_accord=01/01/2014
    ...  heure_accord=15:00:00
    Ajouter une procuration  ${args_procuration}
    Valid Message Should Be  Vos modifications ont bien été enregistrées.

    Click Link  css=.retour
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Page Should Contain Element  name=recherche  # retour à la liste
    Page Should Contain  MEVEL LéA

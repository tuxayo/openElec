*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Test suites de génération des PDF


*** Test Cases ***
PDFs fiche électeur

    Depuis la page d'accueil  admin  admin

    # Consultation → Liste électorale
    Go To Submenu In Menu  consultation  electeurs
    Click Link  ABAY

    Click Link  Attestation d'électeur
    ${contenu_pdf} =  Create List  ATTESTATION D'INSCRIPTION  M. ABAY BARAN
    Vérifier Que Le PDF Contient Des Strings  pdfetat  ${contenu_pdf}

    Click Link  Carte d'électeur
    ${contenu_pdf} =  Create List  M. ABAY BARAN
    # À part le nom de la fenêtre et la personne, il ne semble pas possible
    # d'arriver à vérifier que le PDF est ce celui de la carte d'électeur.
    Vérifier Que Le PDF Contient Des Strings  carteelecteur  ${contenu_pdf}


PDFs liste d'inscription

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  consultation  inscriptions

    # Ouverture de l'attestation de la 1ere ligne
    Click Element  css=.tab-data:nth-child(2) .pdf-16
    ${contenu_pdf} =  Create List  ATTESTATION D'INSCRIPTION  M. BONNET HERVE
    Vérifier Que Le PDF Contient Des Strings  pdfetat  ${contenu_pdf}
    # Ouverture du refus
    Click Element  css=.tab-data:nth-child(2) .courrierrefus-16
    ${contenu_pdf} =  Create List  REFUS D'INSCRIPTION  M. BONNET HERVE
    Vérifier Que Le PDF Contient Des Strings  pdfetat  ${contenu_pdf}


PDFs modifications

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  consultation  modifications

    # Ouverture de l'attestation de la 1ere ligne
    Click Element  css=.tab-data:nth-child(2) .pdf-16
    ${contenu_pdf} =  Create List  AVIS DE MODIFICATION  M. DUPONT GILBERT
    Vérifier Que Le PDF Contient Des Strings  pdfetat  ${contenu_pdf}
    # Ouverture du refus de modification
    Click Element  css=.tab-data:nth-child(2) .courrierrefus-16
    ${contenu_pdf} =  Create List  REFUS DE MODIFICATION  M. DUPONT GILBERT
    Vérifier Que Le PDF Contient Des Strings  pdfetat  ${contenu_pdf}


PDFs radiations

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  consultation  radiations

    # Ouverture de l'attestation de la 1ere ligne
    Click Element  css=.tab-data:nth-child(2) .pdf-16
    ${contenu_pdf} =  Create List  ATTESTATION DE RADIATION  M. AUBAGNE JEAN
    Vérifier Que Le PDF Contient Des Strings  pdfetat  ${contenu_pdf}
    # Ouverture du refus
    Click Element  css=.tab-data:nth-child(2) .courrierrefus-16
    ${contenu_pdf} =  Create List  REFUS DE RADIATION  M. AUBAGNE JEAN
    Vérifier Que Le PDF Contient Des Strings  pdfetat  ${contenu_pdf}


PDFs révision électorale

    Depuis la page d'accueil  admin  admin
    Click Link  Édition
    Sleep  1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  Révision électorale

    # on va sur une révision avec des mouvements et donc un tableau 5J
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  css=.pagination-prev
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  css=.pagination-prev

    # Tableau 5J des commune 19/12/2012
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  css=.detail-premier-tableau tr:nth-child(2) [title='Tableau des cinq jours sur toute la commune']
    ${contenu_pdf} =  Create List  TABLEAU DES CINQ JOURS
    ${ne_doit_pas_contenir} =  Create List  HOTEL DE VILLE
    Vérifier Que Le PDF Contient Des Strings Mais Pas D'Autres
    ...  commission  ${contenu_pdf}  ${ne_doit_pas_contenir}

    # Tableau des 5J par bureau 19/12/2012
    Click Link  css=.detail-premier-tableau tr:nth-child(2) [title='Tableau des cinq jours avec coupure par bureau']
    ${contenu_pdf} =  Create List  TABLEAU DES CINQ JOURS  HOTEL DE VILLE
    Vérifier Que Le PDF Contient Des Strings  commission  ${contenu_pdf}

    # 19/12/2012 → Traitement J-5 → Nouvelles cartes électorales
    Click Link  css=.detail-premier-tableau tr:nth-child(4) [href*=carteelecteur]
    ${contenu_pdf} =  Create List  NOUVELLES CARTES ÉLECTORALES SUITE AU TRAITEMENT
    Vérifier Que Le PDF Contient Des Strings  carteelecteur  ${contenu_pdf}

    # 19/12/2012 → Traitement J-5 → Récapitulatif
    Click Link  css=.detail-premier-tableau tr:nth-child(4) [href*=recap]
    ${contenu_pdf} =  Create List  Détail des mouvements appliqués lors du traitement
    Vérifier Que Le PDF Contient Des Strings
    ...  pdf_recapitulatif_traitement  ${contenu_pdf}

    # Tableau des 5J commune 21/12/2012
    Click Link  css=.detail-premier-tableau tr:nth-child(5) [title='Tableau des cinq jours sur toute la commune']
    ${contenu_pdf} =  Create List  TABLEAU DES CINQ JOURS
    Vérifier Que Le PDF Contient Des Strings  commission  ${contenu_pdf}

    # Tableau des 5J par bureau 21/12/2012
    Click Link  css=.detail-premier-tableau tr:nth-child(5) [title='Tableau des cinq jours avec coupure par bureau']
    ${contenu_pdf} =  Create List  TABLEAU DES CINQ JOURS
    Vérifier Que Le PDF Contient Des Strings  commission  ${contenu_pdf}

    # Premier tableau rectificatif → Récapitulatif
    Click Link  css=.tableau:nth-child(1) [href*=recapitulatif_tableau]
    ${contenu_pdf} =  Create List  Récapitulatif du tableau rectificatif
    ...  1 - HOTEL DE VILLE  2 - ECOLE MATERNELLE  3 - SALLE DES FETE
    Vérifier Que Le PDF Contient Des Strings  pdffromarray  ${contenu_pdf}

    # Premier tableau rectificatif → Tableau (commune)
    Click Link  css=.tableau:nth-child(1) [href*=globale]
    ${contenu_pdf} =  Create List  TABLEAU RECTIFICATIF  Commune : OPENELEC
    Vérifier Que Le PDF Contient Des Strings  commission  ${contenu_pdf}

    # Premier tableau rectificatif → Tableau (par bureau)
    Click Link  css=.tableau:nth-child(1) li:nth-child(1) a
    ${contenu_pdf} =  Create List  TABLEAU RECTIFICATIF  Commune : OPENELEC
    Vérifier Que Le PDF Contient Des Strings  commission  ${contenu_pdf}

    # Second tableau rectificatif → Nouvelles cartes électorales
    Click Link  css=.tableau:nth-child(2) li:nth-child(4) a
    ${contenu_pdf} =  Create List  NOUVELLES CARTES ÉLECTORALES SUITE AU TRAITEMENT ANNUEL
    Vérifier Que Le PDF Contient Des Strings  carteelecteur  ${contenu_pdf}

    # Second tableau rectificatif → Récapitulatif
    Click Link  css=.tableau:nth-child(2) li:nth-child(3) a
    ${contenu_pdf} =  Create List  Récapitulatif du tableau rectificatif
    ...  1 - HOTEL DE VILLE  2 - ECOLE MATERNELLE  3 - SALLE DES FETE
    Vérifier Que Le PDF Contient Des Strings  pdffromarray  ${contenu_pdf}

    # Second tableau rectificatif → Tableau (commune)
    Click Link  css=tr:nth-child(6) [href*=globale]
    ${contenu_pdf} =  Create List  TABLEAU RECTIFICATIF  Commune : OPENELEC
    Vérifier Que Le PDF Contient Des Strings  commission  ${contenu_pdf}

    # Second tableau rectificatif → Tableau (par bureau)
    Click Link  css=tr:nth-child(6) li:nth-child(1) a
    ${contenu_pdf} =  Create List  TABLEAU RECTIFICATIF  Commune : OPENELEC
    Vérifier Que Le PDF Contient Des Strings  commission  ${contenu_pdf}


PDFs par bureau

    Depuis la page d'accueil  admin  admin
    Click Link  Édition
    Sleep  1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  Par Bureau

    # On vérifie tout les PDFs du bureau 1

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  css=[href*=listeemargement][href$="1"]
    ${contenu_pdf} =  Create List  LISTE D'ÉMARGEMENT
    Vérifier Que Le PDF Contient Des Strings  listeemargement  ${contenu_pdf}

    Click Link  css=[href*=listegenerale][href$="1"]
    ${contenu_pdf} =  Create List  LISTE ÉLECTORALE
    Vérifier Que Le PDF Contient Des Strings  listegenerale  ${contenu_pdf}

    Click Link  css=[href*=pdfetiquette][href$="1"]
    # on check le numéro d'ordre, seule chose discriminante avec les autres PDFs
    ${contenu_pdf} =  Create List  1 - 29
    Vérifier Que Le PDF Contient Des Strings  pdfetiquette  ${contenu_pdf}

    Click Link  css=[href*=carteelecteur][href$="1"]
    ${contenu_pdf} =  Create List  CARTES ÉLECTORALES
    Vérifier Que Le PDF Contient Des Strings  carteelecteur  ${contenu_pdf}

    Click Link  css=[href*=commission][href$="1"]
    ${contenu_pdf} =  Create List  ÉTAT POUR LA COMMISSION
    Vérifier Que Le PDF Contient Des Strings  commission  ${contenu_pdf}

    Click Link  css=[href*=listeprocuration][href$="1"]
    ${contenu_pdf} =  Create List  LISTE DES PROCURATIONS
    Vérifier Que Le PDF Contient Des Strings  listeprocuration  ${contenu_pdf}

    Click Link  css=[href*=listecarteretour][href$="1"]
    ${contenu_pdf} =  Create List  LISTE DES CARTES RETOURNÉES
    Vérifier Que Le PDF Contient Des Strings  listecarteretour  ${contenu_pdf}

    Click Link  css=[href*=listeregistreprocuration][href$="1"]
    ${contenu_pdf} =  Create List  ÉDITION DU REGISTRE DES PROCURATIONS
    Vérifier Que Le PDF Contient Des Strings  listeregistreprocuration  ${contenu_pdf}

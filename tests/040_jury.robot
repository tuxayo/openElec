*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Test suite des jurys d'assises


*** Test Cases ***
Éditions des informations de juré

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  traitement  jury  # Traitement → Jury D'assises

    Click Button  Tirage aléatoire
    Confirm Action
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Valid Message Should Contain  Le traitement est terminé.
    Click Link  Liste préparatoire courante

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Element  css=tr:nth-child(2) .edit-16  # Modifier le 1er électeur
    Select From List  name=jury_effectif  Oui
    Input Text  name=profession  ostréiculteur
    Page Should Not Contain Errors
    Click Button  css=input[type='button']  # On enregistre les modifications
    Page Should Contain  Date de prefecture obligatoire

    Input Text  name=date_jeffectif  06/08/2013
    # On enregistre les modifications
    Click Button  css=input[type='button']
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Page Should Contain  Vos modifications ont bien été enregistrées.
    ${id_electeur} =  Get Value  name=id_electeur

    Click Link  Tirage aléatoire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  Liste préparatoire du jury d'assises
    ${contenu_pdf} =  Create List  ostréiculteur
    Vérifier Que Le PDF Contient Des Strings  pdf_recapitulatif_jury  ${contenu_pdf}

    Click Link  Export CSV des électeurs sélectionnés
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Button  name=valid.reqmo
    Page Should Contain  Recherche${SPACE}${SPACE}EXPORT JURY D'ASSISES
    Page Should Contain  ostréiculteur

    # Consultation → Liste électorale
    Go To Submenu In Menu  consultation  electeurs
    Use Simple Search  Tous  ${id_electeur}
    Click Link  ${id_electeur}
    Page Should Contain  Jury d'assises :
    Page Should Contain  L'électeur fait parti de la liste provisoire courante.
    Page Should Contain  Profession de l'électeur : ostréiculteur
    Page Should Contain  L'électeur est juré effectif le : 06/08/2013

    Click Link  Juré d'assises
    Select From List  name=jury_effectif  Non
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Button  css=input[type='button']  # On enregistre les modifications
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Page Should Contain  Vos modifications ont bien été enregistrées.
    Click Link  css=#sformulaire .formControls .retour
    Page Should Not Contain   Date préfecture  # sous formulaire fermés
    Page Should Not Contain  L'électeur est juré effectif le : 06/08/2013

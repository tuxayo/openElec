*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Test suite des imports et exports INSEE

*** Test Cases ***
Import des inscriptions d'office depuis l'INSEE
    [Documentation]  Dependance de
    ...  "Export des mouvements des listes électorales: TXT"

    Depuis la page d'accueil  admin  admin

    Importer le fichier d'inscriptions d'office  io_import.xml

    # On va sur la liste et on vérifie que nos deux inscriptions sont là
    Click Link  Validation des inscriptions d'office INSEE
    Page Should Contain  BELLOZ PHILIPPE 5 BOULEVARD MAQUIS MARCEAU 00000 OPENELEC
    Page Should Contain  DEBORAH MITCHELL 15 BOULEVARD MAQUIS MARCEAU 00000 OPENELEC

    Valider une inscription d'office
    Valider une inscription d'office


Export des mouvements des listes électorales: TXT
    [Documentation]  Dépend de "Import des inscriptions d'office depuis l'INSEE"

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  traitement  insee

    # On vérifie qu'il n'y a pas déjà de fichier exporté. Car si il y en avait
    # il serait difficile de vérifier que l'export a marché et que ça n'est pas
    # un ancien fichier.
    Page Should Contain Element  css=#insee_export_list_content
    Page Should Not Contain Element  css=#insee_export_list_content .file-16

    Click Button  Génération du fichier Export INSEE
    Confirm Action
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Page Should Contain  Le traitement est terminé.  Voir le détail
    Page Should Not Contain  Aucun mouvement à envoyer. Aucun fichier n'a été généré.
    ${file_url}=  Get Element Attribute  css=#insee_export_list_content .file-16@href
    Click Link  css=#insee_export_list_content .file-16
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Select Window  url=${file_url}
    Page Should Contain  BELLOZ*PHILIPPE
    Close Window
    Select Window


Import des radiations depuis l'INSEE

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  traitement  insee
    Click Link  Import Radiation

    # Upload du fichier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Element  css=#traitement_insee_radiation_form .ui-icon-arrowthickstop-1-s
    Select Window  url=${PROJECT_URL}spg/upload.php?origine=fic1&form=f1
    Input Text  name=userfile[]  ${PATH_BIN_FILES}radiation_import.xml
    Click Button  css=[type="submit"]
    Sleep  1
    Select Window  # la pop up s'est fermée, on revient à la fenêtre principale

    Click Button  name=traitement.insee_radiation.form.valid
    Confirm Action
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Page Should Contain  Le transfert s'est effectué
    Page Should Contain  Le traitement est terminé
    Page Should Not Contain Errors

    Click Link  Validation des radiations INSEE
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Page Should Contain  CAMILLE
    Page Should Contain  LOUIS
    Page Should Contain  DADOU
    Page Should Contain  DAVID


*** Test Cases SKIPPED ***
Paramétrage partenaires
    [Documentation]  Ajout de partenaire pour l'envoi d'export xml

# La fonctionnalité ne marche pas. Depuis la v4.1.0-rc3, des colonnes ont été
# supprimées et le code n'a pas été mit à jour. Il s'en sert toujours ce qui
# déclenche l'erreur suivant au moment de la création de partenaire:
# [19206-1] ERROR:  column "identifiant_autorite" of relation "xml_partenaire" does not exist at character 52
# [19206-2] STATEMENT:  INSERT INTO public.xml_partenaire (type_partenaire,identifiant_autorite,identifiant,nom,type,role,contact_nom,contact_mail,contact_numerofax,contact_numerotelephone,contact_adresse_numerovoie,contact_adresse_typevoie,contact_adresse_nomvoie,contact_adresse_cedex,contact_adresse_libellebureaucedex,contact_adresse_divisionterritoriale,service_nom,service_fournisseur,service_version,service_accreditation,collectivite,xml_partenaire) VALUES ('origine',NULL,NULL,'LibreVille',NULL,NULL,'','','','','','','','','','',NULL,'',NULL,'','00000','1')

#   Depuis la page d'accueil  admin  admin
#   Go To Submenu In Menu  traitement  insee
#   Select From List  format  xml
#   Click Button  Génération du fichier Export INSEE
#   Confirm Action
#
#         SUITE DE L'ANCIEN TEST PHP Unit + Selenium.
#         Si un jour la fonctionalité est réparé, voici de quoi la tester
#         rapidement. Attention le test était obsolète, il faut ajouter un
#         mouvement (du bon type) pour que l'export INSEE se fasse.
#         Ou compter sur un autre test malgrès les problèmes que cela peut
#         impliquer.
#
#         // On vérifie qu'il n'y ait pas d'erreur
#         $this->verifynoerrors();
#         // Vérification du message d'erreur
#         $result = $this->byXPath("//div[@id='traitement_insee_export_result']/div")->text();
#         $this->assertEquals("Le traitement à échoué. Voir le détail\n\nLes partenaires d'origine et de destination des exports xml n'ont pas été paramétré.\nParametrer les partenaires.", $result);
#         // Acces au module de test insee
#         $this->byLinkText("Parametrer les partenaires.")->click();
#         // On vérifie qu'il n'y ait pas d'erreur
#         $this->verifynoerrors();
#         // Ajout d'un partenaire
#         $this->byXPath("//div[@id='tabs-1']/table/tbody/tr/th/a/span")->click();
#         // On vérifie qu'il n'y ait pas d'erreur
#         $this->verifynoerrors();
#         // Saisie des infos du partenaire
#         $this->byName("identifiant")->value("00000");
#         $this->byName("nom")->value("LibreVille");
#         $this->byXPath("//input[@value=\"Ajouter l'enregistrement de la table : 'Xml_partenaire'\"]")->click();
#         // On vérifie qu'il n'y ait pas d'erreur
#         $this->verifynoerrors();
#         try {
#             $result = $this->byCssSelector("span.text")->text();
#         $this->assertEquals("Vos modifications ont bien été enregistrées.", $result);
#         } catch (PHPUnit_Framework_AssertionFailedError $e) {
#             array_push($this->verificationErrors, $e->__toString());
#         }
#         // Retour au tableau pour vérification du lien vers le courrier de refus
#         $this->byLinkText("Retour")->click();
#         // On vérifie qu'il n'y ait pas d'erreur
#         $this->verifynoerrors();
#         // Ajout d'un partenaire
#         $this->byXPath("//div[@id='tabs-1']/table/tbody/tr/th/a/span")->click();
#         // On vérifie qu'il n'y ait pas d'erreur
#         $this->verifynoerrors();
#         // Saisie des infos
#         $this->byName("type_partenaire")->value("destination");
#         $this->byName("identifiant")->value("00001");
#         $this->byName("nom")->value("INSEE");
#         $this->byXPath("//input[@value=\"Ajouter l'enregistrement de la table : 'Xml_partenaire'\"]")->click();
#         // On vérifie qu'il n'y ait pas d'erreur
#         $this->verifynoerrors();
#         try {
#             $result = $this->byCssSelector("span.text")->text();
#         $this->assertEquals("Vos modifications ont bien été enregistrées.", $result);
#         } catch (PHPUnit_Framework_AssertionFailedError $e) {
#             array_push($this->verificationErrors, $e->__toString());
#         }
#         // Retour
#         $this->byLinkText("Retour")->click();
#         // On vérifie qu'il n'y ait pas d'erreur
#         $this->verifynoerrors();
#         // On se déconnecte
#         $this->logout();
#     }


Export des mouvements pour envoi à l'INSEE
    [Documentation]  Depend de "Paramétrage partenaires"
# La fonctionnalité ne marche pas. Elle dépend de la création de partenaires.
#
# ANCIEN TEST PHP Unit + Selenium:
#     /**
#      * Méthode permettant de tester l'export des mouvements des listes électorales
#      * au format XML
#      */
# 	public function test_export_insee_xml() {
#         // On se connecte
#         $this->login("admin", "admin");
#         // Accès au module insee
#         $this->byLinkText("Traitement")->click();
#         $this->byLinkText("Insee")->click();
#         // On vérifie qu'il n'y ait pas d'erreur
#         $this->verifynoerrors();
#         // Configuration de l'export xml
#         $this->byName("envoi")->value("1");
#         $this->byName("format")->value("xml");
#         $this->byName("traitement.insee_export.form.valid")->click();
#         $this->acceptAlert();
#         // Récupération du datetime
#         $date_traitement = date("d/m/Y H:i:s");
#         sleep(1);
#         // On vérifie qu'il n'y ait pas d'erreur
#         $this->verifynoerrors();
#         $this->assertRegExp('/^Le traitement est terminé. Voir le détail$/',
#                                 $this->byCssSelector("span.text")->text());
#         $this->assertRegExp('/^[\s\S]*Export du '.str_replace(array("/"," ",":"), array("\\/","\\ ", "\\:"), $date_traitement).' \[1\][\s\S]*$/',$this->byId("insee_export_list_content")->text());
#         $result = $this->byLinkText("Export du ".$date_traitement." [1]")->text();
#         $this->assertEquals("Export du ".$date_traitement." [1]", $result);
#         // Ouverture du fichier généré
#         $this->byLinkText("Export du ".$date_traitement." [1]")->click();
#         sleep(5);
#         // Liste les fenêtres
#         $windows = $this->windowHandles();
#         // Change la fenêtre
#         $this->window($windows[1]);
#         // On vérifie qu'il n'y ait pas d'erreur
#         $this->verifynoerrors();
#         // Vérification des prestataires
#         $this->assertRegExp('/^[\s\S]*\<el\:Origine\>
#             \<io\:Identifiant autorite=\"http\:\/\/xml\.insee\.fr\/identifiants\/SIRET\"\>00000\<\/io\:Identifiant\>
#             \<io\:Nom\>LibreVille\<\/io\:Nom\>[\s\S]*$/',
#             $this->source());
#         $this->assertRegExp('/^[\s\S]*\<el\:Destination\>
#             \<io\:Identifiant autorite=\"http\:\/\/xml\.insee\.fr\/identifiants\/SIRET\"\>00001\<\/io\:Identifiant\>
#             \<io\:Nom\>INSEE\<\/io\:Nom\>[\s\S]*$/',
#             $this->source());
#         // Vérification de mouvements sur les électeurs
#         $this->assertRegExp('/^[\s\S]*\<el\:Noms\>
#                         \<ie\:NomFamille\>BONNET\<\/ie\:NomFamille\>
#                     \<\/el\:Noms\>
#                     \<el\:Prenoms\>
#                         \<ie\:Prenom\>HERVE\<\/ie\:Prenom\>
#                     \<\/el\:Prenoms\>[\s\S]*$/',
#             $this->source());
#         $this->assertRegExp('/^[\s\S]*\<el\:Noms\>
#                         \<ie\:NomFamille\>LACROIX\<\/ie\:NomFamille\>
#                     \<\/el\:Noms\>
#                     \<el\:Prenoms\>
#                         \<ie\:Prenom\>HUBERT\<\/ie\:Prenom\>
#                     \<\/el\:Prenoms\>[\s\S]*$/',
#             $this->source());
#         $this->assertRegExp('/^[\s\S]*\<el\:Noms\>
#                         \<ie\:NomFamille\>LACROIX\<\/ie\:NomFamille\>
#                     \<\/el\:Noms\>
#                     \<el\:Prenoms\>
#                         \<ie\:Prenom\>PAUL\<\/ie\:Prenom\>
#                     \<\/el\:Prenoms\>[\s\S]*$/',
#             $this->source());
#         // On ferme la fenêtre
#         $this->closeWindow();
#         // On change le focus de la fenêtre
#         $this->window($windows[0]);
#
#         // On se déconnecte
#         $this->logout();
# 	}

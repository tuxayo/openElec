*** Keywords ***
Appliquer le traitement de fin d'année
    [Tags]
    [Documentation]  Appliquer le traitement de fin d'année
    
    Go To  ${PROJECT_URL}/trt/module_traitement_annuel.php
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Button  name=traitement.annuel.form.valid
    Confirm Action
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Valid Message Should Contain  Le traitement est terminé.

#!/usr/bin/python
# -*- coding: utf-8 -*-
from resources.core.om_tests import om_tests_core


class om_tests(om_tests_core):
    """
    """

    _database_name_default = 'openelec'

    _instance_name_default = 'openelec'

    _params_copy_files = [
    ]

    _params_delete_files = [
        'tmp/*tedeco*',
        'tmp/*-traitement_*',
    ]

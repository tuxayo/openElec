*** Settings ***
Documentation  Actions spécifiques aux procurations

*** Keywords ***
Ajouter une procuration
    [Documentation]  Crée une procuration depuis du menu.
    ...  À la charge de l'appelant de vérifier le résultat.
    ...
    ...  Clefs de &args_procuration avec exemples de valeurs :
    ...  nom_mandant=Li  # doit pouvoir être trouvé directement par la
    ...  nom_mandataire=Doe  # corrélation c'est à dire pas d'ambiguïtés
    ...  debut_validite=02/04/2014
    ...  fin_validite=15/04/2014
    ...  date_accord=15/08/2013
    ...  heure_accord=15:00:00
    [Arguments]  ${args_procuration}

    Click Link  Consultation
    Sleep  1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  css=[href*="tab.php?obj=procuration"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Link  Ajouter  # plus(+) vert

    Input Text  z_mandant  ${args_procuration.nom_mandant}
    Click Link  css=[href*="correl=mandant"]
    # On s'assure que la correlation soit finie
    Attendre que le champ texte "mandant" soit vide

    Input Text  z_mandataire  ${args_procuration.nom_mandataire}
    Click Link  css=[href*="correl=mandataire"]
    # On s'assure que la correlation soit finie
    Attendre que le champ texte "mandataire" soit vide

    Input Text  debut_validite  ${args_procuration.debut_validite}
    Input Text  fin_validite  ${args_procuration.fin_validite}
    Input Text  date_accord   ${args_procuration.date_accord}
    Input Text  heure_accord  ${args_procuration.heure_accord}
    Click Button  css=[type=submit]


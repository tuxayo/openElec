*** Settings ***
Documentation  Actions sur un PDF visionné depuis Firefox.

*** Keywords ***
Vérifier Que Le PDF Contient Des Strings
    [Tags]
    [Documentation]  Change de fenêtre, fait les verifs, et ferme la fenêtre.
    ...  Il faut donc avoir déclenché l'ouverture de la fenêtre avant.
    ...  L'argument window est {name, title, url} de la fenêtre sans le suffixe '.php'
    [Arguments]  ${window}  ${strings_to_find}

    Open PDF  ${window}
    :FOR  ${string}  IN  @{strings_to_find}
    \    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    \    ...  Page Should Contain  ${string}

    Page Should Not Contain Errors
    Close PDF

Vérifier Que Le PDF Contient Des Strings Mais Pas D'Autres
    [Tags]
    [Documentation]  Change de fenêtre, fait les verifs, et ferme la fenêtre.
    ...  Il faut donc avoir déclenché l'ouverture de la fenêtre avant.
    ...  L'argument window est {name, title, url} de la fenêtre sans le suffixe '.php'
    [Arguments]  ${window}  ${strings_to_find}  ${strings_not_to_find}

    Open PDF  ${window}
    :FOR  ${string}  IN  @{strings_to_find}
    \    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    \    ...  Page Should Contain  ${string}

    :FOR  ${string}  IN  @{strings_not_to_find}
    \    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    \    ...  Page Should Not Contain  ${string}

    Page Should Not Contain Errors
    Close PDF

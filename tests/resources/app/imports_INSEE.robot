*** Keywords ***
Importer le fichier d'inscriptions d'office
    [Tags]
    [Documentation]  Importe le fichier d'inscriptions d'office dont le nom est
    ...  donné en paramètre. Il doit se trouver dans ${PATH_BIN_FILES}.
    [Arguments]  ${file_name}

    Go To Submenu In Menu  traitement  insee
    Click Link  Import IO

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Click Element  css=#traitement_insee_inscription_office_form .ui-icon-arrowthickstop-1-s
    Select Window  url=${PROJECT_URL}spg/upload.php?origine=fic1&form=f1
    Input Text  name=userfile[]  ${PATH_BIN_FILES}/${file_name}
    Click Button  css=[type="submit"]
    Sleep  1
    Select Window  # la pop up s'est fermée, on revient à la fenêtre principale

    # Le champ doit être non vide pour confirmer que l'envoi du fichier a réussi
    Run Keyword And Expect Error
    ...  Value of text field 'fic1' should have been '' but was*
    ...  Textfield Value Should Be  fic1  ${EMPTY}

    Click Button  name=traitement.insee_inscription_office.form.valid
    Confirm Action
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Page Should Contain  Le transfert s'est effectué
    Page Should Contain  Le traitement est terminé
    Capture Page Screenshot
    Page Should Not Contain Errors


Valider une inscription d'office
    [Tags]
    [Documentation]  Valide la 1re inscription d'office non validée
    ...  dans la liste.
    ...  Dépendances: être sur la liste de validation des inscriptions d'office
    ...  INSEE →  Import IO → Validation des inscriptions d'office INSEE

    Click Link  Valider l'inscription d'office
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Input Text  libelle_voie  BOULEVARD MAQUIS MARCEAU
    Click Link  css=[href*="zorigine=libelle_voie"]  # faire la correlation
    Textfield Value Should Be  code_voie  5
    Click Button  css=[type=submit]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Click Link  css=.retour
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Page Should Contain Element  name=recherche  # retour à la liste

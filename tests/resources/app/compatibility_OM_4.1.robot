*** Settings ***
Documentation     Surcharges de keywords pour la compat avec openMairie 4.1
...
...               om_tests vient de la version 4.5.1 du framework openMairie.
...               openElec est sur la 4.1.X. Du fait des changement d'UI qu'il
...               a eu, certains keyword ne sont pas compatibles.


*** Keywords ***

# REASON for override: With framework 4.1.x it's hard to get the current logged in
# user, which is used to check if we need to disconnect to change user.
# The workaround is just to disconnect unconditionnaly, so that part was
# removed.
Depuis la page d'accueil
    [Tags]  compatibility_OM_4.1
    [Arguments]  ${username}  ${password}
    [Documentation]    L'objet de ce 'Keyword' est de positionner l'utilisateur
    ...  sur la page de login ou son tableau de bord si on le fait se connecter.

    # On accède à la page d'accueil
    Go To  ${PROJECT_URL}
    Page Should Not Contain Errors

    # On vérifie si un utilisateur est connecté ou non
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#title h2
    ${titre} =  Get Text  css=#title h2
    ${is_connected} =  Evaluate  "${titre}"!="Veuillez Vous Connecter"

    # On se déconnecte si un utilisateur est déjà connecté
    Run Keyword If  "${is_connected}"=="True"  Click Link  css=#actions a.actions-logout

    # On vérifie si l'utilisateur est connecté ou non
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#title h2
    ${titre} =  Get Text  css=#title h2
    ${is_connected} =  Evaluate  "${titre}"!="Veuillez Vous Connecter"

    # On s'authentifie avec le nouvel utilisateur
    S'authentifier  ${username}  ${password}


# REASON for override: Submenu selection isn't implemented with framework 4.1.x
# The left menu doesn't show the current menu.
Submenu In Menu Should Be Selected
    [Tags]  compatibility_OM_4.1
    [Arguments]    ${rubrikclass}    ${elemclass}
    No Operation


# REASON for override: confirmation message css selector changed
Click On Submit Button
    [Tags]  compatibility_OM_4.1
    [Arguments]    ${nomessage}=null
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click Element  css=#formulaire div.formControls input[type="submit"]
    # On attend le message de validation, qu'il soit correct ou renvoie une erreur.
    Run Keyword If  '${nomessage}' == 'null'  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=.message
    Page Should Not Contain Errors


# REASON for override: in newer versions of the framework, there is no more an edit
# link in the listing. Il faut afficher d'abord les détails.
# So here we use the old way of editing.
Click On Edit Button
    [Tags]  compatibility_OM_4.1
    Click Element    css=span.edit-16
    #Sleep    0.5 if needed, wait for visibility of .formEntete rather than sleeping
    Page Should Not Contain Errors

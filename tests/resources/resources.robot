*** Settings ***
Documentation     Ressources (librairies, keywords et variables)

# Mots-clefs framework
Resource          core${/}om_resources.robot

# Mots-clefs métier
Resource  app${/}compatibility_OM_4.1.robot
Resource  app${/}formulaire.robot
Resource  app${/}imports_INSEE.robot
Resource  app${/}pdf.robot
Resource  app${/}procuration.robot
Resource  app${/}traitements.robot

*** Variables ***
${ADMIN_PASSWORD}  admin
${ADMIN_USER}      admin
${BROWSER}         firefox
${DELAY}           0
${PATH_BIN_FILES}  ${EXECDIR}${/}binary_files${/}
${PROJECT_NAME}    openelec
${PROJECT_URL}     http://${SERVER}/${PROJECT_NAME}/
${SERVER}          localhost
${TITLE}           :: openMairie :: openElec - Gestion des listes électorales

*** Keywords ***
For Suite Setup
    # Les keywords définit dans le resources.robot sont prioritaires
    Set Library Search Order  resources  compatibility_OM_4.1
    Ouvrir le navigateur
    Tests Setup

For Suite Teardown
    Fermer le navigateur

*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Test suite des découpages

*** Test Cases ***
Créer un nouveau canton

    Depuis la page d'accueil    admin    admin
    Go To Submenu In Menu    decoupage    canton
    Page Title Should Be    Découpage > Canton
    Click On Add Button
    Page Title Should Be    Découpage > Canton

    Input Text    xpath=//input[@name='code']       02
    Input Text    xpath=//input[@name='libelle_canton']       CANTON DE TEST
    Input Text    xpath=//input[@name='code_insee']       0002

    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

    # Seul endroit dans les tests où on teste la déconnexion
    Go To    ${PROJECT_URL}
    Se déconnecter

Ajouter un bureau dans le nouveau canton

    Depuis la page d'accueil    admin    admin
    # Ajout d'un bureau au nouveau canton
    Go To Submenu In Menu    decoupage    bureau
    Page Title Should Be    Découpage > Bureau
    Use Simple Search    Tous    HOTEL DE VILLE
    Click On Edit Button
    Wait Until Keyword Succeeds     5 sec     0.2 sec    Select From List    name=code_canton    02 CANTON DE TEST
    Click On Submit Button
    # Vérification de la création d'un paramétrage de jury supplémentaire
    Go To Submenu In Menu    traitement    jury
    Element Should Contain     css=#traitement_jury_status ul    Canton 01 - CANTON DE LIBREVILLE
    Element Should Contain     css=#traitement_jury_status ul    Canton 02 - CANTON DE TEST
    Xpath Should Match X Times    //div[@id="traitement_jury_status"]/ul/li    2


Supprimer le bureau du nouveau canton

    Depuis la page d'accueil    admin    admin
    # Changement de canton
    Go To Submenu In Menu    decoupage    bureau
    Page Title Should Be    Découpage > Bureau
    Use Simple Search    Tous    HOTEL DE VILLE
    Click On Edit Button
    Sleep    1
    Select From List    name=code_canton    01 CANTON DE LIBREVILLE
    Click On Submit Button
    # Vérification de la suppression du paramétrage de jury supplémentaire
    Go To Submenu In Menu    traitement    jury
    Element Should Contain     css=#traitement_jury_status ul    Canton 01 - CANTON DE LIBREVILLE
    Xpath Should Match X Times    //div[@id="traitement_jury_status"]/ul/li    1


Créer une nouvelle voie

    Depuis la page d'accueil    admin    admin
    Go To Submenu In Menu    decoupage    voie
    Page Title Should Be    Découpage > Voie
    Click On Add Button
    Page Title Should Be    Découpage > Voie

    Input Text    name=libelle_voie       AVENUE DU 8 MAI
    Input Text    name=cp       35210
    Input Text    name=ville       LIBREVILLE

    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.


Fusionner des voies

    Depuis la page d'accueil    admin    admin
    # Accès au redécoupage
    Go To Submenu In Menu    traitement    redecoupage
    Page Title Should Be    Traitement > Module Redecoupage
    Click Link    Initialisation
    Page Should Not Contain Errors
    # Vérification du contrôle d'erreurs
    Click Element    name=traitement.decoupage_normalisation_voies.form.valid
    Confirm Action
    Error Message Should Contain    Veuillez renseigner le champ : Libelle de la nouvelle voie.
    Click Element    css=#normalisation_voies>legend
    Sleep    1
    Input Text    name=new_label    AVENUE DU HUIT MAI
    Click Element    name=traitement.decoupage_normalisation_voies.form.valid
    Confirm Action
    Error Message Should Contain    Veuillez sélectionner 2 voies à fusionner.
    # Cas typique
    Click Element    css=#normalisation_voies>legend
    Sleep    1
    Select Checkbox    css=#AVENUE_DU_HUIT_MAI
    Select Checkbox    css=#AVENUE_DU_8_MAI
    Input Text    name=new_label    AVENUE DU HUIT MAI
    Click Element    name=traitement.decoupage_normalisation_voies.form.valid
    Confirm Action
    Page Should Not Contain Errors
    Valid Message Should Be    Le traitement est terminé. Voir le détail


Electeur bureau force

    Depuis la page d'accueil    admin    admin
    # Accès à l'initialisation du bureau forcé
    Go To Submenu In Menu    traitement    redecoupage
    Page Title Should Be    Traitement > Module Redecoupage
    Click Link    Initialisation
    Page Should Not Contain Errors
    Click Element    name=traitement.decoupage_init_bureauforce.form.valid
    Confirm Action
    Page Should Not Contain Errors
    Valid Message Should Contain    Le traitement est terminé. Voir le détail
    Valid Message Should Contain    5 électeur(s) modifié(s)
    Valid Message Should Contain    1 mouvement(s) modifié(s)

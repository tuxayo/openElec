<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Decoupage")." -> "._("Canton");

// Icone
$ico = "ico_decoupage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "canton";

// Nombre d'enregistrements par page
$serie = 15;

// Critere from de la requete
$table = "canton";

// Critere select de la requete
$champAffiche = array(
    "code as \""._("Code")."\"",
    "libelle_canton as \""._("Libelle")."\"",
    "code_insee as \""._("Code INSEE")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "code as \""._("Code")."\"",
    "libelle_canton as \""._("Libelle")."\"",
    "code_insee as \""._("Code INSEE")."\"",
);

// Critere where de la requete
$selection = "";

// Critere order by ou group by de la requete
$tri = "order by libelle_canton";

?>
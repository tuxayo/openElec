<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Consultation")." -> "._("Jury d'assises");

// Icone
$ico = "ico_jury.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 12;

// Critere from de la requete
$table = "electeur ";
$table .= "LEFT JOIN bureau ON electeur.code_bureau=bureau.code ";
$table .= "    AND electeur.collectivite=bureau.collectivite ";
$table .= "LEFT JOIN canton ON bureau.code_canton=canton.code ";


// Critere select de la requete
$champAffiche = array(
    "id_electeur as \""._("Id")."\"",
    "nom as \""._("Nom")."\"",
    "prenom as \""._("Prenom")."\"",
    "nom_usage as \""._("Nom d'usage")."\"",
    "(to_char(date_naissance,'DD/MM/YYYY')||' "._("a")." '||libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')') as \""._("Date et lieu de naissance")."\"",
    "(numero_habitation||' '||complement_numero||' '||libelle_voie) as \""._("Adresse")."\"",
    "code_bureau as \""._("Bureau")."\"",
    "libelle_canton as \""._("Canton")."\"",
    "typecat as \""._("En cours")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
   "id_electeur",
   "nom",
   "prenom"
);

// Critere where de la requete
$selection = " where electeur.collectivite = '".$_SESSION['collectivite']."' ";
$selection .= " and liste = '".$_SESSION['liste']."' ";
$selection .= " and jury=1";

// Critere order by ou group by de la requete
$tri = " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";

/**
 * Tableau de liens
 */
$href = array(
    0 => array("lien" => "", "id" => "", "lib" => "", ),
    1 => array(
            "lien" => "../scr/sousform.php?obj=electeur_jury&amp;idx=",
            "id" => "&amp;objsf=".$obj."&amp;retourformulaire=".$retourformulaire."&amp;idxformulaire=".$idxformulaire,
            "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix edit-16\" title=\""._("Modifier")."\">"._("Modifier")."</span>",
        ),
    2 => array("lien" => "", "id" => "", "lib" => "", ),
    3 => array("lien" => "", "id" => "", "lib" => "", ),
);

//
$formulaire = $table.".form";

/**
 * Options
 */
//
$options = array();
$option = array(
    "type" => "search",
    "display" => false,
);
array_push($options, $option);
//
$option = array(
    "type" => "condition",
    "field" => "typecat",
    "case" => array(
        "0" => array(
            "values" => array("modification", ),
            "style" => "modification-encours",
        ),
        "1" => array(
            "values" => array("radiation", ),
            "style" => "radiation-encours",
        ),
    ),
);
array_push($options, $option);

?>
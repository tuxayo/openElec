<?php
// $Id: import_electeur.inc,v 1.1 2006-09-08 19:43:30 fraynaud Exp $
$import= "Electeur => insert en table electeur";
$table= "electeur";
$id="id_electeur"; // numerotation automatique
// admin======================================================================
// verrou= 1 mise a jour de la base
//       = 0 pas de mise a jour  => phase de test
// debug=1 affichage des enregistrements à l ecran
//      =0 pas d affichage
// =============================================================================
$verrou=1;// =0 pas de mise a jour de la base / =1 mise à jour
$DEBUG=0; // =0 pas d affichage messages / =1 affichage detail enregistrement
$fic_erreur=1; // =0 pas de fichier d erreur / =1  fichier erreur
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
                // contenant les enregistrements en erreur
// 1ere ligne ==================================================================
// la premiere ligne contient les noms de champs
// 1=oui
// 0=non
// =============================================================================
$ligne1=0;
// parametrage des controles ===================================================
// zone obligatoire
$obligatoire['numero_electeur']=1;
$obligatoire['nom']=1;  // conseille = 1
$obligatoire['prenom']=0;
$obligatoire['code_bureau']=1;// conseille = 1
$obligatoire['bureauforce']=1;
$obligatoire['date_naissance']=0;
$obligatoire['liste']=1; // conseille = 1
// test dexistence 0=non / 1=oui
$exist['liste']=0;       // la liste doit exister dans la table liste
$exist['code_bureau']=0; // le bureau doit exister dans la table bureau      // le type de mouvement doit exister en param_mouvement
$exist['nationalite']=0;
$exist['code_voie'] =0;
//  zone à inserer ============================================================
// insertion d un enregistrement dans la table mouvement
// liste des zones à inserer
// mettre en commentaire les zones non traitées
// =============================================================================
      $zone['numero_electeur']='0';
      $zone['liste']='1';
      $zone['code_bureau']='2';
      $zone['bureauforce']="3"; //3
      $zone['numero_bureau']='4';
      $zone['date_modif']= '';
      $zone['utilisateur']='';
      $zone["civilite"]='5';
      $zone["sexe"]='6';
      $zone['nom']='7';
      $zone['nom_usage']="8";
      $zone['prenom']="9";   // null non accepté
      $zone['situation']="10";
      $zone['date_naissance']='11';
      $zone['code_departement_naissance']='12'; //code departement 2 caractères
      $zone['libelle_departement_naissance']="13";
      $zone['code_lieu_de_naissance']='14';    //21 code commune 6 caractères
      $zone['libelle_lieu_de_naissance']="15";
      $zone['code_nationalite']="16"; //16
      $zone['code_voie']="17";//"17"; // numerique 17
      $zone["libelle_voie"]='18';
      $zone['numero_habitation']="19"; // numerique 19
      $zone["complement_numero"]="20"; //20
      $zone['complement']="21"; //21
      $zone['provenance']="22"; //22
      $zone['libelle_provenance']="23"; //23
      $zone['resident']='24'; //24
      /*
      // CG
      $zone['adresse_resident']="";
      $zone['complement_resident']="";
      $zone['cp_resident']="";
      $zone['ville_resident']="";
      $zone['date_tableau'] ='';
      */
// tables de correspondance ====================================================
// openelec impose des codes qui sont repris dans divers états et traitement
// compléter avec les valeurs correspondantes du fichier csv
// cette partie n est pas traite
// =============================================================================
// civilite => autorise M., Mme, Melle
$corr['civilite']['M.'] = "1";
$corr['civilite']['Mme'] = "2";
$corr['civilite']['Melle'] ="";
// sexe autorisé M ou F
$corr['sexe']['M'] = "1";
$corr['sexe']['F'] = "2";
// resident  autorise Oui ou Non
// en cas de resident = Oui => adresse de resident pris en compte
$corr['resident']['Oui'] = "";
$corr['resident']['Non'] = "";
// bureau force autorise Oui ou Non
$corr['bureauforce']['Oui'] = "Oui";
$corr['bureauforce']['Non'] = "Non";
// complement numero
// autorisé: 'bis','ter','quater');
$corr['complement_numero']['bis'] = "bis";
$corr['complement_numero']['ter'] = "ter";
$corr['complement_numero']['quater'] ="quater";
// format de données ===========================================================
// format des dates dans le fichier CSV               => non traite
// date mysql et pgsql par defaut aaaa-mm-jj ou aaaammjj
   $format['date_naissance']= "jjmmaaaa"; // => aaaa-mm-jj
   $format['date_modif']= "jjmmaaaa";   // => aaaa-mm-jj
// zone par defaut ==== ========================================================
// valeur par defaut
// si les zones ne sont pas dans le fichier CSV   zone[champ]=''
// =============================================================================
      $defaut['bureauforce']='Oui';   // Oui ou Non
      $defaut['utilisateur']='admin';
      $defaut['date_modif']=date('Y-m-d');
      $defaut['code_nationalite']="FRA";
      $defaut['resident']="Non";
      $defaut['prenom']=""; // null non accepte
      $defaut['code_voie']=0; // numerique
      $defaut['numero_habitation']=0; //numerique
      $defaut['date_tableau']=null; //numerique
      $defaut['adresse_resident']="";   // null non accepte
      $defaut['complement_resident']="";  // null non accepte
      $defaut['libelle_provenance']="";    // null non accepte
?>
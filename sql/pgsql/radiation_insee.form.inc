<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

// Critere from de la requete
$tableSelect="radiation_insee";

// Critere select de la requete
$champs=array("id",
    "nationalite",
    "sexe",
    "nom",
    "prenom1",
    "prenom2",
    "prenom3",
    "date_naissance",
    "localite_naissance",
    "code_lieu_de_naissance",
    "pays_naissance",
    "adresse1",
    "adresse2",
    "date_deces",
    "localite_deces",
    "code_lieu_de_deces",
    "pays_deces",
    "date_nouvelle_inscription",
    "motif_nouvelle_inscription",
    "code_perte_de_nationalite",
    "code_autre_motif_de_radiation",
    "libelle_lieu_de_deces",
    "libelle_lieu_de_naissance",
    "motif_de_radiation",
    "type_de_liste"
    );

// Critere where de la requete
$selection = "";

?>


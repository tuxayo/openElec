<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Parametrage")." -> "._("Numero Bureau");

// Icone
$ico = "ico_parametrage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 15;

// Critere from de la requete
$table = "numerobureau as nb";

// Critere select de la requete
$champAffiche = array(
    "nb.id",
    "nb.liste",
    "nb.bureau",
    "nb.dernier_numero",
    "nb.dernier_numero_provisoire",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "nb.liste",
    "nb.bureau",
);

// Critere where de la requete
$selection = "where nb.collectivite='".$_SESSION['collectivite']."'";

// Critere order by ou group by de la requete
$tri = "order by nb.id";

?>
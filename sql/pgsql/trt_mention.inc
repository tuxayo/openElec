<?php
/**
 * ...
 *
 * @package openelec
 * @version SVN : $Id$
 */

// Epuration PROCURATION
if (isset ($dateElectionR))
{
    $sqlE = "select procuration.id_procuration, procuration.mandant, procuration.mandataire, ";
    $sqlE .= "procuration.fin_validite, electeur.id_electeur, electeur.liste ";
    $sqlE .= "from procuration left join electeur on procuration.mandant=electeur.id_electeur ";
    /* ++ */ $sqlE .= "where procuration.fin_validite <='".$dateElectionR."' and electeur.collectivite = '".$_SESSION['collectivite']."' ";
}

    /**
     *
     */
    // TRAITEMENT
    $sqlP = "select procuration.id_procuration, procuration.mandant, procuration.mandataire, ";
    $sqlP .= "a.nom as mandant_nom, a.prenom as mandant_prenom, a.code_bureau as mandant_bureau, ";
    $sqlP .= "b.nom as mandataire_nom, b.prenom as mandataire_prenom, b.code_bureau as mandataire_bureau, ";
    $sqlP .= "debut_validite, fin_validite ";
    $sqlP .= "from procuration, electeur as a, electeur as b ";
    $sqlP .= "where procuration.mandant = a.id_electeur and procuration.mandataire = b.id_electeur ";
    $sqlP .= "and refus != 'O'";
    $sqlP .= "and a.collectivite='".$_SESSION['collectivite']."' ";
    $sqlP .= "and b.collectivite='".$_SESSION['collectivite']."' ";
    // Gestion des dates de validite
    $sqlP .= " AND ";
    $sqlP .= " ( ";
    //
    if ($dateelection1 != NULL) {
        //
        $sqlP .= " (procuration.debut_validite <='".$dateelection1."' ";
        $sqlP .= " AND ";
        $sqlP .= " procuration.fin_validite >='".$dateelection1."') ";
    }
    //
    if ($dateelection1 != NULL && $dateelection2 != NULL) {
        //
        $sqlP .= " OR ";
    }
    //
    if ($dateelection2 != NULL) {
        //
        $sqlP .= " (procuration.debut_validite <='".$dateelection2."' ";
        $sqlP .= " AND ";
        $sqlP .= " procuration.fin_validite >='".$dateelection2."') ";
    }
    $sqlP .= " ) ";
    
    /**
     *
     */
    //
    $sqlCV = "select * from centrevote ";
    $sqlCV .= "inner join electeur on centreVote.id_electeur=electeur.id_electeur ";
    $sqlCV .= "where electeur.collectivite='".$_SESSION['collectivite']."'  ";
    // Gestion des dates de validite
    $sqlCV .= " AND ";
    $sqlCV .= " ( ";
    //
    if ($dateelection1 != NULL) {
        //
        $sqlCV .= " (centrevote.debut_validite <='".$dateelection1."' ";
        $sqlCV .= " AND ";
        $sqlCV .= " centrevote.fin_validite >='".$dateelection1."') ";
    }
    //
    if ($dateelection1 != NULL && $dateelection2 != NULL) {
        //
        $sqlCV .= " OR ";
    }
    //
    if ($dateelection2 != NULL) {
        //
        $sqlCV .= " (centrevote.debut_validite <='".$dateelection2."' ";
        $sqlCV .= " AND ";
        $sqlCV .= " centrevote.fin_validite >='".$dateelection2."') ";
    }
    $sqlCV .= " ) ";

    /**
     *
     */
    //
    $sqlME = "select * from mairieEurope ";
    $sqlME .= "inner join electeur on mairieEurope.id_electeur_europe=electeur.id_electeur ";
    $sqlME .= "where electeur.collectivite='".$_SESSION['collectivite']."' ";

?>
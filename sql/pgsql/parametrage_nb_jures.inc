<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Parametrage")." -> "._("Nombre de jures");

// Icone
$ico = "ico_parametrage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 15;

// Critere from de la requete
$table = " parametrage_nb_jures ";
$table .= " LEFT JOIN canton ON parametrage_nb_jures.canton=canton.code ";
$table .= " LEFT JOIN collectivite ON parametrage_nb_jures.collectivite=collectivite.id ";

// Critere select de la requete
$champAffiche = array(
    "parametrage_nb_jures.id as \""._("Id")."\"",
    "canton.libelle_canton as \""._("Canton")."\"",
    "parametrage_nb_jures.nb_jures as \""._("Nombre de jures")."\"",
);
if ($f->isMulti() == true) {
    array_push($champAffiche, "collectivite.ville as \""._("Collectivite")."\"");
}

// Champ sur lesquels la recherche est active
$champRecherche = array(
);

// Critere where de la requete
if ($f->isMulti() == true) {
    $selection = "";
} else {
    $selection = "WHERE parametrage_nb_jures.collectivite='".$_SESSION["collectivite"]."'";
}

// Critere order by ou group by de la requete
$tri = "order by collectivite.ville, canton.libelle_canton";

/**
 * Tableau de liens
 */
// Le but est de cacher les actions : ajouter et supprimer qui ne sont d'aucune
// utilitÃ©
$href[0] = array("lien" => "#", "id" => "", "lib" => "", );
$href[2] = array("lien" => "#", "id" => "", "lib" => "", );

?>
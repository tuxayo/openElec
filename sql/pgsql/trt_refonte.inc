<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 * Cette requete permet de compter tous les electeurs de la table electeur en
 * fonction de la collectivite en cours et de la liste en cours
 * 
 * @param string $_SESSION['collectivite']
 * @param string $_SESSION['liste']
 */
$query_count_electeur = "select count(*) ";
$query_count_electeur .= "from electeur ";
$query_count_electeur .= "where liste='".$_SESSION['liste']."' ";
$query_count_electeur .= "and collectivite='".$_SESSION['collectivite']."' ";

/**
 * Cette requete permet de lister tous les electeurs de la table electeur en
 * fonction de la collectivite en cours et de la liste en cours
 * 
 * @param string $_SESSION['collectivite']
 * @param string $_SESSION['liste']
 */
$query_select_electeur_id = "select id_electeur from electeur ";
$query_select_electeur_id .= "where liste='".$_SESSION['liste']."' ";
$query_select_electeur_id .= "and collectivite='".$_SESSION['collectivite']."' ";
$query_select_electeur_id .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";

/**
 * Cette requete permet de lister tous les bureaux en fonction de la
 * collectivite en cours
 * 
 * @param string $_SESSION['collectivite']
 */
$query_select_bureau = "select * from bureau ";
$query_select_bureau .= "where collectivite='".$_SESSION['collectivite']."' ";
$query_select_bureau .= "order by code ";

/**
 * Cette requete permet de lister tous les electeurs de la table electeur sur
 * le bureau donne en fonction de la collectivite en cours et de la liste en
 * cours
 *
 * @param string $row_bureau['code'] Code du bureau
 * @param string $_SESSION['collectivite']
 * @param string $_SESSION['liste']
 */
if (isset($row_bureau['code'])) {
    $query_select_electeur_id_bureau = "select id_electeur from electeur ";
    $query_select_electeur_id_bureau .= "where liste='".$_SESSION['liste']."' ";
    $query_select_electeur_id_bureau .= "and collectivite='".$_SESSION['collectivite']."' ";
    $query_select_electeur_id_bureau .= "and code_bureau='".$row_bureau['code']."' ";
    $query_select_electeur_id_bureau .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
}

?>
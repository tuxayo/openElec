<?php
/**
 *
 */

/**
 * Cette requete permet de lister tous les bureaux en fonction de la
 * collectivite en cours
 * 
 * @param string $_SESSION['collectivite']
 */
$query_select_bureau = "select * from bureau ";
$query_select_bureau .= "where collectivite='".$_SESSION['collectivite']."' ";
$query_select_bureau .= "order by code ";

if (isset($row_select_bureau['code']) and isset($c['champ']) and isset($c['valeur'])) {
	/**
	 * Cette requete permet de compter tous les mouvements de la date de tableau
	 * en cours en fonction de la collectivite en cours, de la liste en cours,
	 * du bureau selectionne et du champ supplementaire donne
	 *
	 * @param string $row_select_bureau['code'] Code du bureau
	 * @param string $_SESSION['collectivite']
	 * @param string $_SESSION['liste']
	 * @param string $f->collectivite['datetableau']
     * @param string $c['champ']
     * @param string $c['valeur']
     */
	$query_count_mouvement = "select count(*) ";
	$query_count_mouvement .= "from mouvement as m ";
	$query_count_mouvement .= "inner join param_mouvement as pm ";
	$query_count_mouvement .= "on m.types=pm.code ";
	$query_count_mouvement .= "where liste='".$_SESSION['liste']."' ";
	$query_count_mouvement .= "and collectivite='".$_SESSION['collectivite']."' ";
	$query_count_mouvement .= "and code_bureau='".$row_select_bureau['code']."' ";
    $query_count_mouvement .= "and date_tableau='".$f->collectivite['datetableau']."' ";
    $query_count_mouvement .= "and ".$c['champ']."='".$c['valeur']."' ";
}

?>
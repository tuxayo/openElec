<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
//
require "procuration_common_pdf.inc";

/**
 *
 */
//-------------------------- titre----------------------------------------------
$libtitre = "Procuration(s) active(s) le";
//
if ($dateelection1 != NULL && $dateelection2 != NULL) {
    $libtitre .= " ".$f->formatdate($dateelection1);
    $libtitre .= " "._("et le");
    $libtitre .= " ".$f->formatdate($dateelection2);
} else {
    if ($dateelection1 != NULL) {
        $libtitre .= " ".$f->formatdate($dateelection1);
    } else {
        $libtitre .= " ".$f->formatdate($dateelection2);
    }
}

//--------------------------SQL-------------------------------------------------
//
$sql = " SELECT ";
//
$sql .= " to_char(debut_validite,'DD/MM/YYYY')||' au '||to_char(fin_validite,'DD/MM/YYYY') as validite, ";
$sql .= " (a.nom||' '|| a.prenom) as mandant, ";
$sql .= " a.code_bureau as b1, ";
$sql .= " a.liste as liste1, ";
$sql .= " (b.nom||' '|| b.prenom) as mandataire, ";
$sql .= " b.code_bureau as b2, ";
$sql .= " b.liste as liste2 ";
//
$sql .= " FROM procuration, electeur as a, electeur as b ";
//
$sql .= " WHERE ";
$sql .= " a.collectivite='".$_SESSION['collectivite']."'";
$sql .= " AND b.collectivite='".$_SESSION['collectivite']."'";
$sql .= " AND a.liste='".$_SESSION['liste']."' ";
$sql .= " AND b.liste='".$_SESSION['liste']."' ";
$sql .= " and procuration.mandant = a.id_electeur and procuration.mandataire = b.id_electeur";
// Gestion des dates de validite
$sql .= " AND ";
$sql .= " ( ";
//
if ($dateelection1 != NULL) {
    //
    $sql .= " (procuration.debut_validite <='".$dateelection1."' ";
    $sql .= " AND ";
    $sql .= " procuration.fin_validite >='".$dateelection1."') ";
}
//
if ($dateelection1 != NULL && $dateelection2 != NULL) {
    //
    $sql .= " OR ";
}
//
if ($dateelection2 != NULL) {
    //
    $sql .= " (procuration.debut_validite <='".$dateelection2."' ";
    $sql .= " AND ";
    $sql .= " procuration.fin_validite >='".$dateelection2."') ";
}
$sql .= " ) ";
//
$sql .= " order by withoutaccent(lower(a.nom)), withoutaccent(lower(a.prenom)) ";
//------------------------------------------------------------------------------

?>
<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

// Critere from de la requete
$tableSelect = "mouvement";

// attention de ne pas changer l'ordre car repris dans
// les tableaux $val sont indicés et non associés
// repris changement adresse
// mouvement.class = changerbureau
// mouvement.class = bureauforce
// code_voie 22
// numero habitation 21
// code_bureau 5
// bureauforce 6
// Critere select de la requete    
$champs = array(
    //
    "id_electeur",                  //0
    "numero_electeur",              //31
    "liste",
    "ancien_bureau",
    "id",                          //30
    "date_modif",
    "utilisateur",
    //
    "types",
    "date_tableau",
    "code_bureau",                  //2
    "bureauforce",                  //3
    "numero_bureau",                //1
    //
    "etat",
    "tableau",
    //"envoi_cnen",
    //"date_cnen",              
    //
    "civilite",
    "sexe",
    "nom",
    "nom_usage",
    "prenom",
    "situation",
    //
    "date_naissance",
    "code_departement_naissance",
    "libelle_departement_naissance",
    "code_lieu_de_naissance",
    "libelle_lieu_de_naissance",
    "code_nationalite",
    //
    "numero_habitation",   //21
    "code_voie",           //22
    "libelle_voie",
    "complement_numero",
    "complement",
    //
    "resident",
    "adresse_resident",
    "complement_resident",
    "cp_resident",
    "ville_resident",
    //
    "provenance",
    "libelle_provenance",
    "observation"
);

// Critere where de la requete
$selection = " and etat<>'trs'";
if ($_SESSION['multi_collectivite'] == 0) {
    $selection = " and collectivite='".$_SESSION['collectivite']."'";
}
$selectionE = "";

//
$sql_type_common = "SELECT code, libelle FROM param_mouvement";
$sql_type_where_validite = " ((param_mouvement.om_validite_debut IS NULL AND (param_mouvement.om_validite_fin IS NULL OR param_mouvement.om_validite_fin > CURRENT_DATE)) OR (param_mouvement.om_validite_debut <= CURRENT_DATE AND (param_mouvement.om_validite_fin IS NULL OR param_mouvement.om_validite_fin > CURRENT_DATE))) ";
//
$sql_type_M = $sql_type_common." WHERE typecat like 'Modification' AND ".$sql_type_where_validite." order by libelle";
$sql_type_I = $sql_type_common." WHERE typeCat like 'Inscription' AND ".$sql_type_where_validite." order by libelle";
$sql_type_R = $sql_type_common." WHERE typecat like 'Radiation' AND ".$sql_type_where_validite." order by libelle";
//
$sql_type_by_id = "SELECT code, libelle FROM param_mouvement WHERE code = '<idx>'";


/**
 * Parametrages des select
 */
//
$sql_bureau = "select code,(code||' '|| libelle_bureau) as lib from bureau where collectivite = '".$_SESSION['collectivite']."' order by code";
//
$sql_nationalite = "select code, (libelle_nationalite||'('||code||')') from nationalite order by libelle_nationalite";

?>

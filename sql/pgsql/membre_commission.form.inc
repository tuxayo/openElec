<?php
/**
 * Configuration du formulaire d'ajout/modification/suppression
 * de membres
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$tableSelect = "membre_commission";

//
$champs = array(
	"membre_commission",
	"civilite",
	"nom",
	"prenom",
	"adresse",
	"complement",
	"cp",
	"ville",
	"cedex",
	"bp",
	"collectivite",
);

//
$selection = "";

/**
 * Parametrages des select
 */
//
$sql_collectivite = "select id, (ville ||' ['|| id ||']') as lib from collectivite order by ville";

?>
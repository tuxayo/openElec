<?php
/**
 *
 */

/**
 * Cette requete permet de compter tous les electeurs de la table electeur en
 * fonction de la collectivite en cours et de la liste en cours
 * 
 * @param string $_SESSION['collectivite']
 * @param string $_SESSION['liste']
 */
$query_count_electeur = "select count(*) ";
$query_count_electeur .= "from electeur ";
$query_count_electeur .= "where liste='".$_SESSION['liste']."' ";
$query_count_electeur .= "and collectivite='".$_SESSION['collectivite']."' ";


/**
 *
 */
//
$query_j5_where = "and mouvement.liste='".$_SESSION['liste']."' ";
$query_j5_where .= "and collectivite = '".$_SESSION['collectivite']."' ";
$query_j5_where .= "and mouvement.date_tableau='".$datetableau."' ";
if (isset($action) && $action == "recapitulatif") {
    //
    $query_j5_where .= "and mouvement.etat='trs' ";
    $query_j5_where .= "and mouvement.tableau='j5' ";
    $query_j5_where .= "and mouvement.date_j5='".$datej5."' ";
} else {
    //
    $query_j5_where .= "and mouvement.etat='actif' ";
    //
    if (isset($cinqjours) && $cinqjours == true || isset($additions) && count($additions) != 0) {
        //
        $query_j5_where .= "and (";
        if (isset($cinqjours) && $cinqjours == true) {
            $query_j5_where .= "param_mouvement.effet='Immediat' ";
        }
        foreach ($additions as $key => $elem) {
            if ((isset($cinqjours) && $cinqjours == true && $key == 0) || $key > 0 && $key < count($additions)) {
                $query_j5_where .= "or ";
            }
            $query_j5_where .= " mouvement.types='".$elem."' ";
        }
        $query_j5_where .= ")";
    } else {
        $query_j5_where .= "and mouvement.etat='impossible' ";
    }
}

/**
 * Ces requetes permettent l'une de lister et l'autre de compter toutes les
 * inscriptions non traitees a effet immediat a la date de tableau donnee en
 * fonction de la collectivite en cours et de la liste en cours
 *
 * @param string $datetableau 
 * @param string $_SESSION['collectivite']
 * @param string $_SESSION['liste']
 */
$query_inscription = "from mouvement inner join param_mouvement ";
$query_inscription .= "on mouvement.types=param_mouvement.code ";
$query_inscription .= "where param_mouvement.typecat='Inscription' ";
$query_inscription .= $query_j5_where;
$query_select_inscription = "select * ".$query_inscription;
$query_select_inscription .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
$query_count_inscription = "select count(*) ".$query_inscription;

/**
 * Ces requetes permettent l'une de lister et l'autre de compter toutes les
 * radiations non traitees a effet immediat a la date de tableau donnee en
 * fonction de la collectivite en cours et de la liste en cours
 *
 * @param string $datetableau 
 * @param string $_SESSION['collectivite']
 * @param string $_SESSION['liste']
 */
$query_radiation = "from mouvement inner join param_mouvement ";
$query_radiation .= "on mouvement.types=param_mouvement.code ";
$query_radiation .= "where param_mouvement.typecat='Radiation' ";
$query_radiation .= $query_j5_where;
$query_select_radiation = "select * ".$query_radiation;
$query_select_radiation .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
$query_count_radiation = "select count(*) ".$query_radiation;

/**
 * Ces requetes permettent l'une de lister et l'autre de compter toutes les
 * modifications non traitees a effet immediat a la date de tableau donnee en
 * fonction de la collectivite en cours et de la liste en cours
 *
 * @param string $datetableau 
 * @param string $_SESSION['collectivite']
 * @param string $_SESSION['liste']
 */
$query_modification = "from mouvement inner join param_mouvement ";
$query_modification .= "on mouvement.types=param_mouvement.code ";
$query_modification .= "where param_mouvement.typecat='Modification' ";
$query_modification .= $query_j5_where;
$query_select_modification = "select * ".$query_modification;
$query_select_modification .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
$query_count_modification = "select count(*) ".$query_modification;

?>
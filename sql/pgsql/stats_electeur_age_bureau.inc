<?php
/**
 *
 */

/**
 * Cette requete permet de lister tous les bureaux en fonction de la
 * collectivite en cours
 * 
 * @param string $_SESSION['collectivite']
 */
$query_select_bureau = "select * from bureau ";
$query_select_bureau .= "where collectivite='".$_SESSION['collectivite']."' ";
$query_select_bureau .= "order by code ";

if (isset($row_select_bureau['code'])) {
	/**
	 * Cette requete permet de compter tous les electeurs de la table electeur en
	 * fonction de la collectivite en cours, de la liste en cours et du bureau
	 * selectionne
	 *
	 * @param string $row_select_bureau['code'] Code du bureau
	 * @param string $_SESSION['collectivite']
	 * @param string $_SESSION['liste']
	 */
	$query_count_electeur_bureau = "select count(*) ";
	$query_count_electeur_bureau .= "from electeur ";
	$query_count_electeur_bureau .= "where liste='".$_SESSION['liste']."' ";
	$query_count_electeur_bureau .= "and collectivite='".$_SESSION['collectivite']."' ";
	$query_count_electeur_bureau .= "and code_bureau='".$row_select_bureau['code']."' ";

	if (isset($t['debut']) and isset($t['fin'])) {
		/**
		 * Cette requete permet de compter tous les electeurs de la table electeur en
		 * fonction de la collectivite en cours, de la liste en cours, du bureau
		 * selectionne et en fonction de la categorie d'age souhaitee
		 *
		 * @param string $row_select_bureau['code'] Code du bureau
		 * @param integer $t['debut'] Debut de la categorie d'age
		 * @param integer $t['fin'] Fin de la categorie d'age
		 * @param string $_SESSION['collectivite']
		 * @param string $_SESSION['liste']
		 */
		$query_count_electeur_bureau_age = $query_count_electeur_bureau;
		$query_count_electeur_bureau_age .= "and date_part('year', date_naissance)<=".(date('Y') - $t['debut'])." ";
		$query_count_electeur_bureau_age .= "and date_part('year', date_naissance)>=".(date('Y') - $t['fin'])." ";
	}
}

?>
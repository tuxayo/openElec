<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$tableSelect = "param_mouvement";

//
$champs = array(
    "code",
    "libelle",
    "typecat",
    "effet",
    "edition_carte_electeur",
    "cnen",
    "codeinscription",
    "coderadiation",
    "insee_import_radiation",
    "om_validite_debut",
    "om_validite_fin",
);

//
$selection = "";

?>
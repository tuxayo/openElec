<?php
//
$reqmo['libelle'] = "  RECHERCHE BUREAU ";
//
$reqmo['sql'] = " SELECT ";
$reqmo['sql'] .= " code as code_bureau, ";
$reqmo['sql'] .= " [libelle_bureau], ";
$reqmo['sql'] .= " [adresse1], ";
$reqmo['sql'] .= " [adresse2], ";
$reqmo['sql'] .= " [adresse3], ";
$reqmo['sql'] .= " [code_canton], ";
$reqmo['sql'] .= " [collectivite], ";
$reqmo['sql'] .= " [ville] ";
$reqmo['sql'] .= " FROM bureau ";
$reqmo['sql'] .= " LEFT JOIN collectivite ";
$reqmo['sql'] .= " on bureau.collectivite=collectivite.id ";
if (!isset($f) || !$f->isMulti()) {
    $reqmo['sql'] .= " WHERE collectivite = '".$_SESSION['collectivite']."' ";    
}
$reqmo['sql'] .= " ORDER by [TRI] ";
//
$reqmo['TRI']= array('collectivite', 'collectivite.ville', 'code_bureau', 'code_canton');
//
$reqmo['libelle_bureau']="checked";
$reqmo['adresse1']="checked";
$reqmo['adresse2']="checked";
$reqmo['adresse3']="checked";
$reqmo['code_canton']="checked";
$reqmo['collectivite']="checked";
$reqmo['ville']="checked";
//
?>
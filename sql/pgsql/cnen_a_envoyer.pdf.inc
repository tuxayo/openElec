<?php
$DEBUG=0;
// ------------------------document---------------------------------------------
$orientation="L";// orientation P-> portrait L->paysage
$format="A4";// format A3 A4 A5
$police='arial';
$margeleft=10;// marge gauche
$margetop=5;// marge haut
$margeright=5;//  marge droite
$border=1; // 1 ->  bordure 0 -> pas de bordure
$C1="0";// couleur texte  R
$C2="0";// couleur texte  V
$C3="0";// couleur texte  B
//-------------------------LIGNE tableau----------------------------------------
$size=10; //taille POLICE
$height=6; // -> hauteur ligne tableau
$align='L';
$fond=1;// 0- > FOND transparent 1 -> fond
$C1fond1="241";// couleur fond  R
$C2fond1="241";// couleur fond  V
$C3fond1="241";// couleur fond  B
$C1fond2="255";// couleur fond  R
$C2fond2="255";// couleur fond  V
$C3fond2="255";// couleur fond  B
//-------------------------- titre----------------------------------------------
$libtitre="Liste des mouvements à envoyer tableau du ".$dateTableauFr." de la liste ".$_SESSION['liste']; // libelle titre
$bordertitre=0; // 1 ->  bordure 0 -> pas de bordure
$aligntitre='L'; // L,C,R
$heightitre=10;// hauteur ligne titre
$grastitre="B";//$gras="B" -> BOLD OU $gras=""
$fondtitre=0; //0- > FOND transparent 1 -> fond
$C1titrefond="181";// couleur fond  R
$C2titrefond="182";// couleur fond  V
$C3titrefond="188";// couleur fond  B
$C1titre="75";// couleur texte  R
$C2titre="79";// couleur texte  V
$C3titre="81";// couleur texte  B
$sizetitre="15";
//--------------------------libelle entete colonnes-----------------------------
$flag_entete=1;//entete colonne : 0 -> non affichage , 1 -> affichage
$fondentete=1;// 0- > FOND transparent 1 -> fond
$heightentete=10;//hauteur ligne entete colonne
$C1fondentete="210";// couleur fond  R
$C2fondentete="216";// couleur fond  V
$C3fondentete="249";// couleur fond  B
$C1entetetxt="0";// couleur texte R
$C2entetetxt="0";// couleur texte V
$C3entetetxt="0";// couleur texte B
//------ Border entete colonne $be0  à $be.. ( $be OBLIGATOIRE )
$be0="L";
$be1="L";
$be2="L";
$be3="L";
$be4="LR";
// ------ couleur border--------------------------------------------------------
$C1border="159";// couleur texte  R
$C2border="160";// couleur texte  V
$C3border="167";// couleur texte  B
//------ Border cellule colonne $b0  à $b.. ( $b OBLIGATOIRE )
$b0="L";
$b1="L";
$b2="L";
$b3="L";
$b4="LR";
//------ ALIGNEMENT entete colonne $ae0  à $ae.. ( $ae OBLIGATOIRE )
$ae0="C";
$ae1="L";
$ae2="L";
$ae3="C";
$ae4="L";
//------ largeur de chaque colonne $l0  à $l.. ( $l OBLIGATOIRE )---------------
$l0=30;
$l1=60;
$l2=60;
$l3=30;
$l4=100;
$widthtableau=280;// -> ajouter $l0 à $lxx
$bt=1;// border 1ere  et derniere ligne  dutableau par page->0 ou 1
//------ ALIGNEMENT de chaque colonne $l0  à $a.. ( $a OBLIGATOIRE )------------
$a0="C";
$a1="L";
$a2="L";
$a3="C";
$a4="L";
//--------------------------SQL-------------------------------------------------
$sql="select mouvement.code_bureau  , mouvement.nom , mouvement.prenom , to_char(date_naissance,'DD/MM/YYYY') as Naissance";
$sql=$sql.",param_mouvement.libelle " ;
$sql=$sql." from  mouvement ";
$sql=$sql. " inner join param_mouvement ";
$sql=$sql." on  mouvement.types  = param_mouvement.code ";
$sql=$sql." where param_mouvement.cnen='Oui'";
$sql=$sql." and mouvement.date_tableau='".$dateTableau."'";
$sql=$sql." and mouvement.envoi_cnen is null";
$sql=$sql." and mouvement.liste='".$_SESSION['liste']."'";
/* ++ */ $sql=$sql." and mouvement.collectivite='".$_SESSION['collectivite']."'";
$sql=$sql." order by mouvement.code_bureau, withoutaccent(lower(mouvement.nom)), withoutaccent(lower(mouvement.prenom)) ";
//------------------------------------------------------------------------------
?>
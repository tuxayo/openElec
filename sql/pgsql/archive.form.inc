<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id $
 */

// Critere from de la requete
$tableSelect = "archive";

// Critere select de la requete
$champs = array(
    "id",
    "id_electeur",
    "numero_electeur",
    "numero_bureau",
    "types",
    "code_bureau", //5
    "bureauforce", //6
    "date_modif",
    "utilisateur",
    "civilite",
    "sexe",
    "nom",
    "nom_usage",
    "prenom",
    "situation",
    "date_naissance",
    "code_departement_naissance",
    "libelle_departement_naissance",
    "code_lieu_de_naissance",
    "libelle_lieu_de_naissance",
    "code_nationalite",
    "numero_habitation",   //21
    "code_voie",           //22
    "libelle_voie",
    "complement_numero",
    "complement",
    "provenance",
    "libelle_provenance",
    "ancien_bureau",
    "observation",
    "resident",
    "adresse_resident",
    "complement_resident",
    "cp_resident",
    "ville_resident",
    "etat",
    "liste",
    "tableau",
    "date_tableau",
    "envoi_cnen",
    "date_cnen",
    "date_mouvement",
    "mouvement"
);

// Critere where de la requete
$selection = "and collectivite = '".$_SESSION['collectivite']."' ";

?>
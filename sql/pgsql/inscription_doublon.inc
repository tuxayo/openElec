<?php
/**
 *
 */

/**
 * Cette requete permet de lister les electeurs correspondants au nom (d'une
 * maniere exacte ou non) et/ou a la date de naissance donnee en fonction de
 * la collectivite en cours et de la liste en cours
 * 
 * @param string $nom Nom patronymique
 * @param boolean $exact true ou false
 * @param string $datenaissance format DD/MM/YYYY
 * @param string $_SESSION['collectivite']
 * @param string $_SESSION['liste']
 */
$query_doublon_electeur = "select e.nom, e.prenom, e.code_bureau, ";
$query_doublon_electeur .= "to_char(e.date_naissance,'DD/MM/YYYY') as Naissance ";
$query_doublon_electeur .= "from electeur as e ";
$query_doublon_electeur .= "where ";

$nom = mb_strtolower($nom, 'UTF-8');
$nom = str_replace(
    array(
        'à', 'â', 'ä', 'á', 'ã', 'å',
        'î', 'ï', 'ì', 'í',
        'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
        'ù', 'û', 'ü', 'ú',
        'é', 'è', 'ê', 'ë',
        'ç', 'ÿ', 'ñ',
        ),
    array(
        'a', 'a', 'a', 'a', 'a', 'a',
        'i', 'i', 'i', 'i',
        'o', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'u',
        'e', 'e', 'e', 'e',
        'c', 'y', 'n',
        ),
$nom
);
if ($nom != "") {
    if (substr($nom,strlen($nom)-1,1) == '*') {
      $query_doublon_electeur .= " lower(translate(nom::".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
      .substr($nom,0,strlen($nom)-1)."%' ";
    } else {
        if ($exact == 1) {
            $query_doublon_electeur .= " lower(translate(nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ";
            $query_doublon_electeur .= " = '".$nom."' ";
            //$selection .= "nom = '".$nom."' ";
        } else {
            $query_doublon_electeur .= " lower(translate(nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ";
            $query_doublon_electeur .= " like '%".$nom."%' ";
            //$selection .= "nom like '%".$nom."%' ";
        }
    }
    $query_doublon_electeur .= "and ";
}
if ($datenaissance != "") {
    $query_doublon_electeur .= "e.date_naissance='".substr($datenaissance,6,4).'-'.substr($datenaissance,3,2).'-'.substr($datenaissance,0,2)."' ";
    $query_doublon_electeur .= "and ";
}
$query_doublon_electeur .= "e.collectivite = '".$_SESSION['collectivite']."' ";
$query_doublon_electeur .= "and e.liste = '".$_SESSION['liste']."' ";
$query_doublon_electeur .= " order by withoutaccent(lower(e.nom)), withoutaccent(lower(e.prenom)) ";

/**
 * Cette requete permet de lister les inscriptions correspondantes au nom (d'une
 * maniere exacte ou non) et/ou a la date de naissance donnee en fonction de
 * la collectivite en cours et de la liste en cours
 * 
 * @param string $nom Nom patronymique
 * @param boolean $exact true ou false
 * @param string $datenaissance format DD/MM/YYYY
 * @param string $_SESSION['collectivite']
 * @param string $_SESSION['liste']
 */
$query_doublon_mouvement = "select m.nom, m.prenom, m.code_bureau, ";
$query_doublon_mouvement .= "to_char(m.date_naissance,'DD/MM/YYYY') as Naissance ";
$query_doublon_mouvement .= "from mouvement as m ";
$query_doublon_mouvement .= "inner join param_mouvement as pm ";
$query_doublon_mouvement .= "on m.types=pm.code ";
$query_doublon_mouvement .= "where ";
if ($nom != "") {
    if (substr($nom,strlen($nom)-1,1) == '*') {
      $query_doublon_mouvement .= " lower(translate(m.nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
      .substr($nom,0,strlen($nom)-1)."%' ";
    } else {
        if ($exact == 1) {
            $query_doublon_mouvement .= " lower(translate(m.nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ";
            $query_doublon_mouvement .= " = '".$nom."' ";
            //$query_doublon_mouvement .= "m.nom = '".$nom."' ";
        } else {
            $query_doublon_mouvement .= " lower(translate(m.nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ";
            $query_doublon_mouvement .= " like '%".$nom."%' ";
            //$query_doublon_mouvement .= "m.nom like '%".$nom."%' ";
        }
    }
    $query_doublon_mouvement .= "and ";
}
if ($datenaissance != "") {
    $query_doublon_mouvement .= "m.date_naissance='".substr($datenaissance,6,4).'-'.substr($datenaissance,3,2).'-'.substr($datenaissance,0,2)."' ";
    $query_doublon_mouvement .= "and ";
}
$query_doublon_mouvement .= "m.etat='actif' ";
$query_doublon_mouvement .= "and pm.typecat='Inscription' ";
$query_doublon_mouvement .= "and m.collectivite = '".$_SESSION['collectivite']."' ";
$query_doublon_mouvement .= "and m.liste = '".$_SESSION['liste']."' ";
$query_doublon_mouvement .= " order by withoutaccent(lower(m.nom)), withoutaccent(lower(m.prenom)) ";
?>
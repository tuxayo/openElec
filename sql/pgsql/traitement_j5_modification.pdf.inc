<?php
/**
 * 
 */

//
require "traitement_common_pdf.inc";

//
if ($action == "recapitulatif") {
    //
    $libtitre = "Modification(s) appliquée(s) lors du traitement J-5 du ".$f->formatdate($datej5)." [Tableau du ".$f->formatdate($datetableau)."]";
} else {
    //
    $libtitre = "Modification(s) à appliquer au traitement J-5 du ".date("d/m/Y")." [Tableau du ".$f->formatdate($f->collectivite["datetableau"])."]";
}

//------ Border entete colonne $be0  à $be.. ( $be OBLIGATOIRE )
$be4=$be3;
$be5=$be4."R";
//------ Border cellule colonne $b0  à $b.. ( $b OBLIGATOIRE )
$b4=$b3;
$b5=$b4."R";
//------ largeur de chaque colonne $l0  à $l.. ( $l OBLIGATOIRE )---------------
$l5=$l4;
$l4=$l3;
$l3=$l2;
$l2=$l1;
$l1=$l0/2;
$l0=$l0/2;
//------ ALIGNEMENT de chaque colonne $l0  à $a.. ( $a OBLIGATOIRE )------------
$a5=$a4;
$a4=$a3;
$a3=$a2;
$a2=$a1;
$a1=$a0;
$a0=$a0;

//--------------------------SQL-------------------------------------------------
$sql = " SELECT ";
$sql .= " mouvement.code_bureau as \"Bureau\", ";
$sql .= " CASE ";
    $sql .= " WHEN mouvement.code_bureau <> mouvement.ancien_bureau THEN mouvement.ancien_bureau ";
    $sql .= " ELSE '-' ";
$sql .= " END AS \"Ancien\", ";
$sql .= " mouvement.nom, ";
$sql .= " mouvement.prenom as \"Prenom(s)\", ";
$sql .= " to_char(mouvement.date_naissance, 'DD/MM/YYYY') as \"Naissance\", ";
$sql .= " param_mouvement.libelle as \"Motif\" ";
$sql .= $query_modification;
$sql .= " ORDER BY mouvement.code_bureau, withoutaccent(lower(mouvement.nom)), withoutaccent(lower(mouvement.prenom)) ";
//------------------------------------------------------------------------------

?>
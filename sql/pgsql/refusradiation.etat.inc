<?php
//20/08/2013   11:52:25
$etat['orientation']="P";
$etat['format']="A4";
$etat['footerfont']="helvetica";
$etat['footerattribut']="I";
$etat['footertaille']="8";
$etat['logo']="logopdf.png";
$etat['logoleft']="70";
$etat['logotop']="27";
$etat['titre']="REFUS DE RADIATION";
$etat['titreleft']="60";
$etat['titretop']="56";
$etat['titrelargeur']="130";
$etat['titrehauteur']="15";
$etat['titrefont']="arial";
$etat['titreattribut']="B";
$etat['titretaille']="20";
$etat['titrebordure']="0";
$etat['titrealign']="L";
$etat['corps']="[nomprenom]
[nom_usage]
Né(e) le [naissance]
à [lieu]

domicilié(e) [adresse]
             [complement]
             [voiecp]  [voieville]



Est informé(e) que la radiation de la liste électorale  bureau n° : [code_bureau] a été refusée.



À £ville
Le £aujourdhui


£nom";
$etat['corpsleft']="23";
$etat['corpstop']="100";
$etat['corpslargeur']="165";
$etat['corpshauteur']="5";
$etat['corpsfont']="times";
$etat['corpsattribut']="";
$etat['corpstaille']="10";
$etat['corpsbordure']="0";
$etat['corpsalign']="J";
$etat['sql']="SELECT (code_bureau||' - pour le motif suivant : '||param_mouvement.libelle) as code_bureau, (civilite||' '||nom||' '||prenom) as nomprenom, 
nom_usage,
to_char(date_naissance,'DD/MM/YYYY') as naissance, (libelle_lieu_de_naissance||' ('||code_departement_naissance||')' ) as lieu,
 case  when numero_habitation =0  then trim(complement_numero||' '||voie.libelle_voie)
    else trim(numero_habitation||' '||complement_numero||' '||voie.libelle_voie) end as adresse,
complement, voie.cp as voiecp, 
voie.ville as voieville 
FROM ((mouvement LEFT JOIN voie ON mouvement.code_voie = voie.code) LEFT JOIN bureau ON mouvement.code_bureau = bureau.code) LEFT JOIN param_mouvement ON mouvement.types=param_mouvement.code
WHERE bureau.collectivite = '£collectivite' and £idx";
$etat['sousetat']=array();
$etat['se_font']="helvetica";
$etat['se_margeleft']="8";
$etat['se_margetop']="5";
$etat['se_margeright']="5";
$etat['se_couleurtexte']=array("0","0","0");
?>

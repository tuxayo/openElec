<?php
/**
 *
 */

/**
 * Cette requete permet de lister tous les bureaux avec une jointure sur les
 * cantons en fonction de la collectivite en cours
 * 
 * @param string $_SESSION['collectivite']
 */
$query_select_bureau_canton = "select b.code, libelle_bureau, code_canton, ";
$query_select_bureau_canton .= "libelle_canton from bureau as b ";
$query_select_bureau_canton .= "inner join canton as c on b.code_canton=c.code ";
$query_select_bureau_canton .= "where collectivite='".$_SESSION['collectivite']."' ";
$query_select_bureau_canton .= "order by b.code ";

if (isset($nobureau)) {
    /**
     * Cette requete permet de lister tous les electeurs de la table electeur
     * qui ont un enregistrement dans la table centrevote en fonction de la
     * collectivite en cours, de la liste en cours et du bureau selectionne
     *
     * @param string $nobureau Code du bureau
     * @param string $_SESSION['collectivite']
     * @param string $_SESSION['liste']
     */
    $query_select_centrevote = "select ";
    $query_select_centrevote .= "a.code_bureau, a.numero_bureau, ";
    $query_select_centrevote .= "(a.nom||'   '||a.prenom||'  '||a.sexe) as nom, ";
    $query_select_centrevote .= "to_char(c.fin_validite, 'DD/MM/YYYY') as datefin ";
    $query_select_centrevote .= "from electeur as a inner join centrevote as c ";
    $query_select_centrevote .= "on a.id_electeur=c.id_electeur ";
    $query_select_centrevote .= "where a.liste='".$_SESSION ['liste']."' ";
    $query_select_centrevote .= "and a.collectivite='".$_SESSION['collectivite']."' ";
    $query_select_centrevote .= "and a.code_bureau='".$nobureau."' ";
    $query_select_centrevote .= " order by withoutaccent(lower(a.nom)), withoutaccent(lower(a.prenom)) ";
}

?>
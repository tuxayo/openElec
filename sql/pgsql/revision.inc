<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Parametrage")." -> "._("Revisions");

// Icone
$ico = "ico_parametrage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 15;

// Critere from de la requete
$table = " revision ";

// Critere select de la requete
$champAffiche = array(
    "revision.id as \""._("Id")."\"",
    "revision.libelle as \""._("Revision")."\"",
    "to_char(revision.date_debut ,'DD/MM/YYYY') as \""._("Date de debut")."\"",
    "to_char(revision.date_effet ,'DD/MM/YYYY') as \""._("Date d'effet")."\"",
    "to_char(revision.date_tr1 ,'DD/MM/YYYY') as \""._("Date TR1")."\"",
    "to_char(revision.date_tr2 ,'DD/MM/YYYY') as \""._("Date TR2")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
);

//
$selection = "";

// Critere order by ou group by de la requete
$tri = "order by revision.date_effet DESC";

/**
 * Tableau de liens
 */
// Le but est de cacher les actions : ajouter et supprimer qui ne sont d'aucune
// utilitÃ©
// $href[0] = array("lien" => "#", "id" => "", "lib" => "", );
// $href[2] = array("lien" => "#", "id" => "", "lib" => "", );

?>
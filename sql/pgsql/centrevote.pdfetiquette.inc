<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
//
$filename_more = "-liste".$_SESSION['liste'];

/**
 *
 */
//******************************************************************************
//                  DIFFERENTES ZONES A AFFICHER                              //
//******************************************************************************
//------------------------------------------------------------------------------
//                  COMPTEUR                                                  //
//------------------------------------------------------------------------------
//(0) 1 -> affichage compteur ou 0 ->pas d'affichage
// (1) x  (2) y  (3) width (4) bold 1 ou 0  (5) size ou 0
$champs_compteur=array();
//------------------------------------------------------------------------------
//                  IMAGE                                                     //
//------------------------------------------------------------------------------
//  (0) nom image (1) x  (2) y  (3) width (4) hauteue  (5) type
// $img=array(array('../img/arles.png',1,1,17.6,12.6,'png')
$img=array();
//------------------------------------------------------------------------------
//                  TEXTE                                                     //
//------------------------------------------------------------------------------
// (0) texte (1) x  (2) y  (3) width (4) bold 1 ou 0  (5) size ou 0
$texte=array();
//------------------------------------------------------------------------------
//                  DATA                                                      //
//------------------------------------------------------------------------------
// (0) affichage avant data
// (1) affichage apres data
// (2) tableau X Y Width bold(0 ou 1),size ou 0
// (3) 1 = number_format(champs,0) : 0002->2  /  ou 0
$champs = array(
    'ligne1'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 0),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne2'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 1),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne3'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 2),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne4'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 3),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne5'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 4),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne6'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 5),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0)
);

//******************************************************************************
//                        SQL                                                 //
//******************************************************************************
$sql = " SELECT ";
//
$sql .= " '' AS ligne1, ";
//
$sql .= " CASE ";
    $sql .= " WHEN (electeur.sexe = 'F' and electeur.situation = 'M' and electeur.nom_usage<>'')";
        $sql .= " THEN (upper(electeur.civilite) ||' '||electeur.nom||' EP '||electeur.nom_usage||' '||electeur.prenom )";
    $sql .= " WHEN ((electeur.situation <> 'M' or electeur.situation is NULL) and electeur.nom_usage <> '')";
        $sql .= " THEN (upper(electeur.civilite) ||' '||electeur.nom||' - '||electeur.nom_usage||' '||electeur.prenom ) ";
    $sql .= " ELSE (upper(electeur.civilite) ||' '||electeur.nom||' '||electeur.prenom) ";
$sql .= " END AS ligne2, ";
//
$sql .= " '' AS ligne3, ";
//
$sql .= " CASE resident ";
    $sql .= " WHEN 'Non' THEN (";
        $sql .= " CASE numero_habitation ";
            $sql .= " WHEN 0 THEN '' ";
            $sql .= " ELSE (numero_habitation||' ') ";
        $sql .= " END ";
        $sql .= " || ";
        $sql .= " CASE complement_numero ";
            $sql .= " WHEN '' THEN '' ";
            $sql .= " ELSE (complement_numero||' ') ";
        $sql .= " END ";
    $sql .= " ||electeur.libelle_voie) ";
    $sql .= " WHEN 'Oui' THEN adresse_resident ";
$sql .= " END AS ligne4, ";
//
$sql .= " CASE resident ";
    $sql .= " WHEN 'Non' THEN electeur.complement ";
    $sql .= " WHEN 'Oui' THEN complement_resident ";
$sql .= " END AS ligne5, ";
//
$sql .= " CASE resident ";
    $sql .= " WHEN 'Non' THEN (voie.cp||' '||voie.ville) ";
    $sql .= " WHEN 'Oui' THEN (cp_resident||' '||ville_resident) ";
$sql .= " END AS ligne6 ";
//
$sql .= " FROM ";
$sql .= " voie RIGHT JOIN electeur ";
$sql .= " ON voie.code = electeur.code_voie ";
$sql .= " INNER JOIN centrevote ";
$sql .= " ON electeur.id_electeur=centrevote.id_electeur ";
//
$sql .= " WHERE ";
$sql .= " electeur.liste='".$_SESSION['liste']."' ";
$sql .= " AND electeur.collectivite='".$_SESSION['collectivite']."' ";
//
if (isset ($f -> collectivite ['dateelection']) && $f -> collectivite ['dateelection'] != '') {
    $sql .= " and debut_validite<='".$f -> collectivite ['dateelection']."' ";
    $sql .= " and fin_validite>'".$f -> collectivite ['dateelection']."' ";
} else {
    $sql .= " and debut_validite<='".date('Y-m-d')."' ";
    $sql .= " and fin_validite>'".date('Y-m-d')."' ";
}
//
$sql .= " ORDER BY code_bureau, withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
//

?>

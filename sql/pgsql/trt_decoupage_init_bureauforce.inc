<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

$sql_decoupage_init_bureauforce_electeur = "
UPDATE electeur
SET bureauforce = 'Non'
WHERE id_electeur in (
    SELECT id_electeur from electeur
    JOIN decoupage ON decoupage.code_voie = electeur.code_voie
        AND CASE numero_habitation % 2
            WHEN 0
            THEN decoupage.premier_pair <= numero_habitation AND numero_habitation <= decoupage.dernier_pair
            WHEN 1
            THEN decoupage.premier_impair <= numero_habitation AND numero_habitation <= decoupage.dernier_impair
            END
    WHERE
    -- Filtrage de la collectivite
    electeur.collectivite = '".$_SESSION["collectivite"]."' AND
    electeur.liste='".$_SESSION["liste"]."' AND
    electeur.code_bureau = decoupage.code_bureau
)
AND electeur.bureauforce = 'Oui';
";

$sql_decoupage_init_bureauforce_mouvement = "
UPDATE mouvement
SET bureauforce = 'Non'
WHERE id in (
    SELECT mouvement.id from mouvement
    JOIN decoupage ON decoupage.code_voie = mouvement.code_voie
        AND CASE numero_habitation % 2
            WHEN 0
            THEN decoupage.premier_pair <= numero_habitation AND numero_habitation <= decoupage.dernier_pair
            WHEN 1
            THEN decoupage.premier_impair <= numero_habitation AND numero_habitation <= decoupage.dernier_impair
            END
    WHERE
    -- Filtrage de la collectivite
    mouvement.collectivite = '".$_SESSION["collectivite"]."' AND
    mouvement.liste='".$_SESSION["liste"]."' AND
    mouvement.code_bureau = decoupage.code_bureau
)
AND mouvement.bureauforce = 'Oui'
AND mouvement.etat = 'actif';
";

?>
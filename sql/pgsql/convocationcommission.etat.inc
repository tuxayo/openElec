<?php
//03/09/2013   17:41:28
$etat['orientation']="P";
$etat['format']="A4";
$etat['footerfont']="helvetica";
$etat['footerattribut']="I";
$etat['footertaille']="10";
$etat['logo']="logopdf.png";
$etat['logoleft']="10";
$etat['logotop']="10";
$etat['titre']="                                                                                                [civnomprenom]
                                                                                                [adresse]
                                                                                                [complement] [bp]
                                                                                                [cp]  [ville]
                                                                                                [cedex]";
$etat['titreleft']="14";
$etat['titretop']="30";
$etat['titrelargeur']="170";
$etat['titrehauteur']="5";
$etat['titrefont']="arial";
$etat['titreattribut']="";
$etat['titretaille']="10";
$etat['titrebordure']="0";
$etat['titrealign']="L";
$etat['corps']="[civilite],

Nous avons l'honneur de vous informer que la commission administrative chargée de la révision des listes électorales dont vous faites partie se réunira :

£complement

Votre présence est indispensable; toutefois, si vous ne pouvez pas être présent, merci de nous en avertir au plus tôt.

Nous vous prions d'agréer, [civilite], l'assurance, de nos sentiments les meilleurs.



                                                                                                À £ville
                                                                                                Le £aujourdhui";
$etat['corpsleft']="14";
$etat['corpstop']="66";
$etat['corpslargeur']="170";
$etat['corpshauteur']="5";
$etat['corpsfont']="times";
$etat['corpsattribut']="";
$etat['corpstaille']="10";
$etat['corpsbordure']="0";
$etat['corpsalign']="J";
$etat['sql']="SELECT
CASE WHEN membre_commission.civilite = 'M.' THEN 'Monsieur'
        WHEN membre_commission.civilite = 'Mme' THEN 'Madame'
        WHEN membre_commission.civilite = 'Mlle' THEN 'Mademoiselle'
END as civilite,
membre_commission.civilite||' '||membre_commission.nom||' '||membre_commission.prenom as civnomprenom,
adresse as adresse,
membre_commission.complement as complement,
membre_commission.bp as bp,
membre_commission.cp as cp,
membre_commission.ville as ville,
membre_commission.cedex as cedex
FROM membre_commission
WHERE membre_commission = £idx";
$etat['sousetat']=array();
$etat['se_font']="helvetica";
$etat['se_margeleft']="8";
$etat['se_margetop']="5";
$etat['se_margeright']="5";
$etat['se_couleurtexte']=array("0","0","0");
?>

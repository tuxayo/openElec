<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$tableSelect = "utilisateur";

//
$champs = array(
    "idutilisateur",
    "nom",
    "login",
    "email",
    "pwd",
    "profil",
    "listedefaut",
    "collectivite",
);

//
$selection = "";

/**
 * Parametrages des select
 */
//
$sql_profil = "select profil, (profil ||' '|| libelle_profil) as lib from profil order by profil";
//
$sql_liste = "select liste, (liste ||' '|| libelle_liste) as lib from liste order by liste";
//
$sql_collectivite = "select id, (ville ||' ['|| id ||']') as lib from collectivite order by ville";

?>
<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Nombre d'enregistrements par page
$serie = 20;

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Titre
if (isset($maj) && $maj == 0) {
    $ent = _("Saisie")." -> "._("Nouvelle inscription");
} else {
    $ent = _("Consultation")." -> "._("Inscription");
}


// Icone
$ico = "ico_mouvementi.png";

// Critere from de la requete
$table = "(param_mouvement right JOIN mouvement ON param_mouvement.code = mouvement.types)";

// attention l ordre est important dans mouvement.tab.class.php
// id 0
// etat 6
// Critere select de la requete
$champAffiche = array(
    "id as \""._("Id")."\"",
    "nom as \""._("Nom")."\"",
    "prenom as \""._("Prenom")."\"",
    "nom_usage as \""._("Nom d'usage")."\"",
    "(to_char(date_naissance,'DD/MM/YYYY')||' "._("a")." '||libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')') as \""._("Date et lieu de naissance")."\"",
    "libelle as \""._("Mouvement")."\"",
    "to_char(date_tableau,'DD/MM/YYYY') as \""._("Tableau du")."\"",
    "etat as \""._("Etat")."\"",
    "to_char(date_j5,'DD/MM/YYYY') as \""._("Traite le")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "nom as \""._("Nom")."\"",
    "prenom as \""._("Prenom")."\"",
    "nom_usage as \""._("Nom d'usage")."\"",
    "libelle",
    "date_naissance as \""._("Date de naissance")."\"",
);

// Critere where de la requete
if ($f->isMulti() == true) {
    //
    $selection = " where param_mouvement.typecat like 'Inscription' and liste = '".$_SESSION['liste']."'";
    //
    $href = array(
        0 => array("lien" => "", "id" => "", "lib" => ""),
        1 => array("lien" => "", "id" => "", "lib" => ""),
        2 => array("lien" => "", "id" => "", "lib" => ""),
        3 => array("lien" => "", "id" => "", "lib" => ""),
    );
} else {
    //
    $selection = " where param_mouvement.typecat like 'Inscription' and liste = '".$_SESSION['liste']."' and collectivite = '".$_SESSION['collectivite']."' ";
    //
    $href [0]['lien']= "";
    $href [0]['id']= "";
    $href [0]['lib']= "";
    $href [3]['lien'] = "../pdf/pdfetat.php?obj=attestationinscription&amp;idx=";
    $href [3]['id']= "&amp;id=mouvement";
    $href [3]['lib']= "<span class=\"om-icon om-icon-16 om-icon-fix pdf-16\" title=\""._("Attestation d'inscription")."\">"._("Attestation d'inscription")."</span>";
    $href[3]['target'] = "_blank";
    // Courrier de refus
    if(file_exists("../dyn/var.inc")) {
        include "../dyn/var.inc";
    }
    if(isset($option_refus_mouvement) AND $option_refus_mouvement === true) {

        $href [4]['lien'] = "../pdf/pdfetat.php?obj=refusinscription&amp;idx=";

        $href [4]['id']= "&amp;id=mouvement";
        $href [4]['lib']= "<span class=\"om-icon om-icon-16 om-icon-fix courrierrefus-16\" title=\""._("Refus d'inscription")."\">"._("Refus d'inscription")."</span>";
        $href[4]['target'] = "_blank";
    }
}

// Critere order by ou group by de la requete
$tri = "order by etat, date_tableau, withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";

/**
 * Options
 */
//
$options = array();
//
$option = array(
    "type" => "condition",
    "field" => "etat",
    "case" => array(
        "0" => array(
            "values" => array("trs", ),
            "style" => "etat-trs",
            "href" => array(
                0 => array("lien" => "", "id" => "", "lib" => ""),
                1 => array("lien" => "", "id" => "", "lib" => ""),
                2 => array("lien" => "", "id" => "", "lib" => ""),
                3 => array("lien" => "", "id" => "", "lib" => ""),
            ),
        ),
    ),
);
array_push($options, $option);

?>

<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

$ent = "Electeur";

// Nombre d'enregistrements par page
$serie = 12;

// Objet d'une eventuelle edition .pdf.inc
$edition = "";


// Critere from de la requete
$table = "electeur";

//
$formulaire = $table.".form";

// Critere select de la requete
$champAffiche = array(
   "id_electeur as \""._("Id")."\"",
   "liste as \""._("Liste")."\"",
   //"nom",
   //"prenom",
   "('<b>'|| nom || '</b><br /> ' || prenom || '<br /><b>'|| nom_usage||'&nbsp;</b>') as \""._("Nom et prenom")."\"",
   "(to_char(date_naissance,'DD/MM/YYYY')||' <br />a '||libelle_lieu_de_naissance ) as \""._("Date et lieu de naissance")."\"",
   "(numero_habitation||' '||complement_numero||' '||libelle_voie) as \""._("Adresse")."\"",
   "code_bureau as \""._("Bureau")."\"",
   "mouvement as \""._("Mouvement")."\"",
   "typecat as \""._("Categorie")."\""
);

// non utilisé
$champRecherche = array(
   "id_electeur",
   "nom",
   "prenom"
);

// Critere where de la requete
$selection = " where collectivite = '".$_SESSION['collectivite']."' ";
$selection .= " and liste = '".$_SESSION['liste']."' ";

if (isset($nom) and $nom != "") {
   if (substr($nom,strlen($nom)-1,1) == '*') {
      $selection .= " and lower(translate(nom::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY')) like '%"
      .strtolower(strtr(strtolower(substr($nom,0,strlen($nom)-1)),'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'))
      ."%' ";
   } else {
      if( isset($exact) and $exact != true ) {
         $selection .= " and lower(translate(nom::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY')) like '%"
         .strtolower(strtr(strtolower($nom),'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'))
         ."%' ";
      } else {
         $selection .= " and lower(translate(nom::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY')) ='"
         .strtolower(strtr(strtolower($nom),'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'))
         ."' ";
      }
   }
}
//
if (isset($prenom) and $prenom != ""){
   if (substr($prenom,strlen($prenom)-1,1) == '*') {
      $selection .= " and lower(translate(prenom::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY')) like '"
      .strtolower(strtr(strtolower(substr($prenom,0,strlen($prenom)-1)),'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'))
      ."%' ";
   } else {
      if( isset($exact) and $exact != true ) {
         $selection .= " and lower(translate(prenom::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY')) like '%"
         .strtolower(strtr(strtolower($prenom),'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'))
         ."%' ";
      } else {
         $selection .= " and lower(translate(prenom::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY')) ='"
         .strtolower(strtr(strtolower($prenom),'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'))
         ."' ";
      }
   }
}

//
if (isset($marital) and $marital != "") {
   if (substr($marital,strlen($marital)-1,1) == '*') {
      $selection .= " and lower(translate(nom_usage::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY')) like '"
      .strtolower(strtr(strtolower(substr($marital,0,strlen($marital)-1)),'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'))
      ."%' ";
   } else {
      if( isset($exact) and $exact != true ) {
         $selection .= " and lower(translate(nom_usage::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY')) like '%"
         .strtolower(strtr(strtolower($marital),'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'))
         ."%' ";
      } else {
         $selection .= " and lower(translate(nom_usage::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY')) ='"
         .strtolower(strtr(strtolower($marital),'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'))
         ."' ";
      }
   }
}

if (isset($datenaissance) and $datenaissance != "") {
   $selection .= " and date_naissance ='".$datenaissance."' ";
}

// Critere order by ou group by de la requete
$tri = " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";



/**
 * Tableau de liens
 */
$href = array(
    0 => array("lien" => "", "id" => "", "lib" => "", ),
    1 => array("lien" => "", "id" => "", "lib" => "", ),
    2 => array(
        "lien" => "../scr/form.php?origin=electeur&amp;obj=electeur&amp;idx=",
        "id" => "&amp;nom=".(isset($nom) ? $nom : "").
                "&amp;exact=".(isset($exact) ? $exact : "").
                "&amp;prenom=".(isset($prenom) ? $prenom : "").
                "&amp;premier=".(isset($premier) ? $premier : "").
                "&amp;datenaissance=".(isset($datenaissance) ? $datenaissance : "").
                "&amp;marital=".(isset($marital) ? $marital : "").
                "&amp;tricol=".(isset($tricol) ? $tricol : "").
                "&amp;premier=".(isset($premier) ? $premier : 0),
        "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix modification-16\" title=\""._("Modification de l'electeur")."\">"._("Modification de l'electeur")."</span>",
    ),
    3 => array("lien" => "", "id" => "", "lib" => "", ),
);



//
$formulaire = $table.".form";

/**
 * Options
 */
//
$options = array();
$option = array(
    "type" => "search",
    "display" => false,
);
array_push($options, $option);
//
$option = array(
    "type" => "condition",
    "field" => "typecat",
    "case" => array(
        "0" => array(
            "values" => array("modification", ),
            "style" => "modification-encours",
        ),
        "1" => array(
            "values" => array("radiation", ),
            "style" => "radiation-encours",
        ),
    ),
);
array_push($options, $option);

?>
<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

// Critere from de la requete
$tableSelect=" centrevote as c, electeur as a ";

// Critere select de la requete
$champs = array(
    "c.idcentrevote",
    "to_char(c.date_modif,'DD/MM/YYYY') as date_modif",
    "c.utilisateur",
    "c.id_electeur",
    // IMPORTANT : La concaténation est vérifiée dans la classe
    "(a.nom||' - '||a.prenom||' - '||to_char(date_naissance,'DD/MM/YYYY')||' - '||a.code_bureau) as z_electeur",
    "c.debut_validite",
    "c.fin_validite"
);

// Critere where de la requete
$selection=" and c.id_electeur = a.id_electeur and a.collectivite = '".$_SESSION['collectivite']."' ";

?>
<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Decoupage")." -> "._("Bureau");

// Icone
$ico = "ico_decoupage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "bureau";

// Nombre d'enregistrements par page
$serie = 20;

// Critere from de la requete
$table = "bureau left join canton ";
$table .= "on bureau.code_canton = canton.code ";
$table .= "left join collectivite ";
$table .= "on bureau.collectivite=collectivite.id ";

// Critere select de la requete
$champAffiche = array(
    "bureau.code as \""._("Code")."\"",
    "bureau.libelle_bureau as \""._("Libelle")."\"",
    "(bureau.adresse1||'<br />'||bureau.adresse2) as \""._("Adresse")."\"",
    "canton.libelle_canton as \""._("Canton")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "bureau.code as \""._("Code")."\"",
    "bureau.libelle_bureau as \""._("Libelle")."\"",
    "(bureau.adresse1||'<br />'||bureau.adresse2) as \""._("Adresse")."\"",
    "*\""._("Code")."\"",
    "*\""._("Libelle")."\"",
    "*\""._("Adresse")."\"",
);

// Critere where de la requete
$selection = "where bureau.collectivite='".$_SESSION['collectivite']."'";

// Critere order by ou group by de la requete
$tri = "order by bureau.collectivite, bureau.code";

/**
 * MULTI
 */
if ($f->isMulti() == true) {
    // Critere select de la requete
    array_push($champAffiche, "collectivite.ville as \""._("Collectivite")."\"");
    
    // Champ sur lesquels la recherche est active
    array_push($champRecherche, "collectivite.ville as \""._("Collectivite")."\"");
    
    // Critere where de la requete
    $selection = "";
}

?>
<?php
/**
 * 
 * 
 * @package openelec
 * @version SVN : $Id$
 */

/**
 * 
 */
//
$sqlbur = " SELECT ";
$sqlbur .= " (bureau.code) AS burcod, ";
$sqlbur .= " (bureau.libelle_bureau) AS libbur, ";
$sqlbur .= " (canton.libelle_canton) AS libcanton ";
$sqlbur .= " FROM bureau LEFT JOIN canton ON bureau.code_canton = canton.code ";
//
$sqlbur .= " WHERE ";
$sqlbur .= " bureau.code='".$nobureau."' ";
$sqlbur .= " AND bureau.collectivite = '".$_SESSION['collectivite']."' ";

/**
 * 
 */
//
$sql = " SELECT ";
$sql .= " (nom || ' - ' || prenom) AS nomprenom,";
$sql .= " nom_usage, ";
$sql .= " to_char(date_naissance,'DD/MM/YYYY') AS naissance, ";
$sql .= " (substring(libelle_lieu_de_naissance from 0 for 29) || ' (' || code_departement_naissance || ')') AS lieu, ";

if ($_SESSION["liste"] == "01") {
    //
    $sql .= " CASE ";
        // Les deux mentions (CENTRE VOTE & MAIRIE EUROPE) sont a inscrire
        // alors on affiche la premiere ligne de la mention CENTRE VOTE
        $sql .= " WHEN position('***| ***' in procuration) <> 0 THEN ";
        $sql .= " substring(procuration from 55 for 68) ";
        // La mention CENTRE VOTE seule est a inscrire
        // alors on affiche la premiere ligne de la mention CENTRE VOTE
        $sql .= " WHEN position('***/' in procuration) <> 0 THEN ";
        $sql .= " substring(procuration from 1 for 68) ";
        //
        $sql .= " ELSE ";
        $sql .= " '' ";
    //
    $sql .= " END AS colonne1_ligne4,";
    //
    $sql .= " CASE ";
        // Les deux mentions (CENTRE VOTE & MAIRIE EUROPE) sont a inscrire
        // alors on affiche la deuxieme ligne de la mention CENTRE VOTE
        $sql .= " WHEN position('***| ***' in procuration) <> 0 THEN ";
        $sql .= " substring(procuration from 123 for 54) ";
        // La mention CENTRE VOTE seule est a inscrire
        // alors on affiche la deuxieme ligne de la mention CENTRE VOTE
        $sql .= " WHEN position('***/' in procuration) <> 0 THEN ";
        $sql .= " substring(procuration from 69 for 54) ";
        // La mention MAIRIE EUROPE seule est a inscrire
        // alors on affiche la ligne de la mention MAIRIE EUROPE
        $sql .= " WHEN position('***|' in procuration) <> 0 THEN ";
        $sql .= " substring(procuration from 1 for 52) ";
        //
        $sql .= " ELSE ";
        $sql .= " '' ";
    //
    $sql .= " END AS colonne1_ligne5,";
} else {
    $sql .= " libelle_nationalite AS colonne1_ligne4,";
    $sql .= " '' AS colonne1_ligne5,";
}
//
$sql .= " CASE resident ";
    $sql .= " WHEN 'Non' THEN (";
    $sql .= " CASE numero_habitation > 0 ";
            $sql .= " WHEN True THEN (numero_habitation || ' ' || complement_numero || ' ' || libelle_voie) ";
            $sql .= " WHEN False THEN libelle_voie ";
    $sql .= " END) ";
    $sql .= " WHEN 'Oui' THEN adresse_resident ";
$sql .= " END AS adresse, ";
$sql .= " CASE resident ";
    $sql .= " WHEN 'Non' THEN complement ";
    $sql .= " WHEN 'Oui' THEN (complement_resident || ' ' || cp_resident || ' - ' || ville_resident) ";
$sql .= " END AS complement, ";
//
$sql .= " CASE ";
    // Les deux mentions (CENTRE VOTE & MAIRIE EUROPE) sont a inscrire
    // alors on affiche la ligne de la mention MAIRIE EUROPE
    $sql .= " WHEN position('***| ***' in procuration) <> 0 THEN ";
    $sql .= " substring(procuration from 1 for 52) ";
    // La mention CENTRE VOTE seule est a inscrire
    // alors on affiche la premiere ligne des PROCURATIONS
    $sql .= " WHEN position('***/' in procuration) <> 0 THEN ";
    $sql .= " substring(procuration from 125 for 65) ";
    // La mention MAIRIE EUROPE seule est a inscrire
    // alors on affiche la premiere ligne des PROCURATIONS
    $sql .= " WHEN position('***|' in procuration) <> 0 THEN ";
    $sql .= " substring(procuration from 55 for 65) ";
    // SINON on affiche la premiere ligne des PROCURATIONS
    $sql .= " ELSE ";
    $sql .= " substring(procuration from 1 for 65) ";
//
$sql .= " END AS colonne2_ligne3, ";
//
$sql .= " CASE ";
    // Les deux mentions (CENTRE VOTE & MAIRIE EUROPE) sont a inscrire
    // alors on affiche la premiere ligne des PROCURATIONS
    $sql .= " WHEN position('***| ***' in procuration) <> 0 THEN ";
    $sql .= " substring(procuration from 179 for 65) ";
    // La mention CENTRE VOTE seule est a inscrire
    // alors on affiche la premiere ligne des PROCURATIONS
    $sql .= " WHEN position('***/' in procuration) <> 0 THEN ";
    $sql .= " substring(procuration from 193 for 65) ";
    // La mention MAIRIE EUROPE seule est a inscrire
    // alors on affiche la premiere ligne des PROCURATIONS
    $sql .= " WHEN position('***|' in procuration) <> 0 THEN ";
    $sql .= " substring(procuration from 120 for 65) ";
    // SINON on affiche la deuxieme ligne des PROCURATIONS
    $sql .= " ELSE ";
    $sql .= " substring(procuration from 66 for 65) ";
//
$sql .= " END AS colonne2_ligne4, ";
//
$sql .= " CASE ";
    // Les deux mentions (CENTRE VOTE & MAIRIE EUROPE) sont a inscrire
    // alors on affiche la deuxieme ligne des PROCURATIONS
    $sql .= " WHEN position('***| ***' in procuration) <> 0 THEN ";
    $sql .= " substring(procuration from 244 for 65) ";
    // La mention CENTRE VOTE seule est a inscrire
    // alors on affiche la premiere ligne des PROCURATIONS
    $sql .= " WHEN position('***/' in procuration) <> 0 THEN ";
    $sql .= " substring(procuration from 193 for 65) ";
    // La mention MAIRIE EUROPE seule est a inscrire
    // alors on affiche la deuxiÃšme ligne des PROCURATIONS
    $sql .= " WHEN position('***|' in procuration) <> 0 THEN ";
    $sql .= " substring(procuration from 185 for 65) ";
    // SINON on affiche la troixieme ligne des PROCURATIONS
    $sql .= " ELSE ";
    $sql .= " substring(procuration from 131 for 65) ";
//
$sql .= " END AS colonne2_ligne5, ";
// OPTION - Double Ã©margement
// Quel est l'interet d'avoir l'id_electeur dans la liste d'Ã©margement lors du double Ã©margement
// alors que cet identifiant n'apparait pas ?
if (isset($double_emargement) && $double_emargement == true) {
    $sql .= " numero_bureau, ";
    $sql .= " ' ' as tour2, ";
    $sql .= " ' ' as tour1, ";
    $sql .= " ' ' as emargement ";
} else {
    $sql .= " numero_bureau, ";
    $sql .= " ' ' as emargement ";
}
//
$sql .= " FROM electeur ";
$sql .= " LEFT JOIN nationalite ";
$sql .= " on electeur.code_nationalite=nationalite.code ";
//
$sql .= " WHERE ";
$sql .= " liste='".$_SESSION['liste']."' ";
$sql .= " AND electeur.collectivite = '".$_SESSION['collectivite']."' ";
//
if ($id == "bureau") {
    $sql .= " AND code_bureau='".$nobureau."' ";
}

// OPTION - La liste d'Ã©margement peut Ãªtre dressÃ©e par ordre des numÃ©ros
// d'inscription ou par ordre alphabÃ©tique des Ã©lecteurs, au choix de la mairie
// $option_tri_liste_emargement = "alpha";
// $option_tri_liste_emargement = "numero";
// (default : $option_tri_liste_emargement = "alpha";)
if (isset($option_tri_liste_emargement) && $option_tri_liste_emargement == "numero") {
    $sql .= " ORDER BY numero_bureau";
} else {
    $sql .= " ORDER BY withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
}

/**
 * 
 */
//
$sqlproc = " SELECT ";
$sqlproc .= " count(*) ";
$sqlproc .= " FROM electeur ";
//
$sqlproc .= " WHERE ";
$sqlproc .= " liste='".$_SESSION['liste']."' ";
$sqlproc .= " AND electeur.collectivite='".$_SESSION['collectivite']."' ";
$sqlproc .= " AND procuration like '%***%' ";
//
if ($id == "bureau") {
    $sqlproc .= " AND code_bureau='".$nobureau."'";
}

?>

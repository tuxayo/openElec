<?php
/**
 * $Id$
 *
 */
    $sqlbur="SELECT ";
    $sqlbur=$sqlbur." (bureau.code) AS burcod,";
    $sqlbur=$sqlbur." (bureau.libelle_bureau) as libbur,";
    $sqlbur=$sqlbur." (canton.libelle_canton) as libcanton";
    $sqlbur=$sqlbur." FROM bureau LEFT JOIN canton ON bureau.code_canton = canton.code";
    $sqlbur=$sqlbur." WHERE bureau.code='".$nobureau."'";
    $sqlbur=$sqlbur." AND bureau.collectivite = '".$_SESSION['collectivite']."'";
    //
    $sql="SELECT ";
    // MANDAT
    $sql=$sql."  a.nom as nom,";
    $sql=$sql."  a.prenom as prenom,";
    $sql=$sql."  a.nom_usage as nom_usage,";
    $sql=$sql."  to_char(a.date_naissance,'DD/MM/YYYY') as naissance,";
    $sql=$sql."  (a.libelle_lieu_de_naissance||' ('||a.code_departement_naissance||')' ) as lieu,";
    $sql=$sql."  case a.resident when 'Non' then (a.numero_habitation||' '||a.complement_numero||' '||a.libelle_voie) when 'Oui' then a.adresse_resident end as adresse,";
    $sql=$sql."  case a.resident when 'Non' then a.complement when 'Oui' then (a.complement_resident||' ' ||a.cp_resident||' - '||a.ville_resident) end as complement,";
    $sql=$sql."  a.numero_electeur,a.numero_bureau,";
    // MANDATAIRE
    $sql=$sql."  b.nom as nomm,";
    $sql=$sql."  b.prenom as prenomm,";
    $sql=$sql."  b.nom_usage as nom_usagem,";
    $sql=$sql."  to_char(b.date_naissance,'DD/MM/YYYY') as naissancem,";
    $sql=$sql."  (b.libelle_lieu_de_naissance||' ('||b.code_departement_naissance||')' ) as lieum,";
    $sql=$sql."  case b.resident when 'Non' then (b.numero_habitation||' '||b.complement_numero||' '||b.libelle_voie) when 'Oui' then b.adresse_resident end as adressem,";
    $sql=$sql."  case b.resident when 'Non' then b.complement when 'Oui' then (b.complement_resident||' ' ||b.cp_resident||' - '||b.ville_resident) end as complementm,";
    $sql=$sql."  b.numero_electeur as numelec,";
    $sql=$sql."  b.code_bureau as codeburm,";
    $sql=$sql."  b.numero_bureau as numburm,";
    $sql=$sql." to_char(procuration.debut_validite,'DD/MM/YYYY') as debutvalidm, ";
    $sql=$sql." to_char(procuration.fin_validite,'DD/MM/YYYY') as finvalidm ";
    //
    $sql=$sql."  FROM procuration, electeur as a, electeur as b ";
    $sql=$sql."  WHERE a.liste='".$_SESSION['liste']."'";
    $sql=$sql."  AND procuration.refus != 'O'";
    $sql=$sql."  AND a.collectivite = '".$_SESSION['collectivite']."'";
    $sql=$sql."  AND b.collectivite = '".$_SESSION['collectivite']."'";
    $sql=$sql."  and procuration.mandant = a.id_electeur and procuration.mandataire = b.id_electeur";

    if (isset ($f -> collectivite ['dateelection']) && $f -> collectivite ['dateelection'] != '')
    {
         $sql .= " and (procuration.debut_validite<='".$f -> collectivite ['dateelection']."' and ";
         $sql .= " procuration.fin_validite>='".$f -> collectivite ['dateelection']."') ";
    }
    //
    if($id=="bureau")
    $sql=$sql."  AND a.code_bureau='".$nobureau."'";

    //
    $sql=$sql." order by withoutaccent(lower(a.nom)), withoutaccent(lower(a.prenom)) ";

?>
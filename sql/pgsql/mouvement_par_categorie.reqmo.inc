<?php
$reqmo['libelle']="EXPORT DES MOUVEMENTS PAR CATEGORIE DE MOUVEMENT";
$reqmo['sql']="select numero_bureau as no_dans_bureau,numero_electeur,liste, types as type_mouvt ,[date_modif],[civilite], [nom], [prenom],
                      [nom_usage],
                      [sexe],
                      [situation],
                      [to_char(date_naissance,'DD/MM/YYYY') as date_de_naissance],
                      [code_departement_naissance],
                      [libelle_departement_naissance],
                      [code_lieu_de_naissance],
                      [libelle_lieu_de_naissance],
                      [numero_habitation],
                      [libelle_voie],
                      [complement],
                      [provenance],
                      [libelle_provenance],
                      [ancien_bureau],
                      [observation],
                      [tableau],
                      [to_char(date_j5,'DD/MM/YYYY') as date_j5],
                      [to_char(date_tableau,'DD/MM/YYYY') as date_du_tableau],
                      [envoi_cnen],
                      [date_cnen]
                       from mouvement inner join param_mouvement on mouvement.types = param_mouvement.code
               where typecat = '[CHOIX_TYPECAT]' and liste='".$_SESSION['liste']."' and mouvement.collectivite = '".$_SESSION['collectivite']."' order by [TRI]";
$reqmo['TRI']= array('nom', 'nom_usage', 'prenom', 'date_modif');
//
$reqmo['civilite']="checked";
$reqmo['nom']="checked";
$reqmo['prenom']="checked";
$reqmo['nom_usage']="checked";
$reqmo['sexe']="checked";
$reqmo['situation']="checked";
$reqmo['date_modif']="checked";
$reqmo['date_de_naissance']="checked";
$reqmo['code_departement_naissance']="checked";
$reqmo['libelle_departement_naissance']="checked";
$reqmo['code_lieu_de_naissance']="checked";
$reqmo['libelle_lieu_de_naissance']="checked";
$reqmo['numero_habitation']="checked";
$reqmo['libelle_voie']="checked";
$reqmo['complement']="checked";
$reqmo['provenance']="checked";
$reqmo['libelle_provenance']="checked";
$reqmo['ancien_bureau']="checked";
$reqmo['observation']="checked";
$reqmo['tableau']="checked";
$reqmo['date_j5']="checked";
$reqmo['date_du_tableau']="checked";
$reqmo['envoi_cnen']="checked";
$reqmo['date_cnen']="checked";
//
$reqmo['CHOIX_TYPECAT']="select typecat,(typecat) from param_mouvement group by typecat order by typecat";
?>
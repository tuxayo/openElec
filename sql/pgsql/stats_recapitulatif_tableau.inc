<?php
/**
 * $Id$
 *
 */

//
/* ++ */ $sql_bureau = "select * from bureau where collectivite = '".$_SESSION['collectivite']."' order by code";

if (isset ($row ['code']))
{
	//
	$sqlB = "select count(code_bureau) from electeur where liste ='".$_SESSION['liste']."' and  code_bureau ='".$row['code']."'";
    $sqlB .= " and collectivite = '".$_SESSION['collectivite']."'"; 

	// ins j5
	$sqlIj5 = "select count(id) from mouvement inner join param_mouvement on  mouvement.types=param_mouvement.code ";
	$sqlIj5 .= " where param_mouvement.typecat='Inscription' and mouvement.liste='".$_SESSION['liste']."'";
	$sqlIj5 .= " and mouvement.date_tableau='".$f -> collectivite ['datetableau']."' and mouvement.etat='trs'";
	$sqlIj5 .= " and code_bureau = '".$row['code']."' and collectivite = '".$_SESSION['collectivite']."'";

	//  rad j5
	$sqlRj5 = "select count(id) from mouvement inner join param_mouvement on mouvement.types=param_mouvement.code ";
	$sqlRj5 .= " where param_mouvement.typecat='Radiation' and mouvement.liste='".$_SESSION['liste']."'";
	$sqlRj5 .= " and mouvement.date_tableau='".$f -> collectivite ['datetableau']."' and mouvement.etat='trs'";
	$sqlRj5 .= " and code_bureau = '".$row['code']."' and collectivite = '".$_SESSION['collectivite']."'";

	// ***
	$sqlI = "select count(id) from mouvement inner join param_mouvement on mouvement.types=param_mouvement.code ";
	$sqlI .= " where param_mouvement.typecat='Inscription' and mouvement.liste='".$_SESSION['liste']."'";
	$sqlI .= " and mouvement.date_tableau='".$f -> collectivite ['datetableau']."' and mouvement.etat='actif'";
	$sqlI .= " and code_bureau = '".$row['code']."' and collectivite = '".$_SESSION['collectivite']."'";

	//
	$sqlM1 = "select count(id) from mouvement inner join param_mouvement on  mouvement.types=param_mouvement.code ";
	$sqlM1 .= " where param_mouvement.typecat='Modification' and mouvement.liste='".$_SESSION['liste']."'";
	$sqlM1 .= " and mouvement.date_tableau='".$f -> collectivite ['datetableau']."' and code_bureau <> ancien_bureau ";
	$sqlM1 .= " and mouvement.etat='actif' and code_bureau='".$row['code']."' and collectivite = '".$_SESSION['collectivite']."'";

	//
	$sqlM2 = "select count(id) from mouvement inner join param_mouvement on mouvement.types=param_mouvement.code ";
	$sqlM2 .= " where param_mouvement.typecat='Modification' and mouvement.liste='".$_SESSION['liste']."'";
	$sqlM2 .= " and mouvement.date_tableau='".$f -> collectivite ['datetableau']."' and code_bureau <> ancien_bureau ";
	$sqlM2 .= " and mouvement.etat='actif' and  ancien_bureau = '".$row['code']."' and collectivite = '".$_SESSION['collectivite']."'";

	//
	$sqlR = "select count(id) from mouvement inner join param_mouvement on  mouvement.types=param_mouvement.code ";
	$sqlR .= " where param_mouvement.typecat='Radiation' and mouvement.liste='".$_SESSION['liste']."'";
	$sqlR .= " and mouvement.date_tableau='".$f -> collectivite ['datetableau']."' and mouvement.etat='actif'";
	$sqlR .= " and code_bureau='".$row['code']."' and collectivite = '".$_SESSION['collectivite']."'";
}

//
$query_electeurs_actuels_groupby_bureau = " SELECT electeur.code_bureau as bureau, electeur.sexe as sexe, count(electeur.id_electeur) as total ";
$query_electeurs_actuels_groupby_bureau .= " FROM electeur ";
$query_electeurs_actuels_groupby_bureau .= " WHERE electeur.collectivite='".$_SESSION['collectivite']."' ";
$query_electeurs_actuels_groupby_bureau .= " AND electeur.liste='".$_SESSION['liste']."' ";
$query_electeurs_actuels_groupby_bureau .= " GROUP BY electeur.code_bureau, electeur.sexe";

//
$query_inscriptions_radiations_stats_groupby_bureau = " SELECT mouvement.code_bureau as bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe as sexe, count(*) as total";
$query_inscriptions_radiations_stats_groupby_bureau .= " FROM mouvement INNER JOIN param_mouvement ";
$query_inscriptions_radiations_stats_groupby_bureau .= " ON mouvement.types=param_mouvement.code ";
$query_inscriptions_radiations_stats_groupby_bureau .= " WHERE mouvement.collectivite='".$_SESSION['collectivite']."' ";
$query_inscriptions_radiations_stats_groupby_bureau .= " AND mouvement.liste='".$_SESSION['liste']."' ";
$query_inscriptions_radiations_stats_groupby_bureau .= " AND (param_mouvement.typecat='Inscription' OR param_mouvement.typecat='Radiation') ";
$query_inscriptions_radiations_stats_groupby_bureau .= " GROUP BY mouvement.code_bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe ";
$query_inscriptions_radiations_stats_groupby_bureau .= " ORDER BY mouvement.code_bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe ";

//
$query_transferts_plus_stats_groupby_bureau = " SELECT mouvement.code_bureau as bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe as sexe, count(*) as total";
$query_transferts_plus_stats_groupby_bureau .= " FROM mouvement INNER JOIN param_mouvement ";
$query_transferts_plus_stats_groupby_bureau .= " ON mouvement.types=param_mouvement.code ";
$query_transferts_plus_stats_groupby_bureau .= " WHERE mouvement.collectivite='".$_SESSION['collectivite']."' ";
$query_transferts_plus_stats_groupby_bureau .= " AND mouvement.liste='".$_SESSION['liste']."' ";
$query_transferts_plus_stats_groupby_bureau .= " AND mouvement.code_bureau <> mouvement.ancien_bureau ";
$query_transferts_plus_stats_groupby_bureau .= " AND param_mouvement.typecat='Modification' ";
$query_transferts_plus_stats_groupby_bureau .= " AND mouvement.ancien_bureau<>'' ";
$query_transferts_plus_stats_groupby_bureau .= " GROUP BY mouvement.code_bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe ";

//
$query_transferts_moins_stats_groupby_bureau = " SELECT mouvement.ancien_bureau as bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe as sexe, count(*) as total";
$query_transferts_moins_stats_groupby_bureau .= " FROM mouvement INNER JOIN param_mouvement ";
$query_transferts_moins_stats_groupby_bureau .= " ON mouvement.types=param_mouvement.code ";
$query_transferts_moins_stats_groupby_bureau .= " WHERE mouvement.collectivite='".$_SESSION['collectivite']."' ";
$query_transferts_moins_stats_groupby_bureau .= " AND mouvement.liste='".$_SESSION['liste']."' ";
$query_transferts_moins_stats_groupby_bureau .= " AND mouvement.code_bureau <> mouvement.ancien_bureau ";
$query_transferts_moins_stats_groupby_bureau .= " AND param_mouvement.typecat='Modification' ";
$query_transferts_moins_stats_groupby_bureau .= " AND mouvement.ancien_bureau<>'' ";
$query_transferts_moins_stats_groupby_bureau .= " GROUP BY mouvement.ancien_bureau, mouvement.date_tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe ";


//
$query_inscriptions_radiations_groupby_bureau = " SELECT mouvement.code_bureau as bureau, param_mouvement.typecat, mouvement.sexe as sexe, count(mouvement.id) as total";
$query_inscriptions_radiations_groupby_bureau .= " FROM mouvement INNER JOIN param_mouvement ";
$query_inscriptions_radiations_groupby_bureau .= " ON mouvement.types=param_mouvement.code ";
$query_inscriptions_radiations_groupby_bureau .= " WHERE mouvement.collectivite='".$_SESSION['collectivite']."' ";
$query_inscriptions_radiations_groupby_bureau .= " AND mouvement.liste='".$_SESSION['liste']."' ";
$query_inscriptions_radiations_groupby_bureau .= " AND mouvement.date_tableau='".$datetableau."' ";
$query_inscriptions_radiations_groupby_bureau .= " AND (param_mouvement.typecat='Inscription' OR param_mouvement.typecat='Radiation') ";
$query_inscriptions_radiations_groupby_bureau .= " GROUP BY mouvement.code_bureau, param_mouvement.typecat, mouvement.sexe ";

//
$query_transferts_plus_groupby_bureau = " SELECT mouvement.code_bureau as bureau, mouvement.sexe as sexe, count(mouvement.id) as total";
$query_transferts_plus_groupby_bureau .= " FROM mouvement INNER JOIN param_mouvement ";
$query_transferts_plus_groupby_bureau .= " ON mouvement.types=param_mouvement.code ";
$query_transferts_plus_groupby_bureau .= " WHERE mouvement.collectivite='".$_SESSION['collectivite']."' ";
$query_transferts_plus_groupby_bureau .= " AND mouvement.liste='".$_SESSION['liste']."' ";
$query_transferts_plus_groupby_bureau .= " AND mouvement.date_tableau='".$datetableau."' ";
$query_transferts_plus_groupby_bureau .= " AND mouvement.code_bureau <> mouvement.ancien_bureau ";
$query_transferts_plus_groupby_bureau .= " AND param_mouvement.typecat='Modification' ";
$query_transferts_plus_groupby_bureau .= " AND mouvement.ancien_bureau<>'' ";
$query_transferts_plus_groupby_bureau .= " GROUP BY mouvement.code_bureau, mouvement.sexe ";

//
$query_transferts_moins_groupby_bureau = " SELECT mouvement.ancien_bureau as bureau, mouvement.sexe as sexe, count(mouvement.id) as total";
$query_transferts_moins_groupby_bureau .= " FROM mouvement INNER JOIN param_mouvement ";
$query_transferts_moins_groupby_bureau .= " ON mouvement.types=param_mouvement.code ";
$query_transferts_moins_groupby_bureau .= " WHERE mouvement.collectivite='".$_SESSION['collectivite']."' ";
$query_transferts_moins_groupby_bureau .= " AND mouvement.liste='".$_SESSION['liste']."' ";
$query_transferts_moins_groupby_bureau .= " AND mouvement.date_tableau='".$datetableau."' ";
$query_transferts_moins_groupby_bureau .= " AND mouvement.code_bureau <> mouvement.ancien_bureau ";
$query_transferts_moins_groupby_bureau .= " AND mouvement.ancien_bureau<>'' ";
$query_transferts_moins_groupby_bureau .= " AND param_mouvement.typecat='Modification' ";
$query_transferts_moins_groupby_bureau .= " GROUP BY mouvement.ancien_bureau, mouvement.sexe ";

?>

<?php
$reqmo['libelle']=" LISTE ELECTORALE ";
$reqmo['sql']="select numero_bureau as no_dans_bureau,
                      numero_electeur as no_electeur,
                      [electeur.civilite as electeur_civilite],
                      [nom],
                      [prenom],
                      [nom_usage],
                      [sexe],
                      [situation],
                      [to_char(date_naissance,'DD/MM/YYYY') as date_de_naissance],
                      [code_departement_naissance],
                      [libelle_departement_naissance],
                      [code_lieu_de_naissance],
                      [libelle_lieu_de_naissance],
                      [code_nationalite],
                      [libelle_nationalite],
                      [numero_habitation],
                      [complement_numero],
                      [libelle_voie],
                      [complement],
                      [resident],
                      [adresse_resident],
                      [complement_resident],
                      [cp_resident],
                      [ville_resident],
                      [provenance],
                      [code_bureau],
                      [libelle_provenance],
                      [carte],
                      [jury],
		      [ville], 
                      [collectivite],
                      liste 
                      ";
//
$reqmo['sql'] .= " FROM electeur ";
$reqmo['sql'] .= " LEFT JOIN collectivite ";
$reqmo['sql'] .= " ON electeur.collectivite=collectivite.id ";
$reqmo['sql'] .= " LEFT JOIN nationalite ";
$reqmo['sql'] .= " ON electeur.code_nationalite=nationalite.code ";
//
$reqmo['sql'] .= " where liste='".$_SESSION['liste']."' ";
if (!isset($f) || !$f->isMulti()) {
    $reqmo['sql'] .= " and collectivite = '".$_SESSION['collectivite']."' ";
}
//
$reqmo['sql'] .= " order by [TRI]";
//
$reqmo['TRI']= array('nom',
                     'nom_usage',
                     'prenom',
                     'date_naissance',
                     'collectivite', 'ville');
//
$reqmo['electeur_civilite']="checked";
$reqmo['nom']="checked";
$reqmo['prenom']="checked";
$reqmo['nom_usage']="checked";
$reqmo['sexe']="checked";
$reqmo['situation']="checked";
$reqmo['date_de_naissance']="checked";
$reqmo['code_departement_naissance']="checked";
$reqmo['libelle_departement_naissance']="checked";
$reqmo['code_lieu_de_naissance']="checked";
$reqmo['libelle_lieu_de_naissance']="checked";
$reqmo['numero_habitation']="checked";
$reqmo['libelle_voie']="checked";
$reqmo['complement']="checked";
$reqmo['provenance']="checked";
$reqmo['libelle_provenance']="checked";
$reqmo['code_bureau']="checked";
$reqmo['carte']="checked";
$reqmo['jury']="checked";
$reqmo['collectivite']="checked";
$reqmo['ville']="checked";
$reqmo['resident']="checked";
$reqmo['adresse_resident']="checked";
$reqmo['complement_resident']="checked";
$reqmo['cp_resident']="checked";
$reqmo['ville_resident']="checked";
$reqmo['complement_numero']="checked";
$reqmo['code_nationalite']="checked";
$reqmo['libelle_nationalite']="checked";
//
?>

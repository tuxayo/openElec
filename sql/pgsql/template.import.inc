<?php
/**
 * Fichier de parametrage template pour l'import de donnees via un fichier CSV
 * (voir scr/import.php)
 *
 * @package openelec
 * @version SVN : $Id$
 */

// Si la transaction doit etre commitee ou non
// (default) Aucune valeur par defaut
//  - 0 => Pas de commit de la transaction
//  - 1 => Commit de la transaction
$verrou = 1;

// Si un fichier d'erreur doit etre retourne ou non
// (default) Aucune valeur par defaut
//  - 0 => Pas de fichier d'erreur
//  - 1 => Fichier d'erreur
$fic_rejet = 1;

// Si le fichier contient une ligne d'entete ou non
// (default) Aucune valeur par defaut
//  - 0 => Pas de ligne d'entete
//  - 1 => Ligne d'entete
$ligne1 = 1;

// Table sur laquelle va etre realise l'insert
// (default) Aucune valeur par defaut
//  - "<NOM_DE_LA_TABLE>"
$table = "table";

// Cle primaire de la table sur laquelle va etre realise l'insert
// (default) Aucune valeur par defaut
//  - ""
//  - "<NOM_DE_LA_CLE_PRIMAIRE>"
$id = "id";

// Toutes les colonnes a importer
// ATTENTION : l'ordre correspond a l'ordre des colonnes a mettre dans le
// fichier CSV
// (default) Aucune valeur par defaut
//  - $zone['<NOM_DU_CHAMP>'] = ""; => 
//  - $zone['<NOM_DU_CHAMP>'] = "1"; => 
$zone = array();

//
// (default) $obligatoire = array();

//
// (default) $defaut = array();

//
// (default) $exist = array();

// INUTILISE
//$corr = array();

// INUTILISE
//$format = array();

?>

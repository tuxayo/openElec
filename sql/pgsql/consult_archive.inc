<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */


//
$DEBUG = 0;

// Nombre d'enregistrements par page
$serie = 20;

// Titre
$ent = _("Consultation")." -> "._("Archive");

// Icone
$ico = "ico_archive.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere from de la requete
$table = "archive inner join param_mouvement on archive.types=param_mouvement.code, collectivite c";

// Critere select de la requete
/*
$champAffiche = array(
    "('<b>'||nom||'</b><br />'||prenom||'<br /><b>'||nom_usage||'&nbsp;</b>') as \""._("Nom<br /> Prénom(s)<br /> Usage")."\"",
    "(to_char(date_naissance,'DD/MM/YYYY')||'<br />'||libelle_lieu_de_naissance||' '||code_departement_naissance) as \""._("Date et lieu de naissance")."\"",
    "(code_bureau||'<br />'||numero_bureau||'<br /><b>'||id_electeur||'</b>') as \""._("bureau<br /> n° bureau<br /> id")."\"",
    "(numero_habitation||complement_numero||' '||libelle_voie||'<br />'||complement) as \""._("adresse")."\"",
    "(mouvement||' '||types||' '||param_mouvement.typecat||'<br />'||param_mouvement.libelle) as \""._("mouvement")."\"",
    "(tableau||' du '||to_char(date_tableau,'DD/MM/YYYY')) as \""._("tableau")."\"",
    "('N°: '||envoi_cnen||' * '||to_char(date_cnen,'DD/MM/YYYY')) as \""._("transfert insee")."\""
);*/


if ($f->isMulti() == true) {
    // Critere select de la requete
    $champAffiche = array (
        "archive.id as \""._("Id")."\"",
        "archive.id_electeur as \""._("Id Electeur")."\"",
        "nom as \""._("Nom")."\"",
        "prenom as \""._("Prenom")."\"",
        "nom_usage as \""._("Nom d'usage")."\"",
        "to_char(date_naissance,'DD/MM/YYYY') as \""._("Date de naissance")."\"",
        "libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')' as \""._("Lieu de naissance")."\"",
        "case resident
                when 'Non' then (
            case numero_habitation > 0
                when True then (numero_habitation || ' ' || complement_numero || ' ' || archive.libelle_voie)
                when False then archive.libelle_voie
            end)
                when 'Oui' then adresse_resident
            end || '<br />' ||case resident
                when 'Non' then archive.complement
                when 'Oui' then (complement_resident || ' ' || cp_resident || ' - ' || ville_resident)
            end as \""._("Adresse")."\"",
        "c.ville as \""._("Ville")."\""
    );
    
} else {

    // Critere select de la requete
    $champAffiche = array (
        "archive.id as \""._("Id")."\"",
        "archive.id_electeur as \""._("Id Electeur")."\"",
        "nom as \""._("Nom")."\"",
        "prenom as \""._("Prenom")."\"",
        "nom_usage as \""._("Nom d'usage")."\"",
        "to_char(date_naissance,'DD/MM/YYYY') as \""._("Date de naissance")."\"",
        "libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')' as \""._("Lieu de naissance")."\"",
        "case resident
                when 'Non' then (
            case numero_habitation > 0
                when True then (numero_habitation || ' ' || complement_numero || ' ' || archive.libelle_voie)
                when False then archive.libelle_voie
            end)
                when 'Oui' then adresse_resident
            end || '<br />' ||case resident
                when 'Non' then archive.complement
                when 'Oui' then (complement_resident || ' ' || cp_resident || ' - ' || ville_resident)
            end as \""._("Adresse")."\"",
        "archive.code_bureau as \""._("Bureau")."\"",
        "(param_mouvement.typecat||' ['||param_mouvement.libelle||' ('||types||')]<br/>"._("valide lors du traitement")." '||archive.tableau||'<br/>"._("a la date de tableau du")." '||to_char(date_tableau,'DD/MM/YYYY')) as \""._("Archive")."\""
    );

}

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "archive.id_electeur as \""._("Id Electeur")."\"",
    "nom as \""._("Nom")."\"",
    "prenom as \""._("Prenom")."\"",
    "nom_usage as \""._("Nom d'usage")."\"",
    "date_naissance as \""._("Date de naissance")."\"",
    /*"*bureau<br /> n° bureau<br /> id",
    "*nom<br /> prénom(s)<br /> usage"*/
);

// Critere where de la requete
if ($f->isMulti() == true) {
    $selection = "where archive.collectivite = c.id";

} else {
    $selection = "where archive.collectivite = c.id and archive.collectivite = '".$_SESSION['collectivite']."' ";
}

// Critere order by ou group by de la requete
$tri = " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)), date_tableau, date_mouvement";

//
$href [0]['lien'] = "";
$href [0]['id'] = "";
$href [0]['lib'] = "";
$href [1]['lien'] = "";
$href [1]['id'] = "";
$href [1]['lib'] = "";
$href [2]['lien'] = "";
$href [2]['id'] = "";
$href [2]['lib'] = "";

?>

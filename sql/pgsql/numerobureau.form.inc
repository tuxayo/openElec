<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$tableSelect = "numerobureau";

//
$champs = array(
    "id",
    "bureau",
    "liste",
    "dernier_numero",
    "dernier_numero_provisoire",
);

//
$selection = "";

/**
 * Parametrages des select
 */
//
$sql_liste = "select liste, (liste||' '|| libelle_liste) as lib from liste order by liste";
//
$sql_bureau = "select code, (code||' '|| libelle_bureau) as lib from bureau where collectivite='".$_SESSION['collectivite']."' order by code";

?>
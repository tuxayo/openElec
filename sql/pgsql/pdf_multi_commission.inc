<?php
/**
 * $Id$
 *
 */
//
// Liste les bureaux en associant leur canton
$sql_bureau_a = "select bureau.code as code, libelle_bureau, code_canton, libelle_canton from bureau inner join canton on bureau.code_canton=canton.code where bureau.collectivite = '".$_SESSION['collectivite']."' order by bureau.code";

// Requête générale compteur des mouvements
$nbsql = "select count(*) as nbr from mouvement as m left join param_mouvement as pm on m.types=pm.code where liste='".$_SESSION['liste']."' and m.collectivite = '".$_SESSION['collectivite']."' and date_tableau ='".$f -> collectivite ['datetableau']."'";

// Requête générale des mouvements
$sql = "select nom, prenom, nom_usage,";
$sql .= " to_char(date_naissance,'DD/MM/YYYY') as naissance,";
$sql .= " (libelle_lieu_de_naissance||' ('||code_departement_naissance||')' ) as lieu,";
$sql .= " (numero_habitation||' '||complement_numero||' '||libelle_voie) as adresse,";
$sql .= " complement, numero_bureau, (pm.libelle) as libmvt,";
$sql .= " ' ' as ancien_bureau ";
$sql .= " from (mouvement as m left join param_mouvement as pm on m.types=pm.code) left join bureau on m.code_bureau=bureau.code";
$sql .= " where bureau.collectivite='".$_SESSION['collectivite']."' and liste='".$_SESSION['liste']."' and m.collectivite = '".$_SESSION['collectivite']."' and date_tableau ='".$f -> collectivite ['datetableau']."'";

//
$sans_modification = true;

if (isset ($b ['code'])) {
    // Clause where particulière (Inscription ou Radiation)
    $nbins = $nbsql." and ((typecat='Modification' and ancien_bureau<>'".$b ['code']."') or (typecat='Inscription')) and code_bureau='".$b ['code']."'";
    $nbrad = $nbsql." and ((typecat='Modification' and ancien_bureau='".$b ['code']."' and code_bureau<>'".$b ['code']."') or (typecat='Radiation' and code_bureau='".$b ['code']."'))";

    // Clause where particulière (Inscription ou Radiation)
    $sql_Inscription = $sql." and ((typecat='Modification' and ancien_bureau<>'".$b ['code']."') or (typecat='Inscription')) and code_bureau='".$b ['code']."' ";
    $sql_Inscription .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
    $sql_Radiation = $sql." and ((typecat='Modification' and ancien_bureau='".$b ['code']."' and code_bureau<>'".$b ['code']."') or (typecat='Radiation' and code_bureau='".$b ['code']."')) ";
    $sql_Radiation .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";

    $nbrad_actif = $nbrad." and etat='actif'";
    $nbins_actif = $nbins." and etat='actif'";

    $nbelec = "select count(*) ";
    $nbelec .= "from electeur ";
    $nbelec .= "where code_bureau='".$b ['code']."' and liste='".$_SESSION['liste']."' and collectivite = '".$_SESSION['collectivite']."'";
    
    // Requete de recuperation du nombre de radiations de tous les mouvements
    // traites a une date superieure ou egale a la date de tableau en cours
    $nb_radiation_dtsup = "select count(*) as nbr ";
    $nb_radiation_dtsup .= "from mouvement as m inner join param_mouvement as pm ";
    $nb_radiation_dtsup .= "on m.types=pm.code ";
    $nb_radiation_dtsup .= "where date_tableau>='".$f->collectivite ['datetableau']."' ";
    $nb_radiation_dtsup .= " and liste='".$_SESSION['liste']."'";
    $nb_radiation_dtsup .= " and m.collectivite = '".$_SESSION['collectivite']."' ";
    $nb_radiation_dtsup .= "and ((pm.typecat='Radiation' and code_bureau='".$b ['code']."') ";
    // Avec une option pour la prise en compte ou non des modifications
    if ($sans_modification == true) {
        $nb_radiation_dtsup .= " or (typecat='Modification' and ";
        $nb_radiation_dtsup .= " ancien_bureau='".$b ['code']."' ";
        $nb_radiation_dtsup .= " and ancien_bureau != code_bureau)) ";
    } else {
        $nb_radiation_dtsup .= " or (pm.typecat='Modification' ";
        $nb_radiation_dtsup .= " and ancien_bureau='".$b ['code']."')) ";
    }
    $nb_radiation_dtsup .= " and m.etat='trs'";
    
    // Requete de recuperation du nombre d'additions de tous les mouvements
    // traites a une date superieure ou egale a la date de tableau en cours
    $nb_addition_dtsup = "select count(*) as nbr ";
    $nb_addition_dtsup .= "from mouvement as m inner join param_mouvement as pm ";
    $nb_addition_dtsup .= "on m.types=pm.code ";
    $nb_addition_dtsup .= "where date_tableau>='".$f->collectivite ['datetableau']."' ";
    $nb_addition_dtsup .= "and code_bureau='".$b ['code']."' and ";
    $nb_addition_dtsup .= "m.collectivite = '".$_SESSION['collectivite']."' ";
    $nb_addition_dtsup .= " and liste='".$_SESSION['liste']."'";
    // Avec une option pour la prise en compte ou non des modifications
    if ($sans_modification == true) {
        $nb_addition_dtsup .= " and (typecat='Inscription' or ";
        $nb_addition_dtsup .= " (typecat='Modification' ";
        $nb_addition_dtsup .= " AND code_bureau='".$b ['code']."' ";
        $nb_addition_dtsup .= " and ancien_bureau != code_bureau)) ";
    } else {
        $nb_addition_dtsup .= " and (typecat='Inscription' or typecat='Modification') ";
    }
    $nb_addition_dtsup .= " and m.etat='trs'";
}

?>
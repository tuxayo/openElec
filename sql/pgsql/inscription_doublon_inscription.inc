<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Electeur(s) avec inscription en cours");

// Icone
$ico = "ico_doublon.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 2000000;

// Critere from de la requete
$table = "mouvement as m ";
$table .= "inner join param_mouvement as pm ";
$table .= "on m.types=pm.code ";

// Critere select de la requete
$champAffiche = array(
    "id as \""._("Id")."\"",
    "nom as \""._("Nom")."\"",
    "prenom as \""._("Prenom")."\"",
    "nom_usage as \""._("Nom d'usage")."\"",
    "(to_char(date_naissance,'DD/MM/YYYY')||' "._("a")." '||libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')') as \""._("Date et lieu de naissance")."\"",
    "(numero_habitation||' '||complement_numero||' '||libelle_voie) as \""._("Adresse")."\"",
    "code_bureau as \""._("Bureau")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array();
$nom = mb_strtolower($nom, 'UTF-8');
$nom = str_replace(
    array(
        'à', 'â', 'ä', 'á', 'ã', 'å',
        'î', 'ï', 'ì', 'í',
        'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
        'ù', 'û', 'ü', 'ú',
        'é', 'è', 'ê', 'ë',
        'ç', 'ÿ', 'ñ',
        ),
    array(
        'a', 'a', 'a', 'a', 'a', 'a',
        'i', 'i', 'i', 'i',
        'o', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'u',
        'e', 'e', 'e', 'e',
        'c', 'y', 'n',
        ),
$nom
);
// Critere where de la requete
$selection = "where ";
if ($nom != "") {
    if (substr($nom,strlen($nom)-1,1) == '*') {
      $selection .= " lower(translate(nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
      .substr($nom,0,strlen($nom)-1)."%' ";
    } else {
        if ($exact == 1) {
            $selection .= " lower(translate(nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ";
            $selection .= " = '".$nom."' ";
            //$selection .= "nom = '".$nom."' ";
        } else {
            $selection .= " lower(translate(nom::".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ";
            $selection .= " like '%".$nom."%' ";
            //$selection .= "nom like '%".$nom."%' ";
        }
    }
    $selection .= "and ";
}
if ($datenaissance != "") {
    $selection .= "date_naissance='".substr($datenaissance,6,4).'-'.substr($datenaissance,3,2).'-'.substr($datenaissance,0,2)."' ";
    $selection .= "and ";
}
$selection .= "etat='actif' ";
$selection .= "and pm.typecat='Inscription' ";
$selection .= "and collectivite = '".$_SESSION['collectivite']."' ";
$selection .= "and liste = '".$_SESSION['liste']."' ";

// Critere order by ou group by de la requete
$tri = " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";

/**
 * Tableau de liens
 */
$href = array();
$href[0] = array("lien" => "#", "id" => "", "lib" => "", );
$href[1] = array("lien" => "", "id" => "", "lib" => "", );
$href[2] = array("lien" => "#", "id" => "", "lib" => "", );

/**
 * Options
 */
//
$options = array();
$option = array(
    "type" => "search",
    "display" => false,
);
array_push($options, $option);
$option = array(
    "type" => "pagination",
    "display" => false,
);
array_push($options, $option);

?>
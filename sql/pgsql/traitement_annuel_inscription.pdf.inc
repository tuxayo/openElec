<?php
/**
 * 
 */

//
require "traitement_common_pdf.inc";

//-------------------------- titre----------------------------------------------
//
if ($action == "recapitulatif") {
    //
    $libtitre = "Inscription(s) appliquée(s) lors du traitement annuel du ".$f->formatdate($datetableau)." [Tableau du ".$f->formatdate($datetableau)."]";
} else {
    //
    $libtitre = "Inscription(s) à appliquer au traitement annuel du ".$f->formatdate($f->collectivite["datetableau"])." [Tableau du ".$f->formatdate($f->collectivite["datetableau"])."]";
}
//--------------------------SQL-------------------------------------------------
$sql = " SELECT ";
$sql .= " mouvement.code_bureau as \"Bureau\", ";
$sql .= " mouvement.nom, ";
$sql .= " mouvement.prenom as \"Prenom(s)\", ";
$sql .= " to_char(mouvement.date_naissance, 'DD/MM/YYYY') as \"Naissance\", ";
$sql .= " param_mouvement.libelle as \"Motif\" ";
$sql .= $query_inscription;
$sql .= " ORDER BY mouvement.code_bureau, withoutaccent(lower(mouvement.nom)), withoutaccent(lower(mouvement.prenom)) ";
//------------------------------------------------------------------------------

?>
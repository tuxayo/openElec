<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$tableSelect = "collectivite";

//
$champs = array(
    "id",
    "ville",
    "adresse",
    "complement_adresse",
    "cp",
    "mail",
    "num_tel",
    "num_fax",
    "maire",
    "civilite",
    "maire_de",
    "inseeville",
    "expediteurinsee",
    "siret",
    "datetableau",
    "dateelection",

);

//
$selection = "";

?>
<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Nombre d'enregistrements par page
$serie = 15;

// Titre
$ent = _("Electeur")." -> "._("Procuration");

// Icone
$ico = "ico_procuration.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere from de la requete
$table = "(procuration as p inner join electeur as a on p.mandant=a.id_electeur) inner join electeur as b on p.mandataire=b.id_electeur";

// Critere select de la requete
$champAffiche = array(
    "p.id_procuration as \""._("id")."\"",
    "p.types as \""._("types")."\"",
    "(a.nom||' '||a.prenom||' '||a.code_bureau) as \""._("mandant")."\"",
    "(b.nom||' '||b.prenom||' '||b.code_bureau) as \""._("mandataire")."\"",
    "to_char(p.debut_validite,'DD/MM/YYYY') as \""._("debut")."\"",
    "to_char(p.fin_validite,'DD/MM/YYYY') as \""._("fin")."\"",
    "CASE p.refus WHEN 'O' THEN 'Oui' ELSE 'Non' END as \""._("refuse")."\"",
);


/**
 * Options
 */
//
$options = array();

// affichage du courrier de refus
$href[3]['lien']="../pdf/refus_procuration.php?idx=";

$href[3]['id']= "&amp;id=procuration";
$href[3]['lib']= "<span class=\"om-icon om-icon-16 om-icon-fix pdf-16\" title=\""._("Refus de procuration")."\">"._("Refus de procuration")."</span>";
$href[3]['target'] = "_blank";
//
$option = array(
    "type" => "condition",
    "field" => "CASE p.refus WHEN 'O' THEN 'Oui' ELSE 'Non' END",
    "case" => array(
        0 => array(
            "values" => array("Oui"),
            "href" => $href,
        ),
    ),
);
unset($href[3]);
array_push($options, $option);


// Champ sur lesquels la recherche est active
$champRecherche = array(
    "a.nom",
    "b.nom",
    "a.prenom",
    "b.prenom",
    "a.code_bureau",
    "b.code_bureau",
    "*mandant",
    "*mandataire"
);

// Critere where de la requete
$selection = "where a.collectivite = '".$_SESSION['collectivite']."' and b.collectivite = '".$_SESSION['collectivite']."' ";

// Critere order by ou group by de la requete
$tri = " order by withoutaccent(lower(a.nom)), withoutaccent(lower(a.prenom)) ";

?>

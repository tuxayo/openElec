<?php
//11/09/2006   16:02:08
$etat['orientation']="P";
$etat['format']="A4";
$etat['footerfont']="helvetica";
$etat['footerattribut']="I";
$etat['footertaille']="8";
$etat['logo']="logopdf.png";
$etat['logoleft']="70";
$etat['logotop']="27";
$etat['titre']="ATTESTATION DE RADIATION";
$etat['titreleft']="53";
$etat['titretop']="56";
$etat['titrelargeur']="130";
$etat['titrehauteur']="15";
$etat['titrefont']="helvetica";
$etat['titreattribut']="B";
$etat['titretaille']="20";
$etat['titrebordure']="0";
$etat['titrealign']="L";
$etat['corps']="[nomprenom]
[nom_usage]
Né(e) le [naissance]
à [lieu]

domicilié(e) [adresse]
             [complement]
             [voiecp]  [voieville]



Est informé(e) que la commission administrative d'£ville propose sa radiation de la liste électorale  bureau n° : [code_bureau] et qu il/elle peut présenter ses observations à cette commission dans les 24 heures, conformément aux articles L23 et R8 du code électoral.

Il vous est  précisé que ce recours gracieux est facultatif et que vous conservez le droit de former un recours - devant le tribunal d'instance - dans les dix jours à compter du £dateTableau ,date de publication et d'affichage  du tableau rectificatif, en application des articles L.25 et R.13 du code électoral.

Vous pouvez demander votre inscription dans votre nouvelle commune avant le 31 décembre de cette année.



À £ville
Le £aujourdhui


£nom";
$etat['corpsleft']="23";
$etat['corpstop']="90";
$etat['corpslargeur']="165";
$etat['corpshauteur']="5";
$etat['corpsfont']="helvetica";
$etat['corpsattribut']="";
$etat['corpstaille']="12";
$etat['corpsbordure']="0";
$etat['corpsalign']="J";
$etat['sql']="SELECT (code_bureau||' - pour le motif suivant : '||param_mouvement.libelle) as code_bureau, (civilite||' '||nom||' '||prenom) as nomprenom, 
nom_usage,
to_char(date_naissance,'DD/MM/YYYY') as naissance, (libelle_lieu_de_naissance||' ('||code_departement_naissance||')' ) as lieu,
 case  when numero_habitation =0  then trim(complement_numero||' '||voie.libelle_voie)
    else trim(numero_habitation||' '||complement_numero||' '||voie.libelle_voie) end as adresse,
complement, voie.cp as voiecp, 
voie.ville as voieville 
FROM ((mouvement LEFT JOIN voie ON mouvement.code_voie = voie.code) LEFT JOIN bureau ON mouvement.code_bureau = bureau.code) LEFT JOIN param_mouvement ON mouvement.types=param_mouvement.code
WHERE bureau.collectivite = '£collectivite' and £idx";
$etat['sousetat']=array();
$etat['se_font']="helvetica";
$etat['se_margeleft']="8";
$etat['se_margetop']="5";
$etat['se_margeright']="5";
$etat['se_couleurtexte']=array("0","0","0");
?>
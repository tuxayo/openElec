<?php
/**
 *
 */

/**
 * Cette requete permet de lister tous les bureaux en fonction de la
 * collectivite en cours
 * 
 * @param string $_SESSION['collectivite']
 */
$query_select_bureau = "select * from bureau ";
$query_select_bureau .= "where collectivite='".$_SESSION['collectivite']."' ";
$query_select_bureau .= "order by code ";

if (isset($bureau['code'])) {
    /**
     * Cette requete permet de compter le nombre de mandant dans un bureau
     * en fonction de la collectivite en cours, de la liste en cours,
	 * du bureau selectionne
	 * 
	 * @param string $bureau['code'] Code du bureau
	 * @param string $_SESSION['collectivite']
	 * @param string $_SESSION['liste']
     */
    $query_count_mandant_bureau = "select count(*) ";
    $query_count_mandant_bureau .= "from procuration, electeur as a ";
    $query_count_mandant_bureau .= "where a.liste='".$_SESSION['liste']."' ";
    $query_count_mandant_bureau .= "and a.collectivite='".$_SESSION['collectivite']."' ";
    $query_count_mandant_bureau .= "and a.code_bureau='".$bureau['code']."' ";
    $query_count_mandant_bureau .= "and procuration.mandant=a.id_electeur ";
}

?>
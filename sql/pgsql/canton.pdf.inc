<?php
/**
 *
 */

// ------------------------document---------------------------------------------
$orientation="L";// orientation P-> portrait L->paysage
$format="A4";// format A3 A4 A5
$police='arial';
$margeleft=10;// marge gauche
$margetop=5;// marge haut
$margeright=5;//  marge droite
$border=1; // 1 ->  bordure 0 -> pas de bordure
$C1="0";// couleur texte  R
$C2="0";// couleur texte  V
$C3="0";// couleur texte  B

//-------------------------LIGNE tableau----------------------------------------
$size=8; //taille POLICE
$height=4.6; // -> hauteur ligne tableau
$align='L';
$fond=1;// 0- > FOND transparent 1 -> fond
$C1fond1="234";// couleur fond  R 241
$C2fond1="240";// couleur fond  V 241
$C3fond1="245";// couleur fond  B 241
$C1fond2="255";// couleur fond  R
$C2fond2="255";// couleur fond  V
$C3fond2="255";// couleur fond  B

//-------------------------- titre----------------------------------------------
$libtitre = $f->collectivite['ville']." - "._("Liste des cantons"); // libelle titre
$flagsessionliste=0;// 1 - > affichage session liste ou 0 -> pas d'affichage
$bordertitre=0; // 1 ->  bordure 0 -> pas de bordure
$aligntitre='L'; // L,C,R
$heightitre=10;// hauteur ligne titre
$grastitre="";//$gras="B" -> BOLD OU $gras=""
$fondtitre=0; //0- > FOND transparent 1 -> fond
$C1titrefond="181";// couleur fond  R
$C2titrefond="182";// couleur fond  V
$C3titrefond="188";// couleur fond  B
$C1titre="75";// couleur texte  R
$C2titre="79";// couleur texte  V
$C3titre="81";// couleur texte  B
$sizetitre="13";
//--------------------------libelle entete colonnes-----------------------------
$flag_entete=1;//entete colonne : 0 -> non affichage , 1 -> affichage
$fondentete=1;// 0- > FOND transparent 1 -> fond
$heightentete=10;//hauteur ligne entete colonne
$C1fondentete="210";// couleur fond  R
$C2fondentete="216";// couleur fond  V
$C3fondentete="249";// couleur fond  B
$C1entetetxt="0";// couleur texte R
$C2entetetxt="0";// couleur texte V
$C3entetetxt="0";// couleur texte B
// ------ couleur border--------------------------------------------------------
$C1border="159";// couleur texte  R
$C2border="160";// couleur texte  V
$C3border="167";// couleur texte  B
//------ largeur de chaque colonne $l0  à $l.. ( $l OBLIGATOIRE )---------------
$widthtableau=272;// -> ajouter $l0 à $lxx
$bt=1;// border 1ere  et derniere ligne  dutableau par page->0 ou 1
//-------
$nbcol = 2;   
//-------
for ($i = 0; $i < $nbcol; $i++) {
    //------ Border entete colonne $be0  à $be.. ( $be OBLIGATOIRE )
    ${"be".$i} = "L";
    //------ Border entete colonne $be0  à $be.. ( $be OBLIGATOIRE )
    ${"b".$i} = "L";
    //------ ALIGNEMENT entete colonne $ae0  à $ae.. ( $ae OBLIGATOIRE )
    ${"ae".$i} = "C";
    //------ ALIGNEMENT de chaque colonne $l0  à $a.. ( $a OBLIGATOIRE )-------
    ${"a".$i} = "L";
    // ---- ch(amps)n(umerique)d(ecimale)0 à nn / 999->non, 0 à nn->nbr decimale(s)-
    ${"chnd".$i} = 999;
    // calcul et affichage totaux 1->Totaux 0->non ----------------------------
    ${"chtot".$i} = 0;
    // calcul et affichage moyenne 1->moyenne 0->non --------------------------
    ${"chmoy".$i} = 0;
    //------ largeur de chaque colonne $l0  à $l.. ( $l OBLIGATOIRE )---------------
}
//
${"be".($nbcol-1)} = "LR";
${"b".($nbcol-1)} = "LR";

$l0 = 10;
$l1 = $widthtableau - $l0;
//--------------------------SQL-------------------------------------------------
$sql = "select ";
$sql .= "code as \""._("Code")."\", ";
$sql .= "libelle_canton as \""._("Canton")."\" ";
$sql .= "from canton order by libelle_canton";
//------------------------------------------------------------------------------
?>
<?php
/**
 * $Id$
 *
 */
//
// inscription j-5 ==========================================================
      $sqlinsj5='';
      $sqlinsj5="SELECT ";
      //
      $sqlinsj5=$sqlinsj5." count(*) as nbrinsj5";
      $sqlinsj5=$sqlinsj5." FROM  mouvement inner join param_mouvement  on  mouvement.types  = param_mouvement.code ";
      $sqlinsj5=$sqlinsj5." WHERE param_mouvement.typecat='Inscription'";
      $sqlinsj5=$sqlinsj5." AND mouvement.tableau='j5'";
      $sqlinsj5=$sqlinsj5." AND mouvement.date_tableau='".$f -> collectivite ['datetableau']."'";
      /* ++ */ $sqlinsj5=$sqlinsj5." AND mouvement.liste='".$_SESSION['liste']."' AND mouvement.collectivite = '".$mairie."' ";
//
// radiation j-5 =========================================================

      $sqlradj5='';
      $sqlradj5="SELECT ";
      //
      $sqlradj5=$sqlradj5." count(*) as nbradj5";
      $sqlradj5=$sqlradj5." FROM  mouvement inner join param_mouvement  on  mouvement.types  = param_mouvement.code ";
      $sqlradj5=$sqlradj5." WHERE param_mouvement.typecat='Radiation'";
      $sqlradj5=$sqlradj5." AND mouvement.tableau='j5'";
      $sqlradj5=$sqlradj5." AND mouvement.date_tableau='".$f -> collectivite ['datetableau']."'";
      /* ++ */ $sqlradj5=$sqlradj5." AND mouvement.liste='".$_SESSION['liste']."' AND mouvement.collectivite = '".$mairie."' ";

// Requete de recuperation du nombre de radiations de tous les mouvements
// traites a une date superieure ou egale a la date de tableau en cours
$nb_radiation_dtsup = "select count(*) as nbr ";
$nb_radiation_dtsup .= "from mouvement as m inner join param_mouvement as pm ";
$nb_radiation_dtsup .= "on m.types=pm.code ";
$nb_radiation_dtsup .= "where date_tableau>='".$f->collectivite ['datetableau']."' ";
$nb_radiation_dtsup .= " and liste='".$_SESSION['liste']."'";
$nb_radiation_dtsup .= " and m.collectivite = '".$mairie."' ";
$nb_radiation_dtsup .= " and pm.typecat='Radiation' ";
$nb_radiation_dtsup .= " and m.etat='trs'";

// Requete de recuperation du nombre d'additions de tous les mouvements
// traites a une date superieure ou egale a la date de tableau en cours
$nb_addition_dtsup = "select count(*) as nbr ";
$nb_addition_dtsup .= "from mouvement as m inner join param_mouvement as pm ";
$nb_addition_dtsup .= "on m.types=pm.code ";
$nb_addition_dtsup .= "where date_tableau>='".$f->collectivite ['datetableau']."' ";
$nb_addition_dtsup .= " and liste='".$_SESSION['liste']."'";
$nb_addition_dtsup .= " and m.collectivite = '".$mairie."' ";
$nb_addition_dtsup .= " and pm.typecat='Inscription' ";
$nb_addition_dtsup .= " and m.etat='trs'";

?>
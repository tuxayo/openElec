<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$query_electeurs_actuels_groupby_bureau = " SELECT ";
$query_electeurs_actuels_groupby_bureau .= " electeur.code_bureau as bureau, ";
$query_electeurs_actuels_groupby_bureau .= " electeur.sexe as sexe, ";
$query_electeurs_actuels_groupby_bureau .= " count(electeur.id_electeur) as total ";
$query_electeurs_actuels_groupby_bureau .= " FROM electeur ";
$query_electeurs_actuels_groupby_bureau .= " WHERE electeur.collectivite='".$_SESSION['collectivite']."' ";
$query_electeurs_actuels_groupby_bureau .= " AND electeur.liste='".$_SESSION['liste']."' ";
$query_electeurs_actuels_groupby_bureau .= " GROUP BY electeur.code_bureau, electeur.sexe";

//
$query_inscriptions_radiations_stats_groupby_bureau = " SELECT ";
$query_inscriptions_radiations_stats_groupby_bureau .= " mouvement.code_bureau as bureau, ";
$query_inscriptions_radiations_stats_groupby_bureau .= " mouvement.date_tableau, ";
$query_inscriptions_radiations_stats_groupby_bureau .= " mouvement.date_j5, ";
$query_inscriptions_radiations_stats_groupby_bureau .= " mouvement.tableau, ";
$query_inscriptions_radiations_stats_groupby_bureau .= " param_mouvement.typecat, ";
$query_inscriptions_radiations_stats_groupby_bureau .= " mouvement.etat, ";
$query_inscriptions_radiations_stats_groupby_bureau .= " mouvement.sexe as sexe, ";
$query_inscriptions_radiations_stats_groupby_bureau .= " count(*) as total";
$query_inscriptions_radiations_stats_groupby_bureau .= " FROM mouvement INNER JOIN param_mouvement ";
$query_inscriptions_radiations_stats_groupby_bureau .= " ON mouvement.types=param_mouvement.code ";
$query_inscriptions_radiations_stats_groupby_bureau .= " WHERE mouvement.collectivite='".$_SESSION['collectivite']."' ";
$query_inscriptions_radiations_stats_groupby_bureau .= " AND mouvement.liste='".$_SESSION['liste']."' ";
$query_inscriptions_radiations_stats_groupby_bureau .= " AND (param_mouvement.typecat='Inscription' OR param_mouvement.typecat='Radiation') ";
$query_inscriptions_radiations_stats_groupby_bureau .= " GROUP BY mouvement.code_bureau, mouvement.date_tableau, mouvement.date_j5, mouvement.tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe ";
$query_inscriptions_radiations_stats_groupby_bureau .= " ORDER BY mouvement.code_bureau, mouvement.date_tableau, mouvement.date_j5, mouvement.tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe ";

//
$query_transferts_plus_stats_groupby_bureau = " SELECT ";
$query_transferts_plus_stats_groupby_bureau .= " mouvement.code_bureau as bureau, ";
$query_transferts_plus_stats_groupby_bureau .= " mouvement.date_tableau, ";
$query_transferts_plus_stats_groupby_bureau .= " mouvement.date_j5, ";
$query_transferts_plus_stats_groupby_bureau .= " mouvement.tableau, ";
$query_transferts_plus_stats_groupby_bureau .= " param_mouvement.typecat, ";
$query_transferts_plus_stats_groupby_bureau .= " mouvement.etat, ";
$query_transferts_plus_stats_groupby_bureau .= " mouvement.sexe as sexe, ";
$query_transferts_plus_stats_groupby_bureau .= " count(*) as total ";
$query_transferts_plus_stats_groupby_bureau .= " FROM mouvement INNER JOIN param_mouvement ";
$query_transferts_plus_stats_groupby_bureau .= " ON mouvement.types=param_mouvement.code ";
$query_transferts_plus_stats_groupby_bureau .= " WHERE mouvement.collectivite='".$_SESSION['collectivite']."' ";
$query_transferts_plus_stats_groupby_bureau .= " AND mouvement.liste='".$_SESSION['liste']."' ";
$query_transferts_plus_stats_groupby_bureau .= " AND mouvement.code_bureau <> mouvement.ancien_bureau ";
$query_transferts_plus_stats_groupby_bureau .= " AND param_mouvement.typecat='Modification' ";
$query_transferts_plus_stats_groupby_bureau .= " AND mouvement.ancien_bureau<>'' ";
$query_transferts_plus_stats_groupby_bureau .= " GROUP BY mouvement.code_bureau, mouvement.date_tableau, mouvement.date_j5, mouvement.tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe ";

//
$query_transferts_moins_stats_groupby_bureau = " SELECT ";
$query_transferts_moins_stats_groupby_bureau .= " mouvement.ancien_bureau as bureau, ";
$query_transferts_moins_stats_groupby_bureau .= " mouvement.date_tableau, ";
$query_transferts_moins_stats_groupby_bureau .= " mouvement.date_j5, ";
$query_transferts_moins_stats_groupby_bureau .= " mouvement.tableau, ";
$query_transferts_moins_stats_groupby_bureau .= " param_mouvement.typecat, ";
$query_transferts_moins_stats_groupby_bureau .= " mouvement.etat, ";
$query_transferts_moins_stats_groupby_bureau .= " mouvement.sexe as sexe, ";
$query_transferts_moins_stats_groupby_bureau .= " count(*) as total ";
$query_transferts_moins_stats_groupby_bureau .= " FROM mouvement INNER JOIN param_mouvement ";
$query_transferts_moins_stats_groupby_bureau .= " ON mouvement.types=param_mouvement.code ";
$query_transferts_moins_stats_groupby_bureau .= " WHERE mouvement.collectivite='".$_SESSION['collectivite']."' ";
$query_transferts_moins_stats_groupby_bureau .= " AND mouvement.liste='".$_SESSION['liste']."' ";
$query_transferts_moins_stats_groupby_bureau .= " AND mouvement.code_bureau <> mouvement.ancien_bureau ";
$query_transferts_moins_stats_groupby_bureau .= " AND param_mouvement.typecat='Modification' ";
$query_transferts_moins_stats_groupby_bureau .= " AND mouvement.ancien_bureau<>'' ";
$query_transferts_moins_stats_groupby_bureau .= " GROUP BY mouvement.ancien_bureau, mouvement.date_tableau, mouvement.date_j5, mouvement.tableau, param_mouvement.typecat, mouvement.etat, mouvement.sexe";

?>

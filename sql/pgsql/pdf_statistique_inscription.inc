<?php
/**
 *
 * @package openelec
 * @version SVN : $Id$
 */

// communes 
$sqlcom1="SELECT ";
$sqlcom1.=" c.id as id, c.ville as ville,";
////// Compteur inscription homme "Exclu Office"
$sqlcom1.=" (SELECT count(m.*) FROM mouvement m,param_mouvement p where m.collectivite=c.id and p.code=m.types and p.typecat='Inscription' and p.codeinscription<>'8' and m.sexe='M' and m.date_tableau='".$f -> collectivite ['datetableau']."'  ) as totalm, ";
////// Compteur inscription femme "Exclu Office"
$sqlcom1.=" (SELECT count(m.*) FROM mouvement m,param_mouvement p where m.collectivite=c.id and p.code=m.types and p.typecat='Inscription' and p.codeinscription<>'8' and m.sexe='F' and m.date_tableau='".$f -> collectivite ['datetableau']."' ) as totalf, ";
////// Compteur inscription homme "Inclu Office"
$sqlcom1.=" (SELECT count(m.*) FROM mouvement m,param_mouvement p where m.collectivite=c.id and p.code=m.types and p.typecat='Inscription' and p.codeinscription='8' and m.sexe='M' and m.date_tableau='".$f -> collectivite ['datetableau']."' ) as totalm8, ";
////// Compteur inscription femme "Inclu Office"
$sqlcom1.=" (SELECT count(m.*) FROM mouvement m,param_mouvement p where m.collectivite=c.id and p.code=m.types and p.typecat='Inscription' and p.codeinscription='8' and m.sexe='F' and m.date_tableau='".$f -> collectivite ['datetableau']."' ) as totalf8 ";
$sqlcom1.=" FROM collectivite c";
if ($f->isMulti() != true){
      $sqlcom1.=" WHERE c.ville='".$f->collectivite['ville']."' ";
} else {
      $sqlcom1.=" WHERE c.type_interface='0' ";
}
$sqlcom1.=" ORDER BY c.ville;";

?>

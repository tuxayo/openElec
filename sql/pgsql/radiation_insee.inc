<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Nombre d'enregistrements par page
$serie = 20;

// Titre
$ent = "Consultation -> Radiation Insee";

// Icone
$ico = "ico_mouvementi.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere from de la requete
$table = "radiation_insee";

// Critere select de la requete
$champAffiche = array (
        "id",
        "('<b>'|| nom || '</b><br /> ' || prenom1 || ' '|| prenom2 || ' ' || prenom3) as \""._("nom et prenoms")."\"",
        "(to_char(date_naissance,'DD/MM/YYYY')||' <br />en '||libelle_lieu_de_naissance ) as \""._("date et lieu de naissance")."\"",
        "(adresse1||' '||adresse2) as \""._("adresse")."\"",
        "motif_de_radiation as \""._("motif de radiation")."\"",
        "types as \""._("etat")."\"");

// Champ sur lesquels la recherche est active
$champRecherche = array ("nom", "prenom1", "prenom2","prenom3");

// Critere order by ou group by de la requete
$tri = "order by types ASC, withoutaccent(lower(nom)) ASC, withoutaccent(lower(prenom1)) ASC";

// Critere where de la requete
$selection = "where collectivite = '".$_SESSION['collectivite']."'";

//
$href[0]['lien'] = "";
$href[0]['id']= "";
$href[0]['lib']= "";
$href[1]['lien'] = "";
$href[1]['id']= "";
$href[1]['lib']= "";
$href[2]['lien'] = "../scr/form.php?obj=radiation_insee&amp;idx=";
$href[2]['id']= "&amp;premier=".$premier."&amp;recherche=".$recherche."&amp;ids=1";
$href[2]['lib']= "<span class=\"om-icon om-icon-16 om-icon-fix delete-16\" title=\""._("Supprimer")."\">"._("Supprimer")."</span>";
$href[3]['lien'] = "../app/radiation_insee.doublon.php?idrad=";
$href[3]['id']= "&amp;premier=".$premier."&amp;recherche=".$recherche;
$href[3]['lib']= "<span class=\"om-icon om-icon-16 om-icon-fix radiation-insee-16\" title=\""._("Valider la radiation")."\">"._("Valider la radiation")."</span>";

/**
 * Options
 */
//
$options = array();
//
$option = array(
    "type" => "condition",
    "field" => "types",
    "case" => array(
        "0" => array(
            "values" => array("1", ),
            "style" => "etat-trs",
            "href" => array(
                0 => array("lien" => "", "id" => "", "lib" => ""),
                1 => array("lien" => "", "id" => "", "lib" => ""),
                2 => array("lien" => "", "id" => "", "lib" => ""),
                3 => array("lien" => "", "id" => "", "lib" => ""),
            ),
        ),
    ),
);
array_push($options, $option);


?>

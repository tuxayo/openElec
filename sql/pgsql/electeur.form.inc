<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

// Critere from de la requete
$tableSelect = "electeur";

    //////////// => A VERIFIER SI TOUJOURS D'ACTUALITE
    // attention de ne pas changer l'ordre car repris dans
    // radiation.form.php
    // transfert.form.php
    // modification.form.php
    // les tableaux $val sont indicés et non associés
    // mouvement.class = setval
    // modification.class = archiveElecteur
    ////////////////////////////////////////////////////

// Critere select de la requete
$champs = array(
    //
    "id_electeur",                  //0
    "numero_electeur",              //31
    "liste",                        //30
    "bureauforce",                  //3
    "numero_bureau",                //1
    "code_bureau",                  //2
    "date_modif",
    "utilisateur",
    //
    "civilite",                     //4
    "sexe",                         //5
    "nom",                          //6
    "nom_usage",                    //7
    "prenom",                       //8
    "situation",                    //9
    //
    "date_naissance",               //10
    "code_departement_naissance",   //11
    "libelle_departement_naissance",//12
    "code_lieu_de_naissance",       //13
    "libelle_lieu_de_naissance",    //14
    "code_nationalite",             //15
    //
    "numero_habitation",            //16
    "code_voie",                    //18
    "libelle_voie",                 //17
    "complement_numero",            //19
    "complement",                   //20
    //
    "resident",                     //25
    "adresse_resident",             //26
    "complement_resident",          //27
    "cp_resident",                  //28
    "ville_resident",               //29
    //
    "provenance",                   //21
    "libelle_provenance",           //22
    //
    "carte",                        //32 non transfere
    "jury",                         //33 non transfere
    "jury_effectif",
    "date_jeffectif",
    "profession",
    "motif_dispense_jury",
    //
    "mouvement",                    //23
    "procuration"                  //24
);

// Critere where de la requete
$selection = "and collectivite = '".$_SESSION['collectivite']."' ";

/**
 * Parametrages des select
 */
//
$sql_nationalite = "select code, (libelle_nationalite||'('||code||')') from nationalite order by libelle_nationalite";

?>
<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
//
$filename_more = "-liste".$_SESSION['liste'];
//
//$offset = 0;
//if (isset($f) && isset($_GET["code_bureau"])) {
//    //
//    $code_bureau = $_GET["code_bureau"];
//    $filename_more .= "-".$code_bureau;
//	//
//    $offset = $f->db->getone("select count(*) from electeur where code_bureau<'".$code_bureau."' and collectivite='".$_SESSION['collectivite']."' and liste='".$_SESSION ['liste']."'");
//    $f->isDatabaseError($offset);
//}
if (isset($f) && isset($_GET["type"])) {
    //
    $type_mouvement = $_GET["type"];
    $filename_more .= "-".$type_mouvement;
}

if (isset($f) && isset($_GET["datetableau"])) {
    //
    $datetableau = $_GET["datetableau"];
    $filename_more .= "-".$datetableau;
}
if (isset($f) && isset($_GET["datej5"])) {
    //
    $datej5 = $_GET["datej5"];
    $filename_more .= "-".$datej5;
}

/**
 *
 */
//******************************************************************************
//                  DIFFERENTES ZONES A AFFICHER                              //
//******************************************************************************
//------------------------------------------------------------------------------
//                  COMPTEUR                                                  //
//------------------------------------------------------------------------------
//(0) 1 -> affichage compteur ou 0 ->pas d'affichage
// (1) x  (2) y  (3) width (4) bold 1 ou 0  (5) size ou 0
$champs_compteur = array();
//$champs_compteur = array(
//    1,
//    $parametrage_etiquettes["etiquette_marge_int_gauche"]+($parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"])*(2/3),
//    $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 0),
//    ($parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"])*(1/3),
//    0,
//    8,
//    $offset,
//    "R"
//);
//------------------------------------------------------------------------------
//                  IMAGE                                                     //
//------------------------------------------------------------------------------
//  (0) nom image (1) x  (2) y  (3) width (4) hauteue  (5) type
// $img=array(array('../img/arles.png',1,1,17.6,12.6,'png')
$img=array();
//------------------------------------------------------------------------------
//                  TEXTE                                                     //
//------------------------------------------------------------------------------
// (0) texte (1) x  (2) y  (3) width (4) bold 1 ou 0  (5) size ou 0
$texte=array();
//------------------------------------------------------------------------------
//                  DATA                                                      //
//------------------------------------------------------------------------------
// (0) affichage avant data
// (1) affichage apres data
// (2) tableau X Y Width bold(0 ou 1),size ou 0
// (3) 1 = number_format(champs,0) : 0002->2  /  ou 0
$champs = array(
    'ligne1'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 0),
                              ($parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"])*(2/3),
                              0,
                              8),0),
    'ligne2'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 1),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne3'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 2),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne4'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 3),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne5'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 4),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne6'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 5),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0)
);

//******************************************************************************
//                        SQL                                                 //
//******************************************************************************
$sql = " SELECT ";
//
$sql .= " ''  AS ligne1, ";
//$sql .= " (code_bureau||' - '|| numero_bureau)  AS ligne1, ";
//
$sql .= " CASE ";
    $sql .= " WHEN (mouvement.sexe = 'F' and mouvement.situation = 'M' and mouvement.nom_usage<>'')";
        $sql .= " THEN (upper(mouvement.civilite) ||' '||mouvement.nom||' EP '||mouvement.nom_usage||' '||mouvement.prenom )";
    $sql .= " WHEN ((mouvement.situation <> 'M' or mouvement.situation is NULL) and mouvement.nom_usage <> '')";
        $sql .= " THEN (upper(mouvement.civilite) ||' '||mouvement.nom||' - '||mouvement.nom_usage||' '||mouvement.prenom ) ";
    $sql .= " ELSE (upper(mouvement.civilite) ||' '||mouvement.nom||' '||mouvement.prenom) ";
$sql .= " END AS ligne2, ";
//
$sql .= " '' AS ligne3, ";
//
$sql .= " CASE resident ";
    $sql .= " WHEN 'Non' THEN (";
        $sql .= " CASE numero_habitation ";
            $sql .= " WHEN 0 THEN '' ";
            $sql .= " ELSE (numero_habitation||' ') ";
        $sql .= " END ";
        $sql .= " || ";
        $sql .= " CASE complement_numero ";
            $sql .= " WHEN '' THEN '' ";
            $sql .= " ELSE (complement_numero||' ') ";
        $sql .= " END ";
    $sql .= " ||mouvement.libelle_voie) ";
    $sql .= " WHEN 'Oui' THEN adresse_resident ";
$sql .= " END AS ligne4, ";
//
$sql .= " CASE resident ";
    $sql .= " WHEN 'Non' THEN mouvement.complement ";
    $sql .= " WHEN 'Oui' THEN complement_resident ";
$sql .= " END AS ligne5, ";
//
$sql .= " CASE resident ";
    $sql .= " WHEN 'Non' THEN (voie.cp||' '||voie.ville) ";
    $sql .= " WHEN 'Oui' THEN (cp_resident||' '||ville_resident) ";
$sql .= " END AS ligne6 ";
//
$sql .= " FROM ";
$sql .= " voie RIGHT JOIN mouvement ";
$sql .= " ON voie.code = mouvement.code_voie ";
$sql .= " INNER JOIN param_mouvement ";
$sql .= " ON mouvement.types = param_mouvement.code ";
//
$sql .= " WHERE ";
$sql .= " mouvement.liste='".$_SESSION['liste']."' ";
$sql .= " AND mouvement.collectivite='".$_SESSION['collectivite']."' ";
$sql .= " AND param_mouvement.effet='Election' ";
//
if (isset($type_mouvement)) {
    $sql .= " AND types='".$type_mouvement."'";
} else {
    $sql .= " AND types='ZZZZZZZZZ'";
}
//
if (isset($datetableau)) {
    $sql .= " AND mouvement.date_tableau='".$datetableau."'";
}
//
if (isset($datej5)) {
    $sql .= " AND mouvement.date_j5='".$datej5."'";
} else {
    $sql .= " AND mouvement.etat='actif'";
}



//
$sql .= " ORDER BY code_bureau, withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
//

?>

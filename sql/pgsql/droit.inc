<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Parametrage")." -> "._("Droit");

// Icone
$ico = "ico_parametrage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 15;

// Critere from de la requete
$table = "droit left join profil ";
$table .= "on profil.profil=droit.profil";

// Critere select de la requete
$champAffiche = array(
    "droit.droit as \""._("Droit")."\"",
    "(droit.profil||' '||profil.libelle_profil) as \""._("Profil utilisateur")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "droit.droit as \""._("Droit")."\"",
    "droit.profil as \""._("Profil")."\"",
    "profil.libelle_profil as \""._("Libelle profil")."\"",
    "(droit.profil||' '||profil.libelle_profil) as \""._("Profil utilisateur")."\"",
);

// Critere where de la requete
$selection = "";

// Critere order by ou group by de la requete
$tri = " order by droit";

?>
<?php
/**
 * $Id$
 *
 */

//
$sqlbur="SELECT ";
$sqlbur=$sqlbur." (bureau.code) AS burcod,";
$sqlbur=$sqlbur." (bureau.libelle_bureau) as libbur,";
$sqlbur=$sqlbur." (canton.libelle_canton) as libcanton";
$sqlbur=$sqlbur." FROM bureau LEFT JOIN canton ON bureau.code_canton = canton.code";
$sqlbur=$sqlbur." WHERE bureau.code='".$nobureau."'";
$sqlbur=$sqlbur." AND bureau.collectivite='".$_SESSION["collectivite"]."'";
//
$sql="SELECT ";
// MANDAT
$sql=$sql."  a.nom as nom,";
$sql=$sql."  a.nom_usage as nom_usage,";
$sql=$sql."  a.prenom as prenom,";
// MANDATAIRE
$sql=$sql."  b.nom as nomm,";
$sql=$sql."  b.nom_usage as nom_usagem,";
$sql=$sql."  b.prenom as prenomm,";
//
$sql=$sql."  substring(procuration.origine1 from 1 for 25) as origine1_1, ";
$sql=$sql."  substring(procuration.origine1 from 26 for 25) as origine1_2, ";
//
$sql=$sql."  substring(procuration.origine2 from 1 for 25) as origine2_1, ";
$sql=$sql."  substring(procuration.origine2 from 26 for 25) as origine2_2, ";


$sql=$sql."  CASE refus WHEN 'O' THEN 'Oui' ELSE 'Non' END as refus, ";

$sql=$sql."  substring(motif_refus from 1 for 24) as motif_refus_1, ";
$sql=$sql."  substring(motif_refus from 24 for 24) as motif_refus_2, ";
$sql=$sql."  substring(motif_refus from 48 for 24) as motif_refus_3, ";

$sql=$sql."  to_char(date_accord,'DD/MM/YYYY') as dateaccord, ";
$sql=$sql."  procuration.heure_accord as heureaccord, ";
$sql=$sql."  to_char(procuration.debut_validite,'DD/MM/YYYY') as debutvalidm, ";
$sql=$sql."  to_char(procuration.fin_validite,'DD/MM/YYYY') as finvalidm";
//
$sql=$sql."  FROM procuration, electeur as a, electeur as b ";
$sql=$sql."  WHERE a.liste='".$_SESSION['liste']."'";
$sql=$sql."  and procuration.mandant = a.id_electeur and procuration.mandataire = b.id_electeur";
//
if($id=="bureau")
$sql=$sql."  AND a.code_bureau='".$nobureau."'";
//
$sql=$sql."  AND a.collectivite='".$_SESSION["collectivite"]."'";
$sql=$sql."  AND b.collectivite='".$_SESSION["collectivite"]."'";
//
$sql=$sql." order by withoutaccent(lower(a.nom)), withoutaccent(lower(a.prenom)) ";

?>
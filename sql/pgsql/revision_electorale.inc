<?php
/**
 * Requetes necessaires a l'affichage de la revision electorale
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 * Cette requete permet de compter tous les electeurs de la table electeur en
 * fonction de la collectivite en cours et de la liste en cours
 * 
 * @param string $_SESSION['collectivite']
 * @param string $_SESSION['liste']
 */
$query_count_electeur = "select count(*) ";
$query_count_electeur .= "from ".DB_PREFIXE."electeur ";
$query_count_electeur .= "where electeur.liste='".$_SESSION['liste']."' ";
$query_count_electeur .= "and electeur.collectivite='".$_SESSION['collectivite']."' ";

/**
 *
 */
$query_list_tableau_des_cinq_jours = " select date_j5 ";
$query_list_tableau_des_cinq_jours .= " from ".DB_PREFIXE."mouvement ";
$query_list_tableau_des_cinq_jours .= " where mouvement.date_tableau='".$date_tableau_premier_tableau."' ";
$query_list_tableau_des_cinq_jours .= " and mouvement.etat='trs' ";
$query_list_tableau_des_cinq_jours .= " and mouvement.tableau='j5' ";
$query_list_tableau_des_cinq_jours .= " and mouvement.liste='".$_SESSION['liste']."' ";
$query_list_tableau_des_cinq_jours .= " and mouvement.collectivite='".$_SESSION['collectivite']."' ";
$query_list_tableau_des_cinq_jours .= " group by date_j5 ";
$query_list_tableau_des_cinq_jours .= " order by date_j5 ";

/**
 *
 */
$query_count_all_mouvement_by_typecat = " select date_tableau, date_j5, typecat, effet, types, libelle, etat, count(*) ";
$query_count_all_mouvement_by_typecat .= " from mouvement as m inner join param_mouvement as pm on m.types=pm.code ";
$query_count_all_mouvement_by_typecat .= " where liste='".$_SESSION['liste']."' ";
$query_count_all_mouvement_by_typecat .= " and collectivite='".$_SESSION['collectivite']."' ";
$query_count_all_mouvement_by_typecat .= " group by date_tableau, date_j5, typecat, effet, types, libelle, etat ";
$query_count_all_mouvement_by_typecat .= " order by date_tableau, date_j5, typecat, effet, types, libelle, etat ";

?>

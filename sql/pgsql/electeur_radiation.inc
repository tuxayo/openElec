<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Saisie")." -> "._("Radiation");

// Icone
$ico = "ico_mouvementr.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 12;

// Critere from de la requete
$table = "electeur";

// Critere select de la requete
$champAffiche = array(
   "id_electeur as \""._("Id")."\"",
    "nom as \""._("Nom")."\"",
    "prenom as \""._("Prenom")."\"",
    "nom_usage as \""._("Nom d'usage")."\"",
        "(to_char(date_naissance,'DD/MM/YYYY')||' "._("a")." '||libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')') as \""._("Date et lieu de naissance")."\"",
   "(numero_habitation||' '||complement_numero||' '||libelle_voie) as \""._("Adresse")."\"",
   "code_bureau as \""._("Bureau")."\"",
   "typecat as \""._("En cours")."\""
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
   "id_electeur",
   "nom",
   "prenom"
);

// Critere where de la requete
$selection = " where collectivite = '".$_SESSION['collectivite']."' ";
$selection .= " and liste = '".$_SESSION['liste']."' ";

/**
 *
 */
//
$nom = mb_strtolower($nom, 'UTF-8');
$nom = str_replace(
    array(
        'à', 'â', 'ä', 'á', 'ã', 'å',
        'î', 'ï', 'ì', 'í',
        'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
        'ù', 'û', 'ü', 'ú',
        'é', 'è', 'ê', 'ë',
        'ç', 'ÿ', 'ñ',
        ),
    array(
        'a', 'a', 'a', 'a', 'a', 'a',
        'i', 'i', 'i', 'i',
        'o', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'u',
        'e', 'e', 'e', 'e',
        'c', 'y', 'n',
        ),
$nom
);
if (isset($nom) and $nom != "") {
   if (substr($nom,strlen($nom)-1,1) == '*') {
      $selection .= " and lower(translate(nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
      .substr($nom,0,strlen($nom)-1)."%' ";
   } else {
      if( isset($exact) and $exact != true ) {
         $selection .= " and lower(translate(nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
         .$nom."%' ";
      } else {
         $selection .= " and lower(translate(nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ='"
         .$nom."' ";
      }
   }
}
//
$prenom = mb_strtolower($prenom, 'UTF-8');
$prenom = str_replace(
    array(
        'à', 'â', 'ä', 'á', 'ã', 'å',
        'î', 'ï', 'ì', 'í',
        'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
        'ù', 'û', 'ü', 'ú',
        'é', 'è', 'ê', 'ë',
        'ç', 'ÿ', 'ñ',
        ),
    array(
        'a', 'a', 'a', 'a', 'a', 'a',
        'i', 'i', 'i', 'i',
        'o', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'u',
        'e', 'e', 'e', 'e',
        'c', 'y', 'n',
        ),
$prenom
);
if (isset($prenom) and $prenom != ""){
   if (substr($prenom,strlen($prenom)-1,1) == '*') {
      $selection .= " and lower(translate(prenom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
      .substr($prenom,0,strlen($prenom)-1)."%' ";
   } else {
      if( isset($exact) and $exact != true ) {
         $selection .= " and lower(translate(prenom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
         .$prenom."%' ";
      } else {
         $selection .= " and lower(translate(prenom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ='"
         .$prenom."' ";
      }
   }
}

//
$marital = mb_strtolower($marital, 'UTF-8');
$marital = str_replace(
    array(
        'à', 'â', 'ä', 'á', 'ã', 'å',
        'î', 'ï', 'ì', 'í',
        'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
        'ù', 'û', 'ü', 'ú',
        'é', 'è', 'ê', 'ë',
        'ç', 'ÿ', 'ñ',
        ),
    array(
        'a', 'a', 'a', 'a', 'a', 'a',
        'i', 'i', 'i', 'i',
        'o', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'u',
        'e', 'e', 'e', 'e',
        'c', 'y', 'n',
        ),
$marital
);
if (isset($marital) and $marital != "") {
   if (substr($marital,strlen($marital)-1,1) == '*') {
      $selection .= " and lower(translate(nom_usage".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
      .substr($marital,0,strlen($marital)-1)."%' ";
   } else {
      if( isset($exact) and $exact != true ) {
         $selection .= " and lower(translate(nom_usage".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
         .$marital."%' ";
      } else {
         $selection .= " and lower(translate(nom_usage".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ='"
         .$marital."' ";
      }
   }
}

//
if (isset($datenaissance) and $datenaissance != "") {
   $selection .= " and date_naissance ='".$datenaissance."' ";
}

// Critere order by ou group by de la requete
$tri = " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";

/**
 * Tableau de liens
 */
$href = array(
    0 => array("lien" => "", "id" => "", "lib" => "", ),
    1 => array("lien" => "", "id" => "", "lib" => "", ),
    2 => array(
        "lien" => "../scr/form.php?obj=radiation&amp;origin=electeur_radiation&amp;maj=0&amp;idxelecteur=",
        "id" => "&amp;nom=".(isset($nom) ? $nom : "").
                "&amp;exact=".(isset($exact) ? $exact : "").
                "&amp;prenom=".(isset($prenom) ? $prenom : "").
                "&amp;premier=".(isset($premier) ? $premier : "").
                "&amp;datenaissance=".(isset($datenaissance) ? $datenaissance : "").
                "&amp;marital=".(isset($marital) ? $marital : "").
                "&amp;tricol=".(isset($tricol) ? $tricol : "").
                "&amp;premier=".(isset($premier) ? $premier : 0),
        "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix radiation-16\" title=\""._("Creer un mouvement de radiation")."\">"._("Creer un mouvement de radiation")."</span>",
    ),
    3 => array("lien" => "", "id" => "", "lib" => "", ),
);

//
$formulaire = $table.".form";

/**
 * Options
 */
//
$options = array();
$option = array(
    "type" => "search",
    "display" => false,
);
array_push($options, $option);
//
$option = array(
    "type" => "condition",
    "field" => "typecat",
    "case" => array(
        "0" => array(
            "values" => array("modification", ),
            "style" => "modification-encours",
            "href" => array(
                0 => array("lien" => "", "id" => "", "lib" => ""),
                1 => array("lien" => "", "id" => "", "lib" => ""),
                2 => array("lien" => "", "id" => "", "lib" => "", ),
                3 => array("lien" => "", "id" => "", "lib" => "", ),
 
            ),
        ),
        "1" => array(
            "values" => array("radiation", ),
            "style" => "radiation-encours",
            "href" => array(
                0 => array("lien" => "", "id" => "", "lib" => ""),
                1 => array("lien" => "", "id" => "", "lib" => ""),
                2 => array(
                    "lien" => "../scr/form.php?obj=radiation&amp;origin=electeur_radiation&amp;maj=1&amp;idxelecteur=",
                    "id" => "&amp;nom=".(isset($nom) ? $nom : "").
                            "&amp;exact=".(isset($exact) ? $exact : "").
                            "&amp;prenom=".(isset($prenom) ? $prenom : "").
                            "&amp;premier=".(isset($premier) ? $premier : "").
                            "&amp;datenaissance=".(isset($datenaissance) ? $datenaissance : "").
                            "&amp;marital=".(isset($marital) ? $marital : "").
                            "&amp;tricol=".(isset($tricol) ? $tricol : "").
                            "&amp;premier=".(isset($premier) ? $premier : 0),
                    "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix edit-16\" title=\""._("Modifier le mouvement de radiation en cours")."\">"._("Modifier")."</span>",
                ),
                3 => array(
                    "lien" => "../scr/form.php?obj=radiation&amp;origin=electeur_radiation&amp;maj=2&amp;idxelecteur=",
                    "id" => "&amp;ids=1".
                            "&amp;nom=".(isset($nom) ? $nom : "").
                            "&amp;exact=".(isset($exact) ? $exact : "").
                            "&amp;prenom=".(isset($prenom) ? $prenom : "").
                            "&amp;premier=".(isset($premier) ? $premier : "").
                            "&amp;datenaissance=".(isset($datenaissance) ? $datenaissance : "").
                            "&amp;marital=".(isset($marital) ? $marital : "").
                            "&amp;tricol=".(isset($tricol) ? $tricol : "").
                            "&amp;premier=".(isset($premier) ? $premier : 0),
                    "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix delete-16\" title=\""._("Supprimer le mouvement de radiation en cours")."\">"._("Supprimer")."</span>",
                ),
                4 => array(
                    "lien" => "../app/bind-electeur-mouvement.php?bind=attestationradiation&amp;idx_electeur=",
                    "id" => "",
                    "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix pdf-16\" title=\""._("Attestation de radiation")."\">"._("Attestation de radiation")."</span>",
                    "target" => "_blank"
                ),
            ),
        ),
    ),
);
array_push($options, $option);

?>
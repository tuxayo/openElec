<?php
/**
 * Script contenant les requêtes permettant l'export des électeurs
 * 
 * @package openelec
 * @version SVN : $Id$
 *
 */

// Infos de la commune
$sqlcom="SELECT 'MEC' as typefichier,";
$sqlcom.=" c.id as id, c.inseeville as inseeville, 
	liste.liste_insee as typeliste, 1 as nbfichier, 1 as nofichier,";
$sqlcom.=" (SELECT count(*) FROM electeur e WHERE e.collectivite=c.id AND liste='".$_SESSION['liste']."') as total, ";
$sqlcom.=" (SELECT count(*) FROM electeur e WHERE e.collectivite=c.id AND liste='".$_SESSION['liste']."') as totalfichier, ";
$sqlcom.=" false as ack, c.siret as siret, c.ville as ville, false as test,
 c.maire as maire, c.mail as mail, c.num_fax as fax, c.num_tel as tel";

$sqlcom.=" FROM collectivite c, liste";
$sqlcom.=" WHERE c.type_interface=0 AND liste.liste='".$_SESSION['liste']."'";
$sqlcom.=" ORDER BY c.ville;";

// Infos électeurs
$sqlelec = "SELECT electeur.numero_electeur as numero_electeur,
			electeur.nom as nom,
			electeur.nom_usage as nom_usage,
			electeur.prenom as prenomusuel,
			'' as prenom,
			electeur.date_naissance as date_naissance,
			electeur.libelle_lieu_de_naissance as localitenaiss,
			electeur.code_lieu_de_naissance as codelocalitenaiss,
			electeur.libelle_departement_naissance as deptnaiss,
			electeur.code_departement_naissance as codedeptnaiss,
			'' as pays,
			'' as codepays,
			electeur.sexe as sexe,
			nationalite.libelle_nationalite as nationalite,
			'' as pointremise,
			CASE WHEN resident = 'Non'
				THEN electeur.complement
				ELSE electeur.complement_resident
			END as complement,
			CASE WHEN resident = 'Non' 
				THEN electeur.numero_habitation
				ELSE NULL
			END as numerovoie,
			CASE WHEN resident = 'Non'
				THEN electeur.complement_numero
				ELSE ''
			END as extension,
			'' as typevoie,
			CASE WHEN resident = 'Non'
				THEN electeur.libelle_voie
				ELSE electeur.adresse_resident
			END as nomvoie,
			'' as lieudit,
			'' as mentiondistrib,
			'' as cedex,
			'' as libellebureauxedex,
			CASE WHEN resident = 'Non'
				THEN c.cp
				ELSE electeur.cp_resident
			END as codepostal,
			CASE WHEN resident = 'Non'
				THEN c.ville
				ELSE electeur.ville_resident
			END as localite,
			CASE WHEN resident = 'Non'
				THEN c.inseeville
				ELSE ''
			END as codelocalite,
			'' as departement,
			'' as codedepartement,
			'' as paysadresse,
			'' as codepaysadresse,
			centrevote.debut_validite as datedebut,
			centrevote.fin_validite as datefin,
			'' as ambassadeposte,
			'' as libpays,
			'' as codelibpays,
			date_inscription as date_inscription,
			CASE WHEN param_mouvement.codeinscription = '1' THEN 'vol'
				WHEN param_mouvement.codeinscription = '2' THEN 'dj'
				WHEN param_mouvement.codeinscription = '8' THEN 'off'
			END as typeinscription";

$sqlelec.=" FROM electeur";
$sqlelec.=" LEFT OUTER JOIN nationalite ON nationalite.code=electeur.code_nationalite";
$sqlelec.=" LEFT OUTER JOIN collectivite c ON electeur.collectivite=c.id";
$sqlelec.=" LEFT OUTER JOIN centrevote ON electeur.id_electeur=centrevote.id_electeur";
$sqlelec.=" LEFT OUTER JOIN param_mouvement on electeur.code_inscription=param_mouvement.code";
$sqlelec.=" WHERE c.type_interface=0";
$sqlelec.=" ORDER BY c.ville;";
?>
<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
//******************************************************************************
//                  DIFFERENTES ZONES A AFFICHER                              //
//******************************************************************************
//------------------------------------------------------------------------------
//                  COMPTEUR                                                  //
//------------------------------------------------------------------------------
//(0) 1 -> affichage compteur ou 0 ->pas d'affichage
// (1) x  (2) y  (3) width (4) bold 1 ou 0  (5) size ou 0
$champs_compteur=array();
//------------------------------------------------------------------------------
//                  IMAGE                                                     //
//------------------------------------------------------------------------------
//  (0) nom image (1) x  (2) y  (3) width (4) hauteue  (5) type
// $img=array(array('../img/arles.png',1,1,17.6,12.6,'png')
$img=array();
//------------------------------------------------------------------------------
//                  TEXTE                                                     //
//------------------------------------------------------------------------------
// (0) texte (1) x  (2) y  (3) width (4) bold 1 ou 0  (5) size ou 0
$texte=array();
//------------------------------------------------------------------------------
//                  DATA                                                      //
//------------------------------------------------------------------------------
// (0) affichage avant data
// (1) affichage apres data
// (2) tableau X Y Width bold(0 ou 1),size ou 0
// (3) 1 = number_format(champs,0) : 0002->2  /  ou 0
$champs = array(
    'ligne1'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 0),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne2'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 1),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne3'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 2),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne4'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 3),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne5'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 4),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne6'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 5),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0)
);

//******************************************************************************
//                        SQL                                                 //
//******************************************************************************
$sql = " SELECT ";
//
$sql .= " adresse1 AS ligne1, ";
//
$sql .= " adresse2 AS ligne2, ";
//
$sql .= " adresse3 AS ligne3, ";
//
$sql .= " adresse4 AS ligne4, ";
//
$sql .= " adresse5 AS ligne5, ";
//
$sql .= " adresse6 AS ligne6 ";
//
$sql .= " FROM inscription_office ";
//
$sql .= " WHERE collectivite='".$_SESSION['collectivite']."' ";
//
$sql .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
//

?>
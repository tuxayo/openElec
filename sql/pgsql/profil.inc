<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Parametrage")." -> "._("Profil");

// Icone
$ico = "ico_parametrage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 15;

// Critere from de la requete
$table = "profil";

// Critere select de la requete
$champAffiche = array(
    "profil as \""._("Profil")."\"",
    "libelle_profil as \""._("Libelle")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "profil as \""._("Profil")."\"",
    "libelle_profil as \""._("Libelle")."\"",
);

// Critere where de la requete
$selection = "";

// Critere order by ou group by de la requete
$tri = "order by profil";

?>
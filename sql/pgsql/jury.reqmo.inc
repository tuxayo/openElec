<?php
$reqmo['libelle']=" EXPORT JURY D'ASSISES";
$reqmo['sql']="select [civilite],
                      [nom],
                      [prenom],
                      [nom_usage],
                      [sexe],
                      [situation],
                      [to_char(date_naissance,'DD/MM/YYYY') as date_de_naissance],
                      [code_departement_naissance],
                      [libelle_departement_naissance],
                      [code_lieu_de_naissance],
                      [libelle_lieu_de_naissance],
                      [numero_habitation],
                      [libelle_voie],
                      [complement],
                      [resident],
                      [adresse_resident],
                      [complement_resident],
                      [cp_resident],
                      [ville_resident],
                      [provenance],
                      [libelle_provenance],
                      [numero_bureau],
                      [numero_electeur],
                      [liste],
                      [code_canton],
                      [libelle_canton],
                      [profession],
                      [motif_dispense_jury]
from electeur
left join bureau on electeur.code_bureau=bureau.code
    and electeur.collectivite=bureau.collectivite
left join canton on bureau.code_canton=canton.code
where jury=1
and electeur.liste='".$_SESSION['liste']."'
and electeur.collectivite='".$_SESSION['collectivite']."'

order by [TRI]";
$reqmo['TRI']= array('nom',
                     'nom_usage',
                     'prenom'
                     );
//
$reqmo['civilite']="checked";
$reqmo['nom']="checked";
$reqmo['prenom']="checked";
$reqmo['nom_usage']="checked";
$reqmo['sexe']="checked";
$reqmo['situation']="checked";
$reqmo['date_de_naissance']="checked";
$reqmo['code_departement_naissance']="checked";
$reqmo['libelle_departement_naissance']="checked";
$reqmo['code_lieu_de_naissance']="checked";
$reqmo['libelle_lieu_de_naissance']="checked";
$reqmo['numero_habitation']="checked";
$reqmo['libelle_voie']="checked";
$reqmo['complement']="checked";
$reqmo['resident']="checked";
$reqmo['adresse_resident']="checked";
$reqmo['complement_resident']="checked";
$reqmo['cp_resident']="checked";
$reqmo['ville_resident']="checked";
$reqmo['provenance']="checked";
$reqmo['libelle_provenance']="checked";
$reqmo['numero_bureau']="checked";
$reqmo['numero_electeur']="checked";
$reqmo['liste']="checked";
$reqmo['code_canton']="checked";
$reqmo['libelle_canton']="checked";
$reqmo['profession']="checked";
$reqmo['motif_dispense_jury']="checked";

?>
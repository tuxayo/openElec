<?php
/**
 * $Id$
 *
 */

//
$DEBUG = 0;

// Nombre d'enregistrements par page
$serie = 15;

// Titre
$ent = "Xml -> Partenaires";

// Icone
$ico = "ico_parametrage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere from de la requete
$table = "xml_partenaire";

// Critere select de la requete
$champAffiche = array
(
    "xml_partenaire as \""._("Id")."\"",
    "type_partenaire as \""._("Type")."\"",
    "siret as \""._("SIRET")."\"",
    "nom as \""._("Nom")."\"",
    "contact_nom as \""._("Nom du contact")."\"",
    "contact_mail as \""._("Mail du contact")."\""
);

// Champ sur lesquels la recherche est active
$champRecherche = array ();

// Critere where de la requete
$selection = "where collectivite='".$_SESSION['collectivite']."' ";

// Critere order by ou group by de la requete
$tri = "order by type_partenaire desc";

?>
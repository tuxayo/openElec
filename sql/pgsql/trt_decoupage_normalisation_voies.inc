<?php
/**
 * Ce fichier contient les requêtes utilisées lors de la normalisation des voies
 *
 * @package openelec
 * @version SVN : $Id$
 */

$sql_voies = "SELECT * FROM ".DB_PREFIXE."voie WHERE code_collectivite = '".$_SESSION['collectivite']."' ";
$sql_voies .= "ORDER BY libelle_voie ASC;";

$sql_voie_infos = "SELECT * FROM ".DB_PREFIXE."voie WHERE code = '<id>';";

$sql_next_voie_id = "SELECT max(to_number(code, '999999')) FROM ".DB_PREFIXE."voie;";

$sql_update_electeurs = "SELECT id_electeur FROM ".DB_PREFIXE."electeur WHERE code_voie = '<old_code>';";

$sql_update_mouvements = "SELECT id, id_electeur FROM ".DB_PREFIXE."mouvement WHERE code_voie = '<old_code>';";

$sql_select_old_voies = "SELECT code FROM ".DB_PREFIXE."voie WHERE code = '<old_code>';";

$sql_delete_old_voies = "DELETE FROM ".DB_PREFIXE."voie WHERE code = '<old_code>';";
?>
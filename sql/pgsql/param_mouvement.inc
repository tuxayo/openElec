<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Parametrage")." -> "._("Mouvements");

// Icone
$ico = "ico_parametrage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 15;

// Critere from de la requete
$table = "param_mouvement";

// Critere select de la requete
$champAffiche = array(
    "code as \""._("Code")."\"",
    "libelle as \""._("Libelle")."\"",
    "typecat as \""._("Categorie")."\"",
    "effet as \""._("Date d'effet")."\"",
    "(codeinscription || coderadiation) as \""._("Transfert INSEE")."\"",
    "case edition_carte_electeur 
        when '0' then 'Jamais' 
        when '1' then 'Toujours'
        when '2' then 'Lors de changement de bureau'
        else '' 
    end as \""._("Impression")."\"",
    "to_char(param_mouvement.om_validite_debut ,'DD/MM/YYYY') as \""._("om_validite_debut")."\"",
    "to_char(param_mouvement.om_validite_fin ,'DD/MM/YYYY') as \""._("om_validite_fin")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "code as \""._("Code")."\"",
    "libelle as \""._("Libelle")."\"",
    "typecat as \""._("Categorie")."\"",
    "effet as \""._("Date d'effet")."\"",
    "cnen as \""._("Transfert INSEE")."\"",
    "codeinscription as \""._("INSEE - Code INS")."\"",
    "coderadiation as \""._("INSEE - Code RAD")."\"",
);

// Critere where de la requete
$selection = "";

// Critere order by ou group by de la requete
$tri = "order by libelle";

?>
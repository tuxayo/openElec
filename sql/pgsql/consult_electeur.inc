<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */


//
$DEBUG = 0;

// Nombre d'enregistrements par page
$serie = 12;

// Titre
$ent = _("Consultation")." -> "._("Liste electorale");

// Icone
$ico = "ico_consultation.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

//
if ($f->isMulti() == true) {
    
    // Critere from de la requete
    $table = "electeur inner join collectivite on electeur.collectivite = collectivite.id";
    
    // Critere select de la requete
    $champAffiche = array (
        "id_electeur as \""._("Id Electeur")."\"",
        "nom as \""._("Nom")."\"",
        "prenom as \""._("Prenom")."\"",
        "nom_usage as \""._("Nom d'usage")."\"",
        "(to_char(date_naissance,'DD/MM/YYYY')||'<br />"._("a")." '||libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')') as \""._("Date et lieu de naissance")."\"",
        "case resident
                when 'Non' then (
            case numero_habitation > 0
                when True then (numero_habitation || ' ' || complement_numero || ' ' || electeur.libelle_voie)
                when False then electeur.libelle_voie
            end)
                when 'Oui' then adresse_resident
            end || '<br />' ||case resident
                when 'Non' then electeur.complement
                when 'Oui' then (complement_resident || ' ' || cp_resident || ' - ' || ville_resident)
            end as \""._("Adresse")."\"",
        "ville as \""._("Ville")."\"",
    );
    
    // Champ sur lesquels la recherche est active
    $champRecherche = array(
        "id_electeur as \""._("Id Electeur")."\"",
        "nom as \""._("Nom")."\"",
        "prenom as \""._("Prenom")."\"",
        "nom_usage as \""._("Nom d'usage")."\"",
        "ville as \""._("Ville")."\"",
        "date_naissance as \""._("Date de naissance")."\"",
        "bureauforce as \""._("bureau force")."\"",
    );
    
    // Critere where de la requete
    $selection = " where liste='".$_SESSION['liste']."' ";
    
    // Critere order by ou group by de la requete
    $tri = " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
    
    //
    $href [0]['lien'] = "";
    $href [0]['id'] = "";
    $href [0]['lib'] = "";
    $href [1]['lien'] = "";
    $href [1]['id'] = "";
    $href [1]['lib'] = "";
    $href [2]['lien'] = "";
    $href [2]['id'] = "";
    $href [2]['lib'] = "";
} else {
    
    // Critere from de la requete
    $table = "electeur";
    
    // Critere select de la requete
    $champAffiche = array(
        "id_electeur as \""._("Id Electeur")."\"",
        "nom as \""._("Nom")."\"",
        "prenom as \""._("Prenom")."\"",
        "nom_usage as \""._("Nom d'usage")."\"",
        "to_char(date_naissance,'DD/MM/YYYY') as \""._("Date de naissance")."\"",
        "libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')' as \""._("Lieu de naissance")."\"",
        "case resident 
                when 'Non' then (
            case numero_habitation > 0
                when True then (numero_habitation || ' ' || complement_numero || ' ' || electeur.libelle_voie)
                when False then electeur.libelle_voie
            end)
                when 'Oui' then adresse_resident 
            end || '<br />' ||case resident
                when 'Non' then electeur.complement 
                when 'Oui' then (complement_resident || ' ' || cp_resident || ' - ' || ville_resident) 
            end as \""._("Adresse")."\"",
        "code_bureau as \""._("Bureau")."\"",
        "typecat as \""._("En cours")."\""
    );
    
    // Champ sur lesquels la recherche est active
    $champRecherche = array(
        "id_electeur as \""._("Id Electeur")."\"",
        "nom as \""._("Nom")."\"",
        "prenom as \""._("Prenom")."\"",
        "nom_usage as \""._("Nom d'usage")."\"",
        "code_bureau as \""._("Bureau")."\"",
        "date_naissance as \""._("Date de naissance")."\"",
        "bureauforce as \""._("bureau force")."\"",
    );
    
    // Critere where de la requete
    $selection = " where liste='".$_SESSION['liste']."' AND collectivite = '".$_SESSION['collectivite']."' ";
    //
    $href [0]['lien'] = "";
    $href [0]['id'] = "";
    $href [0]['lib'] = "";
    $href [1]['lien'] = "../app/electeur.view.php?id_electeur=";
    $href [1]['id'] = "&amp;premier=".$premier."&amp;recherche=".$recherche."&amp;tricol=".$tricol."&amp;selectioncol=".$selectioncol;
    $href [1]['lib'] = "<span class=\"om-icon om-icon-16 om-icon-fix ficheelecteur-16\" title=\""._("Fiche de l'electeur")."\">"._("Fiche de l'electeur")."</span>";
    $href [2]['lien'] = "";
    $href [2]['id'] = "";
    $href [2]['lib'] = "";

    // Critere order by ou group by de la requete
    $tri = " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
}


/**
 * Options
 */
//
$options = array();
//
$option = array(
    "type" => "condition",
    "field" => "typecat",
    "case" => array(
        "0" => array(
            "values" => array("modification", ),
            "style" => "modification-encours",
        ),
        "1" => array(
            "values" => array("radiation", ),
            "style" => "radiation-encours",
        ),
    ),
);
array_push($options, $option);
//
$option = array(
    "type" => "pagination_select",
    "display" => false,
);
array_push($options, $option);

?>

<?php
/**
 *
 */

/**
 * Cette requete permet de lister tous les bureaux en fonction de la
 * collectivite en cours
 * 
 * @param string $_SESSION['collectivite']
 */
$query_select_bureau = "select * from bureau ";
$query_select_bureau .= "where collectivite='".$_SESSION['collectivite']."' ";
$query_select_bureau .= "order by code ";

if (isset($row_select_bureau['code'])) {
    /**
     * Cette requete permet de compter tous les electeurs de la table electeur en
     * fonction de la collectivite en cours, de la liste en cours et du bureau
     * selectionne
     *
     * @param string $row_select_bureau['code'] Code du bureau
     * @param string $_SESSION['collectivite']
     * @param string $_SESSION['liste']
     */
    $query_count_electeur_bureau = "select count(*) ";
	$query_count_electeur_bureau .= "from electeur ";
	$query_count_electeur_bureau .= "where liste='".$_SESSION['liste']."' ";
	$query_count_electeur_bureau .= "and collectivite='".$_SESSION['collectivite']."' ";
	$query_count_electeur_bureau .= "and code_bureau='".$row_select_bureau['code']."' ";
    
    /**
     * Cette requete permet de compter tous les electeurs de la table electeur en
     * fonction de la collectivite en cours, de la liste en cours et du bureau
     * selectionne et de sexe Masculin
     *
     * @param string $row_select_bureau['code'] Code du bureau
     * @param string $_SESSION['collectivite']
     * @param string $_SESSION['liste']
     */
    $query_count_electeur_homme_bureau = $query_count_electeur_bureau;
    $query_count_electeur_homme_bureau .= "and sexe='M' ";
    
    /**
     * Cette requete permet de compter tous les electeurs de la table electeur en
     * fonction de la collectivite en cours, de la liste en cours et du bureau
     * selectionne et de sexe Feminin
     *
     * @param string $row_select_bureau['code'] Code du bureau
     * @param string $_SESSION['collectivite']
     * @param string $_SESSION['liste']
     */
    $query_count_electeur_femme_bureau = $query_count_electeur_bureau;
    $query_count_electeur_femme_bureau .= "and sexe='F' ";
}

?>
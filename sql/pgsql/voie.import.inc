<?php
// $Id: import_voie.inc,v 1.1 2006-09-08 19:43:30 fraynaud Exp $

//==============================================================================
// voie
// =============================================================================
$import= "voie => insert en table voie";
$table= "voie";
$id=""; // pas de numerotation automatique
// admin======================================================================
// verrou= 1 mise a jour de la base
//       = 0 pas de mise a jour  => phase de test
// debug=1 affichage des enregistrements à l ecran
//      =0 pas d affichage
// =============================================================================
$verrou=1;// =0 pas de mise a jour de la base / =1 mise à jour
$DEBUG=0; // =0 pas d affichage messages / =1 affichage detail enregistrement
$fic_erreur=1; // =0 pas de fichier d erreur / =1  fichier erreur
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
                // contenant les enregistrements en erreur
// 1ere ligne ==================================================================
// la premiere ligne contient les noms de champs
// 1=oui
// 0=non
// =============================================================================
$ligne1=1;
// parametrage des controles ===================================================
// zone obligatoire
$obligatoire['code']=1;  // conseille = 1
$obligatoire['libelle_voie']=1;// conseille = 1
$obligatoire['cp']=1;// conseille = 1
$obligatoire['ville']=1;// conseille = 1
//  zone à inserer ============================================================
// insertion d un enregistrement dans la table mouvement
// liste des zones à inserer
// mettre en commentaire les zones non traitées
// =============================================================================
$zone['code']='0';
$zone['libelle_voie']='1';
$zone['cp']='2';
$zone['ville']='3';
$zone['code_collectivite']='';
// =============================================================================
// valeur par defaut
// si les zones ne sont pas dans le fichier CSV   zone[champ]=''
// =============================================================================
//      $defaut['cp']='13200';
  //    $defaut['ville']='Arles';
$defaut['code_collectivite'] = $_SESSION['collectivite'];

?>

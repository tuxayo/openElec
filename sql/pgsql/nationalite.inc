<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Decoupage")." -> "._("Nationalite");

// Icone
$ico = "ico_cnen.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 15;

// Critere from de la requete
$table = "nationalite";

// Critere select de la requete
$champAffiche = array(
    "code as \""._("Code")."\"",
    "libelle_nationalite as \""._("Libelle")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "code as \""._("Code")."\"",
    "libelle_nationalite as \""._("Libelle")."\"",
);

// Critere where de la requete
$selection = "";

// Critere order by ou group by de la requete
$tri = " order by libelle_nationalite";

?>
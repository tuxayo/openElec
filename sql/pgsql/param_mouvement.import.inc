<?php
// $Id: import_param_mouvement.inc,v 1.1 2006-09-08 19:43:30 fraynaud Exp $

//==============================================================================
// voie
// =============================================================================
$import= "parametrage mouvement => insert en table param_mouvement";
$table= "param_mouvement";
$id=""; // pas de numerotation automatique
// admin======================================================================
// verrou= 1 mise a jour de la base
//       = 0 pas de mise a jour  => phase de test
// debug=1 affichage des enregistrements à l ecran
//      =0 pas d affichage
// =============================================================================
$verrou=1;// =0 pas de mise a jour de la base / =1 mise à jour
$DEBUG=0; // =0 pas d affichage messages / =1 affichage detail enregistrement
$fic_erreur=1; // =0 pas de fichier d erreur / =1  fichier erreur
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
                // contenant les enregistrements en erreur
// 1ere ligne ==================================================================
// la premiere ligne contient les noms de champs
// 1=oui
// 0=non
// =============================================================================
$ligne1=0;
// parametrage des controles ===================================================
// zone obligatoire
$obligatoire['code']=1;  // conseille = 1
$obligatoire['libelle']=1;// conseille = 1
$obligatoire['typecat']=1;// conseille = 1
//  zone à inserer ============================================================
// insertion d un enregistrement dans la table mouvement
// liste des zones à inserer
// mettre en commentaire les zones non traitées
// =============================================================================
      $zone['code']='0';
      $zone['libelle']='1';
      $zone['typecat']='2';
      $zone['effet']='3';
      $zone['cnen']='4';
      $zone['codeinscription']='5';
      $zone['coderadiation']='6';
// =============================================================================
// valeur par defaut
// si les zones ne sont pas dans le fichier CSV   zone[champ]=''
// =============================================================================
    //  $defaut['cnen']='Non';
    //  $defaut['effet']='1erMars';
?>
<?php
$DEBUG=0;
// ------------------------document---------------------------------------------
$orientation="P";// orientation P-> portrait L->paysage
$format="A4";// format A3 A4 A5
$police='arial';
$margeleft=10;// marge gauche
$margetop=5;// marge haut
$margeright=5;//  marge droite
$border=1; // 1 ->  bordure 0 -> pas de bordure
$C1="0";// couleur texte  R
$C2="0";// couleur texte  V
$C3="0";// couleur texte  B
//-------------------------LIGNE tableau----------------------------------------
$size=9; //taille POLICE
$height=4; // -> hauteur ligne tableau
$align='L';
$fond=1;// 0- > FOND transparent 1 -> fond
$C1fond1="241";// couleur fond  R
$C2fond1="241";// couleur fond  V
$C3fond1="241";// couleur fond  B
$C1fond2="255";// couleur fond  R
$C2fond2="255";// couleur fond  V
$C3fond2="255";// couleur fond  B
//-------------------------- titre----------------------------------------------
$libtitre=_("Decoupage electoral des voies"); // libelle titre
$flagsessionliste=0;// 1 - > affichage session liste ou 0 -> pas d'affichage
$bordertitre=0; // 1 ->  bordure 0 -> pas de bordure
$aligntitre='L'; // L,C,R
$heightitre=10;// hauteur ligne titre
$grastitre="";//$gras="B" -> BOLD OU $gras=""
$fondtitre=0; //0- > FOND transparent 1 -> fond
$C1titrefond="181";// couleur fond  R
$C2titrefond="182";// couleur fond  V
$C3titrefond="188";// couleur fond  B
$C1titre="75";// couleur texte  R
$C2titre="79";// couleur texte  V
$C3titre="81";// couleur texte  B
$sizetitre="12";
//--------------------------libelle entete colonnes-----------------------------
$flag_entete=1;//entete colonne : 0 -> non affichage , 1 -> affichage
$fondentete=1;// 0- > FOND transparent 1 -> fond
$heightentete=10;//hauteur ligne entete colonne
$C1fondentete="210";// couleur fond  R
$C2fondentete="216";// couleur fond  V
$C3fondentete="249";// couleur fond  B
$C1entetetxt="0";// couleur texte R
$C2entetetxt="0";// couleur texte V
$C3entetetxt="0";// couleur texte B
//------ Borderentete colonne $be0  à $be.. ( $be OBLIGATOIRE )
$be0="L";
$be1="L";
$be2="L";
$be3="L";
$be4="L";
$be5="L";
$be6="LR";
// ------ couleur border--------------------------------------------------------
$C1border="159";// couleur texte  R
$C2border="160";// couleur texte  V
$C3border="167";// couleur texte  B
//------ Border cellule colonne $b0  à $b.. ( $b OBLIGATOIRE )
$b0="L";
$b1="L";
$b2="L";
$b3="L";
$b4="L";
$b5="L";
$b6="LR";
//------ ALIGNEMENT entete colonne $ae0  à $ae.. ( $ae OBLIGATOIRE )
$ae0="C";
$ae1="L";
$ae2="L";
$ae3="C";
$ae4="C";
$ae5="C";
$ae6="C";
//------ largeur de chaque colonne $l0  à $l.. ( $l OBLIGATOIRE )---------------
$l0=9;
$l1=57;
$l2=57;
$l3=16;
$l4=16;
$l5=16;
$l6=16;
$widthtableau=187;// -> ajouter $l0 à $lxx
$bt=1;// border 1ere  et derniere ligne  dutableau par page->0 ou 1
//------ ALIGNEMENT de chaque colonne $l0  à $a.. ( $a OBLIGATOIRE )------------
$a0="C";
$a1="L";
$a2="L";
$a3="L";
$a4="L";
$a5="L";
$a6="L";
//--------------------------SQL-------------------------------------------------
$sql = "
SELECT
    decoupage.id,
    voie.libelle_voie as voie,
    (decoupage.code_bureau||' - '||bureau.libelle_bureau) as bureau,
    premier_pair as \"pair >\",
    dernier_pair as \"< pair\",
    premier_impair as \"impair >\",
    dernier_impair as \"< impair\"
FROM
".DB_PREFIXE."decoupage
JOIN ".DB_PREFIXE."voie ON decoupage.code_voie=voie.code
JOIN ".DB_PREFIXE."bureau ON decoupage.code_bureau=bureau.code
WHERE
    voie.code_collectivite = '".$_SESSION['collectivite']."' AND
    bureau.collectivite = '".$_SESSION['collectivite']."'
ORDER BY
    libelle_voie, code_bureau
";
//------------------------------------------------------------------------------
?>

<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Parametrage")." -> "._("Liste");

// Icone
$ico = "ico_parametrage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 10;

// Critere from de la requete
$table = "liste";

// Critere select de la requete
$champAffiche = array(
    "liste as \""._("Code")."\"",
    "libelle_liste as \""._("Libelle")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "liste as \""._("Code")."\"",
    "libelle_liste as \""._("Libelle")."\"",
);

// Critere where de la requete
$selection = "";

// Critere order by ou group by de la requete
$tri = "order by liste";

?>
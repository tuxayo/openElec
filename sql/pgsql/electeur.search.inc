<?php
/**
 * Fichier de configuration pour le module recherche (obj/search.class.php)
 *
 * @package opencimetiere
 * @version CVS : $Id$
 */

///// definitions des types de recherche
// $conf = array(
//    "libelle" => lang("Recherche globale"),
//    "champAffiche" => array(), /// liste des champs a afficher
//    "champRecherche" => array(), /// champs de recherche 
//    "table" => "", // tables
//    "serie" => 0, // limit (facultatif - par defaut 100 )
//    "selection" => "", // where
//    "tri" => "", // order by (facultatif)
//    "href" => "" // lien (facultatif)
// );
// array_push( $configTypeRecherche, $conf);

$configTypeRecherche = array();

///// Module Recherche Globale
$conf = array(
    "libelle" => _("Recherche globale"),
    "champAffiche" => "#",
    "champRecherche" => "",
    "table" => "",
    "serie" => 0,
    "selection" => "",
    "tri" => "",
    "href" => ""
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => _("Electeur"),
    "obj" => "consult_electeur",
    "default" => "true"
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => _("Procuration"),
    "obj" => "procuration"
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => _("Inscription"),
    "obj" => "inscription"
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => _("Modification"),
    "obj" => "modification"
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => _("Radiation"),
    "obj" => "radiation"
);
array_push( $configTypeRecherche, $conf);





?>
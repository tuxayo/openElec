<?php
//03/09/2013   17:51:40
$etat['orientation']="P";
$etat['format']="A4";
$etat['footerfont']="helvetica";
$etat['footerattribut']="I";
$etat['footertaille']="8";
$etat['logo']="logopdf.png";
$etat['logoleft']="10";
$etat['logotop']="10";
$etat['titre']="                                                                                                [civnomprenom_mandataire]
                                                                                                [adresse_mandataire]
                                                                                                [complement_mandataire]
                                                                                                [cpville_mandataire]";
$etat['titreleft']="14";
$etat['titretop']="30";
$etat['titrelargeur']="170";
$etat['titrehauteur']="5";
$etat['titrefont']="arial";
$etat['titreattribut']="";
$etat['titretaille']="10";
$etat['titrebordure']="0";
$etat['titrealign']="J";
$etat['corps']="Objet : Vote par procuration


[civ_mandataire],
Nous vous informons que [civnomprenom_mandant] a fait établir une procuration le [date_procu] par l'autorité : [origine_procu],
vous désignant en qualité de mandataire.
Celle-ci ne peut être prise en compte pour le motif suivant :

[motif_refus]

Nous restons à votre disposition,
et vous prions d'agréer, [civ_mandataire], l'assurance de nos salutations distinguées.


                                                                                                À £ville
                                                                                                Le £aujourdhui";
$etat['corpsleft']="14";
$etat['corpstop']="100";
$etat['corpslargeur']="170";
$etat['corpshauteur']="5";
$etat['corpsfont']="times";
$etat['corpsattribut']="";
$etat['corpstaille']="10";
$etat['corpsbordure']="0";
$etat['corpsalign']="J";
$etat['sql']="SELECT 
CASE mandataire.civilite 
	WHEN 'M.' THEN 'Monsieur'
	WHEN 'Mlle' THEN 'Mademoiselle' 
	WHEN 'Mme' THEN 'Madame' 
END as civ_mandataire, 
mandataire.civilite||' '||(CASE WHEN trim(mandataire.nom_usage) <> '' AND mandataire.nom_usage IS NOT NULL THEN mandataire.nom_usage ELSE mandataire.nom END)||' '||mandataire.prenom as civnomprenom_mandataire,
mandant.civilite||' '||(CASE WHEN trim(mandant.nom_usage) <> '' AND mandant.nom_usage IS NOT NULL THEN mandant.nom_usage ELSE mandant.nom END)||' '||mandant.prenom as civnomprenom_mandant,
CASE mandataire.resident 
	WHEN 'Non' THEN ( CASE mandataire.numero_habitation 
					WHEN 0 THEN '' ELSE (mandataire.numero_habitation||' ') END || 
					CASE mandataire.complement_numero WHEN '' THEN '' ELSE (mandataire.complement_numero||' ') END ||
				mandataire.libelle_voie)
	WHEN 'Oui' THEN mandataire.adresse_resident
END AS adresse_mandataire,
CASE mandataire.resident 
	WHEN 'Non' THEN mandataire.complement 
	WHEN 'Oui' THEN mandataire.complement_resident
END AS complement_mandataire,
CASE mandataire.resident 
	WHEN 'Non' THEN (voie.cp||' '||voie.ville)
	WHEN 'Oui' THEN (mandataire.cp_resident||' '||mandataire.ville_resident)
END AS cpville_mandataire ,
to_char(procuration.date_modif,'DD/MM/YYYY') as date_procu,
trim(procuration.origine1||' '||procuration.origine2) as origine_procu,
motif_refus 
FROM procuration
INNER JOIN electeur as mandataire ON procuration.mandataire=mandataire.id_electeur
INNER JOIN electeur as mandant ON procuration.mandant=mandant.id_electeur
INNER JOIN voie ON mandataire.code_voie = voie.code
WHERE procuration.id_procuration =  £idx";
$etat['sousetat']=array();
$etat['se_font']="helvetica";
$etat['se_margeleft']="8";
$etat['se_margetop']="5";
$etat['se_margeright']="5";
$etat['se_couleurtexte']=array("0","0","0");
?>

<?php
/**
 * $Id$
 *
 */
//
$sql = "select ";
$sql .= "(nom || ' - ' || prenom) as nomprenom, ";
$sql .= "nom_usage, ";
$sql .= "to_char(date_naissance,'DD/MM/YYYY') as naissance, ";
$sql .= "(substring(libelle_lieu_de_naissance from 0 for 44) || ' (' || code_departement_naissance || ')') as lieu, ";
$sql .= "case liste ";
    $sql .= "when '01' then '' ";
    $sql .= "else libelle_nationalite ";
$sql .= "end as nationalite, ";
$sql .= "case resident ";
    $sql .= "when 'Non' then (";
        $sql .= "case numero_habitation > 0 ";
            $sql .= "when True then (numero_habitation || ' ' || complement_numero || ' ' || electeur.libelle_voie) ";
            $sql .= "when False then electeur.libelle_voie ";
        $sql .= "end) ";
    $sql .= "when 'Oui' then adresse_resident ";
$sql .= "end as adresse, ";
$sql .= "case resident ";
    $sql .= "when 'Non' then electeur.complement ";
    $sql .= "when 'Oui' then (complement_resident || ' ' || cp_resident || ' - ' || ville_resident) ";
$sql .= "end as complement, ";
$sql .= "'' as blank2, ";
$sql .= "'' as blank3, ";
$sql .= "numero_electeur, ";
$sql .= "code_bureau, ";
$sql .= "numero_bureau ";
$sql .= "from electeur ";
$sql .= "left join nationalite ";
$sql .= "on electeur.code_nationalite=nationalite.code ";
$sql .= "where liste='".$_SESSION ['liste']."' AND electeur.collectivite = '".$_SESSION['collectivite']."' ";

if (isset ($nobureau))
    $sql .= "and code_bureau='".$nobureau."' ";

$sql .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
?>
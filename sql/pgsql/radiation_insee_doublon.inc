<?php
/**
 *
 */


/**
 * Cette requete permet de recuperer les informations de l'electeur du tableau
 * "radiation_insee"
 */
$query_radiation_electeur = "select * from radiation_insee where id=".$idrad;


/**
 * Cette requete permet de lister les electeurs correspondants au nom, prenom
 * et a la date de naissance donnee en fonction de
 * la collectivite en cours et de la liste en cours
 * 
 * @param string $nom Nom patronymique
 * @param string $prenom Prenoms
 * @param string $datenaissance format DD/MM/YYYY
 * @param string $_SESSION['collectivite']
 * @param string $_SESSION['liste']
 */
$nom = mb_strtolower($nom, 'UTF-8');
$nom = str_replace(
    array(
        '-', '\'',
        'à', 'â', 'ä', 'á', 'ã', 'å',
        'î', 'ï', 'ì', 'í',
        'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
        'ù', 'û', 'ü', 'ú',
        'é', 'è', 'ê', 'ë',
        'ç', 'ÿ', 'ñ',
        ),
    array(
        ' ', ' ', 
        'a', 'a', 'a', 'a', 'a', 'a',
        'i', 'i', 'i', 'i',
        'o', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'u',
        'e', 'e', 'e', 'e',
        'c', 'y', 'n',
        ),
$nom
);
$prenom = mb_strtolower($prenom, 'UTF-8');
$prenom = str_replace(
    array(
        '-', '\'',
        'à', 'â', 'ä', 'á', 'ã', 'å',
        'î', 'ï', 'ì', 'í',
        'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
        'ù', 'û', 'ü', 'ú',
        'é', 'è', 'ê', 'ë',
        'ç', 'ÿ', 'ñ',
        ),
    array(
        ' ', ' ', 
        'a', 'a', 'a', 'a', 'a', 'a',
        'i', 'i', 'i', 'i',
        'o', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'u',
        'e', 'e', 'e', 'e',
        'c', 'y', 'n',
        ),
$prenom
);
$query_doublon_electeur = "select e.id_electeur, e.nom, e.prenom, e.code_bureau, ";
$query_doublon_electeur .= "to_char(e.date_naissance,'DD/MM/YYYY') as Naissance ";
$query_doublon_electeur .= "from electeur as e ";
$query_doublon_electeur .= "where ";
$query_doublon_electeur .= " lower(translate(nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'-''àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','  aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '"
    .$nom."'";
$query_doublon_electeur .= " and e.date_naissance = '".$datenaissance."'";

$query_doublon_electeur .= "and e.collectivite = '".$_SESSION['collectivite']."' ";
$query_doublon_electeur .= "and e.liste = '".$_SESSION['liste']."' ";
$query_doublon_electeur .= " order by withoutaccent(lower(e.nom)), withoutaccent(lower(e.prenom)) ";

/**
 * Cette requete permet de lister les radiations correspondantes au nom,
 * prenom et a la date de naissance donnee en fonction de
 * la collectivite en cours et de la liste en cours
 * 
 * @param string $nom Nom patronymique
 * @param string $prenom Prenoms
 * @param string $datenaissance  DD/MM/YYYY
 * @param string $_SESSION['collectivite']
 * @param string $_SESSION['liste']
 */

$query_doublon_mouvement = "select m.nom, m.prenom, m.code_bureau, ";
$query_doublon_mouvement .= "to_char(m.date_naissance,'DD/MM/YYYY') as Naissance ";
$query_doublon_mouvement .= "from mouvement as m ";
$query_doublon_mouvement .= "inner join param_mouvement as pm ";
$query_doublon_mouvement .= "on m.types=pm.code ";
$query_doublon_mouvement .= "where ";

$query_doublon_mouvement .= " lower(translate(m.nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'-''àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','  aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '"
    .$nom."'";
$query_doublon_mouvement .= " and m.date_naissance = '".$datenaissance."'";

$query_doublon_mouvement .= "and m.etat='actif' ";
$query_doublon_mouvement .= "and pm.typecat='Radiation' ";
$query_doublon_mouvement .= "and m.collectivite = '".$_SESSION['collectivite']."' ";
$query_doublon_mouvement .= "and m.liste = '".$_SESSION['liste']."' ";
$query_doublon_mouvement .= " order by withoutaccent(lower(m.nom)), withoutaccent(lower(m.prenom)) ";

?>

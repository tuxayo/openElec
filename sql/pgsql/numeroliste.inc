<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Parametrage")." -> "._("Numero Liste");

// Icone
$ico = "ico_parametrage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 15;

// Critere from de la requete
$table = " numeroliste as nl";

// Critere select de la requete
$champAffiche = array (
    "nl.id",
    "nl.liste",
    "nl.dernier_numero",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "nl.liste",
);

// Critere where de la requete
$selection = "where nl.collectivite = '".$_SESSION['collectivite']."'";

// Critere order by ou group by de la requete
$tri = "order by nl.id";

?>
<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

$sql_decoupage_init_decoupage = "
INSERT INTO ".DB_PREFIXE."decoupage (id, code_bureau, code_voie, premier_impair, dernier_impair, premier_pair, dernier_pair)
    SELECT
        nextval('".DB_PREFIXE."decoupage_seq'),
        (SELECT DISTINCT code_bureau from ".DB_PREFIXE."electeur where electeur.code_voie = voie.code),
        voie.code,
        1,99999,0,99998
    FROM ".DB_PREFIXE."voie
    WHERE voie.code in (
        --liste des voies dont tous les electeurs sont dans le meme bureau
        SELECT code
        FROM (
            SELECT DISTINCT
            voie.code,
            electeur.code_bureau
            FROM ".DB_PREFIXE."electeur
 LEFT OUTER JOIN ".DB_PREFIXE."voie ON electeur.code_voie = voie.code
 LEFT OUTER JOIN ".DB_PREFIXE."decoupage ON voie.code = decoupage.code_voie
 WHERE voie.code_collectivite = '".$_SESSION["collectivite"]."' AND decoupage.id IS NULL
 GROUP BY voie.code, electeur.code_bureau
 ) AS voie_bureau
 GROUP BY code
 HAVING count(code_bureau) =1
 );
";

?>
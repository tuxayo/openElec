<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Parametrage")." -> "._("Utilisateur");

// Icone
$ico = "ico_parametrage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 15;

// Critere from de la requete
$table = "utilisateur inner join profil ";
$table .= "on utilisateur.profil=profil.profil ";
$table .= " inner join collectivite ";
$table .= "on utilisateur.collectivite = collectivite.id ";

// Critere select de la requete
$champAffiche = array(
    "idutilisateur as \""._("Id")."\"",
    "nom as \""._("Nom de l'utilisateur")."\"",
    "login as \""._("Identifiant")."\"",
    "email as \""._("Courriel")."\"",
    "libelle_profil as \""._("Profil")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "idutilisateur as \""._("Id")."\"",
    "nom as \""._("Nom de l'utilisateur")."\"",
    "login as \""._("Identifiant")."\"",
    "email as \""._("Courriel")."\"",
    "libelle_profil as \""._("Profil")."\"",
);

// Critere where de la requete
$selection = " where utilisateur.collectivite='".$_SESSION['collectivite']."'";

// Critere order by ou group by de la requete
$tri = "order by withoutaccent(lower(utilisateur.nom)) ";

/**
 * MULTI
 */
if ($f->isMulti() == true) {
    // Critere select de la requete
    array_push($champAffiche, "collectivite.ville as \""._("Collectivite")."\"");
    
    // Champ sur lesquels la recherche est active
    array_push($champRecherche, "collectivite.ville as \""._("Collectivite")."\"");
    
    // Critere where de la requete
    $selection = "";
}

?>
<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Nombre d'enregistrements par page
$serie = 20;

// Titre
$ent = _("Electeur")." -> "._("Centre de vote");

// Icone
$ico = "ico_centrevote.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere from de la requete
$table = "centrevote as c, electeur as a ";

// Critere select de la requete
$champAffiche = array( 
    "idcentrevote as \""._("id")."\"",
    "(a.nom||' '||a.prenom||' '||a.code_bureau) as \""._("electeur")."\"",
    "to_char(c.debut_validite,'DD/MM/YYYY') as \""._("debut")."\"",
    "to_char(c.fin_validite,'DD/MM/YYYY') as \""._("fin")."\""
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "a.nom",
    "a.prenom",
    "a.code_bureau",
    "*electeur"
);

// Critere where de la requete
$selection = " where c.id_electeur = a.id_electeur and a.collectivite = '".$_SESSION['collectivite']."' ";

// Critere order by ou group by de la requete
$tri = " order by withoutaccent(lower(a.nom)), withoutaccent(lower(a.prenom)) ";

?>
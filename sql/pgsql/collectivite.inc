<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Parametrage")." -> "._("Collectivite");

// Icone
$ico = "ico_parametrage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "collectivite";

// Nombre d'enregistrements par page
$serie = 20;

// Critere from de la requete
$table = "collectivite";

// Critere select de la requete
$champAffiche = array(
    "id as \""._("Id")."\"",
    "ville as \""._("Collectivite")."\"",
    "maire as \""._("Maire")."\"",
    "to_char(datetableau,'DD/MM/YYYY') as \""._("Date de tableau")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "id as \""._("Id")."\"",
    "ville as \""._("Collectivite")."\"",
    "maire as \""._("Maire")."\"",
);

// Critere where de la requete
$selection = "where id='".$_SESSION['collectivite']."'";

// Critere order by ou group by de la requete
$tri = "order by ville";

/**
 * MULTI
 */
if ($f->isMulti() == true) {
    // Critere where de la requete
    $selection = "";
}

?>
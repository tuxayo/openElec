<?php
/**
 *
 */

/**
 * Cette requete permet de lister tous les id_electeur presents en centre de
 * vote en fonction de la collectivite en cours
 *
 * @param string $_SESSION['collectivite']
 */
$query_select_centrevote = "select c.id_electeur ";
$query_select_centrevote .= "from centrevote as c ";
$query_select_centrevote .= "inner join electeur as e ";
$query_select_centrevote .= "on c.id_electeur = e.id_electeur ";
$query_select_centrevote .= "where e.collectivite='".$_SESSION['collectivite']."'";

?>
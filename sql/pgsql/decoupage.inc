<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Decoupage")." -> "._("Voie");

// Icone
$ico = "ico_decoupage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 15;

// Critere from de la requete
$table = "decoupage";

// Critere select de la requete
$champAffiche = array(
    "id as \""._("Id")."\"",
    "code_bureau",
    "premier_impair",
    "dernier_impair",
    "premier_pair",
    "dernier_pair"
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "id as \""._("Id")."\"",
    "code_bureau",
    "premier_impair",
    "dernier_impair",
    "premier_pair",
    "dernier_pair"
);

// Critere where de la requete
$selection = "";

// Critere order by ou group by de la requete
$tri = "";

/**
 * Tableau de liens
 */
$href[3]['lien'] = "../pdf/pdf.php?idx=";
$href[3]['id'] = "&amp;obj=electeurpardecoupage";
$href[3]['lib'] = "<span class=\"om-icon om-icon-16 om-icon-fix pdf-16\" title=\""._("Electeurs par decoupage")."\">"._("Electeurs par decoupage")."</span>";
$href[3]['target'] = "_blank";

/**
 * Parametres de sous formulaires
 */
// 
if (isset($retourformulaire) and $retourformulaire == "voie"
    and isset($idxformulaire) and $idxformulaire != "") {
    $selection = "where code_voie='".$idx."'";
}

?>
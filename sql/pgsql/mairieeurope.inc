<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG=0;

// Nombre d'enregistrements par page
$serie=15;

// Titre
$ent = _("Electeur")." -> "._("Mairie Europe");

// Icone
$ico="ico_mairieeurope.png";

// Objet d'une eventuelle edition .pdf.inc
$edition="";

// Critere from de la requete
$table="mairieeurope as c, electeur as a ";

// Critere select de la requete
$champAffiche = array(
    "c.id_electeur_europe as \""._("id")."\"",
    "(a.nom||' '||a.prenom||' '||a.code_bureau) as \""._("electeur")."\"",
    "c.mairie as \""._("mairie")."\""
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "a.nom",
    "a.prenom",
    "a.code_bureau",
    "*electeur"
);

// Critere where de la requete
$selection=" where c.id_electeur_europe = a.id_electeur and a.collectivite = '".$_SESSION['collectivite']."' ";

// Critere order by ou group by de la requete
$tri= " order by withoutaccent(lower(a.nom)), withoutaccent(lower(a.prenom)) ";

?>
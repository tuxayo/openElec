<?php
/**
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 * Requete pour compter le nombre d'electeur(s) selectionne(s) comme jure
 */
$query_count_jury = "select count(*) from electeur ";
$query_count_jury .= "where collectivite='".$_SESSION['collectivite']."' ";
$query_count_jury .= "and liste='".$_SESSION['liste']."' ";
$query_count_jury .= "and jury=1 ";

/**
 * Requete pour de-selectionner tous les electeurs selectionnes comme jure
 */
$query_update_jury = "update electeur set jury=0, profession='', motif_dispense_jury=''";
$query_update_jury .= "where collectivite='".$_SESSION['collectivite']."' ";
$query_update_jury .= "and liste='".$_SESSION['liste']."' ";
$query_update_jury .= "and jury=1 ";

?>

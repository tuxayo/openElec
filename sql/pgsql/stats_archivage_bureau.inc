<?php
/**
 *
 */

/**
 * Cette requete permet de lister tous les bureaux en fonction de la
 * collectivite en cours
 * 
 * @param string $_SESSION['collectivite']
 */
$query_select_bureau = "select * from bureau ";
$query_select_bureau .= "where collectivite='".$_SESSION['collectivite']."' ";
$query_select_bureau .= "order by code ";

if (isset($row_select_bureau['code'])) {
	/**
	 * Cette requete permet de compter tous les mouvements de la date de tableau
	 * en cours en fonction de la collectivite en cours, de la liste en cours,
	 * du bureau selectionne
	 *
	 * @param string $row_select_bureau['code'] Code du bureau
	 * @param string $_SESSION['collectivite']
	 * @param string $_SESSION['liste']
	 * @param string $f->collectivite['datetableau']
     */
	$query_count_mouvement = "select count(*) ";
	$query_count_mouvement .= "from mouvement ";
	$query_count_mouvement .= "where liste='".$_SESSION['liste']."' ";
	$query_count_mouvement .= "and collectivite='".$_SESSION['collectivite']."' ";
	$query_count_mouvement .= "and code_bureau='".$row_select_bureau['code']."' ";
    $query_count_mouvement .= "and date_tableau='".$f->collectivite['datetableau']."' ";
    
	/**
	 * Cette requete permet de compter tous les mouvements de la date de tableau
	 * en cours en fonction de la collectivite en cours, de la liste en cours,
	 * du bureau selectionne et dans l'etat "actif"
	 *
	 * @param string $row_select_bureau['code'] Code du bureau
	 * @param string $_SESSION['collectivite']
	 * @param string $_SESSION['liste']
	 * @param string $f->collectivite['datetableau']
     */    
    $query_count_mouvement_actif = $query_count_mouvement;
    $query_count_mouvement_actif .= "and etat='actif' ";

	/**
	 * Cette requete permet de compter tous les mouvements de la date de tableau
	 * en cours en fonction de la collectivite en cours, de la liste en cours,
	 * du bureau selectionne et dans l'etat "trs"
	 *
	 * @param string $row_select_bureau['code'] Code du bureau
	 * @param string $_SESSION['collectivite']
	 * @param string $_SESSION['liste']
	 * @param string $f->collectivite['datetableau']
     */
    $query_count_mouvement_trs = $query_count_mouvement;
    $query_count_mouvement_trs .= "and etat='trs' ";

	/**
	 * Cette requete permet de compter tous les mouvements de la date de tableau
	 * en cours en fonction de la collectivite en cours, de la liste en cours,
	 * du bureau selectionne et dans l'etat "trs" ayant ete transmis a l'insee
	 *
	 * @param string $row_select_bureau['code'] Code du bureau
	 * @param string $_SESSION['collectivite']
	 * @param string $_SESSION['liste']
	 * @param string $f->collectivite['datetableau']
     */
    $query_count_mouvement_trs_cnen = $query_count_mouvement_trs;
    $query_count_mouvement_trs_cnen .= "and envoi_cnen<>'' ";
}

?>

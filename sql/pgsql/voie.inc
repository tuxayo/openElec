<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Decoupage")." -> "._("Voie");

// Icone
$ico = "ico_decoupage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "voie";

// Nombre d'enregistrements par page
$serie = 20;

// Critere from de la requete
$table = "voie inner join collectivite ";
$table .= "on voie.code_collectivite = collectivite.id ";

// Critere select de la requete
if($f->getParameter("option_code_voie_auto") == true) {
    $voie = "voie.code as \""._("Code")."\"";
} else {
    $voie = "voie.code as \""._("Code")."\"";
}
$champAffiche = array(
    $voie,
    "voie.libelle_voie as \""._("Libelle")."\"",
    "voie.cp as \"Code Postal\"",
    "voie.ville as \""._("Ville")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "voie.code as \""._("Code")."\"",
    "voie.libelle_voie as \""._("Libelle")."\"",
    "voie.cp as \"Code Postal\"",
    "voie.ville as \""._("Ville")."\"",
);

// Critere where de la requete
$selection = "where voie.code_collectivite='".$_SESSION['collectivite']."' ";

// Critere order by ou group by de la requete
$tri = "order by voie.code_collectivite, voie.libelle_voie";

/**
 * Tableau de liens
 */
$href[3]['lien'] = "../pdf/pdf.php?idx=";
$href[3]['id'] = "&amp;obj=electeurparvoie";
$href[3]['lib'] = "<span class=\"om-icon om-icon-16 om-icon-fix pdf-16\" title=\""._("Electeurs par voie")."\">"._("Electeurs par voie")."</span>";
$href[3]['target'] = "_blank";

/**
 * Sous formulaires
 */
$sousformulaire = array(
    "decoupage",
);
//
if (isset($idz) and $idz != "") {
    $ent .= " -> ".$idz;
}

/**
 * MULTI
 */
if ($f->isMulti() == true) {
    // Critere select de la requete
    array_push($champAffiche, "collectivite.ville as \""._("Collectivite")."\"");
    
    // Champ sur lesquels la recherche est active
    array_push($champRecherche, "collectivite.ville as \""._("Collectivite")."\"");
    
    // Critere where de la requete
    $selection = "";
    
    // Tableau de liens inutiles
    unset($href[3]);
}

/**
 * Options
 */
//
$options = array();
//
$option = array(
    "type" => "pagination_select",
    "display" => false,
);
array_push($options, $option);

?>
<?php
/**
 * Configuration du tableau de membres de commission
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Titre
$ent = _("Parametrage")." -> "._("Membres de la commission");

// Icone
$ico = "ico_parametrage.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 15;

// Critere from de la requete
$table = "membre_commission inner join collectivite ";
$table .= "on membre_commission.collectivite = collectivite.id ";

// Critere select de la requete
$champAffiche = array(
    "membre_commission.membre_commission as \""._("Id")."\"",
    "membre_commission.nom as \""._("Nom")."\"",
    "membre_commission.prenom as \""._("Prenom")."\"",
    "membre_commission.cp as \""._("Code postal")."\"",
    "membre_commission.ville as \""._("Ville")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "membre_commission.membre_commission as \""._("Id")."\"",
    "membre_commission.nom as \""._("Nom")."\"",
    "membre_commission.prenom as \""._("Prenom")."\"",
    "membre_commission.cp as \""._("Code postal")."\"",
    "membre_commission.ville as \""._("Ville")."\"",
);

// Critere where de la requete
$selection = " where membre_commission.collectivite='".$_SESSION['collectivite']."'";

// Critere order by ou group by de la requete
$tri = "order by withoutaccent(lower(membre_commission.nom)) ";

/**
 * MULTI
 */
if ($f->isMulti() == true) {
    // Critere select de la requete
    array_push($champAffiche, "collectivite.ville as \""._("Collectivite")."\"");
    
    // Champ sur lesquels la recherche est active
    array_push($champRecherche, "collectivite.ville as \""._("Collectivite")."\"");
    
    // Critere where de la requete
    $selection = "";
}

?>
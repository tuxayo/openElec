<?php
/**
 * Ce fichier est appele par trt/insee_export.php
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 * Cette requete permet de lister tous les mouvements a transmettre a l'insee
 * en fonction de la date de tableau en cours, de la collectivite en cours et
 * de la liste en cours
 *
 * @param string $datetableau
 * @param string $_SESSION['collectivite']
 * @param string $_SESSION['liste']
 *
 * @param string $envoi Ce parametre permet de selectionner les mouvements
 *                      deja envoyes lors d'un precedent envoi
 */
$query_select_mouvement = "
SELECT * FROM mouvement
JOIN param_mouvement ON mouvement.types=param_mouvement.code 
JOIN nationalite ON mouvement.code_nationalite=nationalite.code ";

$query_select_mouvement .= "where date_tableau='".$datetableau."' ";
if (isset($envoi) and $envoi != "") {
    $query_select_mouvement .= "and mouvement.envoi_cnen='".$envoi."' ";
} else {
    $query_select_mouvement .= "and mouvement.envoi_cnen is null ";
}
$query_select_mouvement .= "and param_mouvement.cnen='Oui' ";
$query_select_mouvement .= "and liste='".$_SESSION['liste']."' ";
$query_select_mouvement .= "and collectivite='".$_SESSION['collectivite']."' ";
$query_select_mouvement .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";

/**
 *
 * @param string $tomdep
 */
if (isset($tomdep)) {
    $query_libelle_departement = "select libelle_departement from departement ";
    $query_libelle_departement .= "where code='".$tomdep."'";
}

?>

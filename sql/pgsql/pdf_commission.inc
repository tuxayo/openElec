<?php
/**
 * Ce fichier est appele par le script pdf/commission.php pour generer les
 * details de commission.
 *
 * @package openElec
 * @version SVN : $Id$
 */




//
 
/*
$nobureau: numero de bureau
Ce fichier include est inclus 4 fois
   passage 0
   passage 1
   passage 2 2 fois

$mouvement_categorie => type categorie du mouvement

variable sans modification permet de garder que les modifications de changement
de bureau dans l etat commission

*/

/**
 *
 */

// TABLES
$critere_table_commun = " FROM mouvement ";
$critere_table_commun .= " LEFT JOIN param_mouvement ";
$critere_table_commun .= " ON mouvement.types = param_mouvement.code ";

// SELECTION COMMUNE
$critere_where_commun = " WHERE mouvement.collectivite='".$_SESSION['collectivite']."' ";
$critere_where_commun .= " AND mouvement.liste='".$_SESSION['liste']."' ";
$critere_where_commun .= " AND mouvement.date_tableau='".$datetableau."' ";

if ($passage == 1 || $passage == 2) {
    // SELECTION ADDITION
    $critere_where_inscription = " AND ((param_mouvement.typecat='Inscription' ";
    if ($nobureau != "ALL") {
        $critere_where_inscription .= " AND mouvement.code_bureau='".$nobureau."' ";
    }
    $critere_where_inscription .= " ) ";
    //
    if ($sans_modification == true) {
        //
        $critere_where_inscription .= " OR (param_mouvement.typecat='Modification' ";
        if ($nobureau != "ALL") {
            $critere_where_inscription .= " AND mouvement.code_bureau='".$nobureau."' ";
        }
        $critere_where_inscription .= " AND mouvement.ancien_bureau!=mouvement.code_bureau)) ";
    } else {
        //
        $critere_where_inscription .= " OR (param_mouvement.typecat='Modification' ";
        if ($nobureau != "ALL") {
            $critere_where_inscription .= " AND mouvement.code_bureau='".$nobureau."' ";
        }
        $critere_where_inscription .= " )) ";
    }

    // SELECTION RADIATION
    $critere_where_radiation = " AND ((param_mouvement.typecat='Radiation' ";
    if ($nobureau != "ALL") {
        $critere_where_radiation .= " AND mouvement.code_bureau='".$nobureau."' ";
    }
    $critere_where_radiation .= " ) ";
    //
    if ($sans_modification == true) {
        //
        $critere_where_radiation .= " OR (param_mouvement.typecat='Modification' ";
        if ($nobureau != "ALL") {
            $critere_where_radiation .= " AND mouvement.ancien_bureau='".$nobureau."' ";
        }
        $critere_where_radiation .= " AND mouvement.ancien_bureau != mouvement.code_bureau)) ";
    } else {
        //
        $critere_where_radiation .= " OR (param_mouvement.typecat='Modification' ";
        if ($nobureau != "ALL") {
            $critere_where_radiation .= " AND mouvement.ancien_bureau='".$nobureau."' ";
        }
        $critere_where_radiation .= " )) "; 
    }

    // SELECTION DATES
    $critere_filtre_dates = "
        AND (mouvement.date_modif >= '".$date_debut."' 
        AND date_modif <= '".$date_fin."') 
    ";

}


/**
 * 
 */
//
if ($passage == 0) {
    //
    $query_liste_bureaux = " SELECT bureau.code AS bureau, ";
    $query_liste_bureaux .= " canton.code as canton, ";
    $query_liste_bureaux .= " bureau.libelle_bureau, ";
    $query_liste_bureaux .= " canton.libelle_canton ";
    $query_liste_bureaux .= " FROM bureau ";
    $query_liste_bureaux .= " INNER JOIN canton ";
    $query_liste_bureaux .= " ON bureau.code_canton=canton.code ";
    $query_liste_bureaux .= " WHERE bureau.collectivite='".$_SESSION["collectivite"]."' ";
    $query_liste_bureaux .= " ORDER BY bureau.code ";
    //
    $passage = 1;
    //
} elseif ($passage == 1) {
    //
    if ($mode_edition == "tableau" 
        && $tableau == "tableau_des_additions_des_jeunes"
    ) {
        if ($datej5 == false) {
            //
            $query_nb_additions = "SELECT count(*) ";
            $query_nb_additions .= $critere_table_commun;
            $query_nb_additions .= $critere_where_commun;
            $query_nb_additions .= " AND param_mouvement.typecat='Inscription' ";
            $query_nb_additions .= " AND param_mouvement.effet='Election' ";
            $query_nb_additions .= " AND mouvement.etat='actif' ";
            $query_nb_additions .= " AND mouvement.types='".$type_mouvement_io."' ";
            if ($nobureau != "ALL") {
                $query_nb_additions .= " AND mouvement.code_bureau='".$nobureau."' ";
            }
        } else {
            //
            $query_nb_additions = "SELECT count(*) ";
            $query_nb_additions .= $critere_table_commun;
            $query_nb_additions .= $critere_where_commun;
            $query_nb_additions .= " AND param_mouvement.typecat='Inscription' ";
            $query_nb_additions .= " AND mouvement.etat='trs' ";
            $query_nb_additions .= " AND mouvement.types='".$type_mouvement_io."' ";
            if ($nobureau != "ALL") {
                $query_nb_additions .= " AND mouvement.code_bureau='".$nobureau."' ";
            }
            $query_nb_additions .= " AND mouvement.date_j5='".$datej5."' ";
        }
        
    } elseif ($mode_edition == "tableau" 
        && $tableau == "tableau_des_cinq_jours"
    ) {
        if ($datej5 == false) {
            //
            $query_nb_additions = "SELECT count(*) ";
            $query_nb_additions .= $critere_table_commun;
            $query_nb_additions .= $critere_where_commun;
            $query_nb_additions .= " AND param_mouvement.typecat='Inscription' ";
            $query_nb_additions .= " AND param_mouvement.effet='Immediat' ";
            $query_nb_additions .= " AND mouvement.etat='actif' ";
            if ($nobureau != "ALL") {
                $query_nb_additions .= " AND mouvement.code_bureau='".$nobureau."' ";
            }
            //
            $query_nb_radiations = "SELECT count(*) ";
            $query_nb_radiations .= $critere_table_commun;
            $query_nb_radiations .= $critere_where_commun;
            $query_nb_radiations .= " AND param_mouvement.typecat='Radiation' ";
            $query_nb_radiations .= " AND param_mouvement.effet='Immediat' ";
            $query_nb_radiations .= " AND mouvement.etat='actif' ";
            if ($nobureau != "ALL") {
                $query_nb_radiations .= " AND mouvement.code_bureau='".$nobureau."' ";
            }
        } else {
            //
            $query_nb_additions = "SELECT count(*) ";
            $query_nb_additions .= $critere_table_commun;
            $query_nb_additions .= $critere_where_commun;
            $query_nb_additions .= " AND param_mouvement.typecat='Inscription' ";
            $query_nb_additions .= " AND param_mouvement.effet<>'Election' ";
            $query_nb_additions .= " AND mouvement.date_j5='".$datej5."' ";
            $query_nb_additions .= " AND mouvement.etat='trs' ";
            if ($nobureau != "ALL") {
                $query_nb_additions .= " AND mouvement.code_bureau='".$nobureau."' ";
            }
            //
            $query_nb_radiations = "SELECT count(*) ";
            $query_nb_radiations .= $critere_table_commun;
            $query_nb_radiations .= $critere_where_commun;
            $query_nb_radiations .= " AND param_mouvement.typecat='Radiation' ";
            $query_nb_radiations .= " AND param_mouvement.effet<>'Election' ";
            $query_nb_radiations .= " AND mouvement.date_j5='".$datej5."' ";
            $query_nb_radiations .= " AND mouvement.etat='trs' ";
            if ($nobureau != "ALL") {
                $query_nb_radiations .= " AND mouvement.code_bureau='".$nobureau."' ";
            }
        }

    } elseif ($mode_edition == "commission" 
        && $commission == "entre_deux_dates"
    ) {
        //// COMMISSION ENTRE DEUX DATES

        //
        $query_nb_additions = "SELECT count(*) ";
        $query_nb_additions .= $critere_table_commun;
        $query_nb_additions .= $critere_where_commun;
        $query_nb_additions .= $critere_where_inscription;
        $query_nb_additions .= $critere_filtre_dates;

        //
        $query_nb_radiations = "SELECT count(*) ";
        $query_nb_radiations .= $critere_table_commun;
        $query_nb_radiations .= $critere_where_commun;
        $query_nb_radiations .= $critere_where_radiation;
        $query_nb_radiations .= $critere_filtre_dates;

    } else {
        // Requete de recuperation du nombre d'additions a la date de tableau en cours
        $nbins = " SELECT count(*) ";
        $nbins .= $critere_table_commun;
        $nbins .= $critere_where_commun;
        if ($nobureau != "ALL") {
            $nbins .= " AND mouvement.code_bureau='".$nobureau."' ";
        }
        // Avec une option pour la prise en compte ou non des modifications
        if ($sans_modification == true) {
            $nbins .= " AND (param_mouvement.typecat='Inscription' or ";
            $nbins .= " (param_mouvement.typecat='Modification' ";
            if ($nobureau != "ALL") {
                $nbins .= " AND mouvement.code_bureau='".$nobureau."' ";
            }
            $nbins .= " and mouvement.ancien_bureau != mouvement.code_bureau)) ";
        } else {
            $nbins .= " and (param_mouvement.typecat='Inscription' or param_mouvement.typecat='Modification') ";
        }
    
        // Requete de recuperation du nombre d'additions actives a la date de tableau en cours
        // Avec une option pour la prise en compte ou non des modifications
        $nbins_actif = $nbins." and mouvement.etat='actif'";
    
        // Requete de recuperation du nombre de radiations a la date de tableau en cours
        $nbrad = " SELECT count(*) ";
        //
        $nbrad .= $critere_table_commun;
        $nbrad .= $critere_where_commun;
        //
        $nbrad .= "and ((param_mouvement.typecat='Radiation'";
        if ($nobureau != "ALL") {
            $nbrad .= "and mouvement.code_bureau='".$nobureau."'";
        }
        $nbrad .= ") ";
        // Avec une option pour la prise en compte ou non des modifications
        if ($sans_modification == true) {
            $nbrad .= " or (param_mouvement.typecat='Modification' ";
            if ($nobureau != "ALL") {
                $nbrad .= " and mouvement.ancien_bureau='".$nobureau."' ";
            }
            $nbrad .= " and mouvement.ancien_bureau != mouvement.code_bureau)) ";
        } else {
            $nbrad .= " or (param_mouvement.typecat='Modification' ";
            if ($nobureau != "ALL") {
                $nbrad .= " and mouvement.ancien_bureau='".$nobureau."'";
            }
            $nbrad .= " )) ";
        }
    
        // Requete de recuperation du nombre de radiations actives a la date de tableau en cours
        // Avec une option pour la prise en compte ou non des modifications
        $nbrad_actif = $nbrad." and mouvement.etat='actif'";
    
        // Requete de recuperation du nombre de l'electeur a l'instant t dans la table electeur
        $nbelec = " SELECT count(*) ";
        $nbelec .= " FROM electeur ";
        $nbelec .= " WHERE electeur.collectivite='".$_SESSION['collectivite']."' ";
        $nbelec .= " and electeur.liste='".$_SESSION['liste']."' ";
        if ($nobureau != "ALL") {
            $nbelec .= " and electeur.code_bureau='".$nobureau."' ";
        }
    
        // Requete de recuperation du nombre de radiations de tous les mouvements
        // traites a une date superieure ou egale a la date de tableau en cours
        $nb_radiation_dtsup = "select count(*) as nbr ";
        $nb_radiation_dtsup .= "from mouvement as m inner join param_mouvement as pm ";
        $nb_radiation_dtsup .= "on m.types=pm.code ";
        $nb_radiation_dtsup .= "where m.date_tableau>='".$datetableau."' ";
        $nb_radiation_dtsup .= " and m.liste='".$_SESSION['liste']."'";
        $nb_radiation_dtsup .= " and m.collectivite = '".$_SESSION['collectivite']."' ";
        $nb_radiation_dtsup .= "and ((pm.typecat='Radiation' and m.code_bureau='".$nobureau."') ";
        // Avec une option pour la prise en compte ou non des modifications
        if ($sans_modification == true) {
            $nb_radiation_dtsup .= " or (pm.typecat='Modification' and ";
            $nb_radiation_dtsup .= " m.ancien_bureau='".$nobureau."' ";
            $nb_radiation_dtsup .= " and m.ancien_bureau != m.code_bureau)) ";
        } else {
            $nb_radiation_dtsup .= " or (param_mouvement.typecat='Modification' ";
            $nb_radiation_dtsup .= " and m.ancien_bureau='".$nobureau."')) ";
        }
        $nb_radiation_dtsup .= " and m.etat='trs'";
        
        // Requete de recuperation du nombre d'additions de tous les mouvements
        // traites a une date superieure ou egale a la date de tableau en cours
        $nb_addition_dtsup = "select count(*) as nbr ";
        $nb_addition_dtsup .= "from mouvement as m inner join param_mouvement as pm ";
        $nb_addition_dtsup .= "on m.types=pm.code ";
        $nb_addition_dtsup .= "where m.date_tableau>='".$datetableau."' ";
        $nb_addition_dtsup .= "and m.code_bureau='".$nobureau."' and ";
        $nb_addition_dtsup .= "m.collectivite = '".$_SESSION['collectivite']."' ";
        $nb_addition_dtsup .= " and m.liste='".$_SESSION['liste']."'";
        // Avec une option pour la prise en compte ou non des modifications
        if ($sans_modification == true) {
            $nb_addition_dtsup .= " and (pm.typecat='Inscription' or ";
            $nb_addition_dtsup .= " (pm.typecat='Modification' ";
            $nb_addition_dtsup .= " AND m.code_bureau='".$nobureau."' ";
            $nb_addition_dtsup .= " and m.ancien_bureau != m.code_bureau)) ";
        } else {
            $nb_addition_dtsup .= " and (pm.typecat='Inscription' or pm.typecat='Modification') ";
        }
        $nb_addition_dtsup .= " and m.etat='trs'";
    }
    //
    $passage = 2;
    //
} elseif ($passage == 2) {

    // MOUVEMENT
    $sql = "SELECT ";
    $sql .= " mouvement.nom, ";
    $sql .= " mouvement.prenom, ";
    $sql .= " mouvement.nom_usage, ";
    $sql .= " to_char(mouvement.date_naissance,'DD/MM/YYYY') as naissance, ";
    $sql .= " (mouvement.libelle_lieu_de_naissance||' ('||mouvement.code_departement_naissance||')' ) as lieu, ";
    // Adresse - ligne 1 (numero + complement du numero + adresse)
    $sql .= " ( ";
    $sql .= " CASE mouvement.numero_habitation ";
        $sql .= " WHEN 0 then '' ";
        $sql .= " ELSE (mouvement.numero_habitation||' ') ";
    $sql .= " END";
    $sql .= " || ";
    $sql .= " CASE mouvement.complement_numero ";
        $sql .= " WHEN '' then '' ";
        $sql .= " ELSE (upper(mouvement.complement_numero)||' ') ";
    $sql .= " END";
    $sql .= " || ";
    $sql .= " mouvement.libelle_voie ";
    $sql .= " ) AS adresse, ";
    // Adresse - ligne 2 (complement)
    $sql .= " mouvement.complement, ";
    //
    if ($mouvement_categorie == "Inscription") {
        $sql .= " mouvement.code_bureau as bureau, ";
    } elseif ($mouvement_categorie == "Radiation") {
        $sql .= " mouvement.ancien_bureau as bureau, ";
    }
    $sql .= " mouvement.numero_bureau, ";
    $sql .= " (param_mouvement.libelle) as libmvt, ";
    //
    $sql .= " ( ";
    $sql .= " ('('||param_mouvement.typecat||') ') ";
    $sql .= " || ";
    if ($mouvement_categorie == "Inscription") {
        $sql .= " CASE mouvement.ancien_bureau ";
            $sql .= " WHEN '' then '' ";
            $sql .= " ELSE ";
            $sql .= " CASE param_mouvement.typecat ";
                $sql .= " WHEN 'Modification' then ('(Ancien bureau : '||mouvement.ancien_bureau||') ') ";
                $sql .= " ELSE '' ";
            $sql .= " END";
        $sql .= " END";
    } elseif ($mouvement_categorie == "Radiation") {
        $sql .= " CASE mouvement.ancien_bureau ";
            $sql .= " WHEN '' then '' ";
            $sql .= " ELSE ";
            $sql .= " CASE param_mouvement.typecat ";
                $sql .= " WHEN 'Modification' then ('(Nouveau bureau : '||mouvement.code_bureau||') ') ";
                $sql .= " ELSE '' ";
            $sql .= " END";
        $sql .= " END";
    }
    $sql .= " || ";
    $sql .= " CASE mouvement.etat ";
        $sql .= " WHEN 'actif' then '' ";
        $sql .= " ELSE ('('||mouvement.tableau||') ') ";
    $sql .= " END";
    $sql .= " ) AS observations ";
    // TABLES
    $sql .= $critere_table_commun;
    // SELECTION COMMUNE
    $sql .= $critere_where_commun;
    //
    if ($mouvement_categorie == "Inscription") {
        //
        $sql .= $critere_where_inscription;

    } elseif ($mouvement_categorie == "Radiation") {
        //
        $sql .= $critere_where_radiation;
    }
    //
    if ($mode_edition == "tableau" && $tableau == "tableau_des_additions_des_jeunes") {
        if ($datej5 == false) {
            $sql .= " AND mouvement.etat='actif' ";
            $sql .= " AND mouvement.types='".$type_mouvement_io."' ";
        } else {
            $sql .= " AND mouvement.date_j5='".$datej5."' ";
            $sql .= " AND mouvement.etat='trs' ";
            $sql .= " AND mouvement.types='".$type_mouvement_io."' ";
        }
    } elseif ($mode_edition == "tableau" && $tableau == "tableau_des_cinq_jours") {
        if ($datej5 == false) {
            $sql .= " AND mouvement.etat='actif' ";
            $sql .= " AND param_mouvement.effet='Immediat' ";
        } else {
            $sql .= " AND mouvement.date_j5='".$datej5."' ";
            $sql .= " AND mouvement.etat='trs' ";
            $sql .= " AND param_mouvement.effet<>'Election' ";
        }
    } elseif ($mode_edition == "commission" && $commission == "entre_deux_dates") {
        $sql .= $critere_filtre_dates;
    }
    //
    $sql .= " order by withoutaccent(lower(mouvement.nom)), withoutaccent(lower(mouvement.prenom)) ";
}

?>

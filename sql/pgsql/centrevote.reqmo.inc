<?php
$reqmo['libelle']=" EXPORT DES VOTANTS EN CENTRE DE VOTE ";
$reqmo['sql']="select numero_bureau as no_dans_bureau,numero_electeur as no_electeur,liste, [civilite], [nom], [prenom],
                      [nom_usage],
                      [sexe],
                      [situation],
                      [(to_char(date_naissance, 'DD/MM/YYYY')) as date_de_naissance],
                      [code_departement_naissance],
                      [libelle_departement_naissance],
                      [code_lieu_de_naissance],
                      [libelle_lieu_de_naissance],
                      [numero_habitation],
                      [libelle_voie],
                      [complement],
                      [provenance],
                      [libelle_provenance],
                      [carte],
                      [jury]
                       from electeur inner join centrevote on electeur.id_electeur=centrevote.id_electeur
                       where electeur.collectivite = '".$_SESSION['collectivite']."'
                        order by [TRI]";
$reqmo['TRI']= array('nom',
                     'nom_usage',
                     'prenom'
                     );
//
$reqmo['civilite']="checked";
$reqmo['nom']="checked";
$reqmo['prenom']="checked";
$reqmo['nom_usage']="checked";
$reqmo['sexe']="checked";
$reqmo['situation']="checked";
$reqmo['date_de_naissance']="checked";
$reqmo['code_departement_naissance']="checked";
$reqmo['libelle_departement_naissance']="checked";
$reqmo['code_lieu_de_naissance']="checked";
$reqmo['libelle_lieu_de_naissance']="checked";
$reqmo['numero_habitation']="checked";
$reqmo['libelle_voie']="checked";
$reqmo['complement']="checked";
$reqmo['provenance']="checked";
$reqmo['libelle_provenance']="checked";
$reqmo['carte']="checked";
$reqmo['jury']="checked";


?>

<?php
//11/09/2006   16:50:55
$etat['orientation']="P";
$etat['format']="A4";
$etat['footerfont']="helvetica";
$etat['footerattribut']="I";
$etat['footertaille']="8";
$etat['logo']="logopdf.png";
$etat['logoleft']="70";
$etat['logotop']="37";
$etat['titre']="ATTESTATION D'INSCRIPTION
";
$etat['titreleft']="50";
$etat['titretop']="66";
$etat['titrelargeur']="130";
$etat['titrehauteur']="15";
$etat['titrefont']="helvetica";
$etat['titreattribut']="B";
$etat['titretaille']="20";
$etat['titrebordure']="0";
$etat['titrealign']="L";
$etat['corps']="[nomprenom]
[nom_usage]
Né(e) le [naissance]
à [lieu]

domicilié(e) [adresse]
             [complement]
             [voiecp]  [voieville]



sera inscrit(e) à partir du 1er mars £anneeTableau, sur la liste électorale de  £ville  bureau de vote  [code_bureau] [libur] situé à l'adresse suivante :


[code_bureau] [libur]
[adr1]
[adr2]
[adr3]


À £ville
Le £aujourdhui


£nom";
$etat['corpsleft']="23";
$etat['corpstop']="120";
$etat['corpslargeur']="165";
$etat['corpshauteur']="5";
$etat['corpsfont']="helvetica";
$etat['corpsattribut']="";
$etat['corpstaille']="12";
$etat['corpsbordure']="0";
$etat['corpsalign']="J";
$etat['sql']="SELECT code_bureau, bureau.libelle_bureau as libur,
bureau.adresse1 as adr1, bureau.adresse2 as adr2,
bureau.adresse3 as adr3,
(civilite||' '||nom||' '||prenom) as nomprenom,
nom_usage,
to_char(date_naissance,'DD/MM/YYYY') as naissance,
 (libelle_lieu_de_naissance||' ('||code_departement_naissance||')' ) as lieu,
  case  when numero_habitation =0  then trim(complement_numero||' '||voie.libelle_voie)
  else trim(numero_habitation||' '||complement_numero||' '||voie.libelle_voie) end as adresse,
  complement,
  voie.cp as voiecp,
  voie.ville as voieville
  FROM (mouvement LEFT JOIN voie ON mouvement.code_voie = voie.code)
  LEFT JOIN bureau ON mouvement.code_bureau = bureau.code
  WHERE bureau.collectivite = '£collectivite' and £idx";
$etat['sousetat']=array();
$etat['se_font']="helvetica";
$etat['se_margeleft']="8";
$etat['se_margetop']="5";
$etat['se_margeright']="5";
$etat['se_couleurtexte']=array("0","0","0");
?>
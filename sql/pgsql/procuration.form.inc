<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

// Critere from de la requete
$tableSelect=" procuration, electeur as a,electeur as b ";

// Critere select de la requete
$champs=array(
    "id_procuration",
    "to_char(procuration.date_modif,'DD/MM/YYYY') as date_modif",
    "procuration.utilisateur as utilisateur",
    "mandant",
    "(a.nom||' - '||a.prenom||' - '||to_char(a.date_naissance,'DD/MM/YYYY')||' - '||a.code_bureau) as z_mandant",
    "mandataire",
    "(b.nom||' - '||b.prenom||' - '||to_char(b.date_naissance,'DD/MM/YYYY')||' - '||b.code_bureau) as z_mandataire",
    "debut_validite",
    "fin_validite",
    "types",
    "origine1",
    "origine2",
    "refus",
    "motif_refus",
    "date_accord",
    "heure_accord"
);

// Critere where de la requete
$selection=" and procuration.mandant = a.id_electeur and procuration.mandataire = b.id_electeur and a.collectivite='".$_SESSION['collectivite']."' and b.collectivite='".$_SESSION['collectivite']."'";

?>
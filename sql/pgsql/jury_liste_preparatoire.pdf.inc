<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
//
$canton = NULL;
if (isset($_GET["canton"])) {
    $canton = $_GET["canton"];
}
//
$sql = "select libelle_canton from canton where code='".$canton."'";
$libelle_canton = $f->db->getone($sql);
$f->addToLog("jury_liste_preparatoire.pdf.inc: db->getone(\"".$sql."\");", VERBOSE_MODE);
$f->isDatabaseError($libelle_canton);


/**
 *
 */
//
$DEBUG=0;
// ------------------------document---------------------------------------------
$orientation="L";// orientation P-> portrait L->paysage
$format="A4";// format A3 A4 A5
$police='arial';
$margeleft=10;// marge gauche
$margetop=5;// marge haut
$margeright=10;//  marge droite
$border=1; // 1 ->  bordure 0 -> pas de bordure
$C1="0";// couleur texte  R
$C2="0";// couleur texte  V
$C3="0";// couleur texte  B
//-------------------------LIGNE tableau----------------------------------------
$size=7; //taille POLICE
$height=5; // -> hauteur ligne tableau
$align='L';
$fond=1;// 0- > FOND transparent 1 -> fond
$C1fond1="255";// couleur fond  R
$C2fond1="255";// couleur fond  V
$C3fond1="255";// couleur fond  B
$C1fond2="241";// couleur fond  R
$C2fond2="241";// couleur fond  V
$C3fond2="241";// couleur fond  B
//-------------------------- titre----------------------------------------------
$libtitre = "Liste préparatoire du jury d'assises";
$libtitre .= ($libelle_canton != "" ? " - ".$libelle_canton : "");
$libtitre .= " - Année ".date("Y")."/".(date("Y")+1);
$flagsessionliste=0;// 1 - > affichage session liste ou 0 -> pas d'affichage
$bordertitre=0; // 1 ->  bordure 0 -> pas de bordure
$aligntitre='L'; // L,C,R
$heightitre=10;// hauteur ligne titre
$grastitre="B";//$gras="B" -> BOLD OU $gras=""
$fondtitre=0; //0- > FOND transparent 1 -> fond
$C1titrefond="181";// couleur fond  R
$C2titrefond="182";// couleur fond  V
$C3titrefond="188";// couleur fond  B
$C1titre="75";// couleur texte  R
$C2titre="79";// couleur texte  V
$C3titre="81";// couleur texte  B
$sizetitre=11;
//--------------------------libelle entete colonnes-----------------------------
$flag_entete=1;//entete colonne : 0 -> non affichage , 1 -> affichage
$fondentete=1;// 0- > FOND transparent 1 -> fond
$heightentete=5;//hauteur ligne entete colonne
$C1fondentete="180";// couleur fond  R
$C2fondentete="180";// couleur fond  V
$C3fondentete="180";// couleur fond  B
$C1entetetxt="0";// couleur texte R
$C2entetetxt="0";// couleur texte V
$C3entetetxt="0";// couleur texte B
$entete_style="";
$entete_size=8;
//------ Border entete colonne $be0  à $be.. ( $be OBLIGATOIRE )
$be0="TLB";
$be1="TLB";
$be2="TLB";
$be3="TLB";
$be4="TLB";
$be5="TLB";
$be6="TLBR";
// ------ couleur border--------------------------------------------------------
$C1border="159";// couleur texte  R
$C2border="160";// couleur texte  V
$C3border="167";// couleur texte  B
//------ Border cellule colonne $b0  à $b.. ( $b OBLIGATOIRE )
$b0="BL";
$b1="BL";
$b2="BL";
$b3="BL";
$b4="BL";
$b5="BL";
$b6="BLR";
//------ ALIGNEMENT entete colonne $ae0  à $ae.. ( $ae OBLIGATOIRE )
$ae0="C";
$ae1="C";
$ae2="C";
$ae3="C";
$ae4="C";
$ae5="C";
$ae6="C";
//------ largeur de chaque colonne $l0  à $l.. ( $l OBLIGATOIRE )---------------
$l0=12;
$l1=50;
$l2=50;
$l3=75;
$l4=30;
$l5=30;
$l6=30;
$widthtableau=277;// -> ajouter $l0 à $lxx
$bt=0;// border 1ere  et derniere ligne  dutableau par page->0 ou 1
//------ Affichage d'une ligne de compteur du nombre d'enregistrements----------
$counter_flag = true;
$counter_prefix = "";
$counter_suffix = "enregistrement(s)";
//------ ALIGNEMENT de chaque colonne $l0  à $a.. ( $a OBLIGATOIRE )------------
$a0="C";
$a1="L";
$a2="L";
$a3="L";
$a4="L";
$a5="L";
$a6="L";

/**
 *
 */
//--------------------------SQL-------------------------------------------------
$sql="SELECT ";
$sql .= " code_bureau as "._("bureau").", ";
$sql .= " case ";
    $sql .= " when (electeur.sexe = 'F' and electeur.situation = 'M' and electeur.nom_usage<>'')";
        $sql .= " then (electeur.civilite ||' '||electeur.nom||' EP '||electeur.nom_usage||' '||electeur.prenom )";
    $sql .= " when ((electeur.situation <> 'M' or electeur.situation is NULL) and electeur.nom_usage <> '')";
        $sql .= " then (electeur.civilite ||' '||electeur.nom||' - '||electeur.nom_usage||' '||electeur.prenom ) ";
    $sql .= " else (electeur.civilite ||' '||electeur.nom||' '||electeur.prenom) ";
$sql .= " end as \""._("Nom(s) Prenom(s)")."\", ";

$sql .= " (to_char(date_naissance,'DD/MM/YYYY' )||' à '||libelle_lieu_de_naissance) as "._("naissance").", ";

$sql .= " case electeur.resident ";
    $sql .= " when 'Non' then (";
        $sql .= " case electeur.numero_habitation ";
            $sql .= " when 0 then '' ";
            $sql .= " else (electeur.numero_habitation||' ') ";
        $sql .= " end";
        $sql .= "||";
        $sql .= " case electeur.complement_numero ";
            $sql .= " when '' then '' ";
            $sql .= " else (electeur.complement_numero||' ') ";
        $sql .= " end";
        $sql .= "||";
        $sql .= "electeur.libelle_voie";
        $sql .= "||";
        $sql .= " case electeur.complement ";
            $sql .= " when '' then '' ";
            $sql .= " else (' / '||electeur.complement) ";
        $sql .= " end";
    $sql .= ") ";
    $sql .= " when 'Oui' then (";
        $sql .= "electeur.adresse_resident";
        $sql .= "||";
        $sql .= " case electeur.complement_resident ";
            $sql .= " when '' then '' ";
            $sql .= " else (' / '||electeur.complement_resident) ";
        $sql .= " end";
    $sql .= ") ";
    $sql .= " end as \""._("Adresse / Complement")."\", ";

$sql .= " case resident ";
    $sql .= " when 'Non' ";
        $sql .= " then (voie.cp||' '||voie.ville) ";
    $sql .= " when 'Oui' ";
        $sql .= " then (cp_resident||' ' ||ville_resident) ";
    $sql .= " end as \""._("CP Ville")."\", ";
$sql .= " profession as "._("profession").", ";
$sql .= " motif_dispense_jury as "._("dispense")." ";
$sql .= " from electeur ";
$sql .= " left join voie on electeur.code_voie=voie.code ";
$sql .= " left join bureau on electeur.code_bureau=bureau.code ";
$sql .= "     and electeur.collectivite=bureau.collectivite";
$sql .= " left join canton on bureau.code_canton=canton.code ";

$sql .= " WHERE ";
$sql .= " jury=1 ";
$sql .= " AND electeur.collectivite='".$_SESSION['collectivite']."' ";
if (isset($canton) && $canton != NULL) {
    $sql .= " AND bureau.code_canton='".$canton."' ";
}

$sql .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
//------------------------------------------------------------------------------

?>
<?php
/**
 * $Id$
 *
 */
//

$sql = "select ";
//
$sql .= " electeur.id_electeur,";
$sql .= " electeur.code_bureau,";
$sql .= " '' as canton,";
if ($_SESSION["liste"] == "01") {
    $sql .= " electeur.numero_bureau, ";
} else {
    $sql .= " ' ' as numero_bureau, ";
}

$sql .= " bureau.libelle_bureau as libur,";
$sql .= " bureau.adresse1 as adr1,";
$sql .= " bureau.adresse2 as adr2,";
$sql .= " bureau.adresse3 as adr3,";

$sql .= " case ";
    $sql .= " when (electeur.sexe = 'F' and electeur.situation = 'M' and electeur.nom_usage<>'')";
        $sql .= " then (electeur.civilite ||' '||electeur.nom||' EP '||electeur.nom_usage||' '||electeur.prenom )";
    $sql .= " when ((electeur.situation <> 'M' or electeur.situation is NULL) and electeur.nom_usage <> '')";
        $sql .= " then (electeur.civilite ||' '||electeur.nom||' - '||electeur.nom_usage||' '||electeur.prenom ) ";
    $sql .= " else (electeur.civilite ||' '||electeur.nom||' '||electeur.prenom) ";
$sql .= " end as nomprenom, ";

$sql .= " case electeur.resident ";
    $sql .= " when 'Non' then (";
        $sql .= " case electeur.numero_habitation ";
            $sql .= " when 0 then '' ";
            $sql .= " else (electeur.numero_habitation||' ') ";
        $sql .= " end";
        $sql .= "||";
        $sql .= " case electeur.complement_numero ";
            $sql .= " when '' then '' ";
            $sql .= " else (electeur.complement_numero||' ') ";
        $sql .= " end";
        $sql .= "||";
        $sql .= "electeur.libelle_voie) ";
    $sql .= " when 'Oui' then electeur.adresse_resident end as adresse,";
$sql .= " case electeur.resident ";
    $sql .= " when 'Non' then electeur.complement ";
    $sql .= " when 'Oui' then electeur.complement_resident ";
$sql .= " end as complement,";
$sql .= " case electeur.resident ";
    $sql .= " when 'Non' then voie.cp ";
    $sql .= " when 'Oui' then electeur.cp_resident ";
$sql .= " end as voiecp,";
$sql .= " case electeur.resident ";
    $sql .= " when 'Non' then voie.ville ";
    $sql .= " when 'Oui' then electeur.ville_resident ";
$sql .= " end as voieville,";

$sql .= " (to_char(electeur.date_naissance,'DD/MM/YYYY')) as naissance,";
$sql .= " electeur.code_departement_naissance, ";
$sql .= " electeur.libelle_lieu_de_naissance, ";
if ($_SESSION["liste"] == "01") {
    $sql .= " ' ' as nationalite ";
} else {
    $sql .= " ('Nationalité : '||libelle_nationalite) as nationalite ";
}

// FROM
$sql .= " FROM electeur left join voie on (electeur.code_voie = voie.code and electeur.collectivite = voie.code_collectivite) ";
$sql .= " left join bureau on (electeur.code_bureau = bureau.code and electeur.collectivite = bureau.collectivite) ";
$sql .= " left join canton on bureau.code_canton = canton.code";
$sql .= " left join nationalite on electeur.code_nationalite = nationalite.code";
//
if ($mode_edition == "traitement") {
    // On fait une jointure avec la table mouvement et param_mouvement
    $sql .= " left join mouvement on electeur.id_electeur = mouvement.id_electeur";
    $sql .= " left join param_mouvement on mouvement.types = param_mouvement.code";
}

// WHERE
$sql .= " WHERE ";
//
$sql .= " electeur.liste='".$_SESSION['liste']."' ";
$sql .= " AND electeur.collectivite='".$_SESSION['collectivite']."' ";
//
if ($mode_edition == "parbureau") {
    // On filtre sur le bureau de vote selectionne
    $sql .= " AND electeur.code_bureau='".$nobureau."'";
} elseif ($mode_edition == "electeur") {
    // On filtre sur l'electeur selectionne
    $sql .= " AND electeur.id_electeur='".$id_electeur."'";
} elseif ($mode_edition == "multielecteur") {
    // On vérifie que la variable existe
    if (isset($_GET["idxs"])) {
        // On boucle sur la variable pour ajouter les identifiants d'électeurs
        $sql .= " AND ( ";
        foreach($_GET["idxs"] as $idel) {
            $sql .= " electeur.id_electeur='".$idel."' or";
        }
        $sql = substr($sql , 0 , -2);
        $sql .= " ) ";
    }
} elseif ($mode_edition == "traitement") {
    // On filtre sur les mouvements de la date de tableau selectionnee
    $sql .= " and mouvement.date_tableau = '".$datetableau."'";
    // On filtre sur les mouvements traites lors du traitement selectionne
    $sql .= " and mouvement.tableau = '".$traitement."' ";
    // Si c'est un traitement J5
    if ($traitement == "j5") {
        // On filtre sur les mouvements traites a la date selectionnee
        $sql .= " and mouvement.date_j5 = '".$datej5."'";
    }
    // Filtre sur le paramètre d'impression de la carte d'électeur
    // dans la table param_mouvement
    $sql .= " and (";
    // On filtre uniquement les mouvements paramétrés pour TOUJOURS 
    // éditer une nouvelle carte electorale
    $sql .= " param_mouvement.edition_carte_electeur = '1' ";
    // OU
    $sql .= " or ";
    // On filtre uniquement les mouvements paramétrés pour EN CAS DE CHANGEMENT DE BUREAU 
    // pour éditer une nouvelle carte electorale
    $sql .= " (param_mouvement.edition_carte_electeur = '2' and mouvement.code_bureau<>mouvement.ancien_bureau) ";
    //
    $sql .= " ) ";
}

// ORDER BY
$sql .= " ORDER BY withoutaccent(lower(electeur.nom)), withoutaccent(lower(electeur.prenom)) ";

?>

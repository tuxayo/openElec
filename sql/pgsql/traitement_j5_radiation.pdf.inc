<?php
/**
 * 
 */

//
require "traitement_common_pdf.inc";

//
if ($action == "recapitulatif") {
    //
    $libtitre = "Radiation(s) appliquée(s) lors du traitement J-5 du ".$f->formatdate($datej5)." [Tableau du ".$f->formatdate($datetableau)."]";
} else {
    //
    $libtitre = "Radiation(s) à appliquer au traitement J-5 du ".date("d/m/Y")." [Tableau du ".$f->formatdate($f->collectivite["datetableau"])."]";
}

//--------------------------SQL-------------------------------------------------
$sql = " SELECT ";
$sql .= " mouvement.code_bureau as \"Bureau\", ";
$sql .= " mouvement.nom, ";
$sql .= " mouvement.prenom as \"Prenom(s)\", ";
$sql .= " to_char(mouvement.date_naissance, 'DD/MM/YYYY') as \"Naissance\", ";
$sql .= " param_mouvement.libelle as \"Motif\" ";
$sql .= $query_radiation;
$sql .= " ORDER BY mouvement.code_bureau, withoutaccent(lower(mouvement.nom)), withoutaccent(lower(mouvement.prenom)) ";
//------------------------------------------------------------------------------

?>
<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG = 0;

// Nombre d'enregistrements par page
$serie = 20;

//
$formulaire = "inscription_office.form";

// Titre
$ent = _("Consultation")." -> "._("Inscription Office Insee");

// Icone
$ico = "ico_mouvementi.png";

// Objet d'une eventuelle edition .pdf.inc
$edition = "inscription_office";

// Critere from de la requete
$table = "inscription_office";

// Critere select de la requete
$champAffiche = array(
    "id as \""._("Id")."\"",
    "('<b>'|| nom || '</b><br /> ' || prenom || '<br /><b>'|| nom_usage||'&nbsp;</b>') as \""._("Nom et prenom")."\"",
    "(to_char(date_naissance,'DD/MM/YYYY')||' <br />a '||libelle_lieu_de_naissance ) as \""._("Date et lieu de naissance")."\"",
    "(adresse1||' '||adresse2||' '||adresse3||' '||adresse4||' '||adresse5) as \""._("Adresse")."\"",
    "types as \""._("Types")."\""
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "nom",
    "prenom",
    "nom_usage",
    "*nom_prenom"
);

// Critere where de la requete
$selection = "where collectivite = '".$_SESSION['collectivite']."'";
//
$href[0]['lien'] = "";
$href[0]['id']= "";
$href[0]['lib']= "";
$href[1]['lien'] = "";
$href[1]['id']= "";
$href[1]['lib']= "";
$href[2]['lien'] = "../scr/form.php?obj=inscription_office&amp;idx=";
$href[2]['id']= "&amp;premier=".$premier."&amp;recherche=".$recherche."&amp;ids=1";
$href[2]['lib']= "<span class=\"om-icon om-icon-16 om-icon-fix delete-16\" title=\""._("Supprimer")."\">"._("Supprimer")."</span>";
$href[3]['lien'] = "../scr/form.php?obj=inscription_office_valider&amp;io=";
$href[3]['id']= "&amp;premier=".$premier."&amp;recherche=".$recherche;
$href[3]['lib']= "<span class=\"om-icon om-icon-16 om-icon-fix inscription-office-insee-16\" title=\""._("Valider l'inscription d'office")."\">"._("Valider l'inscription d'office")."</span>";

// Critere order by ou group by de la requete
$tri = "order by types, withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";

/**
 * Options
 */
//
$options = array();
//
$option = array(
    "type" => "condition",
    "field" => "types",
    "case" => array(
        "0" => array(
            "values" => array("IO", ),
            "style" => "etat-trs",
            "href" => array(
                0 => array("lien" => "", "id" => "", "lib" => ""),
                1 => array("lien" => "", "id" => "", "lib" => ""),
                2 => array("lien" => "", "id" => "", "lib" => ""),
                3 => array("lien" => "", "id" => "", "lib" => ""),
            ),
        ),
    ),
);
array_push($options, $option);

?>

<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$tableSelect = "parametrage_nb_jures";

//
$champs = array(
    "id",
    "canton",
    "collectivite",
    "nb_jures",
);

//
$selection = "";

/**
 * Parametrages des select
 */
//
$sql_canton = "select code, (code||' - '|| libelle_canton) as lib from canton order by lib";
//
$sql_collectivite = "select id, (id||' - '|| ville) as lib from collectivite order by lib";

?>
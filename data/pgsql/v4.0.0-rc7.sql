--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.0.0-rc6 vers 4.0.0-rc7
--

UPDATE version SET version = '4.0.0-rc7';

INSERT INTO droit VALUES ('traitement_j5_modification_pdf', 4);
ALTER TABLE electeur ALTER COLUMN procuration TYPE character varying(255);

--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.1.1 vers 4.2.0
--
UPDATE version SET version = '4.2.0-rc1';

UPDATE droit SET profil=5 WHERE droit = 'decoupage_initialisation';

--
-- Name: voie_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE voie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: voie_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE voie_seq OWNED BY voie.code;

select setval('voie_seq', (select max(cast(code as integer))+1 from voie), false);

INSERT INTO droit (droit, profil)
SELECT 'electeurpardecoupage_pdf',(SELECT profil FROM profil WHERE libelle_profil = 'ADMINISTRATEUR')
WHERE
    NOT EXISTS (
        SELECT droit FROM droit WHERE droit = 'electeurpardecoupage_pdf'
    );
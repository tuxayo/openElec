--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.0.0-rc3 vers 4.0.0-rc4
--

UPDATE version SET version = '4.0.0-rc4';

ALTER TABLE param_mouvement ADD COLUMN insee_import_radiation character varying (100);

INSERT INTO droit VALUES ('electeur_pdfetiquette', 3);
INSERT INTO droit VALUES ('centrevote_pdfetiquette', 3);
INSERT INTO droit VALUES ('jury_pdfetiquette', 3);
INSERT INTO droit VALUES ('inscription_office_pdfetiquette', 3);

INSERT INTO droit VALUES ('revision_electorale', 3);
INSERT INTO droit VALUES ('revision_electorale_multi', 99);

UPDATE droit SET profil=5 where droit='menu_edition_multi';
UPDATE droit SET profil=99 where droit='edition_multi';
UPDATE droit SET profil=5 where droit='reqmo_multi';

update electeur set civilite='M.' where civilite='Mr';
update mouvement set civilite='M.' where civilite='Mr';
update archive set civilite='M.' where civilite='Mr';

--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.0.0-rc1 vers 4.0.0-rc2
--

UPDATE version SET version = '4.0.0-rc2';

INSERT INTO droit VALUES ('electeurpardecoupage_pdf', 5);
INSERT INTO droit VALUES ('inscription_office_valider', 5);

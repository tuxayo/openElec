--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.0.0-rc11 vers 4.0.0
--

UPDATE version SET version = '4.0.0';

--
ALTER SEQUENCE archive_seq OWNED BY archive.id;
ALTER SEQUENCE centrevote_seq OWNED BY centrevote.idcentrevote;
ALTER SEQUENCE decoupage_seq OWNED BY decoupage.id;
ALTER SEQUENCE electeur_seq OWNED BY electeur.id_electeur;
ALTER SEQUENCE inscription_office_seq OWNED BY inscription_office.id; 
ALTER SEQUENCE mouvement_seq OWNED BY mouvement.id;
ALTER SEQUENCE procuration_seq OWNED BY procuration.id_procuration;
ALTER SEQUENCE radiation_insee_seq OWNED BY radiation_insee.id;
ALTER SEQUENCE utilisateur_seq OWNED BY utilisateur.idutilisateur;
ALTER SEQUENCE xml_partenaire_seq OWNED BY xml_partenaire.xml_partenaire;


--------------------------------------------------------------------------------
-- Paramétrage - Initialisation de base
--
-- Génération de ce fichier : voir le fichier make_init.sh et faire un meld
--
-- @package openelec
-- @version SVN : $Id$
--------------------------------------------------------------------------------

--
-- PostgreSQL database dump
--

--
-- Data for Name: collectivite; Type: TABLE DATA; Schema: public; Owner: -
--

COPY collectivite (ville, logo, maire, inseeville, expediteurinsee, datetableau, id, cp, dateelection, type_interface, adresse, complement_adresse, maire_de, civilite, siret, mail, num_tel, num_fax) FROM stdin;
OPENELEC	../img/logo.png	Monsieur le Maire	00000	00000000	2015-01-10	00000	00000	\N	0		\N			\N	\N	\N	\N
\.


--
-- Data for Name: profil; Type: TABLE DATA; Schema: public; Owner: -
--

COPY profil (profil, libelle_profil) FROM stdin;
1	AFFICHAGE
2	UTILISATEUR (LIMITE)
3	UTILISATEUR
4	SUPER UTILISATEUR
5	ADMINISTRATEUR
99	NON UTILISE
\.


--
-- Data for Name: droit; Type: TABLE DATA; Schema: public; Owner: -
--

COPY droit (droit, profil) FROM stdin;
attestationmodification	2
attestationinscription	2
menu_decoupage	4
nationalite_tab	4
departement_tab	4
commune_tab	4
voie_tab	4
bureau_tab	4
canton_tab	4
bureau	4
voie	4
canton	4
commune	4
departement	4
nationalite	4
menu_saisie	2
rechercheinscription	2
inscription	2
modification	2
radiation	3
rechercheelecteur	99
electeur	4
procuration	3
centrevote	3
mairieeurope	3
menu_consultation	1
consult_electeur_tab	1
consult_electeur	2
consult_archive_tab	3
consult_archive	3
inscription_tab	2
modification_tab	2
radiation_tab	3
procuration_tab	3
centrevote_tab	3
mairieeurope_tab	3
inscription_office_tab	3
inscription_office	3
tmp	4
menu_edition	3
bureau_edition_tab	3
reqmo	3
bureau_edition	3
edition	3
statistiques	3
menu_traitement	4
traitement_j5	5
traitement_annuel	5
refonte	5
redecoupage	5
cnen_in	4
cnen_out	4
mention	4
jury	4
carteretour	4
trt_centrevote	4
archivage	5
epuration	5
decoupage	5
menu_parametrage	5
param_mouvement_tab	5
collectivite_tab	5
profil_tab	5
droit_tab	5
utilisateur_tab	5
liste_tab	5
etat	5
sousetat	5
utilisateur	5
profil	5
droit	5
collectivite	5
param_mouvement	5
liste	5
listedefaut	1
documentation	1
traitement_multi	99
traitement_multi_multi	5
collectivitedefautmenu_multi	1
collectivitedefaut_multi	1
facturation_multi	99
facturation_multi_multi	5
statistiques_multi	99
statistiques_multi_multi	5
electeur_mouvement	99
manage_ldap	5
electeur_modification	3
electeur_radiation	3
module_commission	4
module_traitement_j5	4
module_traitement_annuel	4
radiation_insee_tab	5
module_insee	5
module_election	5
module_centrevote	5
module_carteretour	5
module_jury	5
module_refonte	5
module_archivage	5
module_redecoupage	5
radiation_insee	5
logout	1
password	1
ficheelecteur	1
electeur_modification_tab	5
attestationelecteur	2
carteelecteur	4
search	3
search_tab	3
traitement_commission	5
traitement_insee_export	5
traitement_insee_inscription_office	5
traitement_insee_radiation	5
traitement_insee_export_test	5
traitement_insee_epuration	5
traitement_election_mention	5
traitement_election_epuration	5
traitement_centrevote	5
traitement_carteretour	5
traitement_carteretour_epuration	5
traitement_jury	5
traitement_jury_epuration	5
traitement_refonte	5
traitement_archivage	5
traitement_redecoupage	5
collectivitedefautmenu	99
collectivitedefaut	99
cnen_a_envoyer_pdf	5
cnen_deja_envoyer_pdf	5
inscription_office_pdf	5
procurationok_pdf	5
procurationnonok_pdf	5
traitement_j5_inscription_pdf	5
traitement_j5_radiation_pdf	5
traitement_annuel_inscription_pdf	5
traitement_annuel_modification_pdf	5
traitement_annuel_radiation_pdf	5
electeur_carteretour	5
jury_pdf	5
jury_assise_pdf	5
electeur_jury	5
archivage_pdf	5
bureau_pdf	5
canton_pdf	5
electeurparvoie_pdf	5
om_directory	99
directory	99
attestationradiation	2
widget_datetableau	2
check	5
numerobureau_tab	99
numeroliste	99
numeroliste_tab	99
numerobureau	99
radiation_insee_valider	5
collectivitedefaut_multi_utilisateur	1
module_commission_multi	99
module_traitement_j5_multi	99
module_traitement_annuel_multi	99
module_insee_multi	99
module_election_multi	99
module_centrevote_multi	99
module_carteretour_multi	99
module_jury_multi	99
module_refonte_multi	99
module_archivage_multi	99
module_redecoupage_multi	99
inscription_multi	99
electeur_mouvement_multi	99
electeur_modification_multi	99
electeur_radiation_multi	99
modification_multi	99
radiation_multi	99
procuration_multi	99
centrevote_multi	99
mairieeurope_multi	99
electeur_multi	99
bureau_edition_multi	99
electeurpardecoupage_pdf	5
inscription_office_valider	5
voie_multi	99
voie_pdf	4
electeur_pdfetiquette	3
centrevote_pdfetiquette	3
jury_pdfetiquette	3
inscription_office_pdfetiquette	3
revision_electorale	3
revision_electorale_multi	99
menu_edition_multi	5
edition_multi	99
reqmo_multi	5
additions_des_jeunes_pdfetiquette	3
traitement_j5_modification_pdf	4
traitement_jury_parametrage	4
parametrage_nb_jures	4
jury_liste_preparatoire_pdf	4
electeur_jury_tab	4
membre_commission	5
membre_commission_tab	5
convocationcommission	5
refusinscription	2
refusmodification	2
refusradiation	2
refusprocuration	3
decoupage_pdf	4
decoupage_manquant_pdf	4
decoupage_simulation	4
xml_partenaire	5
xml_partenaire_tab	5
prefecture_dematerialisation_liste_electorale	3
prefecture_dematerialisation_liste_electorale_multi	99
prefecture_dematerialisation_tableau	3
prefecture_dematerialisation_tableau_multi	99
module_prefecture	3
module_prefecture_multi	99
decoupage_initialisation	5
revision	5
revision_tab	5
\.


--
-- Data for Name: liste; Type: TABLE DATA; Schema: public; Owner: -
--

COPY liste (liste, libelle_liste, liste_insee) FROM stdin;
01	LISTE GENERALE	p
02	LISTE EUROPEENNE	ce
03	LISTE MUNICIPALES EUROPEENNE	cm
\.


--
-- Data for Name: param_mouvement; Type: TABLE DATA; Schema: public; Owner: -
--

COPY param_mouvement (code, libelle, typecat, effet, cnen, codeinscription, coderadiation, edition_carte_electeur, insee_import_radiation, om_validite_debut, om_validite_fin) FROM stdin;
IN	INSCRIPTION NATIONALITE FRANCAISE	Inscription	Immediat	Oui	1	 	1	\N	\N	\N
IM	INSCRIPTION MAJEUR J-5 (L11-2)	Inscription	Immediat	Oui	8	 	1	\N	\N	\N
PI	PREMIERE INSCRIPTION 	Inscription	1erMars	Oui	1	 	1	\N	\N	\N
CC	VENANT D'UNE AUTRE COMMUNE 	Inscription	1erMars	Oui	1	 	1	\N	\N	\N
IO	INSCRIPTION D'OFFICE	Inscription	Immediat	Oui	8	 	1	\N	\N	\N
IJ	INSCRIPTION JUDICIAIRE	Inscription	Immediat	Oui	2	 	1	\N	\N	\N
EC	MODIFICATION ETAT CIVIL 	Modification	1erMars	Non	 	 	1	\N	\N	\N
CX	MODIFICATION ADRESSE ET ETAT CIVIL 	Modification	1erMars	Non	 	 	1	\N	\N	\N
CB	CHANGEMENT ADRESSE	Modification	1erMars	Non	 	 	1	\N	\N	\N
L5	MISE SOUS TUTELLE INSEE	Radiation	Immediat	Non	 	 	0	\N	\N	\N
RA	RADIATION DEMANDEE PAR INSEE 	Radiation	Immediat	Non	 	 	0	\N	\N	\N
DN	DECES INSEE	Radiation	Immediat	Non	 	 	0	\N	\N	\N
DC	DECES	Radiation	Immediat	Oui	 	D	0	\N	\N	\N
DM	DEPART COMMUNE INSEE	Radiation	Immediat	Non	 	 	0	\N	\N	\N
DI	DOUBLE INSCRIPTIONS	Radiation	Immediat	Oui	 	E	0	\N	\N	\N
DP	DEPART AUTRE COMMUNE	Radiation	Immediat	Oui	 	P	0	\N	\N	\N
BR	MODIFICATION REDECOUPAGE	Modification	1erMars	Non	 	 	1		\N	\N
I6J	IO 18 ANS 01/03 -> 22/03/2014 (L11-2)	Inscription	Election	Oui	8	 	1		\N	2014-04-01
I6M	IO TABLEAU DU 6 MARS (L11-2)	Inscription	Election	Oui	8	 	1		\N	2014-06-01
IOD	IO 18 ANS 01/03 -> 22/03/2015 (L11-2)	Inscription	Election	Oui	8	 	1		\N	2015-04-01
I6O	IO TABLEAU DU 6 OCTOBRE (L11-2 AL2)	Inscription	Election	Oui	8	 	1		\N	2015-12-14
\.


--
-- Data for Name: revision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY revision (id, libelle, date_debut, date_effet, date_tr1, date_tr2) FROM stdin;
2	Révision exceptionnelle pour le 01/12/2015	2015-03-01	2015-12-01	2015-10-10	2015-11-30
1	Révision traditionnelle pour le 01/03/2016	2015-12-01	2016-03-01	2016-01-10	2016-02-29
3	Révision traditionnelle pour le 01/03/2015	2014-03-01	2015-03-01	2015-01-10	2015-02-28
4	Révision traditionnelle pour le 01/03/2014	2013-03-01	2014-03-01	2014-01-10	2014-02-28
5	Révision traditionnelle pour le 01/03/2013	2012-03-01	2013-03-01	2013-01-10	2013-02-28
6	Révision traditionnelle pour le 01/03/2012	2011-03-01	2012-03-01	2012-01-10	2012-02-29
7	Révision traditionnelle pour le 01/03/2017	2016-03-01	2017-03-01	2017-01-10	2017-02-28
8	Révision traditionnelle pour le 01/03/2018	2017-03-01	2018-03-01	2018-01-10	2018-02-28
\.


--
-- Name: revision_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('revision_seq', 9, false);


--
-- Data for Name: version; Type: TABLE DATA; Schema: public; Owner: -
--

COPY version (version) FROM stdin;
4.3.0
\.
--
-- PostgreSQL database dump complete
--

--
-- Specific - START
--

INSERT INTO utilisateur VALUES
(1, 'Administrateur', 'admin', '', '21232f297a57a5a743894a0e4a801fc3', 5, '01', '00000', 'db');

--
-- Specific - END
--

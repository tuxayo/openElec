--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.1.0-rc3 vers 4.1.0-rc4
--

--
UPDATE version SET version = '4.1.0-rc4';


--
ALTER TABLE param_mouvement ADD COLUMN om_validite_debut date;
ALTER TABLE param_mouvement ADD COLUMN om_validite_fin date;

--
INSERT INTO param_mouvement VALUES ('I6J', 'IO 18 ANS 01/03 -> 22/03/2014 (L11-2)', 'Inscription', 'Election', 'Oui', '8', ' ', true, '', NULL, '2014-04-01');
INSERT INTO param_mouvement VALUES ('I6M', 'IO TABLEAU DU 6 MARS (L11-2)', 'Inscription', 'Election', 'Oui', '8', ' ', true, '', NULL, '2014-06-01');


--
-- SCRIPT UTILITAIRE QUI CONTIENT DES REQUËTES DE MAINTENANCE
--

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--
-- A utiliser en connaissance de cause. Ces requêtes permettent de mettre 
-- en cohérence la table electeur et la table mouvement concernant les 
-- département et communes de naissance.
--
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

-- Codes à 5 chiffres qui commencent par 99 et 91352 92352 93352 94352
-- => code_lieu_de_naissance=code_departement_naissance 
-- le code de naissance de ces éléments ne peut exister puisque le logiciel 
-- ne stocke pas toutes les communes du monde entier
update electeur 
    set code_lieu_de_naissance=code_departement_naissance 
where (
        length(trim(code_departement_naissance))=5 
        and substr(code_lieu_de_naissance,1,2)='99' 
        and substr(code_lieu_de_naissance, 4, 3)=substr(code_departement_naissance, 3, 3)
    ) 
    or (
        length(trim(code_departement_naissance))=5 
        and substr(code_lieu_de_naissance,1,2)='99' 
        and trim(code_lieu_de_naissance)='99'
    ) 
    or (
        code_departement_naissance='91352' 
        and code_lieu_de_naissance='91 352'
    ) 
    or (
        code_departement_naissance='92352' 
        and code_lieu_de_naissance='92 352'
    ) 
    or (
        code_departement_naissance='93352' 
        and code_lieu_de_naissance='93 352'
    ) 
    or (
        code_departement_naissance='94352' 
        and code_lieu_de_naissance='94 352'
    )
;
update mouvement 
    set code_lieu_de_naissance=code_departement_naissance 
where 
    (
        (
            length(trim(code_departement_naissance))=5 
            and substr(code_lieu_de_naissance,1,2)='99' 
            and substr(code_lieu_de_naissance, 4, 3)=substr(code_departement_naissance, 3, 3)
        ) 
        or (
            length(trim(code_departement_naissance))=5 
            and substr(code_lieu_de_naissance,1,2)='99' 
            and trim(code_lieu_de_naissance)='99'
        ) 
        or (
            code_departement_naissance='91352' 
            and code_lieu_de_naissance='91 352'
        ) 
        or (
            code_departement_naissance='92352' 
            and code_lieu_de_naissance='92 352'
        ) 
        or (
            code_departement_naissance='93352' 
            and code_lieu_de_naissance='93 352'
        ) 
        or (
            code_departement_naissance='94352' 
            and code_lieu_de_naissance='94 352'
        )
    )
;

-- Codes à 5 chiffres dont : - le code_lieu_de_naissance=code_departement_naissance 
--                           - le libelle_lieu_de_naissance est vide
--                           - le libelle_departement_naissance est vide
--                           - le code_departement_naissance existe dans departement
-- => libelle_departement_naissance=departement.libelle_departement
update electeur 
    set libelle_departement_naissance=(select trim(libelle_departement) from departement where code=code_departement_naissance)
where 
    trim(libelle_lieu_de_naissance)=''
    and trim(libelle_departement_naissance)=''
    and (
            length(trim(code_departement_naissance))=5 
            and code_departement_naissance=code_lieu_de_naissance
    )
    and code_departement_naissance IN (select code from departement)
;
update mouvement 
    set libelle_departement_naissance=(select trim(libelle_departement) from departement where code=code_departement_naissance)
where 
    trim(libelle_lieu_de_naissance)=''
    and trim(libelle_departement_naissance)=''
    and (
            length(trim(code_departement_naissance))=5 
            and code_departement_naissance=code_lieu_de_naissance
    )
    and code_departement_naissance IN (select code from departement)
;

-- Codes à 5 chiffres dont : - le code_lieu_de_naissance=code_departement_naissance 
--                           - le libelle_lieu_de_naissance est vide
--                           - le libelle_departement_naissance=departement.libelle_departement
-- => libelle_lieu_de_naissance=libelle_departement_naissance 
update electeur 
    set libelle_lieu_de_naissance=libelle_departement_naissance
where 
    trim(libelle_lieu_de_naissance)=''
    and trim(libelle_departement_naissance)=(select trim(libelle_departement) from departement where code=code_departement_naissance)
    and (
            length(trim(code_departement_naissance))=5 
            and code_departement_naissance=code_lieu_de_naissance
    )
    and code_departement_naissance IN (select code from departement)
;
update mouvement 
    set libelle_lieu_de_naissance=libelle_departement_naissance
where 
    trim(libelle_lieu_de_naissance)=''
    and trim(libelle_departement_naissance)=(select trim(libelle_departement) from departement where code=code_departement_naissance)
    and (
            length(trim(code_departement_naissance))=5 
            and code_departement_naissance=code_lieu_de_naissance
    )
    and code_departement_naissance IN (select code from departement)
;

--
-- Requête permettant de récupérer la liste des électeurs qui ont des
-- informations sur les lieux de naissance qui ne correspondent pas 
-- aux critères de saisie
--
--select id_electeur, code_departement_naissance, departement.code, libelle_departement_naissance, libelle_departement, code_lieu_de_naissance, commune.code, libelle_lieu_de_naissance
--from electeur left join departement on electeur.code_departement_naissance=departement.code 
--left join commune on electeur.code_lieu_de_naissance=commune.code
--where 
--(length(trim(code_departement_naissance))=5 and code_departement_naissance<>code_lieu_de_naissance)
--OR (length(trim(code_departement_naissance))=3 and trim(code_departement_naissance)<>substr(code_lieu_de_naissance, 1, 3)) 
--OR (length(trim(code_departement_naissance))=2 and trim(code_departement_naissance)<>substr(code_lieu_de_naissance, 1, 2)) 
--OR trim(libelle_departement_naissance)='' 
--OR trim(libelle_lieu_de_naissance)='' 
--OR departement.code is NULL 
--OR (commune.code is NULL and NOT (length(trim(code_departement_naissance))=5 and code_departement_naissance=code_lieu_de_naissance) and code_lieu_de_naissance<>'99')
--OR trim(libelle_departement_naissance)<>(select trim(libelle_departement) from departement where code=code_departement_naissance)
--order by code_departement_naissance, code_lieu_de_naissance, libelle_lieu_de_naissance
--;


#! /bin/sh
##
# Ce script permet de générer les fichiers sql d'initialisation de la base de
# données pour permettre de publier une nouvelle version facilement
#
# @package openelec
# @version SVN : $Id$
##

#
#mkdir /tmp/

# Génération du fichier init.sql
pg_dump -s -O -n public -t public.utilisateur -t public.utilisateur_seq -t public.profil -t public.version -t public.collectivite -t public.droit openelec > /tmp/init.sql

# Génération du fichier init_metier.sql
pg_dump -s -O -n public -T public.om_* -T public.geometry* -T public.utilisateur -T public.utilisateur_seq -T public.profil -T public.version -T public.collectivite -T public.droit openelec > /tmp/init_metier.sql

# Génération du fichier init_parametrage.sql
pg_dump -a -O -t public.version -t public.collectivite -t public.profil -t public.droit -t public.liste -t public.param_mouvement -t public.revision openelec > /tmp/init_parametrage.sql

# Génération du fichier init_parametrage_decoupage.sql
pg_dump -a -O -t public.nationalite -t public.commune -t public.departement openelec > /tmp/init_parametrage_decoupage.sql

# Génération du fichier init_data.sql
pg_dump -a -O -t public.electeur -t public.mouvement -t public.archive -t public.procuration -t public.mairieeurope -t public.centrevote -t public.numerobureau -t public.numeroliste -t public.voie -t public.decoupage -t public.bureau -t public.canton -t public.parametrage_nb_jures openelec > /tmp/init_data.sql


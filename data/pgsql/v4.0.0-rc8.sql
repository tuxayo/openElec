--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.0.0-rc7 vers 4.0.0-rc8
--

UPDATE version SET version = '4.0.0-rc8';

update mouvement set numero_bureau=(select electeur.numero_bureau from electeur where electeur.id_electeur=mouvement.id_electeur) where mouvement.id in (select m.id from mouvement as m inner join param_mouvement as pm on m.types=pm.code inner join electeur as e on m.id_electeur=e.id_electeur where etat='actif' and m.numero_bureau=0 and pm.typecat='Modification' and m.code_bureau=e.code_bureau);

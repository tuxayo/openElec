--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      3.01 vers 4.0.0-rc1 ou
--      3.02 vers 4.0.0-rc1
--

-- MISE A JOUR TABLE VERSION --
ALTER TABLE version ALTER COLUMN version TYPE character varying(100);
UPDATE version SET version = '4.0.0-rc1';

-- MISE A JOUR TABLE ARCHIVE --
ALTER TABLE archive ALTER COLUMN libelle_departement_naissance TYPE character varying(50);
ALTER TABLE archive ALTER COLUMN libelle_lieu_de_naissance TYPE character varying(50);
ALTER TABLE archive ALTER COLUMN libelle_voie TYPE character varying(50);
ALTER TABLE archive ALTER COLUMN libelle_provenance TYPE character varying(50);
ALTER TABLE archive ALTER COLUMN ville_resident TYPE character varying(50);
ALTER TABLE archive ADD collectivite character varying(6) DEFAULT '00000'::character varying NOT NULL;

-- MISE A JOUR TABLE BUREAU --
ALTER TABLE bureau ALTER COLUMN collectivite TYPE character varying(6);
ALTER TABLE bureau ALTER COLUMN collectivite SET DEFAULT '00000'::character varying;

-- MISE A JOUR TABLE COLLECTIVITE --
ALTER TABLE collectivite ALTER COLUMN ville TYPE character varying(50);
ALTER TABLE collectivite ALTER COLUMN id TYPE character varying(6);
ALTER TABLE collectivite ALTER COLUMN id DROP DEFAULT;
ALTER TABLE collectivite ADD dateelection date;
ALTER TABLE collectivite ADD adresse character varying(50) DEFAULT ''::character varying NOT NULL;
ALTER TABLE collectivite ADD complement_adresse character varying(50);
ALTER TABLE collectivite ADD maire_de character varying(15) DEFAULT ''::character varying NOT NULL;
ALTER TABLE collectivite ADD civilite character varying(15) DEFAULT ''::character varying NOT NULL;

-- MISE A JOUR TABLE DEPARTEMENT --
ALTER TABLE departement ALTER COLUMN libelle_departement TYPE character varying(50);

-- MISE A JOUR TABLE DROIT --
ALTER TABLE droit ALTER COLUMN droit TYPE character varying(100);

-- MISE A JOUR TABLE ELECTEUR --
ALTER TABLE electeur ALTER COLUMN libelle_departement_naissance TYPE character varying(50);
ALTER TABLE electeur ALTER COLUMN libelle_lieu_de_naissance TYPE character varying(50);
ALTER TABLE electeur ALTER COLUMN libelle_voie TYPE character varying(50);
ALTER TABLE electeur ALTER COLUMN libelle_provenance TYPE character varying(50);
ALTER TABLE electeur ALTER COLUMN ville_resident TYPE character varying(50);
ALTER TABLE electeur ADD collectivite character varying(6) DEFAULT '00000'::character varying NOT NULL;

-- MISE A JOUR INSCRIPTION_OFFICE --
ALTER TABLE inscription_office ALTER COLUMN libelle_departement_naissance TYPE character varying(50);
ALTER TABLE inscription_office ALTER COLUMN libelle_lieu_de_naissance TYPE character varying(50);
ALTER TABLE inscription_office ADD collectivite character varying(6) DEFAULT '00000'::character varying NOT NULL;

-- MISE A JOUR TABLE MOUVEMENT --
ALTER TABLE mouvement ALTER COLUMN libelle_departement_naissance TYPE character varying(50);
ALTER TABLE mouvement ALTER COLUMN libelle_lieu_de_naissance TYPE character varying(50);
ALTER TABLE mouvement ALTER COLUMN libelle_voie TYPE character varying(50);
ALTER TABLE mouvement ALTER COLUMN libelle_provenance TYPE character varying(50);
ALTER TABLE mouvement ADD collectivite character varying(6) DEFAULT '00000'::character varying NOT NULL;

-- MISE A JOUR TABLE NUMEROBUREAU --
ALTER TABLE numerobureau RENAME COLUMN idlistebureau TO id;
ALTER TABLE numerobureau ALTER COLUMN id TYPE character varying(13);
ALTER TABLE numerobureau ADD collectivite character varying(6) DEFAULT '00000'::character varying NOT NULL;

-- MISE A JOUR TABLE UTILISATEUR --
ALTER TABLE utilisateur ALTER COLUMN nom TYPE character varying(100);
ALTER TABLE utilisateur ALTER COLUMN login TYPE character varying(100);
ALTER TABLE utilisateur ALTER COLUMN login DROP DEFAULT;
ALTER TABLE utilisateur ADD email character varying(100) DEFAULT ''::character varying NOT NULL;
ALTER TABLE utilisateur ALTER COLUMN collectivite TYPE character varying(6);
ALTER TABLE utilisateur ALTER COLUMN collectivite SET NOT NULL;
ALTER TABLE utilisateur ALTER COLUMN collectivite SET DEFAULT '00000'::character varying;
ALTER TABLE utilisateur ADD type character varying(6) DEFAULT 'db'::character varying;

-- MISE A JOUR TABLE VOIE --
ALTER TABLE voie ALTER COLUMN code TYPE character varying(6);
ALTER TABLE voie ALTER COLUMN libelle_voie TYPE character varying(50);
ALTER TABLE voie ALTER COLUMN ville TYPE character varying(50);
ALTER TABLE voie ALTER COLUMN code_collectivite TYPE  character varying(6);
ALTER TABLE voie ALTER COLUMN code_collectivite SET NOT NULL;

-- MISE A JOUR TABLE LISTE --
ALTER TABLE liste DROP COLUMN dernier_numero ;

-- UPDATES --
UPDATE archive SET collectivite = '00000';
UPDATE bureau SET collectivite = '00000';
UPDATE collectivite SET id = '00000', type_interface = 0;
UPDATE electeur SET collectivite = '00000';
UPDATE inscription_office SET collectivite = '00000';
UPDATE mouvement SET collectivite = '00000';
UPDATE numerobureau SET collectivite = '00000';
UPDATE numerobureau SET id = collectivite || liste || bureau;
UPDATE utilisateur SET type = 'db';
UPDATE utilisateur SET collectivite = '00000';
UPDATE voie SET code_collectivite = '00000';

-- CREATE TABLE NUMEROLISTE --
CREATE TABLE numeroliste (
    id character varying(12) DEFAULT ''::character varying NOT NULL,
    collectivite character varying(6) DEFAULT '00000'::character varying NOT NULL,
    liste character varying(6) DEFAULT '0'::bpchar NOT NULL,
    dernier_numero bigint DEFAULT (0)::bigint NOT NULL
);

-- CREATE TABLE MAIRIEEUROPE --
CREATE TABLE mairieeurope (
    id_electeur_europe bigint NOT NULL,
    date_modif date,
    utilisateur character(20),
    mairie character(40)
);

-- CONSTRAINTS --
ALTER TABLE ONLY numeroliste
    ADD CONSTRAINT numeroliste_pkey PRIMARY KEY (id);

ALTER TABLE ONLY mairieeurope
    ADD CONSTRAINT mairieeurope_pkey PRIMARY KEY (id_electeur_europe);

ALTER TABLE ONLY mairieeurope
    ADD CONSTRAINT id_electeur_europe_fkey FOREIGN KEY (id_electeur_europe) REFERENCES electeur(id_electeur) ON UPDATE RESTRICT ON DELETE RESTRICT;

-- INSERT --
INSERT INTO profil VALUES (99, 'NON UTILISE');

INSERT INTO numeroliste VALUES ('0000001', '00000','01',(
    SELECT MAX(numero_electeur) FROM electeur WHERE collectivite='00000' AND liste='01'
));

INSERT INTO numeroliste VALUES ('0000002', '00000','02',(
    SELECT MAX(numero_electeur) FROM electeur WHERE collectivite='00000' AND liste='02'
));

INSERT INTO numeroliste VALUES ('0000003', '00000','03',(
    SELECT MAX(numero_electeur) FROM electeur WHERE collectivite='00000' AND liste='03'
));

DELETE FROM droit;
INSERT INTO droit VALUES ('attestationmodification', 2);
INSERT INTO droit VALUES ('attestationinscription', 2);
INSERT INTO droit VALUES ('menu_decoupage', 4);
INSERT INTO droit VALUES ('nationalite_tab', 4);
INSERT INTO droit VALUES ('departement_tab', 4);
INSERT INTO droit VALUES ('commune_tab', 4);
INSERT INTO droit VALUES ('voie_tab', 4);
INSERT INTO droit VALUES ('bureau_tab', 4);
INSERT INTO droit VALUES ('canton_tab', 4);
INSERT INTO droit VALUES ('bureau', 4);
INSERT INTO droit VALUES ('voie', 4);
INSERT INTO droit VALUES ('canton', 4);
INSERT INTO droit VALUES ('commune', 4);
INSERT INTO droit VALUES ('departement', 4);
INSERT INTO droit VALUES ('nationalite', 4);
INSERT INTO droit VALUES ('menu_saisie', 2);
INSERT INTO droit VALUES ('rechercheinscription', 2);
INSERT INTO droit VALUES ('inscription', 2);
INSERT INTO droit VALUES ('modification', 2);
INSERT INTO droit VALUES ('radiation', 3);
INSERT INTO droit VALUES ('rechercheelecteur', 99);
INSERT INTO droit VALUES ('electeur', 4);
INSERT INTO droit VALUES ('procuration', 3);
INSERT INTO droit VALUES ('centrevote', 3);
INSERT INTO droit VALUES ('mairieeurope', 3);
INSERT INTO droit VALUES ('menu_consultation', 1);
INSERT INTO droit VALUES ('consult_electeur_tab', 1);
INSERT INTO droit VALUES ('consult_electeur', 2);
INSERT INTO droit VALUES ('consult_archive_tab', 3);
INSERT INTO droit VALUES ('consult_archive', 3);
INSERT INTO droit VALUES ('inscription_tab', 2);
INSERT INTO droit VALUES ('modification_tab', 2);
INSERT INTO droit VALUES ('radiation_tab', 3);
INSERT INTO droit VALUES ('procuration_tab', 3);
INSERT INTO droit VALUES ('centrevote_tab', 3);
INSERT INTO droit VALUES ('mairieeurope_tab', 3);
INSERT INTO droit VALUES ('inscription_office_tab', 3);
INSERT INTO droit VALUES ('inscription_office', 3);
INSERT INTO droit VALUES ('tmp', 4);
INSERT INTO droit VALUES ('menu_edition', 3);
INSERT INTO droit VALUES ('bureau_edition_tab', 3);
INSERT INTO droit VALUES ('reqmo', 3);
INSERT INTO droit VALUES ('bureau_edition', 3);
INSERT INTO droit VALUES ('edition', 3);
INSERT INTO droit VALUES ('statistiques', 3);
INSERT INTO droit VALUES ('menu_traitement', 4);
INSERT INTO droit VALUES ('traitement_j5', 5);
INSERT INTO droit VALUES ('traitement_annuel', 5);
INSERT INTO droit VALUES ('refonte', 5);
INSERT INTO droit VALUES ('redecoupage', 5);
INSERT INTO droit VALUES ('cnen_in', 4);
INSERT INTO droit VALUES ('cnen_out', 4);
INSERT INTO droit VALUES ('mention', 4);
INSERT INTO droit VALUES ('jury', 4);
INSERT INTO droit VALUES ('carteretour', 4);
INSERT INTO droit VALUES ('trt_centrevote', 4);
INSERT INTO droit VALUES ('archivage', 5);
INSERT INTO droit VALUES ('epuration', 5);
INSERT INTO droit VALUES ('decoupage', 5);
INSERT INTO droit VALUES ('menu_parametrage', 5);
INSERT INTO droit VALUES ('param_mouvement_tab', 5);
INSERT INTO droit VALUES ('collectivite_tab', 5);
INSERT INTO droit VALUES ('profil_tab', 5);
INSERT INTO droit VALUES ('droit_tab', 5);
INSERT INTO droit VALUES ('utilisateur_tab', 5);
INSERT INTO droit VALUES ('liste_tab', 5);
INSERT INTO droit VALUES ('etat', 5);
INSERT INTO droit VALUES ('sousetat', 5);
INSERT INTO droit VALUES ('utilisateur', 5);
INSERT INTO droit VALUES ('profil', 5);
INSERT INTO droit VALUES ('droit', 5);
INSERT INTO droit VALUES ('collectivite', 5);
INSERT INTO droit VALUES ('param_mouvement', 5);
INSERT INTO droit VALUES ('liste', 5);
INSERT INTO droit VALUES ('listedefaut', 1);
INSERT INTO droit VALUES ('documentation', 1);
INSERT INTO droit VALUES ('traitement_multi', 99);
INSERT INTO droit VALUES ('traitement_multi_multi', 5);
INSERT INTO droit VALUES ('collectivitedefautmenu_multi', 1);
INSERT INTO droit VALUES ('collectivitedefaut_multi', 1);
INSERT INTO droit VALUES ('facturation_multi', 99);
INSERT INTO droit VALUES ('facturation_multi_multi', 5);
INSERT INTO droit VALUES ('statistiques_multi', 99);
INSERT INTO droit VALUES ('statistiques_multi_multi', 5);
INSERT INTO droit VALUES ('electeur_mouvement', 99);
INSERT INTO droit VALUES ('manage_ldap', 5);
INSERT INTO droit VALUES ('electeur_modification', 3);
INSERT INTO droit VALUES ('electeur_radiation', 3);
INSERT INTO droit VALUES ('module_commission', 4);
INSERT INTO droit VALUES ('module_traitement_j5', 4);
INSERT INTO droit VALUES ('module_traitement_annuel', 4);
INSERT INTO droit VALUES ('radiation_insee_tab', 5);
INSERT INTO droit VALUES ('module_insee', 5);
INSERT INTO droit VALUES ('module_election', 5);
INSERT INTO droit VALUES ('module_centrevote', 5);
INSERT INTO droit VALUES ('module_carteretour', 5);
INSERT INTO droit VALUES ('module_jury', 5);
INSERT INTO droit VALUES ('module_refonte', 5);
INSERT INTO droit VALUES ('module_archivage', 5);
INSERT INTO droit VALUES ('module_redecoupage', 5);
INSERT INTO droit VALUES ('radiation_insee', 5);
INSERT INTO droit VALUES ('logout', 1);
INSERT INTO droit VALUES ('password', 1);
INSERT INTO droit VALUES ('ficheelecteur', 1);
INSERT INTO droit VALUES ('electeur_modification_tab', 5);
INSERT INTO droit VALUES ('attestationelecteur', 2);
INSERT INTO droit VALUES ('carteelecteur', 4);
INSERT INTO droit VALUES ('search', 3);
INSERT INTO droit VALUES ('search_tab', 3);
INSERT INTO droit VALUES ('traitement_commission', 5);
INSERT INTO droit VALUES ('traitement_insee_export', 5);
INSERT INTO droit VALUES ('traitement_insee_inscription_office', 5);
INSERT INTO droit VALUES ('traitement_insee_radiation', 5);
INSERT INTO droit VALUES ('traitement_insee_export_test', 5);
INSERT INTO droit VALUES ('traitement_insee_epuration', 5);
INSERT INTO droit VALUES ('traitement_election_mention', 5);
INSERT INTO droit VALUES ('traitement_election_epuration', 5);
INSERT INTO droit VALUES ('traitement_centrevote', 5);
INSERT INTO droit VALUES ('traitement_carteretour', 5);
INSERT INTO droit VALUES ('traitement_carteretour_epuration', 5);
INSERT INTO droit VALUES ('traitement_jury', 5);
INSERT INTO droit VALUES ('traitement_jury_epuration', 5);
INSERT INTO droit VALUES ('traitement_refonte', 5);
INSERT INTO droit VALUES ('traitement_archivage', 5);
INSERT INTO droit VALUES ('traitement_redecoupage', 5);
INSERT INTO droit VALUES ('collectivitedefautmenu', 99);
INSERT INTO droit VALUES ('collectivitedefaut', 99);
INSERT INTO droit VALUES ('cnen_a_envoyer_pdf', 5);
INSERT INTO droit VALUES ('cnen_deja_envoyer_pdf', 5);
INSERT INTO droit VALUES ('inscription_office_pdf', 5);
INSERT INTO droit VALUES ('procurationok_pdf', 5);
INSERT INTO droit VALUES ('procurationnonok_pdf', 5);
INSERT INTO droit VALUES ('traitement_j5_inscription_pdf', 5);
INSERT INTO droit VALUES ('traitement_j5_radiation_pdf', 5);
INSERT INTO droit VALUES ('traitement_annuel_inscription_pdf', 5);
INSERT INTO droit VALUES ('traitement_annuel_modification_pdf', 5);
INSERT INTO droit VALUES ('traitement_annuel_radiation_pdf', 5);
INSERT INTO droit VALUES ('electeur_carteretour', 5);
INSERT INTO droit VALUES ('jury_pdf', 5);
INSERT INTO droit VALUES ('jury_assise_pdf', 5);
INSERT INTO droit VALUES ('electeur_jury', 5);
INSERT INTO droit VALUES ('archivage_pdf', 5);
INSERT INTO droit VALUES ('bureau_pdf', 5);
INSERT INTO droit VALUES ('canton_pdf', 5);
INSERT INTO droit VALUES ('electeurparvoie_pdf', 5);
INSERT INTO droit VALUES ('om_directory', 99);
INSERT INTO droit VALUES ('directory', 99);
INSERT INTO droit VALUES ('attestationradiation', 2);
INSERT INTO droit VALUES ('widget_datetableau', 2);
INSERT INTO droit VALUES ('check', 5);
INSERT INTO droit VALUES ('numerobureau_tab', 99);
INSERT INTO droit VALUES ('numeroliste', 99);
INSERT INTO droit VALUES ('numeroliste_tab', 99);
INSERT INTO droit VALUES ('numerobureau', 99);
INSERT INTO droit VALUES ('radiation_insee_valider', 5);
INSERT INTO droit VALUES ('collectivitedefaut_multi_utilisateur', 1);
INSERT INTO droit VALUES ('module_commission_multi', 99);
INSERT INTO droit VALUES ('module_traitement_j5_multi', 99);
INSERT INTO droit VALUES ('module_traitement_annuel_multi', 99);
INSERT INTO droit VALUES ('module_insee_multi', 99);
INSERT INTO droit VALUES ('module_election_multi', 99);
INSERT INTO droit VALUES ('module_centrevote_multi', 99);
INSERT INTO droit VALUES ('module_carteretour_multi', 99);
INSERT INTO droit VALUES ('module_jury_multi', 99);
INSERT INTO droit VALUES ('module_refonte_multi', 99);
INSERT INTO droit VALUES ('module_archivage_multi', 99);
INSERT INTO droit VALUES ('module_redecoupage_multi', 99);
INSERT INTO droit VALUES ('inscription_multi', 99);
INSERT INTO droit VALUES ('electeur_mouvement_multi', 99);
INSERT INTO droit VALUES ('electeur_modification_multi', 99);
INSERT INTO droit VALUES ('electeur_radiation_multi', 99);
INSERT INTO droit VALUES ('modification_multi', 99);
INSERT INTO droit VALUES ('radiation_multi', 99);
INSERT INTO droit VALUES ('procuration_multi', 99);
INSERT INTO droit VALUES ('centrevote_multi', 99);
INSERT INTO droit VALUES ('mairieeurope_multi', 99);
INSERT INTO droit VALUES ('electeur_multi', 99);
INSERT INTO droit VALUES ('bureau_edition_multi', 99);
INSERT INTO droit VALUES ('edition_multi', 99);
INSERT INTO droit VALUES ('reqmo_multi', 99);
INSERT INTO droit VALUES ('menu_edition_multi', 99);;

---------- RADIATION INSEE ----------

-- Table radiation_insee
CREATE TABLE radiation_insee (
    id bigint DEFAULT (0)::bigint NOT NULL,
    nationalite character varying(50) DEFAULT ''::character varying NOT NULL,
    sexe character(1) DEFAULT ''::bpchar NOT NULL,
    nom character varying(63) DEFAULT ''::character varying NOT NULL,
    prenom1 character varying(50) DEFAULT ''::character varying NOT NULL,
    prenom2 character varying(50) DEFAULT ''::character varying,
    prenom3 character varying(50) DEFAULT ''::character varying,
    date_naissance date NOT NULL,
    localite_naissance character varying(30) DEFAULT ''::character varying,
    code_lieu_de_naissance integer DEFAULT 0 NOT NULL,
    pays_naissance  character varying(30) DEFAULT ''::character varying,
    adresse1 character varying(46) DEFAULT ''::character varying,
    adresse2 character varying(46) DEFAULT ''::character varying,
    date_deces date,
    localite_deces character varying(30) DEFAULT ''::character varying,
    code_lieu_de_deces integer DEFAULT 0,
    pays_deces  character varying(30) DEFAULT ''::character varying,
    date_nouvelle_inscription date,
    motif_nouvelle_inscription character varying(30) DEFAULT ''::character varying,
    code_perte_de_nationalite character varying(30) DEFAULT ''::character varying,
    code_autre_motif_de_radiation character varying(50),
    libelle_lieu_de_deces character varying(60),
    libelle_lieu_de_naissance character varying(30) DEFAULT ''::character varying ,
    motif_de_radiation character varying(60),
    type_de_liste character varying(60),
    types character(1) DEFAULT ''::bpchar NOT NULL,
    collectivite character varying(6)
);

-- Séquence radiation_insee
CREATE SEQUENCE radiation_insee_seq
  INCREMENT BY 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE ONLY radiation_insee
    ADD CONSTRAINT radiation_insee_pkey PRIMARY KEY (id);

-- IO/Radiation INSEE - XML EXPORT --
ALTER TABLE liste ADD liste_insee character varying(10);

CREATE TABLE xml_partenaire (
    xml_partenaire bigint NOT NULL,
    type_partenaire text,
    identifiant_autorite text,
    identifiant text,
    nom text,
    "type" text,
    "role" text,
    contact_nom text,
    contact_mail text,
    contact_numerofax text,
    contact_numerotelephone text,
    contact_adresse_numerovoie text,
    contact_adresse_typevoie text,
    contact_adresse_nomvoie text,
    contact_adresse_cedex text,
    contact_adresse_libellebureaucedex text,
    contact_adresse_divisionterritoriale text,
    service_nom text,
    service_fournisseur text,
    service_version text,
    service_accreditation text,
    collectivite character varying(10)
);

ALTER TABLE ONLY xml_partenaire
    ADD CONSTRAINT xml_partenaire_pkey PRIMARY KEY (xml_partenaire);

CREATE SEQUENCE xml_partenaire_seq START WITH 1;

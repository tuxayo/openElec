--------------------------------------------------------------------------------
-- Mise à jour des séquences avec le max + 1
--
-- Ce fichier permet de créer une fonction capable de mettre à jour toutes les
-- séquences correctement liées aux champs auxquels elles se rattachent en
-- fonction de la dernière valeur du champ dans la table. En plus de la création
-- de la fonction ce script exécute la fonction.
--
-- @package openmairie_exemple
-- @version SVN : $Id: update_sequences.sql 2304 2013-05-23 18:10:54Z fmichon $
--------------------------------------------------------------------------------

--
CREATE OR REPLACE FUNCTION fn_fixsequences() RETURNS integer AS
$BODY$
DECLARE
themax BIGINT;
mytables RECORD;
num integer;
BEGIN
 num := 0;
 FOR mytables IN
    SELECT  S.relname as seq, C.attname as attname, T.relname as relname
    FROM pg_class AS S, pg_depend AS D, pg_class AS T, pg_attribute AS C
    WHERE S.relkind = 'S'
        AND S.oid = D.objid
        AND D.refobjid = T.oid
        AND D.refobjid = C.attrelid
        AND D.refobjsubid = C.attnum
 LOOP
      EXECUTE 'SELECT MAX('||mytables.attname||') FROM '||mytables.relname||';' INTO themax;
      IF (themax is null OR themax < 0) THEN
       themax := 0;
      END IF;
      themax := themax +1;
      EXECUTE 'ALTER SEQUENCE ' || mytables.seq || ' RESTART WITH '||themax;
      num := num + 1;
  END LOOP;
  PERFORM setval('voie_seq', (select max(cast(code as integer))+1 from voie), false);
  RETURN num;
  
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE;

--
select fn_fixsequences();


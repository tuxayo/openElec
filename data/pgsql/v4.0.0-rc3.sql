--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.0.0-rc2 vers 4.0.0-rc3
--

UPDATE version SET version = '4.0.0-rc3';

INSERT INTO droit VALUES ('voie_multi', 99);
INSERT INTO droit VALUES ('voie_pdf', 4);

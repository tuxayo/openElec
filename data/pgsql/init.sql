--------------------------------------------------------------------------------
-- Instructions de base du framework openmairie
--
-- Génération de ce fichier : voir le fichier make_init.sh et faire un meld
--
-- @package openelec
-- @version SVN : $Id$
--------------------------------------------------------------------------------

--
-- PostgreSQL database dump
--

--
-- Name: collectivite; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE collectivite (
    ville character varying(50) DEFAULT ''::character varying NOT NULL,
    logo character varying(40) DEFAULT ''::character varying NOT NULL,
    maire character varying(40) DEFAULT ''::character varying NOT NULL,
    inseeville character varying(5) DEFAULT ''::character varying NOT NULL,
    expediteurinsee character varying(10) DEFAULT ''::character varying NOT NULL,
    datetableau date,
    id character varying(6) NOT NULL,
    cp character varying(5),
    dateelection date,
    type_interface integer,
    adresse character varying(50) DEFAULT ''::character varying NOT NULL,
    complement_adresse character varying(50),
    maire_de character varying(15) DEFAULT ''::character varying NOT NULL,
    civilite character varying(15) DEFAULT ''::character varying NOT NULL,
    siret character varying(14),
    mail character varying(80),
    num_tel character varying(10),
    num_fax character varying(10)
);


--
-- Name: droit; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE droit (
    droit character varying(100) DEFAULT ''::character varying NOT NULL,
    profil smallint DEFAULT (1)::smallint NOT NULL
);


--
-- Name: profil; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE profil (
    profil smallint DEFAULT (0)::smallint NOT NULL,
    libelle_profil character varying(30) DEFAULT ''::character varying NOT NULL
);


--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE utilisateur (
    idutilisateur bigint DEFAULT (0)::bigint NOT NULL,
    nom character varying(100) DEFAULT ''::character varying NOT NULL,
    login character varying(100) NOT NULL,
    email character varying(100) DEFAULT ''::character varying NOT NULL,
    pwd character varying(100) DEFAULT ''::character varying NOT NULL,
    profil smallint DEFAULT (1)::smallint NOT NULL,
    listedefaut character varying(6),
    collectivite character varying(6) NOT NULL,
    type character varying(6) DEFAULT 'db'::character varying
);


--
-- Name: utilisateur_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE utilisateur_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: utilisateur_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE utilisateur_seq OWNED BY utilisateur.idutilisateur;


--
-- Name: version; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE version (
    version character varying(100) NOT NULL
);


--
-- Name: collectivite_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY collectivite
    ADD CONSTRAINT collectivite_pkey PRIMARY KEY (id);


--
-- Name: droit_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY droit
    ADD CONSTRAINT droit_pkey PRIMARY KEY (droit);


--
-- Name: profil_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY profil
    ADD CONSTRAINT profil_pkey PRIMARY KEY (profil);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (idutilisateur);


--
-- Name: droit_profil_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY droit
    ADD CONSTRAINT droit_profil_fkey FOREIGN KEY (profil) REFERENCES profil(profil);


--
-- Name: utilisateur_profil_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_profil_fkey FOREIGN KEY (profil) REFERENCES profil(profil);


--
-- PostgreSQL database dump complete
--


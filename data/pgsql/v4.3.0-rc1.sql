--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.2.0-rc4 vers 4.3.0-dev
--
UPDATE version SET version = '4.3.0-rc1';

-- Ajout de la colonne contenant le code insee du canton
ALTER TABLE canton ADD COLUMN code_insee character varying(5);

UPDATE canton AS c
SET code_insee = substring(inseeville from 1 for 2)||lpad(c.code,2,'0')
FROM bureau AS b, collectivite as coll
WHERE c.code = b.code_canton AND b.collectivite = coll.id;

-- Modification de la date d'effet du mouvement IO 18 ANS 01/03 -> 22/03/2015 (L11-2)
UPDATE param_mouvement SET effet='1erMars' WHERE code = 'IOD';
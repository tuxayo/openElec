--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.0.0-rc4 vers 4.0.0-rc5
--

UPDATE version SET version = '4.0.0-rc5';

CREATE OR REPLACE FUNCTION withoutaccent(text) RETURNS text AS $$
    SELECT translate($1,'���������������������������������������������������','aaaaaceeeeiiiinooooouuuuyyaaaaaceeeeiiiinooooouuuuy');
$$ LANGUAGE SQL;

INSERT INTO droit VALUES ('additions_des_jeunes_pdfetiquette', 3);

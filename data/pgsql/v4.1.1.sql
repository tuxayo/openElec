--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.1.0 vers 4.1.1
--

--
UPDATE version SET version = '4.1.1';

--
ALTER TABLE param_mouvement ALTER COLUMN edition_carte_electeur DROP DEFAULT;
ALTER TABLE param_mouvement ALTER COLUMN edition_carte_electeur TYPE integer 
    USING CASE
        WHEN edition_carte_electeur::boolean is TRUE THEN 1
        ELSE 0
    END;

--
-- Mise à jour -> v3.02
--

--
-- A exécuter seulement si la table n'existe pas déjà (version précédente)
--
CREATE TABLE mairieeurope 
(
    id_electeur_europe bigint NOT NULL,
    date_modif date,
    utilisateur character(20),
    mairie character(40)
);

ALTER TABLE ONLY mairieeurope
    ADD CONSTRAINT mairieeurope_pkey PRIMARY KEY (id_electeur_europe);

ALTER TABLE ONLY mairieeurope
    ADD CONSTRAINT id_electeur_europe_fkey FOREIGN KEY (id_electeur_europe) REFERENCES electeur(id_electeur) ON UPDATE RESTRICT ON DELETE RESTRICT;


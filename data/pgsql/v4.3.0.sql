--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.3.0-rc1 vers 4.3.0-rc2
--

--
UPDATE version SET version = '4.3.0';

-- Modification de la date d'effet du mouvement IO 18 ANS 01/03 -> 22/03/2015 (L11-2)
UPDATE param_mouvement SET effet='Election' WHERE code = 'IOD';

--
--
-- Name: revision; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE revision (
    id integer NOT NULL,
    libelle character varying(150) NOT NULL,
    date_debut date NOT NULL,
    date_effet date NOT NULL,
    date_tr1 date NOT NULL,
    date_tr2 date NOT NULL
);


--
-- Name: revision_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE revision_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: revision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE revision_seq OWNED BY revision.id;


ALTER TABLE ONLY revision
    ADD CONSTRAINT revision_pkey PRIMARY KEY (id);

--
INSERT INTO "droit" ("droit", "profil") VALUES ('revision', 5);
INSERT INTO "droit" ("droit", "profil") VALUES ('revision_tab', 5);

--
--
-- Data for Name: revision; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY revision (id, libelle, date_debut, date_effet, date_tr1, date_tr2) FROM stdin;
2	Révision exceptionnelle pour le 01/12/2015	2015-03-01	2015-12-01	2015-10-10	2015-11-30
1	Révision traditionnelle pour le 01/03/2016	2015-12-01	2016-03-01	2016-01-10	2016-02-29
3	Révision traditionnelle pour le 01/03/2015	2014-03-01	2015-03-01	2015-01-10	2015-02-28
4	Révision traditionnelle pour le 01/03/2014	2013-03-01	2014-03-01	2014-01-10	2014-02-28
5	Révision traditionnelle pour le 01/03/2013	2012-03-01	2013-03-01	2013-01-10	2013-02-28
6	Révision traditionnelle pour le 01/03/2012	2011-03-01	2012-03-01	2012-01-10	2012-02-29
7	Révision traditionnelle pour le 01/03/2017	2016-03-01	2017-03-01	2017-01-10	2017-02-28
8	Révision traditionnelle pour le 01/03/2018	2017-03-01	2018-03-01	2018-01-10	2018-02-28
\.

-- Ajout du mouvement des IO pour le 06/12/2015
INSERT INTO param_mouvement VALUES ('I6O', 'IO TABLEAU DU 6 OCTOBRE (L11-2 AL2)', 'Inscription', 'Election', 'Oui', '8', ' ', 1, '', NULL, '2015-12-14');

--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.1.0-rc2 vers 4.1.0-rc3
--

UPDATE version SET version = '4.1.0-rc3';

-- Correction des libellés erronnés pour les départements 972 et 973 récupérés 
-- depuis une mise à jour d'une version antérieure à la 4.1.x
UPDATE departement SET libelle_departement = 'MARTINIQUE' WHERE code = '972';
UPDATE departement SET libelle_departement = 'GUYANE' WHERE code = '973';

-- Suppression des champs inutile dans la table xml_partenaire
ALTER TABLE xml_partenaire DROP COLUMN type;
ALTER TABLE xml_partenaire DROP COLUMN role;
ALTER TABLE xml_partenaire DROP COLUMN service_nom;
ALTER TABLE xml_partenaire DROP COLUMN service_version;
ALTER TABLE xml_partenaire DROP COLUMN identifiant_autorite;
ALTER TABLE xml_partenaire RENAME COLUMN identifiant TO siret;


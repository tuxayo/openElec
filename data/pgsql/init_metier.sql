--------------------------------------------------------------------------------
-- Instructions de base de l'applicatif 
--
-- Génération de ce fichier : voir le fichier make_init.sh et faire un meld
--
-- @package openelec
-- @version SVN : $Id$
--------------------------------------------------------------------------------

--
-- SPECIFIC - start
--

CREATE OR REPLACE FUNCTION withoutaccent(text) RETURNS text AS $$
    SELECT translate($1,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyaaaaaceeeeiiiinooooouuuuy');
$$ LANGUAGE SQL;

--
-- SPECIFIC - end
--

--
-- PostgreSQL database dump
--

--
-- Name: archive; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE archive (
    id bigint DEFAULT (0)::bigint NOT NULL,
    types character varying(3) DEFAULT 'pi'::bpchar NOT NULL,
    id_electeur bigint DEFAULT (0)::bigint,
    numero_electeur bigint DEFAULT (0)::bigint,
    liste character varying(6) DEFAULT ''::bpchar NOT NULL,
    code_bureau character varying(4) DEFAULT ''::character varying NOT NULL,
    bureauforce character(3) DEFAULT ''::bpchar NOT NULL,
    numero_bureau bigint DEFAULT (0)::bigint,
    date_modif date,
    utilisateur character varying(30) DEFAULT ''::character varying NOT NULL,
    civilite character varying(4) DEFAULT 'M.'::character varying NOT NULL,
    sexe character(1) DEFAULT ''::bpchar NOT NULL,
    nom character varying(63) DEFAULT ''::character varying NOT NULL,
    nom_usage character varying(63),
    prenom character varying(50) DEFAULT ''::character varying NOT NULL,
    situation character varying(12),
    date_naissance date NOT NULL,
    code_departement_naissance character varying(5) DEFAULT ''::character varying NOT NULL,
    libelle_departement_naissance character varying(51) DEFAULT ''::character varying NOT NULL,
    code_lieu_de_naissance character varying(6) DEFAULT ''::character varying NOT NULL,
    libelle_lieu_de_naissance character varying(50) DEFAULT ''::character varying NOT NULL,
    code_nationalite character varying(4) DEFAULT 'FRA'::character varying NOT NULL,
    code_voie character varying(10) DEFAULT ''::character varying NOT NULL,
    libelle_voie character varying(50) DEFAULT ''::character varying NOT NULL,
    numero_habitation integer DEFAULT 0 NOT NULL,
    complement_numero character varying(10),
    complement character varying(80),
    provenance character varying(6),
    libelle_provenance character varying(50) DEFAULT ''::character varying NOT NULL,
    ancien_bureau character varying(4) DEFAULT ''::character varying NOT NULL,
    observation character varying(100) DEFAULT ''::character varying NOT NULL,
    resident character(3),
    adresse_resident character varying(40),
    complement_resident character varying(40),
    cp_resident character varying(5),
    ville_resident character varying(50),
    tableau character(10),
    date_tableau date,
    envoi_cnen character(3),
    date_cnen date,
    mouvement bigint DEFAULT (0)::bigint NOT NULL,
    typecat character varying(20) DEFAULT ''::character varying NOT NULL,
    date_mouvement date,
    etat character varying(10),
    collectivite character varying(6) NOT NULL
);


--
-- Name: archive_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE archive_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: archive_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE archive_seq OWNED BY archive.id;


--
-- Name: bureau; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bureau (
    code character varying(4) DEFAULT '0'::character varying NOT NULL,
    libelle_bureau character varying(80) DEFAULT ''::character varying NOT NULL,
    adresse1 character varying(40) DEFAULT ''::character varying,
    adresse2 character varying(40) DEFAULT ''::character varying,
    adresse3 character varying(40) DEFAULT ''::character varying,
    code_canton character varying(2) DEFAULT ''::character varying NOT NULL,
    collectivite character varying(6) NOT NULL
);


--
-- Name: canton; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE canton (
    code character varying(2) NOT NULL,
    libelle_canton character(30),
    code_insee character varying(5)
);


--
-- Name: centrevote; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE centrevote (
    idcentrevote bigint NOT NULL,
    id_electeur bigint,
    debut_validite date,
    fin_validite date,
    date_modif date,
    utilisateur character(20)
);


--
-- Name: centrevote_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE centrevote_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: centrevote_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE centrevote_seq OWNED BY centrevote.idcentrevote;


--
-- Name: cnen_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cnen_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: commune; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE commune (
    code character varying(6) DEFAULT ''::character varying NOT NULL,
    code_commune character varying(4) DEFAULT ''::character varying NOT NULL,
    libelle_commune character varying(45) DEFAULT ''::character varying NOT NULL,
    code_departement character varying(5) DEFAULT ''::character varying NOT NULL
);


--
-- Name: decoupage; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE decoupage (
    id bigint DEFAULT (0)::bigint NOT NULL,
    code_bureau character varying(4) DEFAULT '0'::character varying NOT NULL,
    code_voie character varying(10) DEFAULT ''::character varying NOT NULL,
    premier_impair integer,
    dernier_impair integer,
    premier_pair integer,
    dernier_pair integer
);


--
-- Name: decoupage_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE decoupage_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: decoupage_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE decoupage_seq OWNED BY decoupage.id;


--
-- Name: departement; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE departement (
    code character varying(5) DEFAULT ''::character varying NOT NULL,
    libelle_departement character varying(51) DEFAULT ''::character varying NOT NULL
);


--
-- Name: electeur; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE electeur (
    id_electeur bigint DEFAULT (0)::bigint NOT NULL,
    numero_electeur bigint DEFAULT (0)::bigint NOT NULL,
    liste character varying(6) DEFAULT '0'::bpchar NOT NULL,
    code_bureau character varying(4) DEFAULT ''::character varying NOT NULL,
    bureauforce character(3) DEFAULT ''::bpchar NOT NULL,
    numero_bureau bigint,
    date_modif date,
    utilisateur character varying(30) DEFAULT ''::character varying NOT NULL,
    civilite character varying(4) DEFAULT 'M.'::character varying NOT NULL,
    sexe character(1) DEFAULT ''::bpchar NOT NULL,
    nom character varying(63) DEFAULT ''::character varying NOT NULL,
    nom_usage character varying(63),
    prenom character varying(50) DEFAULT ''::character varying NOT NULL,
    situation character varying(12),
    date_naissance date,
    code_departement_naissance character varying(5) DEFAULT ''::character varying NOT NULL,
    libelle_departement_naissance character varying(51) DEFAULT ''::character varying NOT NULL,
    code_lieu_de_naissance character varying(6) DEFAULT ''::character varying NOT NULL,
    libelle_lieu_de_naissance character varying(50) DEFAULT ''::character varying NOT NULL,
    code_nationalite character varying(4) DEFAULT 'FRA'::character varying NOT NULL,
    code_voie character varying(10) DEFAULT ''::character varying NOT NULL,
    libelle_voie character varying(50) DEFAULT ''::character varying NOT NULL,
    numero_habitation integer DEFAULT 0 NOT NULL,
    complement_numero character varying(10),
    complement character varying(80),
    provenance character varying(6),
    libelle_provenance character varying(50) DEFAULT ''::character varying NOT NULL,
    resident character(3),
    adresse_resident character varying(40),
    complement_resident character varying(40),
    cp_resident character varying(5),
    ville_resident character varying(50),
    tableau character(10),
    date_tableau date,
    mouvement character varying(8),
    date_mouvement date,
    typecat character varying(30) DEFAULT ''::character varying NOT NULL,
    carte smallint DEFAULT (0)::smallint NOT NULL,
    procuration character varying(255),
    jury smallint DEFAULT 0 NOT NULL,
    date_inscription date,
    code_inscription character varying(3),
    jury_effectif character(3) DEFAULT 'non'::bpchar,
    date_jeffectif date,
    collectivite character varying(6) NOT NULL,
    profession character varying(80),
    motif_dispense_jury character varying(80),
    CONSTRAINT electeur_jury_effectif_check CHECK ((jury_effectif = ANY (ARRAY['oui'::bpchar, 'non'::bpchar])))
);


--
-- Name: electeur_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE electeur_seq
    START WITH 100000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: electeur_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE electeur_seq OWNED BY electeur.id_electeur;


--
-- Name: inscription_office; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE inscription_office (
    id bigint NOT NULL,
    cog character varying(5) DEFAULT ''::character varying,
    types character varying(3) DEFAULT ''::character varying,
    civilite character varying(4) NOT NULL,
    sexe character(1) DEFAULT ''::bpchar NOT NULL,
    nom character varying(63) DEFAULT ''::character varying NOT NULL,
    nom_usage character varying(63),
    prenom character varying(50) DEFAULT ''::character varying NOT NULL,
    date_naissance date NOT NULL,
    code_departement_naissance character varying(5) DEFAULT ''::character varying NOT NULL,
    libelle_departement_naissance character varying(50) DEFAULT ''::character varying NOT NULL,
    code_lieu_de_naissance character varying(6) DEFAULT ''::character varying NOT NULL,
    libelle_lieu_de_naissance character varying(50) DEFAULT ''::character varying NOT NULL,
    adresse1 character varying(50) DEFAULT ''::character varying NOT NULL,
    adresse2 character varying(50) DEFAULT ''::character varying NOT NULL,
    adresse3 character varying(50) DEFAULT ''::character varying NOT NULL,
    adresse4 character varying(50) DEFAULT ''::character varying NOT NULL,
    adresse5 character varying(50) DEFAULT ''::character varying NOT NULL,
    adresse6 character varying(50) DEFAULT ''::character varying NOT NULL,
    idcnen character varying(9) NOT NULL,
    collectivite character varying(6) NOT NULL
);


--
-- Name: inscription_office_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE inscription_office_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: inscription_office_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE inscription_office_seq OWNED BY inscription_office.id;


--
-- Name: liste; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE liste (
    liste character varying(6) DEFAULT '0'::bpchar NOT NULL,
    libelle_liste character varying(40) DEFAULT ''::character varying NOT NULL,
    liste_insee character varying(10)
);


--
-- Name: mairieeurope; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE mairieeurope (
    id_electeur_europe bigint NOT NULL,
    date_modif date,
    utilisateur character(20),
    mairie character(40)
);


--
-- Name: membre_commission; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE membre_commission (
    membre_commission integer NOT NULL,
    civilite character varying(4),
    nom character varying(80),
    prenom character varying(80),
    adresse character varying(80),
    complement character varying(80),
    cp character varying(5),
    ville character varying(80),
    cedex character varying(20),
    bp character varying(20),
    collectivite character varying(6)
);


--
-- Name: membre_commission_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE membre_commission_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mouvement; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE mouvement (
    id bigint DEFAULT (0)::bigint NOT NULL,
    etat character varying(6) DEFAULT ''::character varying NOT NULL,
    liste character varying(6) DEFAULT '0'::bpchar NOT NULL,
    types character varying(3) DEFAULT ''::bpchar NOT NULL,
    id_electeur bigint DEFAULT (0)::bigint,
    numero_electeur bigint DEFAULT (0)::bigint,
    code_bureau character varying(4) DEFAULT ''::character varying NOT NULL,
    bureauforce character(3) DEFAULT ''::bpchar NOT NULL,
    numero_bureau bigint DEFAULT (0)::bigint,
    date_modif date,
    utilisateur character varying(30) DEFAULT ''::character varying NOT NULL,
    civilite character varying(4) DEFAULT 'M.'::character varying NOT NULL,
    sexe character(1) DEFAULT ''::bpchar NOT NULL,
    nom character varying(63) DEFAULT ''::character varying NOT NULL,
    nom_usage character varying(63),
    prenom character varying(50) DEFAULT ''::character varying NOT NULL,
    situation character varying(12),
    date_naissance date,
    code_departement_naissance character varying(5) DEFAULT ''::character varying NOT NULL,
    libelle_departement_naissance character varying(51) DEFAULT ''::character varying NOT NULL,
    code_lieu_de_naissance character varying(6) DEFAULT ''::character varying NOT NULL,
    libelle_lieu_de_naissance character varying(50) DEFAULT ''::character varying NOT NULL,
    code_nationalite character varying(4) DEFAULT 'FRA'::character varying NOT NULL,
    code_voie character varying(10) DEFAULT ''::character varying NOT NULL,
    libelle_voie character varying(50) DEFAULT ''::character varying NOT NULL,
    numero_habitation integer DEFAULT 0 NOT NULL,
    complement_numero character varying(10),
    complement character varying(80),
    provenance character varying(6),
    libelle_provenance character varying(50) DEFAULT ''::character varying NOT NULL,
    ancien_bureau character varying(4) DEFAULT ''::character varying NOT NULL,
    observation character varying(100) DEFAULT ''::character varying NOT NULL,
    resident character(3),
    adresse_resident character varying(40),
    complement_resident character varying(40),
    cp_resident character varying(5),
    ville_resident character varying(30),
    tableau character(10),
    date_j5 date,
    date_tableau date,
    envoi_cnen character(3),
    date_cnen date,
    collectivite character varying(6) NOT NULL
);


--
-- Name: mouvement_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mouvement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mouvement_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mouvement_seq OWNED BY mouvement.id;


--
-- Name: nationalite; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE nationalite (
    code character varying(4) DEFAULT ''::character varying NOT NULL,
    libelle_nationalite character varying(30) DEFAULT ''::character varying NOT NULL
);


--
-- Name: numerobureau; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE numerobureau (
    id character varying(13) DEFAULT ''::character varying NOT NULL,
    collectivite character varying(6) NOT NULL,
    liste character varying(6) DEFAULT '0'::bpchar NOT NULL,
    bureau character varying(4) DEFAULT ''::character varying NOT NULL,
    dernier_numero bigint DEFAULT (0)::bigint NOT NULL,
    dernier_numero_provisoire bigint DEFAULT (0)::bigint NOT NULL
);


--
-- Name: numeroliste; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE numeroliste (
    id character varying(12) DEFAULT ''::character varying NOT NULL,
    collectivite character varying(6) NOT NULL,
    liste character varying(6) DEFAULT '0'::bpchar NOT NULL,
    dernier_numero bigint DEFAULT (0)::bigint NOT NULL
);


--
-- Name: param_mouvement; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE param_mouvement (
    code character varying(3) DEFAULT ''::bpchar NOT NULL,
    libelle character varying(40) DEFAULT ''::character varying NOT NULL,
    typecat character varying(20) DEFAULT ''::character varying NOT NULL,
    effet character varying(10) DEFAULT ''::character varying NOT NULL,
    cnen character(3) DEFAULT ''::bpchar NOT NULL,
    codeinscription character(1) DEFAULT ''::bpchar NOT NULL,
    coderadiation character(1) DEFAULT ''::bpchar NOT NULL,
    edition_carte_electeur integer,
    insee_import_radiation character varying(100),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: parametrage_nb_jures; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE parametrage_nb_jures (
    id integer NOT NULL,
    canton character varying(2) NOT NULL,
    collectivite character varying(6) NOT NULL,
    nb_jures integer DEFAULT 0
);


--
-- Name: parametrage_nb_jures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE parametrage_nb_jures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: parametrage_nb_jures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE parametrage_nb_jures_id_seq OWNED BY parametrage_nb_jures.id;


--
-- Name: procuration; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE procuration (
    id_procuration bigint DEFAULT (0)::bigint NOT NULL,
    date_modif date,
    utilisateur character varying(20) DEFAULT ''::character varying NOT NULL,
    mandant bigint DEFAULT (0)::bigint NOT NULL,
    mandataire bigint DEFAULT (0)::bigint NOT NULL,
    resident character(3) DEFAULT ''::bpchar NOT NULL,
    debut_validite date,
    fin_validite date,
    numero integer DEFAULT 0 NOT NULL,
    types character(2) DEFAULT ''::bpchar NOT NULL,
    origine1 character varying(60) DEFAULT ''::character varying NOT NULL,
    origine2 character varying(60) DEFAULT ''::character varying NOT NULL,
    refus character(2) DEFAULT ''::bpchar NOT NULL,
    date_accord date,
    heure_accord time without time zone DEFAULT '00:00:00'::time without time zone NOT NULL,
    motif_refus character varying(65)
);


--
-- Name: procuration_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE procuration_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: procuration_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE procuration_seq OWNED BY procuration.id_procuration;


--
-- Name: radiation_insee; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE radiation_insee (
    id bigint DEFAULT (0)::bigint NOT NULL,
    nationalite character varying(50) DEFAULT ''::character varying NOT NULL,
    sexe character(1) DEFAULT ''::bpchar NOT NULL,
    nom character varying(63) DEFAULT ''::character varying NOT NULL,
    prenom1 character varying(50) DEFAULT ''::character varying NOT NULL,
    prenom2 character varying(50) DEFAULT ''::character varying,
    prenom3 character varying(50) DEFAULT ''::character varying,
    date_naissance date NOT NULL,
    localite_naissance character varying(30) DEFAULT ''::character varying,
    code_lieu_de_naissance integer DEFAULT 0 NOT NULL,
    pays_naissance character varying(30) DEFAULT ''::character varying,
    adresse1 character varying(46) DEFAULT ''::character varying,
    adresse2 character varying(46) DEFAULT ''::character varying,
    date_deces date,
    localite_deces character varying(30) DEFAULT ''::character varying,
    code_lieu_de_deces integer DEFAULT 0,
    pays_deces character varying(30) DEFAULT ''::character varying,
    date_nouvelle_inscription date,
    motif_nouvelle_inscription character varying(30) DEFAULT ''::character varying,
    code_perte_de_nationalite character varying(30) DEFAULT ''::character varying,
    code_autre_motif_de_radiation character varying(50),
    libelle_lieu_de_deces character varying(60),
    libelle_lieu_de_naissance character varying(30) DEFAULT ''::character varying,
    motif_de_radiation character varying(60),
    type_de_liste character varying(60),
    types character(1) DEFAULT ''::bpchar NOT NULL,
    collectivite character varying(6)
);


--
-- Name: radiation_insee_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE radiation_insee_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: radiation_insee_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE radiation_insee_seq OWNED BY radiation_insee.id;


--
-- Name: revision; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE revision (
    id integer NOT NULL,
    libelle character varying(150) NOT NULL,
    date_debut date NOT NULL,
    date_effet date NOT NULL,
    date_tr1 date NOT NULL,
    date_tr2 date NOT NULL
);


--
-- Name: revision_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE revision_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: revision_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE revision_seq OWNED BY revision.id;


--
-- Name: voie; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE voie (
    code character varying(6) NOT NULL,
    libelle_voie character varying(50),
    cp character varying(5),
    ville character varying(50),
    abrege character(20),
    code_collectivite character varying(6) NOT NULL
);


--
-- Name: voie_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE voie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: voie_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE voie_seq OWNED BY voie.code;


--
-- Name: xml_partenaire; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE xml_partenaire (
    xml_partenaire bigint NOT NULL,
    type_partenaire text,
    siret text,
    nom text,
    contact_nom text,
    contact_mail text,
    contact_numerofax text,
    contact_numerotelephone text,
    contact_adresse_numerovoie text,
    contact_adresse_typevoie text,
    contact_adresse_nomvoie text,
    contact_adresse_cedex text,
    contact_adresse_libellebureaucedex text,
    contact_adresse_divisionterritoriale text,
    service_fournisseur text,
    service_accreditation text,
    collectivite character varying(10)
);


--
-- Name: xml_partenaire_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE xml_partenaire_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: xml_partenaire_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE xml_partenaire_seq OWNED BY xml_partenaire.xml_partenaire;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY parametrage_nb_jures ALTER COLUMN id SET DEFAULT nextval('parametrage_nb_jures_id_seq'::regclass);


--
-- Name: archive_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY archive
    ADD CONSTRAINT archive_pkey PRIMARY KEY (id);


--
-- Name: bureau_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bureau
    ADD CONSTRAINT bureau_pkey PRIMARY KEY (code, collectivite);


--
-- Name: canton_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY canton
    ADD CONSTRAINT canton_pkey PRIMARY KEY (code);


--
-- Name: centrevote_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY centrevote
    ADD CONSTRAINT centrevote_pkey PRIMARY KEY (idcentrevote);


--
-- Name: commune_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY commune
    ADD CONSTRAINT commune_pkey PRIMARY KEY (code);


--
-- Name: decoupage_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY decoupage
    ADD CONSTRAINT decoupage_pkey PRIMARY KEY (id);


--
-- Name: departement_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY departement
    ADD CONSTRAINT departement_pkey PRIMARY KEY (code);


--
-- Name: electeur_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY electeur
    ADD CONSTRAINT electeur_pkey PRIMARY KEY (id_electeur);


--
-- Name: inscription_office_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY inscription_office
    ADD CONSTRAINT inscription_office_pkey PRIMARY KEY (id);


--
-- Name: liste_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY liste
    ADD CONSTRAINT liste_pkey PRIMARY KEY (liste);


--
-- Name: mairieeurope_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mairieeurope
    ADD CONSTRAINT mairieeurope_pkey PRIMARY KEY (id_electeur_europe);


--
-- Name: membre_commission_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY membre_commission
    ADD CONSTRAINT membre_commission_pkey PRIMARY KEY (membre_commission);


--
-- Name: mouvement_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mouvement
    ADD CONSTRAINT mouvement_pkey PRIMARY KEY (id);


--
-- Name: nationalite_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY nationalite
    ADD CONSTRAINT nationalite_pkey PRIMARY KEY (code);


--
-- Name: numerobureau_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY numerobureau
    ADD CONSTRAINT numerobureau_pkey PRIMARY KEY (id);


--
-- Name: numeroliste_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY numeroliste
    ADD CONSTRAINT numeroliste_pkey PRIMARY KEY (id);


--
-- Name: param_mouvement_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY param_mouvement
    ADD CONSTRAINT param_mouvement_pkey PRIMARY KEY (code);


--
-- Name: parametrage_nb_jures_canton_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY parametrage_nb_jures
    ADD CONSTRAINT parametrage_nb_jures_canton_key UNIQUE (canton, collectivite);


--
-- Name: parametrage_nb_jures_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY parametrage_nb_jures
    ADD CONSTRAINT parametrage_nb_jures_pkey PRIMARY KEY (id);


--
-- Name: procuration_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY procuration
    ADD CONSTRAINT procuration_pkey PRIMARY KEY (id_procuration);


--
-- Name: radiation_insee_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY radiation_insee
    ADD CONSTRAINT radiation_insee_pkey PRIMARY KEY (id);


--
-- Name: revision_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY revision
    ADD CONSTRAINT revision_pkey PRIMARY KEY (id);


--
-- Name: voie_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY voie
    ADD CONSTRAINT voie_pkey PRIMARY KEY (code);


--
-- Name: xml_partenaire_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY xml_partenaire
    ADD CONSTRAINT xml_partenaire_pkey PRIMARY KEY (xml_partenaire);


--
-- Name: ndx_electeur_date_naissance; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX ndx_electeur_date_naissance ON electeur USING btree (date_naissance);


--
-- Name: ndx_electeur_nom; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX ndx_electeur_nom ON electeur USING btree (nom);


--
-- Name: commune_code_departement_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY commune
    ADD CONSTRAINT commune_code_departement_fkey FOREIGN KEY (code_departement) REFERENCES departement(code);


--
-- Name: id_electeur_europe_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY mairieeurope
    ADD CONSTRAINT id_electeur_europe_fkey FOREIGN KEY (id_electeur_europe) REFERENCES electeur(id_electeur) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: id_electeur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY centrevote
    ADD CONSTRAINT id_electeur_fkey FOREIGN KEY (id_electeur) REFERENCES electeur(id_electeur) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: parametrage_nb_jures_canton_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY parametrage_nb_jures
    ADD CONSTRAINT parametrage_nb_jures_canton_fkey FOREIGN KEY (canton) REFERENCES canton(code) ON DELETE CASCADE;


--
-- Name: parametrage_nb_jures_collectivite_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY parametrage_nb_jures
    ADD CONSTRAINT parametrage_nb_jures_collectivite_fkey FOREIGN KEY (collectivite) REFERENCES collectivite(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--


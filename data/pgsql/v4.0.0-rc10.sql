--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.0.0-rc9 vers 4.0.0-rc10
--

UPDATE version SET version = '4.0.0-rc10';

CREATE TABLE parametrage_nb_jures (
    id serial NOT NULL,
    canton character varying(2) NOT NULL,
    collectivite character varying(6) NOT NULL,
    nb_jures integer DEFAULT 0
);
ALTER TABLE ONLY parametrage_nb_jures
    ADD CONSTRAINT parametrage_nb_jures_canton_key UNIQUE (canton, collectivite);
ALTER TABLE ONLY parametrage_nb_jures
    ADD CONSTRAINT parametrage_nb_jures_pkey PRIMARY KEY (id);
ALTER TABLE ONLY parametrage_nb_jures
    ADD CONSTRAINT parametrage_nb_jures_canton_fkey FOREIGN KEY (canton) REFERENCES canton(code) ON DELETE CASCADE;
ALTER TABLE ONLY parametrage_nb_jures
    ADD CONSTRAINT parametrage_nb_jures_collectivite_fkey FOREIGN KEY (collectivite) REFERENCES collectivite(id) ON DELETE CASCADE;

INSERT INTO droit VALUES ('traitement_jury_parametrage', 4);
INSERT INTO droit VALUES ('parametrage_nb_jures', 4);
INSERT INTO droit VALUES ('jury_liste_preparatoire_pdf', 4);
INSERT INTO droit VALUES ('electeur_jury_tab', 4);

insert into parametrage_nb_jures (canton, collectivite) select code_canton, collectivite from bureau group by code_canton, collectivite;

ALTER TABLE collectivite DROP COLUMN jury_elec;

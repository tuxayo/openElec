--
-- SCRIPT DE MIGRATION DES BASES OPENELEC
--
--      4.1.0-rc4 vers 4.1.0-rc5
--

--
UPDATE version SET version = '4.1.0-rc5';

INSERT INTO "droit" ("droit", "profil") VALUES ('prefecture_dematerialisation_liste_electorale', 3);
INSERT INTO "droit" ("droit", "profil") VALUES ('prefecture_dematerialisation_liste_electorale_multi', 99);
INSERT INTO "droit" ("droit", "profil") VALUES ('prefecture_dematerialisation_tableau', 3);
INSERT INTO "droit" ("droit", "profil") VALUES ('prefecture_dematerialisation_tableau_multi', 99);
INSERT INTO "droit" ("droit", "profil") VALUES ('module_prefecture', 3);
INSERT INTO "droit" ("droit", "profil") VALUES ('module_prefecture_multi', 99);

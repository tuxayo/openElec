--------------------------------------------------------------------------------
-- Script d'installation
--
-- ATTENTION ce script peut supprimer des données de votre base de données
-- il n'est à utiliser qu'en connaissance de cause
--
-- @package openelec
-- @version SVN : $Id$
--------------------------------------------------------------------------------

-- Usage :
-- cd data/pgsql/
-- dropdb openelec && createdb -E SQL_ASCII -T template0 openelec && psql openelec -f install.sql

-- Initialisation de postgis : A CHANGER selon les configurations
-- A commenter/décommenter pour initialiser postgis
-- --> postgis 1.5 / postgresql 9.1
--\i /usr/share/postgresql/9.1/contrib/postgis-1.5/postgis.sql
--\i /usr/share/postgresql/9.1/contrib/postgis-1.5/spatial_ref_sys.sql
-- --> postgis 2
-- CREATE EXTENSION postgis;

-- Suppression, Création et Utilisation du schéma
--DROP SCHEMA openelec;
--CREATE SCHEMA openelec;
--SET search_path = openelec, public, pg_catalog;

-- Instructions de base du framework openmairie
\i init.sql

-- Instructions de base de l'applicatif 
\i init_metier.sql

-- Instructions pour l'utilisation de postgis
--\i init_metier_sig.sql

-- Initialisation du paramétrage
\i init_parametrage.sql
\i init_parametrage_decoupage.sql

-- Initialisation d'un jeu de données
-- A commenter/décommenter pour installer un jeu de données
\i init_data.sql

-- Mise à jour depuis la dernière version (en cours de développement)
\i v4.4.4.dev0.sql
\i v4.4.4.dev0.init_data.sql
-- Mise à jour des séquences
\i update_sequences.sql

